/*
               File: Requisito
        Description: Requisito
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:32:13.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class requisito : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"REQUISITO_TIPOREQCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAREQUISITO_TIPOREQCOD4U215( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_17") == 0 )
         {
            A2049Requisito_TipoReqCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2049Requisito_TipoReqCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_17( A2049Requisito_TipoReqCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A1685Proposta_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1685Proposta_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A1685Proposta_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_18") == 0 )
         {
            A1999Requisito_ReqCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1999Requisito_ReqCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1999Requisito_ReqCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_18( A1999Requisito_ReqCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Requisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Requisito_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREQUISITO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Requisito_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynRequisito_TipoReqCod.Name = "REQUISITO_TIPOREQCOD";
         dynRequisito_TipoReqCod.WebTags = "";
         cmbRequisito_Status.Name = "REQUISITO_STATUS";
         cmbRequisito_Status.WebTags = "";
         cmbRequisito_Status.addItem("0", "Rascunho", 0);
         cmbRequisito_Status.addItem("1", "Solicitado", 0);
         cmbRequisito_Status.addItem("2", "Aprovado", 0);
         cmbRequisito_Status.addItem("3", "N�o Aprovado", 0);
         cmbRequisito_Status.addItem("4", "Pausado", 0);
         cmbRequisito_Status.addItem("5", "Cancelado", 0);
         if ( cmbRequisito_Status.ItemCount > 0 )
         {
            A1934Requisito_Status = (short)(NumberUtil.Val( cmbRequisito_Status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Requisito", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtRequisito_Ordem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public requisito( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public requisito( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Requisito_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Requisito_Codigo = aP1_Requisito_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynRequisito_TipoReqCod = new GXCombobox();
         cmbRequisito_Status = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynRequisito_TipoReqCod.ItemCount > 0 )
         {
            A2049Requisito_TipoReqCod = (int)(NumberUtil.Val( dynRequisito_TipoReqCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0))), "."));
            n2049Requisito_TipoReqCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)));
         }
         if ( cmbRequisito_Status.ItemCount > 0 )
         {
            A1934Requisito_Status = (short)(NumberUtil.Val( cmbRequisito_Status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4U215( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4U215e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4U215( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4U215( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4U215e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_54_4U215( true) ;
         }
         return  ;
      }

      protected void wb_table3_54_4U215e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4U215e( true) ;
         }
         else
         {
            wb_table1_2_4U215e( false) ;
         }
      }

      protected void wb_table3_54_4U215( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_54_4U215e( true) ;
         }
         else
         {
            wb_table3_54_4U215e( false) ;
         }
      }

      protected void wb_table2_5_4U215( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_4U215( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_4U215e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4U215e( true) ;
         }
         else
         {
            wb_table2_5_4U215e( false) ;
         }
      }

      protected void wb_table4_13_4U215( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_ordem_Internalname, "Ordem", "", "", lblTextblockrequisito_ordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRequisito_Ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ",", "")), ((edtRequisito_Ordem_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9")) : context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRequisito_Ordem_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtRequisito_Ordem_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Ordem", "right", false, "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_agrupador_Internalname, "Agrupador", "", "", lblTextblockrequisito_agrupador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRequisito_Agrupador_Internalname, A1926Requisito_Agrupador, StringUtil.RTrim( context.localUtil.Format( A1926Requisito_Agrupador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRequisito_Agrupador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtRequisito_Agrupador_Enabled, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "ReqAgrupador", "left", true, "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_tiporeqcod_Internalname, "Tipo", "", "", lblTextblockrequisito_tiporeqcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynRequisito_TipoReqCod, dynRequisito_TipoReqCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)), 1, dynRequisito_TipoReqCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynRequisito_TipoReqCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "", true, "HLP_Requisito.htm");
            dynRequisito_TipoReqCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynRequisito_TipoReqCod_Internalname, "Values", (String)(dynRequisito_TipoReqCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_titulo_Internalname, "T�tulo", "", "", lblTextblockrequisito_titulo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtRequisito_Titulo_Internalname, A1927Requisito_Titulo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", 0, 1, edtRequisito_Titulo_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "ReqNome", "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_descricao_Internalname, "Solu��o", "", "", lblTextblockrequisito_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtRequisito_Descricao_Internalname, A1923Requisito_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", 2, 1, edtRequisito_Descricao_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "ReqDescricao", "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_restricao_Internalname, "Restri��o", "", "", lblTextblockrequisito_restricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtRequisito_Restricao_Internalname, A1929Requisito_Restricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, 1, edtRequisito_Restricao_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_status_Internalname, "Status", "", "", lblTextblockrequisito_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbRequisito_Status, cmbRequisito_Status_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)), 1, cmbRequisito_Status_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbRequisito_Status.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_Requisito.htm");
            cmbRequisito_Status.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRequisito_Status_Internalname, "Values", (String)(cmbRequisito_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_pontuacao_Internalname, "Pontua��o", "", "", lblTextblockrequisito_pontuacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRequisito_Pontuacao_Internalname, StringUtil.LTrim( StringUtil.NToC( A1932Requisito_Pontuacao, 14, 5, ",", "")), ((edtRequisito_Pontuacao_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRequisito_Pontuacao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtRequisito_Pontuacao_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_Requisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_4U215e( true) ;
         }
         else
         {
            wb_table4_13_4U215e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114U2 */
         E114U2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtRequisito_Ordem_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtRequisito_Ordem_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REQUISITO_ORDEM");
                  AnyError = 1;
                  GX_FocusControl = edtRequisito_Ordem_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1931Requisito_Ordem = 0;
                  n1931Requisito_Ordem = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
               }
               else
               {
                  A1931Requisito_Ordem = (short)(context.localUtil.CToN( cgiGet( edtRequisito_Ordem_Internalname), ",", "."));
                  n1931Requisito_Ordem = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
               }
               n1931Requisito_Ordem = ((0==A1931Requisito_Ordem) ? true : false);
               A1926Requisito_Agrupador = cgiGet( edtRequisito_Agrupador_Internalname);
               n1926Requisito_Agrupador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1926Requisito_Agrupador", A1926Requisito_Agrupador);
               n1926Requisito_Agrupador = (String.IsNullOrEmpty(StringUtil.RTrim( A1926Requisito_Agrupador)) ? true : false);
               dynRequisito_TipoReqCod.CurrentValue = cgiGet( dynRequisito_TipoReqCod_Internalname);
               A2049Requisito_TipoReqCod = (int)(NumberUtil.Val( cgiGet( dynRequisito_TipoReqCod_Internalname), "."));
               n2049Requisito_TipoReqCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)));
               n2049Requisito_TipoReqCod = ((0==A2049Requisito_TipoReqCod) ? true : false);
               A1927Requisito_Titulo = cgiGet( edtRequisito_Titulo_Internalname);
               n1927Requisito_Titulo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1927Requisito_Titulo", A1927Requisito_Titulo);
               n1927Requisito_Titulo = (String.IsNullOrEmpty(StringUtil.RTrim( A1927Requisito_Titulo)) ? true : false);
               A1923Requisito_Descricao = cgiGet( edtRequisito_Descricao_Internalname);
               n1923Requisito_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1923Requisito_Descricao", A1923Requisito_Descricao);
               n1923Requisito_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1923Requisito_Descricao)) ? true : false);
               A1929Requisito_Restricao = cgiGet( edtRequisito_Restricao_Internalname);
               n1929Requisito_Restricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1929Requisito_Restricao", A1929Requisito_Restricao);
               n1929Requisito_Restricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1929Requisito_Restricao)) ? true : false);
               cmbRequisito_Status.CurrentValue = cgiGet( cmbRequisito_Status_Internalname);
               A1934Requisito_Status = (short)(NumberUtil.Val( cgiGet( cmbRequisito_Status_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtRequisito_Pontuacao_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtRequisito_Pontuacao_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REQUISITO_PONTUACAO");
                  AnyError = 1;
                  GX_FocusControl = edtRequisito_Pontuacao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1932Requisito_Pontuacao = 0;
                  n1932Requisito_Pontuacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1932Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( A1932Requisito_Pontuacao, 14, 5)));
               }
               else
               {
                  A1932Requisito_Pontuacao = context.localUtil.CToN( cgiGet( edtRequisito_Pontuacao_Internalname), ",", ".");
                  n1932Requisito_Pontuacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1932Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( A1932Requisito_Pontuacao, 14, 5)));
               }
               n1932Requisito_Pontuacao = ((Convert.ToDecimal(0)==A1932Requisito_Pontuacao) ? true : false);
               /* Read saved values. */
               Z1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1919Requisito_Codigo"), ",", "."));
               Z1935Requisito_Ativo = StringUtil.StrToBool( cgiGet( "Z1935Requisito_Ativo"));
               Z2001Requisito_Identificador = cgiGet( "Z2001Requisito_Identificador");
               n2001Requisito_Identificador = (String.IsNullOrEmpty(StringUtil.RTrim( A2001Requisito_Identificador)) ? true : false);
               Z1927Requisito_Titulo = cgiGet( "Z1927Requisito_Titulo");
               n1927Requisito_Titulo = (String.IsNullOrEmpty(StringUtil.RTrim( A1927Requisito_Titulo)) ? true : false);
               Z1926Requisito_Agrupador = cgiGet( "Z1926Requisito_Agrupador");
               n1926Requisito_Agrupador = (String.IsNullOrEmpty(StringUtil.RTrim( A1926Requisito_Agrupador)) ? true : false);
               Z1931Requisito_Ordem = (short)(context.localUtil.CToN( cgiGet( "Z1931Requisito_Ordem"), ",", "."));
               n1931Requisito_Ordem = ((0==A1931Requisito_Ordem) ? true : false);
               Z1932Requisito_Pontuacao = context.localUtil.CToN( cgiGet( "Z1932Requisito_Pontuacao"), ",", ".");
               n1932Requisito_Pontuacao = ((Convert.ToDecimal(0)==A1932Requisito_Pontuacao) ? true : false);
               Z1933Requisito_DataHomologacao = context.localUtil.CToD( cgiGet( "Z1933Requisito_DataHomologacao"), 0);
               n1933Requisito_DataHomologacao = ((DateTime.MinValue==A1933Requisito_DataHomologacao) ? true : false);
               Z1934Requisito_Status = (short)(context.localUtil.CToN( cgiGet( "Z1934Requisito_Status"), ",", "."));
               Z1943Requisito_Cadastro = context.localUtil.CToT( cgiGet( "Z1943Requisito_Cadastro"), 0);
               n1943Requisito_Cadastro = ((DateTime.MinValue==A1943Requisito_Cadastro) ? true : false);
               Z2002Requisito_Prioridade = (short)(context.localUtil.CToN( cgiGet( "Z2002Requisito_Prioridade"), ",", "."));
               n2002Requisito_Prioridade = ((0==A2002Requisito_Prioridade) ? true : false);
               Z1685Proposta_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1685Proposta_Codigo"), ",", "."));
               n1685Proposta_Codigo = ((0==A1685Proposta_Codigo) ? true : false);
               Z2049Requisito_TipoReqCod = (int)(context.localUtil.CToN( cgiGet( "Z2049Requisito_TipoReqCod"), ",", "."));
               n2049Requisito_TipoReqCod = ((0==A2049Requisito_TipoReqCod) ? true : false);
               Z1999Requisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( "Z1999Requisito_ReqCod"), ",", "."));
               n1999Requisito_ReqCod = ((0==A1999Requisito_ReqCod) ? true : false);
               A1935Requisito_Ativo = StringUtil.StrToBool( cgiGet( "Z1935Requisito_Ativo"));
               A2001Requisito_Identificador = cgiGet( "Z2001Requisito_Identificador");
               n2001Requisito_Identificador = false;
               n2001Requisito_Identificador = (String.IsNullOrEmpty(StringUtil.RTrim( A2001Requisito_Identificador)) ? true : false);
               A1933Requisito_DataHomologacao = context.localUtil.CToD( cgiGet( "Z1933Requisito_DataHomologacao"), 0);
               n1933Requisito_DataHomologacao = false;
               n1933Requisito_DataHomologacao = ((DateTime.MinValue==A1933Requisito_DataHomologacao) ? true : false);
               A1943Requisito_Cadastro = context.localUtil.CToT( cgiGet( "Z1943Requisito_Cadastro"), 0);
               n1943Requisito_Cadastro = false;
               n1943Requisito_Cadastro = ((DateTime.MinValue==A1943Requisito_Cadastro) ? true : false);
               A2002Requisito_Prioridade = (short)(context.localUtil.CToN( cgiGet( "Z2002Requisito_Prioridade"), ",", "."));
               n2002Requisito_Prioridade = false;
               n2002Requisito_Prioridade = ((0==A2002Requisito_Prioridade) ? true : false);
               A1685Proposta_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1685Proposta_Codigo"), ",", "."));
               n1685Proposta_Codigo = false;
               n1685Proposta_Codigo = ((0==A1685Proposta_Codigo) ? true : false);
               A1999Requisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( "Z1999Requisito_ReqCod"), ",", "."));
               n1999Requisito_ReqCod = false;
               n1999Requisito_ReqCod = ((0==A1999Requisito_ReqCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1685Proposta_Codigo = (int)(context.localUtil.CToN( cgiGet( "N1685Proposta_Codigo"), ",", "."));
               n1685Proposta_Codigo = ((0==A1685Proposta_Codigo) ? true : false);
               N2049Requisito_TipoReqCod = (int)(context.localUtil.CToN( cgiGet( "N2049Requisito_TipoReqCod"), ",", "."));
               n2049Requisito_TipoReqCod = ((0==A2049Requisito_TipoReqCod) ? true : false);
               N1999Requisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( "N1999Requisito_ReqCod"), ",", "."));
               n1999Requisito_ReqCod = ((0==A1999Requisito_ReqCod) ? true : false);
               AV7Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( "vREQUISITO_CODIGO"), ",", "."));
               A1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( "REQUISITO_CODIGO"), ",", "."));
               AV15Insert_Proposta_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PROPOSTA_CODIGO"), ",", "."));
               A1685Proposta_Codigo = (int)(context.localUtil.CToN( cgiGet( "PROPOSTA_CODIGO"), ",", "."));
               n1685Proposta_Codigo = ((0==A1685Proposta_Codigo) ? true : false);
               AV24Insert_Requisito_TipoReqCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_REQUISITO_TIPOREQCOD"), ",", "."));
               AV22Insert_Requisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_REQUISITO_REQCOD"), ",", "."));
               A1999Requisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( "REQUISITO_REQCOD"), ",", "."));
               n1999Requisito_ReqCod = ((0==A1999Requisito_ReqCod) ? true : false);
               A1935Requisito_Ativo = StringUtil.StrToBool( cgiGet( "REQUISITO_ATIVO"));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A1943Requisito_Cadastro = context.localUtil.CToT( cgiGet( "REQUISITO_CADASTRO"), 0);
               n1943Requisito_Cadastro = ((DateTime.MinValue==A1943Requisito_Cadastro) ? true : false);
               A2001Requisito_Identificador = cgiGet( "REQUISITO_IDENTIFICADOR");
               n2001Requisito_Identificador = (String.IsNullOrEmpty(StringUtil.RTrim( A2001Requisito_Identificador)) ? true : false);
               A1925Requisito_ReferenciaTecnica = cgiGet( "REQUISITO_REFERENCIATECNICA");
               n1925Requisito_ReferenciaTecnica = false;
               n1925Requisito_ReferenciaTecnica = (String.IsNullOrEmpty(StringUtil.RTrim( A1925Requisito_ReferenciaTecnica)) ? true : false);
               A1933Requisito_DataHomologacao = context.localUtil.CToD( cgiGet( "REQUISITO_DATAHOMOLOGACAO"), 0);
               n1933Requisito_DataHomologacao = ((DateTime.MinValue==A1933Requisito_DataHomologacao) ? true : false);
               A2002Requisito_Prioridade = (short)(context.localUtil.CToN( cgiGet( "REQUISITO_PRIORIDADE"), ",", "."));
               n2002Requisito_Prioridade = ((0==A2002Requisito_Prioridade) ? true : false);
               A1690Proposta_Objetivo = cgiGet( "PROPOSTA_OBJETIVO");
               AV25Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Requisito";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1685Proposta_Codigo), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1999Requisito_ReqCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1935Requisito_Ativo);
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A2001Requisito_Identificador, ""));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1933Requisito_DataHomologacao, "99/99/99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1943Requisito_Cadastro, "99/99/99 99:99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2002Requisito_Prioridade), "Z9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("requisito:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("requisito:[SecurityCheckFailed value for]"+"Proposta_Codigo:"+context.localUtil.Format( (decimal)(A1685Proposta_Codigo), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("requisito:[SecurityCheckFailed value for]"+"Requisito_ReqCod:"+context.localUtil.Format( (decimal)(A1999Requisito_ReqCod), "ZZZZZ9"));
                  GXUtil.WriteLog("requisito:[SecurityCheckFailed value for]"+"Requisito_Ativo:"+StringUtil.BoolToStr( A1935Requisito_Ativo));
                  GXUtil.WriteLog("requisito:[SecurityCheckFailed value for]"+"Requisito_Identificador:"+StringUtil.RTrim( context.localUtil.Format( A2001Requisito_Identificador, "")));
                  GXUtil.WriteLog("requisito:[SecurityCheckFailed value for]"+"Requisito_DataHomologacao:"+context.localUtil.Format(A1933Requisito_DataHomologacao, "99/99/99"));
                  GXUtil.WriteLog("requisito:[SecurityCheckFailed value for]"+"Requisito_Cadastro:"+context.localUtil.Format( A1943Requisito_Cadastro, "99/99/99 99:99"));
                  GXUtil.WriteLog("requisito:[SecurityCheckFailed value for]"+"Requisito_Prioridade:"+context.localUtil.Format( (decimal)(A2002Requisito_Prioridade), "Z9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1919Requisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode215 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode215;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound215 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_4U0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114U2 */
                           E114U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124U2 */
                           E124U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOPONTUACAO'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E134U2 */
                           E134U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOBTNCANCELAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E144U2 */
                           E144U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'CALCULAPONTUCAO'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E154U2 */
                           E154U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E124U2 */
            E124U2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4U215( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes4U215( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_4U0( )
      {
         BeforeValidate4U215( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4U215( ) ;
            }
            else
            {
               CheckExtendedTable4U215( ) ;
               CloseExtendedTableCursors4U215( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption4U0( )
      {
      }

      protected void E114U2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV25Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV26GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26GXV1), 8, 0)));
            while ( AV26GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV26GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Proposta_Codigo") == 0 )
               {
                  AV15Insert_Proposta_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Insert_Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Insert_Proposta_Codigo), 9, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Requisito_TipoReqCod") == 0 )
               {
                  AV24Insert_Requisito_TipoReqCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Insert_Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Insert_Requisito_TipoReqCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Requisito_ReqCod") == 0 )
               {
                  AV22Insert_Requisito_ReqCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Insert_Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Insert_Requisito_ReqCod), 6, 0)));
               }
               AV26GXV1 = (int)(AV26GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26GXV1), 8, 0)));
            }
         }
      }

      protected void E124U2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwrequisito.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E134U2( )
      {
         /* 'DoPontuacao' Routine */
         context.wjLoc = formatLink("RequisitoPontuacaoWP") + "?" + UrlEncode("" +A1919Requisito_Codigo) + "," + UrlEncode("" +0) + "," + UrlEncode(StringUtil.BoolToStr(true)) + "," + UrlEncode("" +AV17Proposta_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(false)) + "," + UrlEncode("" +AV19PerguntaResposta_Pontuacao);
         context.wjLocDisableFrm = 0;
      }

      protected void E144U2( )
      {
         /* 'DobtnCancelar' Routine */
         context.wjLoc = formatLink("proposta.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +AV17Proposta_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E154U2( )
      {
         /* 'CalculaPontucao' Routine */
         AV20Window.Url = formatLink("RequisitoPontuacaoWP") + "?" + UrlEncode("" +A1919Requisito_Codigo) + "," + UrlEncode("" +0) + "," + UrlEncode(StringUtil.BoolToStr(true)) + "," + UrlEncode("" +A1685Proposta_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(true)) + "," + UrlEncode(StringUtil.Str(A1932Requisito_Pontuacao,14,5));
         AV20Window.SetReturnParms(new Object[] {});
         AV20Window.Autoresize = 0;
         AV20Window.Height = 220;
         AV20Window.Width = 620;
         context.NewWindow(AV20Window);
      }

      protected void ZM4U215( short GX_JID )
      {
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1935Requisito_Ativo = T004U3_A1935Requisito_Ativo[0];
               Z2001Requisito_Identificador = T004U3_A2001Requisito_Identificador[0];
               Z1927Requisito_Titulo = T004U3_A1927Requisito_Titulo[0];
               Z1926Requisito_Agrupador = T004U3_A1926Requisito_Agrupador[0];
               Z1931Requisito_Ordem = T004U3_A1931Requisito_Ordem[0];
               Z1932Requisito_Pontuacao = T004U3_A1932Requisito_Pontuacao[0];
               Z1933Requisito_DataHomologacao = T004U3_A1933Requisito_DataHomologacao[0];
               Z1934Requisito_Status = T004U3_A1934Requisito_Status[0];
               Z1943Requisito_Cadastro = T004U3_A1943Requisito_Cadastro[0];
               Z2002Requisito_Prioridade = T004U3_A2002Requisito_Prioridade[0];
               Z1685Proposta_Codigo = T004U3_A1685Proposta_Codigo[0];
               Z2049Requisito_TipoReqCod = T004U3_A2049Requisito_TipoReqCod[0];
               Z1999Requisito_ReqCod = T004U3_A1999Requisito_ReqCod[0];
            }
            else
            {
               Z1935Requisito_Ativo = A1935Requisito_Ativo;
               Z2001Requisito_Identificador = A2001Requisito_Identificador;
               Z1927Requisito_Titulo = A1927Requisito_Titulo;
               Z1926Requisito_Agrupador = A1926Requisito_Agrupador;
               Z1931Requisito_Ordem = A1931Requisito_Ordem;
               Z1932Requisito_Pontuacao = A1932Requisito_Pontuacao;
               Z1933Requisito_DataHomologacao = A1933Requisito_DataHomologacao;
               Z1934Requisito_Status = A1934Requisito_Status;
               Z1943Requisito_Cadastro = A1943Requisito_Cadastro;
               Z2002Requisito_Prioridade = A2002Requisito_Prioridade;
               Z1685Proposta_Codigo = A1685Proposta_Codigo;
               Z2049Requisito_TipoReqCod = A2049Requisito_TipoReqCod;
               Z1999Requisito_ReqCod = A1999Requisito_ReqCod;
            }
         }
         if ( GX_JID == -15 )
         {
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
            Z1935Requisito_Ativo = A1935Requisito_Ativo;
            Z2001Requisito_Identificador = A2001Requisito_Identificador;
            Z1927Requisito_Titulo = A1927Requisito_Titulo;
            Z1923Requisito_Descricao = A1923Requisito_Descricao;
            Z1925Requisito_ReferenciaTecnica = A1925Requisito_ReferenciaTecnica;
            Z1926Requisito_Agrupador = A1926Requisito_Agrupador;
            Z1929Requisito_Restricao = A1929Requisito_Restricao;
            Z1931Requisito_Ordem = A1931Requisito_Ordem;
            Z1932Requisito_Pontuacao = A1932Requisito_Pontuacao;
            Z1933Requisito_DataHomologacao = A1933Requisito_DataHomologacao;
            Z1934Requisito_Status = A1934Requisito_Status;
            Z1943Requisito_Cadastro = A1943Requisito_Cadastro;
            Z2002Requisito_Prioridade = A2002Requisito_Prioridade;
            Z1685Proposta_Codigo = A1685Proposta_Codigo;
            Z2049Requisito_TipoReqCod = A2049Requisito_TipoReqCod;
            Z1999Requisito_ReqCod = A1999Requisito_ReqCod;
            Z1690Proposta_Objetivo = A1690Proposta_Objetivo;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAREQUISITO_TIPOREQCOD_html4U215( ) ;
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV25Pgmname = "Requisito";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Pgmname", AV25Pgmname);
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Requisito_Codigo) )
         {
            A1919Requisito_Codigo = AV7Requisito_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV24Insert_Requisito_TipoReqCod) )
         {
            dynRequisito_TipoReqCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynRequisito_TipoReqCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynRequisito_TipoReqCod.Enabled), 5, 0)));
         }
         else
         {
            dynRequisito_TipoReqCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynRequisito_TipoReqCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynRequisito_TipoReqCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1935Requisito_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1935Requisito_Ativo", A1935Requisito_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV22Insert_Requisito_ReqCod) )
         {
            A1999Requisito_ReqCod = AV22Insert_Requisito_ReqCod;
            n1999Requisito_ReqCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1999Requisito_ReqCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV24Insert_Requisito_TipoReqCod) )
         {
            A2049Requisito_TipoReqCod = AV24Insert_Requisito_TipoReqCod;
            n2049Requisito_TipoReqCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV15Insert_Proposta_Codigo) )
         {
            A1685Proposta_Codigo = AV15Insert_Proposta_Codigo;
            n1685Proposta_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A1943Requisito_Cadastro) && ( Gx_BScreen == 0 ) )
         {
            A1943Requisito_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
            n1943Requisito_Cadastro = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1943Requisito_Cadastro", context.localUtil.TToC( A1943Requisito_Cadastro, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T004U4 */
            pr_default.execute(2, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
            A1690Proposta_Objetivo = T004U4_A1690Proposta_Objetivo[0];
            pr_default.close(2);
         }
      }

      protected void Load4U215( )
      {
         /* Using cursor T004U7 */
         pr_default.execute(5, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound215 = 1;
            A1935Requisito_Ativo = T004U7_A1935Requisito_Ativo[0];
            A2001Requisito_Identificador = T004U7_A2001Requisito_Identificador[0];
            n2001Requisito_Identificador = T004U7_n2001Requisito_Identificador[0];
            A1927Requisito_Titulo = T004U7_A1927Requisito_Titulo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1927Requisito_Titulo", A1927Requisito_Titulo);
            n1927Requisito_Titulo = T004U7_n1927Requisito_Titulo[0];
            A1923Requisito_Descricao = T004U7_A1923Requisito_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1923Requisito_Descricao", A1923Requisito_Descricao);
            n1923Requisito_Descricao = T004U7_n1923Requisito_Descricao[0];
            A1690Proposta_Objetivo = T004U7_A1690Proposta_Objetivo[0];
            A1925Requisito_ReferenciaTecnica = T004U7_A1925Requisito_ReferenciaTecnica[0];
            n1925Requisito_ReferenciaTecnica = T004U7_n1925Requisito_ReferenciaTecnica[0];
            A1926Requisito_Agrupador = T004U7_A1926Requisito_Agrupador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1926Requisito_Agrupador", A1926Requisito_Agrupador);
            n1926Requisito_Agrupador = T004U7_n1926Requisito_Agrupador[0];
            A1929Requisito_Restricao = T004U7_A1929Requisito_Restricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1929Requisito_Restricao", A1929Requisito_Restricao);
            n1929Requisito_Restricao = T004U7_n1929Requisito_Restricao[0];
            A1931Requisito_Ordem = T004U7_A1931Requisito_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
            n1931Requisito_Ordem = T004U7_n1931Requisito_Ordem[0];
            A1932Requisito_Pontuacao = T004U7_A1932Requisito_Pontuacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1932Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( A1932Requisito_Pontuacao, 14, 5)));
            n1932Requisito_Pontuacao = T004U7_n1932Requisito_Pontuacao[0];
            A1933Requisito_DataHomologacao = T004U7_A1933Requisito_DataHomologacao[0];
            n1933Requisito_DataHomologacao = T004U7_n1933Requisito_DataHomologacao[0];
            A1934Requisito_Status = T004U7_A1934Requisito_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
            A1943Requisito_Cadastro = T004U7_A1943Requisito_Cadastro[0];
            n1943Requisito_Cadastro = T004U7_n1943Requisito_Cadastro[0];
            A2002Requisito_Prioridade = T004U7_A2002Requisito_Prioridade[0];
            n2002Requisito_Prioridade = T004U7_n2002Requisito_Prioridade[0];
            A1685Proposta_Codigo = T004U7_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = T004U7_n1685Proposta_Codigo[0];
            A2049Requisito_TipoReqCod = T004U7_A2049Requisito_TipoReqCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)));
            n2049Requisito_TipoReqCod = T004U7_n2049Requisito_TipoReqCod[0];
            A1999Requisito_ReqCod = T004U7_A1999Requisito_ReqCod[0];
            n1999Requisito_ReqCod = T004U7_n1999Requisito_ReqCod[0];
            ZM4U215( -15) ;
         }
         pr_default.close(5);
         OnLoadActions4U215( ) ;
      }

      protected void OnLoadActions4U215( )
      {
      }

      protected void CheckExtendedTable4U215( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T004U5 */
         pr_default.execute(3, new Object[] {n2049Requisito_TipoReqCod, A2049Requisito_TipoReqCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A2049Requisito_TipoReqCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Requisito_Tipo Requisito'.", "ForeignKeyNotFound", 1, "REQUISITO_TIPOREQCOD");
               AnyError = 1;
               GX_FocusControl = dynRequisito_TipoReqCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(3);
         if ( ! ( ( A1934Requisito_Status == 0 ) || ( A1934Requisito_Status == 1 ) || ( A1934Requisito_Status == 2 ) || ( A1934Requisito_Status == 3 ) || ( A1934Requisito_Status == 4 ) || ( A1934Requisito_Status == 5 ) ) )
         {
            GX_msglist.addItem("Campo Status fora do intervalo", "OutOfRange", 1, "REQUISITO_STATUS");
            AnyError = 1;
            GX_FocusControl = cmbRequisito_Status_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T004U4 */
         pr_default.execute(2, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A1685Proposta_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Proposta'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1690Proposta_Objetivo = T004U4_A1690Proposta_Objetivo[0];
         pr_default.close(2);
         /* Using cursor T004U6 */
         pr_default.execute(4, new Object[] {n1999Requisito_ReqCod, A1999Requisito_ReqCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A1999Requisito_ReqCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Requisito_Requisito De'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors4U215( )
      {
         pr_default.close(3);
         pr_default.close(2);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_17( int A2049Requisito_TipoReqCod )
      {
         /* Using cursor T004U8 */
         pr_default.execute(6, new Object[] {n2049Requisito_TipoReqCod, A2049Requisito_TipoReqCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A2049Requisito_TipoReqCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Requisito_Tipo Requisito'.", "ForeignKeyNotFound", 1, "REQUISITO_TIPOREQCOD");
               AnyError = 1;
               GX_FocusControl = dynRequisito_TipoReqCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_16( int A1685Proposta_Codigo )
      {
         /* Using cursor T004U9 */
         pr_default.execute(7, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A1685Proposta_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Proposta'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1690Proposta_Objetivo = T004U9_A1690Proposta_Objetivo[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A1690Proposta_Objetivo)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_18( int A1999Requisito_ReqCod )
      {
         /* Using cursor T004U10 */
         pr_default.execute(8, new Object[] {n1999Requisito_ReqCod, A1999Requisito_ReqCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A1999Requisito_ReqCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Requisito_Requisito De'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void GetKey4U215( )
      {
         /* Using cursor T004U11 */
         pr_default.execute(9, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound215 = 1;
         }
         else
         {
            RcdFound215 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004U3 */
         pr_default.execute(1, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4U215( 15) ;
            RcdFound215 = 1;
            A1919Requisito_Codigo = T004U3_A1919Requisito_Codigo[0];
            A1935Requisito_Ativo = T004U3_A1935Requisito_Ativo[0];
            A2001Requisito_Identificador = T004U3_A2001Requisito_Identificador[0];
            n2001Requisito_Identificador = T004U3_n2001Requisito_Identificador[0];
            A1927Requisito_Titulo = T004U3_A1927Requisito_Titulo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1927Requisito_Titulo", A1927Requisito_Titulo);
            n1927Requisito_Titulo = T004U3_n1927Requisito_Titulo[0];
            A1923Requisito_Descricao = T004U3_A1923Requisito_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1923Requisito_Descricao", A1923Requisito_Descricao);
            n1923Requisito_Descricao = T004U3_n1923Requisito_Descricao[0];
            A1925Requisito_ReferenciaTecnica = T004U3_A1925Requisito_ReferenciaTecnica[0];
            n1925Requisito_ReferenciaTecnica = T004U3_n1925Requisito_ReferenciaTecnica[0];
            A1926Requisito_Agrupador = T004U3_A1926Requisito_Agrupador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1926Requisito_Agrupador", A1926Requisito_Agrupador);
            n1926Requisito_Agrupador = T004U3_n1926Requisito_Agrupador[0];
            A1929Requisito_Restricao = T004U3_A1929Requisito_Restricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1929Requisito_Restricao", A1929Requisito_Restricao);
            n1929Requisito_Restricao = T004U3_n1929Requisito_Restricao[0];
            A1931Requisito_Ordem = T004U3_A1931Requisito_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
            n1931Requisito_Ordem = T004U3_n1931Requisito_Ordem[0];
            A1932Requisito_Pontuacao = T004U3_A1932Requisito_Pontuacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1932Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( A1932Requisito_Pontuacao, 14, 5)));
            n1932Requisito_Pontuacao = T004U3_n1932Requisito_Pontuacao[0];
            A1933Requisito_DataHomologacao = T004U3_A1933Requisito_DataHomologacao[0];
            n1933Requisito_DataHomologacao = T004U3_n1933Requisito_DataHomologacao[0];
            A1934Requisito_Status = T004U3_A1934Requisito_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
            A1943Requisito_Cadastro = T004U3_A1943Requisito_Cadastro[0];
            n1943Requisito_Cadastro = T004U3_n1943Requisito_Cadastro[0];
            A2002Requisito_Prioridade = T004U3_A2002Requisito_Prioridade[0];
            n2002Requisito_Prioridade = T004U3_n2002Requisito_Prioridade[0];
            A1685Proposta_Codigo = T004U3_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = T004U3_n1685Proposta_Codigo[0];
            A2049Requisito_TipoReqCod = T004U3_A2049Requisito_TipoReqCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)));
            n2049Requisito_TipoReqCod = T004U3_n2049Requisito_TipoReqCod[0];
            A1999Requisito_ReqCod = T004U3_A1999Requisito_ReqCod[0];
            n1999Requisito_ReqCod = T004U3_n1999Requisito_ReqCod[0];
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
            sMode215 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load4U215( ) ;
            if ( AnyError == 1 )
            {
               RcdFound215 = 0;
               InitializeNonKey4U215( ) ;
            }
            Gx_mode = sMode215;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound215 = 0;
            InitializeNonKey4U215( ) ;
            sMode215 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode215;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4U215( ) ;
         if ( RcdFound215 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound215 = 0;
         /* Using cursor T004U12 */
         pr_default.execute(10, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T004U12_A1919Requisito_Codigo[0] < A1919Requisito_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T004U12_A1919Requisito_Codigo[0] > A1919Requisito_Codigo ) ) )
            {
               A1919Requisito_Codigo = T004U12_A1919Requisito_Codigo[0];
               RcdFound215 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound215 = 0;
         /* Using cursor T004U13 */
         pr_default.execute(11, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T004U13_A1919Requisito_Codigo[0] > A1919Requisito_Codigo ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T004U13_A1919Requisito_Codigo[0] < A1919Requisito_Codigo ) ) )
            {
               A1919Requisito_Codigo = T004U13_A1919Requisito_Codigo[0];
               RcdFound215 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4U215( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtRequisito_Ordem_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4U215( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound215 == 1 )
            {
               if ( A1919Requisito_Codigo != Z1919Requisito_Codigo )
               {
                  A1919Requisito_Codigo = Z1919Requisito_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtRequisito_Ordem_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update4U215( ) ;
                  GX_FocusControl = edtRequisito_Ordem_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1919Requisito_Codigo != Z1919Requisito_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtRequisito_Ordem_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4U215( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtRequisito_Ordem_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4U215( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1919Requisito_Codigo != Z1919Requisito_Codigo )
         {
            A1919Requisito_Codigo = Z1919Requisito_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtRequisito_Ordem_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency4U215( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004U2 */
            pr_default.execute(0, new Object[] {A1919Requisito_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Requisito"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1935Requisito_Ativo != T004U2_A1935Requisito_Ativo[0] ) || ( StringUtil.StrCmp(Z2001Requisito_Identificador, T004U2_A2001Requisito_Identificador[0]) != 0 ) || ( StringUtil.StrCmp(Z1927Requisito_Titulo, T004U2_A1927Requisito_Titulo[0]) != 0 ) || ( StringUtil.StrCmp(Z1926Requisito_Agrupador, T004U2_A1926Requisito_Agrupador[0]) != 0 ) || ( Z1931Requisito_Ordem != T004U2_A1931Requisito_Ordem[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1932Requisito_Pontuacao != T004U2_A1932Requisito_Pontuacao[0] ) || ( Z1933Requisito_DataHomologacao != T004U2_A1933Requisito_DataHomologacao[0] ) || ( Z1934Requisito_Status != T004U2_A1934Requisito_Status[0] ) || ( Z1943Requisito_Cadastro != T004U2_A1943Requisito_Cadastro[0] ) || ( Z2002Requisito_Prioridade != T004U2_A2002Requisito_Prioridade[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1685Proposta_Codigo != T004U2_A1685Proposta_Codigo[0] ) || ( Z2049Requisito_TipoReqCod != T004U2_A2049Requisito_TipoReqCod[0] ) || ( Z1999Requisito_ReqCod != T004U2_A1999Requisito_ReqCod[0] ) )
            {
               if ( Z1935Requisito_Ativo != T004U2_A1935Requisito_Ativo[0] )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z1935Requisito_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A1935Requisito_Ativo[0]);
               }
               if ( StringUtil.StrCmp(Z2001Requisito_Identificador, T004U2_A2001Requisito_Identificador[0]) != 0 )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_Identificador");
                  GXUtil.WriteLogRaw("Old: ",Z2001Requisito_Identificador);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A2001Requisito_Identificador[0]);
               }
               if ( StringUtil.StrCmp(Z1927Requisito_Titulo, T004U2_A1927Requisito_Titulo[0]) != 0 )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_Titulo");
                  GXUtil.WriteLogRaw("Old: ",Z1927Requisito_Titulo);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A1927Requisito_Titulo[0]);
               }
               if ( StringUtil.StrCmp(Z1926Requisito_Agrupador, T004U2_A1926Requisito_Agrupador[0]) != 0 )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_Agrupador");
                  GXUtil.WriteLogRaw("Old: ",Z1926Requisito_Agrupador);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A1926Requisito_Agrupador[0]);
               }
               if ( Z1931Requisito_Ordem != T004U2_A1931Requisito_Ordem[0] )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_Ordem");
                  GXUtil.WriteLogRaw("Old: ",Z1931Requisito_Ordem);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A1931Requisito_Ordem[0]);
               }
               if ( Z1932Requisito_Pontuacao != T004U2_A1932Requisito_Pontuacao[0] )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_Pontuacao");
                  GXUtil.WriteLogRaw("Old: ",Z1932Requisito_Pontuacao);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A1932Requisito_Pontuacao[0]);
               }
               if ( Z1933Requisito_DataHomologacao != T004U2_A1933Requisito_DataHomologacao[0] )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_DataHomologacao");
                  GXUtil.WriteLogRaw("Old: ",Z1933Requisito_DataHomologacao);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A1933Requisito_DataHomologacao[0]);
               }
               if ( Z1934Requisito_Status != T004U2_A1934Requisito_Status[0] )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_Status");
                  GXUtil.WriteLogRaw("Old: ",Z1934Requisito_Status);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A1934Requisito_Status[0]);
               }
               if ( Z1943Requisito_Cadastro != T004U2_A1943Requisito_Cadastro[0] )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_Cadastro");
                  GXUtil.WriteLogRaw("Old: ",Z1943Requisito_Cadastro);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A1943Requisito_Cadastro[0]);
               }
               if ( Z2002Requisito_Prioridade != T004U2_A2002Requisito_Prioridade[0] )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_Prioridade");
                  GXUtil.WriteLogRaw("Old: ",Z2002Requisito_Prioridade);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A2002Requisito_Prioridade[0]);
               }
               if ( Z1685Proposta_Codigo != T004U2_A1685Proposta_Codigo[0] )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Proposta_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z1685Proposta_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A1685Proposta_Codigo[0]);
               }
               if ( Z2049Requisito_TipoReqCod != T004U2_A2049Requisito_TipoReqCod[0] )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_TipoReqCod");
                  GXUtil.WriteLogRaw("Old: ",Z2049Requisito_TipoReqCod);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A2049Requisito_TipoReqCod[0]);
               }
               if ( Z1999Requisito_ReqCod != T004U2_A1999Requisito_ReqCod[0] )
               {
                  GXUtil.WriteLog("requisito:[seudo value changed for attri]"+"Requisito_ReqCod");
                  GXUtil.WriteLogRaw("Old: ",Z1999Requisito_ReqCod);
                  GXUtil.WriteLogRaw("Current: ",T004U2_A1999Requisito_ReqCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Requisito"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4U215( )
      {
         BeforeValidate4U215( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4U215( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4U215( 0) ;
            CheckOptimisticConcurrency4U215( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4U215( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4U215( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004U14 */
                     pr_default.execute(12, new Object[] {A1935Requisito_Ativo, n2001Requisito_Identificador, A2001Requisito_Identificador, n1927Requisito_Titulo, A1927Requisito_Titulo, n1923Requisito_Descricao, A1923Requisito_Descricao, n1925Requisito_ReferenciaTecnica, A1925Requisito_ReferenciaTecnica, n1926Requisito_Agrupador, A1926Requisito_Agrupador, n1929Requisito_Restricao, A1929Requisito_Restricao, n1931Requisito_Ordem, A1931Requisito_Ordem, n1932Requisito_Pontuacao, A1932Requisito_Pontuacao, n1933Requisito_DataHomologacao, A1933Requisito_DataHomologacao, A1934Requisito_Status, n1943Requisito_Cadastro, A1943Requisito_Cadastro, n2002Requisito_Prioridade, A2002Requisito_Prioridade, n1685Proposta_Codigo, A1685Proposta_Codigo, n2049Requisito_TipoReqCod, A2049Requisito_TipoReqCod, n1999Requisito_ReqCod, A1999Requisito_ReqCod});
                     A1919Requisito_Codigo = T004U14_A1919Requisito_Codigo[0];
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("Requisito") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4U0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4U215( ) ;
            }
            EndLevel4U215( ) ;
         }
         CloseExtendedTableCursors4U215( ) ;
      }

      protected void Update4U215( )
      {
         BeforeValidate4U215( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4U215( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4U215( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4U215( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4U215( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004U15 */
                     pr_default.execute(13, new Object[] {A1935Requisito_Ativo, n2001Requisito_Identificador, A2001Requisito_Identificador, n1927Requisito_Titulo, A1927Requisito_Titulo, n1923Requisito_Descricao, A1923Requisito_Descricao, n1925Requisito_ReferenciaTecnica, A1925Requisito_ReferenciaTecnica, n1926Requisito_Agrupador, A1926Requisito_Agrupador, n1929Requisito_Restricao, A1929Requisito_Restricao, n1931Requisito_Ordem, A1931Requisito_Ordem, n1932Requisito_Pontuacao, A1932Requisito_Pontuacao, n1933Requisito_DataHomologacao, A1933Requisito_DataHomologacao, A1934Requisito_Status, n1943Requisito_Cadastro, A1943Requisito_Cadastro, n2002Requisito_Prioridade, A2002Requisito_Prioridade, n1685Proposta_Codigo, A1685Proposta_Codigo, n2049Requisito_TipoReqCod, A2049Requisito_TipoReqCod, n1999Requisito_ReqCod, A1999Requisito_ReqCod, A1919Requisito_Codigo});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("Requisito") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Requisito"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4U215( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4U215( ) ;
         }
         CloseExtendedTableCursors4U215( ) ;
      }

      protected void DeferredUpdate4U215( )
      {
      }

      protected void delete( )
      {
         BeforeValidate4U215( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4U215( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4U215( ) ;
            AfterConfirm4U215( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4U215( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004U16 */
                  pr_default.execute(14, new Object[] {A1919Requisito_Codigo});
                  pr_default.close(14);
                  dsDefault.SmartCacheProvider.SetUpdated("Requisito") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode215 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel4U215( ) ;
         Gx_mode = sMode215;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls4U215( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T004U17 */
            pr_default.execute(15, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
            A1690Proposta_Objetivo = T004U17_A1690Proposta_Objetivo[0];
            pr_default.close(15);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T004U18 */
            pr_default.execute(16, new Object[] {A1919Requisito_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T213"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor T004U19 */
            pr_default.execute(17, new Object[] {A1919Requisito_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Requisito"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
            /* Using cursor T004U20 */
            pr_default.execute(18, new Object[] {A1919Requisito_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Req Neg Req Tec"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
         }
      }

      protected void EndLevel4U215( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4U215( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(15);
            context.CommitDataStores( "Requisito");
            if ( AnyError == 0 )
            {
               ConfirmValues4U0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(15);
            context.RollbackDataStores( "Requisito");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4U215( )
      {
         /* Scan By routine */
         /* Using cursor T004U21 */
         pr_default.execute(19);
         RcdFound215 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound215 = 1;
            A1919Requisito_Codigo = T004U21_A1919Requisito_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4U215( )
      {
         /* Scan next routine */
         pr_default.readNext(19);
         RcdFound215 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound215 = 1;
            A1919Requisito_Codigo = T004U21_A1919Requisito_Codigo[0];
         }
      }

      protected void ScanEnd4U215( )
      {
         pr_default.close(19);
      }

      protected void AfterConfirm4U215( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4U215( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4U215( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4U215( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4U215( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4U215( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4U215( )
      {
         edtRequisito_Ordem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Ordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Ordem_Enabled), 5, 0)));
         edtRequisito_Agrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Agrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Agrupador_Enabled), 5, 0)));
         dynRequisito_TipoReqCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynRequisito_TipoReqCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynRequisito_TipoReqCod.Enabled), 5, 0)));
         edtRequisito_Titulo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Titulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Titulo_Enabled), 5, 0)));
         edtRequisito_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Descricao_Enabled), 5, 0)));
         edtRequisito_Restricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Restricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Restricao_Enabled), 5, 0)));
         cmbRequisito_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRequisito_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbRequisito_Status.Enabled), 5, 0)));
         edtRequisito_Pontuacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Pontuacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Pontuacao_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4U0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299321578");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("requisito.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Requisito_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1919Requisito_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1919Requisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1935Requisito_Ativo", Z1935Requisito_Ativo);
         GxWebStd.gx_hidden_field( context, "Z2001Requisito_Identificador", Z2001Requisito_Identificador);
         GxWebStd.gx_hidden_field( context, "Z1927Requisito_Titulo", Z1927Requisito_Titulo);
         GxWebStd.gx_hidden_field( context, "Z1926Requisito_Agrupador", Z1926Requisito_Agrupador);
         GxWebStd.gx_hidden_field( context, "Z1931Requisito_Ordem", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1931Requisito_Ordem), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1932Requisito_Pontuacao", StringUtil.LTrim( StringUtil.NToC( Z1932Requisito_Pontuacao, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1933Requisito_DataHomologacao", context.localUtil.DToC( Z1933Requisito_DataHomologacao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1934Requisito_Status", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1934Requisito_Status), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1943Requisito_Cadastro", context.localUtil.TToC( Z1943Requisito_Cadastro, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z2002Requisito_Prioridade", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2002Requisito_Prioridade), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1685Proposta_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1685Proposta_Codigo), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2049Requisito_TipoReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1999Requisito_ReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1685Proposta_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1685Proposta_Codigo), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2049Requisito_TipoReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1999Requisito_ReqCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vPROPOSTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Proposta_Codigo), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPERGUNTARESPOSTA_PONTUACAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19PerguntaResposta_Pontuacao), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPERGUNTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Pergunta_Codigo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vREQUISITO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Requisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PROPOSTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Insert_Proposta_Codigo), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1685Proposta_Codigo), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_REQUISITO_TIPOREQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Insert_Requisito_TipoReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_REQUISITO_REQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Insert_Requisito_ReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_REQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1999Requisito_ReqCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "REQUISITO_ATIVO", A1935Requisito_Ativo);
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_CADASTRO", context.localUtil.TToC( A1943Requisito_Cadastro, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "REQUISITO_IDENTIFICADOR", A2001Requisito_Identificador);
         GxWebStd.gx_hidden_field( context, "REQUISITO_REFERENCIATECNICA", A1925Requisito_ReferenciaTecnica);
         GxWebStd.gx_hidden_field( context, "REQUISITO_DATAHOMOLOGACAO", context.localUtil.DToC( A1933Requisito_DataHomologacao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "REQUISITO_PRIORIDADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2002Requisito_Prioridade), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROPOSTA_OBJETIVO", A1690Proposta_Objetivo);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV25Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vREQUISITO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Requisito_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Requisito";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1685Proposta_Codigo), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1999Requisito_ReqCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1935Requisito_Ativo);
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A2001Requisito_Identificador, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1933Requisito_DataHomologacao, "99/99/99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1943Requisito_Cadastro, "99/99/99 99:99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2002Requisito_Prioridade), "Z9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("requisito:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("requisito:[SendSecurityCheck value for]"+"Proposta_Codigo:"+context.localUtil.Format( (decimal)(A1685Proposta_Codigo), "ZZZZZZZZ9"));
         GXUtil.WriteLog("requisito:[SendSecurityCheck value for]"+"Requisito_ReqCod:"+context.localUtil.Format( (decimal)(A1999Requisito_ReqCod), "ZZZZZ9"));
         GXUtil.WriteLog("requisito:[SendSecurityCheck value for]"+"Requisito_Ativo:"+StringUtil.BoolToStr( A1935Requisito_Ativo));
         GXUtil.WriteLog("requisito:[SendSecurityCheck value for]"+"Requisito_Identificador:"+StringUtil.RTrim( context.localUtil.Format( A2001Requisito_Identificador, "")));
         GXUtil.WriteLog("requisito:[SendSecurityCheck value for]"+"Requisito_DataHomologacao:"+context.localUtil.Format(A1933Requisito_DataHomologacao, "99/99/99"));
         GXUtil.WriteLog("requisito:[SendSecurityCheck value for]"+"Requisito_Cadastro:"+context.localUtil.Format( A1943Requisito_Cadastro, "99/99/99 99:99"));
         GXUtil.WriteLog("requisito:[SendSecurityCheck value for]"+"Requisito_Prioridade:"+context.localUtil.Format( (decimal)(A2002Requisito_Prioridade), "Z9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("requisito.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Requisito_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Requisito" ;
      }

      public override String GetPgmdesc( )
      {
         return "Requisito" ;
      }

      protected void InitializeNonKey4U215( )
      {
         A1685Proposta_Codigo = 0;
         n1685Proposta_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1685Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1685Proposta_Codigo), 9, 0)));
         A2049Requisito_TipoReqCod = 0;
         n2049Requisito_TipoReqCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2049Requisito_TipoReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)));
         n2049Requisito_TipoReqCod = ((0==A2049Requisito_TipoReqCod) ? true : false);
         A1999Requisito_ReqCod = 0;
         n1999Requisito_ReqCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1999Requisito_ReqCod), 6, 0)));
         A1935Requisito_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1935Requisito_Ativo", A1935Requisito_Ativo);
         A2001Requisito_Identificador = "";
         n2001Requisito_Identificador = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2001Requisito_Identificador", A2001Requisito_Identificador);
         A1927Requisito_Titulo = "";
         n1927Requisito_Titulo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1927Requisito_Titulo", A1927Requisito_Titulo);
         n1927Requisito_Titulo = (String.IsNullOrEmpty(StringUtil.RTrim( A1927Requisito_Titulo)) ? true : false);
         A1923Requisito_Descricao = "";
         n1923Requisito_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1923Requisito_Descricao", A1923Requisito_Descricao);
         n1923Requisito_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1923Requisito_Descricao)) ? true : false);
         A1690Proposta_Objetivo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1690Proposta_Objetivo", A1690Proposta_Objetivo);
         A1925Requisito_ReferenciaTecnica = "";
         n1925Requisito_ReferenciaTecnica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1925Requisito_ReferenciaTecnica", A1925Requisito_ReferenciaTecnica);
         A1926Requisito_Agrupador = "";
         n1926Requisito_Agrupador = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1926Requisito_Agrupador", A1926Requisito_Agrupador);
         n1926Requisito_Agrupador = (String.IsNullOrEmpty(StringUtil.RTrim( A1926Requisito_Agrupador)) ? true : false);
         A1929Requisito_Restricao = "";
         n1929Requisito_Restricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1929Requisito_Restricao", A1929Requisito_Restricao);
         n1929Requisito_Restricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1929Requisito_Restricao)) ? true : false);
         A1931Requisito_Ordem = 0;
         n1931Requisito_Ordem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
         n1931Requisito_Ordem = ((0==A1931Requisito_Ordem) ? true : false);
         A1932Requisito_Pontuacao = 0;
         n1932Requisito_Pontuacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1932Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( A1932Requisito_Pontuacao, 14, 5)));
         n1932Requisito_Pontuacao = ((Convert.ToDecimal(0)==A1932Requisito_Pontuacao) ? true : false);
         A1933Requisito_DataHomologacao = DateTime.MinValue;
         n1933Requisito_DataHomologacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1933Requisito_DataHomologacao", context.localUtil.Format(A1933Requisito_DataHomologacao, "99/99/99"));
         A1934Requisito_Status = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
         A2002Requisito_Prioridade = 0;
         n2002Requisito_Prioridade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2002Requisito_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(A2002Requisito_Prioridade), 2, 0)));
         A1943Requisito_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1943Requisito_Cadastro = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1943Requisito_Cadastro", context.localUtil.TToC( A1943Requisito_Cadastro, 8, 5, 0, 3, "/", ":", " "));
         Z1935Requisito_Ativo = false;
         Z2001Requisito_Identificador = "";
         Z1927Requisito_Titulo = "";
         Z1926Requisito_Agrupador = "";
         Z1931Requisito_Ordem = 0;
         Z1932Requisito_Pontuacao = 0;
         Z1933Requisito_DataHomologacao = DateTime.MinValue;
         Z1934Requisito_Status = 0;
         Z1943Requisito_Cadastro = (DateTime)(DateTime.MinValue);
         Z2002Requisito_Prioridade = 0;
         Z1685Proposta_Codigo = 0;
         Z2049Requisito_TipoReqCod = 0;
         Z1999Requisito_ReqCod = 0;
      }

      protected void InitAll4U215( )
      {
         A1919Requisito_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
         InitializeNonKey4U215( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1935Requisito_Ativo = i1935Requisito_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1935Requisito_Ativo", A1935Requisito_Ativo);
         A1943Requisito_Cadastro = i1943Requisito_Cadastro;
         n1943Requisito_Cadastro = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1943Requisito_Cadastro", context.localUtil.TToC( A1943Requisito_Cadastro, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299321612");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("requisito.js", "?20205299321612");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockrequisito_ordem_Internalname = "TEXTBLOCKREQUISITO_ORDEM";
         edtRequisito_Ordem_Internalname = "REQUISITO_ORDEM";
         lblTextblockrequisito_agrupador_Internalname = "TEXTBLOCKREQUISITO_AGRUPADOR";
         edtRequisito_Agrupador_Internalname = "REQUISITO_AGRUPADOR";
         lblTextblockrequisito_tiporeqcod_Internalname = "TEXTBLOCKREQUISITO_TIPOREQCOD";
         dynRequisito_TipoReqCod_Internalname = "REQUISITO_TIPOREQCOD";
         lblTextblockrequisito_titulo_Internalname = "TEXTBLOCKREQUISITO_TITULO";
         edtRequisito_Titulo_Internalname = "REQUISITO_TITULO";
         lblTextblockrequisito_descricao_Internalname = "TEXTBLOCKREQUISITO_DESCRICAO";
         edtRequisito_Descricao_Internalname = "REQUISITO_DESCRICAO";
         lblTextblockrequisito_restricao_Internalname = "TEXTBLOCKREQUISITO_RESTRICAO";
         edtRequisito_Restricao_Internalname = "REQUISITO_RESTRICAO";
         lblTextblockrequisito_status_Internalname = "TEXTBLOCKREQUISITO_STATUS";
         cmbRequisito_Status_Internalname = "REQUISITO_STATUS";
         lblTextblockrequisito_pontuacao_Internalname = "TEXTBLOCKREQUISITO_PONTUACAO";
         edtRequisito_Pontuacao_Internalname = "REQUISITO_PONTUACAO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Requisito";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Requisito";
         edtRequisito_Pontuacao_Jsonclick = "";
         edtRequisito_Pontuacao_Enabled = 1;
         cmbRequisito_Status_Jsonclick = "";
         cmbRequisito_Status.Enabled = 1;
         edtRequisito_Restricao_Enabled = 1;
         edtRequisito_Descricao_Enabled = 1;
         edtRequisito_Titulo_Enabled = 1;
         dynRequisito_TipoReqCod_Jsonclick = "";
         dynRequisito_TipoReqCod.Enabled = 1;
         edtRequisito_Agrupador_Jsonclick = "";
         edtRequisito_Agrupador_Enabled = 1;
         edtRequisito_Ordem_Jsonclick = "";
         edtRequisito_Ordem_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAREQUISITO_TIPOREQCOD4U215( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAREQUISITO_TIPOREQCOD_data4U215( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAREQUISITO_TIPOREQCOD_html4U215( )
      {
         int gxdynajaxvalue ;
         GXDLAREQUISITO_TIPOREQCOD_data4U215( ) ;
         gxdynajaxindex = 1;
         dynRequisito_TipoReqCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynRequisito_TipoReqCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAREQUISITO_TIPOREQCOD_data4U215( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T004U22 */
         pr_default.execute(20);
         while ( (pr_default.getStatus(20) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004U22_A2049Requisito_TipoReqCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T004U22_A2042TipoRequisito_Identificador[0]);
            pr_default.readNext(20);
         }
         pr_default.close(20);
      }

      public void Valid_Requisito_tiporeqcod( GXCombobox dynGX_Parm1 )
      {
         dynRequisito_TipoReqCod = dynGX_Parm1;
         A2049Requisito_TipoReqCod = (int)(NumberUtil.Val( dynRequisito_TipoReqCod.CurrentValue, "."));
         n2049Requisito_TipoReqCod = false;
         /* Using cursor T004U23 */
         pr_default.execute(21, new Object[] {n2049Requisito_TipoReqCod, A2049Requisito_TipoReqCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            if ( ! ( (0==A2049Requisito_TipoReqCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Requisito_Tipo Requisito'.", "ForeignKeyNotFound", 1, "REQUISITO_TIPOREQCOD");
               AnyError = 1;
               GX_FocusControl = dynRequisito_TipoReqCod_Internalname;
            }
         }
         pr_default.close(21);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E124U2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOPONTUACAO'","{handler:'E134U2',iparms:[{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV17Proposta_Codigo',fld:'vPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'AV19PerguntaResposta_Pontuacao',fld:'vPERGUNTARESPOSTA_PONTUACAO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV19PerguntaResposta_Pontuacao',fld:'vPERGUNTARESPOSTA_PONTUACAO',pic:'ZZZ9',nv:0},{av:'AV17Proposta_Codigo',fld:'vPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOBTNCANCELAR'","{handler:'E144U2',iparms:[{av:'AV17Proposta_Codigo',fld:'vPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0}],oparms:[{av:'AV17Proposta_Codigo',fld:'vPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0}]}");
         setEventMetadata("'CALCULAPONTUCAO'","{handler:'E154U2',iparms:[{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV18Pergunta_Codigo',fld:'vPERGUNTA_CODIGO',pic:'ZZZ9',nv:0},{av:'A1685Proposta_Codigo',fld:'PROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(21);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z2001Requisito_Identificador = "";
         Z1927Requisito_Titulo = "";
         Z1926Requisito_Agrupador = "";
         Z1933Requisito_DataHomologacao = DateTime.MinValue;
         Z1943Requisito_Cadastro = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockrequisito_ordem_Jsonclick = "";
         lblTextblockrequisito_agrupador_Jsonclick = "";
         A1926Requisito_Agrupador = "";
         lblTextblockrequisito_tiporeqcod_Jsonclick = "";
         lblTextblockrequisito_titulo_Jsonclick = "";
         A1927Requisito_Titulo = "";
         lblTextblockrequisito_descricao_Jsonclick = "";
         A1923Requisito_Descricao = "";
         lblTextblockrequisito_restricao_Jsonclick = "";
         A1929Requisito_Restricao = "";
         lblTextblockrequisito_status_Jsonclick = "";
         lblTextblockrequisito_pontuacao_Jsonclick = "";
         A2001Requisito_Identificador = "";
         A1933Requisito_DataHomologacao = DateTime.MinValue;
         A1943Requisito_Cadastro = (DateTime)(DateTime.MinValue);
         A1925Requisito_ReferenciaTecnica = "";
         A1690Proposta_Objetivo = "";
         AV25Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode215 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV20Window = new GXWindow();
         Z1923Requisito_Descricao = "";
         Z1925Requisito_ReferenciaTecnica = "";
         Z1929Requisito_Restricao = "";
         Z1690Proposta_Objetivo = "";
         T004U4_A1690Proposta_Objetivo = new String[] {""} ;
         T004U7_A1919Requisito_Codigo = new int[1] ;
         T004U7_A1935Requisito_Ativo = new bool[] {false} ;
         T004U7_A2001Requisito_Identificador = new String[] {""} ;
         T004U7_n2001Requisito_Identificador = new bool[] {false} ;
         T004U7_A1927Requisito_Titulo = new String[] {""} ;
         T004U7_n1927Requisito_Titulo = new bool[] {false} ;
         T004U7_A1923Requisito_Descricao = new String[] {""} ;
         T004U7_n1923Requisito_Descricao = new bool[] {false} ;
         T004U7_A1690Proposta_Objetivo = new String[] {""} ;
         T004U7_A1925Requisito_ReferenciaTecnica = new String[] {""} ;
         T004U7_n1925Requisito_ReferenciaTecnica = new bool[] {false} ;
         T004U7_A1926Requisito_Agrupador = new String[] {""} ;
         T004U7_n1926Requisito_Agrupador = new bool[] {false} ;
         T004U7_A1929Requisito_Restricao = new String[] {""} ;
         T004U7_n1929Requisito_Restricao = new bool[] {false} ;
         T004U7_A1931Requisito_Ordem = new short[1] ;
         T004U7_n1931Requisito_Ordem = new bool[] {false} ;
         T004U7_A1932Requisito_Pontuacao = new decimal[1] ;
         T004U7_n1932Requisito_Pontuacao = new bool[] {false} ;
         T004U7_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T004U7_n1933Requisito_DataHomologacao = new bool[] {false} ;
         T004U7_A1934Requisito_Status = new short[1] ;
         T004U7_A1943Requisito_Cadastro = new DateTime[] {DateTime.MinValue} ;
         T004U7_n1943Requisito_Cadastro = new bool[] {false} ;
         T004U7_A2002Requisito_Prioridade = new short[1] ;
         T004U7_n2002Requisito_Prioridade = new bool[] {false} ;
         T004U7_A1685Proposta_Codigo = new int[1] ;
         T004U7_n1685Proposta_Codigo = new bool[] {false} ;
         T004U7_A2049Requisito_TipoReqCod = new int[1] ;
         T004U7_n2049Requisito_TipoReqCod = new bool[] {false} ;
         T004U7_A1999Requisito_ReqCod = new int[1] ;
         T004U7_n1999Requisito_ReqCod = new bool[] {false} ;
         T004U5_A2049Requisito_TipoReqCod = new int[1] ;
         T004U5_n2049Requisito_TipoReqCod = new bool[] {false} ;
         T004U6_A1999Requisito_ReqCod = new int[1] ;
         T004U6_n1999Requisito_ReqCod = new bool[] {false} ;
         T004U8_A2049Requisito_TipoReqCod = new int[1] ;
         T004U8_n2049Requisito_TipoReqCod = new bool[] {false} ;
         T004U9_A1690Proposta_Objetivo = new String[] {""} ;
         T004U10_A1999Requisito_ReqCod = new int[1] ;
         T004U10_n1999Requisito_ReqCod = new bool[] {false} ;
         T004U11_A1919Requisito_Codigo = new int[1] ;
         T004U3_A1919Requisito_Codigo = new int[1] ;
         T004U3_A1935Requisito_Ativo = new bool[] {false} ;
         T004U3_A2001Requisito_Identificador = new String[] {""} ;
         T004U3_n2001Requisito_Identificador = new bool[] {false} ;
         T004U3_A1927Requisito_Titulo = new String[] {""} ;
         T004U3_n1927Requisito_Titulo = new bool[] {false} ;
         T004U3_A1923Requisito_Descricao = new String[] {""} ;
         T004U3_n1923Requisito_Descricao = new bool[] {false} ;
         T004U3_A1925Requisito_ReferenciaTecnica = new String[] {""} ;
         T004U3_n1925Requisito_ReferenciaTecnica = new bool[] {false} ;
         T004U3_A1926Requisito_Agrupador = new String[] {""} ;
         T004U3_n1926Requisito_Agrupador = new bool[] {false} ;
         T004U3_A1929Requisito_Restricao = new String[] {""} ;
         T004U3_n1929Requisito_Restricao = new bool[] {false} ;
         T004U3_A1931Requisito_Ordem = new short[1] ;
         T004U3_n1931Requisito_Ordem = new bool[] {false} ;
         T004U3_A1932Requisito_Pontuacao = new decimal[1] ;
         T004U3_n1932Requisito_Pontuacao = new bool[] {false} ;
         T004U3_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T004U3_n1933Requisito_DataHomologacao = new bool[] {false} ;
         T004U3_A1934Requisito_Status = new short[1] ;
         T004U3_A1943Requisito_Cadastro = new DateTime[] {DateTime.MinValue} ;
         T004U3_n1943Requisito_Cadastro = new bool[] {false} ;
         T004U3_A2002Requisito_Prioridade = new short[1] ;
         T004U3_n2002Requisito_Prioridade = new bool[] {false} ;
         T004U3_A1685Proposta_Codigo = new int[1] ;
         T004U3_n1685Proposta_Codigo = new bool[] {false} ;
         T004U3_A2049Requisito_TipoReqCod = new int[1] ;
         T004U3_n2049Requisito_TipoReqCod = new bool[] {false} ;
         T004U3_A1999Requisito_ReqCod = new int[1] ;
         T004U3_n1999Requisito_ReqCod = new bool[] {false} ;
         T004U12_A1919Requisito_Codigo = new int[1] ;
         T004U13_A1919Requisito_Codigo = new int[1] ;
         T004U2_A1919Requisito_Codigo = new int[1] ;
         T004U2_A1935Requisito_Ativo = new bool[] {false} ;
         T004U2_A2001Requisito_Identificador = new String[] {""} ;
         T004U2_n2001Requisito_Identificador = new bool[] {false} ;
         T004U2_A1927Requisito_Titulo = new String[] {""} ;
         T004U2_n1927Requisito_Titulo = new bool[] {false} ;
         T004U2_A1923Requisito_Descricao = new String[] {""} ;
         T004U2_n1923Requisito_Descricao = new bool[] {false} ;
         T004U2_A1925Requisito_ReferenciaTecnica = new String[] {""} ;
         T004U2_n1925Requisito_ReferenciaTecnica = new bool[] {false} ;
         T004U2_A1926Requisito_Agrupador = new String[] {""} ;
         T004U2_n1926Requisito_Agrupador = new bool[] {false} ;
         T004U2_A1929Requisito_Restricao = new String[] {""} ;
         T004U2_n1929Requisito_Restricao = new bool[] {false} ;
         T004U2_A1931Requisito_Ordem = new short[1] ;
         T004U2_n1931Requisito_Ordem = new bool[] {false} ;
         T004U2_A1932Requisito_Pontuacao = new decimal[1] ;
         T004U2_n1932Requisito_Pontuacao = new bool[] {false} ;
         T004U2_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T004U2_n1933Requisito_DataHomologacao = new bool[] {false} ;
         T004U2_A1934Requisito_Status = new short[1] ;
         T004U2_A1943Requisito_Cadastro = new DateTime[] {DateTime.MinValue} ;
         T004U2_n1943Requisito_Cadastro = new bool[] {false} ;
         T004U2_A2002Requisito_Prioridade = new short[1] ;
         T004U2_n2002Requisito_Prioridade = new bool[] {false} ;
         T004U2_A1685Proposta_Codigo = new int[1] ;
         T004U2_n1685Proposta_Codigo = new bool[] {false} ;
         T004U2_A2049Requisito_TipoReqCod = new int[1] ;
         T004U2_n2049Requisito_TipoReqCod = new bool[] {false} ;
         T004U2_A1999Requisito_ReqCod = new int[1] ;
         T004U2_n1999Requisito_ReqCod = new bool[] {false} ;
         T004U14_A1919Requisito_Codigo = new int[1] ;
         T004U17_A1690Proposta_Objetivo = new String[] {""} ;
         T004U18_A1999Requisito_ReqCod = new int[1] ;
         T004U18_n1999Requisito_ReqCod = new bool[] {false} ;
         T004U19_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         T004U20_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004U20_A1919Requisito_Codigo = new int[1] ;
         T004U21_A1919Requisito_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i1943Requisito_Cadastro = (DateTime)(DateTime.MinValue);
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T004U22_A2049Requisito_TipoReqCod = new int[1] ;
         T004U22_n2049Requisito_TipoReqCod = new bool[] {false} ;
         T004U22_A2042TipoRequisito_Identificador = new String[] {""} ;
         T004U22_n2042TipoRequisito_Identificador = new bool[] {false} ;
         T004U23_A2049Requisito_TipoReqCod = new int[1] ;
         T004U23_n2049Requisito_TipoReqCod = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.requisito__default(),
            new Object[][] {
                new Object[] {
               T004U2_A1919Requisito_Codigo, T004U2_A1935Requisito_Ativo, T004U2_A2001Requisito_Identificador, T004U2_n2001Requisito_Identificador, T004U2_A1927Requisito_Titulo, T004U2_n1927Requisito_Titulo, T004U2_A1923Requisito_Descricao, T004U2_n1923Requisito_Descricao, T004U2_A1925Requisito_ReferenciaTecnica, T004U2_n1925Requisito_ReferenciaTecnica,
               T004U2_A1926Requisito_Agrupador, T004U2_n1926Requisito_Agrupador, T004U2_A1929Requisito_Restricao, T004U2_n1929Requisito_Restricao, T004U2_A1931Requisito_Ordem, T004U2_n1931Requisito_Ordem, T004U2_A1932Requisito_Pontuacao, T004U2_n1932Requisito_Pontuacao, T004U2_A1933Requisito_DataHomologacao, T004U2_n1933Requisito_DataHomologacao,
               T004U2_A1934Requisito_Status, T004U2_A1943Requisito_Cadastro, T004U2_n1943Requisito_Cadastro, T004U2_A2002Requisito_Prioridade, T004U2_n2002Requisito_Prioridade, T004U2_A1685Proposta_Codigo, T004U2_n1685Proposta_Codigo, T004U2_A2049Requisito_TipoReqCod, T004U2_n2049Requisito_TipoReqCod, T004U2_A1999Requisito_ReqCod,
               T004U2_n1999Requisito_ReqCod
               }
               , new Object[] {
               T004U3_A1919Requisito_Codigo, T004U3_A1935Requisito_Ativo, T004U3_A2001Requisito_Identificador, T004U3_n2001Requisito_Identificador, T004U3_A1927Requisito_Titulo, T004U3_n1927Requisito_Titulo, T004U3_A1923Requisito_Descricao, T004U3_n1923Requisito_Descricao, T004U3_A1925Requisito_ReferenciaTecnica, T004U3_n1925Requisito_ReferenciaTecnica,
               T004U3_A1926Requisito_Agrupador, T004U3_n1926Requisito_Agrupador, T004U3_A1929Requisito_Restricao, T004U3_n1929Requisito_Restricao, T004U3_A1931Requisito_Ordem, T004U3_n1931Requisito_Ordem, T004U3_A1932Requisito_Pontuacao, T004U3_n1932Requisito_Pontuacao, T004U3_A1933Requisito_DataHomologacao, T004U3_n1933Requisito_DataHomologacao,
               T004U3_A1934Requisito_Status, T004U3_A1943Requisito_Cadastro, T004U3_n1943Requisito_Cadastro, T004U3_A2002Requisito_Prioridade, T004U3_n2002Requisito_Prioridade, T004U3_A1685Proposta_Codigo, T004U3_n1685Proposta_Codigo, T004U3_A2049Requisito_TipoReqCod, T004U3_n2049Requisito_TipoReqCod, T004U3_A1999Requisito_ReqCod,
               T004U3_n1999Requisito_ReqCod
               }
               , new Object[] {
               T004U4_A1690Proposta_Objetivo
               }
               , new Object[] {
               T004U5_A2049Requisito_TipoReqCod
               }
               , new Object[] {
               T004U6_A1999Requisito_ReqCod
               }
               , new Object[] {
               T004U7_A1919Requisito_Codigo, T004U7_A1935Requisito_Ativo, T004U7_A2001Requisito_Identificador, T004U7_n2001Requisito_Identificador, T004U7_A1927Requisito_Titulo, T004U7_n1927Requisito_Titulo, T004U7_A1923Requisito_Descricao, T004U7_n1923Requisito_Descricao, T004U7_A1690Proposta_Objetivo, T004U7_A1925Requisito_ReferenciaTecnica,
               T004U7_n1925Requisito_ReferenciaTecnica, T004U7_A1926Requisito_Agrupador, T004U7_n1926Requisito_Agrupador, T004U7_A1929Requisito_Restricao, T004U7_n1929Requisito_Restricao, T004U7_A1931Requisito_Ordem, T004U7_n1931Requisito_Ordem, T004U7_A1932Requisito_Pontuacao, T004U7_n1932Requisito_Pontuacao, T004U7_A1933Requisito_DataHomologacao,
               T004U7_n1933Requisito_DataHomologacao, T004U7_A1934Requisito_Status, T004U7_A1943Requisito_Cadastro, T004U7_n1943Requisito_Cadastro, T004U7_A2002Requisito_Prioridade, T004U7_n2002Requisito_Prioridade, T004U7_A1685Proposta_Codigo, T004U7_n1685Proposta_Codigo, T004U7_A2049Requisito_TipoReqCod, T004U7_n2049Requisito_TipoReqCod,
               T004U7_A1999Requisito_ReqCod, T004U7_n1999Requisito_ReqCod
               }
               , new Object[] {
               T004U8_A2049Requisito_TipoReqCod
               }
               , new Object[] {
               T004U9_A1690Proposta_Objetivo
               }
               , new Object[] {
               T004U10_A1999Requisito_ReqCod
               }
               , new Object[] {
               T004U11_A1919Requisito_Codigo
               }
               , new Object[] {
               T004U12_A1919Requisito_Codigo
               }
               , new Object[] {
               T004U13_A1919Requisito_Codigo
               }
               , new Object[] {
               T004U14_A1919Requisito_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004U17_A1690Proposta_Objetivo
               }
               , new Object[] {
               T004U18_A1999Requisito_ReqCod
               }
               , new Object[] {
               T004U19_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               T004U20_A1895SolicServicoReqNeg_Codigo, T004U20_A1919Requisito_Codigo
               }
               , new Object[] {
               T004U21_A1919Requisito_Codigo
               }
               , new Object[] {
               T004U22_A2049Requisito_TipoReqCod, T004U22_A2042TipoRequisito_Identificador, T004U22_n2042TipoRequisito_Identificador
               }
               , new Object[] {
               T004U23_A2049Requisito_TipoReqCod
               }
            }
         );
         Z1943Requisito_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1943Requisito_Cadastro = false;
         A1943Requisito_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1943Requisito_Cadastro = false;
         i1943Requisito_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1943Requisito_Cadastro = false;
         AV25Pgmname = "Requisito";
      }

      private short Z1931Requisito_Ordem ;
      private short Z1934Requisito_Status ;
      private short Z2002Requisito_Prioridade ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A1934Requisito_Status ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1931Requisito_Ordem ;
      private short A2002Requisito_Prioridade ;
      private short Gx_BScreen ;
      private short RcdFound215 ;
      private short AV19PerguntaResposta_Pontuacao ;
      private short AV18Pergunta_Codigo ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Requisito_Codigo ;
      private int Z1919Requisito_Codigo ;
      private int Z1685Proposta_Codigo ;
      private int Z2049Requisito_TipoReqCod ;
      private int Z1999Requisito_ReqCod ;
      private int N1685Proposta_Codigo ;
      private int N2049Requisito_TipoReqCod ;
      private int N1999Requisito_ReqCod ;
      private int A2049Requisito_TipoReqCod ;
      private int A1685Proposta_Codigo ;
      private int A1999Requisito_ReqCod ;
      private int AV7Requisito_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtRequisito_Ordem_Enabled ;
      private int edtRequisito_Agrupador_Enabled ;
      private int edtRequisito_Titulo_Enabled ;
      private int edtRequisito_Descricao_Enabled ;
      private int edtRequisito_Restricao_Enabled ;
      private int edtRequisito_Pontuacao_Enabled ;
      private int A1919Requisito_Codigo ;
      private int AV15Insert_Proposta_Codigo ;
      private int AV24Insert_Requisito_TipoReqCod ;
      private int AV22Insert_Requisito_ReqCod ;
      private int AV26GXV1 ;
      private int AV17Proposta_Codigo ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z1932Requisito_Pontuacao ;
      private decimal A1932Requisito_Pontuacao ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtRequisito_Ordem_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockrequisito_ordem_Internalname ;
      private String lblTextblockrequisito_ordem_Jsonclick ;
      private String edtRequisito_Ordem_Jsonclick ;
      private String lblTextblockrequisito_agrupador_Internalname ;
      private String lblTextblockrequisito_agrupador_Jsonclick ;
      private String edtRequisito_Agrupador_Internalname ;
      private String edtRequisito_Agrupador_Jsonclick ;
      private String lblTextblockrequisito_tiporeqcod_Internalname ;
      private String lblTextblockrequisito_tiporeqcod_Jsonclick ;
      private String dynRequisito_TipoReqCod_Internalname ;
      private String dynRequisito_TipoReqCod_Jsonclick ;
      private String lblTextblockrequisito_titulo_Internalname ;
      private String lblTextblockrequisito_titulo_Jsonclick ;
      private String edtRequisito_Titulo_Internalname ;
      private String lblTextblockrequisito_descricao_Internalname ;
      private String lblTextblockrequisito_descricao_Jsonclick ;
      private String edtRequisito_Descricao_Internalname ;
      private String lblTextblockrequisito_restricao_Internalname ;
      private String lblTextblockrequisito_restricao_Jsonclick ;
      private String edtRequisito_Restricao_Internalname ;
      private String lblTextblockrequisito_status_Internalname ;
      private String lblTextblockrequisito_status_Jsonclick ;
      private String cmbRequisito_Status_Internalname ;
      private String cmbRequisito_Status_Jsonclick ;
      private String lblTextblockrequisito_pontuacao_Internalname ;
      private String lblTextblockrequisito_pontuacao_Jsonclick ;
      private String edtRequisito_Pontuacao_Internalname ;
      private String edtRequisito_Pontuacao_Jsonclick ;
      private String AV25Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode215 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z1943Requisito_Cadastro ;
      private DateTime A1943Requisito_Cadastro ;
      private DateTime i1943Requisito_Cadastro ;
      private DateTime Z1933Requisito_DataHomologacao ;
      private DateTime A1933Requisito_DataHomologacao ;
      private bool Z1935Requisito_Ativo ;
      private bool entryPointCalled ;
      private bool n2049Requisito_TipoReqCod ;
      private bool n1685Proposta_Codigo ;
      private bool n1999Requisito_ReqCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1931Requisito_Ordem ;
      private bool n1926Requisito_Agrupador ;
      private bool n1927Requisito_Titulo ;
      private bool n1923Requisito_Descricao ;
      private bool n1929Requisito_Restricao ;
      private bool n1932Requisito_Pontuacao ;
      private bool n2001Requisito_Identificador ;
      private bool n1933Requisito_DataHomologacao ;
      private bool n1943Requisito_Cadastro ;
      private bool n2002Requisito_Prioridade ;
      private bool A1935Requisito_Ativo ;
      private bool n1925Requisito_ReferenciaTecnica ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i1935Requisito_Ativo ;
      private String A1923Requisito_Descricao ;
      private String A1929Requisito_Restricao ;
      private String A1925Requisito_ReferenciaTecnica ;
      private String A1690Proposta_Objetivo ;
      private String Z1923Requisito_Descricao ;
      private String Z1925Requisito_ReferenciaTecnica ;
      private String Z1929Requisito_Restricao ;
      private String Z1690Proposta_Objetivo ;
      private String Z2001Requisito_Identificador ;
      private String Z1927Requisito_Titulo ;
      private String Z1926Requisito_Agrupador ;
      private String A1926Requisito_Agrupador ;
      private String A1927Requisito_Titulo ;
      private String A2001Requisito_Identificador ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynRequisito_TipoReqCod ;
      private GXCombobox cmbRequisito_Status ;
      private IDataStoreProvider pr_default ;
      private String[] T004U4_A1690Proposta_Objetivo ;
      private int[] T004U7_A1919Requisito_Codigo ;
      private bool[] T004U7_A1935Requisito_Ativo ;
      private String[] T004U7_A2001Requisito_Identificador ;
      private bool[] T004U7_n2001Requisito_Identificador ;
      private String[] T004U7_A1927Requisito_Titulo ;
      private bool[] T004U7_n1927Requisito_Titulo ;
      private String[] T004U7_A1923Requisito_Descricao ;
      private bool[] T004U7_n1923Requisito_Descricao ;
      private String[] T004U7_A1690Proposta_Objetivo ;
      private String[] T004U7_A1925Requisito_ReferenciaTecnica ;
      private bool[] T004U7_n1925Requisito_ReferenciaTecnica ;
      private String[] T004U7_A1926Requisito_Agrupador ;
      private bool[] T004U7_n1926Requisito_Agrupador ;
      private String[] T004U7_A1929Requisito_Restricao ;
      private bool[] T004U7_n1929Requisito_Restricao ;
      private short[] T004U7_A1931Requisito_Ordem ;
      private bool[] T004U7_n1931Requisito_Ordem ;
      private decimal[] T004U7_A1932Requisito_Pontuacao ;
      private bool[] T004U7_n1932Requisito_Pontuacao ;
      private DateTime[] T004U7_A1933Requisito_DataHomologacao ;
      private bool[] T004U7_n1933Requisito_DataHomologacao ;
      private short[] T004U7_A1934Requisito_Status ;
      private DateTime[] T004U7_A1943Requisito_Cadastro ;
      private bool[] T004U7_n1943Requisito_Cadastro ;
      private short[] T004U7_A2002Requisito_Prioridade ;
      private bool[] T004U7_n2002Requisito_Prioridade ;
      private int[] T004U7_A1685Proposta_Codigo ;
      private bool[] T004U7_n1685Proposta_Codigo ;
      private int[] T004U7_A2049Requisito_TipoReqCod ;
      private bool[] T004U7_n2049Requisito_TipoReqCod ;
      private int[] T004U7_A1999Requisito_ReqCod ;
      private bool[] T004U7_n1999Requisito_ReqCod ;
      private int[] T004U5_A2049Requisito_TipoReqCod ;
      private bool[] T004U5_n2049Requisito_TipoReqCod ;
      private int[] T004U6_A1999Requisito_ReqCod ;
      private bool[] T004U6_n1999Requisito_ReqCod ;
      private int[] T004U8_A2049Requisito_TipoReqCod ;
      private bool[] T004U8_n2049Requisito_TipoReqCod ;
      private String[] T004U9_A1690Proposta_Objetivo ;
      private int[] T004U10_A1999Requisito_ReqCod ;
      private bool[] T004U10_n1999Requisito_ReqCod ;
      private int[] T004U11_A1919Requisito_Codigo ;
      private int[] T004U3_A1919Requisito_Codigo ;
      private bool[] T004U3_A1935Requisito_Ativo ;
      private String[] T004U3_A2001Requisito_Identificador ;
      private bool[] T004U3_n2001Requisito_Identificador ;
      private String[] T004U3_A1927Requisito_Titulo ;
      private bool[] T004U3_n1927Requisito_Titulo ;
      private String[] T004U3_A1923Requisito_Descricao ;
      private bool[] T004U3_n1923Requisito_Descricao ;
      private String[] T004U3_A1925Requisito_ReferenciaTecnica ;
      private bool[] T004U3_n1925Requisito_ReferenciaTecnica ;
      private String[] T004U3_A1926Requisito_Agrupador ;
      private bool[] T004U3_n1926Requisito_Agrupador ;
      private String[] T004U3_A1929Requisito_Restricao ;
      private bool[] T004U3_n1929Requisito_Restricao ;
      private short[] T004U3_A1931Requisito_Ordem ;
      private bool[] T004U3_n1931Requisito_Ordem ;
      private decimal[] T004U3_A1932Requisito_Pontuacao ;
      private bool[] T004U3_n1932Requisito_Pontuacao ;
      private DateTime[] T004U3_A1933Requisito_DataHomologacao ;
      private bool[] T004U3_n1933Requisito_DataHomologacao ;
      private short[] T004U3_A1934Requisito_Status ;
      private DateTime[] T004U3_A1943Requisito_Cadastro ;
      private bool[] T004U3_n1943Requisito_Cadastro ;
      private short[] T004U3_A2002Requisito_Prioridade ;
      private bool[] T004U3_n2002Requisito_Prioridade ;
      private int[] T004U3_A1685Proposta_Codigo ;
      private bool[] T004U3_n1685Proposta_Codigo ;
      private int[] T004U3_A2049Requisito_TipoReqCod ;
      private bool[] T004U3_n2049Requisito_TipoReqCod ;
      private int[] T004U3_A1999Requisito_ReqCod ;
      private bool[] T004U3_n1999Requisito_ReqCod ;
      private int[] T004U12_A1919Requisito_Codigo ;
      private int[] T004U13_A1919Requisito_Codigo ;
      private int[] T004U2_A1919Requisito_Codigo ;
      private bool[] T004U2_A1935Requisito_Ativo ;
      private String[] T004U2_A2001Requisito_Identificador ;
      private bool[] T004U2_n2001Requisito_Identificador ;
      private String[] T004U2_A1927Requisito_Titulo ;
      private bool[] T004U2_n1927Requisito_Titulo ;
      private String[] T004U2_A1923Requisito_Descricao ;
      private bool[] T004U2_n1923Requisito_Descricao ;
      private String[] T004U2_A1925Requisito_ReferenciaTecnica ;
      private bool[] T004U2_n1925Requisito_ReferenciaTecnica ;
      private String[] T004U2_A1926Requisito_Agrupador ;
      private bool[] T004U2_n1926Requisito_Agrupador ;
      private String[] T004U2_A1929Requisito_Restricao ;
      private bool[] T004U2_n1929Requisito_Restricao ;
      private short[] T004U2_A1931Requisito_Ordem ;
      private bool[] T004U2_n1931Requisito_Ordem ;
      private decimal[] T004U2_A1932Requisito_Pontuacao ;
      private bool[] T004U2_n1932Requisito_Pontuacao ;
      private DateTime[] T004U2_A1933Requisito_DataHomologacao ;
      private bool[] T004U2_n1933Requisito_DataHomologacao ;
      private short[] T004U2_A1934Requisito_Status ;
      private DateTime[] T004U2_A1943Requisito_Cadastro ;
      private bool[] T004U2_n1943Requisito_Cadastro ;
      private short[] T004U2_A2002Requisito_Prioridade ;
      private bool[] T004U2_n2002Requisito_Prioridade ;
      private int[] T004U2_A1685Proposta_Codigo ;
      private bool[] T004U2_n1685Proposta_Codigo ;
      private int[] T004U2_A2049Requisito_TipoReqCod ;
      private bool[] T004U2_n2049Requisito_TipoReqCod ;
      private int[] T004U2_A1999Requisito_ReqCod ;
      private bool[] T004U2_n1999Requisito_ReqCod ;
      private int[] T004U14_A1919Requisito_Codigo ;
      private String[] T004U17_A1690Proposta_Objetivo ;
      private int[] T004U18_A1999Requisito_ReqCod ;
      private bool[] T004U18_n1999Requisito_ReqCod ;
      private int[] T004U19_A2005ContagemResultadoRequisito_Codigo ;
      private long[] T004U20_A1895SolicServicoReqNeg_Codigo ;
      private int[] T004U20_A1919Requisito_Codigo ;
      private int[] T004U21_A1919Requisito_Codigo ;
      private int[] T004U22_A2049Requisito_TipoReqCod ;
      private bool[] T004U22_n2049Requisito_TipoReqCod ;
      private String[] T004U22_A2042TipoRequisito_Identificador ;
      private bool[] T004U22_n2042TipoRequisito_Identificador ;
      private int[] T004U23_A2049Requisito_TipoReqCod ;
      private bool[] T004U23_n2049Requisito_TipoReqCod ;
      private GXWebForm Form ;
      private GXWindow AV20Window ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class requisito__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004U7 ;
          prmT004U7 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U5 ;
          prmT004U5 = new Object[] {
          new Object[] {"@Requisito_TipoReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U4 ;
          prmT004U4 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmT004U6 ;
          prmT004U6 = new Object[] {
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U8 ;
          prmT004U8 = new Object[] {
          new Object[] {"@Requisito_TipoReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U9 ;
          prmT004U9 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmT004U10 ;
          prmT004U10 = new Object[] {
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U11 ;
          prmT004U11 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U3 ;
          prmT004U3 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U12 ;
          prmT004U12 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U13 ;
          prmT004U13 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U2 ;
          prmT004U2 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U14 ;
          prmT004U14 = new Object[] {
          new Object[] {"@Requisito_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Requisito_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Requisito_Titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@Requisito_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Requisito_ReferenciaTecnica",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Requisito_Agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@Requisito_Restricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Requisito_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@Requisito_Pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Requisito_DataHomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Requisito_Status",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Requisito_Cadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Requisito_Prioridade",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0} ,
          new Object[] {"@Requisito_TipoReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U15 ;
          prmT004U15 = new Object[] {
          new Object[] {"@Requisito_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Requisito_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Requisito_Titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@Requisito_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Requisito_ReferenciaTecnica",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Requisito_Agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@Requisito_Restricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Requisito_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@Requisito_Pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Requisito_DataHomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Requisito_Status",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Requisito_Cadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Requisito_Prioridade",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0} ,
          new Object[] {"@Requisito_TipoReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U16 ;
          prmT004U16 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U17 ;
          prmT004U17 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmT004U18 ;
          prmT004U18 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U19 ;
          prmT004U19 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U20 ;
          prmT004U20 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004U21 ;
          prmT004U21 = new Object[] {
          } ;
          Object[] prmT004U22 ;
          prmT004U22 = new Object[] {
          } ;
          Object[] prmT004U23 ;
          prmT004U23 = new Object[] {
          new Object[] {"@Requisito_TipoReqCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004U2", "SELECT [Requisito_Codigo], [Requisito_Ativo], [Requisito_Identificador], [Requisito_Titulo], [Requisito_Descricao], [Requisito_ReferenciaTecnica], [Requisito_Agrupador], [Requisito_Restricao], [Requisito_Ordem], [Requisito_Pontuacao], [Requisito_DataHomologacao], [Requisito_Status], [Requisito_Cadastro], [Requisito_Prioridade], [Proposta_Codigo], [Requisito_TipoReqCod] AS Requisito_TipoReqCod, [Requisito_ReqCod] AS Requisito_ReqCod FROM [Requisito] WITH (UPDLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U2,1,0,true,false )
             ,new CursorDef("T004U3", "SELECT [Requisito_Codigo], [Requisito_Ativo], [Requisito_Identificador], [Requisito_Titulo], [Requisito_Descricao], [Requisito_ReferenciaTecnica], [Requisito_Agrupador], [Requisito_Restricao], [Requisito_Ordem], [Requisito_Pontuacao], [Requisito_DataHomologacao], [Requisito_Status], [Requisito_Cadastro], [Requisito_Prioridade], [Proposta_Codigo], [Requisito_TipoReqCod] AS Requisito_TipoReqCod, [Requisito_ReqCod] AS Requisito_ReqCod FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U3,1,0,true,false )
             ,new CursorDef("T004U4", "SELECT [Proposta_Objetivo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U4,1,0,true,false )
             ,new CursorDef("T004U5", "SELECT [TipoRequisito_Codigo] AS Requisito_TipoReqCod FROM [TipoRequisito] WITH (NOLOCK) WHERE [TipoRequisito_Codigo] = @Requisito_TipoReqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U5,1,0,true,false )
             ,new CursorDef("T004U6", "SELECT [Requisito_Codigo] AS Requisito_ReqCod FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_ReqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U6,1,0,true,false )
             ,new CursorDef("T004U7", "SELECT TM1.[Requisito_Codigo], TM1.[Requisito_Ativo], TM1.[Requisito_Identificador], TM1.[Requisito_Titulo], TM1.[Requisito_Descricao], T2.[Proposta_Objetivo], TM1.[Requisito_ReferenciaTecnica], TM1.[Requisito_Agrupador], TM1.[Requisito_Restricao], TM1.[Requisito_Ordem], TM1.[Requisito_Pontuacao], TM1.[Requisito_DataHomologacao], TM1.[Requisito_Status], TM1.[Requisito_Cadastro], TM1.[Requisito_Prioridade], TM1.[Proposta_Codigo], TM1.[Requisito_TipoReqCod] AS Requisito_TipoReqCod, TM1.[Requisito_ReqCod] AS Requisito_ReqCod FROM ([Requisito] TM1 WITH (NOLOCK) LEFT JOIN dbo.[Proposta] T2 WITH (NOLOCK) ON T2.[Proposta_Codigo] = TM1.[Proposta_Codigo]) WHERE TM1.[Requisito_Codigo] = @Requisito_Codigo ORDER BY TM1.[Requisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004U7,100,0,true,false )
             ,new CursorDef("T004U8", "SELECT [TipoRequisito_Codigo] AS Requisito_TipoReqCod FROM [TipoRequisito] WITH (NOLOCK) WHERE [TipoRequisito_Codigo] = @Requisito_TipoReqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U8,1,0,true,false )
             ,new CursorDef("T004U9", "SELECT [Proposta_Objetivo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U9,1,0,true,false )
             ,new CursorDef("T004U10", "SELECT [Requisito_Codigo] AS Requisito_ReqCod FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_ReqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U10,1,0,true,false )
             ,new CursorDef("T004U11", "SELECT [Requisito_Codigo] FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004U11,1,0,true,false )
             ,new CursorDef("T004U12", "SELECT TOP 1 [Requisito_Codigo] FROM [Requisito] WITH (NOLOCK) WHERE ( [Requisito_Codigo] > @Requisito_Codigo) ORDER BY [Requisito_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004U12,1,0,true,true )
             ,new CursorDef("T004U13", "SELECT TOP 1 [Requisito_Codigo] FROM [Requisito] WITH (NOLOCK) WHERE ( [Requisito_Codigo] < @Requisito_Codigo) ORDER BY [Requisito_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004U13,1,0,true,true )
             ,new CursorDef("T004U14", "INSERT INTO [Requisito]([Requisito_Ativo], [Requisito_Identificador], [Requisito_Titulo], [Requisito_Descricao], [Requisito_ReferenciaTecnica], [Requisito_Agrupador], [Requisito_Restricao], [Requisito_Ordem], [Requisito_Pontuacao], [Requisito_DataHomologacao], [Requisito_Status], [Requisito_Cadastro], [Requisito_Prioridade], [Proposta_Codigo], [Requisito_TipoReqCod], [Requisito_ReqCod]) VALUES(@Requisito_Ativo, @Requisito_Identificador, @Requisito_Titulo, @Requisito_Descricao, @Requisito_ReferenciaTecnica, @Requisito_Agrupador, @Requisito_Restricao, @Requisito_Ordem, @Requisito_Pontuacao, @Requisito_DataHomologacao, @Requisito_Status, @Requisito_Cadastro, @Requisito_Prioridade, @Proposta_Codigo, @Requisito_TipoReqCod, @Requisito_ReqCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004U14)
             ,new CursorDef("T004U15", "UPDATE [Requisito] SET [Requisito_Ativo]=@Requisito_Ativo, [Requisito_Identificador]=@Requisito_Identificador, [Requisito_Titulo]=@Requisito_Titulo, [Requisito_Descricao]=@Requisito_Descricao, [Requisito_ReferenciaTecnica]=@Requisito_ReferenciaTecnica, [Requisito_Agrupador]=@Requisito_Agrupador, [Requisito_Restricao]=@Requisito_Restricao, [Requisito_Ordem]=@Requisito_Ordem, [Requisito_Pontuacao]=@Requisito_Pontuacao, [Requisito_DataHomologacao]=@Requisito_DataHomologacao, [Requisito_Status]=@Requisito_Status, [Requisito_Cadastro]=@Requisito_Cadastro, [Requisito_Prioridade]=@Requisito_Prioridade, [Proposta_Codigo]=@Proposta_Codigo, [Requisito_TipoReqCod]=@Requisito_TipoReqCod, [Requisito_ReqCod]=@Requisito_ReqCod  WHERE [Requisito_Codigo] = @Requisito_Codigo", GxErrorMask.GX_NOMASK,prmT004U15)
             ,new CursorDef("T004U16", "DELETE FROM [Requisito]  WHERE [Requisito_Codigo] = @Requisito_Codigo", GxErrorMask.GX_NOMASK,prmT004U16)
             ,new CursorDef("T004U17", "SELECT [Proposta_Objetivo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U17,1,0,true,false )
             ,new CursorDef("T004U18", "SELECT TOP 1 [Requisito_Codigo] AS Requisito_ReqCod FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_ReqCod] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U18,1,0,true,true )
             ,new CursorDef("T004U19", "SELECT TOP 1 [ContagemResultadoRequisito_Codigo] FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE [ContagemResultadoRequisito_ReqCod] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U19,1,0,true,true )
             ,new CursorDef("T004U20", "SELECT TOP 1 [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U20,1,0,true,true )
             ,new CursorDef("T004U21", "SELECT [Requisito_Codigo] FROM [Requisito] WITH (NOLOCK) ORDER BY [Requisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004U21,100,0,true,false )
             ,new CursorDef("T004U22", "SELECT [TipoRequisito_Codigo] AS Requisito_TipoReqCod, [TipoRequisito_Identificador] FROM [TipoRequisito] WITH (NOLOCK) ORDER BY [TipoRequisito_Identificador] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U22,0,0,true,false )
             ,new CursorDef("T004U23", "SELECT [TipoRequisito_Codigo] AS Requisito_TipoReqCod FROM [TipoRequisito] WITH (NOLOCK) WHERE [TipoRequisito_Codigo] = @Requisito_TipoReqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004U23,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((int[]) buf[29])[0] = rslt.getInt(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((int[]) buf[29])[0] = rslt.getInt(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((short[]) buf[15])[0] = rslt.getShort(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((short[]) buf[24])[0] = rslt.getShort(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((int[]) buf[26])[0] = rslt.getInt(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((int[]) buf[28])[0] = rslt.getInt(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (bool)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(10, (DateTime)parms[18]);
                }
                stmt.SetParameter(11, (short)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(12, (DateTime)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[29]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (bool)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(10, (DateTime)parms[18]);
                }
                stmt.SetParameter(11, (short)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(12, (DateTime)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[29]);
                }
                stmt.SetParameter(17, (int)parms[30]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
