/*
               File: WC_FuncaoDadosTabelas
        Description:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:41:14.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_funcaodadostabelas : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_funcaodadostabelas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_funcaodadostabelas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_codigo ,
                           int aP1_FuncaoDados_Codigo )
      {
         this.AV8Sistema_codigo = aP0_Sistema_codigo;
         this.AV10FuncaoDados_Codigo = aP1_FuncaoDados_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         chkAtributos_PK = new GXCheckbox();
         chkAtributos_FK = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV8Sistema_codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8Sistema_codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Sistema_codigo), 6, 0)));
                  AV10FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV8Sistema_codigo,(int)AV10FuncaoDados_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridtabelas") == 0 )
               {
                  nRC_GXsfl_30 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_30_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_30_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridtabelas_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridtabelas") == 0 )
               {
                  subGridtabelas_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV5Tabela_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Tabela_Nome", AV5Tabela_Nome);
                  AV10FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 6, 0)));
                  AV7Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0)));
                  A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridtabelas_refresh( subGridtabelas_Rows, AV5Tabela_Nome, AV10FuncaoDados_Codigo, AV7Tabela_Codigo, A172Tabela_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridatributos") == 0 )
               {
                  nRC_GXsfl_44 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_44_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_44_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridatributos_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridatributos") == 0 )
               {
                  subGridatributos_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV5Tabela_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Tabela_Nome", AV5Tabela_Nome);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridatributos_refresh( subGridatributos_Rows, AV5Tabela_Nome, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAA92( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSA92( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812411476");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_funcaodadostabelas.aspx") + "?" + UrlEncode("" +AV8Sistema_codigo) + "," + UrlEncode("" +AV10FuncaoDados_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_NOME", StringUtil.RTrim( AV5Tabela_Nome));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_30", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_30), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_44", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_44), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDTABELASPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15GridTabelasPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDATRIBUTOSPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17GridAtributosPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV8Sistema_codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV8Sistema_codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV10FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV10FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Tabela_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Sistema_codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Class", StringUtil.RTrim( Gridtabelaspaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_First", StringUtil.RTrim( Gridtabelaspaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Previous", StringUtil.RTrim( Gridtabelaspaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Next", StringUtil.RTrim( Gridtabelaspaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Last", StringUtil.RTrim( Gridtabelaspaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Caption", StringUtil.RTrim( Gridtabelaspaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridtabelaspaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridtabelaspaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridtabelaspaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridtabelaspaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridtabelaspaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridtabelaspaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridtabelaspaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridtabelaspaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridtabelaspaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Class", StringUtil.RTrim( Gridatributospaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_First", StringUtil.RTrim( Gridatributospaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Previous", StringUtil.RTrim( Gridatributospaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Next", StringUtil.RTrim( Gridatributospaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Last", StringUtil.RTrim( Gridatributospaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Caption", StringUtil.RTrim( Gridatributospaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridatributospaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridatributospaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridatributospaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridatributospaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridatributospaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridatributospaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridatributospaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridatributospaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridatributospaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_TABELAS_Width", StringUtil.RTrim( Dvpanel_tabelas_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_TABELAS_Cls", StringUtil.RTrim( Dvpanel_tabelas_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_TABELAS_Title", StringUtil.RTrim( Dvpanel_tabelas_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_TABELAS_Collapsible", StringUtil.BoolToStr( Dvpanel_tabelas_Collapsible));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_TABELAS_Collapsed", StringUtil.BoolToStr( Dvpanel_tabelas_Collapsed));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_TABELAS_Autowidth", StringUtil.BoolToStr( Dvpanel_tabelas_Autowidth));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_TABELAS_Autoheight", StringUtil.BoolToStr( Dvpanel_tabelas_Autoheight));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_TABELAS_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tabelas_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_TABELAS_Iconposition", StringUtil.RTrim( Dvpanel_tabelas_Iconposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_TABELAS_Autoscroll", StringUtil.BoolToStr( Dvpanel_tabelas_Autoscroll));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridtabelaspaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridatributospaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormA92( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_funcaodadostabelas.js", "?202051812411586");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_FuncaoDadosTabelas" ;
      }

      public override String GetPgmdesc( )
      {
         return "" ;
      }

      protected void WBA90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_funcaodadostabelas.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
            }
            wb_table1_2_A92( true) ;
         }
         else
         {
            wb_table1_2_A92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_A92e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTA92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPA90( ) ;
            }
         }
      }

      protected void WSA92( )
      {
         STARTA92( ) ;
         EVTA92( ) ;
      }

      protected void EVTA92( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDTABELASPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11A92 */
                                    E11A92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOSPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12A92 */
                                    E12A92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERTTABELA'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13A92 */
                                    E13A92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavTabela_nome_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "GRIDTABELAS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "VDELETETABELA.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 26), "GRIDTABELAS.ONLINEACTIVATE") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "VDELETETABELA.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA90( ) ;
                              }
                              nGXsfl_30_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
                              SubsflControlProps_302( ) ;
                              AV6DeleteTabela = cgiGet( edtavDeletetabela_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDeletetabela_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV6DeleteTabela)) ? AV20Deletetabela_GXI : context.convertURL( context.PathToRelativeUrl( AV6DeleteTabela))));
                              A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_Codigo_Internalname), ",", "."));
                              A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
                              A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
                              A189Tabela_ModuloDes = StringUtil.Upper( cgiGet( edtTabela_ModuloDes_Internalname));
                              n189Tabela_ModuloDes = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14A92 */
                                          E14A92 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15A92 */
                                          E15A92 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDTABELAS.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16A92 */
                                          E16A92 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VDELETETABELA.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17A92 */
                                          E17A92 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDTABELAS.ONLINEACTIVATE") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18A92 */
                                          E18A92 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Tabela_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME"), AV5Tabela_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPA90( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "GRIDATRIBUTOS.LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA90( ) ;
                              }
                              nGXsfl_44_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
                              SubsflControlProps_443( ) ;
                              A356Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( edtAtributos_TabelaCod_Internalname), ",", "."));
                              A176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAtributos_Codigo_Internalname), ",", "."));
                              A177Atributos_Nome = StringUtil.Upper( cgiGet( edtAtributos_Nome_Internalname));
                              A400Atributos_PK = StringUtil.StrToBool( cgiGet( chkAtributos_PK_Internalname));
                              n400Atributos_PK = false;
                              A401Atributos_FK = StringUtil.StrToBool( cgiGet( chkAtributos_FK_Internalname));
                              n401Atributos_FK = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOS.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19A93 */
                                          E19A93 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPA90( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEA92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormA92( ) ;
            }
         }
      }

      protected void PAA92( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "ATRIBUTOS_PK_" + sGXsfl_44_idx;
            chkAtributos_PK.Name = GXCCtl;
            chkAtributos_PK.WebTags = "";
            chkAtributos_PK.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_PK_Internalname, "TitleCaption", chkAtributos_PK.Caption);
            chkAtributos_PK.CheckedValue = "false";
            GXCCtl = "ATRIBUTOS_FK_" + sGXsfl_44_idx;
            chkAtributos_FK.Name = GXCCtl;
            chkAtributos_FK.WebTags = "";
            chkAtributos_FK.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_FK_Internalname, "TitleCaption", chkAtributos_FK.Caption);
            chkAtributos_FK.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavTabela_nome_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridtabelas_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_302( ) ;
         while ( nGXsfl_30_idx <= nRC_GXsfl_30 )
         {
            sendrow_302( ) ;
            nGXsfl_30_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_30_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_30_idx+1));
            sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
            SubsflControlProps_302( ) ;
         }
         context.GX_webresponse.AddString(GridtabelasContainer.ToJavascriptSource());
         /* End function gxnrGridtabelas_newrow */
      }

      protected void gxnrGridatributos_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_443( ) ;
         while ( nGXsfl_44_idx <= nRC_GXsfl_44 )
         {
            sendrow_443( ) ;
            nGXsfl_44_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_44_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_44_idx+1));
            sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
            SubsflControlProps_443( ) ;
         }
         context.GX_webresponse.AddString(GridatributosContainer.ToJavascriptSource());
         /* End function gxnrGridatributos_newrow */
      }

      protected void gxgrGridtabelas_refresh( int subGridtabelas_Rows ,
                                              String AV5Tabela_Nome ,
                                              int AV10FuncaoDados_Codigo ,
                                              int AV7Tabela_Codigo ,
                                              int A172Tabela_Codigo ,
                                              String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
         GRIDTABELAS_nCurrentRecord = 0;
         RFA92( ) ;
         /* End function gxgrGridtabelas_refresh */
      }

      protected void gxgrGridatributos_refresh( int subGridatributos_Rows ,
                                                String AV5Tabela_Nome ,
                                                String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         /* Execute user event: E15A92 */
         E15A92 ();
         GRIDATRIBUTOS_nCurrentRecord = 0;
         RFA93( ) ;
         /* End function gxgrGridatributos_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFA92( ) ;
         RFA93( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFA92( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridtabelasContainer.ClearRows();
         }
         wbStart = 30;
         /* Execute user event: E15A92 */
         E15A92 ();
         nGXsfl_30_idx = 1;
         sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
         SubsflControlProps_302( ) ;
         nGXsfl_30_Refreshing = 1;
         GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
         GridtabelasContainer.AddObjectProperty("CmpContext", sPrefix);
         GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
         GridtabelasContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
         GridtabelasContainer.PageSize = subGridtabelas_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_302( ) ;
            GXPagingFrom2 = (int)(((subGridtabelas_Rows==0) ? 1 : GRIDTABELAS_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGridtabelas_Rows==0) ? 10000 : GRIDTABELAS_nFirstRecordOnPage+subGridtabelas_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV5Tabela_Nome ,
                                                 A173Tabela_Nome ,
                                                 A174Tabela_Ativo ,
                                                 AV10FuncaoDados_Codigo ,
                                                 A368FuncaoDados_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV5Tabela_Nome = StringUtil.PadR( StringUtil.RTrim( AV5Tabela_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Tabela_Nome", AV5Tabela_Nome);
            /* Using cursor H00A92 */
            pr_default.execute(0, new Object[] {AV10FuncaoDados_Codigo, lV5Tabela_Nome, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_30_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGridtabelas_Rows == 0 ) || ( GRIDTABELAS_nCurrentRecord < subGridtabelas_Recordsperpage( ) ) ) ) )
            {
               A188Tabela_ModuloCod = H00A92_A188Tabela_ModuloCod[0];
               n188Tabela_ModuloCod = H00A92_n188Tabela_ModuloCod[0];
               A174Tabela_Ativo = H00A92_A174Tabela_Ativo[0];
               n174Tabela_Ativo = H00A92_n174Tabela_Ativo[0];
               A189Tabela_ModuloDes = H00A92_A189Tabela_ModuloDes[0];
               n189Tabela_ModuloDes = H00A92_n189Tabela_ModuloDes[0];
               A173Tabela_Nome = H00A92_A173Tabela_Nome[0];
               A172Tabela_Codigo = H00A92_A172Tabela_Codigo[0];
               A368FuncaoDados_Codigo = H00A92_A368FuncaoDados_Codigo[0];
               A188Tabela_ModuloCod = H00A92_A188Tabela_ModuloCod[0];
               n188Tabela_ModuloCod = H00A92_n188Tabela_ModuloCod[0];
               A174Tabela_Ativo = H00A92_A174Tabela_Ativo[0];
               n174Tabela_Ativo = H00A92_n174Tabela_Ativo[0];
               A173Tabela_Nome = H00A92_A173Tabela_Nome[0];
               A189Tabela_ModuloDes = H00A92_A189Tabela_ModuloDes[0];
               n189Tabela_ModuloDes = H00A92_n189Tabela_ModuloDes[0];
               /* Execute user event: E16A92 */
               E16A92 ();
               pr_default.readNext(0);
            }
            GRIDTABELAS_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 30;
            WBA90( ) ;
         }
         nGXsfl_30_Refreshing = 0;
      }

      protected void RFA93( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridatributosContainer.ClearRows();
         }
         wbStart = 44;
         nGXsfl_44_idx = 1;
         sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
         SubsflControlProps_443( ) ;
         nGXsfl_44_Refreshing = 1;
         GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
         GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
         GridatributosContainer.AddObjectProperty("InMasterPage", "false");
         GridatributosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
         GridatributosContainer.PageSize = subGridatributos_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_443( ) ;
            GXPagingFrom3 = (int)(((subGridatributos_Rows==0) ? 1 : GRIDATRIBUTOS_nFirstRecordOnPage+1));
            GXPagingTo3 = (int)(((subGridatributos_Rows==0) ? 10000 : GRIDATRIBUTOS_nFirstRecordOnPage+subGridatributos_Recordsperpage( )+1));
            /* Using cursor H00A93 */
            pr_default.execute(1, new Object[] {AV7Tabela_Codigo, GXPagingFrom3, GXPagingTo3});
            nGXsfl_44_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGridatributos_Rows == 0 ) || ( GRIDATRIBUTOS_nCurrentRecord < subGridatributos_Recordsperpage( ) ) ) ) )
            {
               A174Tabela_Ativo = H00A93_A174Tabela_Ativo[0];
               n174Tabela_Ativo = H00A93_n174Tabela_Ativo[0];
               A180Atributos_Ativo = H00A93_A180Atributos_Ativo[0];
               A401Atributos_FK = H00A93_A401Atributos_FK[0];
               n401Atributos_FK = H00A93_n401Atributos_FK[0];
               A400Atributos_PK = H00A93_A400Atributos_PK[0];
               n400Atributos_PK = H00A93_n400Atributos_PK[0];
               A177Atributos_Nome = H00A93_A177Atributos_Nome[0];
               A176Atributos_Codigo = H00A93_A176Atributos_Codigo[0];
               A356Atributos_TabelaCod = H00A93_A356Atributos_TabelaCod[0];
               A174Tabela_Ativo = H00A93_A174Tabela_Ativo[0];
               n174Tabela_Ativo = H00A93_n174Tabela_Ativo[0];
               /* Execute user event: E19A93 */
               E19A93 ();
               pr_default.readNext(1);
            }
            GRIDATRIBUTOS_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 44;
            WBA90( ) ;
         }
         nGXsfl_44_Refreshing = 0;
      }

      protected int subGridtabelas_Pagecount( )
      {
         GRIDTABELAS_nRecordCount = subGridtabelas_Recordcount( );
         if ( ((int)((GRIDTABELAS_nRecordCount) % (subGridtabelas_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDTABELAS_nRecordCount/ (decimal)(subGridtabelas_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDTABELAS_nRecordCount/ (decimal)(subGridtabelas_Recordsperpage( ))))+1) ;
      }

      protected int subGridtabelas_Recordcount( )
      {
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV5Tabela_Nome ,
                                              A173Tabela_Nome ,
                                              A174Tabela_Ativo ,
                                              AV10FuncaoDados_Codigo ,
                                              A368FuncaoDados_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV5Tabela_Nome = StringUtil.PadR( StringUtil.RTrim( AV5Tabela_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Tabela_Nome", AV5Tabela_Nome);
         /* Using cursor H00A94 */
         pr_default.execute(2, new Object[] {AV10FuncaoDados_Codigo, lV5Tabela_Nome});
         GRIDTABELAS_nRecordCount = H00A94_AGRIDTABELAS_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRIDTABELAS_nRecordCount) ;
      }

      protected int subGridtabelas_Recordsperpage( )
      {
         if ( subGridtabelas_Rows > 0 )
         {
            return subGridtabelas_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridtabelas_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDTABELAS_nFirstRecordOnPage/ (decimal)(subGridtabelas_Recordsperpage( ))))+1) ;
      }

      protected short subgridtabelas_firstpage( )
      {
         GRIDTABELAS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, AV5Tabela_Nome, AV10FuncaoDados_Codigo, AV7Tabela_Codigo, A172Tabela_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridtabelas_nextpage( )
      {
         GRIDTABELAS_nRecordCount = subGridtabelas_Recordcount( );
         if ( ( GRIDTABELAS_nRecordCount >= subGridtabelas_Recordsperpage( ) ) && ( GRIDTABELAS_nEOF == 0 ) )
         {
            GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nFirstRecordOnPage+subGridtabelas_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, AV5Tabela_Nome, AV10FuncaoDados_Codigo, AV7Tabela_Codigo, A172Tabela_Codigo, sPrefix) ;
         }
         return (short)(((GRIDTABELAS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridtabelas_previouspage( )
      {
         if ( GRIDTABELAS_nFirstRecordOnPage >= subGridtabelas_Recordsperpage( ) )
         {
            GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nFirstRecordOnPage-subGridtabelas_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, AV5Tabela_Nome, AV10FuncaoDados_Codigo, AV7Tabela_Codigo, A172Tabela_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridtabelas_lastpage( )
      {
         GRIDTABELAS_nRecordCount = subGridtabelas_Recordcount( );
         if ( GRIDTABELAS_nRecordCount > subGridtabelas_Recordsperpage( ) )
         {
            if ( ((int)((GRIDTABELAS_nRecordCount) % (subGridtabelas_Recordsperpage( )))) == 0 )
            {
               GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nRecordCount-subGridtabelas_Recordsperpage( ));
            }
            else
            {
               GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nRecordCount-((int)((GRIDTABELAS_nRecordCount) % (subGridtabelas_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDTABELAS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, AV5Tabela_Nome, AV10FuncaoDados_Codigo, AV7Tabela_Codigo, A172Tabela_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgridtabelas_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDTABELAS_nFirstRecordOnPage = (long)(subGridtabelas_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDTABELAS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, AV5Tabela_Nome, AV10FuncaoDados_Codigo, AV7Tabela_Codigo, A172Tabela_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected int subGridatributos_Pagecount( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( ((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))+1) ;
      }

      protected int subGridatributos_Recordcount( )
      {
         /* Using cursor H00A95 */
         pr_default.execute(3, new Object[] {AV7Tabela_Codigo});
         GRIDATRIBUTOS_nRecordCount = H00A95_AGRIDATRIBUTOS_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRIDATRIBUTOS_nRecordCount) ;
      }

      protected int subGridatributos_Recordsperpage( )
      {
         if ( subGridatributos_Rows > 0 )
         {
            return subGridatributos_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridatributos_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nFirstRecordOnPage/ (decimal)(subGridatributos_Recordsperpage( ))))+1) ;
      }

      protected short subgridatributos_firstpage( )
      {
         GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV5Tabela_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_nextpage( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( ( GRIDATRIBUTOS_nRecordCount >= subGridatributos_Recordsperpage( ) ) && ( GRIDATRIBUTOS_nEOF == 0 ) )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage+subGridatributos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV5Tabela_Nome, sPrefix) ;
         }
         return (short)(((GRIDATRIBUTOS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridatributos_previouspage( )
      {
         if ( GRIDATRIBUTOS_nFirstRecordOnPage >= subGridatributos_Recordsperpage( ) )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage-subGridatributos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV5Tabela_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_lastpage( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( GRIDATRIBUTOS_nRecordCount > subGridatributos_Recordsperpage( ) )
         {
            if ( ((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))) == 0 )
            {
               GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nRecordCount-subGridatributos_Recordsperpage( ));
            }
            else
            {
               GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nRecordCount-((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV5Tabela_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgridatributos_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(subGridatributos_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV5Tabela_Nome, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPA90( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E14A92 */
         E14A92 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5Tabela_Nome = StringUtil.Upper( cgiGet( edtavTabela_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Tabela_Nome", AV5Tabela_Nome);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGridtabelascurrentpage_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGridtabelascurrentpage_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGRIDTABELASCURRENTPAGE");
               GX_FocusControl = edtavGridtabelascurrentpage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14GridTabelasCurrentPage = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GridTabelasCurrentPage), 10, 0)));
            }
            else
            {
               AV14GridTabelasCurrentPage = (long)(context.localUtil.CToN( cgiGet( edtavGridtabelascurrentpage_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GridTabelasCurrentPage), 10, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGridatributoscurrentpage_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGridatributoscurrentpage_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGRIDATRIBUTOSCURRENTPAGE");
               GX_FocusControl = edtavGridatributoscurrentpage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16GridAtributosCurrentPage = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16GridAtributosCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridAtributosCurrentPage), 10, 0)));
            }
            else
            {
               AV16GridAtributosCurrentPage = (long)(context.localUtil.CToN( cgiGet( edtavGridatributoscurrentpage_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16GridAtributosCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridAtributosCurrentPage), 10, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_30 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_30"), ",", "."));
            nRC_GXsfl_44 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_44"), ",", "."));
            AV15GridTabelasPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDTABELASPAGECOUNT"), ",", "."));
            AV17GridAtributosPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDATRIBUTOSPAGECOUNT"), ",", "."));
            wcpOAV8Sistema_codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV8Sistema_codigo"), ",", "."));
            wcpOAV10FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV10FuncaoDados_Codigo"), ",", "."));
            GRIDTABELAS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDTABELAS_nFirstRecordOnPage"), ",", "."));
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage"), ",", "."));
            GRIDTABELAS_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDTABELAS_nEOF"), ",", "."));
            GRIDATRIBUTOS_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nEOF"), ",", "."));
            subGridtabelas_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDTABELAS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
            subGridatributos_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
            Gridtabelaspaginationbar_Class = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Class");
            Gridtabelaspaginationbar_First = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_First");
            Gridtabelaspaginationbar_Previous = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Previous");
            Gridtabelaspaginationbar_Next = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Next");
            Gridtabelaspaginationbar_Last = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Last");
            Gridtabelaspaginationbar_Caption = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Caption");
            Gridtabelaspaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Showfirst"));
            Gridtabelaspaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Showprevious"));
            Gridtabelaspaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Shownext"));
            Gridtabelaspaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Showlast"));
            Gridtabelaspaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridtabelaspaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Pagingbuttonsposition");
            Gridtabelaspaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Pagingcaptionposition");
            Gridtabelaspaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Emptygridclass");
            Gridtabelaspaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Emptygridcaption");
            Gridatributospaginationbar_Class = cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Class");
            Gridatributospaginationbar_First = cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_First");
            Gridatributospaginationbar_Previous = cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Previous");
            Gridatributospaginationbar_Next = cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Next");
            Gridatributospaginationbar_Last = cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Last");
            Gridatributospaginationbar_Caption = cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Caption");
            Gridatributospaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Showfirst"));
            Gridatributospaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Showprevious"));
            Gridatributospaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Shownext"));
            Gridatributospaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Showlast"));
            Gridatributospaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridatributospaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Pagingbuttonsposition");
            Gridatributospaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Pagingcaptionposition");
            Gridatributospaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Emptygridclass");
            Gridatributospaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Emptygridcaption");
            Dvpanel_tabelas_Width = cgiGet( sPrefix+"DVPANEL_TABELAS_Width");
            Dvpanel_tabelas_Cls = cgiGet( sPrefix+"DVPANEL_TABELAS_Cls");
            Dvpanel_tabelas_Title = cgiGet( sPrefix+"DVPANEL_TABELAS_Title");
            Dvpanel_tabelas_Collapsible = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_TABELAS_Collapsible"));
            Dvpanel_tabelas_Collapsed = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_TABELAS_Collapsed"));
            Dvpanel_tabelas_Autowidth = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_TABELAS_Autowidth"));
            Dvpanel_tabelas_Autoheight = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_TABELAS_Autoheight"));
            Dvpanel_tabelas_Showcollapseicon = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_TABELAS_Showcollapseicon"));
            Dvpanel_tabelas_Iconposition = cgiGet( sPrefix+"DVPANEL_TABELAS_Iconposition");
            Dvpanel_tabelas_Autoscroll = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_TABELAS_Autoscroll"));
            Gridtabelaspaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Selectedpage");
            Gridatributospaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDATRIBUTOSPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME"), AV5Tabela_Nome) != 0 )
            {
               GRIDTABELAS_nFirstRecordOnPage = 0;
            }
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E14A92 */
         E14A92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14A92( )
      {
         /* Start Routine */
         subGridtabelas_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
         AV14GridTabelasCurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GridTabelasCurrentPage), 10, 0)));
         edtavGridtabelascurrentpage_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGridtabelascurrentpage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridtabelascurrentpage_Visible), 5, 0)));
         AV15GridTabelasPageCount = -1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15GridTabelasPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GridTabelasPageCount), 10, 0)));
         subGridatributos_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         AV16GridAtributosCurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16GridAtributosCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridAtributosCurrentPage), 10, 0)));
         edtavGridatributoscurrentpage_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGridatributoscurrentpage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridatributoscurrentpage_Visible), 5, 0)));
         AV17GridAtributosPageCount = -1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17GridAtributosPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GridAtributosPageCount), 10, 0)));
      }

      protected void E15A92( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
      }

      private void E16A92( )
      {
         /* Gridtabelas_Load Routine */
         AV6DeleteTabela = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDeletetabela_Internalname, AV6DeleteTabela);
         AV20Deletetabela_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDeletetabela_Tooltiptext = "Excluir a tabela desta fun��o";
         AV6DeleteTabela = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDeletetabela_Internalname, AV6DeleteTabela);
         AV20Deletetabela_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDeletetabela_Tooltiptext = "Excluir a tabela desta fun��o";
         if ( (0==AV7Tabela_Codigo) )
         {
            AV7Tabela_Codigo = A172Tabela_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0)));
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 30;
         }
         sendrow_302( ) ;
         GRIDTABELAS_nCurrentRecord = (long)(GRIDTABELAS_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_30_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(30, GridtabelasRow);
         }
      }

      protected void E11A92( )
      {
         /* Gridtabelaspaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridtabelaspaginationbar_Selectedpage, "Previous") == 0 )
         {
            AV14GridTabelasCurrentPage = (long)(AV14GridTabelasCurrentPage-1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GridTabelasCurrentPage), 10, 0)));
            subgridtabelas_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridtabelaspaginationbar_Selectedpage, "Next") == 0 )
         {
            AV14GridTabelasCurrentPage = (long)(AV14GridTabelasCurrentPage+1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GridTabelasCurrentPage), 10, 0)));
            subgridtabelas_nextpage( ) ;
         }
         else
         {
            AV13PageToGo = (int)(NumberUtil.Val( Gridtabelaspaginationbar_Selectedpage, "."));
            AV14GridTabelasCurrentPage = AV13PageToGo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GridTabelasCurrentPage), 10, 0)));
            subgridtabelas_gotopage( AV13PageToGo) ;
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E12A92( )
      {
         /* Gridatributospaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridatributospaginationbar_Selectedpage, "Previous") == 0 )
         {
            AV16GridAtributosCurrentPage = (long)(AV16GridAtributosCurrentPage-1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16GridAtributosCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridAtributosCurrentPage), 10, 0)));
            subgridatributos_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridatributospaginationbar_Selectedpage, "Next") == 0 )
         {
            AV16GridAtributosCurrentPage = (long)(AV16GridAtributosCurrentPage+1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16GridAtributosCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridAtributosCurrentPage), 10, 0)));
            subgridatributos_nextpage( ) ;
         }
         else
         {
            AV13PageToGo = (int)(NumberUtil.Val( Gridatributospaginationbar_Selectedpage, "."));
            AV16GridAtributosCurrentPage = AV13PageToGo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16GridAtributosCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridAtributosCurrentPage), 10, 0)));
            subgridatributos_gotopage( AV13PageToGo) ;
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E13A92( )
      {
         /* 'DoInsertTabela' Routine */
         context.PopUp(formatLink("wp_funcaodadostabelains.aspx") + "?" + UrlEncode("" +AV8Sistema_codigo) + "," + UrlEncode("" +AV10FuncaoDados_Codigo), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E17A92( )
      {
         /* Deletetabela_Click Routine */
         AV7Tabela_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0)));
         context.DoAjaxRefreshCmp(sPrefix);
         new prc_funcaodadosdlttabela(context ).execute( ref  A368FuncaoDados_Codigo, ref  A172Tabela_Codigo) ;
      }

      protected void E18A92( )
      {
         /* Gridtabelas_Onlineactivate Routine */
         AV7Tabela_Codigo = A172Tabela_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0)));
         gxgrGridatributos_refresh( subGridatributos_Rows, AV5Tabela_Nome, sPrefix) ;
      }

      private void E19A93( )
      {
         /* Gridatributos_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 44;
         }
         sendrow_443( ) ;
         GRIDATRIBUTOS_nCurrentRecord = (long)(GRIDATRIBUTOS_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_44_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(44, GridatributosRow);
         }
      }

      protected void wb_table1_2_A92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_A92( true) ;
         }
         else
         {
            wb_table2_8_A92( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_A92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_A92e( true) ;
         }
         else
         {
            wb_table1_2_A92e( false) ;
         }
      }

      protected void wb_table2_8_A92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVPANEL_TABELASContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVPANEL_TABELASContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table3_13_A92( true) ;
         }
         else
         {
            wb_table3_13_A92( false) ;
         }
         return  ;
      }

      protected void wb_table3_13_A92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_A92e( true) ;
         }
         else
         {
            wb_table2_8_A92e( false) ;
         }
      }

      protected void wb_table3_13_A92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabelas_Internalname, tblTabelas_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_16_A92( true) ;
         }
         else
         {
            wb_table4_16_A92( false) ;
         }
         return  ;
      }

      protected void wb_table4_16_A92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_13_A92e( true) ;
         }
         else
         {
            wb_table3_13_A92e( false) ;
         }
      }

      protected void wb_table4_16_A92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            wb_table5_19_A92( true) ;
         }
         else
         {
            wb_table5_19_A92( false) ;
         }
         return  ;
      }

      protected void wb_table5_19_A92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table6_27_A92( true) ;
         }
         else
         {
            wb_table6_27_A92( false) ;
         }
         return  ;
      }

      protected void wb_table6_27_A92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table7_41_A92( true) ;
         }
         else
         {
            wb_table7_41_A92( false) ;
         }
         return  ;
      }

      protected void wb_table7_41_A92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInserttabela_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Incluir novas tabelas nesta Fun��o", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInserttabela_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERTTABELA\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WC_FuncaoDadosTabelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_A92e( true) ;
         }
         else
         {
            wb_table4_16_A92e( false) ;
         }
      }

      protected void wb_table7_41_A92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridatributostablewithpaginationbar_Internalname, tblGridatributostablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridatributosContainer.SetWrapped(nGXWrapped);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"DivS\" data-gxgridid=\"44\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridatributos_Internalname, subGridatributos_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridatributos_Backcolorstyle == 0 )
               {
                  subGridatributos_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridatributos_Class) > 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Title";
                  }
               }
               else
               {
                  subGridatributos_Titlebackstyle = 1;
                  if ( subGridatributos_Backcolorstyle == 1 )
                  {
                     subGridatributos_Titlebackcolor = subGridatributos_Allbackcolor;
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Atributos") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PK") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "FK") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridatributosContainer = new GXWebGrid( context);
               }
               else
               {
                  GridatributosContainer.Clear();
               }
               GridatributosContainer.SetWrapped(nGXWrapped);
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
               GridatributosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
               GridatributosContainer.AddObjectProperty("InMasterPage", "false");
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( A177Atributos_Nome));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A400Atributos_PK));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A401Atributos_FK));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowselection), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Selectioncolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowhovering), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Hoveringcolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowcollapsing), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 44 )
         {
            wbEnd = 0;
            nRC_GXsfl_44 = (short)(nGXsfl_44_idx-1);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Gridatributos", GridatributosContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData", GridatributosContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData"+"V", GridatributosContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridatributosContainerData"+"V"+"\" value='"+GridatributosContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDATRIBUTOSPAGINATIONBARContainer"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGridatributoscurrentpage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16GridAtributosCurrentPage), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV16GridAtributosCurrentPage), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,53);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGridatributoscurrentpage_Jsonclick, 0, "Attribute", "", "", "", edtavGridatributoscurrentpage_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WC_FuncaoDadosTabelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_41_A92e( true) ;
         }
         else
         {
            wb_table7_41_A92e( false) ;
         }
      }

      protected void wb_table6_27_A92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtabelastablewithpaginationbar_Internalname, tblGridtabelastablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridtabelasContainer.SetWrapped(nGXWrapped);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridtabelasContainer"+"DivS\" data-gxgridid=\"30\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridtabelas_Internalname, subGridtabelas_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridtabelas_Backcolorstyle == 0 )
               {
                  subGridtabelas_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                  }
               }
               else
               {
                  subGridtabelas_Titlebackstyle = 1;
                  if ( subGridtabelas_Backcolorstyle == 1 )
                  {
                     subGridtabelas_Titlebackcolor = subGridtabelas_Allbackcolor;
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "M�dulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridtabelasContainer = new GXWebGrid( context);
               }
               else
               {
                  GridtabelasContainer.Clear();
               }
               GridtabelasContainer.SetWrapped(nGXWrapped);
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
               GridtabelasContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("CmpContext", sPrefix);
               GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", context.convertURL( AV6DeleteTabela));
               GridtabelasColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDeletetabela_Tooltiptext));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ".", "")));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.RTrim( A173Tabela_Nome));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.RTrim( A189Tabela_ModuloDes));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowselection), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Selectioncolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowhovering), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Hoveringcolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowcollapsing), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 30 )
         {
            wbEnd = 0;
            nRC_GXsfl_30 = (short)(nGXsfl_30_idx-1);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridtabelasContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Gridtabelas", GridtabelasContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridtabelasContainerData", GridtabelasContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridtabelasContainerData"+"V", GridtabelasContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridtabelasContainerData"+"V"+"\" value='"+GridtabelasContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDTABELASPAGINATIONBARContainer"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGridtabelascurrentpage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14GridTabelasCurrentPage), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14GridTabelasCurrentPage), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGridtabelascurrentpage_Jsonclick, 0, "Attribute", "", "", "", edtavGridtabelascurrentpage_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WC_FuncaoDadosTabelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_27_A92e( true) ;
         }
         else
         {
            wb_table6_27_A92e( false) ;
         }
      }

      protected void wb_table5_19_A92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_nome_Internalname, "Filtrar tabelas contendo no nome:", "", "", lblTextblocktabela_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WC_FuncaoDadosTabelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_nome_Internalname, StringUtil.RTrim( AV5Tabela_Nome), StringUtil.RTrim( context.localUtil.Format( AV5Tabela_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,24);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WC_FuncaoDadosTabelas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_A92e( true) ;
         }
         else
         {
            wb_table5_19_A92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV8Sistema_codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8Sistema_codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Sistema_codigo), 6, 0)));
         AV10FuncaoDados_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAA92( ) ;
         WSA92( ) ;
         WEA92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV8Sistema_codigo = (String)((String)getParm(obj,0));
         sCtrlAV10FuncaoDados_Codigo = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAA92( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_funcaodadostabelas");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAA92( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV8Sistema_codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8Sistema_codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Sistema_codigo), 6, 0)));
            AV10FuncaoDados_Codigo = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 6, 0)));
         }
         wcpOAV8Sistema_codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV8Sistema_codigo"), ",", "."));
         wcpOAV10FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV10FuncaoDados_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV8Sistema_codigo != wcpOAV8Sistema_codigo ) || ( AV10FuncaoDados_Codigo != wcpOAV10FuncaoDados_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV8Sistema_codigo = AV8Sistema_codigo;
         wcpOAV10FuncaoDados_Codigo = AV10FuncaoDados_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV8Sistema_codigo = cgiGet( sPrefix+"AV8Sistema_codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV8Sistema_codigo) > 0 )
         {
            AV8Sistema_codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV8Sistema_codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8Sistema_codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Sistema_codigo), 6, 0)));
         }
         else
         {
            AV8Sistema_codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV8Sistema_codigo_PARM"), ",", "."));
         }
         sCtrlAV10FuncaoDados_Codigo = cgiGet( sPrefix+"AV10FuncaoDados_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV10FuncaoDados_Codigo) > 0 )
         {
            AV10FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV10FuncaoDados_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 6, 0)));
         }
         else
         {
            AV10FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV10FuncaoDados_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAA92( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSA92( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSA92( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV8Sistema_codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Sistema_codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV8Sistema_codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV8Sistema_codigo_CTRL", StringUtil.RTrim( sCtrlAV8Sistema_codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV10FuncaoDados_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10FuncaoDados_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV10FuncaoDados_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV10FuncaoDados_Codigo_CTRL", StringUtil.RTrim( sCtrlAV10FuncaoDados_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEA92( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812411843");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("wc_funcaodadostabelas.js", "?202051812411843");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
            context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_302( )
      {
         edtavDeletetabela_Internalname = sPrefix+"vDELETETABELA_"+sGXsfl_30_idx;
         edtFuncaoDados_Codigo_Internalname = sPrefix+"FUNCAODADOS_CODIGO_"+sGXsfl_30_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_30_idx;
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME_"+sGXsfl_30_idx;
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES_"+sGXsfl_30_idx;
      }

      protected void SubsflControlProps_fel_302( )
      {
         edtavDeletetabela_Internalname = sPrefix+"vDELETETABELA_"+sGXsfl_30_fel_idx;
         edtFuncaoDados_Codigo_Internalname = sPrefix+"FUNCAODADOS_CODIGO_"+sGXsfl_30_fel_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_30_fel_idx;
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME_"+sGXsfl_30_fel_idx;
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES_"+sGXsfl_30_fel_idx;
      }

      protected void sendrow_302( )
      {
         SubsflControlProps_302( ) ;
         WBA90( ) ;
         if ( ( subGridtabelas_Rows * 1 == 0 ) || ( nGXsfl_30_idx <= subGridtabelas_Recordsperpage( ) * 1 ) )
         {
            GridtabelasRow = GXWebRow.GetNew(context,GridtabelasContainer);
            if ( subGridtabelas_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridtabelas_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
               }
            }
            else if ( subGridtabelas_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridtabelas_Backstyle = 0;
               subGridtabelas_Backcolor = subGridtabelas_Allbackcolor;
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Uniform";
               }
            }
            else if ( subGridtabelas_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridtabelas_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
               }
               subGridtabelas_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridtabelas_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridtabelas_Backstyle = 1;
               if ( ((int)((nGXsfl_30_idx) % (2))) == 0 )
               {
                  subGridtabelas_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Even";
                  }
               }
               else
               {
                  subGridtabelas_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
                  }
               }
            }
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridtabelas_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_30_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavDeletetabela_Enabled!=0)&&(edtavDeletetabela_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 31,'"+sPrefix+"',false,'',30)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV6DeleteTabela_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV6DeleteTabela))&&String.IsNullOrEmpty(StringUtil.RTrim( AV20Deletetabela_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV6DeleteTabela)));
            GridtabelasRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDeletetabela_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV6DeleteTabela)) ? AV20Deletetabela_GXI : context.PathToRelativeUrl( AV6DeleteTabela)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDeletetabela_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavDeletetabela_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDELETETABELA.CLICK."+sGXsfl_30_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV6DeleteTabela_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Nome_Internalname,StringUtil.RTrim( A173Tabela_Nome),StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_ModuloDes_Internalname,StringUtil.RTrim( A189Tabela_ModuloDes),StringUtil.RTrim( context.localUtil.Format( A189Tabela_ModuloDes, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_ModuloDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GridtabelasContainer.AddRow(GridtabelasRow);
            nGXsfl_30_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_30_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_30_idx+1));
            sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
            SubsflControlProps_302( ) ;
         }
         /* End function sendrow_302 */
      }

      protected void SubsflControlProps_443( )
      {
         edtAtributos_TabelaCod_Internalname = sPrefix+"ATRIBUTOS_TABELACOD_"+sGXsfl_44_idx;
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO_"+sGXsfl_44_idx;
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME_"+sGXsfl_44_idx;
         chkAtributos_PK_Internalname = sPrefix+"ATRIBUTOS_PK_"+sGXsfl_44_idx;
         chkAtributos_FK_Internalname = sPrefix+"ATRIBUTOS_FK_"+sGXsfl_44_idx;
      }

      protected void SubsflControlProps_fel_443( )
      {
         edtAtributos_TabelaCod_Internalname = sPrefix+"ATRIBUTOS_TABELACOD_"+sGXsfl_44_fel_idx;
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO_"+sGXsfl_44_fel_idx;
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME_"+sGXsfl_44_fel_idx;
         chkAtributos_PK_Internalname = sPrefix+"ATRIBUTOS_PK_"+sGXsfl_44_fel_idx;
         chkAtributos_FK_Internalname = sPrefix+"ATRIBUTOS_FK_"+sGXsfl_44_fel_idx;
      }

      protected void sendrow_443( )
      {
         SubsflControlProps_443( ) ;
         WBA90( ) ;
         if ( ( subGridatributos_Rows * 1 == 0 ) || ( nGXsfl_44_idx <= subGridatributos_Recordsperpage( ) * 1 ) )
         {
            GridatributosRow = GXWebRow.GetNew(context,GridatributosContainer);
            if ( subGridatributos_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridatributos_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridatributos_Backstyle = 0;
               subGridatributos_Backcolor = subGridatributos_Allbackcolor;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Uniform";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
               subGridatributos_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridatributos_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( ((int)((nGXsfl_44_idx) % (2))) == 0 )
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Even";
                  }
               }
               else
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Odd";
                  }
               }
            }
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridatributos_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_44_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_TabelaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_TabelaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)44,(short)1,(short)-1,(short)0,(bool)false,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)44,(short)1,(short)-1,(short)0,(bool)false,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Nome_Internalname,StringUtil.RTrim( A177Atributos_Nome),StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)44,(short)1,(short)-1,(short)-1,(bool)false,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridatributosRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAtributos_PK_Internalname,StringUtil.BoolToStr( A400Atributos_PK),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridatributosRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAtributos_FK_Internalname,StringUtil.BoolToStr( A401Atributos_FK),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GridatributosContainer.AddRow(GridatributosRow);
            nGXsfl_44_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_44_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_44_idx+1));
            sGXsfl_44_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_44_idx), 4, 0)), 4, "0");
            SubsflControlProps_443( ) ;
         }
         /* End function sendrow_443 */
      }

      protected void init_default_properties( )
      {
         lblTextblocktabela_nome_Internalname = sPrefix+"TEXTBLOCKTABELA_NOME";
         edtavTabela_nome_Internalname = sPrefix+"vTABELA_NOME";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         edtavDeletetabela_Internalname = sPrefix+"vDELETETABELA";
         edtFuncaoDados_Codigo_Internalname = sPrefix+"FUNCAODADOS_CODIGO";
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO";
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME";
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES";
         Gridtabelaspaginationbar_Internalname = sPrefix+"GRIDTABELASPAGINATIONBAR";
         edtavGridtabelascurrentpage_Internalname = sPrefix+"vGRIDTABELASCURRENTPAGE";
         tblGridtabelastablewithpaginationbar_Internalname = sPrefix+"GRIDTABELASTABLEWITHPAGINATIONBAR";
         edtAtributos_TabelaCod_Internalname = sPrefix+"ATRIBUTOS_TABELACOD";
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO";
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME";
         chkAtributos_PK_Internalname = sPrefix+"ATRIBUTOS_PK";
         chkAtributos_FK_Internalname = sPrefix+"ATRIBUTOS_FK";
         Gridatributospaginationbar_Internalname = sPrefix+"GRIDATRIBUTOSPAGINATIONBAR";
         edtavGridatributoscurrentpage_Internalname = sPrefix+"vGRIDATRIBUTOSCURRENTPAGE";
         tblGridatributostablewithpaginationbar_Internalname = sPrefix+"GRIDATRIBUTOSTABLEWITHPAGINATIONBAR";
         imgInserttabela_Internalname = sPrefix+"INSERTTABELA";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         tblTabelas_Internalname = sPrefix+"TABELAS";
         Dvpanel_tabelas_Internalname = sPrefix+"DVPANEL_TABELAS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         tblTablemain_Internalname = sPrefix+"TABLEMAIN";
         Form.Internalname = sPrefix+"FORM";
         subGridtabelas_Internalname = sPrefix+"GRIDTABELAS";
         subGridatributos_Internalname = sPrefix+"GRIDATRIBUTOS";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtAtributos_Nome_Jsonclick = "";
         edtAtributos_Codigo_Jsonclick = "";
         edtAtributos_TabelaCod_Jsonclick = "";
         edtTabela_ModuloDes_Jsonclick = "";
         edtTabela_Nome_Jsonclick = "";
         edtTabela_Codigo_Jsonclick = "";
         edtFuncaoDados_Codigo_Jsonclick = "";
         edtavDeletetabela_Jsonclick = "";
         edtavDeletetabela_Visible = -1;
         edtavDeletetabela_Enabled = 1;
         edtavTabela_nome_Jsonclick = "";
         edtavGridtabelascurrentpage_Jsonclick = "";
         subGridtabelas_Allowcollapsing = 0;
         subGridtabelas_Allowselection = 0;
         edtavDeletetabela_Tooltiptext = "Excluir a tabela desta fun��o";
         subGridtabelas_Class = "WorkWithBorder WorkWith";
         edtavGridatributoscurrentpage_Jsonclick = "";
         subGridatributos_Allowcollapsing = 0;
         subGridatributos_Allowselection = 0;
         subGridatributos_Class = "WorkWithBorder WorkWith";
         edtavGridatributoscurrentpage_Visible = 1;
         edtavGridtabelascurrentpage_Visible = 1;
         subGridatributos_Backcolorstyle = 3;
         subGridtabelas_Backcolorstyle = 3;
         chkAtributos_FK.Caption = "";
         chkAtributos_PK.Caption = "";
         Dvpanel_tabelas_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tabelas_Iconposition = "left";
         Dvpanel_tabelas_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tabelas_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tabelas_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tabelas_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tabelas_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_tabelas_Title = "Tabelas da Fun��o";
         Dvpanel_tabelas_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tabelas_Width = "100%";
         Gridatributospaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridatributospaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridatributospaginationbar_Pagingcaptionposition = "Left";
         Gridatributospaginationbar_Pagingbuttonsposition = "Right";
         Gridatributospaginationbar_Pagestoshow = 5;
         Gridatributospaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridatributospaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridatributospaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridatributospaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridatributospaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridatributospaginationbar_Last = "�|";
         Gridatributospaginationbar_Next = "�";
         Gridatributospaginationbar_Previous = "�";
         Gridatributospaginationbar_First = "|�";
         Gridatributospaginationbar_Class = "PaginationBar";
         Gridtabelaspaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridtabelaspaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridtabelaspaginationbar_Pagingcaptionposition = "Left";
         Gridtabelaspaginationbar_Pagingbuttonsposition = "Right";
         Gridtabelaspaginationbar_Pagestoshow = 5;
         Gridtabelaspaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridtabelaspaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridtabelaspaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridtabelaspaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridtabelaspaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridtabelaspaginationbar_Last = "�|";
         Gridtabelaspaginationbar_Next = "�";
         Gridtabelaspaginationbar_Previous = "�";
         Gridtabelaspaginationbar_First = "|�";
         Gridtabelaspaginationbar_Class = "PaginationBar";
         subGridatributos_Rows = 0;
         subGridtabelas_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'AV10FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV5Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDTABELAS.LOAD","{handler:'E16A92',iparms:[{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV6DeleteTabela',fld:'vDELETETABELA',pic:'',nv:''},{av:'edtavDeletetabela_Tooltiptext',ctrl:'vDELETETABELA',prop:'Tooltiptext'},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDATRIBUTOS.LOAD","{handler:'E19A93',iparms:[],oparms:[]}");
         setEventMetadata("GRIDTABELASPAGINATIONBAR.CHANGEPAGE","{handler:'E11A92',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'AV5Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'AV10FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridtabelaspaginationbar_Selectedpage',ctrl:'GRIDTABELASPAGINATIONBAR',prop:'SelectedPage'},{av:'AV14GridTabelasCurrentPage',fld:'vGRIDTABELASCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}],oparms:[{av:'AV14GridTabelasCurrentPage',fld:'vGRIDTABELASCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDATRIBUTOSPAGINATIONBAR.CHANGEPAGE","{handler:'E12A92',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'AV5Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'AV10FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'Gridatributospaginationbar_Selectedpage',ctrl:'GRIDATRIBUTOSPAGINATIONBAR',prop:'SelectedPage'},{av:'AV16GridAtributosCurrentPage',fld:'vGRIDATRIBUTOSCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}],oparms:[{av:'AV16GridAtributosCurrentPage',fld:'vGRIDATRIBUTOSCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("'DOINSERTTABELA'","{handler:'E13A92',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'AV5Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'AV10FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV8Sistema_codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("VDELETETABELA.CLICK","{handler:'E17A92',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'AV5Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'AV10FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDTABELAS.ONLINEACTIVATE","{handler:'E18A92',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV5Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridtabelaspaginationbar_Selectedpage = "";
         Gridatributospaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV5Tabela_Nome = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV6DeleteTabela = "";
         AV20Deletetabela_GXI = "";
         A173Tabela_Nome = "";
         A189Tabela_ModuloDes = "";
         A177Atributos_Nome = "";
         GXCCtl = "";
         GridtabelasContainer = new GXWebGrid( context);
         GridatributosContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV5Tabela_Nome = "";
         H00A92_A188Tabela_ModuloCod = new int[1] ;
         H00A92_n188Tabela_ModuloCod = new bool[] {false} ;
         H00A92_A174Tabela_Ativo = new bool[] {false} ;
         H00A92_n174Tabela_Ativo = new bool[] {false} ;
         H00A92_A189Tabela_ModuloDes = new String[] {""} ;
         H00A92_n189Tabela_ModuloDes = new bool[] {false} ;
         H00A92_A173Tabela_Nome = new String[] {""} ;
         H00A92_A172Tabela_Codigo = new int[1] ;
         H00A92_A368FuncaoDados_Codigo = new int[1] ;
         H00A93_A174Tabela_Ativo = new bool[] {false} ;
         H00A93_n174Tabela_Ativo = new bool[] {false} ;
         H00A93_A180Atributos_Ativo = new bool[] {false} ;
         H00A93_A401Atributos_FK = new bool[] {false} ;
         H00A93_n401Atributos_FK = new bool[] {false} ;
         H00A93_A400Atributos_PK = new bool[] {false} ;
         H00A93_n400Atributos_PK = new bool[] {false} ;
         H00A93_A177Atributos_Nome = new String[] {""} ;
         H00A93_A176Atributos_Codigo = new int[1] ;
         H00A93_A356Atributos_TabelaCod = new int[1] ;
         H00A94_AGRIDTABELAS_nRecordCount = new long[1] ;
         H00A95_AGRIDATRIBUTOS_nRecordCount = new long[1] ;
         GridtabelasRow = new GXWebRow();
         GridatributosRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         imgInserttabela_Jsonclick = "";
         subGridatributos_Linesclass = "";
         GridatributosColumn = new GXWebColumn();
         subGridtabelas_Linesclass = "";
         GridtabelasColumn = new GXWebColumn();
         lblTextblocktabela_nome_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV8Sistema_codigo = "";
         sCtrlAV10FuncaoDados_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_funcaodadostabelas__default(),
            new Object[][] {
                new Object[] {
               H00A92_A188Tabela_ModuloCod, H00A92_n188Tabela_ModuloCod, H00A92_A174Tabela_Ativo, H00A92_A189Tabela_ModuloDes, H00A92_n189Tabela_ModuloDes, H00A92_A173Tabela_Nome, H00A92_A172Tabela_Codigo, H00A92_A368FuncaoDados_Codigo
               }
               , new Object[] {
               H00A93_A174Tabela_Ativo, H00A93_n174Tabela_Ativo, H00A93_A180Atributos_Ativo, H00A93_A401Atributos_FK, H00A93_n401Atributos_FK, H00A93_A400Atributos_PK, H00A93_n400Atributos_PK, H00A93_A177Atributos_Nome, H00A93_A176Atributos_Codigo, H00A93_A356Atributos_TabelaCod
               }
               , new Object[] {
               H00A94_AGRIDTABELAS_nRecordCount
               }
               , new Object[] {
               H00A95_AGRIDATRIBUTOS_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_30 ;
      private short nGXsfl_30_idx=1 ;
      private short nRC_GXsfl_44 ;
      private short nGXsfl_44_idx=1 ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRIDTABELAS_nEOF ;
      private short GRIDATRIBUTOS_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_30_Refreshing=0 ;
      private short subGridtabelas_Backcolorstyle ;
      private short nGXsfl_44_Refreshing=0 ;
      private short subGridatributos_Backcolorstyle ;
      private short subGridatributos_Titlebackstyle ;
      private short subGridatributos_Allowselection ;
      private short subGridatributos_Allowhovering ;
      private short subGridatributos_Allowcollapsing ;
      private short subGridatributos_Collapsed ;
      private short subGridtabelas_Titlebackstyle ;
      private short subGridtabelas_Allowselection ;
      private short subGridtabelas_Allowhovering ;
      private short subGridtabelas_Allowcollapsing ;
      private short subGridtabelas_Collapsed ;
      private short subGridtabelas_Backstyle ;
      private short subGridatributos_Backstyle ;
      private int AV8Sistema_codigo ;
      private int AV10FuncaoDados_Codigo ;
      private int wcpOAV8Sistema_codigo ;
      private int wcpOAV10FuncaoDados_Codigo ;
      private int subGridtabelas_Rows ;
      private int AV7Tabela_Codigo ;
      private int A172Tabela_Codigo ;
      private int subGridatributos_Rows ;
      private int Gridtabelaspaginationbar_Pagestoshow ;
      private int Gridatributospaginationbar_Pagestoshow ;
      private int A368FuncaoDados_Codigo ;
      private int A356Atributos_TabelaCod ;
      private int A176Atributos_Codigo ;
      private int subGridtabelas_Islastpage ;
      private int subGridatributos_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A188Tabela_ModuloCod ;
      private int GXPagingFrom3 ;
      private int GXPagingTo3 ;
      private int edtavGridtabelascurrentpage_Visible ;
      private int edtavGridatributoscurrentpage_Visible ;
      private int AV13PageToGo ;
      private int subGridatributos_Titlebackcolor ;
      private int subGridatributos_Allbackcolor ;
      private int subGridatributos_Selectioncolor ;
      private int subGridatributos_Hoveringcolor ;
      private int subGridtabelas_Titlebackcolor ;
      private int subGridtabelas_Allbackcolor ;
      private int subGridtabelas_Selectioncolor ;
      private int subGridtabelas_Hoveringcolor ;
      private int idxLst ;
      private int subGridtabelas_Backcolor ;
      private int edtavDeletetabela_Enabled ;
      private int edtavDeletetabela_Visible ;
      private int subGridatributos_Backcolor ;
      private long GRIDTABELAS_nFirstRecordOnPage ;
      private long GRIDATRIBUTOS_nFirstRecordOnPage ;
      private long AV15GridTabelasPageCount ;
      private long AV17GridAtributosPageCount ;
      private long GRIDTABELAS_nCurrentRecord ;
      private long GRIDATRIBUTOS_nCurrentRecord ;
      private long GRIDTABELAS_nRecordCount ;
      private long GRIDATRIBUTOS_nRecordCount ;
      private long AV14GridTabelasCurrentPage ;
      private long AV16GridAtributosCurrentPage ;
      private String Gridtabelaspaginationbar_Selectedpage ;
      private String Gridatributospaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_30_idx="0001" ;
      private String AV5Tabela_Nome ;
      private String GXKey ;
      private String sGXsfl_44_idx="0001" ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridtabelaspaginationbar_Class ;
      private String Gridtabelaspaginationbar_First ;
      private String Gridtabelaspaginationbar_Previous ;
      private String Gridtabelaspaginationbar_Next ;
      private String Gridtabelaspaginationbar_Last ;
      private String Gridtabelaspaginationbar_Caption ;
      private String Gridtabelaspaginationbar_Pagingbuttonsposition ;
      private String Gridtabelaspaginationbar_Pagingcaptionposition ;
      private String Gridtabelaspaginationbar_Emptygridclass ;
      private String Gridtabelaspaginationbar_Emptygridcaption ;
      private String Gridatributospaginationbar_Class ;
      private String Gridatributospaginationbar_First ;
      private String Gridatributospaginationbar_Previous ;
      private String Gridatributospaginationbar_Next ;
      private String Gridatributospaginationbar_Last ;
      private String Gridatributospaginationbar_Caption ;
      private String Gridatributospaginationbar_Pagingbuttonsposition ;
      private String Gridatributospaginationbar_Pagingcaptionposition ;
      private String Gridatributospaginationbar_Emptygridclass ;
      private String Gridatributospaginationbar_Emptygridcaption ;
      private String Dvpanel_tabelas_Width ;
      private String Dvpanel_tabelas_Cls ;
      private String Dvpanel_tabelas_Title ;
      private String Dvpanel_tabelas_Iconposition ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavTabela_nome_Internalname ;
      private String edtavDeletetabela_Internalname ;
      private String edtFuncaoDados_Codigo_Internalname ;
      private String edtTabela_Codigo_Internalname ;
      private String A173Tabela_Nome ;
      private String edtTabela_Nome_Internalname ;
      private String A189Tabela_ModuloDes ;
      private String edtTabela_ModuloDes_Internalname ;
      private String edtAtributos_TabelaCod_Internalname ;
      private String edtAtributos_Codigo_Internalname ;
      private String A177Atributos_Nome ;
      private String edtAtributos_Nome_Internalname ;
      private String chkAtributos_PK_Internalname ;
      private String chkAtributos_FK_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV5Tabela_Nome ;
      private String edtavGridtabelascurrentpage_Internalname ;
      private String edtavGridatributoscurrentpage_Internalname ;
      private String edtavDeletetabela_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablecontent_Internalname ;
      private String tblTabelas_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String TempTags ;
      private String imgInserttabela_Internalname ;
      private String imgInserttabela_Jsonclick ;
      private String tblGridatributostablewithpaginationbar_Internalname ;
      private String subGridatributos_Internalname ;
      private String subGridatributos_Class ;
      private String subGridatributos_Linesclass ;
      private String edtavGridatributoscurrentpage_Jsonclick ;
      private String tblGridtabelastablewithpaginationbar_Internalname ;
      private String subGridtabelas_Internalname ;
      private String subGridtabelas_Class ;
      private String subGridtabelas_Linesclass ;
      private String edtavGridtabelascurrentpage_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblocktabela_nome_Internalname ;
      private String lblTextblocktabela_nome_Jsonclick ;
      private String edtavTabela_nome_Jsonclick ;
      private String sCtrlAV8Sistema_codigo ;
      private String sCtrlAV10FuncaoDados_Codigo ;
      private String sGXsfl_30_fel_idx="0001" ;
      private String edtavDeletetabela_Jsonclick ;
      private String ROClassString ;
      private String edtFuncaoDados_Codigo_Jsonclick ;
      private String edtTabela_Codigo_Jsonclick ;
      private String edtTabela_Nome_Jsonclick ;
      private String edtTabela_ModuloDes_Jsonclick ;
      private String sGXsfl_44_fel_idx="0001" ;
      private String edtAtributos_TabelaCod_Jsonclick ;
      private String edtAtributos_Codigo_Jsonclick ;
      private String edtAtributos_Nome_Jsonclick ;
      private String Gridtabelaspaginationbar_Internalname ;
      private String Gridatributospaginationbar_Internalname ;
      private String Dvpanel_tabelas_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Gridtabelaspaginationbar_Showfirst ;
      private bool Gridtabelaspaginationbar_Showprevious ;
      private bool Gridtabelaspaginationbar_Shownext ;
      private bool Gridtabelaspaginationbar_Showlast ;
      private bool Gridatributospaginationbar_Showfirst ;
      private bool Gridatributospaginationbar_Showprevious ;
      private bool Gridatributospaginationbar_Shownext ;
      private bool Gridatributospaginationbar_Showlast ;
      private bool Dvpanel_tabelas_Collapsible ;
      private bool Dvpanel_tabelas_Collapsed ;
      private bool Dvpanel_tabelas_Autowidth ;
      private bool Dvpanel_tabelas_Autoheight ;
      private bool Dvpanel_tabelas_Showcollapseicon ;
      private bool Dvpanel_tabelas_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n189Tabela_ModuloDes ;
      private bool A400Atributos_PK ;
      private bool n400Atributos_PK ;
      private bool A401Atributos_FK ;
      private bool n401Atributos_FK ;
      private bool A174Tabela_Ativo ;
      private bool n188Tabela_ModuloCod ;
      private bool n174Tabela_Ativo ;
      private bool A180Atributos_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV6DeleteTabela_IsBlob ;
      private String AV20Deletetabela_GXI ;
      private String AV6DeleteTabela ;
      private GXWebGrid GridtabelasContainer ;
      private GXWebGrid GridatributosContainer ;
      private GXWebRow GridtabelasRow ;
      private GXWebRow GridatributosRow ;
      private GXWebColumn GridatributosColumn ;
      private GXWebColumn GridtabelasColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkAtributos_PK ;
      private GXCheckbox chkAtributos_FK ;
      private IDataStoreProvider pr_default ;
      private int[] H00A92_A188Tabela_ModuloCod ;
      private bool[] H00A92_n188Tabela_ModuloCod ;
      private bool[] H00A92_A174Tabela_Ativo ;
      private bool[] H00A92_n174Tabela_Ativo ;
      private String[] H00A92_A189Tabela_ModuloDes ;
      private bool[] H00A92_n189Tabela_ModuloDes ;
      private String[] H00A92_A173Tabela_Nome ;
      private int[] H00A92_A172Tabela_Codigo ;
      private int[] H00A92_A368FuncaoDados_Codigo ;
      private bool[] H00A93_A174Tabela_Ativo ;
      private bool[] H00A93_n174Tabela_Ativo ;
      private bool[] H00A93_A180Atributos_Ativo ;
      private bool[] H00A93_A401Atributos_FK ;
      private bool[] H00A93_n401Atributos_FK ;
      private bool[] H00A93_A400Atributos_PK ;
      private bool[] H00A93_n400Atributos_PK ;
      private String[] H00A93_A177Atributos_Nome ;
      private int[] H00A93_A176Atributos_Codigo ;
      private int[] H00A93_A356Atributos_TabelaCod ;
      private long[] H00A94_AGRIDTABELAS_nRecordCount ;
      private long[] H00A95_AGRIDATRIBUTOS_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class wc_funcaodadostabelas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00A92( IGxContext context ,
                                             String AV5Tabela_Nome ,
                                             String A173Tabela_Nome ,
                                             bool A174Tabela_Ativo ,
                                             int AV10FuncaoDados_Codigo ,
                                             int A368FuncaoDados_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Tabela_ModuloCod] AS Tabela_ModuloCod, T2.[Tabela_Ativo], T3.[Modulo_Nome] AS Tabela_ModuloDes, T2.[Tabela_Nome], T1.[Tabela_Codigo], T1.[FuncaoDados_Codigo]";
         sFromString = " FROM (([FuncaoDadosTabela] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Tabela_Codigo]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T2.[Tabela_ModuloCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[FuncaoDados_Codigo] = @AV10FuncaoDados_Codigo)";
         sWhereString = sWhereString + " and (T2.[Tabela_Ativo] = 1)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5Tabela_Nome)) )
         {
            sWhereString = sWhereString + " and (UPPER(T2.[Tabela_Nome]) like '%' + UPPER(@lV5Tabela_Nome))";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         sOrderString = sOrderString + " ORDER BY T1.[FuncaoDados_Codigo]";
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00A94( IGxContext context ,
                                             String AV5Tabela_Nome ,
                                             String A173Tabela_Nome ,
                                             bool A174Tabela_Ativo ,
                                             int AV10FuncaoDados_Codigo ,
                                             int A368FuncaoDados_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [2] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([FuncaoDadosTabela] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Tabela_Codigo]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T2.[Tabela_ModuloCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoDados_Codigo] = @AV10FuncaoDados_Codigo)";
         scmdbuf = scmdbuf + " and (T2.[Tabela_Ativo] = 1)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5Tabela_Nome)) )
         {
            sWhereString = sWhereString + " and (UPPER(T2.[Tabela_Nome]) like '%' + UPPER(@lV5Tabela_Nome))";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00A92(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
               case 2 :
                     return conditional_H00A94(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00A93 ;
          prmH00A93 = new Object[] {
          new Object[] {"@AV7Tabela_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom3",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo3",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00A95 ;
          prmH00A95 = new Object[] {
          new Object[] {"@AV7Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00A92 ;
          prmH00A92 = new Object[] {
          new Object[] {"@AV10FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV5Tabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00A94 ;
          prmH00A94 = new Object[] {
          new Object[] {"@AV10FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV5Tabela_Nome",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00A92", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A92,11,0,true,false )
             ,new CursorDef("H00A93", "SELECT * FROM (SELECT  T2.[Tabela_Ativo], T1.[Atributos_Ativo], T1.[Atributos_FK], T1.[Atributos_PK], T1.[Atributos_Nome], T1.[Atributos_Codigo], T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, ROW_NUMBER() OVER ( ORDER BY T1.[Atributos_TabelaCod] ) AS GX_ROW_NUMBER FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) WHERE (T1.[Atributos_TabelaCod] = @AV7Tabela_Codigo) AND (T2.[Tabela_Ativo] = 1) AND (T1.[Atributos_Ativo] = 1)) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom3 AND @GXPagingTo3 OR @GXPagingTo3 < @GXPagingFrom3 AND GX_ROW_NUMBER >= @GXPagingFrom3",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A93,11,0,false,false )
             ,new CursorDef("H00A94", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A94,1,0,true,false )
             ,new CursorDef("H00A95", "SELECT COUNT(*) FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) WHERE (T1.[Atributos_TabelaCod] = @AV7Tabela_Codigo) AND (T2.[Tabela_Ativo] = 1) AND (T1.[Atributos_Ativo] = 1) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A95,1,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[3]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
