/*
               File: PRC_SistemasFromExcel
        Description: Stub for PRC_SistemasFromExcel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:36:54.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_sistemasfromexcel : GXProcedure
   {
      public prc_sistemasfromexcel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_sistemasfromexcel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_FileName ,
                           String aP1_FileNameReal ,
                           String aP2_Aba ,
                           ref short aP3_PraLinea ,
                           short aP4_ColSisNom ,
                           short aP5_ColSisSgl ,
                           short aP6_ColModNom ,
                           short aP7_ColModSglOpc )
      {
         this.AV2FileName = aP0_FileName;
         this.AV3FileNameReal = aP1_FileNameReal;
         this.AV4Aba = aP2_Aba;
         this.AV5PraLinea = aP3_PraLinea;
         this.AV6ColSisNom = aP4_ColSisNom;
         this.AV7ColSisSgl = aP5_ColSisSgl;
         this.AV8ColModNom = aP6_ColModNom;
         this.AV9ColModSglOpc = aP7_ColModSglOpc;
         initialize();
         executePrivate();
         aP3_PraLinea=this.AV5PraLinea;
      }

      public void executeSubmit( String aP0_FileName ,
                                 String aP1_FileNameReal ,
                                 String aP2_Aba ,
                                 ref short aP3_PraLinea ,
                                 short aP4_ColSisNom ,
                                 short aP5_ColSisSgl ,
                                 short aP6_ColModNom ,
                                 short aP7_ColModSglOpc )
      {
         prc_sistemasfromexcel objprc_sistemasfromexcel;
         objprc_sistemasfromexcel = new prc_sistemasfromexcel();
         objprc_sistemasfromexcel.AV2FileName = aP0_FileName;
         objprc_sistemasfromexcel.AV3FileNameReal = aP1_FileNameReal;
         objprc_sistemasfromexcel.AV4Aba = aP2_Aba;
         objprc_sistemasfromexcel.AV5PraLinea = aP3_PraLinea;
         objprc_sistemasfromexcel.AV6ColSisNom = aP4_ColSisNom;
         objprc_sistemasfromexcel.AV7ColSisSgl = aP5_ColSisSgl;
         objprc_sistemasfromexcel.AV8ColModNom = aP6_ColModNom;
         objprc_sistemasfromexcel.AV9ColModSglOpc = aP7_ColModSglOpc;
         objprc_sistemasfromexcel.context.SetSubmitInitialConfig(context);
         objprc_sistemasfromexcel.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_sistemasfromexcel);
         aP3_PraLinea=this.AV5PraLinea;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_sistemasfromexcel)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2FileName,(String)AV3FileNameReal,(String)AV4Aba,(short)AV5PraLinea,(short)AV6ColSisNom,(short)AV7ColSisSgl,(short)AV8ColModNom,(short)AV9ColModSglOpc} ;
         ClassLoader.Execute("aprc_sistemasfromexcel","GeneXus.Programs.aprc_sistemasfromexcel", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 8 ) )
         {
            AV5PraLinea = (short)(args[3]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV5PraLinea ;
      private short AV6ColSisNom ;
      private short AV7ColSisSgl ;
      private short AV8ColModNom ;
      private short AV9ColModSglOpc ;
      private String AV2FileName ;
      private String AV3FileNameReal ;
      private String AV4Aba ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private short aP3_PraLinea ;
      private Object[] args ;
   }

}
