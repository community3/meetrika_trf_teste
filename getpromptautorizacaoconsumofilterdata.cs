/*
               File: GetPromptAutorizacaoConsumoFilterData
        Description: Get Prompt Autorizacao Consumo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:52:38.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptautorizacaoconsumofilterdata : GXProcedure
   {
      public getpromptautorizacaoconsumofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptautorizacaoconsumofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV30DDOName = aP0_DDOName;
         this.AV28SearchTxt = aP1_SearchTxt;
         this.AV29SearchTxtTo = aP2_SearchTxtTo;
         this.AV34OptionsJson = "" ;
         this.AV37OptionsDescJson = "" ;
         this.AV39OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV30DDOName = aP0_DDOName;
         this.AV28SearchTxt = aP1_SearchTxt;
         this.AV29SearchTxtTo = aP2_SearchTxtTo;
         this.AV34OptionsJson = "" ;
         this.AV37OptionsDescJson = "" ;
         this.AV39OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
         return AV39OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptautorizacaoconsumofilterdata objgetpromptautorizacaoconsumofilterdata;
         objgetpromptautorizacaoconsumofilterdata = new getpromptautorizacaoconsumofilterdata();
         objgetpromptautorizacaoconsumofilterdata.AV30DDOName = aP0_DDOName;
         objgetpromptautorizacaoconsumofilterdata.AV28SearchTxt = aP1_SearchTxt;
         objgetpromptautorizacaoconsumofilterdata.AV29SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptautorizacaoconsumofilterdata.AV34OptionsJson = "" ;
         objgetpromptautorizacaoconsumofilterdata.AV37OptionsDescJson = "" ;
         objgetpromptautorizacaoconsumofilterdata.AV39OptionIndexesJson = "" ;
         objgetpromptautorizacaoconsumofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptautorizacaoconsumofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptautorizacaoconsumofilterdata);
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptautorizacaoconsumofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV33Options = (IGxCollection)(new GxSimpleCollection());
         AV36OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV38OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM") == 0 )
         {
            /* Execute user subroutine: 'LOADAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV34OptionsJson = AV33Options.ToJSonString(false);
         AV37OptionsDescJson = AV36OptionsDesc.ToJSonString(false);
         AV39OptionIndexesJson = AV38OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV41Session.Get("PromptAutorizacaoConsumoGridState"), "") == 0 )
         {
            AV43GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptAutorizacaoConsumoGridState"), "");
         }
         else
         {
            AV43GridState.FromXml(AV41Session.Get("PromptAutorizacaoConsumoGridState"), "");
         }
         AV59GXV1 = 1;
         while ( AV59GXV1 <= AV43GridState.gxTpr_Filtervalues.Count )
         {
            AV44GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV43GridState.gxTpr_Filtervalues.Item(AV59GXV1));
            if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_CODIGO") == 0 )
            {
               AV10TFAutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
               AV11TFAutorizacaoConsumo_Codigo_To = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATO_CODIGO") == 0 )
            {
               AV12TFContrato_Codigo = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContrato_Codigo_To = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATO_DATAVIGENCIATERMINO") == 0 )
            {
               AV14TFContrato_DataVigenciaTermino = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Value, 2);
               AV15TFContrato_DataVigenciaTermino_To = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATO_DATAVIGENCIAINICIO") == 0 )
            {
               AV16TFContrato_DataVigenciaInicio = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Value, 2);
               AV17TFContrato_DataVigenciaInicio_To = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
            {
               AV18TFAutorizacaoConsumo_VigenciaInicio = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Value, 2);
               AV19TFAutorizacaoConsumo_VigenciaInicio_To = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VIGENCIAFIM") == 0 )
            {
               AV20TFAutorizacaoConsumo_VigenciaFim = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Value, 2);
               AV21TFAutorizacaoConsumo_VigenciaFim_To = context.localUtil.CToD( AV44GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD") == 0 )
            {
               AV22TFAutorizacaoConsumo_UnidadeMedicaoCod = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
               AV23TFAutorizacaoConsumo_UnidadeMedicaoCod_To = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM") == 0 )
            {
               AV24TFAutorizacaoConsumo_UnidadeMedicaoNom = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL") == 0 )
            {
               AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_QUANTIDADE") == 0 )
            {
               AV26TFAutorizacaoConsumo_Quantidade = (short)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
               AV27TFAutorizacaoConsumo_Quantidade_To = (short)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV59GXV1 = (int)(AV59GXV1+1);
         }
         if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(1));
            AV46DynamicFiltersSelector1 = AV45GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
            {
               AV47AutorizacaoConsumo_VigenciaInicio1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
               AV48AutorizacaoConsumo_VigenciaInicio_To1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV49DynamicFiltersEnabled2 = true;
               AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(2));
               AV50DynamicFiltersSelector2 = AV45GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
               {
                  AV51AutorizacaoConsumo_VigenciaInicio2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                  AV52AutorizacaoConsumo_VigenciaInicio_To2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV53DynamicFiltersEnabled3 = true;
                  AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(3));
                  AV54DynamicFiltersSelector3 = AV45GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
                  {
                     AV55AutorizacaoConsumo_VigenciaInicio3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                     AV56AutorizacaoConsumo_VigenciaInicio_To3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMOPTIONS' Routine */
         AV24TFAutorizacaoConsumo_UnidadeMedicaoNom = AV28SearchTxt;
         AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV46DynamicFiltersSelector1 ,
                                              AV47AutorizacaoConsumo_VigenciaInicio1 ,
                                              AV48AutorizacaoConsumo_VigenciaInicio_To1 ,
                                              AV49DynamicFiltersEnabled2 ,
                                              AV50DynamicFiltersSelector2 ,
                                              AV51AutorizacaoConsumo_VigenciaInicio2 ,
                                              AV52AutorizacaoConsumo_VigenciaInicio_To2 ,
                                              AV53DynamicFiltersEnabled3 ,
                                              AV54DynamicFiltersSelector3 ,
                                              AV55AutorizacaoConsumo_VigenciaInicio3 ,
                                              AV56AutorizacaoConsumo_VigenciaInicio_To3 ,
                                              AV10TFAutorizacaoConsumo_Codigo ,
                                              AV11TFAutorizacaoConsumo_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV14TFContrato_DataVigenciaTermino ,
                                              AV15TFContrato_DataVigenciaTermino_To ,
                                              AV16TFContrato_DataVigenciaInicio ,
                                              AV17TFContrato_DataVigenciaInicio_To ,
                                              AV18TFAutorizacaoConsumo_VigenciaInicio ,
                                              AV19TFAutorizacaoConsumo_VigenciaInicio_To ,
                                              AV20TFAutorizacaoConsumo_VigenciaFim ,
                                              AV21TFAutorizacaoConsumo_VigenciaFim_To ,
                                              AV22TFAutorizacaoConsumo_UnidadeMedicaoCod ,
                                              AV23TFAutorizacaoConsumo_UnidadeMedicaoCod_To ,
                                              AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ,
                                              AV24TFAutorizacaoConsumo_UnidadeMedicaoNom ,
                                              AV26TFAutorizacaoConsumo_Quantidade ,
                                              AV27TFAutorizacaoConsumo_Quantidade_To ,
                                              A1775AutorizacaoConsumo_VigenciaInicio ,
                                              A1774AutorizacaoConsumo_Codigo ,
                                              A74Contrato_Codigo ,
                                              A83Contrato_DataVigenciaTermino ,
                                              A82Contrato_DataVigenciaInicio ,
                                              A1776AutorizacaoConsumo_VigenciaFim ,
                                              A1777AutorizacaoConsumo_UnidadeMedicaoCod ,
                                              A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                              A1779AutorizacaoConsumo_Quantidade },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV24TFAutorizacaoConsumo_UnidadeMedicaoNom = StringUtil.PadR( StringUtil.RTrim( AV24TFAutorizacaoConsumo_UnidadeMedicaoNom), 50, "%");
         /* Using cursor P00RO2 */
         pr_default.execute(0, new Object[] {AV47AutorizacaoConsumo_VigenciaInicio1, AV48AutorizacaoConsumo_VigenciaInicio_To1, AV51AutorizacaoConsumo_VigenciaInicio2, AV52AutorizacaoConsumo_VigenciaInicio_To2, AV55AutorizacaoConsumo_VigenciaInicio3, AV56AutorizacaoConsumo_VigenciaInicio_To3, AV10TFAutorizacaoConsumo_Codigo, AV11TFAutorizacaoConsumo_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, AV14TFContrato_DataVigenciaTermino, AV15TFContrato_DataVigenciaTermino_To, AV16TFContrato_DataVigenciaInicio, AV17TFContrato_DataVigenciaInicio_To, AV18TFAutorizacaoConsumo_VigenciaInicio, AV19TFAutorizacaoConsumo_VigenciaInicio_To, AV20TFAutorizacaoConsumo_VigenciaFim, AV21TFAutorizacaoConsumo_VigenciaFim_To, AV22TFAutorizacaoConsumo_UnidadeMedicaoCod, AV23TFAutorizacaoConsumo_UnidadeMedicaoCod_To, lV24TFAutorizacaoConsumo_UnidadeMedicaoNom, AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV26TFAutorizacaoConsumo_Quantidade, AV27TFAutorizacaoConsumo_Quantidade_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKRO2 = false;
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = P00RO2_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
            A1779AutorizacaoConsumo_Quantidade = P00RO2_A1779AutorizacaoConsumo_Quantidade[0];
            A1778AutorizacaoConsumo_UnidadeMedicaoNom = P00RO2_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            n1778AutorizacaoConsumo_UnidadeMedicaoNom = P00RO2_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            A1776AutorizacaoConsumo_VigenciaFim = P00RO2_A1776AutorizacaoConsumo_VigenciaFim[0];
            A82Contrato_DataVigenciaInicio = P00RO2_A82Contrato_DataVigenciaInicio[0];
            A83Contrato_DataVigenciaTermino = P00RO2_A83Contrato_DataVigenciaTermino[0];
            A74Contrato_Codigo = P00RO2_A74Contrato_Codigo[0];
            A1774AutorizacaoConsumo_Codigo = P00RO2_A1774AutorizacaoConsumo_Codigo[0];
            A1775AutorizacaoConsumo_VigenciaInicio = P00RO2_A1775AutorizacaoConsumo_VigenciaInicio[0];
            A1778AutorizacaoConsumo_UnidadeMedicaoNom = P00RO2_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            n1778AutorizacaoConsumo_UnidadeMedicaoNom = P00RO2_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            A82Contrato_DataVigenciaInicio = P00RO2_A82Contrato_DataVigenciaInicio[0];
            A83Contrato_DataVigenciaTermino = P00RO2_A83Contrato_DataVigenciaTermino[0];
            AV40count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00RO2_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0] == A1777AutorizacaoConsumo_UnidadeMedicaoCod ) )
            {
               BRKRO2 = false;
               A1774AutorizacaoConsumo_Codigo = P00RO2_A1774AutorizacaoConsumo_Codigo[0];
               AV40count = (long)(AV40count+1);
               BRKRO2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1778AutorizacaoConsumo_UnidadeMedicaoNom)) )
            {
               AV32Option = A1778AutorizacaoConsumo_UnidadeMedicaoNom;
               AV31InsertIndex = 1;
               while ( ( AV31InsertIndex <= AV33Options.Count ) && ( StringUtil.StrCmp(((String)AV33Options.Item(AV31InsertIndex)), AV32Option) < 0 ) )
               {
                  AV31InsertIndex = (int)(AV31InsertIndex+1);
               }
               AV33Options.Add(AV32Option, AV31InsertIndex);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), AV31InsertIndex);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKRO2 )
            {
               BRKRO2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV33Options = new GxSimpleCollection();
         AV36OptionsDesc = new GxSimpleCollection();
         AV38OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV41Session = context.GetSession();
         AV43GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV44GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFContrato_DataVigenciaTermino = DateTime.MinValue;
         AV15TFContrato_DataVigenciaTermino_To = DateTime.MinValue;
         AV16TFContrato_DataVigenciaInicio = DateTime.MinValue;
         AV17TFContrato_DataVigenciaInicio_To = DateTime.MinValue;
         AV18TFAutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         AV19TFAutorizacaoConsumo_VigenciaInicio_To = DateTime.MinValue;
         AV20TFAutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         AV21TFAutorizacaoConsumo_VigenciaFim_To = DateTime.MinValue;
         AV24TFAutorizacaoConsumo_UnidadeMedicaoNom = "";
         AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "";
         AV45GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV46DynamicFiltersSelector1 = "";
         AV47AutorizacaoConsumo_VigenciaInicio1 = DateTime.MinValue;
         AV48AutorizacaoConsumo_VigenciaInicio_To1 = DateTime.MinValue;
         AV50DynamicFiltersSelector2 = "";
         AV51AutorizacaoConsumo_VigenciaInicio2 = DateTime.MinValue;
         AV52AutorizacaoConsumo_VigenciaInicio_To2 = DateTime.MinValue;
         AV54DynamicFiltersSelector3 = "";
         AV55AutorizacaoConsumo_VigenciaInicio3 = DateTime.MinValue;
         AV56AutorizacaoConsumo_VigenciaInicio_To3 = DateTime.MinValue;
         scmdbuf = "";
         lV24TFAutorizacaoConsumo_UnidadeMedicaoNom = "";
         A1775AutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A1776AutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = "";
         P00RO2_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         P00RO2_A1779AutorizacaoConsumo_Quantidade = new short[1] ;
         P00RO2_A1778AutorizacaoConsumo_UnidadeMedicaoNom = new String[] {""} ;
         P00RO2_n1778AutorizacaoConsumo_UnidadeMedicaoNom = new bool[] {false} ;
         P00RO2_A1776AutorizacaoConsumo_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         P00RO2_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00RO2_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         P00RO2_A74Contrato_Codigo = new int[1] ;
         P00RO2_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         P00RO2_A1775AutorizacaoConsumo_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         AV32Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptautorizacaoconsumofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00RO2_A1777AutorizacaoConsumo_UnidadeMedicaoCod, P00RO2_A1779AutorizacaoConsumo_Quantidade, P00RO2_A1778AutorizacaoConsumo_UnidadeMedicaoNom, P00RO2_n1778AutorizacaoConsumo_UnidadeMedicaoNom, P00RO2_A1776AutorizacaoConsumo_VigenciaFim, P00RO2_A82Contrato_DataVigenciaInicio, P00RO2_A83Contrato_DataVigenciaTermino, P00RO2_A74Contrato_Codigo, P00RO2_A1774AutorizacaoConsumo_Codigo, P00RO2_A1775AutorizacaoConsumo_VigenciaInicio
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV26TFAutorizacaoConsumo_Quantidade ;
      private short AV27TFAutorizacaoConsumo_Quantidade_To ;
      private short A1779AutorizacaoConsumo_Quantidade ;
      private int AV59GXV1 ;
      private int AV10TFAutorizacaoConsumo_Codigo ;
      private int AV11TFAutorizacaoConsumo_Codigo_To ;
      private int AV12TFContrato_Codigo ;
      private int AV13TFContrato_Codigo_To ;
      private int AV22TFAutorizacaoConsumo_UnidadeMedicaoCod ;
      private int AV23TFAutorizacaoConsumo_UnidadeMedicaoCod_To ;
      private int A1774AutorizacaoConsumo_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int AV31InsertIndex ;
      private long AV40count ;
      private String AV24TFAutorizacaoConsumo_UnidadeMedicaoNom ;
      private String AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ;
      private String scmdbuf ;
      private String lV24TFAutorizacaoConsumo_UnidadeMedicaoNom ;
      private String A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private DateTime AV14TFContrato_DataVigenciaTermino ;
      private DateTime AV15TFContrato_DataVigenciaTermino_To ;
      private DateTime AV16TFContrato_DataVigenciaInicio ;
      private DateTime AV17TFContrato_DataVigenciaInicio_To ;
      private DateTime AV18TFAutorizacaoConsumo_VigenciaInicio ;
      private DateTime AV19TFAutorizacaoConsumo_VigenciaInicio_To ;
      private DateTime AV20TFAutorizacaoConsumo_VigenciaFim ;
      private DateTime AV21TFAutorizacaoConsumo_VigenciaFim_To ;
      private DateTime AV47AutorizacaoConsumo_VigenciaInicio1 ;
      private DateTime AV48AutorizacaoConsumo_VigenciaInicio_To1 ;
      private DateTime AV51AutorizacaoConsumo_VigenciaInicio2 ;
      private DateTime AV52AutorizacaoConsumo_VigenciaInicio_To2 ;
      private DateTime AV55AutorizacaoConsumo_VigenciaInicio3 ;
      private DateTime AV56AutorizacaoConsumo_VigenciaInicio_To3 ;
      private DateTime A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A1776AutorizacaoConsumo_VigenciaFim ;
      private bool returnInSub ;
      private bool AV49DynamicFiltersEnabled2 ;
      private bool AV53DynamicFiltersEnabled3 ;
      private bool BRKRO2 ;
      private bool n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String AV39OptionIndexesJson ;
      private String AV34OptionsJson ;
      private String AV37OptionsDescJson ;
      private String AV30DDOName ;
      private String AV28SearchTxt ;
      private String AV29SearchTxtTo ;
      private String AV46DynamicFiltersSelector1 ;
      private String AV50DynamicFiltersSelector2 ;
      private String AV54DynamicFiltersSelector3 ;
      private String AV32Option ;
      private IGxSession AV41Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00RO2_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private short[] P00RO2_A1779AutorizacaoConsumo_Quantidade ;
      private String[] P00RO2_A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool[] P00RO2_n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private DateTime[] P00RO2_A1776AutorizacaoConsumo_VigenciaFim ;
      private DateTime[] P00RO2_A82Contrato_DataVigenciaInicio ;
      private DateTime[] P00RO2_A83Contrato_DataVigenciaTermino ;
      private int[] P00RO2_A74Contrato_Codigo ;
      private int[] P00RO2_A1774AutorizacaoConsumo_Codigo ;
      private DateTime[] P00RO2_A1775AutorizacaoConsumo_VigenciaInicio ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV38OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV43GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV44GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV45GridStateDynamicFilter ;
   }

   public class getpromptautorizacaoconsumofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00RO2( IGxContext context ,
                                             String AV46DynamicFiltersSelector1 ,
                                             DateTime AV47AutorizacaoConsumo_VigenciaInicio1 ,
                                             DateTime AV48AutorizacaoConsumo_VigenciaInicio_To1 ,
                                             bool AV49DynamicFiltersEnabled2 ,
                                             String AV50DynamicFiltersSelector2 ,
                                             DateTime AV51AutorizacaoConsumo_VigenciaInicio2 ,
                                             DateTime AV52AutorizacaoConsumo_VigenciaInicio_To2 ,
                                             bool AV53DynamicFiltersEnabled3 ,
                                             String AV54DynamicFiltersSelector3 ,
                                             DateTime AV55AutorizacaoConsumo_VigenciaInicio3 ,
                                             DateTime AV56AutorizacaoConsumo_VigenciaInicio_To3 ,
                                             int AV10TFAutorizacaoConsumo_Codigo ,
                                             int AV11TFAutorizacaoConsumo_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             DateTime AV14TFContrato_DataVigenciaTermino ,
                                             DateTime AV15TFContrato_DataVigenciaTermino_To ,
                                             DateTime AV16TFContrato_DataVigenciaInicio ,
                                             DateTime AV17TFContrato_DataVigenciaInicio_To ,
                                             DateTime AV18TFAutorizacaoConsumo_VigenciaInicio ,
                                             DateTime AV19TFAutorizacaoConsumo_VigenciaInicio_To ,
                                             DateTime AV20TFAutorizacaoConsumo_VigenciaFim ,
                                             DateTime AV21TFAutorizacaoConsumo_VigenciaFim_To ,
                                             int AV22TFAutorizacaoConsumo_UnidadeMedicaoCod ,
                                             int AV23TFAutorizacaoConsumo_UnidadeMedicaoCod_To ,
                                             String AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ,
                                             String AV24TFAutorizacaoConsumo_UnidadeMedicaoNom ,
                                             short AV26TFAutorizacaoConsumo_Quantidade ,
                                             short AV27TFAutorizacaoConsumo_Quantidade_To ,
                                             DateTime A1775AutorizacaoConsumo_VigenciaInicio ,
                                             int A1774AutorizacaoConsumo_Codigo ,
                                             int A74Contrato_Codigo ,
                                             DateTime A83Contrato_DataVigenciaTermino ,
                                             DateTime A82Contrato_DataVigenciaInicio ,
                                             DateTime A1776AutorizacaoConsumo_VigenciaFim ,
                                             int A1777AutorizacaoConsumo_UnidadeMedicaoCod ,
                                             String A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                             short A1779AutorizacaoConsumo_Quantidade )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [24] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[AutorizacaoConsumo_UnidadeMedicaoCod] AS AutorizacaoConsumo_UnidadeMedicaoCod, T1.[AutorizacaoConsumo_Quantidade], T2.[UnidadeMedicao_Nome] AS AutorizacaoConsumo_UnidadeMedicaoNom, T1.[AutorizacaoConsumo_VigenciaFim], T3.[Contrato_DataVigenciaInicio], T3.[Contrato_DataVigenciaTermino], T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_Codigo], T1.[AutorizacaoConsumo_VigenciaInicio] FROM (([AutorizacaoConsumo] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[AutorizacaoConsumo_UnidadeMedicaoCod]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV47AutorizacaoConsumo_VigenciaInicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV47AutorizacaoConsumo_VigenciaInicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV47AutorizacaoConsumo_VigenciaInicio1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV48AutorizacaoConsumo_VigenciaInicio_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV48AutorizacaoConsumo_VigenciaInicio_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV48AutorizacaoConsumo_VigenciaInicio_To1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV51AutorizacaoConsumo_VigenciaInicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV51AutorizacaoConsumo_VigenciaInicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV51AutorizacaoConsumo_VigenciaInicio2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV52AutorizacaoConsumo_VigenciaInicio_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV52AutorizacaoConsumo_VigenciaInicio_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV52AutorizacaoConsumo_VigenciaInicio_To2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV53DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV55AutorizacaoConsumo_VigenciaInicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV55AutorizacaoConsumo_VigenciaInicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV55AutorizacaoConsumo_VigenciaInicio3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV53DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV56AutorizacaoConsumo_VigenciaInicio_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV56AutorizacaoConsumo_VigenciaInicio_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV56AutorizacaoConsumo_VigenciaInicio_To3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV10TFAutorizacaoConsumo_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] >= @AV10TFAutorizacaoConsumo_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Codigo] >= @AV10TFAutorizacaoConsumo_Codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV11TFAutorizacaoConsumo_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] <= @AV11TFAutorizacaoConsumo_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Codigo] <= @AV11TFAutorizacaoConsumo_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContrato_DataVigenciaTermino) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_DataVigenciaTermino] >= @AV14TFContrato_DataVigenciaTermino)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_DataVigenciaTermino] >= @AV14TFContrato_DataVigenciaTermino)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContrato_DataVigenciaTermino_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_DataVigenciaTermino] <= @AV15TFContrato_DataVigenciaTermino_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_DataVigenciaTermino] <= @AV15TFContrato_DataVigenciaTermino_To)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV16TFContrato_DataVigenciaInicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_DataVigenciaInicio] >= @AV16TFContrato_DataVigenciaInicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_DataVigenciaInicio] >= @AV16TFContrato_DataVigenciaInicio)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV17TFContrato_DataVigenciaInicio_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_DataVigenciaInicio] <= @AV17TFContrato_DataVigenciaInicio_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_DataVigenciaInicio] <= @AV17TFContrato_DataVigenciaInicio_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFAutorizacaoConsumo_VigenciaInicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV18TFAutorizacaoConsumo_VigenciaInicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV18TFAutorizacaoConsumo_VigenciaInicio)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFAutorizacaoConsumo_VigenciaInicio_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV19TFAutorizacaoConsumo_VigenciaInicio_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV19TFAutorizacaoConsumo_VigenciaInicio_To)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV20TFAutorizacaoConsumo_VigenciaFim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] >= @AV20TFAutorizacaoConsumo_VigenciaFim)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaFim] >= @AV20TFAutorizacaoConsumo_VigenciaFim)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV21TFAutorizacaoConsumo_VigenciaFim_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] <= @AV21TFAutorizacaoConsumo_VigenciaFim_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaFim] <= @AV21TFAutorizacaoConsumo_VigenciaFim_To)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (0==AV22TFAutorizacaoConsumo_UnidadeMedicaoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_UnidadeMedicaoCod] >= @AV22TFAutorizacaoConsumo_UnidadeMedicaoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_UnidadeMedicaoCod] >= @AV22TFAutorizacaoConsumo_UnidadeMedicaoCod)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (0==AV23TFAutorizacaoConsumo_UnidadeMedicaoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_UnidadeMedicaoCod] <= @AV23TFAutorizacaoConsumo_UnidadeMedicaoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_UnidadeMedicaoCod] <= @AV23TFAutorizacaoConsumo_UnidadeMedicaoCod_To)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFAutorizacaoConsumo_UnidadeMedicaoNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV24TFAutorizacaoConsumo_UnidadeMedicaoNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] like @lV24TFAutorizacaoConsumo_UnidadeMedicaoNom)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] = @AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (0==AV26TFAutorizacaoConsumo_Quantidade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] >= @AV26TFAutorizacaoConsumo_Quantidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Quantidade] >= @AV26TFAutorizacaoConsumo_Quantidade)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (0==AV27TFAutorizacaoConsumo_Quantidade_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] <= @AV27TFAutorizacaoConsumo_Quantidade_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Quantidade] <= @AV27TFAutorizacaoConsumo_Quantidade_To)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[AutorizacaoConsumo_UnidadeMedicaoCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00RO2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (DateTime)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (int)dynConstraints[35] , (String)dynConstraints[36] , (short)dynConstraints[37] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00RO2 ;
          prmP00RO2 = new Object[] {
          new Object[] {"@AV47AutorizacaoConsumo_VigenciaInicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48AutorizacaoConsumo_VigenciaInicio_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV51AutorizacaoConsumo_VigenciaInicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV52AutorizacaoConsumo_VigenciaInicio_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55AutorizacaoConsumo_VigenciaInicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56AutorizacaoConsumo_VigenciaInicio_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10TFAutorizacaoConsumo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFAutorizacaoConsumo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContrato_DataVigenciaTermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFContrato_DataVigenciaTermino_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV16TFContrato_DataVigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17TFContrato_DataVigenciaInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV18TFAutorizacaoConsumo_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV19TFAutorizacaoConsumo_VigenciaInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20TFAutorizacaoConsumo_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21TFAutorizacaoConsumo_VigenciaFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV22TFAutorizacaoConsumo_UnidadeMedicaoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23TFAutorizacaoConsumo_UnidadeMedicaoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV24TFAutorizacaoConsumo_UnidadeMedicaoNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV25TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV26TFAutorizacaoConsumo_Quantidade",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV27TFAutorizacaoConsumo_Quantidade_To",SqlDbType.SmallInt,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00RO2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00RO2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[47]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptautorizacaoconsumofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptautorizacaoconsumofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptautorizacaoconsumofilterdata") )
          {
             return  ;
          }
          getpromptautorizacaoconsumofilterdata worker = new getpromptautorizacaoconsumofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
