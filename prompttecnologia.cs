/*
               File: PromptTecnologia
        Description: Selecione Tecnologia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:14:3.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prompttecnologia : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public prompttecnologia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prompttecnologia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutTecnologia_Codigo ,
                           ref String aP1_InOutTecnologia_Nome )
      {
         this.AV7InOutTecnologia_Codigo = aP0_InOutTecnologia_Codigo;
         this.AV8InOutTecnologia_Nome = aP1_InOutTecnologia_Nome;
         executePrivate();
         aP0_InOutTecnologia_Codigo=this.AV7InOutTecnologia_Codigo;
         aP1_InOutTecnologia_Nome=this.AV8InOutTecnologia_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavTecnologia_tipotecnologia1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavTecnologia_tipotecnologia2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbavTecnologia_tipotecnologia3 = new GXCombobox();
         cmbTecnologia_TipoTecnologia = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_83 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_83_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_83_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Tecnologia_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
               AV56Tecnologia_TipoTecnologia1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Tecnologia_TipoTecnologia1", AV56Tecnologia_TipoTecnologia1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV22Tecnologia_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Tecnologia_Nome2", AV22Tecnologia_Nome2);
               AV57Tecnologia_TipoTecnologia2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Tecnologia_TipoTecnologia2", AV57Tecnologia_TipoTecnologia2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV27Tecnologia_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Tecnologia_Nome3", AV27Tecnologia_Nome3);
               AV58Tecnologia_TipoTecnologia3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Tecnologia_TipoTecnologia3", AV58Tecnologia_TipoTecnologia3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV60TFTecnologia_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFTecnologia_Nome", AV60TFTecnologia_Nome);
               AV61TFTecnologia_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFTecnologia_Nome_Sel", AV61TFTecnologia_Nome_Sel);
               AV62ddo_Tecnologia_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Tecnologia_NomeTitleControlIdToReplace", AV62ddo_Tecnologia_NomeTitleControlIdToReplace);
               AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace", AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV65TFTecnologia_TipoTecnologia_Sels);
               AV74Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tecnologia_Nome1, AV56Tecnologia_TipoTecnologia1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Tecnologia_Nome2, AV57Tecnologia_TipoTecnologia2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Tecnologia_Nome3, AV58Tecnologia_TipoTecnologia3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV60TFTecnologia_Nome, AV61TFTecnologia_Nome_Sel, AV62ddo_Tecnologia_NomeTitleControlIdToReplace, AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV65TFTecnologia_TipoTecnologia_Sels, AV74Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutTecnologia_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutTecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutTecnologia_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutTecnologia_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutTecnologia_Nome", AV8InOutTecnologia_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA4O2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV74Pgmname = "PromptTecnologia";
               context.Gx_err = 0;
               WS4O2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE4O2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282314369");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("prompttecnologia.aspx") + "?" + UrlEncode("" +AV7InOutTecnologia_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutTecnologia_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTECNOLOGIA_NOME1", StringUtil.RTrim( AV17Tecnologia_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vTECNOLOGIA_TIPOTECNOLOGIA1", StringUtil.RTrim( AV56Tecnologia_TipoTecnologia1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTECNOLOGIA_NOME2", StringUtil.RTrim( AV22Tecnologia_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vTECNOLOGIA_TIPOTECNOLOGIA2", StringUtil.RTrim( AV57Tecnologia_TipoTecnologia2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTECNOLOGIA_NOME3", StringUtil.RTrim( AV27Tecnologia_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vTECNOLOGIA_TIPOTECNOLOGIA3", StringUtil.RTrim( AV58Tecnologia_TipoTecnologia3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTECNOLOGIA_NOME", StringUtil.RTrim( AV60TFTecnologia_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTECNOLOGIA_NOME_SEL", StringUtil.RTrim( AV61TFTecnologia_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_83", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_83), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV67DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV67DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTECNOLOGIA_NOMETITLEFILTERDATA", AV59Tecnologia_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTECNOLOGIA_NOMETITLEFILTERDATA", AV59Tecnologia_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTECNOLOGIA_TIPOTECNOLOGIATITLEFILTERDATA", AV63Tecnologia_TipoTecnologiaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTECNOLOGIA_TIPOTECNOLOGIATITLEFILTERDATA", AV63Tecnologia_TipoTecnologiaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS", AV65TFTecnologia_TipoTecnologia_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS", AV65TFTecnologia_TipoTecnologia_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV74Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTTECNOLOGIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutTecnologia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTTECNOLOGIA_NOME", StringUtil.RTrim( AV8InOutTecnologia_Nome));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Caption", StringUtil.RTrim( Ddo_tecnologia_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Tooltip", StringUtil.RTrim( Ddo_tecnologia_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Cls", StringUtil.RTrim( Ddo_tecnologia_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_tecnologia_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_tecnologia_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_tecnologia_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tecnologia_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_tecnologia_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_tecnologia_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Sortedstatus", StringUtil.RTrim( Ddo_tecnologia_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Includefilter", StringUtil.BoolToStr( Ddo_tecnologia_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Filtertype", StringUtil.RTrim( Ddo_tecnologia_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_tecnologia_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_tecnologia_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Datalisttype", StringUtil.RTrim( Ddo_tecnologia_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_tecnologia_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Datalistproc", StringUtil.RTrim( Ddo_tecnologia_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tecnologia_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Sortasc", StringUtil.RTrim( Ddo_tecnologia_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Sortdsc", StringUtil.RTrim( Ddo_tecnologia_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Loadingdata", StringUtil.RTrim( Ddo_tecnologia_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Cleanfilter", StringUtil.RTrim( Ddo_tecnologia_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_tecnologia_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Rangefilterto", StringUtil.RTrim( Ddo_tecnologia_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Noresultsfound", StringUtil.RTrim( Ddo_tecnologia_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_tecnologia_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Caption", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Tooltip", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Cls", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Selectedvalue_set", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includesortasc", StringUtil.BoolToStr( Ddo_tecnologia_tipotecnologia_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includesortdsc", StringUtil.BoolToStr( Ddo_tecnologia_tipotecnologia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortedstatus", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includefilter", StringUtil.BoolToStr( Ddo_tecnologia_tipotecnologia_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Filterisrange", StringUtil.BoolToStr( Ddo_tecnologia_tipotecnologia_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includedatalist", StringUtil.BoolToStr( Ddo_tecnologia_tipotecnologia_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Datalisttype", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Allowmultipleselection", StringUtil.BoolToStr( Ddo_tecnologia_tipotecnologia_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Datalistfixedvalues", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tecnologia_tipotecnologia_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortasc", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortdsc", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Loadingdata", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Cleanfilter", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Rangefilterfrom", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Rangefilterto", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Noresultsfound", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Searchbuttontext", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Activeeventkey", StringUtil.RTrim( Ddo_tecnologia_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_tecnologia_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_tecnologia_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Activeeventkey", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Selectedvalue_get", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm4O2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptTecnologia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Tecnologia" ;
      }

      protected void WB4O0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_4O2( true) ;
         }
         else
         {
            wb_table1_2_4O2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_4O2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(92, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(93, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftecnologia_nome_Internalname, StringUtil.RTrim( AV60TFTecnologia_Nome), StringUtil.RTrim( context.localUtil.Format( AV60TFTecnologia_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftecnologia_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTftecnologia_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptTecnologia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftecnologia_nome_sel_Internalname, StringUtil.RTrim( AV61TFTecnologia_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV61TFTecnologia_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftecnologia_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftecnologia_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptTecnologia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TECNOLOGIA_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tecnologia_nometitlecontrolidtoreplace_Internalname, AV62ddo_Tecnologia_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"", 0, edtavDdo_tecnologia_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptTecnologia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TECNOLOGIA_TIPOTECNOLOGIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Internalname, AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", 0, edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptTecnologia.htm");
         }
         wbLoad = true;
      }

      protected void START4O2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Tecnologia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP4O0( ) ;
      }

      protected void WS4O2( )
      {
         START4O2( ) ;
         EVT4O2( ) ;
      }

      protected void EVT4O2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114O2 */
                           E114O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_TECNOLOGIA_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124O2 */
                           E124O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_TECNOLOGIA_TIPOTECNOLOGIA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E134O2 */
                           E134O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E144O2 */
                           E144O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E154O2 */
                           E154O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E164O2 */
                           E164O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E174O2 */
                           E174O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E184O2 */
                           E184O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E194O2 */
                           E194O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E204O2 */
                           E204O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E214O2 */
                           E214O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E224O2 */
                           E224O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E234O2 */
                           E234O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_83_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
                           SubsflControlProps_832( ) ;
                           AV31Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV73Select_GXI : context.convertURL( context.PathToRelativeUrl( AV31Select))));
                           A131Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTecnologia_Codigo_Internalname), ",", "."));
                           A132Tecnologia_Nome = StringUtil.Upper( cgiGet( edtTecnologia_Nome_Internalname));
                           cmbTecnologia_TipoTecnologia.Name = cmbTecnologia_TipoTecnologia_Internalname;
                           cmbTecnologia_TipoTecnologia.CurrentValue = cgiGet( cmbTecnologia_TipoTecnologia_Internalname);
                           A355Tecnologia_TipoTecnologia = cgiGet( cmbTecnologia_TipoTecnologia_Internalname);
                           n355Tecnologia_TipoTecnologia = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E244O2 */
                                 E244O2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E254O2 */
                                 E254O2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E264O2 */
                                 E264O2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tecnologia_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_NOME1"), AV17Tecnologia_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tecnologia_tipotecnologia1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_TIPOTECNOLOGIA1"), AV56Tecnologia_TipoTecnologia1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tecnologia_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_NOME2"), AV22Tecnologia_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tecnologia_tipotecnologia2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_TIPOTECNOLOGIA2"), AV57Tecnologia_TipoTecnologia2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tecnologia_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_NOME3"), AV27Tecnologia_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tecnologia_tipotecnologia3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_TIPOTECNOLOGIA3"), AV58Tecnologia_TipoTecnologia3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tftecnologia_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTECNOLOGIA_NOME"), AV60TFTecnologia_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tftecnologia_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTECNOLOGIA_NOME_SEL"), AV61TFTecnologia_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E274O2 */
                                       E274O2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE4O2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm4O2( ) ;
            }
         }
      }

      protected void PA4O2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("TECNOLOGIA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("TECNOLOGIA_TIPOTECNOLOGIA", "Tipo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavTecnologia_tipotecnologia1.Name = "vTECNOLOGIA_TIPOTECNOLOGIA1";
            cmbavTecnologia_tipotecnologia1.WebTags = "";
            cmbavTecnologia_tipotecnologia1.addItem("", "Todos", 0);
            cmbavTecnologia_tipotecnologia1.addItem("OS", "Sistema Operacional", 0);
            cmbavTecnologia_tipotecnologia1.addItem("LNG", "Linguagem", 0);
            cmbavTecnologia_tipotecnologia1.addItem("DBM", "Banco de Dados", 0);
            cmbavTecnologia_tipotecnologia1.addItem("SFT", "Software", 0);
            cmbavTecnologia_tipotecnologia1.addItem("SRV", "Servidor", 0);
            cmbavTecnologia_tipotecnologia1.addItem("DSK", "Desktop", 0);
            cmbavTecnologia_tipotecnologia1.addItem("NTB", "Notebook", 0);
            cmbavTecnologia_tipotecnologia1.addItem("PRN", "Impresora", 0);
            cmbavTecnologia_tipotecnologia1.addItem("HRD", "Hardware", 0);
            if ( cmbavTecnologia_tipotecnologia1.ItemCount > 0 )
            {
               AV56Tecnologia_TipoTecnologia1 = cmbavTecnologia_tipotecnologia1.getValidValue(AV56Tecnologia_TipoTecnologia1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Tecnologia_TipoTecnologia1", AV56Tecnologia_TipoTecnologia1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("TECNOLOGIA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("TECNOLOGIA_TIPOTECNOLOGIA", "Tipo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavTecnologia_tipotecnologia2.Name = "vTECNOLOGIA_TIPOTECNOLOGIA2";
            cmbavTecnologia_tipotecnologia2.WebTags = "";
            cmbavTecnologia_tipotecnologia2.addItem("", "Todos", 0);
            cmbavTecnologia_tipotecnologia2.addItem("OS", "Sistema Operacional", 0);
            cmbavTecnologia_tipotecnologia2.addItem("LNG", "Linguagem", 0);
            cmbavTecnologia_tipotecnologia2.addItem("DBM", "Banco de Dados", 0);
            cmbavTecnologia_tipotecnologia2.addItem("SFT", "Software", 0);
            cmbavTecnologia_tipotecnologia2.addItem("SRV", "Servidor", 0);
            cmbavTecnologia_tipotecnologia2.addItem("DSK", "Desktop", 0);
            cmbavTecnologia_tipotecnologia2.addItem("NTB", "Notebook", 0);
            cmbavTecnologia_tipotecnologia2.addItem("PRN", "Impresora", 0);
            cmbavTecnologia_tipotecnologia2.addItem("HRD", "Hardware", 0);
            if ( cmbavTecnologia_tipotecnologia2.ItemCount > 0 )
            {
               AV57Tecnologia_TipoTecnologia2 = cmbavTecnologia_tipotecnologia2.getValidValue(AV57Tecnologia_TipoTecnologia2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Tecnologia_TipoTecnologia2", AV57Tecnologia_TipoTecnologia2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("TECNOLOGIA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("TECNOLOGIA_TIPOTECNOLOGIA", "Tipo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            cmbavTecnologia_tipotecnologia3.Name = "vTECNOLOGIA_TIPOTECNOLOGIA3";
            cmbavTecnologia_tipotecnologia3.WebTags = "";
            cmbavTecnologia_tipotecnologia3.addItem("", "Todos", 0);
            cmbavTecnologia_tipotecnologia3.addItem("OS", "Sistema Operacional", 0);
            cmbavTecnologia_tipotecnologia3.addItem("LNG", "Linguagem", 0);
            cmbavTecnologia_tipotecnologia3.addItem("DBM", "Banco de Dados", 0);
            cmbavTecnologia_tipotecnologia3.addItem("SFT", "Software", 0);
            cmbavTecnologia_tipotecnologia3.addItem("SRV", "Servidor", 0);
            cmbavTecnologia_tipotecnologia3.addItem("DSK", "Desktop", 0);
            cmbavTecnologia_tipotecnologia3.addItem("NTB", "Notebook", 0);
            cmbavTecnologia_tipotecnologia3.addItem("PRN", "Impresora", 0);
            cmbavTecnologia_tipotecnologia3.addItem("HRD", "Hardware", 0);
            if ( cmbavTecnologia_tipotecnologia3.ItemCount > 0 )
            {
               AV58Tecnologia_TipoTecnologia3 = cmbavTecnologia_tipotecnologia3.getValidValue(AV58Tecnologia_TipoTecnologia3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Tecnologia_TipoTecnologia3", AV58Tecnologia_TipoTecnologia3);
            }
            GXCCtl = "TECNOLOGIA_TIPOTECNOLOGIA_" + sGXsfl_83_idx;
            cmbTecnologia_TipoTecnologia.Name = GXCCtl;
            cmbTecnologia_TipoTecnologia.WebTags = "";
            cmbTecnologia_TipoTecnologia.addItem("", "(Nenhum)", 0);
            cmbTecnologia_TipoTecnologia.addItem("OS", "Sistema Operacional", 0);
            cmbTecnologia_TipoTecnologia.addItem("LNG", "Linguagem", 0);
            cmbTecnologia_TipoTecnologia.addItem("DBM", "Banco de Dados", 0);
            cmbTecnologia_TipoTecnologia.addItem("SFT", "Software", 0);
            cmbTecnologia_TipoTecnologia.addItem("SRV", "Servidor", 0);
            cmbTecnologia_TipoTecnologia.addItem("DSK", "Desktop", 0);
            cmbTecnologia_TipoTecnologia.addItem("NTB", "Notebook", 0);
            cmbTecnologia_TipoTecnologia.addItem("PRN", "Impresora", 0);
            cmbTecnologia_TipoTecnologia.addItem("HRD", "Hardware", 0);
            if ( cmbTecnologia_TipoTecnologia.ItemCount > 0 )
            {
               A355Tecnologia_TipoTecnologia = cmbTecnologia_TipoTecnologia.getValidValue(A355Tecnologia_TipoTecnologia);
               n355Tecnologia_TipoTecnologia = false;
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_832( ) ;
         while ( nGXsfl_83_idx <= nRC_GXsfl_83 )
         {
            sendrow_832( ) ;
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Tecnologia_Nome1 ,
                                       String AV56Tecnologia_TipoTecnologia1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22Tecnologia_Nome2 ,
                                       String AV57Tecnologia_TipoTecnologia2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       String AV27Tecnologia_Nome3 ,
                                       String AV58Tecnologia_TipoTecnologia3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       String AV60TFTecnologia_Nome ,
                                       String AV61TFTecnologia_Nome_Sel ,
                                       String AV62ddo_Tecnologia_NomeTitleControlIdToReplace ,
                                       String AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace ,
                                       IGxCollection AV65TFTecnologia_TipoTecnologia_Sels ,
                                       String AV74Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF4O2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TECNOLOGIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A131Tecnologia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A132Tecnologia_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "TECNOLOGIA_NOME", StringUtil.RTrim( A132Tecnologia_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_TIPOTECNOLOGIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A355Tecnologia_TipoTecnologia, ""))));
         GxWebStd.gx_hidden_field( context, "TECNOLOGIA_TIPOTECNOLOGIA", StringUtil.RTrim( A355Tecnologia_TipoTecnologia));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavTecnologia_tipotecnologia1.ItemCount > 0 )
         {
            AV56Tecnologia_TipoTecnologia1 = cmbavTecnologia_tipotecnologia1.getValidValue(AV56Tecnologia_TipoTecnologia1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Tecnologia_TipoTecnologia1", AV56Tecnologia_TipoTecnologia1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavTecnologia_tipotecnologia2.ItemCount > 0 )
         {
            AV57Tecnologia_TipoTecnologia2 = cmbavTecnologia_tipotecnologia2.getValidValue(AV57Tecnologia_TipoTecnologia2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Tecnologia_TipoTecnologia2", AV57Tecnologia_TipoTecnologia2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
         if ( cmbavTecnologia_tipotecnologia3.ItemCount > 0 )
         {
            AV58Tecnologia_TipoTecnologia3 = cmbavTecnologia_tipotecnologia3.getValidValue(AV58Tecnologia_TipoTecnologia3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Tecnologia_TipoTecnologia3", AV58Tecnologia_TipoTecnologia3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF4O2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV74Pgmname = "PromptTecnologia";
         context.Gx_err = 0;
      }

      protected void RF4O2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 83;
         /* Execute user event: E254O2 */
         E254O2 ();
         nGXsfl_83_idx = 1;
         sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
         SubsflControlProps_832( ) ;
         nGXsfl_83_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_832( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A355Tecnologia_TipoTecnologia ,
                                                 AV65TFTecnologia_TipoTecnologia_Sels ,
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17Tecnologia_Nome1 ,
                                                 AV56Tecnologia_TipoTecnologia1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV22Tecnologia_Nome2 ,
                                                 AV57Tecnologia_TipoTecnologia2 ,
                                                 AV24DynamicFiltersEnabled3 ,
                                                 AV25DynamicFiltersSelector3 ,
                                                 AV26DynamicFiltersOperator3 ,
                                                 AV27Tecnologia_Nome3 ,
                                                 AV58Tecnologia_TipoTecnologia3 ,
                                                 AV61TFTecnologia_Nome_Sel ,
                                                 AV60TFTecnologia_Nome ,
                                                 AV65TFTecnologia_TipoTecnologia_Sels.Count ,
                                                 A132Tecnologia_Nome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17Tecnologia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17Tecnologia_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
            lV17Tecnologia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17Tecnologia_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
            lV22Tecnologia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Tecnologia_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Tecnologia_Nome2", AV22Tecnologia_Nome2);
            lV22Tecnologia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Tecnologia_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Tecnologia_Nome2", AV22Tecnologia_Nome2);
            lV27Tecnologia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27Tecnologia_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Tecnologia_Nome3", AV27Tecnologia_Nome3);
            lV27Tecnologia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27Tecnologia_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Tecnologia_Nome3", AV27Tecnologia_Nome3);
            lV60TFTecnologia_Nome = StringUtil.PadR( StringUtil.RTrim( AV60TFTecnologia_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFTecnologia_Nome", AV60TFTecnologia_Nome);
            /* Using cursor H004O2 */
            pr_default.execute(0, new Object[] {lV17Tecnologia_Nome1, lV17Tecnologia_Nome1, AV56Tecnologia_TipoTecnologia1, lV22Tecnologia_Nome2, lV22Tecnologia_Nome2, AV57Tecnologia_TipoTecnologia2, lV27Tecnologia_Nome3, lV27Tecnologia_Nome3, AV58Tecnologia_TipoTecnologia3, lV60TFTecnologia_Nome, AV61TFTecnologia_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_83_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A355Tecnologia_TipoTecnologia = H004O2_A355Tecnologia_TipoTecnologia[0];
               n355Tecnologia_TipoTecnologia = H004O2_n355Tecnologia_TipoTecnologia[0];
               A132Tecnologia_Nome = H004O2_A132Tecnologia_Nome[0];
               A131Tecnologia_Codigo = H004O2_A131Tecnologia_Codigo[0];
               /* Execute user event: E264O2 */
               E264O2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 83;
            WB4O0( ) ;
         }
         nGXsfl_83_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A355Tecnologia_TipoTecnologia ,
                                              AV65TFTecnologia_TipoTecnologia_Sels ,
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17Tecnologia_Nome1 ,
                                              AV56Tecnologia_TipoTecnologia1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV22Tecnologia_Nome2 ,
                                              AV57Tecnologia_TipoTecnologia2 ,
                                              AV24DynamicFiltersEnabled3 ,
                                              AV25DynamicFiltersSelector3 ,
                                              AV26DynamicFiltersOperator3 ,
                                              AV27Tecnologia_Nome3 ,
                                              AV58Tecnologia_TipoTecnologia3 ,
                                              AV61TFTecnologia_Nome_Sel ,
                                              AV60TFTecnologia_Nome ,
                                              AV65TFTecnologia_TipoTecnologia_Sels.Count ,
                                              A132Tecnologia_Nome ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV17Tecnologia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17Tecnologia_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
         lV17Tecnologia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17Tecnologia_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
         lV22Tecnologia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Tecnologia_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Tecnologia_Nome2", AV22Tecnologia_Nome2);
         lV22Tecnologia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Tecnologia_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Tecnologia_Nome2", AV22Tecnologia_Nome2);
         lV27Tecnologia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27Tecnologia_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Tecnologia_Nome3", AV27Tecnologia_Nome3);
         lV27Tecnologia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27Tecnologia_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Tecnologia_Nome3", AV27Tecnologia_Nome3);
         lV60TFTecnologia_Nome = StringUtil.PadR( StringUtil.RTrim( AV60TFTecnologia_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFTecnologia_Nome", AV60TFTecnologia_Nome);
         /* Using cursor H004O3 */
         pr_default.execute(1, new Object[] {lV17Tecnologia_Nome1, lV17Tecnologia_Nome1, AV56Tecnologia_TipoTecnologia1, lV22Tecnologia_Nome2, lV22Tecnologia_Nome2, AV57Tecnologia_TipoTecnologia2, lV27Tecnologia_Nome3, lV27Tecnologia_Nome3, AV58Tecnologia_TipoTecnologia3, lV60TFTecnologia_Nome, AV61TFTecnologia_Nome_Sel});
         GRID_nRecordCount = H004O3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tecnologia_Nome1, AV56Tecnologia_TipoTecnologia1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Tecnologia_Nome2, AV57Tecnologia_TipoTecnologia2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Tecnologia_Nome3, AV58Tecnologia_TipoTecnologia3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV60TFTecnologia_Nome, AV61TFTecnologia_Nome_Sel, AV62ddo_Tecnologia_NomeTitleControlIdToReplace, AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV65TFTecnologia_TipoTecnologia_Sels, AV74Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tecnologia_Nome1, AV56Tecnologia_TipoTecnologia1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Tecnologia_Nome2, AV57Tecnologia_TipoTecnologia2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Tecnologia_Nome3, AV58Tecnologia_TipoTecnologia3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV60TFTecnologia_Nome, AV61TFTecnologia_Nome_Sel, AV62ddo_Tecnologia_NomeTitleControlIdToReplace, AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV65TFTecnologia_TipoTecnologia_Sels, AV74Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tecnologia_Nome1, AV56Tecnologia_TipoTecnologia1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Tecnologia_Nome2, AV57Tecnologia_TipoTecnologia2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Tecnologia_Nome3, AV58Tecnologia_TipoTecnologia3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV60TFTecnologia_Nome, AV61TFTecnologia_Nome_Sel, AV62ddo_Tecnologia_NomeTitleControlIdToReplace, AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV65TFTecnologia_TipoTecnologia_Sels, AV74Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tecnologia_Nome1, AV56Tecnologia_TipoTecnologia1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Tecnologia_Nome2, AV57Tecnologia_TipoTecnologia2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Tecnologia_Nome3, AV58Tecnologia_TipoTecnologia3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV60TFTecnologia_Nome, AV61TFTecnologia_Nome_Sel, AV62ddo_Tecnologia_NomeTitleControlIdToReplace, AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV65TFTecnologia_TipoTecnologia_Sels, AV74Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tecnologia_Nome1, AV56Tecnologia_TipoTecnologia1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Tecnologia_Nome2, AV57Tecnologia_TipoTecnologia2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Tecnologia_Nome3, AV58Tecnologia_TipoTecnologia3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV60TFTecnologia_Nome, AV61TFTecnologia_Nome_Sel, AV62ddo_Tecnologia_NomeTitleControlIdToReplace, AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV65TFTecnologia_TipoTecnologia_Sels, AV74Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP4O0( )
      {
         /* Before Start, stand alone formulas. */
         AV74Pgmname = "PromptTecnologia";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E244O2 */
         E244O2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV67DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vTECNOLOGIA_NOMETITLEFILTERDATA"), AV59Tecnologia_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTECNOLOGIA_TIPOTECNOLOGIATITLEFILTERDATA"), AV63Tecnologia_TipoTecnologiaTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Tecnologia_Nome1 = StringUtil.Upper( cgiGet( edtavTecnologia_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
            cmbavTecnologia_tipotecnologia1.Name = cmbavTecnologia_tipotecnologia1_Internalname;
            cmbavTecnologia_tipotecnologia1.CurrentValue = cgiGet( cmbavTecnologia_tipotecnologia1_Internalname);
            AV56Tecnologia_TipoTecnologia1 = cgiGet( cmbavTecnologia_tipotecnologia1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Tecnologia_TipoTecnologia1", AV56Tecnologia_TipoTecnologia1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22Tecnologia_Nome2 = StringUtil.Upper( cgiGet( edtavTecnologia_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Tecnologia_Nome2", AV22Tecnologia_Nome2);
            cmbavTecnologia_tipotecnologia2.Name = cmbavTecnologia_tipotecnologia2_Internalname;
            cmbavTecnologia_tipotecnologia2.CurrentValue = cgiGet( cmbavTecnologia_tipotecnologia2_Internalname);
            AV57Tecnologia_TipoTecnologia2 = cgiGet( cmbavTecnologia_tipotecnologia2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Tecnologia_TipoTecnologia2", AV57Tecnologia_TipoTecnologia2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            AV27Tecnologia_Nome3 = StringUtil.Upper( cgiGet( edtavTecnologia_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Tecnologia_Nome3", AV27Tecnologia_Nome3);
            cmbavTecnologia_tipotecnologia3.Name = cmbavTecnologia_tipotecnologia3_Internalname;
            cmbavTecnologia_tipotecnologia3.CurrentValue = cgiGet( cmbavTecnologia_tipotecnologia3_Internalname);
            AV58Tecnologia_TipoTecnologia3 = cgiGet( cmbavTecnologia_tipotecnologia3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Tecnologia_TipoTecnologia3", AV58Tecnologia_TipoTecnologia3);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            AV60TFTecnologia_Nome = StringUtil.Upper( cgiGet( edtavTftecnologia_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFTecnologia_Nome", AV60TFTecnologia_Nome);
            AV61TFTecnologia_Nome_Sel = StringUtil.Upper( cgiGet( edtavTftecnologia_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFTecnologia_Nome_Sel", AV61TFTecnologia_Nome_Sel);
            AV62ddo_Tecnologia_NomeTitleControlIdToReplace = cgiGet( edtavDdo_tecnologia_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Tecnologia_NomeTitleControlIdToReplace", AV62ddo_Tecnologia_NomeTitleControlIdToReplace);
            AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace = cgiGet( edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace", AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_83 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_83"), ",", "."));
            AV69GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV70GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_tecnologia_nome_Caption = cgiGet( "DDO_TECNOLOGIA_NOME_Caption");
            Ddo_tecnologia_nome_Tooltip = cgiGet( "DDO_TECNOLOGIA_NOME_Tooltip");
            Ddo_tecnologia_nome_Cls = cgiGet( "DDO_TECNOLOGIA_NOME_Cls");
            Ddo_tecnologia_nome_Filteredtext_set = cgiGet( "DDO_TECNOLOGIA_NOME_Filteredtext_set");
            Ddo_tecnologia_nome_Selectedvalue_set = cgiGet( "DDO_TECNOLOGIA_NOME_Selectedvalue_set");
            Ddo_tecnologia_nome_Dropdownoptionstype = cgiGet( "DDO_TECNOLOGIA_NOME_Dropdownoptionstype");
            Ddo_tecnologia_nome_Titlecontrolidtoreplace = cgiGet( "DDO_TECNOLOGIA_NOME_Titlecontrolidtoreplace");
            Ddo_tecnologia_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_NOME_Includesortasc"));
            Ddo_tecnologia_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_NOME_Includesortdsc"));
            Ddo_tecnologia_nome_Sortedstatus = cgiGet( "DDO_TECNOLOGIA_NOME_Sortedstatus");
            Ddo_tecnologia_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_NOME_Includefilter"));
            Ddo_tecnologia_nome_Filtertype = cgiGet( "DDO_TECNOLOGIA_NOME_Filtertype");
            Ddo_tecnologia_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_NOME_Filterisrange"));
            Ddo_tecnologia_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_NOME_Includedatalist"));
            Ddo_tecnologia_nome_Datalisttype = cgiGet( "DDO_TECNOLOGIA_NOME_Datalisttype");
            Ddo_tecnologia_nome_Datalistfixedvalues = cgiGet( "DDO_TECNOLOGIA_NOME_Datalistfixedvalues");
            Ddo_tecnologia_nome_Datalistproc = cgiGet( "DDO_TECNOLOGIA_NOME_Datalistproc");
            Ddo_tecnologia_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TECNOLOGIA_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tecnologia_nome_Sortasc = cgiGet( "DDO_TECNOLOGIA_NOME_Sortasc");
            Ddo_tecnologia_nome_Sortdsc = cgiGet( "DDO_TECNOLOGIA_NOME_Sortdsc");
            Ddo_tecnologia_nome_Loadingdata = cgiGet( "DDO_TECNOLOGIA_NOME_Loadingdata");
            Ddo_tecnologia_nome_Cleanfilter = cgiGet( "DDO_TECNOLOGIA_NOME_Cleanfilter");
            Ddo_tecnologia_nome_Rangefilterfrom = cgiGet( "DDO_TECNOLOGIA_NOME_Rangefilterfrom");
            Ddo_tecnologia_nome_Rangefilterto = cgiGet( "DDO_TECNOLOGIA_NOME_Rangefilterto");
            Ddo_tecnologia_nome_Noresultsfound = cgiGet( "DDO_TECNOLOGIA_NOME_Noresultsfound");
            Ddo_tecnologia_nome_Searchbuttontext = cgiGet( "DDO_TECNOLOGIA_NOME_Searchbuttontext");
            Ddo_tecnologia_tipotecnologia_Caption = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Caption");
            Ddo_tecnologia_tipotecnologia_Tooltip = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Tooltip");
            Ddo_tecnologia_tipotecnologia_Cls = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Cls");
            Ddo_tecnologia_tipotecnologia_Selectedvalue_set = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Selectedvalue_set");
            Ddo_tecnologia_tipotecnologia_Dropdownoptionstype = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Dropdownoptionstype");
            Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Titlecontrolidtoreplace");
            Ddo_tecnologia_tipotecnologia_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includesortasc"));
            Ddo_tecnologia_tipotecnologia_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includesortdsc"));
            Ddo_tecnologia_tipotecnologia_Sortedstatus = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortedstatus");
            Ddo_tecnologia_tipotecnologia_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includefilter"));
            Ddo_tecnologia_tipotecnologia_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Filterisrange"));
            Ddo_tecnologia_tipotecnologia_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includedatalist"));
            Ddo_tecnologia_tipotecnologia_Datalisttype = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Datalisttype");
            Ddo_tecnologia_tipotecnologia_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Allowmultipleselection"));
            Ddo_tecnologia_tipotecnologia_Datalistfixedvalues = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Datalistfixedvalues");
            Ddo_tecnologia_tipotecnologia_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tecnologia_tipotecnologia_Sortasc = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortasc");
            Ddo_tecnologia_tipotecnologia_Sortdsc = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortdsc");
            Ddo_tecnologia_tipotecnologia_Loadingdata = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Loadingdata");
            Ddo_tecnologia_tipotecnologia_Cleanfilter = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Cleanfilter");
            Ddo_tecnologia_tipotecnologia_Rangefilterfrom = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Rangefilterfrom");
            Ddo_tecnologia_tipotecnologia_Rangefilterto = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Rangefilterto");
            Ddo_tecnologia_tipotecnologia_Noresultsfound = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Noresultsfound");
            Ddo_tecnologia_tipotecnologia_Searchbuttontext = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_tecnologia_nome_Activeeventkey = cgiGet( "DDO_TECNOLOGIA_NOME_Activeeventkey");
            Ddo_tecnologia_nome_Filteredtext_get = cgiGet( "DDO_TECNOLOGIA_NOME_Filteredtext_get");
            Ddo_tecnologia_nome_Selectedvalue_get = cgiGet( "DDO_TECNOLOGIA_NOME_Selectedvalue_get");
            Ddo_tecnologia_tipotecnologia_Activeeventkey = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Activeeventkey");
            Ddo_tecnologia_tipotecnologia_Selectedvalue_get = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_NOME1"), AV17Tecnologia_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_TIPOTECNOLOGIA1"), AV56Tecnologia_TipoTecnologia1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_NOME2"), AV22Tecnologia_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_TIPOTECNOLOGIA2"), AV57Tecnologia_TipoTecnologia2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_NOME3"), AV27Tecnologia_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_TIPOTECNOLOGIA3"), AV58Tecnologia_TipoTecnologia3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTECNOLOGIA_NOME"), AV60TFTecnologia_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTECNOLOGIA_NOME_SEL"), AV61TFTecnologia_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E244O2 */
         E244O2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E244O2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV56Tecnologia_TipoTecnologia1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Tecnologia_TipoTecnologia1", AV56Tecnologia_TipoTecnologia1);
         AV15DynamicFiltersSelector1 = "TECNOLOGIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV57Tecnologia_TipoTecnologia2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Tecnologia_TipoTecnologia2", AV57Tecnologia_TipoTecnologia2);
         AV20DynamicFiltersSelector2 = "TECNOLOGIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV58Tecnologia_TipoTecnologia3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Tecnologia_TipoTecnologia3", AV58Tecnologia_TipoTecnologia3);
         AV25DynamicFiltersSelector3 = "TECNOLOGIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTftecnologia_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftecnologia_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftecnologia_nome_Visible), 5, 0)));
         edtavTftecnologia_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftecnologia_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftecnologia_nome_sel_Visible), 5, 0)));
         Ddo_tecnologia_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Tecnologia_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "TitleControlIdToReplace", Ddo_tecnologia_nome_Titlecontrolidtoreplace);
         AV62ddo_Tecnologia_NomeTitleControlIdToReplace = Ddo_tecnologia_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Tecnologia_NomeTitleControlIdToReplace", AV62ddo_Tecnologia_NomeTitleControlIdToReplace);
         edtavDdo_tecnologia_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tecnologia_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tecnologia_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace = subGrid_Internalname+"_Tecnologia_TipoTecnologia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "TitleControlIdToReplace", Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace);
         AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace = Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace", AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace);
         edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione uma Tecnologia";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV67DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV67DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E254O2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV59Tecnologia_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63Tecnologia_TipoTecnologiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtTecnologia_Nome_Titleformat = 2;
         edtTecnologia_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tecnologia", AV62ddo_Tecnologia_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTecnologia_Nome_Internalname, "Title", edtTecnologia_Nome_Title);
         cmbTecnologia_TipoTecnologia_Titleformat = 2;
         cmbTecnologia_TipoTecnologia.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTecnologia_TipoTecnologia_Internalname, "Title", cmbTecnologia_TipoTecnologia.Title.Text);
         AV69GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69GridCurrentPage), 10, 0)));
         AV70GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV59Tecnologia_NomeTitleFilterData", AV59Tecnologia_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV63Tecnologia_TipoTecnologiaTitleFilterData", AV63Tecnologia_TipoTecnologiaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E114O2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV68PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV68PageToGo) ;
         }
      }

      protected void E124O2( )
      {
         /* Ddo_tecnologia_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tecnologia_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tecnologia_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "SortedStatus", Ddo_tecnologia_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tecnologia_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tecnologia_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "SortedStatus", Ddo_tecnologia_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tecnologia_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV60TFTecnologia_Nome = Ddo_tecnologia_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFTecnologia_Nome", AV60TFTecnologia_Nome);
            AV61TFTecnologia_Nome_Sel = Ddo_tecnologia_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFTecnologia_Nome_Sel", AV61TFTecnologia_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E134O2( )
      {
         /* Ddo_tecnologia_tipotecnologia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tecnologia_tipotecnologia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tecnologia_tipotecnologia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "SortedStatus", Ddo_tecnologia_tipotecnologia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tecnologia_tipotecnologia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tecnologia_tipotecnologia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "SortedStatus", Ddo_tecnologia_tipotecnologia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tecnologia_tipotecnologia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV64TFTecnologia_TipoTecnologia_SelsJson = Ddo_tecnologia_tipotecnologia_Selectedvalue_get;
            AV65TFTecnologia_TipoTecnologia_Sels.FromJSonString(AV64TFTecnologia_TipoTecnologia_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV65TFTecnologia_TipoTecnologia_Sels", AV65TFTecnologia_TipoTecnologia_Sels);
      }

      private void E264O2( )
      {
         /* Grid_Load Routine */
         AV31Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV31Select);
         AV73Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 83;
         }
         sendrow_832( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_83_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(83, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E274O2 */
         E274O2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E274O2( )
      {
         /* Enter Routine */
         AV7InOutTecnologia_Codigo = A131Tecnologia_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutTecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutTecnologia_Codigo), 6, 0)));
         AV8InOutTecnologia_Nome = A132Tecnologia_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutTecnologia_Nome", AV8InOutTecnologia_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutTecnologia_Codigo,(String)AV8InOutTecnologia_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E144O2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E194O2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E154O2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tecnologia_Nome1, AV56Tecnologia_TipoTecnologia1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Tecnologia_Nome2, AV57Tecnologia_TipoTecnologia2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Tecnologia_Nome3, AV58Tecnologia_TipoTecnologia3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV60TFTecnologia_Nome, AV61TFTecnologia_Nome_Sel, AV62ddo_Tecnologia_NomeTitleControlIdToReplace, AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV65TFTecnologia_TipoTecnologia_Sels, AV74Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia1.CurrentValue = StringUtil.RTrim( AV56Tecnologia_TipoTecnologia1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia1_Internalname, "Values", cmbavTecnologia_tipotecnologia1.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia2.CurrentValue = StringUtil.RTrim( AV57Tecnologia_TipoTecnologia2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia2_Internalname, "Values", cmbavTecnologia_tipotecnologia2.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia3.CurrentValue = StringUtil.RTrim( AV58Tecnologia_TipoTecnologia3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia3_Internalname, "Values", cmbavTecnologia_tipotecnologia3.ToJavascriptSource());
      }

      protected void E204O2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E214O2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E164O2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tecnologia_Nome1, AV56Tecnologia_TipoTecnologia1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Tecnologia_Nome2, AV57Tecnologia_TipoTecnologia2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Tecnologia_Nome3, AV58Tecnologia_TipoTecnologia3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV60TFTecnologia_Nome, AV61TFTecnologia_Nome_Sel, AV62ddo_Tecnologia_NomeTitleControlIdToReplace, AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV65TFTecnologia_TipoTecnologia_Sels, AV74Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia1.CurrentValue = StringUtil.RTrim( AV56Tecnologia_TipoTecnologia1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia1_Internalname, "Values", cmbavTecnologia_tipotecnologia1.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia2.CurrentValue = StringUtil.RTrim( AV57Tecnologia_TipoTecnologia2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia2_Internalname, "Values", cmbavTecnologia_tipotecnologia2.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia3.CurrentValue = StringUtil.RTrim( AV58Tecnologia_TipoTecnologia3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia3_Internalname, "Values", cmbavTecnologia_tipotecnologia3.ToJavascriptSource());
      }

      protected void E224O2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E174O2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Tecnologia_Nome1, AV56Tecnologia_TipoTecnologia1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Tecnologia_Nome2, AV57Tecnologia_TipoTecnologia2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Tecnologia_Nome3, AV58Tecnologia_TipoTecnologia3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV60TFTecnologia_Nome, AV61TFTecnologia_Nome_Sel, AV62ddo_Tecnologia_NomeTitleControlIdToReplace, AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV65TFTecnologia_TipoTecnologia_Sels, AV74Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia1.CurrentValue = StringUtil.RTrim( AV56Tecnologia_TipoTecnologia1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia1_Internalname, "Values", cmbavTecnologia_tipotecnologia1.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia2.CurrentValue = StringUtil.RTrim( AV57Tecnologia_TipoTecnologia2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia2_Internalname, "Values", cmbavTecnologia_tipotecnologia2.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia3.CurrentValue = StringUtil.RTrim( AV58Tecnologia_TipoTecnologia3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia3_Internalname, "Values", cmbavTecnologia_tipotecnologia3.ToJavascriptSource());
      }

      protected void E234O2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E184O2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV65TFTecnologia_TipoTecnologia_Sels", AV65TFTecnologia_TipoTecnologia_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia1.CurrentValue = StringUtil.RTrim( AV56Tecnologia_TipoTecnologia1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia1_Internalname, "Values", cmbavTecnologia_tipotecnologia1.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia2.CurrentValue = StringUtil.RTrim( AV57Tecnologia_TipoTecnologia2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia2_Internalname, "Values", cmbavTecnologia_tipotecnologia2.ToJavascriptSource());
         cmbavTecnologia_tipotecnologia3.CurrentValue = StringUtil.RTrim( AV58Tecnologia_TipoTecnologia3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia3_Internalname, "Values", cmbavTecnologia_tipotecnologia3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_tecnologia_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "SortedStatus", Ddo_tecnologia_nome_Sortedstatus);
         Ddo_tecnologia_tipotecnologia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "SortedStatus", Ddo_tecnologia_tipotecnologia_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_tecnologia_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "SortedStatus", Ddo_tecnologia_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_tecnologia_tipotecnologia_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "SortedStatus", Ddo_tecnologia_tipotecnologia_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavTecnologia_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTecnologia_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTecnologia_nome1_Visible), 5, 0)));
         cmbavTecnologia_tipotecnologia1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTecnologia_tipotecnologia1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 )
         {
            edtavTecnologia_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTecnologia_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTecnologia_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
         {
            cmbavTecnologia_tipotecnologia1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTecnologia_tipotecnologia1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavTecnologia_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTecnologia_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTecnologia_nome2_Visible), 5, 0)));
         cmbavTecnologia_tipotecnologia2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTecnologia_tipotecnologia2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_NOME") == 0 )
         {
            edtavTecnologia_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTecnologia_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTecnologia_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
         {
            cmbavTecnologia_tipotecnologia2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTecnologia_tipotecnologia2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavTecnologia_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTecnologia_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTecnologia_nome3_Visible), 5, 0)));
         cmbavTecnologia_tipotecnologia3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTecnologia_tipotecnologia3.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_NOME") == 0 )
         {
            edtavTecnologia_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTecnologia_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTecnologia_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
         {
            cmbavTecnologia_tipotecnologia3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTecnologia_tipotecnologia3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "TECNOLOGIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22Tecnologia_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Tecnologia_Nome2", AV22Tecnologia_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "TECNOLOGIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         AV27Tecnologia_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Tecnologia_Nome3", AV27Tecnologia_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV60TFTecnologia_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFTecnologia_Nome", AV60TFTecnologia_Nome);
         Ddo_tecnologia_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "FilteredText_set", Ddo_tecnologia_nome_Filteredtext_set);
         AV61TFTecnologia_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFTecnologia_Nome_Sel", AV61TFTecnologia_Nome_Sel);
         Ddo_tecnologia_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "SelectedValue_set", Ddo_tecnologia_nome_Selectedvalue_set);
         AV65TFTecnologia_TipoTecnologia_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_tecnologia_tipotecnologia_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "SelectedValue_set", Ddo_tecnologia_tipotecnologia_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "TECNOLOGIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Tecnologia_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Tecnologia_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
            {
               AV56Tecnologia_TipoTecnologia1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Tecnologia_TipoTecnologia1", AV56Tecnologia_TipoTecnologia1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22Tecnologia_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Tecnologia_Nome2", AV22Tecnologia_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
               {
                  AV57Tecnologia_TipoTecnologia2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Tecnologia_TipoTecnologia2", AV57Tecnologia_TipoTecnologia2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_NOME") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV27Tecnologia_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Tecnologia_Nome3", AV27Tecnologia_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
                  {
                     AV58Tecnologia_TipoTecnologia3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Tecnologia_TipoTecnologia3", AV58Tecnologia_TipoTecnologia3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFTecnologia_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTECNOLOGIA_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV60TFTecnologia_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFTecnologia_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTECNOLOGIA_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV61TFTecnologia_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV65TFTecnologia_TipoTecnologia_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTECNOLOGIA_TIPOTECNOLOGIA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV65TFTecnologia_TipoTecnologia_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV74Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Tecnologia_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Tecnologia_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Tecnologia_TipoTecnologia1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV56Tecnologia_TipoTecnologia1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Tecnologia_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22Tecnologia_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV57Tecnologia_TipoTecnologia2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV57Tecnologia_TipoTecnologia2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Tecnologia_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV27Tecnologia_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV58Tecnologia_TipoTecnologia3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV58Tecnologia_TipoTecnologia3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_4O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_4O2( true) ;
         }
         else
         {
            wb_table2_5_4O2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_4O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_77_4O2( true) ;
         }
         else
         {
            wb_table3_77_4O2( false) ;
         }
         return  ;
      }

      protected void wb_table3_77_4O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4O2e( true) ;
         }
         else
         {
            wb_table1_2_4O2e( false) ;
         }
      }

      protected void wb_table3_77_4O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_80_4O2( true) ;
         }
         else
         {
            wb_table4_80_4O2( false) ;
         }
         return  ;
      }

      protected void wb_table4_80_4O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_77_4O2e( true) ;
         }
         else
         {
            wb_table3_77_4O2e( false) ;
         }
      }

      protected void wb_table4_80_4O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"83\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTecnologia_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTecnologia_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTecnologia_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbTecnologia_TipoTecnologia_Titleformat == 0 )
               {
                  context.SendWebValue( cmbTecnologia_TipoTecnologia.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbTecnologia_TipoTecnologia.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A131Tecnologia_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A132Tecnologia_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTecnologia_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTecnologia_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A355Tecnologia_TipoTecnologia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbTecnologia_TipoTecnologia.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTecnologia_TipoTecnologia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 83 )
         {
            wbEnd = 0;
            nRC_GXsfl_83 = (short)(nGXsfl_83_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_80_4O2e( true) ;
         }
         else
         {
            wb_table4_80_4O2e( false) ;
         }
      }

      protected void wb_table2_5_4O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptTecnologia.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_4O2( true) ;
         }
         else
         {
            wb_table5_14_4O2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_4O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4O2e( true) ;
         }
         else
         {
            wb_table2_5_4O2e( false) ;
         }
      }

      protected void wb_table5_14_4O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_4O2( true) ;
         }
         else
         {
            wb_table6_19_4O2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_4O2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_4O2e( true) ;
         }
         else
         {
            wb_table5_14_4O2e( false) ;
         }
      }

      protected void wb_table6_19_4O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptTecnologia.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_4O2( true) ;
         }
         else
         {
            wb_table7_28_4O2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_4O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTecnologia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_PromptTecnologia.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_46_4O2( true) ;
         }
         else
         {
            wb_table8_46_4O2( false) ;
         }
         return  ;
      }

      protected void wb_table8_46_4O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTecnologia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_PromptTecnologia.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_64_4O2( true) ;
         }
         else
         {
            wb_table9_64_4O2( false) ;
         }
         return  ;
      }

      protected void wb_table9_64_4O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_4O2e( true) ;
         }
         else
         {
            wb_table6_19_4O2e( false) ;
         }
      }

      protected void wb_table9_64_4O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_PromptTecnologia.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTecnologia_nome3_Internalname, StringUtil.RTrim( AV27Tecnologia_Nome3), StringUtil.RTrim( context.localUtil.Format( AV27Tecnologia_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTecnologia_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTecnologia_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptTecnologia.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTecnologia_tipotecnologia3, cmbavTecnologia_tipotecnologia3_Internalname, StringUtil.RTrim( AV58Tecnologia_TipoTecnologia3), 1, cmbavTecnologia_tipotecnologia3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavTecnologia_tipotecnologia3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", "", true, "HLP_PromptTecnologia.htm");
            cmbavTecnologia_tipotecnologia3.CurrentValue = StringUtil.RTrim( AV58Tecnologia_TipoTecnologia3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia3_Internalname, "Values", (String)(cmbavTecnologia_tipotecnologia3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_64_4O2e( true) ;
         }
         else
         {
            wb_table9_64_4O2e( false) ;
         }
      }

      protected void wb_table8_46_4O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_PromptTecnologia.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTecnologia_nome2_Internalname, StringUtil.RTrim( AV22Tecnologia_Nome2), StringUtil.RTrim( context.localUtil.Format( AV22Tecnologia_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTecnologia_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTecnologia_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptTecnologia.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTecnologia_tipotecnologia2, cmbavTecnologia_tipotecnologia2_Internalname, StringUtil.RTrim( AV57Tecnologia_TipoTecnologia2), 1, cmbavTecnologia_tipotecnologia2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavTecnologia_tipotecnologia2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_PromptTecnologia.htm");
            cmbavTecnologia_tipotecnologia2.CurrentValue = StringUtil.RTrim( AV57Tecnologia_TipoTecnologia2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia2_Internalname, "Values", (String)(cmbavTecnologia_tipotecnologia2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_46_4O2e( true) ;
         }
         else
         {
            wb_table8_46_4O2e( false) ;
         }
      }

      protected void wb_table7_28_4O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptTecnologia.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTecnologia_nome1_Internalname, StringUtil.RTrim( AV17Tecnologia_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Tecnologia_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTecnologia_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTecnologia_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptTecnologia.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTecnologia_tipotecnologia1, cmbavTecnologia_tipotecnologia1_Internalname, StringUtil.RTrim( AV56Tecnologia_TipoTecnologia1), 1, cmbavTecnologia_tipotecnologia1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavTecnologia_tipotecnologia1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_PromptTecnologia.htm");
            cmbavTecnologia_tipotecnologia1.CurrentValue = StringUtil.RTrim( AV56Tecnologia_TipoTecnologia1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia1_Internalname, "Values", (String)(cmbavTecnologia_tipotecnologia1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_4O2e( true) ;
         }
         else
         {
            wb_table7_28_4O2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutTecnologia_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutTecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutTecnologia_Codigo), 6, 0)));
         AV8InOutTecnologia_Nome = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutTecnologia_Nome", AV8InOutTecnologia_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA4O2( ) ;
         WS4O2( ) ;
         WE4O2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282314769");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("prompttecnologia.js", "?20204282314769");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_832( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_83_idx;
         edtTecnologia_Codigo_Internalname = "TECNOLOGIA_CODIGO_"+sGXsfl_83_idx;
         edtTecnologia_Nome_Internalname = "TECNOLOGIA_NOME_"+sGXsfl_83_idx;
         cmbTecnologia_TipoTecnologia_Internalname = "TECNOLOGIA_TIPOTECNOLOGIA_"+sGXsfl_83_idx;
      }

      protected void SubsflControlProps_fel_832( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_83_fel_idx;
         edtTecnologia_Codigo_Internalname = "TECNOLOGIA_CODIGO_"+sGXsfl_83_fel_idx;
         edtTecnologia_Nome_Internalname = "TECNOLOGIA_NOME_"+sGXsfl_83_fel_idx;
         cmbTecnologia_TipoTecnologia_Internalname = "TECNOLOGIA_TIPOTECNOLOGIA_"+sGXsfl_83_fel_idx;
      }

      protected void sendrow_832( )
      {
         SubsflControlProps_832( ) ;
         WB4O0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_83_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_83_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_83_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 84,'',false,'',83)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV73Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV73Select_GXI : context.PathToRelativeUrl( AV31Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_83_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTecnologia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A131Tecnologia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTecnologia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTecnologia_Nome_Internalname,StringUtil.RTrim( A132Tecnologia_Nome),StringUtil.RTrim( context.localUtil.Format( A132Tecnologia_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTecnologia_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_83_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "TECNOLOGIA_TIPOTECNOLOGIA_" + sGXsfl_83_idx;
               cmbTecnologia_TipoTecnologia.Name = GXCCtl;
               cmbTecnologia_TipoTecnologia.WebTags = "";
               cmbTecnologia_TipoTecnologia.addItem("", "(Nenhum)", 0);
               cmbTecnologia_TipoTecnologia.addItem("OS", "Sistema Operacional", 0);
               cmbTecnologia_TipoTecnologia.addItem("LNG", "Linguagem", 0);
               cmbTecnologia_TipoTecnologia.addItem("DBM", "Banco de Dados", 0);
               cmbTecnologia_TipoTecnologia.addItem("SFT", "Software", 0);
               cmbTecnologia_TipoTecnologia.addItem("SRV", "Servidor", 0);
               cmbTecnologia_TipoTecnologia.addItem("DSK", "Desktop", 0);
               cmbTecnologia_TipoTecnologia.addItem("NTB", "Notebook", 0);
               cmbTecnologia_TipoTecnologia.addItem("PRN", "Impresora", 0);
               cmbTecnologia_TipoTecnologia.addItem("HRD", "Hardware", 0);
               if ( cmbTecnologia_TipoTecnologia.ItemCount > 0 )
               {
                  A355Tecnologia_TipoTecnologia = cmbTecnologia_TipoTecnologia.getValidValue(A355Tecnologia_TipoTecnologia);
                  n355Tecnologia_TipoTecnologia = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbTecnologia_TipoTecnologia,(String)cmbTecnologia_TipoTecnologia_Internalname,StringUtil.RTrim( A355Tecnologia_TipoTecnologia),(short)1,(String)cmbTecnologia_TipoTecnologia_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbTecnologia_TipoTecnologia.CurrentValue = StringUtil.RTrim( A355Tecnologia_TipoTecnologia);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTecnologia_TipoTecnologia_Internalname, "Values", (String)(cmbTecnologia_TipoTecnologia.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_CODIGO"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_NOME"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, StringUtil.RTrim( context.localUtil.Format( A132Tecnologia_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_TIPOTECNOLOGIA"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, StringUtil.RTrim( context.localUtil.Format( A355Tecnologia_TipoTecnologia, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         /* End function sendrow_832 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavTecnologia_nome1_Internalname = "vTECNOLOGIA_NOME1";
         cmbavTecnologia_tipotecnologia1_Internalname = "vTECNOLOGIA_TIPOTECNOLOGIA1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavTecnologia_nome2_Internalname = "vTECNOLOGIA_NOME2";
         cmbavTecnologia_tipotecnologia2_Internalname = "vTECNOLOGIA_TIPOTECNOLOGIA2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavTecnologia_nome3_Internalname = "vTECNOLOGIA_NOME3";
         cmbavTecnologia_tipotecnologia3_Internalname = "vTECNOLOGIA_TIPOTECNOLOGIA3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtTecnologia_Codigo_Internalname = "TECNOLOGIA_CODIGO";
         edtTecnologia_Nome_Internalname = "TECNOLOGIA_NOME";
         cmbTecnologia_TipoTecnologia_Internalname = "TECNOLOGIA_TIPOTECNOLOGIA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTftecnologia_nome_Internalname = "vTFTECNOLOGIA_NOME";
         edtavTftecnologia_nome_sel_Internalname = "vTFTECNOLOGIA_NOME_SEL";
         Ddo_tecnologia_nome_Internalname = "DDO_TECNOLOGIA_NOME";
         edtavDdo_tecnologia_nometitlecontrolidtoreplace_Internalname = "vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE";
         Ddo_tecnologia_tipotecnologia_Internalname = "DDO_TECNOLOGIA_TIPOTECNOLOGIA";
         edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Internalname = "vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbTecnologia_TipoTecnologia_Jsonclick = "";
         edtTecnologia_Nome_Jsonclick = "";
         edtTecnologia_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         cmbavTecnologia_tipotecnologia1_Jsonclick = "";
         edtavTecnologia_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavTecnologia_tipotecnologia2_Jsonclick = "";
         edtavTecnologia_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavTecnologia_tipotecnologia3_Jsonclick = "";
         edtavTecnologia_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         cmbTecnologia_TipoTecnologia_Titleformat = 0;
         edtTecnologia_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         cmbavTecnologia_tipotecnologia3.Visible = 1;
         edtavTecnologia_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         cmbavTecnologia_tipotecnologia2.Visible = 1;
         edtavTecnologia_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         cmbavTecnologia_tipotecnologia1.Visible = 1;
         edtavTecnologia_nome1_Visible = 1;
         cmbTecnologia_TipoTecnologia.Title.Text = "Tipo";
         edtTecnologia_Nome_Title = "Tecnologia";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tecnologia_nometitlecontrolidtoreplace_Visible = 1;
         edtavTftecnologia_nome_sel_Jsonclick = "";
         edtavTftecnologia_nome_sel_Visible = 1;
         edtavTftecnologia_nome_Jsonclick = "";
         edtavTftecnologia_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_tecnologia_tipotecnologia_Searchbuttontext = "Filtrar Selecionados";
         Ddo_tecnologia_tipotecnologia_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tecnologia_tipotecnologia_Rangefilterto = "At�";
         Ddo_tecnologia_tipotecnologia_Rangefilterfrom = "Desde";
         Ddo_tecnologia_tipotecnologia_Cleanfilter = "Limpar pesquisa";
         Ddo_tecnologia_tipotecnologia_Loadingdata = "Carregando dados...";
         Ddo_tecnologia_tipotecnologia_Sortdsc = "Ordenar de Z � A";
         Ddo_tecnologia_tipotecnologia_Sortasc = "Ordenar de A � Z";
         Ddo_tecnologia_tipotecnologia_Datalistupdateminimumcharacters = 0;
         Ddo_tecnologia_tipotecnologia_Datalistfixedvalues = "OS:Sistema Operacional,LNG:Linguagem,DBM:Banco de Dados,SFT:Software,SRV:Servidor,DSK:Desktop,NTB:Notebook,PRN:Impresora,HRD:Hardware";
         Ddo_tecnologia_tipotecnologia_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_tecnologia_tipotecnologia_Datalisttype = "FixedValues";
         Ddo_tecnologia_tipotecnologia_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tecnologia_tipotecnologia_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tecnologia_tipotecnologia_Includefilter = Convert.ToBoolean( 0);
         Ddo_tecnologia_tipotecnologia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tecnologia_tipotecnologia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace = "";
         Ddo_tecnologia_tipotecnologia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tecnologia_tipotecnologia_Cls = "ColumnSettings";
         Ddo_tecnologia_tipotecnologia_Tooltip = "Op��es";
         Ddo_tecnologia_tipotecnologia_Caption = "";
         Ddo_tecnologia_nome_Searchbuttontext = "Pesquisar";
         Ddo_tecnologia_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tecnologia_nome_Rangefilterto = "At�";
         Ddo_tecnologia_nome_Rangefilterfrom = "Desde";
         Ddo_tecnologia_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_tecnologia_nome_Loadingdata = "Carregando dados...";
         Ddo_tecnologia_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_tecnologia_nome_Sortasc = "Ordenar de A � Z";
         Ddo_tecnologia_nome_Datalistupdateminimumcharacters = 0;
         Ddo_tecnologia_nome_Datalistproc = "GetPromptTecnologiaFilterData";
         Ddo_tecnologia_nome_Datalistfixedvalues = "";
         Ddo_tecnologia_nome_Datalisttype = "Dynamic";
         Ddo_tecnologia_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tecnologia_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tecnologia_nome_Filtertype = "Character";
         Ddo_tecnologia_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_tecnologia_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tecnologia_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tecnologia_nome_Titlecontrolidtoreplace = "";
         Ddo_tecnologia_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tecnologia_nome_Cls = "ColumnSettings";
         Ddo_tecnologia_nome_Tooltip = "Op��es";
         Ddo_tecnologia_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Tecnologia";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV62ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV61TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV65TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''}],oparms:[{av:'AV59Tecnologia_NomeTitleFilterData',fld:'vTECNOLOGIA_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV63Tecnologia_TipoTecnologiaTitleFilterData',fld:'vTECNOLOGIA_TIPOTECNOLOGIATITLEFILTERDATA',pic:'',nv:null},{av:'edtTecnologia_Nome_Titleformat',ctrl:'TECNOLOGIA_NOME',prop:'Titleformat'},{av:'edtTecnologia_Nome_Title',ctrl:'TECNOLOGIA_NOME',prop:'Title'},{av:'cmbTecnologia_TipoTecnologia'},{av:'AV69GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV70GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E114O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV60TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV61TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV62ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_TECNOLOGIA_NOME.ONOPTIONCLICKED","{handler:'E124O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV60TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV61TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV62ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_tecnologia_nome_Activeeventkey',ctrl:'DDO_TECNOLOGIA_NOME',prop:'ActiveEventKey'},{av:'Ddo_tecnologia_nome_Filteredtext_get',ctrl:'DDO_TECNOLOGIA_NOME',prop:'FilteredText_get'},{av:'Ddo_tecnologia_nome_Selectedvalue_get',ctrl:'DDO_TECNOLOGIA_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tecnologia_nome_Sortedstatus',ctrl:'DDO_TECNOLOGIA_NOME',prop:'SortedStatus'},{av:'AV60TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV61TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tecnologia_tipotecnologia_Sortedstatus',ctrl:'DDO_TECNOLOGIA_TIPOTECNOLOGIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TECNOLOGIA_TIPOTECNOLOGIA.ONOPTIONCLICKED","{handler:'E134O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV60TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV61TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV62ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_tecnologia_tipotecnologia_Activeeventkey',ctrl:'DDO_TECNOLOGIA_TIPOTECNOLOGIA',prop:'ActiveEventKey'},{av:'Ddo_tecnologia_tipotecnologia_Selectedvalue_get',ctrl:'DDO_TECNOLOGIA_TIPOTECNOLOGIA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tecnologia_tipotecnologia_Sortedstatus',ctrl:'DDO_TECNOLOGIA_TIPOTECNOLOGIA',prop:'SortedStatus'},{av:'AV65TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'Ddo_tecnologia_nome_Sortedstatus',ctrl:'DDO_TECNOLOGIA_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E264O2',iparms:[],oparms:[{av:'AV31Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E274O2',iparms:[{av:'A131Tecnologia_Codigo',fld:'TECNOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A132Tecnologia_Nome',fld:'TECNOLOGIA_NOME',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutTecnologia_Codigo',fld:'vINOUTTECNOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutTecnologia_Nome',fld:'vINOUTTECNOLOGIA_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E144O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV60TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV61TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV62ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E194O2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E154O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV60TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV61TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV62ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'edtavTecnologia_nome2_Visible',ctrl:'vTECNOLOGIA_NOME2',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavTecnologia_nome3_Visible',ctrl:'vTECNOLOGIA_NOME3',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavTecnologia_nome1_Visible',ctrl:'vTECNOLOGIA_NOME1',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E204O2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavTecnologia_nome1_Visible',ctrl:'vTECNOLOGIA_NOME1',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E214O2',iparms:[],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E164O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV60TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV61TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV62ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'edtavTecnologia_nome2_Visible',ctrl:'vTECNOLOGIA_NOME2',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavTecnologia_nome3_Visible',ctrl:'vTECNOLOGIA_NOME3',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavTecnologia_nome1_Visible',ctrl:'vTECNOLOGIA_NOME1',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E224O2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavTecnologia_nome2_Visible',ctrl:'vTECNOLOGIA_NOME2',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia2'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E174O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV60TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV61TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV62ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'edtavTecnologia_nome2_Visible',ctrl:'vTECNOLOGIA_NOME2',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavTecnologia_nome3_Visible',ctrl:'vTECNOLOGIA_NOME3',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavTecnologia_nome1_Visible',ctrl:'vTECNOLOGIA_NOME1',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E234O2',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavTecnologia_nome3_Visible',ctrl:'vTECNOLOGIA_NOME3',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E184O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV60TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV61TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV62ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV60TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'Ddo_tecnologia_nome_Filteredtext_set',ctrl:'DDO_TECNOLOGIA_NOME',prop:'FilteredText_set'},{av:'AV61TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tecnologia_nome_Selectedvalue_set',ctrl:'DDO_TECNOLOGIA_NOME',prop:'SelectedValue_set'},{av:'AV65TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'Ddo_tecnologia_tipotecnologia_Selectedvalue_set',ctrl:'DDO_TECNOLOGIA_TIPOTECNOLOGIA',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavTecnologia_nome1_Visible',ctrl:'vTECNOLOGIA_NOME1',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia1'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Tecnologia_Nome2',fld:'vTECNOLOGIA_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Tecnologia_Nome3',fld:'vTECNOLOGIA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV56Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV57Tecnologia_TipoTecnologia2',fld:'vTECNOLOGIA_TIPOTECNOLOGIA2',pic:'',nv:''},{av:'AV58Tecnologia_TipoTecnologia3',fld:'vTECNOLOGIA_TIPOTECNOLOGIA3',pic:'',nv:''},{av:'edtavTecnologia_nome2_Visible',ctrl:'vTECNOLOGIA_NOME2',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavTecnologia_nome3_Visible',ctrl:'vTECNOLOGIA_NOME3',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutTecnologia_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_tecnologia_nome_Activeeventkey = "";
         Ddo_tecnologia_nome_Filteredtext_get = "";
         Ddo_tecnologia_nome_Selectedvalue_get = "";
         Ddo_tecnologia_tipotecnologia_Activeeventkey = "";
         Ddo_tecnologia_tipotecnologia_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Tecnologia_Nome1 = "";
         AV56Tecnologia_TipoTecnologia1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22Tecnologia_Nome2 = "";
         AV57Tecnologia_TipoTecnologia2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV27Tecnologia_Nome3 = "";
         AV58Tecnologia_TipoTecnologia3 = "";
         AV60TFTecnologia_Nome = "";
         AV61TFTecnologia_Nome_Sel = "";
         AV62ddo_Tecnologia_NomeTitleControlIdToReplace = "";
         AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace = "";
         AV65TFTecnologia_TipoTecnologia_Sels = new GxSimpleCollection();
         AV74Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV67DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV59Tecnologia_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63Tecnologia_TipoTecnologiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_tecnologia_nome_Filteredtext_set = "";
         Ddo_tecnologia_nome_Selectedvalue_set = "";
         Ddo_tecnologia_nome_Sortedstatus = "";
         Ddo_tecnologia_tipotecnologia_Selectedvalue_set = "";
         Ddo_tecnologia_tipotecnologia_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Select = "";
         AV73Select_GXI = "";
         A132Tecnologia_Nome = "";
         A355Tecnologia_TipoTecnologia = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17Tecnologia_Nome1 = "";
         lV22Tecnologia_Nome2 = "";
         lV27Tecnologia_Nome3 = "";
         lV60TFTecnologia_Nome = "";
         H004O2_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         H004O2_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         H004O2_A132Tecnologia_Nome = new String[] {""} ;
         H004O2_A131Tecnologia_Codigo = new int[1] ;
         H004O3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV64TFTecnologia_TipoTecnologia_SelsJson = "";
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prompttecnologia__default(),
            new Object[][] {
                new Object[] {
               H004O2_A355Tecnologia_TipoTecnologia, H004O2_n355Tecnologia_TipoTecnologia, H004O2_A132Tecnologia_Nome, H004O2_A131Tecnologia_Codigo
               }
               , new Object[] {
               H004O3_AGRID_nRecordCount
               }
            }
         );
         AV74Pgmname = "PromptTecnologia";
         /* GeneXus formulas. */
         AV74Pgmname = "PromptTecnologia";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_83 ;
      private short nGXsfl_83_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_83_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtTecnologia_Nome_Titleformat ;
      private short cmbTecnologia_TipoTecnologia_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutTecnologia_Codigo ;
      private int wcpOAV7InOutTecnologia_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_tecnologia_nome_Datalistupdateminimumcharacters ;
      private int Ddo_tecnologia_tipotecnologia_Datalistupdateminimumcharacters ;
      private int edtavTftecnologia_nome_Visible ;
      private int edtavTftecnologia_nome_sel_Visible ;
      private int edtavDdo_tecnologia_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Visible ;
      private int A131Tecnologia_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV65TFTecnologia_TipoTecnologia_Sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int AV68PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavTecnologia_nome1_Visible ;
      private int edtavTecnologia_nome2_Visible ;
      private int edtavTecnologia_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV69GridCurrentPage ;
      private long AV70GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV8InOutTecnologia_Nome ;
      private String wcpOAV8InOutTecnologia_Nome ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_tecnologia_nome_Activeeventkey ;
      private String Ddo_tecnologia_nome_Filteredtext_get ;
      private String Ddo_tecnologia_nome_Selectedvalue_get ;
      private String Ddo_tecnologia_tipotecnologia_Activeeventkey ;
      private String Ddo_tecnologia_tipotecnologia_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_83_idx="0001" ;
      private String AV17Tecnologia_Nome1 ;
      private String AV56Tecnologia_TipoTecnologia1 ;
      private String AV22Tecnologia_Nome2 ;
      private String AV57Tecnologia_TipoTecnologia2 ;
      private String AV27Tecnologia_Nome3 ;
      private String AV58Tecnologia_TipoTecnologia3 ;
      private String AV60TFTecnologia_Nome ;
      private String AV61TFTecnologia_Nome_Sel ;
      private String AV74Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_tecnologia_nome_Caption ;
      private String Ddo_tecnologia_nome_Tooltip ;
      private String Ddo_tecnologia_nome_Cls ;
      private String Ddo_tecnologia_nome_Filteredtext_set ;
      private String Ddo_tecnologia_nome_Selectedvalue_set ;
      private String Ddo_tecnologia_nome_Dropdownoptionstype ;
      private String Ddo_tecnologia_nome_Titlecontrolidtoreplace ;
      private String Ddo_tecnologia_nome_Sortedstatus ;
      private String Ddo_tecnologia_nome_Filtertype ;
      private String Ddo_tecnologia_nome_Datalisttype ;
      private String Ddo_tecnologia_nome_Datalistfixedvalues ;
      private String Ddo_tecnologia_nome_Datalistproc ;
      private String Ddo_tecnologia_nome_Sortasc ;
      private String Ddo_tecnologia_nome_Sortdsc ;
      private String Ddo_tecnologia_nome_Loadingdata ;
      private String Ddo_tecnologia_nome_Cleanfilter ;
      private String Ddo_tecnologia_nome_Rangefilterfrom ;
      private String Ddo_tecnologia_nome_Rangefilterto ;
      private String Ddo_tecnologia_nome_Noresultsfound ;
      private String Ddo_tecnologia_nome_Searchbuttontext ;
      private String Ddo_tecnologia_tipotecnologia_Caption ;
      private String Ddo_tecnologia_tipotecnologia_Tooltip ;
      private String Ddo_tecnologia_tipotecnologia_Cls ;
      private String Ddo_tecnologia_tipotecnologia_Selectedvalue_set ;
      private String Ddo_tecnologia_tipotecnologia_Dropdownoptionstype ;
      private String Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace ;
      private String Ddo_tecnologia_tipotecnologia_Sortedstatus ;
      private String Ddo_tecnologia_tipotecnologia_Datalisttype ;
      private String Ddo_tecnologia_tipotecnologia_Datalistfixedvalues ;
      private String Ddo_tecnologia_tipotecnologia_Sortasc ;
      private String Ddo_tecnologia_tipotecnologia_Sortdsc ;
      private String Ddo_tecnologia_tipotecnologia_Loadingdata ;
      private String Ddo_tecnologia_tipotecnologia_Cleanfilter ;
      private String Ddo_tecnologia_tipotecnologia_Rangefilterfrom ;
      private String Ddo_tecnologia_tipotecnologia_Rangefilterto ;
      private String Ddo_tecnologia_tipotecnologia_Noresultsfound ;
      private String Ddo_tecnologia_tipotecnologia_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTftecnologia_nome_Internalname ;
      private String edtavTftecnologia_nome_Jsonclick ;
      private String edtavTftecnologia_nome_sel_Internalname ;
      private String edtavTftecnologia_nome_sel_Jsonclick ;
      private String edtavDdo_tecnologia_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtTecnologia_Codigo_Internalname ;
      private String A132Tecnologia_Nome ;
      private String edtTecnologia_Nome_Internalname ;
      private String cmbTecnologia_TipoTecnologia_Internalname ;
      private String A355Tecnologia_TipoTecnologia ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV17Tecnologia_Nome1 ;
      private String lV22Tecnologia_Nome2 ;
      private String lV27Tecnologia_Nome3 ;
      private String lV60TFTecnologia_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavTecnologia_nome1_Internalname ;
      private String cmbavTecnologia_tipotecnologia1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavTecnologia_nome2_Internalname ;
      private String cmbavTecnologia_tipotecnologia2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavTecnologia_nome3_Internalname ;
      private String cmbavTecnologia_tipotecnologia3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_tecnologia_nome_Internalname ;
      private String Ddo_tecnologia_tipotecnologia_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtTecnologia_Nome_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavTecnologia_nome3_Jsonclick ;
      private String cmbavTecnologia_tipotecnologia3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavTecnologia_nome2_Jsonclick ;
      private String cmbavTecnologia_tipotecnologia2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavTecnologia_nome1_Jsonclick ;
      private String cmbavTecnologia_tipotecnologia1_Jsonclick ;
      private String sGXsfl_83_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtTecnologia_Codigo_Jsonclick ;
      private String edtTecnologia_Nome_Jsonclick ;
      private String cmbTecnologia_TipoTecnologia_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_tecnologia_nome_Includesortasc ;
      private bool Ddo_tecnologia_nome_Includesortdsc ;
      private bool Ddo_tecnologia_nome_Includefilter ;
      private bool Ddo_tecnologia_nome_Filterisrange ;
      private bool Ddo_tecnologia_nome_Includedatalist ;
      private bool Ddo_tecnologia_tipotecnologia_Includesortasc ;
      private bool Ddo_tecnologia_tipotecnologia_Includesortdsc ;
      private bool Ddo_tecnologia_tipotecnologia_Includefilter ;
      private bool Ddo_tecnologia_tipotecnologia_Filterisrange ;
      private bool Ddo_tecnologia_tipotecnologia_Includedatalist ;
      private bool Ddo_tecnologia_tipotecnologia_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n355Tecnologia_TipoTecnologia ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Select_IsBlob ;
      private String AV64TFTecnologia_TipoTecnologia_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV62ddo_Tecnologia_NomeTitleControlIdToReplace ;
      private String AV66ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace ;
      private String AV73Select_GXI ;
      private String AV31Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutTecnologia_Codigo ;
      private String aP1_InOutTecnologia_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavTecnologia_tipotecnologia1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavTecnologia_tipotecnologia2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbavTecnologia_tipotecnologia3 ;
      private GXCombobox cmbTecnologia_TipoTecnologia ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H004O2_A355Tecnologia_TipoTecnologia ;
      private bool[] H004O2_n355Tecnologia_TipoTecnologia ;
      private String[] H004O2_A132Tecnologia_Nome ;
      private int[] H004O2_A131Tecnologia_Codigo ;
      private long[] H004O3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV65TFTecnologia_TipoTecnologia_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV59Tecnologia_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV63Tecnologia_TipoTecnologiaTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV67DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class prompttecnologia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H004O2( IGxContext context ,
                                             String A355Tecnologia_TipoTecnologia ,
                                             IGxCollection AV65TFTecnologia_TipoTecnologia_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17Tecnologia_Nome1 ,
                                             String AV56Tecnologia_TipoTecnologia1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22Tecnologia_Nome2 ,
                                             String AV57Tecnologia_TipoTecnologia2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             short AV26DynamicFiltersOperator3 ,
                                             String AV27Tecnologia_Nome3 ,
                                             String AV58Tecnologia_TipoTecnologia3 ,
                                             String AV61TFTecnologia_Nome_Sel ,
                                             String AV60TFTecnologia_Nome ,
                                             int AV65TFTecnologia_TipoTecnologia_Sels_Count ,
                                             String A132Tecnologia_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [16] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Tecnologia_TipoTecnologia], [Tecnologia_Nome], [Tecnologia_Codigo]";
         sFromString = " FROM [Tecnologia] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Tecnologia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV17Tecnologia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV17Tecnologia_Nome1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Tecnologia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV17Tecnologia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV17Tecnologia_Nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Tecnologia_TipoTecnologia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV56Tecnologia_TipoTecnologia1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV56Tecnologia_TipoTecnologia1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Tecnologia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV22Tecnologia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV22Tecnologia_Nome2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Tecnologia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV22Tecnologia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV22Tecnologia_Nome2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57Tecnologia_TipoTecnologia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV57Tecnologia_TipoTecnologia2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV57Tecnologia_TipoTecnologia2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Tecnologia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV27Tecnologia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV27Tecnologia_Nome3)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Tecnologia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV27Tecnologia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV27Tecnologia_Nome3)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58Tecnologia_TipoTecnologia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV58Tecnologia_TipoTecnologia3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV58Tecnologia_TipoTecnologia3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61TFTecnologia_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFTecnologia_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV60TFTecnologia_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV60TFTecnologia_Nome)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFTecnologia_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] = @AV61TFTecnologia_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] = @AV61TFTecnologia_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV65TFTecnologia_TipoTecnologia_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV65TFTecnologia_TipoTecnologia_Sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV65TFTecnologia_TipoTecnologia_Sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Tecnologia_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Tecnologia_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Tecnologia_TipoTecnologia]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Tecnologia_TipoTecnologia] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Tecnologia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H004O3( IGxContext context ,
                                             String A355Tecnologia_TipoTecnologia ,
                                             IGxCollection AV65TFTecnologia_TipoTecnologia_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17Tecnologia_Nome1 ,
                                             String AV56Tecnologia_TipoTecnologia1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22Tecnologia_Nome2 ,
                                             String AV57Tecnologia_TipoTecnologia2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             short AV26DynamicFiltersOperator3 ,
                                             String AV27Tecnologia_Nome3 ,
                                             String AV58Tecnologia_TipoTecnologia3 ,
                                             String AV61TFTecnologia_Nome_Sel ,
                                             String AV60TFTecnologia_Nome ,
                                             int AV65TFTecnologia_TipoTecnologia_Sels_Count ,
                                             String A132Tecnologia_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [11] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Tecnologia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Tecnologia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV17Tecnologia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV17Tecnologia_Nome1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Tecnologia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV17Tecnologia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV17Tecnologia_Nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Tecnologia_TipoTecnologia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV56Tecnologia_TipoTecnologia1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV56Tecnologia_TipoTecnologia1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Tecnologia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV22Tecnologia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV22Tecnologia_Nome2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Tecnologia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV22Tecnologia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV22Tecnologia_Nome2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57Tecnologia_TipoTecnologia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV57Tecnologia_TipoTecnologia2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV57Tecnologia_TipoTecnologia2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Tecnologia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV27Tecnologia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV27Tecnologia_Nome3)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Tecnologia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV27Tecnologia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV27Tecnologia_Nome3)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58Tecnologia_TipoTecnologia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV58Tecnologia_TipoTecnologia3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV58Tecnologia_TipoTecnologia3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61TFTecnologia_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFTecnologia_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV60TFTecnologia_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV60TFTecnologia_Nome)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFTecnologia_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] = @AV61TFTecnologia_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] = @AV61TFTecnologia_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV65TFTecnologia_TipoTecnologia_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV65TFTecnologia_TipoTecnologia_Sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV65TFTecnologia_TipoTecnologia_Sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H004O2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H004O3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH004O2 ;
          prmH004O2 = new Object[] {
          new Object[] {"@lV17Tecnologia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV17Tecnologia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56Tecnologia_TipoTecnologia1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV22Tecnologia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22Tecnologia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57Tecnologia_TipoTecnologia2",SqlDbType.Char,3,0} ,
          new Object[] {"@lV27Tecnologia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27Tecnologia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58Tecnologia_TipoTecnologia3",SqlDbType.Char,3,0} ,
          new Object[] {"@lV60TFTecnologia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV61TFTecnologia_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH004O3 ;
          prmH004O3 = new Object[] {
          new Object[] {"@lV17Tecnologia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV17Tecnologia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56Tecnologia_TipoTecnologia1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV22Tecnologia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22Tecnologia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57Tecnologia_TipoTecnologia2",SqlDbType.Char,3,0} ,
          new Object[] {"@lV27Tecnologia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27Tecnologia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58Tecnologia_TipoTecnologia3",SqlDbType.Char,3,0} ,
          new Object[] {"@lV60TFTecnologia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV61TFTecnologia_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H004O2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH004O2,11,0,true,false )
             ,new CursorDef("H004O3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH004O3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

}
