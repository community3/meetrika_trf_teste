/*
               File: PRC_RetornaCodigoContratanteAreaAtrabalho
        Description: PRC_Retorna Codigo Contratante Area Atrabalho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:49.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_retornacodigocontratanteareaatrabalho : GXProcedure
   {
      public prc_retornacodigocontratanteareaatrabalho( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_retornacodigocontratanteareaatrabalho( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           out int aP1_Contratante_Codigo )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9Contratante_Codigo = 0 ;
         initialize();
         executePrivate();
         aP1_Contratante_Codigo=this.AV9Contratante_Codigo;
      }

      public int executeUdp( int aP0_AreaTrabalho_Codigo )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9Contratante_Codigo = 0 ;
         initialize();
         executePrivate();
         aP1_Contratante_Codigo=this.AV9Contratante_Codigo;
         return AV9Contratante_Codigo ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 out int aP1_Contratante_Codigo )
      {
         prc_retornacodigocontratanteareaatrabalho objprc_retornacodigocontratanteareaatrabalho;
         objprc_retornacodigocontratanteareaatrabalho = new prc_retornacodigocontratanteareaatrabalho();
         objprc_retornacodigocontratanteareaatrabalho.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_retornacodigocontratanteareaatrabalho.AV9Contratante_Codigo = 0 ;
         objprc_retornacodigocontratanteareaatrabalho.context.SetSubmitInitialConfig(context);
         objprc_retornacodigocontratanteareaatrabalho.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_retornacodigocontratanteareaatrabalho);
         aP1_Contratante_Codigo=this.AV9Contratante_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_retornacodigocontratanteareaatrabalho)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P001K2 */
         pr_default.execute(0, new Object[] {AV8AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A5AreaTrabalho_Codigo = P001K2_A5AreaTrabalho_Codigo[0];
            A29Contratante_Codigo = P001K2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P001K2_n29Contratante_Codigo[0];
            AV9Contratante_Codigo = A29Contratante_Codigo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001K2_A5AreaTrabalho_Codigo = new int[1] ;
         P001K2_A29Contratante_Codigo = new int[1] ;
         P001K2_n29Contratante_Codigo = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_retornacodigocontratanteareaatrabalho__default(),
            new Object[][] {
                new Object[] {
               P001K2_A5AreaTrabalho_Codigo, P001K2_A29Contratante_Codigo, P001K2_n29Contratante_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8AreaTrabalho_Codigo ;
      private int AV9Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private String scmdbuf ;
      private bool n29Contratante_Codigo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P001K2_A5AreaTrabalho_Codigo ;
      private int[] P001K2_A29Contratante_Codigo ;
      private bool[] P001K2_n29Contratante_Codigo ;
      private int aP1_Contratante_Codigo ;
   }

   public class prc_retornacodigocontratanteareaatrabalho__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001K2 ;
          prmP001K2 = new Object[] {
          new Object[] {"@AV8AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001K2", "SELECT [AreaTrabalho_Codigo], [Contratante_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV8AreaTrabalho_Codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001K2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
