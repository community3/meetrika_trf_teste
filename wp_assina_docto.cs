/*
               File: wp_assina_docto
        Description: Assinatura de Documento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:23:39.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_assina_docto : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_assina_docto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_assina_docto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_NomeArqPdf ,
                           int aP1_Lote_Codigo ,
                           String aP2_Verificador )
      {
         this.AV13NomeArqPdf = aP0_NomeArqPdf;
         this.AV24Lote_Codigo = aP1_Lote_Codigo;
         this.AV29Verificador = aP2_Verificador;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContratada_codigo = new GXCombobox();
         dynavContrato_prepostocod = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV25WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGOH82( AV25WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATO_PREPOSTOCOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATO_PREPOSTOCODH82( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV13NomeArqPdf = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13NomeArqPdf", AV13NomeArqPdf);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNOMEARQPDF", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13NomeArqPdf, ""))));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV24Lote_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Lote_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Lote_Codigo), "ZZZZZ9")));
                  AV29Verificador = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Verificador", AV29Verificador);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAH82( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV40Pgmname = "wp_assina_docto";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Pgmname", AV40Pgmname);
               context.Gx_err = 0;
               dynavContratada_codigo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_codigo.Enabled), 5, 0)));
               dynavContrato_prepostocod.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContrato_prepostocod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContrato_prepostocod.Enabled), 5, 0)));
               edtavCertificate_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCertificate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCertificate_Enabled), 5, 0)));
               GXVvCONTRATO_PREPOSTOCOD_htmlH82( ) ;
               WSH82( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEH82( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216233958");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_assina_docto.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV13NomeArqPdf)) + "," + UrlEncode("" +AV24Lote_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV29Verificador))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vNOMEARQPDF", StringUtil.RTrim( AV13NomeArqPdf));
         GxWebStd.gx_hidden_field( context, "vLOTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Lote_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vVERIFICADOR", AV29Verificador);
         GxWebStd.gx_hidden_field( context, "vTEMPIMGFILE", StringUtil.RTrim( AV35TempImgFile));
         GxWebStd.gx_hidden_field( context, "vTEMPFILE", StringUtil.RTrim( AV34tempfile));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV25WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV25WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV28Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATO_PREPOSTOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV27Contrato_PrepostoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCERTIFICATE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Certificate, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vNOMEARQPDF", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13NomeArqPdf, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Lote_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vNOMEARQPDF", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13NomeArqPdf, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Lote_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "wp_assina_docto";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV27Contrato_PrepostoCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV6Certificate, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_assina_docto:[SendSecurityCheck value for]"+"Contrato_PrepostoCod:"+context.localUtil.Format( (decimal)(AV27Contrato_PrepostoCod), "ZZZZZ9"));
         GXUtil.WriteLog("wp_assina_docto:[SendSecurityCheck value for]"+"Certificate:"+StringUtil.RTrim( context.localUtil.Format( AV6Certificate, "")));
      }

      protected void RenderHtmlCloseFormH82( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "wp_assina_docto" ;
      }

      public override String GetPgmdesc( )
      {
         return "Assinatura de Documento" ;
      }

      protected void WBH80( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_H82( true) ;
         }
         else
         {
            wb_table1_2_H82( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_H82e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTH82( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Assinatura de Documento", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPH80( ) ;
      }

      protected void WSH82( )
      {
         STARTH82( ) ;
         EVTH82( ) ;
      }

      protected void EVTH82( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11H82 */
                           E11H82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'SIGN PDF'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12H82 */
                           E12H82 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13H82 */
                                 E13H82 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14H82 */
                           E14H82 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEH82( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormH82( ) ;
            }
         }
      }

      protected void PAH82( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            dynavContrato_prepostocod.Name = "vCONTRATO_PREPOSTOCOD";
            dynavContrato_prepostocod.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_CODIGOH82( wwpbaseobjects.SdtWWPContext AV25WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_dataH82( AV25WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_htmlH82( wwpbaseobjects.SdtWWPContext AV25WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_dataH82( AV25WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV28Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Contratada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV28Contratada_Codigo), "ZZZZZ9")));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_dataH82( wwpbaseobjects.SdtWWPContext AV25WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00H82 */
         pr_default.execute(0, new Object[] {AV25WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00H82_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00H82_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTRATO_PREPOSTOCODH82( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATO_PREPOSTOCOD_dataH82( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATO_PREPOSTOCOD_htmlH82( )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATO_PREPOSTOCOD_dataH82( ) ;
         gxdynajaxindex = 1;
         dynavContrato_prepostocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContrato_prepostocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContrato_prepostocod.ItemCount > 0 )
         {
            AV27Contrato_PrepostoCod = (int)(NumberUtil.Val( dynavContrato_prepostocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27Contrato_PrepostoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contrato_PrepostoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATO_PREPOSTOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV27Contrato_PrepostoCod), "ZZZZZ9")));
         }
      }

      protected void GXDLVvCONTRATO_PREPOSTOCOD_dataH82( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00H83 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00H83_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00H83_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV28Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Contratada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV28Contratada_Codigo), "ZZZZZ9")));
         }
         if ( dynavContrato_prepostocod.ItemCount > 0 )
         {
            AV27Contrato_PrepostoCod = (int)(NumberUtil.Val( dynavContrato_prepostocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27Contrato_PrepostoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contrato_PrepostoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATO_PREPOSTOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV27Contrato_PrepostoCod), "ZZZZZ9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFH82( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV40Pgmname = "wp_assina_docto";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Pgmname", AV40Pgmname);
         context.Gx_err = 0;
         dynavContratada_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_codigo.Enabled), 5, 0)));
         dynavContrato_prepostocod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContrato_prepostocod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContrato_prepostocod.Enabled), 5, 0)));
         edtavCertificate_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCertificate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCertificate_Enabled), 5, 0)));
      }

      protected void RFH82( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14H82 */
            E14H82 ();
            WBH80( ) ;
         }
      }

      protected void STRUPH80( )
      {
         /* Before Start, stand alone formulas. */
         AV40Pgmname = "wp_assina_docto";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Pgmname", AV40Pgmname);
         context.Gx_err = 0;
         dynavContratada_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_codigo.Enabled), 5, 0)));
         dynavContrato_prepostocod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContrato_prepostocod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContrato_prepostocod.Enabled), 5, 0)));
         edtavCertificate_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCertificate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCertificate_Enabled), 5, 0)));
         GXVvCONTRATO_PREPOSTOCOD_htmlH82( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11H82 */
         E11H82 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADA_CODIGO_htmlH82( AV25WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV28Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Contratada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV28Contratada_Codigo), "ZZZZZ9")));
            dynavContrato_prepostocod.CurrentValue = cgiGet( dynavContrato_prepostocod_Internalname);
            AV27Contrato_PrepostoCod = (int)(NumberUtil.Val( cgiGet( dynavContrato_prepostocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contrato_PrepostoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATO_PREPOSTOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV27Contrato_PrepostoCod), "ZZZZZ9")));
            AV20unsignedDocument = cgiGet( edtavUnsigneddocument_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20unsignedDocument", AV20unsignedDocument);
            AV16signatureResult = cgiGet( edtavSignatureresult_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16signatureResult", AV16signatureResult);
            AV6Certificate = cgiGet( edtavCertificate_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Certificate", AV6Certificate);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCERTIFICATE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Certificate, ""))));
            AV7CertificatePwd = cgiGet( edtavCertificatepwd_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7CertificatePwd", AV7CertificatePwd);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "wp_assina_docto";
            AV27Contrato_PrepostoCod = (int)(NumberUtil.Val( cgiGet( dynavContrato_prepostocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contrato_PrepostoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATO_PREPOSTOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV27Contrato_PrepostoCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV27Contrato_PrepostoCod), "ZZZZZ9");
            AV6Certificate = cgiGet( edtavCertificate_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Certificate", AV6Certificate);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCERTIFICATE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Certificate, ""))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV6Certificate, ""));
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_assina_docto:[SecurityCheckFailed value for]"+"Contrato_PrepostoCod:"+context.localUtil.Format( (decimal)(AV27Contrato_PrepostoCod), "ZZZZZ9"));
               GXUtil.WriteLog("wp_assina_docto:[SecurityCheckFailed value for]"+"Certificate:"+StringUtil.RTrim( context.localUtil.Format( AV6Certificate, "")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            GXVvCONTRATO_PREPOSTOCOD_htmlH82( ) ;
            GXVvCONTRATADA_CODIGO_htmlH82( AV25WWPContext) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11H82 */
         E11H82 ();
         if (returnInSub) return;
      }

      protected void E11H82( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV25WWPContext) ;
         edtavUnsigneddocument_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnsigneddocument_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnsigneddocument_Visible), 5, 0)));
         edtavSignatureresult_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSignatureresult_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSignatureresult_Visible), 5, 0)));
         edtavCertificate_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCertificate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCertificate_Visible), 5, 0)));
         new getserverpath(context ).execute( ref  AV40Pgmname, ref  AV37Absolutepath) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Pgmname", AV40Pgmname);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Absolutepath", AV37Absolutepath);
         bttButtonsignpdf_Visible = (AV25WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttButtonsignpdf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttButtonsignpdf_Visible), 5, 0)));
         /* Using cursor H00H84 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            A330ParametrosSistema_Codigo = H00H84_A330ParametrosSistema_Codigo[0];
            A1021ParametrosSistema_PathCrtf = H00H84_A1021ParametrosSistema_PathCrtf[0];
            n1021ParametrosSistema_PathCrtf = H00H84_n1021ParametrosSistema_PathCrtf[0];
            if ( H00H84_n1021ParametrosSistema_PathCrtf[0] || String.IsNullOrEmpty(StringUtil.RTrim( A1021ParametrosSistema_PathCrtf)) )
            {
               AV26Pendencia = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Pendencia", AV26Pendencia);
               Gx_msg = "Deve configurar nos par�metros do Sistema o caminho de Certifica��o!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_msg", Gx_msg);
            }
            else
            {
               if ( ( StringUtil.StrCmp(StringUtil.Substring( A1021ParametrosSistema_PathCrtf, 2, 1), ":") == 0 ) || ( StringUtil.StrCmp(StringUtil.Substring( A1021ParametrosSistema_PathCrtf, 1, 2), "\\\\") == 0 ) )
               {
                  AV20unsignedDocument = A1021ParametrosSistema_PathCrtf + StringUtil.Trim( AV13NomeArqPdf);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20unsignedDocument", AV20unsignedDocument);
                  AV34tempfile = A1021ParametrosSistema_PathCrtf + StringUtil.Trim( AV13NomeArqPdf);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34tempfile", AV34tempfile);
                  AV35TempImgFile = A1021ParametrosSistema_PathCrtf + "url_user" + StringUtil.Trim( StringUtil.Str( (decimal)(AV25WWPContext.gxTpr_Userid), 4, 0)) + ".bmp";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TempImgFile", AV35TempImgFile);
                  AV16signatureResult = A1021ParametrosSistema_PathCrtf + "DA" + StringUtil.Trim( AV13NomeArqPdf);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16signatureResult", AV16signatureResult);
               }
               else
               {
                  AV20unsignedDocument = AV37Absolutepath + A1021ParametrosSistema_PathCrtf + StringUtil.Trim( AV13NomeArqPdf);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20unsignedDocument", AV20unsignedDocument);
                  AV34tempfile = AV37Absolutepath + A1021ParametrosSistema_PathCrtf + StringUtil.Trim( AV13NomeArqPdf);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34tempfile", AV34tempfile);
                  AV35TempImgFile = AV37Absolutepath + A1021ParametrosSistema_PathCrtf + "url_user" + StringUtil.Trim( StringUtil.Str( (decimal)(AV25WWPContext.gxTpr_Userid), 4, 0)) + ".bmp";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TempImgFile", AV35TempImgFile);
                  AV16signatureResult = AV37Absolutepath + A1021ParametrosSistema_PathCrtf + "DA" + StringUtil.Trim( AV13NomeArqPdf);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16signatureResult", AV16signatureResult);
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         /* Using cursor H00H85 */
         pr_default.execute(3, new Object[] {AV24Lote_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = H00H85_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = H00H85_n597ContagemResultado_LoteAceiteCod[0];
            A490ContagemResultado_ContratadaCod = H00H85_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00H85_n490ContagemResultado_ContratadaCod[0];
            AV28Contratada_Codigo = A490ContagemResultado_ContratadaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Contratada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV28Contratada_Codigo), "ZZZZZ9")));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(3);
         }
         pr_default.close(3);
         /* Using cursor H00H86 */
         pr_default.execute(4, new Object[] {AV25WWPContext.gxTpr_Areatrabalho_codigo, AV28Contratada_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A39Contratada_Codigo = H00H86_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = H00H86_A75Contrato_AreaTrabalhoCod[0];
            A1013Contrato_PrepostoCod = H00H86_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00H86_n1013Contrato_PrepostoCod[0];
            if ( H00H86_n1013Contrato_PrepostoCod[0] )
            {
               AV26Pendencia = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Pendencia", AV26Pendencia);
               Gx_msg = "N�o existe preposto informado no Contrato!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_msg", Gx_msg);
            }
            else
            {
               AV27Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contrato_PrepostoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATO_PREPOSTOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV27Contrato_PrepostoCod), "ZZZZZ9")));
               /* Execute user subroutine: 'CERTIFICADODIGITALDOUSUARIO' */
               S115 ();
               if ( returnInSub )
               {
                  pr_default.close(4);
                  returnInSub = true;
                  if (true) return;
               }
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( AV26Pendencia )
         {
            GX_msglist.addItem(Gx_msg);
            bttButtonsignpdf_Tooltiptext = Gx_msg;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttButtonsignpdf_Internalname, "Tooltiptext", bttButtonsignpdf_Tooltiptext);
            bttButtonsignpdf_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttButtonsignpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttButtonsignpdf_Enabled), 5, 0)));
         }
         AV7CertificatePwd = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7CertificatePwd", AV7CertificatePwd);
         if ( StringUtil.StringSearch( AV13NomeArqPdf, "Gerencial", 1) > 0 )
         {
            Form.Caption = "Assinatura Digital do Relat�rio Gerencial";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         }
         else if ( StringUtil.StringSearch( AV13NomeArqPdf, "TA", 1) > 0 )
         {
            Form.Caption = "Assinatura Digital do Termo de Aceite";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         }
         else if ( StringUtil.StringSearch( AV13NomeArqPdf, "OS", 1) > 0 )
         {
            Form.Caption = "Assinatura Digital da Ordem de Servi�o";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         }
      }

      protected void E12H82( )
      {
         /* 'Sign PDF' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20unsignedDocument)) )
         {
            Gx_msg = AV15ServerSideSign.signdocument("digitalSign.PDFDigitalSign", AV6Certificate, AV7CertificatePwd, "", AV20unsignedDocument, AV16signatureResult);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_msg", Gx_msg);
            if ( StringUtil.StrCmp(Gx_msg, "OK") == 0 )
            {
               /* Execute user subroutine: 'GUARDARARQUIVO' */
               S122 ();
               if (returnInSub) return;
            }
            else
            {
               if ( StringUtil.StringSearch( Gx_msg, "Error. El archivo no se puede abrir con �sta contrase�a", 1) > 0 )
               {
                  bttButtonsignpdf_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttButtonsignpdf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttButtonsignpdf_Visible), 5, 0)));
                  GX_msglist.addItem("Error. Senha do Certificado Digital incorreta!");
               }
               else
               {
                  GX_msglist.addItem(Gx_msg);
               }
            }
         }
         else
         {
            GX_msglist.addItem("Por favor suba o arquivo(PDF) N�O assinado!");
         }
      }

      protected void S122( )
      {
         /* 'GUARDARARQUIVO' Routine */
         AV23UploadPDF = AV16signatureResult;
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16signatureResult)) )
         {
            AV31i = (short)(StringUtil.StringSearchRev( AV13NomeArqPdf, "_", -1)-1);
            AV30NomeArq = StringUtil.Substring( AV13NomeArqPdf, 1, AV31i);
            new prc_novo_lote_arq_anexo(context ).execute(  AV24Lote_Codigo,  AV30NomeArq,  AV16signatureResult, out  AV11LoteArquivoAnexo_LoteCod, ref  AV29Verificador) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Lote_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Lote_Codigo), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16signatureResult", AV16signatureResult);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Verificador", AV29Verificador);
            if ( AV11LoteArquivoAnexo_LoteCod > 0 )
            {
               AV33File.Source = AV35TempImgFile;
               if ( AV33File.Exists() )
               {
                  AV33File.Delete();
               }
               AV33File.Source = AV34tempfile;
               if ( AV33File.Exists() )
               {
                  AV33File.Delete();
               }
               AV33File.Source = AV16signatureResult;
               if ( AV33File.Exists() )
               {
                  AV33File.Delete();
               }
               bttButtonsignpdf_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttButtonsignpdf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttButtonsignpdf_Visible), 5, 0)));
               GX_msglist.addItem("Arquivo assinado e guardado com sucesso. C�digo verificador n� "+StringUtil.Trim( AV29Verificador));
            }
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E13H82 */
         E13H82 ();
         if (returnInSub) return;
      }

      protected void E13H82( )
      {
         /* Enter Routine */
      }

      protected void S115( )
      {
         /* 'CERTIFICADODIGITALDOUSUARIO' Routine */
         /* Using cursor H00H87 */
         pr_default.execute(5, new Object[] {AV27Contrato_PrepostoCod});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A57Usuario_PessoaCod = H00H87_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = H00H87_A1Usuario_Codigo[0];
            A1017Usuario_CrtfPath = H00H87_A1017Usuario_CrtfPath[0];
            n1017Usuario_CrtfPath = H00H87_n1017Usuario_CrtfPath[0];
            A58Usuario_PessoaNom = H00H87_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00H87_n58Usuario_PessoaNom[0];
            A58Usuario_PessoaNom = H00H87_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00H87_n58Usuario_PessoaNom[0];
            if ( H00H87_n1017Usuario_CrtfPath[0] || String.IsNullOrEmpty(StringUtil.RTrim( A1017Usuario_CrtfPath)) )
            {
               AV26Pendencia = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Pendencia", AV26Pendencia);
               bttButtonsignpdf_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttButtonsignpdf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttButtonsignpdf_Visible), 5, 0)));
               Gx_msg = "Preposto " + StringUtil.Trim( A58Usuario_PessoaNom) + " sem Certificado Digital no cadastro do Usu�rio!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_msg", Gx_msg);
            }
            else
            {
               if ( ( StringUtil.StrCmp(StringUtil.Substring( A1017Usuario_CrtfPath, 2, 1), ":") == 0 ) || ( StringUtil.StrCmp(StringUtil.Substring( A1017Usuario_CrtfPath, 1, 2), "\\\\") == 0 ) )
               {
                  AV6Certificate = A1017Usuario_CrtfPath;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Certificate", AV6Certificate);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCERTIFICATE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Certificate, ""))));
               }
               else
               {
                  AV6Certificate = AV37Absolutepath + A1017Usuario_CrtfPath;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Certificate", AV6Certificate);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCERTIFICATE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Certificate, ""))));
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(5);
      }

      protected void nextLoad( )
      {
      }

      protected void E14H82( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_H82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Contratada", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_wp_assina_docto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavContratada_codigo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_wp_assina_docto.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Preposto", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_wp_assina_docto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContrato_prepostocod, dynavContrato_prepostocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27Contrato_PrepostoCod), 6, 0)), 1, dynavContrato_prepostocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavContrato_prepostocod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "", true, "HLP_wp_assina_docto.htm");
            dynavContrato_prepostocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27Contrato_PrepostoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContrato_prepostocod_Internalname, "Values", (String)(dynavContrato_prepostocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnsigneddocument_Internalname, AV20unsignedDocument, StringUtil.RTrim( context.localUtil.Format( AV20unsignedDocument, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnsigneddocument_Jsonclick, 0, "Attribute", "", "", "", edtavUnsigneddocument_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Certificate", "left", true, "HLP_wp_assina_docto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSignatureresult_Internalname, AV16signatureResult, StringUtil.RTrim( context.localUtil.Format( AV16signatureResult, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSignatureresult_Jsonclick, 0, "Attribute", "", "", "", edtavSignatureresult_Visible, edtavSignatureresult_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Certificate", "left", true, "HLP_wp_assina_docto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCertificate_Internalname, AV6Certificate, StringUtil.RTrim( context.localUtil.Format( AV6Certificate, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCertificate_Jsonclick, 0, "Attribute", "", "", "", edtavCertificate_Visible, edtavCertificate_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Certificate", "left", true, "HLP_wp_assina_docto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Senha", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_wp_assina_docto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCertificatepwd_Internalname, AV7CertificatePwd, StringUtil.RTrim( context.localUtil.Format( AV7CertificatePwd, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCertificatepwd_Jsonclick, 0, "DescriptionAttribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 100, -1, 0, 0, 1, 0, -1, true, "", "left", true, "HLP_wp_assina_docto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:31px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButtonsignpdf_Internalname, "", "Assinar PDF", bttButtonsignpdf_Jsonclick, 5, bttButtonsignpdf_Tooltiptext, "", StyleString, ClassString, bttButtonsignpdf_Visible, bttButtonsignpdf_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"E\\'SIGN PDF\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_wp_assina_docto.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_wp_assina_docto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:29px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_H82e( true) ;
         }
         else
         {
            wb_table1_2_H82e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV13NomeArqPdf = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13NomeArqPdf", AV13NomeArqPdf);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNOMEARQPDF", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13NomeArqPdf, ""))));
         AV24Lote_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Lote_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Lote_Codigo), "ZZZZZ9")));
         AV29Verificador = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Verificador", AV29Verificador);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAH82( ) ;
         WSH82( ) ;
         WEH82( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621623404");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_assina_docto.js", "?2020621623405");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock5_Internalname = "TEXTBLOCK5";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         dynavContrato_prepostocod_Internalname = "vCONTRATO_PREPOSTOCOD";
         edtavUnsigneddocument_Internalname = "vUNSIGNEDDOCUMENT";
         edtavSignatureresult_Internalname = "vSIGNATURERESULT";
         edtavCertificate_Internalname = "vCERTIFICATE";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavCertificatepwd_Internalname = "vCERTIFICATEPWD";
         bttButtonsignpdf_Internalname = "BUTTONSIGNPDF";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         bttButtonsignpdf_Enabled = 1;
         bttButtonsignpdf_Visible = 1;
         edtavCertificatepwd_Jsonclick = "";
         edtavCertificate_Jsonclick = "";
         edtavCertificate_Enabled = 1;
         edtavSignatureresult_Jsonclick = "";
         edtavSignatureresult_Enabled = 1;
         edtavUnsigneddocument_Jsonclick = "";
         dynavContrato_prepostocod_Jsonclick = "";
         dynavContrato_prepostocod.Enabled = 1;
         dynavContratada_codigo_Jsonclick = "";
         dynavContratada_codigo.Enabled = 1;
         bttButtonsignpdf_Tooltiptext = "Assinar PDF";
         edtavCertificate_Visible = 1;
         edtavSignatureresult_Visible = 1;
         edtavUnsigneddocument_Visible = 1;
         Form.Caption = "Assinatura de Documento";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'SIGN PDF'","{handler:'E12H82',iparms:[{av:'AV20unsignedDocument',fld:'vUNSIGNEDDOCUMENT',pic:'',nv:''},{av:'AV6Certificate',fld:'vCERTIFICATE',pic:'',hsh:true,nv:''},{av:'AV7CertificatePwd',fld:'vCERTIFICATEPWD',pic:'',nv:''},{av:'AV16signatureResult',fld:'vSIGNATURERESULT',pic:'',nv:''},{av:'AV13NomeArqPdf',fld:'vNOMEARQPDF',pic:'',hsh:true,nv:''},{av:'AV24Lote_Codigo',fld:'vLOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV29Verificador',fld:'vVERIFICADOR',pic:'',nv:''},{av:'AV35TempImgFile',fld:'vTEMPIMGFILE',pic:'',nv:''},{av:'AV34tempfile',fld:'vTEMPFILE',pic:'',nv:''}],oparms:[{av:'Gx_msg',fld:'vMSG',pic:'',nv:''},{ctrl:'BUTTONSIGNPDF',prop:'Visible'},{av:'AV29Verificador',fld:'vVERIFICADOR',pic:'',nv:''}]}");
         setEventMetadata("ENTER","{handler:'E13H82',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV13NomeArqPdf = "";
         wcpOAV29Verificador = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV25WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV40Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV35TempImgFile = "";
         AV34tempfile = "";
         AV6Certificate = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00H82_A40Contratada_PessoaCod = new int[1] ;
         H00H82_A39Contratada_Codigo = new int[1] ;
         H00H82_A41Contratada_PessoaNom = new String[] {""} ;
         H00H82_n41Contratada_PessoaNom = new bool[] {false} ;
         H00H82_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00H83_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00H83_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00H83_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00H83_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00H83_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00H83_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         AV20unsignedDocument = "";
         AV16signatureResult = "";
         AV7CertificatePwd = "";
         hsh = "";
         AV37Absolutepath = "";
         H00H84_A330ParametrosSistema_Codigo = new int[1] ;
         H00H84_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         H00H84_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         A1021ParametrosSistema_PathCrtf = "";
         Gx_msg = "";
         H00H85_A456ContagemResultado_Codigo = new int[1] ;
         H00H85_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         H00H85_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         H00H85_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00H85_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00H86_A74Contrato_Codigo = new int[1] ;
         H00H86_A39Contratada_Codigo = new int[1] ;
         H00H86_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00H86_A1013Contrato_PrepostoCod = new int[1] ;
         H00H86_n1013Contrato_PrepostoCod = new bool[] {false} ;
         AV15ServerSideSign = new SdtServerSideSign(context);
         AV23UploadPDF = "";
         AV30NomeArq = "";
         AV33File = new GxFile(context.GetPhysicalPath());
         H00H87_A57Usuario_PessoaCod = new int[1] ;
         H00H87_A1Usuario_Codigo = new int[1] ;
         H00H87_A1017Usuario_CrtfPath = new String[] {""} ;
         H00H87_n1017Usuario_CrtfPath = new bool[] {false} ;
         H00H87_A58Usuario_PessoaNom = new String[] {""} ;
         H00H87_n58Usuario_PessoaNom = new bool[] {false} ;
         A1017Usuario_CrtfPath = "";
         A58Usuario_PessoaNom = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock5_Jsonclick = "";
         TempTags = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         bttButtonsignpdf_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_assina_docto__default(),
            new Object[][] {
                new Object[] {
               H00H82_A40Contratada_PessoaCod, H00H82_A39Contratada_Codigo, H00H82_A41Contratada_PessoaNom, H00H82_n41Contratada_PessoaNom, H00H82_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00H83_A70ContratadaUsuario_UsuarioPessoaCod, H00H83_n70ContratadaUsuario_UsuarioPessoaCod, H00H83_A66ContratadaUsuario_ContratadaCod, H00H83_A69ContratadaUsuario_UsuarioCod, H00H83_A71ContratadaUsuario_UsuarioPessoaNom, H00H83_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00H84_A330ParametrosSistema_Codigo, H00H84_A1021ParametrosSistema_PathCrtf, H00H84_n1021ParametrosSistema_PathCrtf
               }
               , new Object[] {
               H00H85_A456ContagemResultado_Codigo, H00H85_A597ContagemResultado_LoteAceiteCod, H00H85_n597ContagemResultado_LoteAceiteCod, H00H85_A490ContagemResultado_ContratadaCod, H00H85_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               H00H86_A74Contrato_Codigo, H00H86_A39Contratada_Codigo, H00H86_A75Contrato_AreaTrabalhoCod, H00H86_A1013Contrato_PrepostoCod, H00H86_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               H00H87_A57Usuario_PessoaCod, H00H87_A1Usuario_Codigo, H00H87_A1017Usuario_CrtfPath, H00H87_n1017Usuario_CrtfPath, H00H87_A58Usuario_PessoaNom, H00H87_n58Usuario_PessoaNom
               }
            }
         );
         AV40Pgmname = "wp_assina_docto";
         /* GeneXus formulas. */
         AV40Pgmname = "wp_assina_docto";
         context.Gx_err = 0;
         dynavContratada_codigo.Enabled = 0;
         dynavContrato_prepostocod.Enabled = 0;
         edtavCertificate_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV31i ;
      private short nGXWrapped ;
      private int AV24Lote_Codigo ;
      private int wcpOAV24Lote_Codigo ;
      private int edtavCertificate_Enabled ;
      private int AV28Contratada_Codigo ;
      private int AV27Contrato_PrepostoCod ;
      private int gxdynajaxindex ;
      private int edtavUnsigneddocument_Visible ;
      private int edtavSignatureresult_Visible ;
      private int edtavCertificate_Visible ;
      private int bttButtonsignpdf_Visible ;
      private int A330ParametrosSistema_Codigo ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A39Contratada_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A1013Contrato_PrepostoCod ;
      private int bttButtonsignpdf_Enabled ;
      private int AV11LoteArquivoAnexo_LoteCod ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int edtavSignatureresult_Enabled ;
      private int idxLst ;
      private String AV13NomeArqPdf ;
      private String wcpOAV13NomeArqPdf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String AV40Pgmname ;
      private String dynavContratada_codigo_Internalname ;
      private String dynavContrato_prepostocod_Internalname ;
      private String edtavCertificate_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV35TempImgFile ;
      private String AV34tempfile ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavUnsigneddocument_Internalname ;
      private String edtavSignatureresult_Internalname ;
      private String edtavCertificatepwd_Internalname ;
      private String hsh ;
      private String AV37Absolutepath ;
      private String bttButtonsignpdf_Internalname ;
      private String Gx_msg ;
      private String bttButtonsignpdf_Tooltiptext ;
      private String AV30NomeArq ;
      private String A58Usuario_PessoaNom ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String TempTags ;
      private String dynavContratada_codigo_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String dynavContrato_prepostocod_Jsonclick ;
      private String edtavUnsigneddocument_Jsonclick ;
      private String edtavSignatureresult_Jsonclick ;
      private String edtavCertificate_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavCertificatepwd_Jsonclick ;
      private String bttButtonsignpdf_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1021ParametrosSistema_PathCrtf ;
      private bool AV26Pendencia ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1017Usuario_CrtfPath ;
      private bool n58Usuario_PessoaNom ;
      private String AV29Verificador ;
      private String wcpOAV29Verificador ;
      private String AV6Certificate ;
      private String AV20unsignedDocument ;
      private String AV16signatureResult ;
      private String AV7CertificatePwd ;
      private String A1021ParametrosSistema_PathCrtf ;
      private String A1017Usuario_CrtfPath ;
      private String AV23UploadPDF ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private SdtServerSideSign AV15ServerSideSign ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContratada_codigo ;
      private GXCombobox dynavContrato_prepostocod ;
      private IDataStoreProvider pr_default ;
      private int[] H00H82_A40Contratada_PessoaCod ;
      private int[] H00H82_A39Contratada_Codigo ;
      private String[] H00H82_A41Contratada_PessoaNom ;
      private bool[] H00H82_n41Contratada_PessoaNom ;
      private int[] H00H82_A52Contratada_AreaTrabalhoCod ;
      private int[] H00H83_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00H83_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00H83_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00H83_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00H83_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00H83_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00H84_A330ParametrosSistema_Codigo ;
      private String[] H00H84_A1021ParametrosSistema_PathCrtf ;
      private bool[] H00H84_n1021ParametrosSistema_PathCrtf ;
      private int[] H00H85_A456ContagemResultado_Codigo ;
      private int[] H00H85_A597ContagemResultado_LoteAceiteCod ;
      private bool[] H00H85_n597ContagemResultado_LoteAceiteCod ;
      private int[] H00H85_A490ContagemResultado_ContratadaCod ;
      private bool[] H00H85_n490ContagemResultado_ContratadaCod ;
      private int[] H00H86_A74Contrato_Codigo ;
      private int[] H00H86_A39Contratada_Codigo ;
      private int[] H00H86_A75Contrato_AreaTrabalhoCod ;
      private int[] H00H86_A1013Contrato_PrepostoCod ;
      private bool[] H00H86_n1013Contrato_PrepostoCod ;
      private int[] H00H87_A57Usuario_PessoaCod ;
      private int[] H00H87_A1Usuario_Codigo ;
      private String[] H00H87_A1017Usuario_CrtfPath ;
      private bool[] H00H87_n1017Usuario_CrtfPath ;
      private String[] H00H87_A58Usuario_PessoaNom ;
      private bool[] H00H87_n58Usuario_PessoaNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxFile AV33File ;
      private wwpbaseobjects.SdtWWPContext AV25WWPContext ;
   }

   public class wp_assina_docto__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00H82 ;
          prmH00H82 = new Object[] {
          new Object[] {"@AV25WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00H83 ;
          prmH00H83 = new Object[] {
          } ;
          Object[] prmH00H84 ;
          prmH00H84 = new Object[] {
          } ;
          Object[] prmH00H85 ;
          prmH00H85 = new Object[] {
          new Object[] {"@AV24Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00H86 ;
          prmH00H86 = new Object[] {
          new Object[] {"@AV25WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00H87 ;
          prmH00H87 = new Object[] {
          new Object[] {"@AV27Contrato_PrepostoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00H82", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV25WWPC_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H82,0,0,true,false )
             ,new CursorDef("H00H83", "SELECT T3.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H83,0,0,true,false )
             ,new CursorDef("H00H84", "SELECT [ParametrosSistema_Codigo], [ParametrosSistema_PathCrtf] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = 1 ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H84,1,0,false,true )
             ,new CursorDef("H00H85", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_LoteAceiteCod], [ContagemResultado_ContratadaCod] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV24Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H85,1,0,false,true )
             ,new CursorDef("H00H86", "SELECT [Contrato_Codigo], [Contratada_Codigo], [Contrato_AreaTrabalhoCod], [Contrato_PrepostoCod] FROM [Contrato] WITH (NOLOCK) WHERE ([Contrato_AreaTrabalhoCod] = @AV25WWPC_1Areatrabalho_codigo) AND ([Contratada_Codigo] = @AV28Contratada_Codigo) ORDER BY [Contrato_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H86,100,0,true,false )
             ,new CursorDef("H00H87", "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T1.[Usuario_CrtfPath], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV27Contrato_PrepostoCod ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H87,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
