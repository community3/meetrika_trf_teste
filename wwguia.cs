/*
               File: WWGuia
        Description:  Guia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:59:29.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwguia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwguia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwguia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_89 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_89_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_89_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Guia_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Guia_Nome1", AV17Guia_Nome1);
               AV31Guia_Codigo1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Guia_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Guia_Codigo1), 6, 0)));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21Guia_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Guia_Nome2", AV21Guia_Nome2);
               AV32Guia_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Guia_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Guia_Codigo2), 6, 0)));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25Guia_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Guia_Nome3", AV25Guia_Nome3);
               AV33Guia_Codigo3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Guia_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Guia_Codigo3), 6, 0)));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV58TFGuia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFGuia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFGuia_Codigo), 6, 0)));
               AV59TFGuia_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFGuia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFGuia_Codigo_To), 6, 0)));
               AV62TFGuia_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFGuia_Nome", AV62TFGuia_Nome);
               AV63TFGuia_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFGuia_Nome_Sel", AV63TFGuia_Nome_Sel);
               AV66TFGuia_Versao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFGuia_Versao", AV66TFGuia_Versao);
               AV67TFGuia_Versao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFGuia_Versao_Sel", AV67TFGuia_Versao_Sel);
               AV70TFGuia_Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFGuia_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70TFGuia_Ano), 4, 0)));
               AV71TFGuia_Ano_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFGuia_Ano_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71TFGuia_Ano_To), 4, 0)));
               AV60ddo_Guia_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_Guia_CodigoTitleControlIdToReplace", AV60ddo_Guia_CodigoTitleControlIdToReplace);
               AV64ddo_Guia_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_Guia_NomeTitleControlIdToReplace", AV64ddo_Guia_NomeTitleControlIdToReplace);
               AV68ddo_Guia_VersaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Guia_VersaoTitleControlIdToReplace", AV68ddo_Guia_VersaoTitleControlIdToReplace);
               AV72ddo_Guia_AnoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Guia_AnoTitleControlIdToReplace", AV72ddo_Guia_AnoTitleControlIdToReplace);
               AV110Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A93Guia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Guia_Nome1, AV31Guia_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Guia_Nome2, AV32Guia_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Guia_Nome3, AV33Guia_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV58TFGuia_Codigo, AV59TFGuia_Codigo_To, AV62TFGuia_Nome, AV63TFGuia_Nome_Sel, AV66TFGuia_Versao, AV67TFGuia_Versao_Sel, AV70TFGuia_Ano, AV71TFGuia_Ano_To, AV60ddo_Guia_CodigoTitleControlIdToReplace, AV64ddo_Guia_NomeTitleControlIdToReplace, AV68ddo_Guia_VersaoTitleControlIdToReplace, AV72ddo_Guia_AnoTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A93Guia_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA5G2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START5G2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812593048");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwguia.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vGUIA_NOME1", StringUtil.RTrim( AV17Guia_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vGUIA_CODIGO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31Guia_Codigo1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vGUIA_NOME2", StringUtil.RTrim( AV21Guia_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vGUIA_CODIGO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32Guia_Codigo2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vGUIA_NOME3", StringUtil.RTrim( AV25Guia_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vGUIA_CODIGO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33Guia_Codigo3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFGuia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFGuia_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_NOME", StringUtil.RTrim( AV62TFGuia_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_NOME_SEL", StringUtil.RTrim( AV63TFGuia_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_VERSAO", StringUtil.RTrim( AV66TFGuia_Versao));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_VERSAO_SEL", StringUtil.RTrim( AV67TFGuia_Versao_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_ANO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70TFGuia_Ano), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_ANO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71TFGuia_Ano_To), 4, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_89", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_89), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV73DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV73DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGUIA_CODIGOTITLEFILTERDATA", AV57Guia_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGUIA_CODIGOTITLEFILTERDATA", AV57Guia_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGUIA_NOMETITLEFILTERDATA", AV61Guia_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGUIA_NOMETITLEFILTERDATA", AV61Guia_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGUIA_VERSAOTITLEFILTERDATA", AV65Guia_VersaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGUIA_VERSAOTITLEFILTERDATA", AV65Guia_VersaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGUIA_ANOTITLEFILTERDATA", AV69Guia_AnoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGUIA_ANOTITLEFILTERDATA", AV69Guia_AnoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV110Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Caption", StringUtil.RTrim( Ddo_guia_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_guia_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Cls", StringUtil.RTrim( Ddo_guia_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_guia_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_guia_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_guia_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_guia_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_guia_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_guia_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_guia_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_guia_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_guia_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_guia_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_guia_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_guia_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_guia_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_guia_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_guia_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_guia_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_guia_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Caption", StringUtil.RTrim( Ddo_guia_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Tooltip", StringUtil.RTrim( Ddo_guia_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Cls", StringUtil.RTrim( Ddo_guia_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_guia_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_guia_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_guia_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_guia_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_guia_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_guia_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Sortedstatus", StringUtil.RTrim( Ddo_guia_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Includefilter", StringUtil.BoolToStr( Ddo_guia_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Filtertype", StringUtil.RTrim( Ddo_guia_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_guia_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_guia_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Datalisttype", StringUtil.RTrim( Ddo_guia_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Datalistproc", StringUtil.RTrim( Ddo_guia_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_guia_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Sortasc", StringUtil.RTrim( Ddo_guia_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Sortdsc", StringUtil.RTrim( Ddo_guia_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Loadingdata", StringUtil.RTrim( Ddo_guia_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Cleanfilter", StringUtil.RTrim( Ddo_guia_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Noresultsfound", StringUtil.RTrim( Ddo_guia_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_guia_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Caption", StringUtil.RTrim( Ddo_guia_versao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Tooltip", StringUtil.RTrim( Ddo_guia_versao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Cls", StringUtil.RTrim( Ddo_guia_versao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Filteredtext_set", StringUtil.RTrim( Ddo_guia_versao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Selectedvalue_set", StringUtil.RTrim( Ddo_guia_versao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_guia_versao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_guia_versao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Includesortasc", StringUtil.BoolToStr( Ddo_guia_versao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Includesortdsc", StringUtil.BoolToStr( Ddo_guia_versao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Sortedstatus", StringUtil.RTrim( Ddo_guia_versao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Includefilter", StringUtil.BoolToStr( Ddo_guia_versao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Filtertype", StringUtil.RTrim( Ddo_guia_versao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Filterisrange", StringUtil.BoolToStr( Ddo_guia_versao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Includedatalist", StringUtil.BoolToStr( Ddo_guia_versao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Datalisttype", StringUtil.RTrim( Ddo_guia_versao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Datalistproc", StringUtil.RTrim( Ddo_guia_versao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_guia_versao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Sortasc", StringUtil.RTrim( Ddo_guia_versao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Sortdsc", StringUtil.RTrim( Ddo_guia_versao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Loadingdata", StringUtil.RTrim( Ddo_guia_versao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Cleanfilter", StringUtil.RTrim( Ddo_guia_versao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Noresultsfound", StringUtil.RTrim( Ddo_guia_versao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Searchbuttontext", StringUtil.RTrim( Ddo_guia_versao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Caption", StringUtil.RTrim( Ddo_guia_ano_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Tooltip", StringUtil.RTrim( Ddo_guia_ano_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Cls", StringUtil.RTrim( Ddo_guia_ano_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Filteredtext_set", StringUtil.RTrim( Ddo_guia_ano_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Filteredtextto_set", StringUtil.RTrim( Ddo_guia_ano_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Dropdownoptionstype", StringUtil.RTrim( Ddo_guia_ano_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_guia_ano_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Includesortasc", StringUtil.BoolToStr( Ddo_guia_ano_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Includesortdsc", StringUtil.BoolToStr( Ddo_guia_ano_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Sortedstatus", StringUtil.RTrim( Ddo_guia_ano_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Includefilter", StringUtil.BoolToStr( Ddo_guia_ano_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Filtertype", StringUtil.RTrim( Ddo_guia_ano_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Filterisrange", StringUtil.BoolToStr( Ddo_guia_ano_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Includedatalist", StringUtil.BoolToStr( Ddo_guia_ano_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Sortasc", StringUtil.RTrim( Ddo_guia_ano_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Sortdsc", StringUtil.RTrim( Ddo_guia_ano_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Cleanfilter", StringUtil.RTrim( Ddo_guia_ano_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Rangefilterfrom", StringUtil.RTrim( Ddo_guia_ano_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Rangefilterto", StringUtil.RTrim( Ddo_guia_ano_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Searchbuttontext", StringUtil.RTrim( Ddo_guia_ano_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_guia_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_guia_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_guia_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Activeeventkey", StringUtil.RTrim( Ddo_guia_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_guia_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_guia_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Activeeventkey", StringUtil.RTrim( Ddo_guia_versao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Filteredtext_get", StringUtil.RTrim( Ddo_guia_versao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_VERSAO_Selectedvalue_get", StringUtil.RTrim( Ddo_guia_versao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Activeeventkey", StringUtil.RTrim( Ddo_guia_ano_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Filteredtext_get", StringUtil.RTrim( Ddo_guia_ano_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_ANO_Filteredtextto_get", StringUtil.RTrim( Ddo_guia_ano_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE5G2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT5G2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwguia.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWGuia" ;
      }

      public override String GetPgmdesc( )
      {
         return " Guia" ;
      }

      protected void WB5G0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_5G2( true) ;
         }
         else
         {
            wb_table1_2_5G2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_5G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(102, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(103, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFGuia_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58TFGuia_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGuia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFGuia_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV59TFGuia_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGuia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_nome_Internalname, StringUtil.RTrim( AV62TFGuia_Nome), StringUtil.RTrim( context.localUtil.Format( AV62TFGuia_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGuia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_nome_sel_Internalname, StringUtil.RTrim( AV63TFGuia_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV63TFGuia_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGuia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_versao_Internalname, StringUtil.RTrim( AV66TFGuia_Versao), StringUtil.RTrim( context.localUtil.Format( AV66TFGuia_Versao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_versao_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_versao_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGuia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_versao_sel_Internalname, StringUtil.RTrim( AV67TFGuia_Versao_Sel), StringUtil.RTrim( context.localUtil.Format( AV67TFGuia_Versao_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_versao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_versao_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGuia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70TFGuia_Ano), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV70TFGuia_Ano), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_ano_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_ano_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGuia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_ano_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71TFGuia_Ano_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV71TFGuia_Ano_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_ano_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_ano_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGuia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GUIA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_guia_codigotitlecontrolidtoreplace_Internalname, AV60ddo_Guia_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", 0, edtavDdo_guia_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGuia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GUIA_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_guia_nometitlecontrolidtoreplace_Internalname, AV64ddo_Guia_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavDdo_guia_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGuia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GUIA_VERSAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_guia_versaotitlecontrolidtoreplace_Internalname, AV68ddo_Guia_VersaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavDdo_guia_versaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGuia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GUIA_ANOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_guia_anotitlecontrolidtoreplace_Internalname, AV72ddo_Guia_AnoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_guia_anotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGuia.htm");
         }
         wbLoad = true;
      }

      protected void START5G2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Guia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP5G0( ) ;
      }

      protected void WS5G2( )
      {
         START5G2( ) ;
         EVT5G2( ) ;
      }

      protected void EVT5G2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E115G2 */
                              E115G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_GUIA_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E125G2 */
                              E125G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_GUIA_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E135G2 */
                              E135G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_GUIA_VERSAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E145G2 */
                              E145G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_GUIA_ANO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E155G2 */
                              E155G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E165G2 */
                              E165G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E175G2 */
                              E175G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E185G2 */
                              E185G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E195G2 */
                              E195G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E205G2 */
                              E205G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E215G2 */
                              E215G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E225G2 */
                              E225G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E235G2 */
                              E235G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E245G2 */
                              E245G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E255G2 */
                              E255G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_89_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
                              SubsflControlProps_892( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV108Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV109Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", "."));
                              A94Guia_Nome = StringUtil.Upper( cgiGet( edtGuia_Nome_Internalname));
                              A230Guia_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtGuia_AreaTrabalhoCod_Internalname), ",", "."));
                              A231Guia_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtGuia_AreaTrabalhoDes_Internalname));
                              n231Guia_AreaTrabalhoDes = false;
                              A95Guia_Versao = cgiGet( edtGuia_Versao_Internalname);
                              A96Guia_Ano = (short)(context.localUtil.CToN( cgiGet( edtGuia_Ano_Internalname), ",", "."));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E265G2 */
                                    E265G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E275G2 */
                                    E275G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E285G2 */
                                    E285G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Guia_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME1"), AV17Guia_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Guia_codigo1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vGUIA_CODIGO1"), ",", ".") != Convert.ToDecimal( AV31Guia_Codigo1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Guia_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME2"), AV21Guia_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Guia_codigo2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vGUIA_CODIGO2"), ",", ".") != Convert.ToDecimal( AV32Guia_Codigo2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Guia_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME3"), AV25Guia_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Guia_codigo3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vGUIA_CODIGO3"), ",", ".") != Convert.ToDecimal( AV33Guia_Codigo3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfguia_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV58TFGuia_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfguia_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV59TFGuia_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfguia_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_NOME"), AV62TFGuia_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfguia_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_NOME_SEL"), AV63TFGuia_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfguia_versao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_VERSAO"), AV66TFGuia_Versao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfguia_versao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_VERSAO_SEL"), AV67TFGuia_Versao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfguia_ano Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_ANO"), ",", ".") != Convert.ToDecimal( AV70TFGuia_Ano )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfguia_ano_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_ANO_TO"), ",", ".") != Convert.ToDecimal( AV71TFGuia_Ano_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE5G2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA5G2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("GUIA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("GUIA_CODIGO", "C�d. do Guia", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("GUIA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("GUIA_CODIGO", "C�d. do Guia", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("GUIA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("GUIA_CODIGO", "C�d. do Guia", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_892( ) ;
         while ( nGXsfl_89_idx <= nRC_GXsfl_89 )
         {
            sendrow_892( ) ;
            nGXsfl_89_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_89_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_89_idx+1));
            sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
            SubsflControlProps_892( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Guia_Nome1 ,
                                       int AV31Guia_Codigo1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21Guia_Nome2 ,
                                       int AV32Guia_Codigo2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25Guia_Nome3 ,
                                       int AV33Guia_Codigo3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV58TFGuia_Codigo ,
                                       int AV59TFGuia_Codigo_To ,
                                       String AV62TFGuia_Nome ,
                                       String AV63TFGuia_Nome_Sel ,
                                       String AV66TFGuia_Versao ,
                                       String AV67TFGuia_Versao_Sel ,
                                       short AV70TFGuia_Ano ,
                                       short AV71TFGuia_Ano_To ,
                                       String AV60ddo_Guia_CodigoTitleControlIdToReplace ,
                                       String AV64ddo_Guia_NomeTitleControlIdToReplace ,
                                       String AV68ddo_Guia_VersaoTitleControlIdToReplace ,
                                       String AV72ddo_Guia_AnoTitleControlIdToReplace ,
                                       String AV110Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A93Guia_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF5G2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_GUIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GUIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A93Guia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_GUIA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A94Guia_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "GUIA_NOME", StringUtil.RTrim( A94Guia_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_GUIA_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A230Guia_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GUIA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_GUIA_VERSAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A95Guia_Versao, ""))));
         GxWebStd.gx_hidden_field( context, "GUIA_VERSAO", StringUtil.RTrim( A95Guia_Versao));
         GxWebStd.gx_hidden_field( context, "gxhash_GUIA_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A96Guia_Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "GUIA_ANO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A96Guia_Ano), 4, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF5G2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV110Pgmname = "WWGuia";
         context.Gx_err = 0;
      }

      protected void RF5G2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 89;
         /* Execute user event: E275G2 */
         E275G2 ();
         nGXsfl_89_idx = 1;
         sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
         SubsflControlProps_892( ) ;
         nGXsfl_89_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_892( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV86WWGuiaDS_1_Dynamicfiltersselector1 ,
                                                 AV87WWGuiaDS_2_Dynamicfiltersoperator1 ,
                                                 AV88WWGuiaDS_3_Guia_nome1 ,
                                                 AV89WWGuiaDS_4_Guia_codigo1 ,
                                                 AV90WWGuiaDS_5_Dynamicfiltersenabled2 ,
                                                 AV91WWGuiaDS_6_Dynamicfiltersselector2 ,
                                                 AV92WWGuiaDS_7_Dynamicfiltersoperator2 ,
                                                 AV93WWGuiaDS_8_Guia_nome2 ,
                                                 AV94WWGuiaDS_9_Guia_codigo2 ,
                                                 AV95WWGuiaDS_10_Dynamicfiltersenabled3 ,
                                                 AV96WWGuiaDS_11_Dynamicfiltersselector3 ,
                                                 AV97WWGuiaDS_12_Dynamicfiltersoperator3 ,
                                                 AV98WWGuiaDS_13_Guia_nome3 ,
                                                 AV99WWGuiaDS_14_Guia_codigo3 ,
                                                 AV100WWGuiaDS_15_Tfguia_codigo ,
                                                 AV101WWGuiaDS_16_Tfguia_codigo_to ,
                                                 AV103WWGuiaDS_18_Tfguia_nome_sel ,
                                                 AV102WWGuiaDS_17_Tfguia_nome ,
                                                 AV105WWGuiaDS_20_Tfguia_versao_sel ,
                                                 AV104WWGuiaDS_19_Tfguia_versao ,
                                                 AV106WWGuiaDS_21_Tfguia_ano ,
                                                 AV107WWGuiaDS_22_Tfguia_ano_to ,
                                                 A94Guia_Nome ,
                                                 A93Guia_Codigo ,
                                                 A95Guia_Versao ,
                                                 A96Guia_Ano ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV88WWGuiaDS_3_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV88WWGuiaDS_3_Guia_nome1), 50, "%");
            lV88WWGuiaDS_3_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV88WWGuiaDS_3_Guia_nome1), 50, "%");
            lV93WWGuiaDS_8_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWGuiaDS_8_Guia_nome2), 50, "%");
            lV93WWGuiaDS_8_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWGuiaDS_8_Guia_nome2), 50, "%");
            lV98WWGuiaDS_13_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV98WWGuiaDS_13_Guia_nome3), 50, "%");
            lV98WWGuiaDS_13_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV98WWGuiaDS_13_Guia_nome3), 50, "%");
            lV102WWGuiaDS_17_Tfguia_nome = StringUtil.PadR( StringUtil.RTrim( AV102WWGuiaDS_17_Tfguia_nome), 50, "%");
            lV104WWGuiaDS_19_Tfguia_versao = StringUtil.PadR( StringUtil.RTrim( AV104WWGuiaDS_19_Tfguia_versao), 15, "%");
            /* Using cursor H005G2 */
            pr_default.execute(0, new Object[] {lV88WWGuiaDS_3_Guia_nome1, lV88WWGuiaDS_3_Guia_nome1, AV89WWGuiaDS_4_Guia_codigo1, lV93WWGuiaDS_8_Guia_nome2, lV93WWGuiaDS_8_Guia_nome2, AV94WWGuiaDS_9_Guia_codigo2, lV98WWGuiaDS_13_Guia_nome3, lV98WWGuiaDS_13_Guia_nome3, AV99WWGuiaDS_14_Guia_codigo3, AV100WWGuiaDS_15_Tfguia_codigo, AV101WWGuiaDS_16_Tfguia_codigo_to, lV102WWGuiaDS_17_Tfguia_nome, AV103WWGuiaDS_18_Tfguia_nome_sel, lV104WWGuiaDS_19_Tfguia_versao, AV105WWGuiaDS_20_Tfguia_versao_sel, AV106WWGuiaDS_21_Tfguia_ano, AV107WWGuiaDS_22_Tfguia_ano_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_89_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A96Guia_Ano = H005G2_A96Guia_Ano[0];
               A95Guia_Versao = H005G2_A95Guia_Versao[0];
               A231Guia_AreaTrabalhoDes = H005G2_A231Guia_AreaTrabalhoDes[0];
               n231Guia_AreaTrabalhoDes = H005G2_n231Guia_AreaTrabalhoDes[0];
               A230Guia_AreaTrabalhoCod = H005G2_A230Guia_AreaTrabalhoCod[0];
               A94Guia_Nome = H005G2_A94Guia_Nome[0];
               A93Guia_Codigo = H005G2_A93Guia_Codigo[0];
               A231Guia_AreaTrabalhoDes = H005G2_A231Guia_AreaTrabalhoDes[0];
               n231Guia_AreaTrabalhoDes = H005G2_n231Guia_AreaTrabalhoDes[0];
               /* Execute user event: E285G2 */
               E285G2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 89;
            WB5G0( ) ;
         }
         nGXsfl_89_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV86WWGuiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWGuiaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV88WWGuiaDS_3_Guia_nome1 = AV17Guia_Nome1;
         AV89WWGuiaDS_4_Guia_codigo1 = AV31Guia_Codigo1;
         AV90WWGuiaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV91WWGuiaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV92WWGuiaDS_7_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV93WWGuiaDS_8_Guia_nome2 = AV21Guia_Nome2;
         AV94WWGuiaDS_9_Guia_codigo2 = AV32Guia_Codigo2;
         AV95WWGuiaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV96WWGuiaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV97WWGuiaDS_12_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV98WWGuiaDS_13_Guia_nome3 = AV25Guia_Nome3;
         AV99WWGuiaDS_14_Guia_codigo3 = AV33Guia_Codigo3;
         AV100WWGuiaDS_15_Tfguia_codigo = AV58TFGuia_Codigo;
         AV101WWGuiaDS_16_Tfguia_codigo_to = AV59TFGuia_Codigo_To;
         AV102WWGuiaDS_17_Tfguia_nome = AV62TFGuia_Nome;
         AV103WWGuiaDS_18_Tfguia_nome_sel = AV63TFGuia_Nome_Sel;
         AV104WWGuiaDS_19_Tfguia_versao = AV66TFGuia_Versao;
         AV105WWGuiaDS_20_Tfguia_versao_sel = AV67TFGuia_Versao_Sel;
         AV106WWGuiaDS_21_Tfguia_ano = AV70TFGuia_Ano;
         AV107WWGuiaDS_22_Tfguia_ano_to = AV71TFGuia_Ano_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV86WWGuiaDS_1_Dynamicfiltersselector1 ,
                                              AV87WWGuiaDS_2_Dynamicfiltersoperator1 ,
                                              AV88WWGuiaDS_3_Guia_nome1 ,
                                              AV89WWGuiaDS_4_Guia_codigo1 ,
                                              AV90WWGuiaDS_5_Dynamicfiltersenabled2 ,
                                              AV91WWGuiaDS_6_Dynamicfiltersselector2 ,
                                              AV92WWGuiaDS_7_Dynamicfiltersoperator2 ,
                                              AV93WWGuiaDS_8_Guia_nome2 ,
                                              AV94WWGuiaDS_9_Guia_codigo2 ,
                                              AV95WWGuiaDS_10_Dynamicfiltersenabled3 ,
                                              AV96WWGuiaDS_11_Dynamicfiltersselector3 ,
                                              AV97WWGuiaDS_12_Dynamicfiltersoperator3 ,
                                              AV98WWGuiaDS_13_Guia_nome3 ,
                                              AV99WWGuiaDS_14_Guia_codigo3 ,
                                              AV100WWGuiaDS_15_Tfguia_codigo ,
                                              AV101WWGuiaDS_16_Tfguia_codigo_to ,
                                              AV103WWGuiaDS_18_Tfguia_nome_sel ,
                                              AV102WWGuiaDS_17_Tfguia_nome ,
                                              AV105WWGuiaDS_20_Tfguia_versao_sel ,
                                              AV104WWGuiaDS_19_Tfguia_versao ,
                                              AV106WWGuiaDS_21_Tfguia_ano ,
                                              AV107WWGuiaDS_22_Tfguia_ano_to ,
                                              A94Guia_Nome ,
                                              A93Guia_Codigo ,
                                              A95Guia_Versao ,
                                              A96Guia_Ano ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV88WWGuiaDS_3_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV88WWGuiaDS_3_Guia_nome1), 50, "%");
         lV88WWGuiaDS_3_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV88WWGuiaDS_3_Guia_nome1), 50, "%");
         lV93WWGuiaDS_8_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWGuiaDS_8_Guia_nome2), 50, "%");
         lV93WWGuiaDS_8_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWGuiaDS_8_Guia_nome2), 50, "%");
         lV98WWGuiaDS_13_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV98WWGuiaDS_13_Guia_nome3), 50, "%");
         lV98WWGuiaDS_13_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV98WWGuiaDS_13_Guia_nome3), 50, "%");
         lV102WWGuiaDS_17_Tfguia_nome = StringUtil.PadR( StringUtil.RTrim( AV102WWGuiaDS_17_Tfguia_nome), 50, "%");
         lV104WWGuiaDS_19_Tfguia_versao = StringUtil.PadR( StringUtil.RTrim( AV104WWGuiaDS_19_Tfguia_versao), 15, "%");
         /* Using cursor H005G3 */
         pr_default.execute(1, new Object[] {lV88WWGuiaDS_3_Guia_nome1, lV88WWGuiaDS_3_Guia_nome1, AV89WWGuiaDS_4_Guia_codigo1, lV93WWGuiaDS_8_Guia_nome2, lV93WWGuiaDS_8_Guia_nome2, AV94WWGuiaDS_9_Guia_codigo2, lV98WWGuiaDS_13_Guia_nome3, lV98WWGuiaDS_13_Guia_nome3, AV99WWGuiaDS_14_Guia_codigo3, AV100WWGuiaDS_15_Tfguia_codigo, AV101WWGuiaDS_16_Tfguia_codigo_to, lV102WWGuiaDS_17_Tfguia_nome, AV103WWGuiaDS_18_Tfguia_nome_sel, lV104WWGuiaDS_19_Tfguia_versao, AV105WWGuiaDS_20_Tfguia_versao_sel, AV106WWGuiaDS_21_Tfguia_ano, AV107WWGuiaDS_22_Tfguia_ano_to});
         GRID_nRecordCount = H005G3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV86WWGuiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWGuiaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV88WWGuiaDS_3_Guia_nome1 = AV17Guia_Nome1;
         AV89WWGuiaDS_4_Guia_codigo1 = AV31Guia_Codigo1;
         AV90WWGuiaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV91WWGuiaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV92WWGuiaDS_7_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV93WWGuiaDS_8_Guia_nome2 = AV21Guia_Nome2;
         AV94WWGuiaDS_9_Guia_codigo2 = AV32Guia_Codigo2;
         AV95WWGuiaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV96WWGuiaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV97WWGuiaDS_12_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV98WWGuiaDS_13_Guia_nome3 = AV25Guia_Nome3;
         AV99WWGuiaDS_14_Guia_codigo3 = AV33Guia_Codigo3;
         AV100WWGuiaDS_15_Tfguia_codigo = AV58TFGuia_Codigo;
         AV101WWGuiaDS_16_Tfguia_codigo_to = AV59TFGuia_Codigo_To;
         AV102WWGuiaDS_17_Tfguia_nome = AV62TFGuia_Nome;
         AV103WWGuiaDS_18_Tfguia_nome_sel = AV63TFGuia_Nome_Sel;
         AV104WWGuiaDS_19_Tfguia_versao = AV66TFGuia_Versao;
         AV105WWGuiaDS_20_Tfguia_versao_sel = AV67TFGuia_Versao_Sel;
         AV106WWGuiaDS_21_Tfguia_ano = AV70TFGuia_Ano;
         AV107WWGuiaDS_22_Tfguia_ano_to = AV71TFGuia_Ano_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Guia_Nome1, AV31Guia_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Guia_Nome2, AV32Guia_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Guia_Nome3, AV33Guia_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV58TFGuia_Codigo, AV59TFGuia_Codigo_To, AV62TFGuia_Nome, AV63TFGuia_Nome_Sel, AV66TFGuia_Versao, AV67TFGuia_Versao_Sel, AV70TFGuia_Ano, AV71TFGuia_Ano_To, AV60ddo_Guia_CodigoTitleControlIdToReplace, AV64ddo_Guia_NomeTitleControlIdToReplace, AV68ddo_Guia_VersaoTitleControlIdToReplace, AV72ddo_Guia_AnoTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A93Guia_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV86WWGuiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWGuiaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV88WWGuiaDS_3_Guia_nome1 = AV17Guia_Nome1;
         AV89WWGuiaDS_4_Guia_codigo1 = AV31Guia_Codigo1;
         AV90WWGuiaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV91WWGuiaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV92WWGuiaDS_7_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV93WWGuiaDS_8_Guia_nome2 = AV21Guia_Nome2;
         AV94WWGuiaDS_9_Guia_codigo2 = AV32Guia_Codigo2;
         AV95WWGuiaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV96WWGuiaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV97WWGuiaDS_12_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV98WWGuiaDS_13_Guia_nome3 = AV25Guia_Nome3;
         AV99WWGuiaDS_14_Guia_codigo3 = AV33Guia_Codigo3;
         AV100WWGuiaDS_15_Tfguia_codigo = AV58TFGuia_Codigo;
         AV101WWGuiaDS_16_Tfguia_codigo_to = AV59TFGuia_Codigo_To;
         AV102WWGuiaDS_17_Tfguia_nome = AV62TFGuia_Nome;
         AV103WWGuiaDS_18_Tfguia_nome_sel = AV63TFGuia_Nome_Sel;
         AV104WWGuiaDS_19_Tfguia_versao = AV66TFGuia_Versao;
         AV105WWGuiaDS_20_Tfguia_versao_sel = AV67TFGuia_Versao_Sel;
         AV106WWGuiaDS_21_Tfguia_ano = AV70TFGuia_Ano;
         AV107WWGuiaDS_22_Tfguia_ano_to = AV71TFGuia_Ano_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Guia_Nome1, AV31Guia_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Guia_Nome2, AV32Guia_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Guia_Nome3, AV33Guia_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV58TFGuia_Codigo, AV59TFGuia_Codigo_To, AV62TFGuia_Nome, AV63TFGuia_Nome_Sel, AV66TFGuia_Versao, AV67TFGuia_Versao_Sel, AV70TFGuia_Ano, AV71TFGuia_Ano_To, AV60ddo_Guia_CodigoTitleControlIdToReplace, AV64ddo_Guia_NomeTitleControlIdToReplace, AV68ddo_Guia_VersaoTitleControlIdToReplace, AV72ddo_Guia_AnoTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A93Guia_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV86WWGuiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWGuiaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV88WWGuiaDS_3_Guia_nome1 = AV17Guia_Nome1;
         AV89WWGuiaDS_4_Guia_codigo1 = AV31Guia_Codigo1;
         AV90WWGuiaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV91WWGuiaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV92WWGuiaDS_7_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV93WWGuiaDS_8_Guia_nome2 = AV21Guia_Nome2;
         AV94WWGuiaDS_9_Guia_codigo2 = AV32Guia_Codigo2;
         AV95WWGuiaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV96WWGuiaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV97WWGuiaDS_12_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV98WWGuiaDS_13_Guia_nome3 = AV25Guia_Nome3;
         AV99WWGuiaDS_14_Guia_codigo3 = AV33Guia_Codigo3;
         AV100WWGuiaDS_15_Tfguia_codigo = AV58TFGuia_Codigo;
         AV101WWGuiaDS_16_Tfguia_codigo_to = AV59TFGuia_Codigo_To;
         AV102WWGuiaDS_17_Tfguia_nome = AV62TFGuia_Nome;
         AV103WWGuiaDS_18_Tfguia_nome_sel = AV63TFGuia_Nome_Sel;
         AV104WWGuiaDS_19_Tfguia_versao = AV66TFGuia_Versao;
         AV105WWGuiaDS_20_Tfguia_versao_sel = AV67TFGuia_Versao_Sel;
         AV106WWGuiaDS_21_Tfguia_ano = AV70TFGuia_Ano;
         AV107WWGuiaDS_22_Tfguia_ano_to = AV71TFGuia_Ano_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Guia_Nome1, AV31Guia_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Guia_Nome2, AV32Guia_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Guia_Nome3, AV33Guia_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV58TFGuia_Codigo, AV59TFGuia_Codigo_To, AV62TFGuia_Nome, AV63TFGuia_Nome_Sel, AV66TFGuia_Versao, AV67TFGuia_Versao_Sel, AV70TFGuia_Ano, AV71TFGuia_Ano_To, AV60ddo_Guia_CodigoTitleControlIdToReplace, AV64ddo_Guia_NomeTitleControlIdToReplace, AV68ddo_Guia_VersaoTitleControlIdToReplace, AV72ddo_Guia_AnoTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A93Guia_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV86WWGuiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWGuiaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV88WWGuiaDS_3_Guia_nome1 = AV17Guia_Nome1;
         AV89WWGuiaDS_4_Guia_codigo1 = AV31Guia_Codigo1;
         AV90WWGuiaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV91WWGuiaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV92WWGuiaDS_7_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV93WWGuiaDS_8_Guia_nome2 = AV21Guia_Nome2;
         AV94WWGuiaDS_9_Guia_codigo2 = AV32Guia_Codigo2;
         AV95WWGuiaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV96WWGuiaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV97WWGuiaDS_12_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV98WWGuiaDS_13_Guia_nome3 = AV25Guia_Nome3;
         AV99WWGuiaDS_14_Guia_codigo3 = AV33Guia_Codigo3;
         AV100WWGuiaDS_15_Tfguia_codigo = AV58TFGuia_Codigo;
         AV101WWGuiaDS_16_Tfguia_codigo_to = AV59TFGuia_Codigo_To;
         AV102WWGuiaDS_17_Tfguia_nome = AV62TFGuia_Nome;
         AV103WWGuiaDS_18_Tfguia_nome_sel = AV63TFGuia_Nome_Sel;
         AV104WWGuiaDS_19_Tfguia_versao = AV66TFGuia_Versao;
         AV105WWGuiaDS_20_Tfguia_versao_sel = AV67TFGuia_Versao_Sel;
         AV106WWGuiaDS_21_Tfguia_ano = AV70TFGuia_Ano;
         AV107WWGuiaDS_22_Tfguia_ano_to = AV71TFGuia_Ano_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Guia_Nome1, AV31Guia_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Guia_Nome2, AV32Guia_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Guia_Nome3, AV33Guia_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV58TFGuia_Codigo, AV59TFGuia_Codigo_To, AV62TFGuia_Nome, AV63TFGuia_Nome_Sel, AV66TFGuia_Versao, AV67TFGuia_Versao_Sel, AV70TFGuia_Ano, AV71TFGuia_Ano_To, AV60ddo_Guia_CodigoTitleControlIdToReplace, AV64ddo_Guia_NomeTitleControlIdToReplace, AV68ddo_Guia_VersaoTitleControlIdToReplace, AV72ddo_Guia_AnoTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A93Guia_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV86WWGuiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWGuiaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV88WWGuiaDS_3_Guia_nome1 = AV17Guia_Nome1;
         AV89WWGuiaDS_4_Guia_codigo1 = AV31Guia_Codigo1;
         AV90WWGuiaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV91WWGuiaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV92WWGuiaDS_7_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV93WWGuiaDS_8_Guia_nome2 = AV21Guia_Nome2;
         AV94WWGuiaDS_9_Guia_codigo2 = AV32Guia_Codigo2;
         AV95WWGuiaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV96WWGuiaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV97WWGuiaDS_12_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV98WWGuiaDS_13_Guia_nome3 = AV25Guia_Nome3;
         AV99WWGuiaDS_14_Guia_codigo3 = AV33Guia_Codigo3;
         AV100WWGuiaDS_15_Tfguia_codigo = AV58TFGuia_Codigo;
         AV101WWGuiaDS_16_Tfguia_codigo_to = AV59TFGuia_Codigo_To;
         AV102WWGuiaDS_17_Tfguia_nome = AV62TFGuia_Nome;
         AV103WWGuiaDS_18_Tfguia_nome_sel = AV63TFGuia_Nome_Sel;
         AV104WWGuiaDS_19_Tfguia_versao = AV66TFGuia_Versao;
         AV105WWGuiaDS_20_Tfguia_versao_sel = AV67TFGuia_Versao_Sel;
         AV106WWGuiaDS_21_Tfguia_ano = AV70TFGuia_Ano;
         AV107WWGuiaDS_22_Tfguia_ano_to = AV71TFGuia_Ano_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Guia_Nome1, AV31Guia_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Guia_Nome2, AV32Guia_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Guia_Nome3, AV33Guia_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV58TFGuia_Codigo, AV59TFGuia_Codigo_To, AV62TFGuia_Nome, AV63TFGuia_Nome_Sel, AV66TFGuia_Versao, AV67TFGuia_Versao_Sel, AV70TFGuia_Ano, AV71TFGuia_Ano_To, AV60ddo_Guia_CodigoTitleControlIdToReplace, AV64ddo_Guia_NomeTitleControlIdToReplace, AV68ddo_Guia_VersaoTitleControlIdToReplace, AV72ddo_Guia_AnoTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A93Guia_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP5G0( )
      {
         /* Before Start, stand alone formulas. */
         AV110Pgmname = "WWGuia";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E265G2 */
         E265G2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV73DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vGUIA_CODIGOTITLEFILTERDATA"), AV57Guia_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vGUIA_NOMETITLEFILTERDATA"), AV61Guia_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vGUIA_VERSAOTITLEFILTERDATA"), AV65Guia_VersaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vGUIA_ANOTITLEFILTERDATA"), AV69Guia_AnoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Guia_Nome1 = StringUtil.Upper( cgiGet( edtavGuia_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Guia_Nome1", AV17Guia_Nome1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGuia_codigo1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGuia_codigo1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGUIA_CODIGO1");
               GX_FocusControl = edtavGuia_codigo1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31Guia_Codigo1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Guia_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Guia_Codigo1), 6, 0)));
            }
            else
            {
               AV31Guia_Codigo1 = (int)(context.localUtil.CToN( cgiGet( edtavGuia_codigo1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Guia_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Guia_Codigo1), 6, 0)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21Guia_Nome2 = StringUtil.Upper( cgiGet( edtavGuia_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Guia_Nome2", AV21Guia_Nome2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGuia_codigo2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGuia_codigo2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGUIA_CODIGO2");
               GX_FocusControl = edtavGuia_codigo2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32Guia_Codigo2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Guia_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Guia_Codigo2), 6, 0)));
            }
            else
            {
               AV32Guia_Codigo2 = (int)(context.localUtil.CToN( cgiGet( edtavGuia_codigo2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Guia_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Guia_Codigo2), 6, 0)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25Guia_Nome3 = StringUtil.Upper( cgiGet( edtavGuia_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Guia_Nome3", AV25Guia_Nome3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGuia_codigo3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGuia_codigo3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGUIA_CODIGO3");
               GX_FocusControl = edtavGuia_codigo3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33Guia_Codigo3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Guia_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Guia_Codigo3), 6, 0)));
            }
            else
            {
               AV33Guia_Codigo3 = (int)(context.localUtil.CToN( cgiGet( edtavGuia_codigo3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Guia_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Guia_Codigo3), 6, 0)));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfguia_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfguia_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFGUIA_CODIGO");
               GX_FocusControl = edtavTfguia_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFGuia_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFGuia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFGuia_Codigo), 6, 0)));
            }
            else
            {
               AV58TFGuia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfguia_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFGuia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFGuia_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfguia_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfguia_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFGUIA_CODIGO_TO");
               GX_FocusControl = edtavTfguia_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFGuia_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFGuia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFGuia_Codigo_To), 6, 0)));
            }
            else
            {
               AV59TFGuia_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfguia_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFGuia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFGuia_Codigo_To), 6, 0)));
            }
            AV62TFGuia_Nome = StringUtil.Upper( cgiGet( edtavTfguia_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFGuia_Nome", AV62TFGuia_Nome);
            AV63TFGuia_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfguia_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFGuia_Nome_Sel", AV63TFGuia_Nome_Sel);
            AV66TFGuia_Versao = cgiGet( edtavTfguia_versao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFGuia_Versao", AV66TFGuia_Versao);
            AV67TFGuia_Versao_Sel = cgiGet( edtavTfguia_versao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFGuia_Versao_Sel", AV67TFGuia_Versao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfguia_ano_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfguia_ano_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFGUIA_ANO");
               GX_FocusControl = edtavTfguia_ano_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70TFGuia_Ano = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFGuia_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70TFGuia_Ano), 4, 0)));
            }
            else
            {
               AV70TFGuia_Ano = (short)(context.localUtil.CToN( cgiGet( edtavTfguia_ano_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFGuia_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70TFGuia_Ano), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfguia_ano_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfguia_ano_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFGUIA_ANO_TO");
               GX_FocusControl = edtavTfguia_ano_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71TFGuia_Ano_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFGuia_Ano_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71TFGuia_Ano_To), 4, 0)));
            }
            else
            {
               AV71TFGuia_Ano_To = (short)(context.localUtil.CToN( cgiGet( edtavTfguia_ano_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFGuia_Ano_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71TFGuia_Ano_To), 4, 0)));
            }
            AV60ddo_Guia_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_guia_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_Guia_CodigoTitleControlIdToReplace", AV60ddo_Guia_CodigoTitleControlIdToReplace);
            AV64ddo_Guia_NomeTitleControlIdToReplace = cgiGet( edtavDdo_guia_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_Guia_NomeTitleControlIdToReplace", AV64ddo_Guia_NomeTitleControlIdToReplace);
            AV68ddo_Guia_VersaoTitleControlIdToReplace = cgiGet( edtavDdo_guia_versaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Guia_VersaoTitleControlIdToReplace", AV68ddo_Guia_VersaoTitleControlIdToReplace);
            AV72ddo_Guia_AnoTitleControlIdToReplace = cgiGet( edtavDdo_guia_anotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Guia_AnoTitleControlIdToReplace", AV72ddo_Guia_AnoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_89 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_89"), ",", "."));
            AV75GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV76GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_guia_codigo_Caption = cgiGet( "DDO_GUIA_CODIGO_Caption");
            Ddo_guia_codigo_Tooltip = cgiGet( "DDO_GUIA_CODIGO_Tooltip");
            Ddo_guia_codigo_Cls = cgiGet( "DDO_GUIA_CODIGO_Cls");
            Ddo_guia_codigo_Filteredtext_set = cgiGet( "DDO_GUIA_CODIGO_Filteredtext_set");
            Ddo_guia_codigo_Filteredtextto_set = cgiGet( "DDO_GUIA_CODIGO_Filteredtextto_set");
            Ddo_guia_codigo_Dropdownoptionstype = cgiGet( "DDO_GUIA_CODIGO_Dropdownoptionstype");
            Ddo_guia_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_GUIA_CODIGO_Titlecontrolidtoreplace");
            Ddo_guia_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_CODIGO_Includesortasc"));
            Ddo_guia_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_CODIGO_Includesortdsc"));
            Ddo_guia_codigo_Sortedstatus = cgiGet( "DDO_GUIA_CODIGO_Sortedstatus");
            Ddo_guia_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GUIA_CODIGO_Includefilter"));
            Ddo_guia_codigo_Filtertype = cgiGet( "DDO_GUIA_CODIGO_Filtertype");
            Ddo_guia_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GUIA_CODIGO_Filterisrange"));
            Ddo_guia_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GUIA_CODIGO_Includedatalist"));
            Ddo_guia_codigo_Sortasc = cgiGet( "DDO_GUIA_CODIGO_Sortasc");
            Ddo_guia_codigo_Sortdsc = cgiGet( "DDO_GUIA_CODIGO_Sortdsc");
            Ddo_guia_codigo_Cleanfilter = cgiGet( "DDO_GUIA_CODIGO_Cleanfilter");
            Ddo_guia_codigo_Rangefilterfrom = cgiGet( "DDO_GUIA_CODIGO_Rangefilterfrom");
            Ddo_guia_codigo_Rangefilterto = cgiGet( "DDO_GUIA_CODIGO_Rangefilterto");
            Ddo_guia_codigo_Searchbuttontext = cgiGet( "DDO_GUIA_CODIGO_Searchbuttontext");
            Ddo_guia_nome_Caption = cgiGet( "DDO_GUIA_NOME_Caption");
            Ddo_guia_nome_Tooltip = cgiGet( "DDO_GUIA_NOME_Tooltip");
            Ddo_guia_nome_Cls = cgiGet( "DDO_GUIA_NOME_Cls");
            Ddo_guia_nome_Filteredtext_set = cgiGet( "DDO_GUIA_NOME_Filteredtext_set");
            Ddo_guia_nome_Selectedvalue_set = cgiGet( "DDO_GUIA_NOME_Selectedvalue_set");
            Ddo_guia_nome_Dropdownoptionstype = cgiGet( "DDO_GUIA_NOME_Dropdownoptionstype");
            Ddo_guia_nome_Titlecontrolidtoreplace = cgiGet( "DDO_GUIA_NOME_Titlecontrolidtoreplace");
            Ddo_guia_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_NOME_Includesortasc"));
            Ddo_guia_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_NOME_Includesortdsc"));
            Ddo_guia_nome_Sortedstatus = cgiGet( "DDO_GUIA_NOME_Sortedstatus");
            Ddo_guia_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GUIA_NOME_Includefilter"));
            Ddo_guia_nome_Filtertype = cgiGet( "DDO_GUIA_NOME_Filtertype");
            Ddo_guia_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GUIA_NOME_Filterisrange"));
            Ddo_guia_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GUIA_NOME_Includedatalist"));
            Ddo_guia_nome_Datalisttype = cgiGet( "DDO_GUIA_NOME_Datalisttype");
            Ddo_guia_nome_Datalistproc = cgiGet( "DDO_GUIA_NOME_Datalistproc");
            Ddo_guia_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_GUIA_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_guia_nome_Sortasc = cgiGet( "DDO_GUIA_NOME_Sortasc");
            Ddo_guia_nome_Sortdsc = cgiGet( "DDO_GUIA_NOME_Sortdsc");
            Ddo_guia_nome_Loadingdata = cgiGet( "DDO_GUIA_NOME_Loadingdata");
            Ddo_guia_nome_Cleanfilter = cgiGet( "DDO_GUIA_NOME_Cleanfilter");
            Ddo_guia_nome_Noresultsfound = cgiGet( "DDO_GUIA_NOME_Noresultsfound");
            Ddo_guia_nome_Searchbuttontext = cgiGet( "DDO_GUIA_NOME_Searchbuttontext");
            Ddo_guia_versao_Caption = cgiGet( "DDO_GUIA_VERSAO_Caption");
            Ddo_guia_versao_Tooltip = cgiGet( "DDO_GUIA_VERSAO_Tooltip");
            Ddo_guia_versao_Cls = cgiGet( "DDO_GUIA_VERSAO_Cls");
            Ddo_guia_versao_Filteredtext_set = cgiGet( "DDO_GUIA_VERSAO_Filteredtext_set");
            Ddo_guia_versao_Selectedvalue_set = cgiGet( "DDO_GUIA_VERSAO_Selectedvalue_set");
            Ddo_guia_versao_Dropdownoptionstype = cgiGet( "DDO_GUIA_VERSAO_Dropdownoptionstype");
            Ddo_guia_versao_Titlecontrolidtoreplace = cgiGet( "DDO_GUIA_VERSAO_Titlecontrolidtoreplace");
            Ddo_guia_versao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_VERSAO_Includesortasc"));
            Ddo_guia_versao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_VERSAO_Includesortdsc"));
            Ddo_guia_versao_Sortedstatus = cgiGet( "DDO_GUIA_VERSAO_Sortedstatus");
            Ddo_guia_versao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GUIA_VERSAO_Includefilter"));
            Ddo_guia_versao_Filtertype = cgiGet( "DDO_GUIA_VERSAO_Filtertype");
            Ddo_guia_versao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GUIA_VERSAO_Filterisrange"));
            Ddo_guia_versao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GUIA_VERSAO_Includedatalist"));
            Ddo_guia_versao_Datalisttype = cgiGet( "DDO_GUIA_VERSAO_Datalisttype");
            Ddo_guia_versao_Datalistproc = cgiGet( "DDO_GUIA_VERSAO_Datalistproc");
            Ddo_guia_versao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_GUIA_VERSAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_guia_versao_Sortasc = cgiGet( "DDO_GUIA_VERSAO_Sortasc");
            Ddo_guia_versao_Sortdsc = cgiGet( "DDO_GUIA_VERSAO_Sortdsc");
            Ddo_guia_versao_Loadingdata = cgiGet( "DDO_GUIA_VERSAO_Loadingdata");
            Ddo_guia_versao_Cleanfilter = cgiGet( "DDO_GUIA_VERSAO_Cleanfilter");
            Ddo_guia_versao_Noresultsfound = cgiGet( "DDO_GUIA_VERSAO_Noresultsfound");
            Ddo_guia_versao_Searchbuttontext = cgiGet( "DDO_GUIA_VERSAO_Searchbuttontext");
            Ddo_guia_ano_Caption = cgiGet( "DDO_GUIA_ANO_Caption");
            Ddo_guia_ano_Tooltip = cgiGet( "DDO_GUIA_ANO_Tooltip");
            Ddo_guia_ano_Cls = cgiGet( "DDO_GUIA_ANO_Cls");
            Ddo_guia_ano_Filteredtext_set = cgiGet( "DDO_GUIA_ANO_Filteredtext_set");
            Ddo_guia_ano_Filteredtextto_set = cgiGet( "DDO_GUIA_ANO_Filteredtextto_set");
            Ddo_guia_ano_Dropdownoptionstype = cgiGet( "DDO_GUIA_ANO_Dropdownoptionstype");
            Ddo_guia_ano_Titlecontrolidtoreplace = cgiGet( "DDO_GUIA_ANO_Titlecontrolidtoreplace");
            Ddo_guia_ano_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_ANO_Includesortasc"));
            Ddo_guia_ano_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_ANO_Includesortdsc"));
            Ddo_guia_ano_Sortedstatus = cgiGet( "DDO_GUIA_ANO_Sortedstatus");
            Ddo_guia_ano_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GUIA_ANO_Includefilter"));
            Ddo_guia_ano_Filtertype = cgiGet( "DDO_GUIA_ANO_Filtertype");
            Ddo_guia_ano_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GUIA_ANO_Filterisrange"));
            Ddo_guia_ano_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GUIA_ANO_Includedatalist"));
            Ddo_guia_ano_Sortasc = cgiGet( "DDO_GUIA_ANO_Sortasc");
            Ddo_guia_ano_Sortdsc = cgiGet( "DDO_GUIA_ANO_Sortdsc");
            Ddo_guia_ano_Cleanfilter = cgiGet( "DDO_GUIA_ANO_Cleanfilter");
            Ddo_guia_ano_Rangefilterfrom = cgiGet( "DDO_GUIA_ANO_Rangefilterfrom");
            Ddo_guia_ano_Rangefilterto = cgiGet( "DDO_GUIA_ANO_Rangefilterto");
            Ddo_guia_ano_Searchbuttontext = cgiGet( "DDO_GUIA_ANO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_guia_codigo_Activeeventkey = cgiGet( "DDO_GUIA_CODIGO_Activeeventkey");
            Ddo_guia_codigo_Filteredtext_get = cgiGet( "DDO_GUIA_CODIGO_Filteredtext_get");
            Ddo_guia_codigo_Filteredtextto_get = cgiGet( "DDO_GUIA_CODIGO_Filteredtextto_get");
            Ddo_guia_nome_Activeeventkey = cgiGet( "DDO_GUIA_NOME_Activeeventkey");
            Ddo_guia_nome_Filteredtext_get = cgiGet( "DDO_GUIA_NOME_Filteredtext_get");
            Ddo_guia_nome_Selectedvalue_get = cgiGet( "DDO_GUIA_NOME_Selectedvalue_get");
            Ddo_guia_versao_Activeeventkey = cgiGet( "DDO_GUIA_VERSAO_Activeeventkey");
            Ddo_guia_versao_Filteredtext_get = cgiGet( "DDO_GUIA_VERSAO_Filteredtext_get");
            Ddo_guia_versao_Selectedvalue_get = cgiGet( "DDO_GUIA_VERSAO_Selectedvalue_get");
            Ddo_guia_ano_Activeeventkey = cgiGet( "DDO_GUIA_ANO_Activeeventkey");
            Ddo_guia_ano_Filteredtext_get = cgiGet( "DDO_GUIA_ANO_Filteredtext_get");
            Ddo_guia_ano_Filteredtextto_get = cgiGet( "DDO_GUIA_ANO_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME1"), AV17Guia_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vGUIA_CODIGO1"), ",", ".") != Convert.ToDecimal( AV31Guia_Codigo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME2"), AV21Guia_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vGUIA_CODIGO2"), ",", ".") != Convert.ToDecimal( AV32Guia_Codigo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME3"), AV25Guia_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vGUIA_CODIGO3"), ",", ".") != Convert.ToDecimal( AV33Guia_Codigo3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV58TFGuia_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV59TFGuia_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_NOME"), AV62TFGuia_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_NOME_SEL"), AV63TFGuia_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_VERSAO"), AV66TFGuia_Versao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_VERSAO_SEL"), AV67TFGuia_Versao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_ANO"), ",", ".") != Convert.ToDecimal( AV70TFGuia_Ano )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_ANO_TO"), ",", ".") != Convert.ToDecimal( AV71TFGuia_Ano_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E265G2 */
         E265G2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E265G2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "GUIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "GUIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "GUIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfguia_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_codigo_Visible), 5, 0)));
         edtavTfguia_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_codigo_to_Visible), 5, 0)));
         edtavTfguia_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_nome_Visible), 5, 0)));
         edtavTfguia_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_nome_sel_Visible), 5, 0)));
         edtavTfguia_versao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_versao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_versao_Visible), 5, 0)));
         edtavTfguia_versao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_versao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_versao_sel_Visible), 5, 0)));
         edtavTfguia_ano_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_ano_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_ano_Visible), 5, 0)));
         edtavTfguia_ano_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_ano_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_ano_to_Visible), 5, 0)));
         Ddo_guia_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Guia_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "TitleControlIdToReplace", Ddo_guia_codigo_Titlecontrolidtoreplace);
         AV60ddo_Guia_CodigoTitleControlIdToReplace = Ddo_guia_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_Guia_CodigoTitleControlIdToReplace", AV60ddo_Guia_CodigoTitleControlIdToReplace);
         edtavDdo_guia_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_guia_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_guia_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_guia_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Guia_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "TitleControlIdToReplace", Ddo_guia_nome_Titlecontrolidtoreplace);
         AV64ddo_Guia_NomeTitleControlIdToReplace = Ddo_guia_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_Guia_NomeTitleControlIdToReplace", AV64ddo_Guia_NomeTitleControlIdToReplace);
         edtavDdo_guia_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_guia_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_guia_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_guia_versao_Titlecontrolidtoreplace = subGrid_Internalname+"_Guia_Versao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_versao_Internalname, "TitleControlIdToReplace", Ddo_guia_versao_Titlecontrolidtoreplace);
         AV68ddo_Guia_VersaoTitleControlIdToReplace = Ddo_guia_versao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Guia_VersaoTitleControlIdToReplace", AV68ddo_Guia_VersaoTitleControlIdToReplace);
         edtavDdo_guia_versaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_guia_versaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_guia_versaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_guia_ano_Titlecontrolidtoreplace = subGrid_Internalname+"_Guia_Ano";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_ano_Internalname, "TitleControlIdToReplace", Ddo_guia_ano_Titlecontrolidtoreplace);
         AV72ddo_Guia_AnoTitleControlIdToReplace = Ddo_guia_ano_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Guia_AnoTitleControlIdToReplace", AV72ddo_Guia_AnoTitleControlIdToReplace);
         edtavDdo_guia_anotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_guia_anotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_guia_anotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Guia";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Recentes", 0);
         cmbavOrderedby.addItem("3", "Vers�o", 0);
         cmbavOrderedby.addItem("4", "Ano", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV73DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV73DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E275G2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV57Guia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61Guia_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65Guia_VersaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69Guia_AnoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_CODIGO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "=", 0);
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "GUIA_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "GUIA_CODIGO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "=", 0);
            }
            if ( AV22DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GUIA_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GUIA_CODIGO") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "=", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtGuia_Codigo_Titleformat = 2;
         edtGuia_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV60ddo_Guia_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Codigo_Internalname, "Title", edtGuia_Codigo_Title);
         edtGuia_Nome_Titleformat = 2;
         edtGuia_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV64ddo_Guia_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Nome_Internalname, "Title", edtGuia_Nome_Title);
         edtGuia_Versao_Titleformat = 2;
         edtGuia_Versao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Vers�o", AV68ddo_Guia_VersaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Versao_Internalname, "Title", edtGuia_Versao_Title);
         edtGuia_Ano_Titleformat = 2;
         edtGuia_Ano_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ano", AV72ddo_Guia_AnoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Ano_Internalname, "Title", edtGuia_Ano_Title);
         AV75GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75GridCurrentPage), 10, 0)));
         AV76GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76GridPageCount), 10, 0)));
         AV86WWGuiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWGuiaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV88WWGuiaDS_3_Guia_nome1 = AV17Guia_Nome1;
         AV89WWGuiaDS_4_Guia_codigo1 = AV31Guia_Codigo1;
         AV90WWGuiaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV91WWGuiaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV92WWGuiaDS_7_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV93WWGuiaDS_8_Guia_nome2 = AV21Guia_Nome2;
         AV94WWGuiaDS_9_Guia_codigo2 = AV32Guia_Codigo2;
         AV95WWGuiaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV96WWGuiaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV97WWGuiaDS_12_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV98WWGuiaDS_13_Guia_nome3 = AV25Guia_Nome3;
         AV99WWGuiaDS_14_Guia_codigo3 = AV33Guia_Codigo3;
         AV100WWGuiaDS_15_Tfguia_codigo = AV58TFGuia_Codigo;
         AV101WWGuiaDS_16_Tfguia_codigo_to = AV59TFGuia_Codigo_To;
         AV102WWGuiaDS_17_Tfguia_nome = AV62TFGuia_Nome;
         AV103WWGuiaDS_18_Tfguia_nome_sel = AV63TFGuia_Nome_Sel;
         AV104WWGuiaDS_19_Tfguia_versao = AV66TFGuia_Versao;
         AV105WWGuiaDS_20_Tfguia_versao_sel = AV67TFGuia_Versao_Sel;
         AV106WWGuiaDS_21_Tfguia_ano = AV70TFGuia_Ano;
         AV107WWGuiaDS_22_Tfguia_ano_to = AV71TFGuia_Ano_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57Guia_CodigoTitleFilterData", AV57Guia_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV61Guia_NomeTitleFilterData", AV61Guia_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV65Guia_VersaoTitleFilterData", AV65Guia_VersaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69Guia_AnoTitleFilterData", AV69Guia_AnoTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E115G2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV74PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV74PageToGo) ;
         }
      }

      protected void E125G2( )
      {
         /* Ddo_guia_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_guia_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "SortedStatus", Ddo_guia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "SortedStatus", Ddo_guia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV58TFGuia_Codigo = (int)(NumberUtil.Val( Ddo_guia_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFGuia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFGuia_Codigo), 6, 0)));
            AV59TFGuia_Codigo_To = (int)(NumberUtil.Val( Ddo_guia_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFGuia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFGuia_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E135G2( )
      {
         /* Ddo_guia_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_guia_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "SortedStatus", Ddo_guia_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "SortedStatus", Ddo_guia_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV62TFGuia_Nome = Ddo_guia_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFGuia_Nome", AV62TFGuia_Nome);
            AV63TFGuia_Nome_Sel = Ddo_guia_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFGuia_Nome_Sel", AV63TFGuia_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E145G2( )
      {
         /* Ddo_guia_versao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_guia_versao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_versao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_versao_Internalname, "SortedStatus", Ddo_guia_versao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_versao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_versao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_versao_Internalname, "SortedStatus", Ddo_guia_versao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_versao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV66TFGuia_Versao = Ddo_guia_versao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFGuia_Versao", AV66TFGuia_Versao);
            AV67TFGuia_Versao_Sel = Ddo_guia_versao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFGuia_Versao_Sel", AV67TFGuia_Versao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E155G2( )
      {
         /* Ddo_guia_ano_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_guia_ano_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_ano_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_ano_Internalname, "SortedStatus", Ddo_guia_ano_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_ano_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_ano_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_ano_Internalname, "SortedStatus", Ddo_guia_ano_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_ano_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFGuia_Ano = (short)(NumberUtil.Val( Ddo_guia_ano_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFGuia_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70TFGuia_Ano), 4, 0)));
            AV71TFGuia_Ano_To = (short)(NumberUtil.Val( Ddo_guia_ano_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFGuia_Ano_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71TFGuia_Ano_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E285G2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV108Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("guia.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A93Guia_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV109Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("guia.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A93Guia_Codigo);
         edtGuia_Nome_Link = formatLink("viewguia.aspx") + "?" + UrlEncode("" +A93Guia_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 89;
         }
         sendrow_892( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_89_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(89, GridRow);
         }
      }

      protected void E165G2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E215G2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Guia_Nome1, AV31Guia_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Guia_Nome2, AV32Guia_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Guia_Nome3, AV33Guia_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV58TFGuia_Codigo, AV59TFGuia_Codigo_To, AV62TFGuia_Nome, AV63TFGuia_Nome_Sel, AV66TFGuia_Versao, AV67TFGuia_Versao_Sel, AV70TFGuia_Ano, AV71TFGuia_Ano_To, AV60ddo_Guia_CodigoTitleControlIdToReplace, AV64ddo_Guia_NomeTitleControlIdToReplace, AV68ddo_Guia_VersaoTitleControlIdToReplace, AV72ddo_Guia_AnoTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A93Guia_Codigo) ;
      }

      protected void E175G2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Guia_Nome1, AV31Guia_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Guia_Nome2, AV32Guia_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Guia_Nome3, AV33Guia_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV58TFGuia_Codigo, AV59TFGuia_Codigo_To, AV62TFGuia_Nome, AV63TFGuia_Nome_Sel, AV66TFGuia_Versao, AV67TFGuia_Versao_Sel, AV70TFGuia_Ano, AV71TFGuia_Ano_To, AV60ddo_Guia_CodigoTitleControlIdToReplace, AV64ddo_Guia_NomeTitleControlIdToReplace, AV68ddo_Guia_VersaoTitleControlIdToReplace, AV72ddo_Guia_AnoTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A93Guia_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E225G2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E235G2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Guia_Nome1, AV31Guia_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Guia_Nome2, AV32Guia_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Guia_Nome3, AV33Guia_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV58TFGuia_Codigo, AV59TFGuia_Codigo_To, AV62TFGuia_Nome, AV63TFGuia_Nome_Sel, AV66TFGuia_Versao, AV67TFGuia_Versao_Sel, AV70TFGuia_Ano, AV71TFGuia_Ano_To, AV60ddo_Guia_CodigoTitleControlIdToReplace, AV64ddo_Guia_NomeTitleControlIdToReplace, AV68ddo_Guia_VersaoTitleControlIdToReplace, AV72ddo_Guia_AnoTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A93Guia_Codigo) ;
      }

      protected void E185G2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Guia_Nome1, AV31Guia_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Guia_Nome2, AV32Guia_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Guia_Nome3, AV33Guia_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV58TFGuia_Codigo, AV59TFGuia_Codigo_To, AV62TFGuia_Nome, AV63TFGuia_Nome_Sel, AV66TFGuia_Versao, AV67TFGuia_Versao_Sel, AV70TFGuia_Ano, AV71TFGuia_Ano_To, AV60ddo_Guia_CodigoTitleControlIdToReplace, AV64ddo_Guia_NomeTitleControlIdToReplace, AV68ddo_Guia_VersaoTitleControlIdToReplace, AV72ddo_Guia_AnoTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A93Guia_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E245G2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E195G2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Guia_Nome1, AV31Guia_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Guia_Nome2, AV32Guia_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Guia_Nome3, AV33Guia_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV58TFGuia_Codigo, AV59TFGuia_Codigo_To, AV62TFGuia_Nome, AV63TFGuia_Nome_Sel, AV66TFGuia_Versao, AV67TFGuia_Versao_Sel, AV70TFGuia_Ano, AV71TFGuia_Ano_To, AV60ddo_Guia_CodigoTitleControlIdToReplace, AV64ddo_Guia_NomeTitleControlIdToReplace, AV68ddo_Guia_VersaoTitleControlIdToReplace, AV72ddo_Guia_AnoTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A93Guia_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E255G2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E205G2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("guia.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_guia_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "SortedStatus", Ddo_guia_codigo_Sortedstatus);
         Ddo_guia_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "SortedStatus", Ddo_guia_nome_Sortedstatus);
         Ddo_guia_versao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_versao_Internalname, "SortedStatus", Ddo_guia_versao_Sortedstatus);
         Ddo_guia_ano_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_ano_Internalname, "SortedStatus", Ddo_guia_ano_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_guia_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "SortedStatus", Ddo_guia_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_guia_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "SortedStatus", Ddo_guia_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_guia_versao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_versao_Internalname, "SortedStatus", Ddo_guia_versao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_guia_ano_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_ano_Internalname, "SortedStatus", Ddo_guia_ano_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavGuia_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome1_Visible), 5, 0)));
         edtavGuia_codigo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_codigo1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 )
         {
            edtavGuia_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_CODIGO") == 0 )
         {
            edtavGuia_codigo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_codigo1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavGuia_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome2_Visible), 5, 0)));
         edtavGuia_codigo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_codigo2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "GUIA_NOME") == 0 )
         {
            edtavGuia_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "GUIA_CODIGO") == 0 )
         {
            edtavGuia_codigo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_codigo2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavGuia_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome3_Visible), 5, 0)));
         edtavGuia_codigo3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_codigo3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GUIA_NOME") == 0 )
         {
            edtavGuia_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GUIA_CODIGO") == 0 )
         {
            edtavGuia_codigo3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_codigo3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "GUIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21Guia_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Guia_Nome2", AV21Guia_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "GUIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25Guia_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Guia_Nome3", AV25Guia_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV110Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV110Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV110Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV111GXV1 = 1;
         while ( AV111GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV111GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGUIA_CODIGO") == 0 )
            {
               AV58TFGuia_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFGuia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFGuia_Codigo), 6, 0)));
               AV59TFGuia_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFGuia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFGuia_Codigo_To), 6, 0)));
               if ( ! (0==AV58TFGuia_Codigo) )
               {
                  Ddo_guia_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV58TFGuia_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "FilteredText_set", Ddo_guia_codigo_Filteredtext_set);
               }
               if ( ! (0==AV59TFGuia_Codigo_To) )
               {
                  Ddo_guia_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV59TFGuia_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "FilteredTextTo_set", Ddo_guia_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGUIA_NOME") == 0 )
            {
               AV62TFGuia_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFGuia_Nome", AV62TFGuia_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFGuia_Nome)) )
               {
                  Ddo_guia_nome_Filteredtext_set = AV62TFGuia_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "FilteredText_set", Ddo_guia_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGUIA_NOME_SEL") == 0 )
            {
               AV63TFGuia_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFGuia_Nome_Sel", AV63TFGuia_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFGuia_Nome_Sel)) )
               {
                  Ddo_guia_nome_Selectedvalue_set = AV63TFGuia_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "SelectedValue_set", Ddo_guia_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGUIA_VERSAO") == 0 )
            {
               AV66TFGuia_Versao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFGuia_Versao", AV66TFGuia_Versao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFGuia_Versao)) )
               {
                  Ddo_guia_versao_Filteredtext_set = AV66TFGuia_Versao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_versao_Internalname, "FilteredText_set", Ddo_guia_versao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGUIA_VERSAO_SEL") == 0 )
            {
               AV67TFGuia_Versao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFGuia_Versao_Sel", AV67TFGuia_Versao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFGuia_Versao_Sel)) )
               {
                  Ddo_guia_versao_Selectedvalue_set = AV67TFGuia_Versao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_versao_Internalname, "SelectedValue_set", Ddo_guia_versao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGUIA_ANO") == 0 )
            {
               AV70TFGuia_Ano = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFGuia_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70TFGuia_Ano), 4, 0)));
               AV71TFGuia_Ano_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFGuia_Ano_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71TFGuia_Ano_To), 4, 0)));
               if ( ! (0==AV70TFGuia_Ano) )
               {
                  Ddo_guia_ano_Filteredtext_set = StringUtil.Str( (decimal)(AV70TFGuia_Ano), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_ano_Internalname, "FilteredText_set", Ddo_guia_ano_Filteredtext_set);
               }
               if ( ! (0==AV71TFGuia_Ano_To) )
               {
                  Ddo_guia_ano_Filteredtextto_set = StringUtil.Str( (decimal)(AV71TFGuia_Ano_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_ano_Internalname, "FilteredTextTo_set", Ddo_guia_ano_Filteredtextto_set);
               }
            }
            AV111GXV1 = (int)(AV111GXV1+1);
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Guia_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Guia_Nome1", AV17Guia_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_CODIGO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV31Guia_Codigo1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Guia_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Guia_Codigo1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "GUIA_NOME") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21Guia_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Guia_Nome2", AV21Guia_Nome2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "GUIA_CODIGO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV32Guia_Codigo2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Guia_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Guia_Codigo2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GUIA_NOME") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25Guia_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Guia_Nome3", AV25Guia_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GUIA_CODIGO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV33Guia_Codigo3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Guia_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Guia_Codigo3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV110Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV58TFGuia_Codigo) && (0==AV59TFGuia_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGUIA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV58TFGuia_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV59TFGuia_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFGuia_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGUIA_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV62TFGuia_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFGuia_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGUIA_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV63TFGuia_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFGuia_Versao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGUIA_VERSAO";
            AV11GridStateFilterValue.gxTpr_Value = AV66TFGuia_Versao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFGuia_Versao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGUIA_VERSAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV67TFGuia_Versao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV70TFGuia_Ano) && (0==AV71TFGuia_Ano_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGUIA_ANO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV70TFGuia_Ano), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV71TFGuia_Ano_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV110Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Guia_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Guia_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_CODIGO") == 0 ) && ! (0==AV31Guia_Codigo1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV31Guia_Codigo1), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Guia_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21Guia_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "GUIA_CODIGO") == 0 ) && ! (0==AV32Guia_Codigo2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV32Guia_Codigo2), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Guia_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Guia_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "GUIA_CODIGO") == 0 ) && ! (0==AV33Guia_Codigo3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV33Guia_Codigo3), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV110Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Guia";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_5G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_5G2( true) ;
         }
         else
         {
            wb_table2_8_5G2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_5G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_83_5G2( true) ;
         }
         else
         {
            wb_table3_83_5G2( false) ;
         }
         return  ;
      }

      protected void wb_table3_83_5G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_5G2e( true) ;
         }
         else
         {
            wb_table1_2_5G2e( false) ;
         }
      }

      protected void wb_table3_83_5G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_86_5G2( true) ;
         }
         else
         {
            wb_table4_86_5G2( false) ;
         }
         return  ;
      }

      protected void wb_table4_86_5G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_83_5G2e( true) ;
         }
         else
         {
            wb_table3_83_5G2e( false) ;
         }
      }

      protected void wb_table4_86_5G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"89\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGuia_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtGuia_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGuia_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGuia_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtGuia_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGuia_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Trabalho Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Trabalho Des") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGuia_Versao_Titleformat == 0 )
               {
                  context.SendWebValue( edtGuia_Versao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGuia_Versao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGuia_Ano_Titleformat == 0 )
               {
                  context.SendWebValue( edtGuia_Ano_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGuia_Ano_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A93Guia_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGuia_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGuia_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A94Guia_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGuia_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGuia_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtGuia_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A231Guia_AreaTrabalhoDes);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A95Guia_Versao));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGuia_Versao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGuia_Versao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A96Guia_Ano), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGuia_Ano_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGuia_Ano_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 89 )
         {
            wbEnd = 0;
            nRC_GXsfl_89 = (short)(nGXsfl_89_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_86_5G2e( true) ;
         }
         else
         {
            wb_table4_86_5G2e( false) ;
         }
      }

      protected void wb_table2_8_5G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGuiatitle_Internalname, "Guias", "", "", lblGuiatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Width100'>") ;
            wb_table5_13_5G2( true) ;
         }
         else
         {
            wb_table5_13_5G2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_5G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWGuia.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_5G2( true) ;
         }
         else
         {
            wb_table6_23_5G2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_5G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_5G2e( true) ;
         }
         else
         {
            wb_table2_8_5G2e( false) ;
         }
      }

      protected void wb_table6_23_5G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_26_5G2( true) ;
         }
         else
         {
            wb_table7_26_5G2( false) ;
         }
         return  ;
      }

      protected void wb_table7_26_5G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_5G2e( true) ;
         }
         else
         {
            wb_table6_23_5G2e( false) ;
         }
      }

      protected void wb_table7_26_5G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_WWGuia.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_35_5G2( true) ;
         }
         else
         {
            wb_table8_35_5G2( false) ;
         }
         return  ;
      }

      protected void wb_table8_35_5G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGuia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_WWGuia.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_53_5G2( true) ;
         }
         else
         {
            wb_table9_53_5G2( false) ;
         }
         return  ;
      }

      protected void wb_table9_53_5G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGuia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWGuia.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_71_5G2( true) ;
         }
         else
         {
            wb_table10_71_5G2( false) ;
         }
         return  ;
      }

      protected void wb_table10_71_5G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_26_5G2e( true) ;
         }
         else
         {
            wb_table7_26_5G2e( false) ;
         }
      }

      protected void wb_table10_71_5G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWGuia.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuia_nome3_Internalname, StringUtil.RTrim( AV25Guia_Nome3), StringUtil.RTrim( context.localUtil.Format( AV25Guia_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuia_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGuia_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGuia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuia_codigo3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33Guia_Codigo3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33Guia_Codigo3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuia_codigo3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGuia_codigo3_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_71_5G2e( true) ;
         }
         else
         {
            wb_table10_71_5G2e( false) ;
         }
      }

      protected void wb_table9_53_5G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_WWGuia.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuia_nome2_Internalname, StringUtil.RTrim( AV21Guia_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21Guia_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuia_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGuia_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGuia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuia_codigo2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32Guia_Codigo2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32Guia_Codigo2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuia_codigo2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGuia_codigo2_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_53_5G2e( true) ;
         }
         else
         {
            wb_table9_53_5G2e( false) ;
         }
      }

      protected void wb_table8_35_5G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_WWGuia.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuia_nome1_Internalname, StringUtil.RTrim( AV17Guia_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Guia_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuia_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGuia_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGuia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuia_codigo1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31Guia_Codigo1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31Guia_Codigo1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuia_codigo1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGuia_codigo1_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_35_5G2e( true) ;
         }
         else
         {
            wb_table8_35_5G2e( false) ;
         }
      }

      protected void wb_table5_13_5G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGuia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_5G2e( true) ;
         }
         else
         {
            wb_table5_13_5G2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA5G2( ) ;
         WS5G2( ) ;
         WE5G2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812593932");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwguia.js", "?202051812593933");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_892( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_89_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_89_idx;
         edtGuia_Codigo_Internalname = "GUIA_CODIGO_"+sGXsfl_89_idx;
         edtGuia_Nome_Internalname = "GUIA_NOME_"+sGXsfl_89_idx;
         edtGuia_AreaTrabalhoCod_Internalname = "GUIA_AREATRABALHOCOD_"+sGXsfl_89_idx;
         edtGuia_AreaTrabalhoDes_Internalname = "GUIA_AREATRABALHODES_"+sGXsfl_89_idx;
         edtGuia_Versao_Internalname = "GUIA_VERSAO_"+sGXsfl_89_idx;
         edtGuia_Ano_Internalname = "GUIA_ANO_"+sGXsfl_89_idx;
      }

      protected void SubsflControlProps_fel_892( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_89_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_89_fel_idx;
         edtGuia_Codigo_Internalname = "GUIA_CODIGO_"+sGXsfl_89_fel_idx;
         edtGuia_Nome_Internalname = "GUIA_NOME_"+sGXsfl_89_fel_idx;
         edtGuia_AreaTrabalhoCod_Internalname = "GUIA_AREATRABALHOCOD_"+sGXsfl_89_fel_idx;
         edtGuia_AreaTrabalhoDes_Internalname = "GUIA_AREATRABALHODES_"+sGXsfl_89_fel_idx;
         edtGuia_Versao_Internalname = "GUIA_VERSAO_"+sGXsfl_89_fel_idx;
         edtGuia_Ano_Internalname = "GUIA_ANO_"+sGXsfl_89_fel_idx;
      }

      protected void sendrow_892( )
      {
         SubsflControlProps_892( ) ;
         WB5G0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_89_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_89_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_89_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV108Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV108Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV109Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV109Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGuia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A93Guia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGuia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGuia_Nome_Internalname,StringUtil.RTrim( A94Guia_Nome),StringUtil.RTrim( context.localUtil.Format( A94Guia_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtGuia_Nome_Link,(String)"",(String)"",(String)"",(String)edtGuia_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGuia_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A230Guia_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGuia_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGuia_AreaTrabalhoDes_Internalname,(String)A231Guia_AreaTrabalhoDes,StringUtil.RTrim( context.localUtil.Format( A231Guia_AreaTrabalhoDes, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGuia_AreaTrabalhoDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGuia_Versao_Internalname,StringUtil.RTrim( A95Guia_Versao),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGuia_Versao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"VersaoGuia",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGuia_Ano_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A96Guia_Ano), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A96Guia_Ano), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGuia_Ano_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"Ano",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_GUIA_CODIGO"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_GUIA_NOME"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, StringUtil.RTrim( context.localUtil.Format( A94Guia_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_GUIA_AREATRABALHOCOD"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, context.localUtil.Format( (decimal)(A230Guia_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_GUIA_VERSAO"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, StringUtil.RTrim( context.localUtil.Format( A95Guia_Versao, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_GUIA_ANO"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, context.localUtil.Format( (decimal)(A96Guia_Ano), "ZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_89_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_89_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_89_idx+1));
            sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
            SubsflControlProps_892( ) ;
         }
         /* End function sendrow_892 */
      }

      protected void init_default_properties( )
      {
         lblGuiatitle_Internalname = "GUIATITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavGuia_nome1_Internalname = "vGUIA_NOME1";
         edtavGuia_codigo1_Internalname = "vGUIA_CODIGO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavGuia_nome2_Internalname = "vGUIA_NOME2";
         edtavGuia_codigo2_Internalname = "vGUIA_CODIGO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavGuia_nome3_Internalname = "vGUIA_NOME3";
         edtavGuia_codigo3_Internalname = "vGUIA_CODIGO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtGuia_Codigo_Internalname = "GUIA_CODIGO";
         edtGuia_Nome_Internalname = "GUIA_NOME";
         edtGuia_AreaTrabalhoCod_Internalname = "GUIA_AREATRABALHOCOD";
         edtGuia_AreaTrabalhoDes_Internalname = "GUIA_AREATRABALHODES";
         edtGuia_Versao_Internalname = "GUIA_VERSAO";
         edtGuia_Ano_Internalname = "GUIA_ANO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfguia_codigo_Internalname = "vTFGUIA_CODIGO";
         edtavTfguia_codigo_to_Internalname = "vTFGUIA_CODIGO_TO";
         edtavTfguia_nome_Internalname = "vTFGUIA_NOME";
         edtavTfguia_nome_sel_Internalname = "vTFGUIA_NOME_SEL";
         edtavTfguia_versao_Internalname = "vTFGUIA_VERSAO";
         edtavTfguia_versao_sel_Internalname = "vTFGUIA_VERSAO_SEL";
         edtavTfguia_ano_Internalname = "vTFGUIA_ANO";
         edtavTfguia_ano_to_Internalname = "vTFGUIA_ANO_TO";
         Ddo_guia_codigo_Internalname = "DDO_GUIA_CODIGO";
         edtavDdo_guia_codigotitlecontrolidtoreplace_Internalname = "vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_guia_nome_Internalname = "DDO_GUIA_NOME";
         edtavDdo_guia_nometitlecontrolidtoreplace_Internalname = "vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE";
         Ddo_guia_versao_Internalname = "DDO_GUIA_VERSAO";
         edtavDdo_guia_versaotitlecontrolidtoreplace_Internalname = "vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE";
         Ddo_guia_ano_Internalname = "DDO_GUIA_ANO";
         edtavDdo_guia_anotitlecontrolidtoreplace_Internalname = "vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtGuia_Ano_Jsonclick = "";
         edtGuia_Versao_Jsonclick = "";
         edtGuia_AreaTrabalhoDes_Jsonclick = "";
         edtGuia_AreaTrabalhoCod_Jsonclick = "";
         edtGuia_Nome_Jsonclick = "";
         edtGuia_Codigo_Jsonclick = "";
         edtavGuia_codigo1_Jsonclick = "";
         edtavGuia_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavGuia_codigo2_Jsonclick = "";
         edtavGuia_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavGuia_codigo3_Jsonclick = "";
         edtavGuia_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtGuia_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtGuia_Ano_Titleformat = 0;
         edtGuia_Versao_Titleformat = 0;
         edtGuia_Nome_Titleformat = 0;
         edtGuia_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavGuia_codigo3_Visible = 1;
         edtavGuia_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavGuia_codigo2_Visible = 1;
         edtavGuia_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavGuia_codigo1_Visible = 1;
         edtavGuia_nome1_Visible = 1;
         edtGuia_Ano_Title = "Ano";
         edtGuia_Versao_Title = "Vers�o";
         edtGuia_Nome_Title = "Nome";
         edtGuia_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_guia_anotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_guia_versaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_guia_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_guia_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfguia_ano_to_Jsonclick = "";
         edtavTfguia_ano_to_Visible = 1;
         edtavTfguia_ano_Jsonclick = "";
         edtavTfguia_ano_Visible = 1;
         edtavTfguia_versao_sel_Jsonclick = "";
         edtavTfguia_versao_sel_Visible = 1;
         edtavTfguia_versao_Jsonclick = "";
         edtavTfguia_versao_Visible = 1;
         edtavTfguia_nome_sel_Jsonclick = "";
         edtavTfguia_nome_sel_Visible = 1;
         edtavTfguia_nome_Jsonclick = "";
         edtavTfguia_nome_Visible = 1;
         edtavTfguia_codigo_to_Jsonclick = "";
         edtavTfguia_codigo_to_Visible = 1;
         edtavTfguia_codigo_Jsonclick = "";
         edtavTfguia_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_guia_ano_Searchbuttontext = "Pesquisar";
         Ddo_guia_ano_Rangefilterto = "At�";
         Ddo_guia_ano_Rangefilterfrom = "Desde";
         Ddo_guia_ano_Cleanfilter = "Limpar pesquisa";
         Ddo_guia_ano_Sortdsc = "Ordenar de Z � A";
         Ddo_guia_ano_Sortasc = "Ordenar de A � Z";
         Ddo_guia_ano_Includedatalist = Convert.ToBoolean( 0);
         Ddo_guia_ano_Filterisrange = Convert.ToBoolean( -1);
         Ddo_guia_ano_Filtertype = "Numeric";
         Ddo_guia_ano_Includefilter = Convert.ToBoolean( -1);
         Ddo_guia_ano_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_guia_ano_Includesortasc = Convert.ToBoolean( -1);
         Ddo_guia_ano_Titlecontrolidtoreplace = "";
         Ddo_guia_ano_Dropdownoptionstype = "GridTitleSettings";
         Ddo_guia_ano_Cls = "ColumnSettings";
         Ddo_guia_ano_Tooltip = "Op��es";
         Ddo_guia_ano_Caption = "";
         Ddo_guia_versao_Searchbuttontext = "Pesquisar";
         Ddo_guia_versao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_guia_versao_Cleanfilter = "Limpar pesquisa";
         Ddo_guia_versao_Loadingdata = "Carregando dados...";
         Ddo_guia_versao_Sortdsc = "Ordenar de Z � A";
         Ddo_guia_versao_Sortasc = "Ordenar de A � Z";
         Ddo_guia_versao_Datalistupdateminimumcharacters = 0;
         Ddo_guia_versao_Datalistproc = "GetWWGuiaFilterData";
         Ddo_guia_versao_Datalisttype = "Dynamic";
         Ddo_guia_versao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_guia_versao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_guia_versao_Filtertype = "Character";
         Ddo_guia_versao_Includefilter = Convert.ToBoolean( -1);
         Ddo_guia_versao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_guia_versao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_guia_versao_Titlecontrolidtoreplace = "";
         Ddo_guia_versao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_guia_versao_Cls = "ColumnSettings";
         Ddo_guia_versao_Tooltip = "Op��es";
         Ddo_guia_versao_Caption = "";
         Ddo_guia_nome_Searchbuttontext = "Pesquisar";
         Ddo_guia_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_guia_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_guia_nome_Loadingdata = "Carregando dados...";
         Ddo_guia_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_guia_nome_Sortasc = "Ordenar de A � Z";
         Ddo_guia_nome_Datalistupdateminimumcharacters = 0;
         Ddo_guia_nome_Datalistproc = "GetWWGuiaFilterData";
         Ddo_guia_nome_Datalisttype = "Dynamic";
         Ddo_guia_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_guia_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_guia_nome_Filtertype = "Character";
         Ddo_guia_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_guia_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_guia_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_guia_nome_Titlecontrolidtoreplace = "";
         Ddo_guia_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_guia_nome_Cls = "ColumnSettings";
         Ddo_guia_nome_Tooltip = "Op��es";
         Ddo_guia_nome_Caption = "";
         Ddo_guia_codigo_Searchbuttontext = "Pesquisar";
         Ddo_guia_codigo_Rangefilterto = "At�";
         Ddo_guia_codigo_Rangefilterfrom = "Desde";
         Ddo_guia_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_guia_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_guia_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_guia_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_guia_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_guia_codigo_Filtertype = "Numeric";
         Ddo_guia_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_guia_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_guia_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_guia_codigo_Titlecontrolidtoreplace = "";
         Ddo_guia_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_guia_codigo_Cls = "ColumnSettings";
         Ddo_guia_codigo_Tooltip = "Op��es";
         Ddo_guia_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Guia";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV57Guia_CodigoTitleFilterData',fld:'vGUIA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV61Guia_NomeTitleFilterData',fld:'vGUIA_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV65Guia_VersaoTitleFilterData',fld:'vGUIA_VERSAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV69Guia_AnoTitleFilterData',fld:'vGUIA_ANOTITLEFILTERDATA',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtGuia_Codigo_Titleformat',ctrl:'GUIA_CODIGO',prop:'Titleformat'},{av:'edtGuia_Codigo_Title',ctrl:'GUIA_CODIGO',prop:'Title'},{av:'edtGuia_Nome_Titleformat',ctrl:'GUIA_NOME',prop:'Titleformat'},{av:'edtGuia_Nome_Title',ctrl:'GUIA_NOME',prop:'Title'},{av:'edtGuia_Versao_Titleformat',ctrl:'GUIA_VERSAO',prop:'Titleformat'},{av:'edtGuia_Versao_Title',ctrl:'GUIA_VERSAO',prop:'Title'},{av:'edtGuia_Ano_Titleformat',ctrl:'GUIA_ANO',prop:'Titleformat'},{av:'edtGuia_Ano_Title',ctrl:'GUIA_ANO',prop:'Title'},{av:'AV75GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV76GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E115G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_GUIA_CODIGO.ONOPTIONCLICKED","{handler:'E125G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_guia_codigo_Activeeventkey',ctrl:'DDO_GUIA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_guia_codigo_Filteredtext_get',ctrl:'DDO_GUIA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_guia_codigo_Filteredtextto_get',ctrl:'DDO_GUIA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_guia_codigo_Sortedstatus',ctrl:'DDO_GUIA_CODIGO',prop:'SortedStatus'},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_guia_nome_Sortedstatus',ctrl:'DDO_GUIA_NOME',prop:'SortedStatus'},{av:'Ddo_guia_versao_Sortedstatus',ctrl:'DDO_GUIA_VERSAO',prop:'SortedStatus'},{av:'Ddo_guia_ano_Sortedstatus',ctrl:'DDO_GUIA_ANO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_GUIA_NOME.ONOPTIONCLICKED","{handler:'E135G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_guia_nome_Activeeventkey',ctrl:'DDO_GUIA_NOME',prop:'ActiveEventKey'},{av:'Ddo_guia_nome_Filteredtext_get',ctrl:'DDO_GUIA_NOME',prop:'FilteredText_get'},{av:'Ddo_guia_nome_Selectedvalue_get',ctrl:'DDO_GUIA_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_guia_nome_Sortedstatus',ctrl:'DDO_GUIA_NOME',prop:'SortedStatus'},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_guia_codigo_Sortedstatus',ctrl:'DDO_GUIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_guia_versao_Sortedstatus',ctrl:'DDO_GUIA_VERSAO',prop:'SortedStatus'},{av:'Ddo_guia_ano_Sortedstatus',ctrl:'DDO_GUIA_ANO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_GUIA_VERSAO.ONOPTIONCLICKED","{handler:'E145G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_guia_versao_Activeeventkey',ctrl:'DDO_GUIA_VERSAO',prop:'ActiveEventKey'},{av:'Ddo_guia_versao_Filteredtext_get',ctrl:'DDO_GUIA_VERSAO',prop:'FilteredText_get'},{av:'Ddo_guia_versao_Selectedvalue_get',ctrl:'DDO_GUIA_VERSAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_guia_versao_Sortedstatus',ctrl:'DDO_GUIA_VERSAO',prop:'SortedStatus'},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'Ddo_guia_codigo_Sortedstatus',ctrl:'DDO_GUIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_guia_nome_Sortedstatus',ctrl:'DDO_GUIA_NOME',prop:'SortedStatus'},{av:'Ddo_guia_ano_Sortedstatus',ctrl:'DDO_GUIA_ANO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_GUIA_ANO.ONOPTIONCLICKED","{handler:'E155G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_guia_ano_Activeeventkey',ctrl:'DDO_GUIA_ANO',prop:'ActiveEventKey'},{av:'Ddo_guia_ano_Filteredtext_get',ctrl:'DDO_GUIA_ANO',prop:'FilteredText_get'},{av:'Ddo_guia_ano_Filteredtextto_get',ctrl:'DDO_GUIA_ANO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_guia_ano_Sortedstatus',ctrl:'DDO_GUIA_ANO',prop:'SortedStatus'},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_guia_codigo_Sortedstatus',ctrl:'DDO_GUIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_guia_nome_Sortedstatus',ctrl:'DDO_GUIA_NOME',prop:'SortedStatus'},{av:'Ddo_guia_versao_Sortedstatus',ctrl:'DDO_GUIA_VERSAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E285G2',iparms:[{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtGuia_Nome_Link',ctrl:'GUIA_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E165G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E215G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E175G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'edtavGuia_nome2_Visible',ctrl:'vGUIA_NOME2',prop:'Visible'},{av:'edtavGuia_codigo2_Visible',ctrl:'vGUIA_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavGuia_nome3_Visible',ctrl:'vGUIA_NOME3',prop:'Visible'},{av:'edtavGuia_codigo3_Visible',ctrl:'vGUIA_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavGuia_nome1_Visible',ctrl:'vGUIA_NOME1',prop:'Visible'},{av:'edtavGuia_codigo1_Visible',ctrl:'vGUIA_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E225G2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavGuia_nome1_Visible',ctrl:'vGUIA_NOME1',prop:'Visible'},{av:'edtavGuia_codigo1_Visible',ctrl:'vGUIA_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E235G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E185G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'edtavGuia_nome2_Visible',ctrl:'vGUIA_NOME2',prop:'Visible'},{av:'edtavGuia_codigo2_Visible',ctrl:'vGUIA_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavGuia_nome3_Visible',ctrl:'vGUIA_NOME3',prop:'Visible'},{av:'edtavGuia_codigo3_Visible',ctrl:'vGUIA_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavGuia_nome1_Visible',ctrl:'vGUIA_NOME1',prop:'Visible'},{av:'edtavGuia_codigo1_Visible',ctrl:'vGUIA_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E245G2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavGuia_nome2_Visible',ctrl:'vGUIA_NOME2',prop:'Visible'},{av:'edtavGuia_codigo2_Visible',ctrl:'vGUIA_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E195G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV58TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV62TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV63TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFGuia_Versao',fld:'vTFGUIA_VERSAO',pic:'',nv:''},{av:'AV67TFGuia_Versao_Sel',fld:'vTFGUIA_VERSAO_SEL',pic:'',nv:''},{av:'AV70TFGuia_Ano',fld:'vTFGUIA_ANO',pic:'ZZZ9',nv:0},{av:'AV71TFGuia_Ano_To',fld:'vTFGUIA_ANO_TO',pic:'ZZZ9',nv:0},{av:'AV60ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Guia_VersaoTitleControlIdToReplace',fld:'vDDO_GUIA_VERSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Guia_AnoTitleControlIdToReplace',fld:'vDDO_GUIA_ANOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV31Guia_Codigo1',fld:'vGUIA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV32Guia_Codigo2',fld:'vGUIA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV33Guia_Codigo3',fld:'vGUIA_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'edtavGuia_nome2_Visible',ctrl:'vGUIA_NOME2',prop:'Visible'},{av:'edtavGuia_codigo2_Visible',ctrl:'vGUIA_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavGuia_nome3_Visible',ctrl:'vGUIA_NOME3',prop:'Visible'},{av:'edtavGuia_codigo3_Visible',ctrl:'vGUIA_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavGuia_nome1_Visible',ctrl:'vGUIA_NOME1',prop:'Visible'},{av:'edtavGuia_codigo1_Visible',ctrl:'vGUIA_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E255G2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavGuia_nome3_Visible',ctrl:'vGUIA_NOME3',prop:'Visible'},{av:'edtavGuia_codigo3_Visible',ctrl:'vGUIA_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E205G2',iparms:[{av:'A93Guia_Codigo',fld:'GUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_guia_codigo_Activeeventkey = "";
         Ddo_guia_codigo_Filteredtext_get = "";
         Ddo_guia_codigo_Filteredtextto_get = "";
         Ddo_guia_nome_Activeeventkey = "";
         Ddo_guia_nome_Filteredtext_get = "";
         Ddo_guia_nome_Selectedvalue_get = "";
         Ddo_guia_versao_Activeeventkey = "";
         Ddo_guia_versao_Filteredtext_get = "";
         Ddo_guia_versao_Selectedvalue_get = "";
         Ddo_guia_ano_Activeeventkey = "";
         Ddo_guia_ano_Filteredtext_get = "";
         Ddo_guia_ano_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Guia_Nome1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21Guia_Nome2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25Guia_Nome3 = "";
         AV62TFGuia_Nome = "";
         AV63TFGuia_Nome_Sel = "";
         AV66TFGuia_Versao = "";
         AV67TFGuia_Versao_Sel = "";
         AV60ddo_Guia_CodigoTitleControlIdToReplace = "";
         AV64ddo_Guia_NomeTitleControlIdToReplace = "";
         AV68ddo_Guia_VersaoTitleControlIdToReplace = "";
         AV72ddo_Guia_AnoTitleControlIdToReplace = "";
         AV110Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV73DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV57Guia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61Guia_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65Guia_VersaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69Guia_AnoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_guia_codigo_Filteredtext_set = "";
         Ddo_guia_codigo_Filteredtextto_set = "";
         Ddo_guia_codigo_Sortedstatus = "";
         Ddo_guia_nome_Filteredtext_set = "";
         Ddo_guia_nome_Selectedvalue_set = "";
         Ddo_guia_nome_Sortedstatus = "";
         Ddo_guia_versao_Filteredtext_set = "";
         Ddo_guia_versao_Selectedvalue_set = "";
         Ddo_guia_versao_Sortedstatus = "";
         Ddo_guia_ano_Filteredtext_set = "";
         Ddo_guia_ano_Filteredtextto_set = "";
         Ddo_guia_ano_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV108Update_GXI = "";
         AV29Delete = "";
         AV109Delete_GXI = "";
         A94Guia_Nome = "";
         A231Guia_AreaTrabalhoDes = "";
         A95Guia_Versao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV88WWGuiaDS_3_Guia_nome1 = "";
         lV93WWGuiaDS_8_Guia_nome2 = "";
         lV98WWGuiaDS_13_Guia_nome3 = "";
         lV102WWGuiaDS_17_Tfguia_nome = "";
         lV104WWGuiaDS_19_Tfguia_versao = "";
         AV86WWGuiaDS_1_Dynamicfiltersselector1 = "";
         AV88WWGuiaDS_3_Guia_nome1 = "";
         AV91WWGuiaDS_6_Dynamicfiltersselector2 = "";
         AV93WWGuiaDS_8_Guia_nome2 = "";
         AV96WWGuiaDS_11_Dynamicfiltersselector3 = "";
         AV98WWGuiaDS_13_Guia_nome3 = "";
         AV103WWGuiaDS_18_Tfguia_nome_sel = "";
         AV102WWGuiaDS_17_Tfguia_nome = "";
         AV105WWGuiaDS_20_Tfguia_versao_sel = "";
         AV104WWGuiaDS_19_Tfguia_versao = "";
         H005G2_A96Guia_Ano = new short[1] ;
         H005G2_A95Guia_Versao = new String[] {""} ;
         H005G2_A231Guia_AreaTrabalhoDes = new String[] {""} ;
         H005G2_n231Guia_AreaTrabalhoDes = new bool[] {false} ;
         H005G2_A230Guia_AreaTrabalhoCod = new int[1] ;
         H005G2_A94Guia_Nome = new String[] {""} ;
         H005G2_A93Guia_Codigo = new int[1] ;
         H005G3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblGuiatitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwguia__default(),
            new Object[][] {
                new Object[] {
               H005G2_A96Guia_Ano, H005G2_A95Guia_Versao, H005G2_A231Guia_AreaTrabalhoDes, H005G2_n231Guia_AreaTrabalhoDes, H005G2_A230Guia_AreaTrabalhoCod, H005G2_A94Guia_Nome, H005G2_A93Guia_Codigo
               }
               , new Object[] {
               H005G3_AGRID_nRecordCount
               }
            }
         );
         AV110Pgmname = "WWGuia";
         /* GeneXus formulas. */
         AV110Pgmname = "WWGuia";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_89 ;
      private short nGXsfl_89_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV70TFGuia_Ano ;
      private short AV71TFGuia_Ano_To ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A96Guia_Ano ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_89_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV87WWGuiaDS_2_Dynamicfiltersoperator1 ;
      private short AV92WWGuiaDS_7_Dynamicfiltersoperator2 ;
      private short AV97WWGuiaDS_12_Dynamicfiltersoperator3 ;
      private short AV106WWGuiaDS_21_Tfguia_ano ;
      private short AV107WWGuiaDS_22_Tfguia_ano_to ;
      private short edtGuia_Codigo_Titleformat ;
      private short edtGuia_Nome_Titleformat ;
      private short edtGuia_Versao_Titleformat ;
      private short edtGuia_Ano_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV31Guia_Codigo1 ;
      private int AV32Guia_Codigo2 ;
      private int AV33Guia_Codigo3 ;
      private int AV58TFGuia_Codigo ;
      private int AV59TFGuia_Codigo_To ;
      private int A93Guia_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_guia_nome_Datalistupdateminimumcharacters ;
      private int Ddo_guia_versao_Datalistupdateminimumcharacters ;
      private int edtavTfguia_codigo_Visible ;
      private int edtavTfguia_codigo_to_Visible ;
      private int edtavTfguia_nome_Visible ;
      private int edtavTfguia_nome_sel_Visible ;
      private int edtavTfguia_versao_Visible ;
      private int edtavTfguia_versao_sel_Visible ;
      private int edtavTfguia_ano_Visible ;
      private int edtavTfguia_ano_to_Visible ;
      private int edtavDdo_guia_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_guia_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_guia_versaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_guia_anotitlecontrolidtoreplace_Visible ;
      private int A230Guia_AreaTrabalhoCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV89WWGuiaDS_4_Guia_codigo1 ;
      private int AV94WWGuiaDS_9_Guia_codigo2 ;
      private int AV99WWGuiaDS_14_Guia_codigo3 ;
      private int AV100WWGuiaDS_15_Tfguia_codigo ;
      private int AV101WWGuiaDS_16_Tfguia_codigo_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV74PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavGuia_nome1_Visible ;
      private int edtavGuia_codigo1_Visible ;
      private int edtavGuia_nome2_Visible ;
      private int edtavGuia_codigo2_Visible ;
      private int edtavGuia_nome3_Visible ;
      private int edtavGuia_codigo3_Visible ;
      private int AV111GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV75GridCurrentPage ;
      private long AV76GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_guia_codigo_Activeeventkey ;
      private String Ddo_guia_codigo_Filteredtext_get ;
      private String Ddo_guia_codigo_Filteredtextto_get ;
      private String Ddo_guia_nome_Activeeventkey ;
      private String Ddo_guia_nome_Filteredtext_get ;
      private String Ddo_guia_nome_Selectedvalue_get ;
      private String Ddo_guia_versao_Activeeventkey ;
      private String Ddo_guia_versao_Filteredtext_get ;
      private String Ddo_guia_versao_Selectedvalue_get ;
      private String Ddo_guia_ano_Activeeventkey ;
      private String Ddo_guia_ano_Filteredtext_get ;
      private String Ddo_guia_ano_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_89_idx="0001" ;
      private String AV17Guia_Nome1 ;
      private String AV21Guia_Nome2 ;
      private String AV25Guia_Nome3 ;
      private String AV62TFGuia_Nome ;
      private String AV63TFGuia_Nome_Sel ;
      private String AV66TFGuia_Versao ;
      private String AV67TFGuia_Versao_Sel ;
      private String AV110Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_guia_codigo_Caption ;
      private String Ddo_guia_codigo_Tooltip ;
      private String Ddo_guia_codigo_Cls ;
      private String Ddo_guia_codigo_Filteredtext_set ;
      private String Ddo_guia_codigo_Filteredtextto_set ;
      private String Ddo_guia_codigo_Dropdownoptionstype ;
      private String Ddo_guia_codigo_Titlecontrolidtoreplace ;
      private String Ddo_guia_codigo_Sortedstatus ;
      private String Ddo_guia_codigo_Filtertype ;
      private String Ddo_guia_codigo_Sortasc ;
      private String Ddo_guia_codigo_Sortdsc ;
      private String Ddo_guia_codigo_Cleanfilter ;
      private String Ddo_guia_codigo_Rangefilterfrom ;
      private String Ddo_guia_codigo_Rangefilterto ;
      private String Ddo_guia_codigo_Searchbuttontext ;
      private String Ddo_guia_nome_Caption ;
      private String Ddo_guia_nome_Tooltip ;
      private String Ddo_guia_nome_Cls ;
      private String Ddo_guia_nome_Filteredtext_set ;
      private String Ddo_guia_nome_Selectedvalue_set ;
      private String Ddo_guia_nome_Dropdownoptionstype ;
      private String Ddo_guia_nome_Titlecontrolidtoreplace ;
      private String Ddo_guia_nome_Sortedstatus ;
      private String Ddo_guia_nome_Filtertype ;
      private String Ddo_guia_nome_Datalisttype ;
      private String Ddo_guia_nome_Datalistproc ;
      private String Ddo_guia_nome_Sortasc ;
      private String Ddo_guia_nome_Sortdsc ;
      private String Ddo_guia_nome_Loadingdata ;
      private String Ddo_guia_nome_Cleanfilter ;
      private String Ddo_guia_nome_Noresultsfound ;
      private String Ddo_guia_nome_Searchbuttontext ;
      private String Ddo_guia_versao_Caption ;
      private String Ddo_guia_versao_Tooltip ;
      private String Ddo_guia_versao_Cls ;
      private String Ddo_guia_versao_Filteredtext_set ;
      private String Ddo_guia_versao_Selectedvalue_set ;
      private String Ddo_guia_versao_Dropdownoptionstype ;
      private String Ddo_guia_versao_Titlecontrolidtoreplace ;
      private String Ddo_guia_versao_Sortedstatus ;
      private String Ddo_guia_versao_Filtertype ;
      private String Ddo_guia_versao_Datalisttype ;
      private String Ddo_guia_versao_Datalistproc ;
      private String Ddo_guia_versao_Sortasc ;
      private String Ddo_guia_versao_Sortdsc ;
      private String Ddo_guia_versao_Loadingdata ;
      private String Ddo_guia_versao_Cleanfilter ;
      private String Ddo_guia_versao_Noresultsfound ;
      private String Ddo_guia_versao_Searchbuttontext ;
      private String Ddo_guia_ano_Caption ;
      private String Ddo_guia_ano_Tooltip ;
      private String Ddo_guia_ano_Cls ;
      private String Ddo_guia_ano_Filteredtext_set ;
      private String Ddo_guia_ano_Filteredtextto_set ;
      private String Ddo_guia_ano_Dropdownoptionstype ;
      private String Ddo_guia_ano_Titlecontrolidtoreplace ;
      private String Ddo_guia_ano_Sortedstatus ;
      private String Ddo_guia_ano_Filtertype ;
      private String Ddo_guia_ano_Sortasc ;
      private String Ddo_guia_ano_Sortdsc ;
      private String Ddo_guia_ano_Cleanfilter ;
      private String Ddo_guia_ano_Rangefilterfrom ;
      private String Ddo_guia_ano_Rangefilterto ;
      private String Ddo_guia_ano_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfguia_codigo_Internalname ;
      private String edtavTfguia_codigo_Jsonclick ;
      private String edtavTfguia_codigo_to_Internalname ;
      private String edtavTfguia_codigo_to_Jsonclick ;
      private String edtavTfguia_nome_Internalname ;
      private String edtavTfguia_nome_Jsonclick ;
      private String edtavTfguia_nome_sel_Internalname ;
      private String edtavTfguia_nome_sel_Jsonclick ;
      private String edtavTfguia_versao_Internalname ;
      private String edtavTfguia_versao_Jsonclick ;
      private String edtavTfguia_versao_sel_Internalname ;
      private String edtavTfguia_versao_sel_Jsonclick ;
      private String edtavTfguia_ano_Internalname ;
      private String edtavTfguia_ano_Jsonclick ;
      private String edtavTfguia_ano_to_Internalname ;
      private String edtavTfguia_ano_to_Jsonclick ;
      private String edtavDdo_guia_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_guia_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_guia_versaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_guia_anotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtGuia_Codigo_Internalname ;
      private String A94Guia_Nome ;
      private String edtGuia_Nome_Internalname ;
      private String edtGuia_AreaTrabalhoCod_Internalname ;
      private String edtGuia_AreaTrabalhoDes_Internalname ;
      private String A95Guia_Versao ;
      private String edtGuia_Versao_Internalname ;
      private String edtGuia_Ano_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV88WWGuiaDS_3_Guia_nome1 ;
      private String lV93WWGuiaDS_8_Guia_nome2 ;
      private String lV98WWGuiaDS_13_Guia_nome3 ;
      private String lV102WWGuiaDS_17_Tfguia_nome ;
      private String lV104WWGuiaDS_19_Tfguia_versao ;
      private String AV88WWGuiaDS_3_Guia_nome1 ;
      private String AV93WWGuiaDS_8_Guia_nome2 ;
      private String AV98WWGuiaDS_13_Guia_nome3 ;
      private String AV103WWGuiaDS_18_Tfguia_nome_sel ;
      private String AV102WWGuiaDS_17_Tfguia_nome ;
      private String AV105WWGuiaDS_20_Tfguia_versao_sel ;
      private String AV104WWGuiaDS_19_Tfguia_versao ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavGuia_nome1_Internalname ;
      private String edtavGuia_codigo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavGuia_nome2_Internalname ;
      private String edtavGuia_codigo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavGuia_nome3_Internalname ;
      private String edtavGuia_codigo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_guia_codigo_Internalname ;
      private String Ddo_guia_nome_Internalname ;
      private String Ddo_guia_versao_Internalname ;
      private String Ddo_guia_ano_Internalname ;
      private String edtGuia_Codigo_Title ;
      private String edtGuia_Nome_Title ;
      private String edtGuia_Versao_Title ;
      private String edtGuia_Ano_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtGuia_Nome_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblGuiatitle_Internalname ;
      private String lblGuiatitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavGuia_nome3_Jsonclick ;
      private String edtavGuia_codigo3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavGuia_nome2_Jsonclick ;
      private String edtavGuia_codigo2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavGuia_nome1_Jsonclick ;
      private String edtavGuia_codigo1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_89_fel_idx="0001" ;
      private String ROClassString ;
      private String edtGuia_Codigo_Jsonclick ;
      private String edtGuia_Nome_Jsonclick ;
      private String edtGuia_AreaTrabalhoCod_Jsonclick ;
      private String edtGuia_AreaTrabalhoDes_Jsonclick ;
      private String edtGuia_Versao_Jsonclick ;
      private String edtGuia_Ano_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_guia_codigo_Includesortasc ;
      private bool Ddo_guia_codigo_Includesortdsc ;
      private bool Ddo_guia_codigo_Includefilter ;
      private bool Ddo_guia_codigo_Filterisrange ;
      private bool Ddo_guia_codigo_Includedatalist ;
      private bool Ddo_guia_nome_Includesortasc ;
      private bool Ddo_guia_nome_Includesortdsc ;
      private bool Ddo_guia_nome_Includefilter ;
      private bool Ddo_guia_nome_Filterisrange ;
      private bool Ddo_guia_nome_Includedatalist ;
      private bool Ddo_guia_versao_Includesortasc ;
      private bool Ddo_guia_versao_Includesortdsc ;
      private bool Ddo_guia_versao_Includefilter ;
      private bool Ddo_guia_versao_Filterisrange ;
      private bool Ddo_guia_versao_Includedatalist ;
      private bool Ddo_guia_ano_Includesortasc ;
      private bool Ddo_guia_ano_Includesortdsc ;
      private bool Ddo_guia_ano_Includefilter ;
      private bool Ddo_guia_ano_Filterisrange ;
      private bool Ddo_guia_ano_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n231Guia_AreaTrabalhoDes ;
      private bool AV90WWGuiaDS_5_Dynamicfiltersenabled2 ;
      private bool AV95WWGuiaDS_10_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV60ddo_Guia_CodigoTitleControlIdToReplace ;
      private String AV64ddo_Guia_NomeTitleControlIdToReplace ;
      private String AV68ddo_Guia_VersaoTitleControlIdToReplace ;
      private String AV72ddo_Guia_AnoTitleControlIdToReplace ;
      private String AV108Update_GXI ;
      private String AV109Delete_GXI ;
      private String A231Guia_AreaTrabalhoDes ;
      private String AV86WWGuiaDS_1_Dynamicfiltersselector1 ;
      private String AV91WWGuiaDS_6_Dynamicfiltersselector2 ;
      private String AV96WWGuiaDS_11_Dynamicfiltersselector3 ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private short[] H005G2_A96Guia_Ano ;
      private String[] H005G2_A95Guia_Versao ;
      private String[] H005G2_A231Guia_AreaTrabalhoDes ;
      private bool[] H005G2_n231Guia_AreaTrabalhoDes ;
      private int[] H005G2_A230Guia_AreaTrabalhoCod ;
      private String[] H005G2_A94Guia_Nome ;
      private int[] H005G2_A93Guia_Codigo ;
      private long[] H005G3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV57Guia_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV61Guia_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV65Guia_VersaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69Guia_AnoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV73DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwguia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H005G2( IGxContext context ,
                                             String AV86WWGuiaDS_1_Dynamicfiltersselector1 ,
                                             short AV87WWGuiaDS_2_Dynamicfiltersoperator1 ,
                                             String AV88WWGuiaDS_3_Guia_nome1 ,
                                             int AV89WWGuiaDS_4_Guia_codigo1 ,
                                             bool AV90WWGuiaDS_5_Dynamicfiltersenabled2 ,
                                             String AV91WWGuiaDS_6_Dynamicfiltersselector2 ,
                                             short AV92WWGuiaDS_7_Dynamicfiltersoperator2 ,
                                             String AV93WWGuiaDS_8_Guia_nome2 ,
                                             int AV94WWGuiaDS_9_Guia_codigo2 ,
                                             bool AV95WWGuiaDS_10_Dynamicfiltersenabled3 ,
                                             String AV96WWGuiaDS_11_Dynamicfiltersselector3 ,
                                             short AV97WWGuiaDS_12_Dynamicfiltersoperator3 ,
                                             String AV98WWGuiaDS_13_Guia_nome3 ,
                                             int AV99WWGuiaDS_14_Guia_codigo3 ,
                                             int AV100WWGuiaDS_15_Tfguia_codigo ,
                                             int AV101WWGuiaDS_16_Tfguia_codigo_to ,
                                             String AV103WWGuiaDS_18_Tfguia_nome_sel ,
                                             String AV102WWGuiaDS_17_Tfguia_nome ,
                                             String AV105WWGuiaDS_20_Tfguia_versao_sel ,
                                             String AV104WWGuiaDS_19_Tfguia_versao ,
                                             short AV106WWGuiaDS_21_Tfguia_ano ,
                                             short AV107WWGuiaDS_22_Tfguia_ano_to ,
                                             String A94Guia_Nome ,
                                             int A93Guia_Codigo ,
                                             String A95Guia_Versao ,
                                             short A96Guia_Ano ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [22] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Guia_Ano], T1.[Guia_Versao], T2.[AreaTrabalho_Descricao] AS Guia_AreaTrabalhoDes, T1.[Guia_AreaTrabalhoCod] AS Guia_AreaTrabalhoCod, T1.[Guia_Nome], T1.[Guia_Codigo]";
         sFromString = " FROM ([Guia] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Guia_AreaTrabalhoCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV86WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV87WWGuiaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWGuiaDS_3_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like @lV88WWGuiaDS_3_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like @lV88WWGuiaDS_3_Guia_nome1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV87WWGuiaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWGuiaDS_3_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like '%' + @lV88WWGuiaDS_3_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like '%' + @lV88WWGuiaDS_3_Guia_nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_CODIGO") == 0 ) && ( AV87WWGuiaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV89WWGuiaDS_4_Guia_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] = @AV89WWGuiaDS_4_Guia_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] = @AV89WWGuiaDS_4_Guia_codigo1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV90WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV92WWGuiaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWGuiaDS_8_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like @lV93WWGuiaDS_8_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like @lV93WWGuiaDS_8_Guia_nome2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV90WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV92WWGuiaDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWGuiaDS_8_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like '%' + @lV93WWGuiaDS_8_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like '%' + @lV93WWGuiaDS_8_Guia_nome2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV90WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_CODIGO") == 0 ) && ( AV92WWGuiaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV94WWGuiaDS_9_Guia_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] = @AV94WWGuiaDS_9_Guia_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] = @AV94WWGuiaDS_9_Guia_codigo2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV95WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV96WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV97WWGuiaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWGuiaDS_13_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like @lV98WWGuiaDS_13_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like @lV98WWGuiaDS_13_Guia_nome3)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV95WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV96WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV97WWGuiaDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWGuiaDS_13_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like '%' + @lV98WWGuiaDS_13_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like '%' + @lV98WWGuiaDS_13_Guia_nome3)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV95WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV96WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_CODIGO") == 0 ) && ( AV97WWGuiaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV99WWGuiaDS_14_Guia_codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] = @AV99WWGuiaDS_14_Guia_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] = @AV99WWGuiaDS_14_Guia_codigo3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV100WWGuiaDS_15_Tfguia_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] >= @AV100WWGuiaDS_15_Tfguia_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] >= @AV100WWGuiaDS_15_Tfguia_codigo)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV101WWGuiaDS_16_Tfguia_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] <= @AV101WWGuiaDS_16_Tfguia_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] <= @AV101WWGuiaDS_16_Tfguia_codigo_to)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWGuiaDS_18_Tfguia_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWGuiaDS_17_Tfguia_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like @lV102WWGuiaDS_17_Tfguia_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like @lV102WWGuiaDS_17_Tfguia_nome)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWGuiaDS_18_Tfguia_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] = @AV103WWGuiaDS_18_Tfguia_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] = @AV103WWGuiaDS_18_Tfguia_nome_sel)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWGuiaDS_20_Tfguia_versao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWGuiaDS_19_Tfguia_versao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Versao] like @lV104WWGuiaDS_19_Tfguia_versao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Versao] like @lV104WWGuiaDS_19_Tfguia_versao)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWGuiaDS_20_Tfguia_versao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Versao] = @AV105WWGuiaDS_20_Tfguia_versao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Versao] = @AV105WWGuiaDS_20_Tfguia_versao_sel)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV106WWGuiaDS_21_Tfguia_ano) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Ano] >= @AV106WWGuiaDS_21_Tfguia_ano)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Ano] >= @AV106WWGuiaDS_21_Tfguia_ano)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (0==AV107WWGuiaDS_22_Tfguia_ano_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Ano] <= @AV107WWGuiaDS_22_Tfguia_ano_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Ano] <= @AV107WWGuiaDS_22_Tfguia_ano_to)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Guia_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Guia_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Guia_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Guia_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Guia_Versao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Guia_Versao] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Guia_Ano]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Guia_Ano] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Guia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H005G3( IGxContext context ,
                                             String AV86WWGuiaDS_1_Dynamicfiltersselector1 ,
                                             short AV87WWGuiaDS_2_Dynamicfiltersoperator1 ,
                                             String AV88WWGuiaDS_3_Guia_nome1 ,
                                             int AV89WWGuiaDS_4_Guia_codigo1 ,
                                             bool AV90WWGuiaDS_5_Dynamicfiltersenabled2 ,
                                             String AV91WWGuiaDS_6_Dynamicfiltersselector2 ,
                                             short AV92WWGuiaDS_7_Dynamicfiltersoperator2 ,
                                             String AV93WWGuiaDS_8_Guia_nome2 ,
                                             int AV94WWGuiaDS_9_Guia_codigo2 ,
                                             bool AV95WWGuiaDS_10_Dynamicfiltersenabled3 ,
                                             String AV96WWGuiaDS_11_Dynamicfiltersselector3 ,
                                             short AV97WWGuiaDS_12_Dynamicfiltersoperator3 ,
                                             String AV98WWGuiaDS_13_Guia_nome3 ,
                                             int AV99WWGuiaDS_14_Guia_codigo3 ,
                                             int AV100WWGuiaDS_15_Tfguia_codigo ,
                                             int AV101WWGuiaDS_16_Tfguia_codigo_to ,
                                             String AV103WWGuiaDS_18_Tfguia_nome_sel ,
                                             String AV102WWGuiaDS_17_Tfguia_nome ,
                                             String AV105WWGuiaDS_20_Tfguia_versao_sel ,
                                             String AV104WWGuiaDS_19_Tfguia_versao ,
                                             short AV106WWGuiaDS_21_Tfguia_ano ,
                                             short AV107WWGuiaDS_22_Tfguia_ano_to ,
                                             String A94Guia_Nome ,
                                             int A93Guia_Codigo ,
                                             String A95Guia_Versao ,
                                             short A96Guia_Ano ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [17] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Guia] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Guia_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV86WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV87WWGuiaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWGuiaDS_3_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like @lV88WWGuiaDS_3_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like @lV88WWGuiaDS_3_Guia_nome1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV87WWGuiaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWGuiaDS_3_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like '%' + @lV88WWGuiaDS_3_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like '%' + @lV88WWGuiaDS_3_Guia_nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_CODIGO") == 0 ) && ( AV87WWGuiaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV89WWGuiaDS_4_Guia_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] = @AV89WWGuiaDS_4_Guia_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] = @AV89WWGuiaDS_4_Guia_codigo1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV90WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV92WWGuiaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWGuiaDS_8_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like @lV93WWGuiaDS_8_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like @lV93WWGuiaDS_8_Guia_nome2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV90WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV92WWGuiaDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWGuiaDS_8_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like '%' + @lV93WWGuiaDS_8_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like '%' + @lV93WWGuiaDS_8_Guia_nome2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV90WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV91WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_CODIGO") == 0 ) && ( AV92WWGuiaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV94WWGuiaDS_9_Guia_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] = @AV94WWGuiaDS_9_Guia_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] = @AV94WWGuiaDS_9_Guia_codigo2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV95WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV96WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV97WWGuiaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWGuiaDS_13_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like @lV98WWGuiaDS_13_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like @lV98WWGuiaDS_13_Guia_nome3)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV95WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV96WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV97WWGuiaDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWGuiaDS_13_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like '%' + @lV98WWGuiaDS_13_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like '%' + @lV98WWGuiaDS_13_Guia_nome3)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV95WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV96WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_CODIGO") == 0 ) && ( AV97WWGuiaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV99WWGuiaDS_14_Guia_codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] = @AV99WWGuiaDS_14_Guia_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] = @AV99WWGuiaDS_14_Guia_codigo3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV100WWGuiaDS_15_Tfguia_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] >= @AV100WWGuiaDS_15_Tfguia_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] >= @AV100WWGuiaDS_15_Tfguia_codigo)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV101WWGuiaDS_16_Tfguia_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] <= @AV101WWGuiaDS_16_Tfguia_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] <= @AV101WWGuiaDS_16_Tfguia_codigo_to)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWGuiaDS_18_Tfguia_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWGuiaDS_17_Tfguia_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] like @lV102WWGuiaDS_17_Tfguia_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] like @lV102WWGuiaDS_17_Tfguia_nome)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWGuiaDS_18_Tfguia_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Nome] = @AV103WWGuiaDS_18_Tfguia_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Nome] = @AV103WWGuiaDS_18_Tfguia_nome_sel)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWGuiaDS_20_Tfguia_versao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWGuiaDS_19_Tfguia_versao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Versao] like @lV104WWGuiaDS_19_Tfguia_versao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Versao] like @lV104WWGuiaDS_19_Tfguia_versao)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWGuiaDS_20_Tfguia_versao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Versao] = @AV105WWGuiaDS_20_Tfguia_versao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Versao] = @AV105WWGuiaDS_20_Tfguia_versao_sel)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV106WWGuiaDS_21_Tfguia_ano) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Ano] >= @AV106WWGuiaDS_21_Tfguia_ano)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Ano] >= @AV106WWGuiaDS_21_Tfguia_ano)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (0==AV107WWGuiaDS_22_Tfguia_ano_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Ano] <= @AV107WWGuiaDS_22_Tfguia_ano_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Ano] <= @AV107WWGuiaDS_22_Tfguia_ano_to)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H005G2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (short)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
               case 1 :
                     return conditional_H005G3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (short)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH005G2 ;
          prmH005G2 = new Object[] {
          new Object[] {"@lV88WWGuiaDS_3_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV88WWGuiaDS_3_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWGuiaDS_4_Guia_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV93WWGuiaDS_8_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWGuiaDS_8_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV94WWGuiaDS_9_Guia_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWGuiaDS_13_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWGuiaDS_13_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWGuiaDS_14_Guia_codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV100WWGuiaDS_15_Tfguia_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV101WWGuiaDS_16_Tfguia_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV102WWGuiaDS_17_Tfguia_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV103WWGuiaDS_18_Tfguia_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWGuiaDS_19_Tfguia_versao",SqlDbType.Char,15,0} ,
          new Object[] {"@AV105WWGuiaDS_20_Tfguia_versao_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV106WWGuiaDS_21_Tfguia_ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV107WWGuiaDS_22_Tfguia_ano_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH005G3 ;
          prmH005G3 = new Object[] {
          new Object[] {"@lV88WWGuiaDS_3_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV88WWGuiaDS_3_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWGuiaDS_4_Guia_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV93WWGuiaDS_8_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWGuiaDS_8_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV94WWGuiaDS_9_Guia_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWGuiaDS_13_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWGuiaDS_13_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWGuiaDS_14_Guia_codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV100WWGuiaDS_15_Tfguia_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV101WWGuiaDS_16_Tfguia_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV102WWGuiaDS_17_Tfguia_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV103WWGuiaDS_18_Tfguia_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWGuiaDS_19_Tfguia_versao",SqlDbType.Char,15,0} ,
          new Object[] {"@AV105WWGuiaDS_20_Tfguia_versao_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV106WWGuiaDS_21_Tfguia_ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV107WWGuiaDS_22_Tfguia_ano_to",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H005G2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005G2,11,0,true,false )
             ,new CursorDef("H005G3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005G3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                return;
       }
    }

 }

}
