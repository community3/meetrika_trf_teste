/*
               File: HistoricoConsumo
        Description: Historico Consumo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:53.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class historicoconsumo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A1563HistoricoConsumo_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1563HistoricoConsumo_UsuarioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1563HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A1563HistoricoConsumo_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7HistoricoConsumo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7HistoricoConsumo_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vHISTORICOCONSUMO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7HistoricoConsumo_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Historico Consumo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtHistoricoConsumo_SaldoContratoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public historicoconsumo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public historicoconsumo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_HistoricoConsumo_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7HistoricoConsumo_Codigo = aP1_HistoricoConsumo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_40180( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_40180e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_40180( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_40180( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_40180e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_60_40180( true) ;
         }
         return  ;
      }

      protected void wb_table3_60_40180e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_40180e( true) ;
         }
         else
         {
            wb_table1_2_40180e( false) ;
         }
      }

      protected void wb_table3_60_40180( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_60_40180e( true) ;
         }
         else
         {
            wb_table3_60_40180e( false) ;
         }
      }

      protected void wb_table2_5_40180( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_40180( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_40180e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_40180e( true) ;
         }
         else
         {
            wb_table2_5_40180e( false) ;
         }
      }

      protected void wb_table4_13_40180( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_codigo_Internalname, "Historico Consumo", "", "", lblTextblockhistoricoconsumo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0, ",", "")), ((edtHistoricoConsumo_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtHistoricoConsumo_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_saldocontratocod_Internalname, "Saldo", "", "", lblTextblockhistoricoconsumo_saldocontratocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_SaldoContratoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0, ",", "")), ((edtHistoricoConsumo_SaldoContratoCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_SaldoContratoCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtHistoricoConsumo_SaldoContratoCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_notaempenhocod_Internalname, "Nota Emprenho", "", "", lblTextblockhistoricoconsumo_notaempenhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_28_40180( true) ;
         }
         return  ;
      }

      protected void wb_table5_28_40180e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_contratocod_Internalname, "Contrato", "", "", lblTextblockhistoricoconsumo_contratocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_ContratoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0, ",", "")), ((edtHistoricoConsumo_ContratoCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_ContratoCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtHistoricoConsumo_ContratoCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_usuariocod_Internalname, "Usu�rio", "", "", lblTextblockhistoricoconsumo_usuariocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A1563HistoricoConsumo_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_UsuarioCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtHistoricoConsumo_UsuarioCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_data_Internalname, "Data", "", "", lblTextblockhistoricoconsumo_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            wb_table6_50_40180( true) ;
         }
         return  ;
      }

      protected void wb_table6_50_40180e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_40180e( true) ;
         }
         else
         {
            wb_table4_13_40180e( false) ;
         }
      }

      protected void wb_table6_50_40180( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedhistoricoconsumo_data_Internalname, tblTablemergedhistoricoconsumo_data_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtHistoricoConsumo_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_Data_Internalname, context.localUtil.TToC( A1577HistoricoConsumo_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1577HistoricoConsumo_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_Data_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtHistoricoConsumo_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_HistoricoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtHistoricoConsumo_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtHistoricoConsumo_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_valor_Internalname, "Valor", "", "", lblTextblockhistoricoconsumo_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A1578HistoricoConsumo_Valor, 18, 5, ",", "")), ((edtHistoricoConsumo_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtHistoricoConsumo_Valor_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_50_40180e( true) ;
         }
         else
         {
            wb_table6_50_40180e( false) ;
         }
      }

      protected void wb_table5_28_40180( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedhistoricoconsumo_notaempenhocod_Internalname, tblTablemergedhistoricoconsumo_notaempenhocod_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_NotaEmpenhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0, ",", "")), ((edtHistoricoConsumo_NotaEmpenhoCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtHistoricoConsumo_NotaEmpenhoCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_contagemresultadocod_Internalname, "Contagem", "", "", lblTextblockhistoricoconsumo_contagemresultadocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_ContagemResultadoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0, ",", "")), ((edtHistoricoConsumo_ContagemResultadoCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_ContagemResultadoCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtHistoricoConsumo_ContagemResultadoCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_HistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_28_40180e( true) ;
         }
         else
         {
            wb_table5_28_40180e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11402 */
         E11402 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1562HistoricoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_SaldoContratoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_SaldoContratoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "HISTORICOCONSUMO_SALDOCONTRATOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtHistoricoConsumo_SaldoContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1580HistoricoConsumo_SaldoContratoCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1580HistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0)));
               }
               else
               {
                  A1580HistoricoConsumo_SaldoContratoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_SaldoContratoCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1580HistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_NotaEmpenhoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_NotaEmpenhoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "HISTORICOCONSUMO_NOTAEMPENHOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtHistoricoConsumo_NotaEmpenhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1581HistoricoConsumo_NotaEmpenhoCod = 0;
                  n1581HistoricoConsumo_NotaEmpenhoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1581HistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0)));
               }
               else
               {
                  A1581HistoricoConsumo_NotaEmpenhoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_NotaEmpenhoCod_Internalname), ",", "."));
                  n1581HistoricoConsumo_NotaEmpenhoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1581HistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0)));
               }
               n1581HistoricoConsumo_NotaEmpenhoCod = ((0==A1581HistoricoConsumo_NotaEmpenhoCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContagemResultadoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContagemResultadoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "HISTORICOCONSUMO_CONTAGEMRESULTADOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtHistoricoConsumo_ContagemResultadoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1582HistoricoConsumo_ContagemResultadoCod = 0;
                  n1582HistoricoConsumo_ContagemResultadoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1582HistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0)));
               }
               else
               {
                  A1582HistoricoConsumo_ContagemResultadoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContagemResultadoCod_Internalname), ",", "."));
                  n1582HistoricoConsumo_ContagemResultadoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1582HistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0)));
               }
               n1582HistoricoConsumo_ContagemResultadoCod = ((0==A1582HistoricoConsumo_ContagemResultadoCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContratoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContratoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "HISTORICOCONSUMO_CONTRATOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtHistoricoConsumo_ContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1579HistoricoConsumo_ContratoCod = 0;
                  n1579HistoricoConsumo_ContratoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1579HistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0)));
               }
               else
               {
                  A1579HistoricoConsumo_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContratoCod_Internalname), ",", "."));
                  n1579HistoricoConsumo_ContratoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1579HistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0)));
               }
               n1579HistoricoConsumo_ContratoCod = ((0==A1579HistoricoConsumo_ContratoCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_UsuarioCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_UsuarioCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "HISTORICOCONSUMO_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtHistoricoConsumo_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1563HistoricoConsumo_UsuarioCod = 0;
                  n1563HistoricoConsumo_UsuarioCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1563HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0)));
               }
               else
               {
                  A1563HistoricoConsumo_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_UsuarioCod_Internalname), ",", "."));
                  n1563HistoricoConsumo_UsuarioCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1563HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0)));
               }
               n1563HistoricoConsumo_UsuarioCod = ((0==A1563HistoricoConsumo_UsuarioCod) ? true : false);
               if ( context.localUtil.VCDateTime( cgiGet( edtHistoricoConsumo_Data_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data"}), 1, "HISTORICOCONSUMO_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtHistoricoConsumo_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1577HistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1577HistoricoConsumo_Data", context.localUtil.TToC( A1577HistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1577HistoricoConsumo_Data = context.localUtil.CToT( cgiGet( edtHistoricoConsumo_Data_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1577HistoricoConsumo_Data", context.localUtil.TToC( A1577HistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_Valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtHistoricoConsumo_Valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "HISTORICOCONSUMO_VALOR");
                  AnyError = 1;
                  GX_FocusControl = edtHistoricoConsumo_Valor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1578HistoricoConsumo_Valor = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1578HistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1578HistoricoConsumo_Valor, 18, 5)));
               }
               else
               {
                  A1578HistoricoConsumo_Valor = context.localUtil.CToN( cgiGet( edtHistoricoConsumo_Valor_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1578HistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1578HistoricoConsumo_Valor, 18, 5)));
               }
               /* Read saved values. */
               Z1562HistoricoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1562HistoricoConsumo_Codigo"), ",", "."));
               Z1646HistoricoConsumo_HistoricoCodigo = cgiGet( "Z1646HistoricoConsumo_HistoricoCodigo");
               Z1577HistoricoConsumo_Data = context.localUtil.CToT( cgiGet( "Z1577HistoricoConsumo_Data"), 0);
               Z1580HistoricoConsumo_SaldoContratoCod = (int)(context.localUtil.CToN( cgiGet( "Z1580HistoricoConsumo_SaldoContratoCod"), ",", "."));
               Z1644HistoricoConsumo_SaldoContratoDtaVigIni = context.localUtil.CToD( cgiGet( "Z1644HistoricoConsumo_SaldoContratoDtaVigIni"), 0);
               Z1645HistoricoConsumo_SaldoContratoDtaVigFim = context.localUtil.CToD( cgiGet( "Z1645HistoricoConsumo_SaldoContratoDtaVigFim"), 0);
               Z1581HistoricoConsumo_NotaEmpenhoCod = (int)(context.localUtil.CToN( cgiGet( "Z1581HistoricoConsumo_NotaEmpenhoCod"), ",", "."));
               n1581HistoricoConsumo_NotaEmpenhoCod = ((0==A1581HistoricoConsumo_NotaEmpenhoCod) ? true : false);
               Z1582HistoricoConsumo_ContagemResultadoCod = (int)(context.localUtil.CToN( cgiGet( "Z1582HistoricoConsumo_ContagemResultadoCod"), ",", "."));
               n1582HistoricoConsumo_ContagemResultadoCod = ((0==A1582HistoricoConsumo_ContagemResultadoCod) ? true : false);
               Z1579HistoricoConsumo_ContratoCod = (int)(context.localUtil.CToN( cgiGet( "Z1579HistoricoConsumo_ContratoCod"), ",", "."));
               n1579HistoricoConsumo_ContratoCod = ((0==A1579HistoricoConsumo_ContratoCod) ? true : false);
               Z1578HistoricoConsumo_Valor = context.localUtil.CToN( cgiGet( "Z1578HistoricoConsumo_Valor"), ",", ".");
               Z1789HistoricoConsumo_AutorizacaoConsumoCod = (int)(context.localUtil.CToN( cgiGet( "Z1789HistoricoConsumo_AutorizacaoConsumoCod"), ",", "."));
               n1789HistoricoConsumo_AutorizacaoConsumoCod = ((0==A1789HistoricoConsumo_AutorizacaoConsumoCod) ? true : false);
               Z1563HistoricoConsumo_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z1563HistoricoConsumo_UsuarioCod"), ",", "."));
               n1563HistoricoConsumo_UsuarioCod = ((0==A1563HistoricoConsumo_UsuarioCod) ? true : false);
               A1646HistoricoConsumo_HistoricoCodigo = cgiGet( "Z1646HistoricoConsumo_HistoricoCodigo");
               A1644HistoricoConsumo_SaldoContratoDtaVigIni = context.localUtil.CToD( cgiGet( "Z1644HistoricoConsumo_SaldoContratoDtaVigIni"), 0);
               A1645HistoricoConsumo_SaldoContratoDtaVigFim = context.localUtil.CToD( cgiGet( "Z1645HistoricoConsumo_SaldoContratoDtaVigFim"), 0);
               A1789HistoricoConsumo_AutorizacaoConsumoCod = (int)(context.localUtil.CToN( cgiGet( "Z1789HistoricoConsumo_AutorizacaoConsumoCod"), ",", "."));
               n1789HistoricoConsumo_AutorizacaoConsumoCod = false;
               n1789HistoricoConsumo_AutorizacaoConsumoCod = ((0==A1789HistoricoConsumo_AutorizacaoConsumoCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1563HistoricoConsumo_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "N1563HistoricoConsumo_UsuarioCod"), ",", "."));
               n1563HistoricoConsumo_UsuarioCod = ((0==A1563HistoricoConsumo_UsuarioCod) ? true : false);
               AV7HistoricoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vHISTORICOCONSUMO_CODIGO"), ",", "."));
               AV15Insert_HistoricoConsumo_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_HISTORICOCONSUMO_USUARIOCOD"), ",", "."));
               A1646HistoricoConsumo_HistoricoCodigo = cgiGet( "HISTORICOCONSUMO_HISTORICOCODIGO");
               A1644HistoricoConsumo_SaldoContratoDtaVigIni = context.localUtil.CToD( cgiGet( "HISTORICOCONSUMO_SALDOCONTRATODTAVIGINI"), 0);
               A1645HistoricoConsumo_SaldoContratoDtaVigFim = context.localUtil.CToD( cgiGet( "HISTORICOCONSUMO_SALDOCONTRATODTAVIGFIM"), 0);
               A1789HistoricoConsumo_AutorizacaoConsumoCod = (int)(context.localUtil.CToN( cgiGet( "HISTORICOCONSUMO_AUTORIZACAOCONSUMOCOD"), ",", "."));
               n1789HistoricoConsumo_AutorizacaoConsumoCod = ((0==A1789HistoricoConsumo_AutorizacaoConsumoCod) ? true : false);
               AV19Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "HistoricoConsumo";
               A1562HistoricoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1646HistoricoConsumo_HistoricoCodigo, ""));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1644HistoricoConsumo_SaldoContratoDtaVigIni, "99/99/99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1645HistoricoConsumo_SaldoContratoDtaVigFim, "99/99/99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1789HistoricoConsumo_AutorizacaoConsumoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1562HistoricoConsumo_Codigo != Z1562HistoricoConsumo_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("historicoconsumo:[SecurityCheckFailed value for]"+"HistoricoConsumo_Codigo:"+context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("historicoconsumo:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("historicoconsumo:[SecurityCheckFailed value for]"+"HistoricoConsumo_HistoricoCodigo:"+StringUtil.RTrim( context.localUtil.Format( A1646HistoricoConsumo_HistoricoCodigo, "")));
                  GXUtil.WriteLog("historicoconsumo:[SecurityCheckFailed value for]"+"HistoricoConsumo_SaldoContratoDtaVigIni:"+context.localUtil.Format(A1644HistoricoConsumo_SaldoContratoDtaVigIni, "99/99/99"));
                  GXUtil.WriteLog("historicoconsumo:[SecurityCheckFailed value for]"+"HistoricoConsumo_SaldoContratoDtaVigFim:"+context.localUtil.Format(A1645HistoricoConsumo_SaldoContratoDtaVigFim, "99/99/99"));
                  GXUtil.WriteLog("historicoconsumo:[SecurityCheckFailed value for]"+"HistoricoConsumo_AutorizacaoConsumoCod:"+context.localUtil.Format( (decimal)(A1789HistoricoConsumo_AutorizacaoConsumoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1562HistoricoConsumo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode180 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode180;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound180 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_400( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "HISTORICOCONSUMO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtHistoricoConsumo_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11402 */
                           E11402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12402 */
                           E12402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12402 */
            E12402 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll40180( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes40180( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_400( )
      {
         BeforeValidate40180( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls40180( ) ;
            }
            else
            {
               CheckExtendedTable40180( ) ;
               CloseExtendedTableCursors40180( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption400( )
      {
      }

      protected void E11402( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV19Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV20GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20GXV1), 8, 0)));
            while ( AV20GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV14TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV20GXV1));
               if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "HistoricoConsumo_UsuarioCod") == 0 )
               {
                  AV15Insert_HistoricoConsumo_UsuarioCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Insert_HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Insert_HistoricoConsumo_UsuarioCod), 6, 0)));
               }
               AV20GXV1 = (int)(AV20GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20GXV1), 8, 0)));
            }
         }
      }

      protected void E12402( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwhistoricoconsumo.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM40180( short GX_JID )
      {
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1646HistoricoConsumo_HistoricoCodigo = T00403_A1646HistoricoConsumo_HistoricoCodigo[0];
               Z1577HistoricoConsumo_Data = T00403_A1577HistoricoConsumo_Data[0];
               Z1580HistoricoConsumo_SaldoContratoCod = T00403_A1580HistoricoConsumo_SaldoContratoCod[0];
               Z1644HistoricoConsumo_SaldoContratoDtaVigIni = T00403_A1644HistoricoConsumo_SaldoContratoDtaVigIni[0];
               Z1645HistoricoConsumo_SaldoContratoDtaVigFim = T00403_A1645HistoricoConsumo_SaldoContratoDtaVigFim[0];
               Z1581HistoricoConsumo_NotaEmpenhoCod = T00403_A1581HistoricoConsumo_NotaEmpenhoCod[0];
               Z1582HistoricoConsumo_ContagemResultadoCod = T00403_A1582HistoricoConsumo_ContagemResultadoCod[0];
               Z1579HistoricoConsumo_ContratoCod = T00403_A1579HistoricoConsumo_ContratoCod[0];
               Z1578HistoricoConsumo_Valor = T00403_A1578HistoricoConsumo_Valor[0];
               Z1789HistoricoConsumo_AutorizacaoConsumoCod = T00403_A1789HistoricoConsumo_AutorizacaoConsumoCod[0];
               Z1563HistoricoConsumo_UsuarioCod = T00403_A1563HistoricoConsumo_UsuarioCod[0];
            }
            else
            {
               Z1646HistoricoConsumo_HistoricoCodigo = A1646HistoricoConsumo_HistoricoCodigo;
               Z1577HistoricoConsumo_Data = A1577HistoricoConsumo_Data;
               Z1580HistoricoConsumo_SaldoContratoCod = A1580HistoricoConsumo_SaldoContratoCod;
               Z1644HistoricoConsumo_SaldoContratoDtaVigIni = A1644HistoricoConsumo_SaldoContratoDtaVigIni;
               Z1645HistoricoConsumo_SaldoContratoDtaVigFim = A1645HistoricoConsumo_SaldoContratoDtaVigFim;
               Z1581HistoricoConsumo_NotaEmpenhoCod = A1581HistoricoConsumo_NotaEmpenhoCod;
               Z1582HistoricoConsumo_ContagemResultadoCod = A1582HistoricoConsumo_ContagemResultadoCod;
               Z1579HistoricoConsumo_ContratoCod = A1579HistoricoConsumo_ContratoCod;
               Z1578HistoricoConsumo_Valor = A1578HistoricoConsumo_Valor;
               Z1789HistoricoConsumo_AutorizacaoConsumoCod = A1789HistoricoConsumo_AutorizacaoConsumoCod;
               Z1563HistoricoConsumo_UsuarioCod = A1563HistoricoConsumo_UsuarioCod;
            }
         }
         if ( GX_JID == -14 )
         {
            Z1562HistoricoConsumo_Codigo = A1562HistoricoConsumo_Codigo;
            Z1646HistoricoConsumo_HistoricoCodigo = A1646HistoricoConsumo_HistoricoCodigo;
            Z1577HistoricoConsumo_Data = A1577HistoricoConsumo_Data;
            Z1580HistoricoConsumo_SaldoContratoCod = A1580HistoricoConsumo_SaldoContratoCod;
            Z1644HistoricoConsumo_SaldoContratoDtaVigIni = A1644HistoricoConsumo_SaldoContratoDtaVigIni;
            Z1645HistoricoConsumo_SaldoContratoDtaVigFim = A1645HistoricoConsumo_SaldoContratoDtaVigFim;
            Z1581HistoricoConsumo_NotaEmpenhoCod = A1581HistoricoConsumo_NotaEmpenhoCod;
            Z1582HistoricoConsumo_ContagemResultadoCod = A1582HistoricoConsumo_ContagemResultadoCod;
            Z1579HistoricoConsumo_ContratoCod = A1579HistoricoConsumo_ContratoCod;
            Z1578HistoricoConsumo_Valor = A1578HistoricoConsumo_Valor;
            Z1789HistoricoConsumo_AutorizacaoConsumoCod = A1789HistoricoConsumo_AutorizacaoConsumoCod;
            Z1563HistoricoConsumo_UsuarioCod = A1563HistoricoConsumo_UsuarioCod;
         }
      }

      protected void standaloneNotModal( )
      {
         edtHistoricoConsumo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_Codigo_Enabled), 5, 0)));
         AV19Pgmname = "HistoricoConsumo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Pgmname", AV19Pgmname);
         edtHistoricoConsumo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7HistoricoConsumo_Codigo) )
         {
            A1562HistoricoConsumo_Codigo = AV7HistoricoConsumo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV15Insert_HistoricoConsumo_UsuarioCod) )
         {
            edtHistoricoConsumo_UsuarioCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_UsuarioCod_Enabled), 5, 0)));
         }
         else
         {
            edtHistoricoConsumo_UsuarioCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_UsuarioCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV15Insert_HistoricoConsumo_UsuarioCod) )
         {
            A1563HistoricoConsumo_UsuarioCod = AV15Insert_HistoricoConsumo_UsuarioCod;
            n1563HistoricoConsumo_UsuarioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1563HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load40180( )
      {
         /* Using cursor T00405 */
         pr_default.execute(3, new Object[] {A1562HistoricoConsumo_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound180 = 1;
            A1646HistoricoConsumo_HistoricoCodigo = T00405_A1646HistoricoConsumo_HistoricoCodigo[0];
            A1577HistoricoConsumo_Data = T00405_A1577HistoricoConsumo_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1577HistoricoConsumo_Data", context.localUtil.TToC( A1577HistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
            A1580HistoricoConsumo_SaldoContratoCod = T00405_A1580HistoricoConsumo_SaldoContratoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1580HistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0)));
            A1644HistoricoConsumo_SaldoContratoDtaVigIni = T00405_A1644HistoricoConsumo_SaldoContratoDtaVigIni[0];
            A1645HistoricoConsumo_SaldoContratoDtaVigFim = T00405_A1645HistoricoConsumo_SaldoContratoDtaVigFim[0];
            A1581HistoricoConsumo_NotaEmpenhoCod = T00405_A1581HistoricoConsumo_NotaEmpenhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1581HistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0)));
            n1581HistoricoConsumo_NotaEmpenhoCod = T00405_n1581HistoricoConsumo_NotaEmpenhoCod[0];
            A1582HistoricoConsumo_ContagemResultadoCod = T00405_A1582HistoricoConsumo_ContagemResultadoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1582HistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0)));
            n1582HistoricoConsumo_ContagemResultadoCod = T00405_n1582HistoricoConsumo_ContagemResultadoCod[0];
            A1579HistoricoConsumo_ContratoCod = T00405_A1579HistoricoConsumo_ContratoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1579HistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0)));
            n1579HistoricoConsumo_ContratoCod = T00405_n1579HistoricoConsumo_ContratoCod[0];
            A1578HistoricoConsumo_Valor = T00405_A1578HistoricoConsumo_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1578HistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1578HistoricoConsumo_Valor, 18, 5)));
            A1789HistoricoConsumo_AutorizacaoConsumoCod = T00405_A1789HistoricoConsumo_AutorizacaoConsumoCod[0];
            n1789HistoricoConsumo_AutorizacaoConsumoCod = T00405_n1789HistoricoConsumo_AutorizacaoConsumoCod[0];
            A1563HistoricoConsumo_UsuarioCod = T00405_A1563HistoricoConsumo_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1563HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0)));
            n1563HistoricoConsumo_UsuarioCod = T00405_n1563HistoricoConsumo_UsuarioCod[0];
            ZM40180( -14) ;
         }
         pr_default.close(3);
         OnLoadActions40180( ) ;
      }

      protected void OnLoadActions40180( )
      {
      }

      protected void CheckExtendedTable40180( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1577HistoricoConsumo_Data) || ( A1577HistoricoConsumo_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "HISTORICOCONSUMO_DATA");
            AnyError = 1;
            GX_FocusControl = edtHistoricoConsumo_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A1577HistoricoConsumo_Data) )
         {
            GX_msglist.addItem("Data � obrigat�rio.", 1, "HISTORICOCONSUMO_DATA");
            AnyError = 1;
            GX_FocusControl = edtHistoricoConsumo_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A1580HistoricoConsumo_SaldoContratoCod) )
         {
            GX_msglist.addItem("Historico Consumo_Saldo Contrato Cod � obrigat�rio.", 1, "HISTORICOCONSUMO_SALDOCONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtHistoricoConsumo_SaldoContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A1579HistoricoConsumo_ContratoCod) )
         {
            GX_msglist.addItem("Contrato C�digo � obrigat�rio.", 1, "HISTORICOCONSUMO_CONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtHistoricoConsumo_ContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00404 */
         pr_default.execute(2, new Object[] {n1563HistoricoConsumo_UsuarioCod, A1563HistoricoConsumo_UsuarioCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A1563HistoricoConsumo_UsuarioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Historico Consumo Usuario'.", "ForeignKeyNotFound", 1, "HISTORICOCONSUMO_USUARIOCOD");
               AnyError = 1;
               GX_FocusControl = edtHistoricoConsumo_UsuarioCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(2);
         if ( (0==A1563HistoricoConsumo_UsuarioCod) )
         {
            GX_msglist.addItem("Usuario C�digo � obrigat�rio.", 1, "HISTORICOCONSUMO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtHistoricoConsumo_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (Convert.ToDecimal(0)==A1578HistoricoConsumo_Valor) )
         {
            GX_msglist.addItem("Valor � obrigat�rio.", 1, "HISTORICOCONSUMO_VALOR");
            AnyError = 1;
            GX_FocusControl = edtHistoricoConsumo_Valor_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors40180( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_15( int A1563HistoricoConsumo_UsuarioCod )
      {
         /* Using cursor T00406 */
         pr_default.execute(4, new Object[] {n1563HistoricoConsumo_UsuarioCod, A1563HistoricoConsumo_UsuarioCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A1563HistoricoConsumo_UsuarioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Historico Consumo Usuario'.", "ForeignKeyNotFound", 1, "HISTORICOCONSUMO_USUARIOCOD");
               AnyError = 1;
               GX_FocusControl = edtHistoricoConsumo_UsuarioCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey40180( )
      {
         /* Using cursor T00407 */
         pr_default.execute(5, new Object[] {A1562HistoricoConsumo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound180 = 1;
         }
         else
         {
            RcdFound180 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00403 */
         pr_default.execute(1, new Object[] {A1562HistoricoConsumo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM40180( 14) ;
            RcdFound180 = 1;
            A1562HistoricoConsumo_Codigo = T00403_A1562HistoricoConsumo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
            A1646HistoricoConsumo_HistoricoCodigo = T00403_A1646HistoricoConsumo_HistoricoCodigo[0];
            A1577HistoricoConsumo_Data = T00403_A1577HistoricoConsumo_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1577HistoricoConsumo_Data", context.localUtil.TToC( A1577HistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
            A1580HistoricoConsumo_SaldoContratoCod = T00403_A1580HistoricoConsumo_SaldoContratoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1580HistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0)));
            A1644HistoricoConsumo_SaldoContratoDtaVigIni = T00403_A1644HistoricoConsumo_SaldoContratoDtaVigIni[0];
            A1645HistoricoConsumo_SaldoContratoDtaVigFim = T00403_A1645HistoricoConsumo_SaldoContratoDtaVigFim[0];
            A1581HistoricoConsumo_NotaEmpenhoCod = T00403_A1581HistoricoConsumo_NotaEmpenhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1581HistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0)));
            n1581HistoricoConsumo_NotaEmpenhoCod = T00403_n1581HistoricoConsumo_NotaEmpenhoCod[0];
            A1582HistoricoConsumo_ContagemResultadoCod = T00403_A1582HistoricoConsumo_ContagemResultadoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1582HistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0)));
            n1582HistoricoConsumo_ContagemResultadoCod = T00403_n1582HistoricoConsumo_ContagemResultadoCod[0];
            A1579HistoricoConsumo_ContratoCod = T00403_A1579HistoricoConsumo_ContratoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1579HistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0)));
            n1579HistoricoConsumo_ContratoCod = T00403_n1579HistoricoConsumo_ContratoCod[0];
            A1578HistoricoConsumo_Valor = T00403_A1578HistoricoConsumo_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1578HistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1578HistoricoConsumo_Valor, 18, 5)));
            A1789HistoricoConsumo_AutorizacaoConsumoCod = T00403_A1789HistoricoConsumo_AutorizacaoConsumoCod[0];
            n1789HistoricoConsumo_AutorizacaoConsumoCod = T00403_n1789HistoricoConsumo_AutorizacaoConsumoCod[0];
            A1563HistoricoConsumo_UsuarioCod = T00403_A1563HistoricoConsumo_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1563HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0)));
            n1563HistoricoConsumo_UsuarioCod = T00403_n1563HistoricoConsumo_UsuarioCod[0];
            Z1562HistoricoConsumo_Codigo = A1562HistoricoConsumo_Codigo;
            sMode180 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load40180( ) ;
            if ( AnyError == 1 )
            {
               RcdFound180 = 0;
               InitializeNonKey40180( ) ;
            }
            Gx_mode = sMode180;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound180 = 0;
            InitializeNonKey40180( ) ;
            sMode180 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode180;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey40180( ) ;
         if ( RcdFound180 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound180 = 0;
         /* Using cursor T00408 */
         pr_default.execute(6, new Object[] {A1562HistoricoConsumo_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00408_A1562HistoricoConsumo_Codigo[0] < A1562HistoricoConsumo_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00408_A1562HistoricoConsumo_Codigo[0] > A1562HistoricoConsumo_Codigo ) ) )
            {
               A1562HistoricoConsumo_Codigo = T00408_A1562HistoricoConsumo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
               RcdFound180 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound180 = 0;
         /* Using cursor T00409 */
         pr_default.execute(7, new Object[] {A1562HistoricoConsumo_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00409_A1562HistoricoConsumo_Codigo[0] > A1562HistoricoConsumo_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00409_A1562HistoricoConsumo_Codigo[0] < A1562HistoricoConsumo_Codigo ) ) )
            {
               A1562HistoricoConsumo_Codigo = T00409_A1562HistoricoConsumo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
               RcdFound180 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey40180( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtHistoricoConsumo_SaldoContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert40180( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound180 == 1 )
            {
               if ( A1562HistoricoConsumo_Codigo != Z1562HistoricoConsumo_Codigo )
               {
                  A1562HistoricoConsumo_Codigo = Z1562HistoricoConsumo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "HISTORICOCONSUMO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtHistoricoConsumo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtHistoricoConsumo_SaldoContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update40180( ) ;
                  GX_FocusControl = edtHistoricoConsumo_SaldoContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1562HistoricoConsumo_Codigo != Z1562HistoricoConsumo_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtHistoricoConsumo_SaldoContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert40180( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "HISTORICOCONSUMO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtHistoricoConsumo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtHistoricoConsumo_SaldoContratoCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert40180( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1562HistoricoConsumo_Codigo != Z1562HistoricoConsumo_Codigo )
         {
            A1562HistoricoConsumo_Codigo = Z1562HistoricoConsumo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "HISTORICOCONSUMO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtHistoricoConsumo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtHistoricoConsumo_SaldoContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency40180( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00402 */
            pr_default.execute(0, new Object[] {A1562HistoricoConsumo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"HistoricoConsumo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1646HistoricoConsumo_HistoricoCodigo, T00402_A1646HistoricoConsumo_HistoricoCodigo[0]) != 0 ) || ( Z1577HistoricoConsumo_Data != T00402_A1577HistoricoConsumo_Data[0] ) || ( Z1580HistoricoConsumo_SaldoContratoCod != T00402_A1580HistoricoConsumo_SaldoContratoCod[0] ) || ( Z1644HistoricoConsumo_SaldoContratoDtaVigIni != T00402_A1644HistoricoConsumo_SaldoContratoDtaVigIni[0] ) || ( Z1645HistoricoConsumo_SaldoContratoDtaVigFim != T00402_A1645HistoricoConsumo_SaldoContratoDtaVigFim[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1581HistoricoConsumo_NotaEmpenhoCod != T00402_A1581HistoricoConsumo_NotaEmpenhoCod[0] ) || ( Z1582HistoricoConsumo_ContagemResultadoCod != T00402_A1582HistoricoConsumo_ContagemResultadoCod[0] ) || ( Z1579HistoricoConsumo_ContratoCod != T00402_A1579HistoricoConsumo_ContratoCod[0] ) || ( Z1578HistoricoConsumo_Valor != T00402_A1578HistoricoConsumo_Valor[0] ) || ( Z1789HistoricoConsumo_AutorizacaoConsumoCod != T00402_A1789HistoricoConsumo_AutorizacaoConsumoCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1563HistoricoConsumo_UsuarioCod != T00402_A1563HistoricoConsumo_UsuarioCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z1646HistoricoConsumo_HistoricoCodigo, T00402_A1646HistoricoConsumo_HistoricoCodigo[0]) != 0 )
               {
                  GXUtil.WriteLog("historicoconsumo:[seudo value changed for attri]"+"HistoricoConsumo_HistoricoCodigo");
                  GXUtil.WriteLogRaw("Old: ",Z1646HistoricoConsumo_HistoricoCodigo);
                  GXUtil.WriteLogRaw("Current: ",T00402_A1646HistoricoConsumo_HistoricoCodigo[0]);
               }
               if ( Z1577HistoricoConsumo_Data != T00402_A1577HistoricoConsumo_Data[0] )
               {
                  GXUtil.WriteLog("historicoconsumo:[seudo value changed for attri]"+"HistoricoConsumo_Data");
                  GXUtil.WriteLogRaw("Old: ",Z1577HistoricoConsumo_Data);
                  GXUtil.WriteLogRaw("Current: ",T00402_A1577HistoricoConsumo_Data[0]);
               }
               if ( Z1580HistoricoConsumo_SaldoContratoCod != T00402_A1580HistoricoConsumo_SaldoContratoCod[0] )
               {
                  GXUtil.WriteLog("historicoconsumo:[seudo value changed for attri]"+"HistoricoConsumo_SaldoContratoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1580HistoricoConsumo_SaldoContratoCod);
                  GXUtil.WriteLogRaw("Current: ",T00402_A1580HistoricoConsumo_SaldoContratoCod[0]);
               }
               if ( Z1644HistoricoConsumo_SaldoContratoDtaVigIni != T00402_A1644HistoricoConsumo_SaldoContratoDtaVigIni[0] )
               {
                  GXUtil.WriteLog("historicoconsumo:[seudo value changed for attri]"+"HistoricoConsumo_SaldoContratoDtaVigIni");
                  GXUtil.WriteLogRaw("Old: ",Z1644HistoricoConsumo_SaldoContratoDtaVigIni);
                  GXUtil.WriteLogRaw("Current: ",T00402_A1644HistoricoConsumo_SaldoContratoDtaVigIni[0]);
               }
               if ( Z1645HistoricoConsumo_SaldoContratoDtaVigFim != T00402_A1645HistoricoConsumo_SaldoContratoDtaVigFim[0] )
               {
                  GXUtil.WriteLog("historicoconsumo:[seudo value changed for attri]"+"HistoricoConsumo_SaldoContratoDtaVigFim");
                  GXUtil.WriteLogRaw("Old: ",Z1645HistoricoConsumo_SaldoContratoDtaVigFim);
                  GXUtil.WriteLogRaw("Current: ",T00402_A1645HistoricoConsumo_SaldoContratoDtaVigFim[0]);
               }
               if ( Z1581HistoricoConsumo_NotaEmpenhoCod != T00402_A1581HistoricoConsumo_NotaEmpenhoCod[0] )
               {
                  GXUtil.WriteLog("historicoconsumo:[seudo value changed for attri]"+"HistoricoConsumo_NotaEmpenhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1581HistoricoConsumo_NotaEmpenhoCod);
                  GXUtil.WriteLogRaw("Current: ",T00402_A1581HistoricoConsumo_NotaEmpenhoCod[0]);
               }
               if ( Z1582HistoricoConsumo_ContagemResultadoCod != T00402_A1582HistoricoConsumo_ContagemResultadoCod[0] )
               {
                  GXUtil.WriteLog("historicoconsumo:[seudo value changed for attri]"+"HistoricoConsumo_ContagemResultadoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1582HistoricoConsumo_ContagemResultadoCod);
                  GXUtil.WriteLogRaw("Current: ",T00402_A1582HistoricoConsumo_ContagemResultadoCod[0]);
               }
               if ( Z1579HistoricoConsumo_ContratoCod != T00402_A1579HistoricoConsumo_ContratoCod[0] )
               {
                  GXUtil.WriteLog("historicoconsumo:[seudo value changed for attri]"+"HistoricoConsumo_ContratoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1579HistoricoConsumo_ContratoCod);
                  GXUtil.WriteLogRaw("Current: ",T00402_A1579HistoricoConsumo_ContratoCod[0]);
               }
               if ( Z1578HistoricoConsumo_Valor != T00402_A1578HistoricoConsumo_Valor[0] )
               {
                  GXUtil.WriteLog("historicoconsumo:[seudo value changed for attri]"+"HistoricoConsumo_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z1578HistoricoConsumo_Valor);
                  GXUtil.WriteLogRaw("Current: ",T00402_A1578HistoricoConsumo_Valor[0]);
               }
               if ( Z1789HistoricoConsumo_AutorizacaoConsumoCod != T00402_A1789HistoricoConsumo_AutorizacaoConsumoCod[0] )
               {
                  GXUtil.WriteLog("historicoconsumo:[seudo value changed for attri]"+"HistoricoConsumo_AutorizacaoConsumoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1789HistoricoConsumo_AutorizacaoConsumoCod);
                  GXUtil.WriteLogRaw("Current: ",T00402_A1789HistoricoConsumo_AutorizacaoConsumoCod[0]);
               }
               if ( Z1563HistoricoConsumo_UsuarioCod != T00402_A1563HistoricoConsumo_UsuarioCod[0] )
               {
                  GXUtil.WriteLog("historicoconsumo:[seudo value changed for attri]"+"HistoricoConsumo_UsuarioCod");
                  GXUtil.WriteLogRaw("Old: ",Z1563HistoricoConsumo_UsuarioCod);
                  GXUtil.WriteLogRaw("Current: ",T00402_A1563HistoricoConsumo_UsuarioCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"HistoricoConsumo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert40180( )
      {
         BeforeValidate40180( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable40180( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM40180( 0) ;
            CheckOptimisticConcurrency40180( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm40180( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert40180( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004010 */
                     pr_default.execute(8, new Object[] {A1646HistoricoConsumo_HistoricoCodigo, A1577HistoricoConsumo_Data, A1580HistoricoConsumo_SaldoContratoCod, A1644HistoricoConsumo_SaldoContratoDtaVigIni, A1645HistoricoConsumo_SaldoContratoDtaVigFim, n1581HistoricoConsumo_NotaEmpenhoCod, A1581HistoricoConsumo_NotaEmpenhoCod, n1582HistoricoConsumo_ContagemResultadoCod, A1582HistoricoConsumo_ContagemResultadoCod, n1579HistoricoConsumo_ContratoCod, A1579HistoricoConsumo_ContratoCod, A1578HistoricoConsumo_Valor, n1789HistoricoConsumo_AutorizacaoConsumoCod, A1789HistoricoConsumo_AutorizacaoConsumoCod, n1563HistoricoConsumo_UsuarioCod, A1563HistoricoConsumo_UsuarioCod});
                     A1562HistoricoConsumo_Codigo = T004010_A1562HistoricoConsumo_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("HistoricoConsumo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption400( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load40180( ) ;
            }
            EndLevel40180( ) ;
         }
         CloseExtendedTableCursors40180( ) ;
      }

      protected void Update40180( )
      {
         BeforeValidate40180( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable40180( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency40180( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm40180( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate40180( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004011 */
                     pr_default.execute(9, new Object[] {A1646HistoricoConsumo_HistoricoCodigo, A1577HistoricoConsumo_Data, A1580HistoricoConsumo_SaldoContratoCod, A1644HistoricoConsumo_SaldoContratoDtaVigIni, A1645HistoricoConsumo_SaldoContratoDtaVigFim, n1581HistoricoConsumo_NotaEmpenhoCod, A1581HistoricoConsumo_NotaEmpenhoCod, n1582HistoricoConsumo_ContagemResultadoCod, A1582HistoricoConsumo_ContagemResultadoCod, n1579HistoricoConsumo_ContratoCod, A1579HistoricoConsumo_ContratoCod, A1578HistoricoConsumo_Valor, n1789HistoricoConsumo_AutorizacaoConsumoCod, A1789HistoricoConsumo_AutorizacaoConsumoCod, n1563HistoricoConsumo_UsuarioCod, A1563HistoricoConsumo_UsuarioCod, A1562HistoricoConsumo_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("HistoricoConsumo") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"HistoricoConsumo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate40180( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel40180( ) ;
         }
         CloseExtendedTableCursors40180( ) ;
      }

      protected void DeferredUpdate40180( )
      {
      }

      protected void delete( )
      {
         BeforeValidate40180( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency40180( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls40180( ) ;
            AfterConfirm40180( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete40180( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004012 */
                  pr_default.execute(10, new Object[] {A1562HistoricoConsumo_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("HistoricoConsumo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode180 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel40180( ) ;
         Gx_mode = sMode180;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls40180( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel40180( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete40180( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "HistoricoConsumo");
            if ( AnyError == 0 )
            {
               ConfirmValues400( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "HistoricoConsumo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart40180( )
      {
         /* Scan By routine */
         /* Using cursor T004013 */
         pr_default.execute(11);
         RcdFound180 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound180 = 1;
            A1562HistoricoConsumo_Codigo = T004013_A1562HistoricoConsumo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext40180( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound180 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound180 = 1;
            A1562HistoricoConsumo_Codigo = T004013_A1562HistoricoConsumo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd40180( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm40180( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert40180( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate40180( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete40180( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete40180( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate40180( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes40180( )
      {
         edtHistoricoConsumo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_Codigo_Enabled), 5, 0)));
         edtHistoricoConsumo_SaldoContratoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_SaldoContratoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_SaldoContratoCod_Enabled), 5, 0)));
         edtHistoricoConsumo_NotaEmpenhoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_NotaEmpenhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_NotaEmpenhoCod_Enabled), 5, 0)));
         edtHistoricoConsumo_ContagemResultadoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_ContagemResultadoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_ContagemResultadoCod_Enabled), 5, 0)));
         edtHistoricoConsumo_ContratoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_ContratoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_ContratoCod_Enabled), 5, 0)));
         edtHistoricoConsumo_UsuarioCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_UsuarioCod_Enabled), 5, 0)));
         edtHistoricoConsumo_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_Data_Enabled), 5, 0)));
         edtHistoricoConsumo_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtHistoricoConsumo_Valor_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues400( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299315516");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("historicoconsumo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7HistoricoConsumo_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1562HistoricoConsumo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1646HistoricoConsumo_HistoricoCodigo", StringUtil.RTrim( Z1646HistoricoConsumo_HistoricoCodigo));
         GxWebStd.gx_hidden_field( context, "Z1577HistoricoConsumo_Data", context.localUtil.TToC( Z1577HistoricoConsumo_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1580HistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1580HistoricoConsumo_SaldoContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1644HistoricoConsumo_SaldoContratoDtaVigIni", context.localUtil.DToC( Z1644HistoricoConsumo_SaldoContratoDtaVigIni, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1645HistoricoConsumo_SaldoContratoDtaVigFim", context.localUtil.DToC( Z1645HistoricoConsumo_SaldoContratoDtaVigFim, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1581HistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1581HistoricoConsumo_NotaEmpenhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1582HistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1582HistoricoConsumo_ContagemResultadoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1579HistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1579HistoricoConsumo_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1578HistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.NToC( Z1578HistoricoConsumo_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1789HistoricoConsumo_AutorizacaoConsumoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1789HistoricoConsumo_AutorizacaoConsumoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1563HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1563HistoricoConsumo_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1563HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vHISTORICOCONSUMO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7HistoricoConsumo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_HISTORICOCONSUMO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Insert_HistoricoConsumo_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_HISTORICOCODIGO", StringUtil.RTrim( A1646HistoricoConsumo_HistoricoCodigo));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_SALDOCONTRATODTAVIGINI", context.localUtil.DToC( A1644HistoricoConsumo_SaldoContratoDtaVigIni, 0, "/"));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_SALDOCONTRATODTAVIGFIM", context.localUtil.DToC( A1645HistoricoConsumo_SaldoContratoDtaVigFim, 0, "/"));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_AUTORIZACAOCONSUMOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1789HistoricoConsumo_AutorizacaoConsumoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV19Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vHISTORICOCONSUMO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7HistoricoConsumo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "HistoricoConsumo";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1646HistoricoConsumo_HistoricoCodigo, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1644HistoricoConsumo_SaldoContratoDtaVigIni, "99/99/99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1645HistoricoConsumo_SaldoContratoDtaVigFim, "99/99/99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1789HistoricoConsumo_AutorizacaoConsumoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("historicoconsumo:[SendSecurityCheck value for]"+"HistoricoConsumo_Codigo:"+context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("historicoconsumo:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("historicoconsumo:[SendSecurityCheck value for]"+"HistoricoConsumo_HistoricoCodigo:"+StringUtil.RTrim( context.localUtil.Format( A1646HistoricoConsumo_HistoricoCodigo, "")));
         GXUtil.WriteLog("historicoconsumo:[SendSecurityCheck value for]"+"HistoricoConsumo_SaldoContratoDtaVigIni:"+context.localUtil.Format(A1644HistoricoConsumo_SaldoContratoDtaVigIni, "99/99/99"));
         GXUtil.WriteLog("historicoconsumo:[SendSecurityCheck value for]"+"HistoricoConsumo_SaldoContratoDtaVigFim:"+context.localUtil.Format(A1645HistoricoConsumo_SaldoContratoDtaVigFim, "99/99/99"));
         GXUtil.WriteLog("historicoconsumo:[SendSecurityCheck value for]"+"HistoricoConsumo_AutorizacaoConsumoCod:"+context.localUtil.Format( (decimal)(A1789HistoricoConsumo_AutorizacaoConsumoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("historicoconsumo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7HistoricoConsumo_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "HistoricoConsumo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Historico Consumo" ;
      }

      protected void InitializeNonKey40180( )
      {
         A1563HistoricoConsumo_UsuarioCod = 0;
         n1563HistoricoConsumo_UsuarioCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1563HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0)));
         n1563HistoricoConsumo_UsuarioCod = ((0==A1563HistoricoConsumo_UsuarioCod) ? true : false);
         A1646HistoricoConsumo_HistoricoCodigo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1646HistoricoConsumo_HistoricoCodigo", A1646HistoricoConsumo_HistoricoCodigo);
         A1577HistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1577HistoricoConsumo_Data", context.localUtil.TToC( A1577HistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
         A1580HistoricoConsumo_SaldoContratoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1580HistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0)));
         A1644HistoricoConsumo_SaldoContratoDtaVigIni = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1644HistoricoConsumo_SaldoContratoDtaVigIni", context.localUtil.Format(A1644HistoricoConsumo_SaldoContratoDtaVigIni, "99/99/99"));
         A1645HistoricoConsumo_SaldoContratoDtaVigFim = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1645HistoricoConsumo_SaldoContratoDtaVigFim", context.localUtil.Format(A1645HistoricoConsumo_SaldoContratoDtaVigFim, "99/99/99"));
         A1581HistoricoConsumo_NotaEmpenhoCod = 0;
         n1581HistoricoConsumo_NotaEmpenhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1581HistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0)));
         n1581HistoricoConsumo_NotaEmpenhoCod = ((0==A1581HistoricoConsumo_NotaEmpenhoCod) ? true : false);
         A1582HistoricoConsumo_ContagemResultadoCod = 0;
         n1582HistoricoConsumo_ContagemResultadoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1582HistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0)));
         n1582HistoricoConsumo_ContagemResultadoCod = ((0==A1582HistoricoConsumo_ContagemResultadoCod) ? true : false);
         A1579HistoricoConsumo_ContratoCod = 0;
         n1579HistoricoConsumo_ContratoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1579HistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0)));
         n1579HistoricoConsumo_ContratoCod = ((0==A1579HistoricoConsumo_ContratoCod) ? true : false);
         A1578HistoricoConsumo_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1578HistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1578HistoricoConsumo_Valor, 18, 5)));
         A1789HistoricoConsumo_AutorizacaoConsumoCod = 0;
         n1789HistoricoConsumo_AutorizacaoConsumoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1789HistoricoConsumo_AutorizacaoConsumoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1789HistoricoConsumo_AutorizacaoConsumoCod), 6, 0)));
         Z1646HistoricoConsumo_HistoricoCodigo = "";
         Z1577HistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
         Z1580HistoricoConsumo_SaldoContratoCod = 0;
         Z1644HistoricoConsumo_SaldoContratoDtaVigIni = DateTime.MinValue;
         Z1645HistoricoConsumo_SaldoContratoDtaVigFim = DateTime.MinValue;
         Z1581HistoricoConsumo_NotaEmpenhoCod = 0;
         Z1582HistoricoConsumo_ContagemResultadoCod = 0;
         Z1579HistoricoConsumo_ContratoCod = 0;
         Z1578HistoricoConsumo_Valor = 0;
         Z1789HistoricoConsumo_AutorizacaoConsumoCod = 0;
         Z1563HistoricoConsumo_UsuarioCod = 0;
      }

      protected void InitAll40180( )
      {
         A1562HistoricoConsumo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
         InitializeNonKey40180( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299315542");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("historicoconsumo.js", "?20205299315542");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockhistoricoconsumo_codigo_Internalname = "TEXTBLOCKHISTORICOCONSUMO_CODIGO";
         edtHistoricoConsumo_Codigo_Internalname = "HISTORICOCONSUMO_CODIGO";
         lblTextblockhistoricoconsumo_saldocontratocod_Internalname = "TEXTBLOCKHISTORICOCONSUMO_SALDOCONTRATOCOD";
         edtHistoricoConsumo_SaldoContratoCod_Internalname = "HISTORICOCONSUMO_SALDOCONTRATOCOD";
         lblTextblockhistoricoconsumo_notaempenhocod_Internalname = "TEXTBLOCKHISTORICOCONSUMO_NOTAEMPENHOCOD";
         edtHistoricoConsumo_NotaEmpenhoCod_Internalname = "HISTORICOCONSUMO_NOTAEMPENHOCOD";
         lblTextblockhistoricoconsumo_contagemresultadocod_Internalname = "TEXTBLOCKHISTORICOCONSUMO_CONTAGEMRESULTADOCOD";
         edtHistoricoConsumo_ContagemResultadoCod_Internalname = "HISTORICOCONSUMO_CONTAGEMRESULTADOCOD";
         tblTablemergedhistoricoconsumo_notaempenhocod_Internalname = "TABLEMERGEDHISTORICOCONSUMO_NOTAEMPENHOCOD";
         lblTextblockhistoricoconsumo_contratocod_Internalname = "TEXTBLOCKHISTORICOCONSUMO_CONTRATOCOD";
         edtHistoricoConsumo_ContratoCod_Internalname = "HISTORICOCONSUMO_CONTRATOCOD";
         lblTextblockhistoricoconsumo_usuariocod_Internalname = "TEXTBLOCKHISTORICOCONSUMO_USUARIOCOD";
         edtHistoricoConsumo_UsuarioCod_Internalname = "HISTORICOCONSUMO_USUARIOCOD";
         lblTextblockhistoricoconsumo_data_Internalname = "TEXTBLOCKHISTORICOCONSUMO_DATA";
         edtHistoricoConsumo_Data_Internalname = "HISTORICOCONSUMO_DATA";
         lblTextblockhistoricoconsumo_valor_Internalname = "TEXTBLOCKHISTORICOCONSUMO_VALOR";
         edtHistoricoConsumo_Valor_Internalname = "HISTORICOCONSUMO_VALOR";
         tblTablemergedhistoricoconsumo_data_Internalname = "TABLEMERGEDHISTORICOCONSUMO_DATA";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Historico Consumo";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Historico Consumo";
         edtHistoricoConsumo_ContagemResultadoCod_Jsonclick = "";
         edtHistoricoConsumo_ContagemResultadoCod_Enabled = 1;
         edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick = "";
         edtHistoricoConsumo_NotaEmpenhoCod_Enabled = 1;
         edtHistoricoConsumo_Valor_Jsonclick = "";
         edtHistoricoConsumo_Valor_Enabled = 1;
         edtHistoricoConsumo_Data_Jsonclick = "";
         edtHistoricoConsumo_Data_Enabled = 1;
         edtHistoricoConsumo_UsuarioCod_Jsonclick = "";
         edtHistoricoConsumo_UsuarioCod_Enabled = 1;
         edtHistoricoConsumo_ContratoCod_Jsonclick = "";
         edtHistoricoConsumo_ContratoCod_Enabled = 1;
         edtHistoricoConsumo_SaldoContratoCod_Jsonclick = "";
         edtHistoricoConsumo_SaldoContratoCod_Enabled = 1;
         edtHistoricoConsumo_Codigo_Jsonclick = "";
         edtHistoricoConsumo_Codigo_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Historicoconsumo_usuariocod( int GX_Parm1 )
      {
         A1563HistoricoConsumo_UsuarioCod = GX_Parm1;
         n1563HistoricoConsumo_UsuarioCod = false;
         /* Using cursor T004014 */
         pr_default.execute(12, new Object[] {n1563HistoricoConsumo_UsuarioCod, A1563HistoricoConsumo_UsuarioCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            if ( ! ( (0==A1563HistoricoConsumo_UsuarioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Historico Consumo Usuario'.", "ForeignKeyNotFound", 1, "HISTORICOCONSUMO_USUARIOCOD");
               AnyError = 1;
               GX_FocusControl = edtHistoricoConsumo_UsuarioCod_Internalname;
            }
         }
         pr_default.close(12);
         if ( (0==A1563HistoricoConsumo_UsuarioCod) )
         {
            GX_msglist.addItem("Usuario C�digo � obrigat�rio.", 1, "HISTORICOCONSUMO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtHistoricoConsumo_UsuarioCod_Internalname;
         }
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7HistoricoConsumo_Codigo',fld:'vHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12402',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1646HistoricoConsumo_HistoricoCodigo = "";
         Z1577HistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
         Z1644HistoricoConsumo_SaldoContratoDtaVigIni = DateTime.MinValue;
         Z1645HistoricoConsumo_SaldoContratoDtaVigFim = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockhistoricoconsumo_codigo_Jsonclick = "";
         lblTextblockhistoricoconsumo_saldocontratocod_Jsonclick = "";
         lblTextblockhistoricoconsumo_notaempenhocod_Jsonclick = "";
         lblTextblockhistoricoconsumo_contratocod_Jsonclick = "";
         lblTextblockhistoricoconsumo_usuariocod_Jsonclick = "";
         lblTextblockhistoricoconsumo_data_Jsonclick = "";
         A1577HistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
         lblTextblockhistoricoconsumo_valor_Jsonclick = "";
         lblTextblockhistoricoconsumo_contagemresultadocod_Jsonclick = "";
         A1646HistoricoConsumo_HistoricoCodigo = "";
         A1644HistoricoConsumo_SaldoContratoDtaVigIni = DateTime.MinValue;
         A1645HistoricoConsumo_SaldoContratoDtaVigFim = DateTime.MinValue;
         AV19Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode180 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV14TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         T00405_A1562HistoricoConsumo_Codigo = new int[1] ;
         T00405_A1646HistoricoConsumo_HistoricoCodigo = new String[] {""} ;
         T00405_A1577HistoricoConsumo_Data = new DateTime[] {DateTime.MinValue} ;
         T00405_A1580HistoricoConsumo_SaldoContratoCod = new int[1] ;
         T00405_A1644HistoricoConsumo_SaldoContratoDtaVigIni = new DateTime[] {DateTime.MinValue} ;
         T00405_A1645HistoricoConsumo_SaldoContratoDtaVigFim = new DateTime[] {DateTime.MinValue} ;
         T00405_A1581HistoricoConsumo_NotaEmpenhoCod = new int[1] ;
         T00405_n1581HistoricoConsumo_NotaEmpenhoCod = new bool[] {false} ;
         T00405_A1582HistoricoConsumo_ContagemResultadoCod = new int[1] ;
         T00405_n1582HistoricoConsumo_ContagemResultadoCod = new bool[] {false} ;
         T00405_A1579HistoricoConsumo_ContratoCod = new int[1] ;
         T00405_n1579HistoricoConsumo_ContratoCod = new bool[] {false} ;
         T00405_A1578HistoricoConsumo_Valor = new decimal[1] ;
         T00405_A1789HistoricoConsumo_AutorizacaoConsumoCod = new int[1] ;
         T00405_n1789HistoricoConsumo_AutorizacaoConsumoCod = new bool[] {false} ;
         T00405_A1563HistoricoConsumo_UsuarioCod = new int[1] ;
         T00405_n1563HistoricoConsumo_UsuarioCod = new bool[] {false} ;
         T00404_A1563HistoricoConsumo_UsuarioCod = new int[1] ;
         T00404_n1563HistoricoConsumo_UsuarioCod = new bool[] {false} ;
         T00406_A1563HistoricoConsumo_UsuarioCod = new int[1] ;
         T00406_n1563HistoricoConsumo_UsuarioCod = new bool[] {false} ;
         T00407_A1562HistoricoConsumo_Codigo = new int[1] ;
         T00403_A1562HistoricoConsumo_Codigo = new int[1] ;
         T00403_A1646HistoricoConsumo_HistoricoCodigo = new String[] {""} ;
         T00403_A1577HistoricoConsumo_Data = new DateTime[] {DateTime.MinValue} ;
         T00403_A1580HistoricoConsumo_SaldoContratoCod = new int[1] ;
         T00403_A1644HistoricoConsumo_SaldoContratoDtaVigIni = new DateTime[] {DateTime.MinValue} ;
         T00403_A1645HistoricoConsumo_SaldoContratoDtaVigFim = new DateTime[] {DateTime.MinValue} ;
         T00403_A1581HistoricoConsumo_NotaEmpenhoCod = new int[1] ;
         T00403_n1581HistoricoConsumo_NotaEmpenhoCod = new bool[] {false} ;
         T00403_A1582HistoricoConsumo_ContagemResultadoCod = new int[1] ;
         T00403_n1582HistoricoConsumo_ContagemResultadoCod = new bool[] {false} ;
         T00403_A1579HistoricoConsumo_ContratoCod = new int[1] ;
         T00403_n1579HistoricoConsumo_ContratoCod = new bool[] {false} ;
         T00403_A1578HistoricoConsumo_Valor = new decimal[1] ;
         T00403_A1789HistoricoConsumo_AutorizacaoConsumoCod = new int[1] ;
         T00403_n1789HistoricoConsumo_AutorizacaoConsumoCod = new bool[] {false} ;
         T00403_A1563HistoricoConsumo_UsuarioCod = new int[1] ;
         T00403_n1563HistoricoConsumo_UsuarioCod = new bool[] {false} ;
         T00408_A1562HistoricoConsumo_Codigo = new int[1] ;
         T00409_A1562HistoricoConsumo_Codigo = new int[1] ;
         T00402_A1562HistoricoConsumo_Codigo = new int[1] ;
         T00402_A1646HistoricoConsumo_HistoricoCodigo = new String[] {""} ;
         T00402_A1577HistoricoConsumo_Data = new DateTime[] {DateTime.MinValue} ;
         T00402_A1580HistoricoConsumo_SaldoContratoCod = new int[1] ;
         T00402_A1644HistoricoConsumo_SaldoContratoDtaVigIni = new DateTime[] {DateTime.MinValue} ;
         T00402_A1645HistoricoConsumo_SaldoContratoDtaVigFim = new DateTime[] {DateTime.MinValue} ;
         T00402_A1581HistoricoConsumo_NotaEmpenhoCod = new int[1] ;
         T00402_n1581HistoricoConsumo_NotaEmpenhoCod = new bool[] {false} ;
         T00402_A1582HistoricoConsumo_ContagemResultadoCod = new int[1] ;
         T00402_n1582HistoricoConsumo_ContagemResultadoCod = new bool[] {false} ;
         T00402_A1579HistoricoConsumo_ContratoCod = new int[1] ;
         T00402_n1579HistoricoConsumo_ContratoCod = new bool[] {false} ;
         T00402_A1578HistoricoConsumo_Valor = new decimal[1] ;
         T00402_A1789HistoricoConsumo_AutorizacaoConsumoCod = new int[1] ;
         T00402_n1789HistoricoConsumo_AutorizacaoConsumoCod = new bool[] {false} ;
         T00402_A1563HistoricoConsumo_UsuarioCod = new int[1] ;
         T00402_n1563HistoricoConsumo_UsuarioCod = new bool[] {false} ;
         T004010_A1562HistoricoConsumo_Codigo = new int[1] ;
         T004013_A1562HistoricoConsumo_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T004014_A1563HistoricoConsumo_UsuarioCod = new int[1] ;
         T004014_n1563HistoricoConsumo_UsuarioCod = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.historicoconsumo__default(),
            new Object[][] {
                new Object[] {
               T00402_A1562HistoricoConsumo_Codigo, T00402_A1646HistoricoConsumo_HistoricoCodigo, T00402_A1577HistoricoConsumo_Data, T00402_A1580HistoricoConsumo_SaldoContratoCod, T00402_A1644HistoricoConsumo_SaldoContratoDtaVigIni, T00402_A1645HistoricoConsumo_SaldoContratoDtaVigFim, T00402_A1581HistoricoConsumo_NotaEmpenhoCod, T00402_n1581HistoricoConsumo_NotaEmpenhoCod, T00402_A1582HistoricoConsumo_ContagemResultadoCod, T00402_n1582HistoricoConsumo_ContagemResultadoCod,
               T00402_A1579HistoricoConsumo_ContratoCod, T00402_n1579HistoricoConsumo_ContratoCod, T00402_A1578HistoricoConsumo_Valor, T00402_A1789HistoricoConsumo_AutorizacaoConsumoCod, T00402_n1789HistoricoConsumo_AutorizacaoConsumoCod, T00402_A1563HistoricoConsumo_UsuarioCod, T00402_n1563HistoricoConsumo_UsuarioCod
               }
               , new Object[] {
               T00403_A1562HistoricoConsumo_Codigo, T00403_A1646HistoricoConsumo_HistoricoCodigo, T00403_A1577HistoricoConsumo_Data, T00403_A1580HistoricoConsumo_SaldoContratoCod, T00403_A1644HistoricoConsumo_SaldoContratoDtaVigIni, T00403_A1645HistoricoConsumo_SaldoContratoDtaVigFim, T00403_A1581HistoricoConsumo_NotaEmpenhoCod, T00403_n1581HistoricoConsumo_NotaEmpenhoCod, T00403_A1582HistoricoConsumo_ContagemResultadoCod, T00403_n1582HistoricoConsumo_ContagemResultadoCod,
               T00403_A1579HistoricoConsumo_ContratoCod, T00403_n1579HistoricoConsumo_ContratoCod, T00403_A1578HistoricoConsumo_Valor, T00403_A1789HistoricoConsumo_AutorizacaoConsumoCod, T00403_n1789HistoricoConsumo_AutorizacaoConsumoCod, T00403_A1563HistoricoConsumo_UsuarioCod, T00403_n1563HistoricoConsumo_UsuarioCod
               }
               , new Object[] {
               T00404_A1563HistoricoConsumo_UsuarioCod
               }
               , new Object[] {
               T00405_A1562HistoricoConsumo_Codigo, T00405_A1646HistoricoConsumo_HistoricoCodigo, T00405_A1577HistoricoConsumo_Data, T00405_A1580HistoricoConsumo_SaldoContratoCod, T00405_A1644HistoricoConsumo_SaldoContratoDtaVigIni, T00405_A1645HistoricoConsumo_SaldoContratoDtaVigFim, T00405_A1581HistoricoConsumo_NotaEmpenhoCod, T00405_n1581HistoricoConsumo_NotaEmpenhoCod, T00405_A1582HistoricoConsumo_ContagemResultadoCod, T00405_n1582HistoricoConsumo_ContagemResultadoCod,
               T00405_A1579HistoricoConsumo_ContratoCod, T00405_n1579HistoricoConsumo_ContratoCod, T00405_A1578HistoricoConsumo_Valor, T00405_A1789HistoricoConsumo_AutorizacaoConsumoCod, T00405_n1789HistoricoConsumo_AutorizacaoConsumoCod, T00405_A1563HistoricoConsumo_UsuarioCod, T00405_n1563HistoricoConsumo_UsuarioCod
               }
               , new Object[] {
               T00406_A1563HistoricoConsumo_UsuarioCod
               }
               , new Object[] {
               T00407_A1562HistoricoConsumo_Codigo
               }
               , new Object[] {
               T00408_A1562HistoricoConsumo_Codigo
               }
               , new Object[] {
               T00409_A1562HistoricoConsumo_Codigo
               }
               , new Object[] {
               T004010_A1562HistoricoConsumo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004013_A1562HistoricoConsumo_Codigo
               }
               , new Object[] {
               T004014_A1563HistoricoConsumo_UsuarioCod
               }
            }
         );
         AV19Pgmname = "HistoricoConsumo";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound180 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7HistoricoConsumo_Codigo ;
      private int Z1562HistoricoConsumo_Codigo ;
      private int Z1580HistoricoConsumo_SaldoContratoCod ;
      private int Z1581HistoricoConsumo_NotaEmpenhoCod ;
      private int Z1582HistoricoConsumo_ContagemResultadoCod ;
      private int Z1579HistoricoConsumo_ContratoCod ;
      private int Z1789HistoricoConsumo_AutorizacaoConsumoCod ;
      private int Z1563HistoricoConsumo_UsuarioCod ;
      private int N1563HistoricoConsumo_UsuarioCod ;
      private int A1563HistoricoConsumo_UsuarioCod ;
      private int AV7HistoricoConsumo_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A1562HistoricoConsumo_Codigo ;
      private int edtHistoricoConsumo_Codigo_Enabled ;
      private int A1580HistoricoConsumo_SaldoContratoCod ;
      private int edtHistoricoConsumo_SaldoContratoCod_Enabled ;
      private int A1579HistoricoConsumo_ContratoCod ;
      private int edtHistoricoConsumo_ContratoCod_Enabled ;
      private int edtHistoricoConsumo_UsuarioCod_Enabled ;
      private int edtHistoricoConsumo_Data_Enabled ;
      private int edtHistoricoConsumo_Valor_Enabled ;
      private int A1581HistoricoConsumo_NotaEmpenhoCod ;
      private int edtHistoricoConsumo_NotaEmpenhoCod_Enabled ;
      private int A1582HistoricoConsumo_ContagemResultadoCod ;
      private int edtHistoricoConsumo_ContagemResultadoCod_Enabled ;
      private int A1789HistoricoConsumo_AutorizacaoConsumoCod ;
      private int AV15Insert_HistoricoConsumo_UsuarioCod ;
      private int AV20GXV1 ;
      private int idxLst ;
      private decimal Z1578HistoricoConsumo_Valor ;
      private decimal A1578HistoricoConsumo_Valor ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1646HistoricoConsumo_HistoricoCodigo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtHistoricoConsumo_SaldoContratoCod_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockhistoricoconsumo_codigo_Internalname ;
      private String lblTextblockhistoricoconsumo_codigo_Jsonclick ;
      private String edtHistoricoConsumo_Codigo_Internalname ;
      private String edtHistoricoConsumo_Codigo_Jsonclick ;
      private String lblTextblockhistoricoconsumo_saldocontratocod_Internalname ;
      private String lblTextblockhistoricoconsumo_saldocontratocod_Jsonclick ;
      private String edtHistoricoConsumo_SaldoContratoCod_Jsonclick ;
      private String lblTextblockhistoricoconsumo_notaempenhocod_Internalname ;
      private String lblTextblockhistoricoconsumo_notaempenhocod_Jsonclick ;
      private String lblTextblockhistoricoconsumo_contratocod_Internalname ;
      private String lblTextblockhistoricoconsumo_contratocod_Jsonclick ;
      private String edtHistoricoConsumo_ContratoCod_Internalname ;
      private String edtHistoricoConsumo_ContratoCod_Jsonclick ;
      private String lblTextblockhistoricoconsumo_usuariocod_Internalname ;
      private String lblTextblockhistoricoconsumo_usuariocod_Jsonclick ;
      private String edtHistoricoConsumo_UsuarioCod_Internalname ;
      private String edtHistoricoConsumo_UsuarioCod_Jsonclick ;
      private String lblTextblockhistoricoconsumo_data_Internalname ;
      private String lblTextblockhistoricoconsumo_data_Jsonclick ;
      private String tblTablemergedhistoricoconsumo_data_Internalname ;
      private String edtHistoricoConsumo_Data_Internalname ;
      private String edtHistoricoConsumo_Data_Jsonclick ;
      private String lblTextblockhistoricoconsumo_valor_Internalname ;
      private String lblTextblockhistoricoconsumo_valor_Jsonclick ;
      private String edtHistoricoConsumo_Valor_Internalname ;
      private String edtHistoricoConsumo_Valor_Jsonclick ;
      private String tblTablemergedhistoricoconsumo_notaempenhocod_Internalname ;
      private String edtHistoricoConsumo_NotaEmpenhoCod_Internalname ;
      private String edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick ;
      private String lblTextblockhistoricoconsumo_contagemresultadocod_Internalname ;
      private String lblTextblockhistoricoconsumo_contagemresultadocod_Jsonclick ;
      private String edtHistoricoConsumo_ContagemResultadoCod_Internalname ;
      private String edtHistoricoConsumo_ContagemResultadoCod_Jsonclick ;
      private String A1646HistoricoConsumo_HistoricoCodigo ;
      private String AV19Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode180 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z1577HistoricoConsumo_Data ;
      private DateTime A1577HistoricoConsumo_Data ;
      private DateTime Z1644HistoricoConsumo_SaldoContratoDtaVigIni ;
      private DateTime Z1645HistoricoConsumo_SaldoContratoDtaVigFim ;
      private DateTime A1644HistoricoConsumo_SaldoContratoDtaVigIni ;
      private DateTime A1645HistoricoConsumo_SaldoContratoDtaVigFim ;
      private bool entryPointCalled ;
      private bool n1563HistoricoConsumo_UsuarioCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1581HistoricoConsumo_NotaEmpenhoCod ;
      private bool n1582HistoricoConsumo_ContagemResultadoCod ;
      private bool n1579HistoricoConsumo_ContratoCod ;
      private bool n1789HistoricoConsumo_AutorizacaoConsumoCod ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T00405_A1562HistoricoConsumo_Codigo ;
      private String[] T00405_A1646HistoricoConsumo_HistoricoCodigo ;
      private DateTime[] T00405_A1577HistoricoConsumo_Data ;
      private int[] T00405_A1580HistoricoConsumo_SaldoContratoCod ;
      private DateTime[] T00405_A1644HistoricoConsumo_SaldoContratoDtaVigIni ;
      private DateTime[] T00405_A1645HistoricoConsumo_SaldoContratoDtaVigFim ;
      private int[] T00405_A1581HistoricoConsumo_NotaEmpenhoCod ;
      private bool[] T00405_n1581HistoricoConsumo_NotaEmpenhoCod ;
      private int[] T00405_A1582HistoricoConsumo_ContagemResultadoCod ;
      private bool[] T00405_n1582HistoricoConsumo_ContagemResultadoCod ;
      private int[] T00405_A1579HistoricoConsumo_ContratoCod ;
      private bool[] T00405_n1579HistoricoConsumo_ContratoCod ;
      private decimal[] T00405_A1578HistoricoConsumo_Valor ;
      private int[] T00405_A1789HistoricoConsumo_AutorizacaoConsumoCod ;
      private bool[] T00405_n1789HistoricoConsumo_AutorizacaoConsumoCod ;
      private int[] T00405_A1563HistoricoConsumo_UsuarioCod ;
      private bool[] T00405_n1563HistoricoConsumo_UsuarioCod ;
      private int[] T00404_A1563HistoricoConsumo_UsuarioCod ;
      private bool[] T00404_n1563HistoricoConsumo_UsuarioCod ;
      private int[] T00406_A1563HistoricoConsumo_UsuarioCod ;
      private bool[] T00406_n1563HistoricoConsumo_UsuarioCod ;
      private int[] T00407_A1562HistoricoConsumo_Codigo ;
      private int[] T00403_A1562HistoricoConsumo_Codigo ;
      private String[] T00403_A1646HistoricoConsumo_HistoricoCodigo ;
      private DateTime[] T00403_A1577HistoricoConsumo_Data ;
      private int[] T00403_A1580HistoricoConsumo_SaldoContratoCod ;
      private DateTime[] T00403_A1644HistoricoConsumo_SaldoContratoDtaVigIni ;
      private DateTime[] T00403_A1645HistoricoConsumo_SaldoContratoDtaVigFim ;
      private int[] T00403_A1581HistoricoConsumo_NotaEmpenhoCod ;
      private bool[] T00403_n1581HistoricoConsumo_NotaEmpenhoCod ;
      private int[] T00403_A1582HistoricoConsumo_ContagemResultadoCod ;
      private bool[] T00403_n1582HistoricoConsumo_ContagemResultadoCod ;
      private int[] T00403_A1579HistoricoConsumo_ContratoCod ;
      private bool[] T00403_n1579HistoricoConsumo_ContratoCod ;
      private decimal[] T00403_A1578HistoricoConsumo_Valor ;
      private int[] T00403_A1789HistoricoConsumo_AutorizacaoConsumoCod ;
      private bool[] T00403_n1789HistoricoConsumo_AutorizacaoConsumoCod ;
      private int[] T00403_A1563HistoricoConsumo_UsuarioCod ;
      private bool[] T00403_n1563HistoricoConsumo_UsuarioCod ;
      private int[] T00408_A1562HistoricoConsumo_Codigo ;
      private int[] T00409_A1562HistoricoConsumo_Codigo ;
      private int[] T00402_A1562HistoricoConsumo_Codigo ;
      private String[] T00402_A1646HistoricoConsumo_HistoricoCodigo ;
      private DateTime[] T00402_A1577HistoricoConsumo_Data ;
      private int[] T00402_A1580HistoricoConsumo_SaldoContratoCod ;
      private DateTime[] T00402_A1644HistoricoConsumo_SaldoContratoDtaVigIni ;
      private DateTime[] T00402_A1645HistoricoConsumo_SaldoContratoDtaVigFim ;
      private int[] T00402_A1581HistoricoConsumo_NotaEmpenhoCod ;
      private bool[] T00402_n1581HistoricoConsumo_NotaEmpenhoCod ;
      private int[] T00402_A1582HistoricoConsumo_ContagemResultadoCod ;
      private bool[] T00402_n1582HistoricoConsumo_ContagemResultadoCod ;
      private int[] T00402_A1579HistoricoConsumo_ContratoCod ;
      private bool[] T00402_n1579HistoricoConsumo_ContratoCod ;
      private decimal[] T00402_A1578HistoricoConsumo_Valor ;
      private int[] T00402_A1789HistoricoConsumo_AutorizacaoConsumoCod ;
      private bool[] T00402_n1789HistoricoConsumo_AutorizacaoConsumoCod ;
      private int[] T00402_A1563HistoricoConsumo_UsuarioCod ;
      private bool[] T00402_n1563HistoricoConsumo_UsuarioCod ;
      private int[] T004010_A1562HistoricoConsumo_Codigo ;
      private int[] T004013_A1562HistoricoConsumo_Codigo ;
      private int[] T004014_A1563HistoricoConsumo_UsuarioCod ;
      private bool[] T004014_n1563HistoricoConsumo_UsuarioCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV14TrnContextAtt ;
   }

   public class historicoconsumo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00405 ;
          prmT00405 = new Object[] {
          new Object[] {"@HistoricoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00404 ;
          prmT00404 = new Object[] {
          new Object[] {"@HistoricoConsumo_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00406 ;
          prmT00406 = new Object[] {
          new Object[] {"@HistoricoConsumo_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00407 ;
          prmT00407 = new Object[] {
          new Object[] {"@HistoricoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00403 ;
          prmT00403 = new Object[] {
          new Object[] {"@HistoricoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00408 ;
          prmT00408 = new Object[] {
          new Object[] {"@HistoricoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00409 ;
          prmT00409 = new Object[] {
          new Object[] {"@HistoricoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00402 ;
          prmT00402 = new Object[] {
          new Object[] {"@HistoricoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004010 ;
          prmT004010 = new Object[] {
          new Object[] {"@HistoricoConsumo_HistoricoCodigo",SqlDbType.Char,3,0} ,
          new Object[] {"@HistoricoConsumo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@HistoricoConsumo_SaldoContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_SaldoContratoDtaVigIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@HistoricoConsumo_SaldoContratoDtaVigFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@HistoricoConsumo_NotaEmpenhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_ContagemResultadoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@HistoricoConsumo_AutorizacaoConsumoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004011 ;
          prmT004011 = new Object[] {
          new Object[] {"@HistoricoConsumo_HistoricoCodigo",SqlDbType.Char,3,0} ,
          new Object[] {"@HistoricoConsumo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@HistoricoConsumo_SaldoContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_SaldoContratoDtaVigIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@HistoricoConsumo_SaldoContratoDtaVigFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@HistoricoConsumo_NotaEmpenhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_ContagemResultadoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@HistoricoConsumo_AutorizacaoConsumoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004012 ;
          prmT004012 = new Object[] {
          new Object[] {"@HistoricoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004013 ;
          prmT004013 = new Object[] {
          } ;
          Object[] prmT004014 ;
          prmT004014 = new Object[] {
          new Object[] {"@HistoricoConsumo_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00402", "SELECT [HistoricoConsumo_Codigo], [HistoricoConsumo_HistoricoCodigo], [HistoricoConsumo_Data], [HistoricoConsumo_SaldoContratoCod], [HistoricoConsumo_SaldoContratoDtaVigIni], [HistoricoConsumo_SaldoContratoDtaVigFim], [HistoricoConsumo_NotaEmpenhoCod], [HistoricoConsumo_ContagemResultadoCod], [HistoricoConsumo_ContratoCod], [HistoricoConsumo_Valor], [HistoricoConsumo_AutorizacaoConsumoCod], [HistoricoConsumo_UsuarioCod] AS HistoricoConsumo_UsuarioCod FROM [HistoricoConsumo] WITH (UPDLOCK) WHERE [HistoricoConsumo_Codigo] = @HistoricoConsumo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00402,1,0,true,false )
             ,new CursorDef("T00403", "SELECT [HistoricoConsumo_Codigo], [HistoricoConsumo_HistoricoCodigo], [HistoricoConsumo_Data], [HistoricoConsumo_SaldoContratoCod], [HistoricoConsumo_SaldoContratoDtaVigIni], [HistoricoConsumo_SaldoContratoDtaVigFim], [HistoricoConsumo_NotaEmpenhoCod], [HistoricoConsumo_ContagemResultadoCod], [HistoricoConsumo_ContratoCod], [HistoricoConsumo_Valor], [HistoricoConsumo_AutorizacaoConsumoCod], [HistoricoConsumo_UsuarioCod] AS HistoricoConsumo_UsuarioCod FROM [HistoricoConsumo] WITH (NOLOCK) WHERE [HistoricoConsumo_Codigo] = @HistoricoConsumo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00403,1,0,true,false )
             ,new CursorDef("T00404", "SELECT [Usuario_Codigo] AS HistoricoConsumo_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @HistoricoConsumo_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00404,1,0,true,false )
             ,new CursorDef("T00405", "SELECT TM1.[HistoricoConsumo_Codigo], TM1.[HistoricoConsumo_HistoricoCodigo], TM1.[HistoricoConsumo_Data], TM1.[HistoricoConsumo_SaldoContratoCod], TM1.[HistoricoConsumo_SaldoContratoDtaVigIni], TM1.[HistoricoConsumo_SaldoContratoDtaVigFim], TM1.[HistoricoConsumo_NotaEmpenhoCod], TM1.[HistoricoConsumo_ContagemResultadoCod], TM1.[HistoricoConsumo_ContratoCod], TM1.[HistoricoConsumo_Valor], TM1.[HistoricoConsumo_AutorizacaoConsumoCod], TM1.[HistoricoConsumo_UsuarioCod] AS HistoricoConsumo_UsuarioCod FROM [HistoricoConsumo] TM1 WITH (NOLOCK) WHERE TM1.[HistoricoConsumo_Codigo] = @HistoricoConsumo_Codigo ORDER BY TM1.[HistoricoConsumo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00405,100,0,true,false )
             ,new CursorDef("T00406", "SELECT [Usuario_Codigo] AS HistoricoConsumo_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @HistoricoConsumo_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00406,1,0,true,false )
             ,new CursorDef("T00407", "SELECT [HistoricoConsumo_Codigo] FROM [HistoricoConsumo] WITH (NOLOCK) WHERE [HistoricoConsumo_Codigo] = @HistoricoConsumo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00407,1,0,true,false )
             ,new CursorDef("T00408", "SELECT TOP 1 [HistoricoConsumo_Codigo] FROM [HistoricoConsumo] WITH (NOLOCK) WHERE ( [HistoricoConsumo_Codigo] > @HistoricoConsumo_Codigo) ORDER BY [HistoricoConsumo_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00408,1,0,true,true )
             ,new CursorDef("T00409", "SELECT TOP 1 [HistoricoConsumo_Codigo] FROM [HistoricoConsumo] WITH (NOLOCK) WHERE ( [HistoricoConsumo_Codigo] < @HistoricoConsumo_Codigo) ORDER BY [HistoricoConsumo_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00409,1,0,true,true )
             ,new CursorDef("T004010", "INSERT INTO [HistoricoConsumo]([HistoricoConsumo_HistoricoCodigo], [HistoricoConsumo_Data], [HistoricoConsumo_SaldoContratoCod], [HistoricoConsumo_SaldoContratoDtaVigIni], [HistoricoConsumo_SaldoContratoDtaVigFim], [HistoricoConsumo_NotaEmpenhoCod], [HistoricoConsumo_ContagemResultadoCod], [HistoricoConsumo_ContratoCod], [HistoricoConsumo_Valor], [HistoricoConsumo_AutorizacaoConsumoCod], [HistoricoConsumo_UsuarioCod]) VALUES(@HistoricoConsumo_HistoricoCodigo, @HistoricoConsumo_Data, @HistoricoConsumo_SaldoContratoCod, @HistoricoConsumo_SaldoContratoDtaVigIni, @HistoricoConsumo_SaldoContratoDtaVigFim, @HistoricoConsumo_NotaEmpenhoCod, @HistoricoConsumo_ContagemResultadoCod, @HistoricoConsumo_ContratoCod, @HistoricoConsumo_Valor, @HistoricoConsumo_AutorizacaoConsumoCod, @HistoricoConsumo_UsuarioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004010)
             ,new CursorDef("T004011", "UPDATE [HistoricoConsumo] SET [HistoricoConsumo_HistoricoCodigo]=@HistoricoConsumo_HistoricoCodigo, [HistoricoConsumo_Data]=@HistoricoConsumo_Data, [HistoricoConsumo_SaldoContratoCod]=@HistoricoConsumo_SaldoContratoCod, [HistoricoConsumo_SaldoContratoDtaVigIni]=@HistoricoConsumo_SaldoContratoDtaVigIni, [HistoricoConsumo_SaldoContratoDtaVigFim]=@HistoricoConsumo_SaldoContratoDtaVigFim, [HistoricoConsumo_NotaEmpenhoCod]=@HistoricoConsumo_NotaEmpenhoCod, [HistoricoConsumo_ContagemResultadoCod]=@HistoricoConsumo_ContagemResultadoCod, [HistoricoConsumo_ContratoCod]=@HistoricoConsumo_ContratoCod, [HistoricoConsumo_Valor]=@HistoricoConsumo_Valor, [HistoricoConsumo_AutorizacaoConsumoCod]=@HistoricoConsumo_AutorizacaoConsumoCod, [HistoricoConsumo_UsuarioCod]=@HistoricoConsumo_UsuarioCod  WHERE [HistoricoConsumo_Codigo] = @HistoricoConsumo_Codigo", GxErrorMask.GX_NOMASK,prmT004011)
             ,new CursorDef("T004012", "DELETE FROM [HistoricoConsumo]  WHERE [HistoricoConsumo_Codigo] = @HistoricoConsumo_Codigo", GxErrorMask.GX_NOMASK,prmT004012)
             ,new CursorDef("T004013", "SELECT [HistoricoConsumo_Codigo] FROM [HistoricoConsumo] WITH (NOLOCK) ORDER BY [HistoricoConsumo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004013,100,0,true,false )
             ,new CursorDef("T004014", "SELECT [Usuario_Codigo] AS HistoricoConsumo_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @HistoricoConsumo_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004014,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(10) ;
                ((int[]) buf[13])[0] = rslt.getInt(11) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((int[]) buf[15])[0] = rslt.getInt(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(10) ;
                ((int[]) buf[13])[0] = rslt.getInt(11) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((int[]) buf[15])[0] = rslt.getInt(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(10) ;
                ((int[]) buf[13])[0] = rslt.getInt(11) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((int[]) buf[15])[0] = rslt.getInt(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (DateTime)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[10]);
                }
                stmt.SetParameter(9, (decimal)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[15]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (DateTime)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[10]);
                }
                stmt.SetParameter(9, (decimal)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[15]);
                }
                stmt.SetParameter(12, (int)parms[16]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
