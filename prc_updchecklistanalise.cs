/*
               File: PRC_UpdCheckListAnalise
        Description: Upd Check List Analise
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:29.51
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updchecklistanalise : GXProcedure
   {
      public prc_updchecklistanalise( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updchecklistanalise( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_User_Codigo )
      {
         this.AV9ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV18User_Codigo = aP1_User_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 int aP1_User_Codigo )
      {
         prc_updchecklistanalise objprc_updchecklistanalise;
         objprc_updchecklistanalise = new prc_updchecklistanalise();
         objprc_updchecklistanalise.AV9ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_updchecklistanalise.AV18User_Codigo = aP1_User_Codigo;
         objprc_updchecklistanalise.context.SetSubmitInitialConfig(context);
         objprc_updchecklistanalise.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updchecklistanalise);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updchecklistanalise)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV15SDT_CheckList.FromXml(AV19WebSession.Get("CheckList"), "");
         AV19WebSession.Remove("CheckList");
         AV8ChckLst_Completo = true;
         AV24GXV1 = 1;
         while ( AV24GXV1 <= AV15SDT_CheckList.Count )
         {
            AV16SDT_Item = ((SdtSDT_CheckList_Item)AV15SDT_CheckList.Item(AV24GXV1));
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV16SDT_Item.gxTpr_Cumpre)) || ( StringUtil.StrCmp(AV16SDT_Item.gxTpr_Cumpre, "N") == 0 ) )
            {
               AV8ChckLst_Completo = false;
               if ( StringUtil.StrCmp(AV16SDT_Item.gxTpr_Cumpre, "N") == 0 )
               {
               }
            }
            /* Using cursor P00832 */
            pr_default.execute(0, new Object[] {AV9ContagemResultado_Codigo, AV16SDT_Item.gxTpr_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A758CheckList_Codigo = P00832_A758CheckList_Codigo[0];
               A1868ContagemResultadoChckLst_OSCod = P00832_A1868ContagemResultadoChckLst_OSCod[0];
               A762ContagemResultadoChckLst_Cumpre = P00832_A762ContagemResultadoChckLst_Cumpre[0];
               A761ContagemResultadoChckLst_Codigo = P00832_A761ContagemResultadoChckLst_Codigo[0];
               A762ContagemResultadoChckLst_Cumpre = AV16SDT_Item.gxTpr_Cumpre;
               BatchSize = 50;
               pr_default.initializeBatch( 1, BatchSize, this, "Executebatchp00833");
               /* Using cursor P00833 */
               pr_default.addRecord(1, new Object[] {A762ContagemResultadoChckLst_Cumpre, A761ContagemResultadoChckLst_Codigo});
               if ( pr_default.recordCount(1) == pr_default.getBatchSize(1) )
               {
                  Executebatchp00833( ) ;
               }
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLst") ;
               pr_default.readNext(0);
            }
            if ( pr_default.getBatchSize(1) > 0 )
            {
               Executebatchp00833( ) ;
            }
            pr_default.close(0);
            AV24GXV1 = (int)(AV24GXV1+1);
         }
         if ( AV8ChckLst_Completo )
         {
            /* Using cursor P00834 */
            pr_default.execute(2, new Object[] {AV9ContagemResultado_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A456ContagemResultado_Codigo = P00834_A456ContagemResultado_Codigo[0];
               A1351ContagemResultado_DataPrevista = P00834_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P00834_n1351ContagemResultado_DataPrevista[0];
               A472ContagemResultado_DataEntrega = P00834_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P00834_n472ContagemResultado_DataEntrega[0];
               A484ContagemResultado_StatusDmn = P00834_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00834_n484ContagemResultado_StatusDmn[0];
               AV14PrazoEntrega = A1351ContagemResultado_DataPrevista;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(A1351ContagemResultado_DataPrevista);
               n472ContagemResultado_DataEntrega = false;
               A484ContagemResultado_StatusDmn = "A";
               n484ContagemResultado_StatusDmn = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00835 */
               pr_default.execute(3, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P00836 */
               pr_default.execute(4, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            new prc_setfimanl(context ).execute( ref  AV9ContagemResultado_Codigo) ;
            new prc_inslogresponsavel(context ).execute( ref  AV9ContagemResultado_Codigo,  0,  "F",  "D",  AV18User_Codigo,  0,  "E",  "A",  "Check list do an�lise.",  AV14PrazoEntrega,  true) ;
            new prc_disparoservicovinculado(context ).execute(  AV9ContagemResultado_Codigo,  AV18User_Codigo) ;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'REGISTRALOG' Routine */
      }

      protected void Executebatchp00833( )
      {
         /* Using cursor P00833 */
         pr_default.executeBatch(1);
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         AV15SDT_CheckList = new GxObjectCollection( context, "SDT_CheckList.Item", "GxEv3Up14_MeetrikaVs3", "SdtSDT_CheckList_Item", "GeneXus.Programs");
         AV19WebSession = context.GetSession();
         AV16SDT_Item = new SdtSDT_CheckList_Item(context);
         scmdbuf = "";
         P00832_A758CheckList_Codigo = new int[1] ;
         P00832_A1868ContagemResultadoChckLst_OSCod = new int[1] ;
         P00832_A762ContagemResultadoChckLst_Cumpre = new String[] {""} ;
         P00832_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         A762ContagemResultadoChckLst_Cumpre = "";
         P00833_A762ContagemResultadoChckLst_Cumpre = new String[] {""} ;
         P00833_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         P00834_A456ContagemResultado_Codigo = new int[1] ;
         P00834_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00834_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00834_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00834_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00834_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00834_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         AV14PrazoEntrega = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updchecklistanalise__default(),
            new Object[][] {
                new Object[] {
               P00832_A758CheckList_Codigo, P00832_A1868ContagemResultadoChckLst_OSCod, P00832_A762ContagemResultadoChckLst_Cumpre, P00832_A761ContagemResultadoChckLst_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P00834_A456ContagemResultado_Codigo, P00834_A1351ContagemResultado_DataPrevista, P00834_n1351ContagemResultado_DataPrevista, P00834_A472ContagemResultado_DataEntrega, P00834_n472ContagemResultado_DataEntrega, P00834_A484ContagemResultado_StatusDmn, P00834_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A761ContagemResultadoChckLst_Codigo ;
      private int AV9ContagemResultado_Codigo ;
      private int AV18User_Codigo ;
      private int AV24GXV1 ;
      private int A758CheckList_Codigo ;
      private int A1868ContagemResultadoChckLst_OSCod ;
      private int BatchSize ;
      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private String A762ContagemResultadoChckLst_Cumpre ;
      private String A484ContagemResultado_StatusDmn ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime AV14PrazoEntrega ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool AV8ChckLst_Completo ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n484ContagemResultado_StatusDmn ;
      private IGxSession AV19WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00832_A758CheckList_Codigo ;
      private int[] P00832_A1868ContagemResultadoChckLst_OSCod ;
      private String[] P00832_A762ContagemResultadoChckLst_Cumpre ;
      private short[] P00832_A761ContagemResultadoChckLst_Codigo ;
      private String[] P00833_A762ContagemResultadoChckLst_Cumpre ;
      private short[] P00833_A761ContagemResultadoChckLst_Codigo ;
      private int[] P00834_A456ContagemResultado_Codigo ;
      private DateTime[] P00834_A1351ContagemResultado_DataPrevista ;
      private bool[] P00834_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00834_A472ContagemResultado_DataEntrega ;
      private bool[] P00834_n472ContagemResultado_DataEntrega ;
      private String[] P00834_A484ContagemResultado_StatusDmn ;
      private bool[] P00834_n484ContagemResultado_StatusDmn ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CheckList_Item ))]
      private IGxCollection AV15SDT_CheckList ;
      private SdtSDT_CheckList_Item AV16SDT_Item ;
   }

   public class prc_updchecklistanalise__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new BatchUpdateCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00832 ;
          prmP00832 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16SDT_Item__Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00833 ;
          prmP00833 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_Cumpre",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultadoChckLst_Codigo",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00834 ;
          prmP00834 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00835 ;
          prmP00835 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00836 ;
          prmP00836 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00832", "SELECT [CheckList_Codigo], [ContagemResultadoChckLst_OSCod], [ContagemResultadoChckLst_Cumpre], [ContagemResultadoChckLst_Codigo] FROM [ContagemResultadoChckLst] WITH (UPDLOCK) WHERE ([ContagemResultadoChckLst_OSCod] = @AV9ContagemResultado_Codigo) AND ([CheckList_Codigo] = @AV16SDT_Item__Codigo) ORDER BY [ContagemResultadoChckLst_OSCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00832,1,0,true,false )
             ,new CursorDef("P00833", "UPDATE [ContagemResultadoChckLst] SET [ContagemResultadoChckLst_Cumpre]=@ContagemResultadoChckLst_Cumpre  WHERE [ContagemResultadoChckLst_Codigo] = @ContagemResultadoChckLst_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00833)
             ,new CursorDef("P00834", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataPrevista], [ContagemResultado_DataEntrega], [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00834,1,0,true,true )
             ,new CursorDef("P00835", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00835)
             ,new CursorDef("P00836", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00836)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
       }
    }

 }

}
