/*
               File: WP_CheckList
        Description:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:22:1.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_checklist : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_checklist( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_checklist( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           short aP1_Etapa )
      {
         this.AV7ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV22Etapa = aP1_Etapa;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavCtlcumpre = new GXCombobox();
         dynavContagemresultado_naocnfdmncod = new GXCombobox();
         cmbavCtlcumpre2 = new GXCombobox();
         dynavContagemresultado_naocnfcntcod = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV14WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_NAOCNFDMNCODEG2( AV14WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_NAOCNFCNTCOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV14WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_NAOCNFCNTCODEG2( AV14WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_19 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_19_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_19_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               AV22Etapa = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Etapa", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Etapa), 4, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV5SDT_CheckList1);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV15SDT_CheckList2);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( AV22Etapa, AV5SDT_CheckList1, AV15SDT_CheckList2) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid2") == 0 )
            {
               nRC_GXsfl_41 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_41_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_41_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid2_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid2") == 0 )
            {
               AV22Etapa = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Etapa", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Etapa), 4, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV5SDT_CheckList1);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV15SDT_CheckList2);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid2_refresh( AV22Etapa, AV5SDT_CheckList1, AV15SDT_CheckList2) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV22Etapa = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Etapa", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Etapa), 4, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAEG2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTEG2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020621622194");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_checklist.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo) + "," + UrlEncode("" +AV22Etapa)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_checklist2", AV15SDT_CheckList2);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_checklist2", AV15SDT_CheckList2);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_checklist1", AV5SDT_CheckList1);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_checklist1", AV5SDT_CheckList1);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_19", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_19), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_41", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_41), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vETAPA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Etapa), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CHECKLIST1", AV5SDT_CheckList1);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CHECKLIST1", AV5SDT_CheckList1);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CHECKLIST2", AV15SDT_CheckList2);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CHECKLIST2", AV15SDT_CheckList2);
         }
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCALLER", StringUtil.RTrim( AV30Caller));
         GxWebStd.gx_boolean_hidden_field( context, "vREFRESH", AV31Refresh);
         GxWebStd.gx_boolean_hidden_field( context, "vOKFLAG", AV16OkFlag);
         GxWebStd.gx_hidden_field( context, "vGXV5", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46GXV5), 8, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ITEM", AV6SDT_Item);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ITEM", AV6SDT_Item);
         }
         GxWebStd.gx_hidden_field( context, "vGXV6", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47GXV6), 8, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV14WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV14WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEEG2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTEG2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_checklist.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo) + "," + UrlEncode("" +AV22Etapa) ;
      }

      public override String GetPgmname( )
      {
         return "WP_CheckList" ;
      }

      public override String GetPgmdesc( )
      {
         return "" ;
      }

      protected void WBEG0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "<p>") ;
            wb_table1_4_EG2( true) ;
         }
         else
         {
            wb_table1_4_EG2( false) ;
         }
         return  ;
      }

      protected void wb_table1_4_EG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_CheckList.htm");
         }
         wbLoad = true;
      }

      protected void STARTEG2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPEG0( ) ;
      }

      protected void WSEG2( )
      {
         STARTEG2( ) ;
         EVTEG2( ) ;
      }

      protected void EVTEG2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E11EG2 */
                                    E11EG2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "CTLCUMPRE2.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID2.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "CTLCUMPRE2.CLICK") == 0 ) )
                           {
                              nGXsfl_41_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_41_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_41_idx), 4, 0)), 4, "0");
                              SubsflControlProps_418( ) ;
                              AV38GXV2 = nGXsfl_41_idx;
                              if ( ( AV15SDT_CheckList2.Count >= AV38GXV2 ) && ( AV38GXV2 > 0 ) )
                              {
                                 AV15SDT_CheckList2.CurrentItem = ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "CTLCUMPRE2.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12EG2 */
                                    E12EG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID2.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13EG8 */
                                    E13EG8 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "CTLCUMPRE.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "CTLCUMPRE.CLICK") == 0 ) )
                           {
                              nGXsfl_19_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
                              SubsflControlProps_192( ) ;
                              AV34GXV1 = nGXsfl_19_idx;
                              if ( ( AV5SDT_CheckList1.Count >= AV34GXV1 ) && ( AV34GXV1 > 0 ) )
                              {
                                 AV5SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14EG2 */
                                    E14EG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15EG2 */
                                    E15EG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CTLCUMPRE.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E16EG2 */
                                    E16EG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID1.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E17EG2 */
                                    E17EG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEG2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAEG2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            GXCCtl = "CTLCUMPRE_" + sGXsfl_19_idx;
            cmbavCtlcumpre.Name = GXCCtl;
            cmbavCtlcumpre.WebTags = "";
            cmbavCtlcumpre.addItem("", "---", 0);
            cmbavCtlcumpre.addItem("S", "Sim", 0);
            cmbavCtlcumpre.addItem("N", "N�o", 0);
            cmbavCtlcumpre.addItem("X", "N/A", 0);
            if ( cmbavCtlcumpre.ItemCount > 0 )
            {
               if ( ( AV34GXV1 > 0 ) && ( AV5SDT_CheckList1.Count >= AV34GXV1 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Cumpre)) )
               {
                  ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Cumpre = cmbavCtlcumpre.getValidValue(((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Cumpre);
               }
            }
            dynavContagemresultado_naocnfdmncod.Name = "vCONTAGEMRESULTADO_NAOCNFDMNCOD";
            dynavContagemresultado_naocnfdmncod.WebTags = "";
            GXCCtl = "CTLCUMPRE2_" + sGXsfl_41_idx;
            cmbavCtlcumpre2.Name = GXCCtl;
            cmbavCtlcumpre2.WebTags = "";
            cmbavCtlcumpre2.addItem("", "---", 0);
            cmbavCtlcumpre2.addItem("S", "Sim", 0);
            cmbavCtlcumpre2.addItem("N", "N�o", 0);
            cmbavCtlcumpre2.addItem("X", "N/A", 0);
            if ( cmbavCtlcumpre2.ItemCount > 0 )
            {
               if ( ( AV38GXV2 > 0 ) && ( AV15SDT_CheckList2.Count >= AV38GXV2 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Cumpre)) )
               {
                  ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Cumpre = cmbavCtlcumpre2.getValidValue(((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Cumpre);
               }
            }
            dynavContagemresultado_naocnfcntcod.Name = "vCONTAGEMRESULTADO_NAOCNFCNTCOD";
            dynavContagemresultado_naocnfcntcod.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContagemresultado_naocnfdmncod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTAGEMRESULTADO_NAOCNFDMNCODEG2( wwpbaseobjects.SdtWWPContext AV14WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_NAOCNFDMNCOD_dataEG2( AV14WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_NAOCNFDMNCOD_htmlEG2( wwpbaseobjects.SdtWWPContext AV14WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_NAOCNFDMNCOD_dataEG2( AV14WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_naocnfdmncod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_naocnfdmncod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_naocnfdmncod.ItemCount > 0 )
         {
            AV9ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( dynavContagemresultado_naocnfdmncod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_NaoCnfDmnCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_NaoCnfDmnCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_NAOCNFDMNCOD_dataEG2( wwpbaseobjects.SdtWWPContext AV14WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00EG2 */
         pr_default.execute(0, new Object[] {AV14WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00EG2_A426NaoConformidade_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00EG2_A427NaoConformidade_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTAGEMRESULTADO_NAOCNFCNTCODEG2( wwpbaseobjects.SdtWWPContext AV14WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_NAOCNFCNTCOD_dataEG2( AV14WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_NAOCNFCNTCOD_htmlEG2( wwpbaseobjects.SdtWWPContext AV14WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_NAOCNFCNTCOD_dataEG2( AV14WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_naocnfcntcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_naocnfcntcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_naocnfcntcod.ItemCount > 0 )
         {
            AV10ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( dynavContagemresultado_naocnfcntcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10ContagemResultado_NaoCnfCntCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ContagemResultado_NaoCnfCntCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_NAOCNFCNTCOD_dataEG2( wwpbaseobjects.SdtWWPContext AV14WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00EG3 */
         pr_default.execute(1, new Object[] {AV14WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00EG3_A426NaoConformidade_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00EG3_A427NaoConformidade_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_192( ) ;
         while ( nGXsfl_19_idx <= nRC_GXsfl_19 )
         {
            sendrow_192( ) ;
            nGXsfl_19_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
            sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
            SubsflControlProps_192( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxnrGrid2_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_418( ) ;
         while ( nGXsfl_41_idx <= nRC_GXsfl_41 )
         {
            sendrow_418( ) ;
            nGXsfl_41_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_41_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_41_idx+1));
            sGXsfl_41_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_41_idx), 4, 0)), 4, "0");
            SubsflControlProps_418( ) ;
         }
         context.GX_webresponse.AddString(Grid2Container.ToJavascriptSource());
         /* End function gxnrGrid2_newrow */
      }

      protected void gxgrGrid1_refresh( short AV22Etapa ,
                                        IGxCollection AV5SDT_CheckList1 ,
                                        IGxCollection AV15SDT_CheckList2 )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RFEG2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void gxgrGrid2_refresh( short AV22Etapa ,
                                        IGxCollection AV5SDT_CheckList1 ,
                                        IGxCollection AV15SDT_CheckList2 )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         /* Execute user event: E15EG2 */
         E15EG2 ();
         GRID2_nCurrentRecord = 0;
         RFEG8( ) ;
         /* End function gxgrGrid2_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContagemresultado_naocnfdmncod.ItemCount > 0 )
         {
            AV9ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( dynavContagemresultado_naocnfdmncod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_NaoCnfDmnCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_NaoCnfDmnCod), 6, 0)));
         }
         if ( dynavContagemresultado_naocnfcntcod.ItemCount > 0 )
         {
            AV10ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( dynavContagemresultado_naocnfcntcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10ContagemResultado_NaoCnfCntCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ContagemResultado_NaoCnfCntCod), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEG2( ) ;
         RFEG8( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcodigo_Enabled), 5, 0)));
         edtavCtldescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldescricao_Enabled), 5, 0)));
         edtavCtlcodigo2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcodigo2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcodigo2_Enabled), 5, 0)));
         edtavCtldescricao2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldescricao2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldescricao2_Enabled), 5, 0)));
      }

      protected void RFEG2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 19;
         /* Execute user event: E15EG2 */
         E15EG2 ();
         nGXsfl_19_idx = 1;
         sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
         SubsflControlProps_192( ) ;
         nGXsfl_19_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "Grid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Titleforecolor), 9, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_192( ) ;
            /* Execute user event: E17EG2 */
            E17EG2 ();
            wbEnd = 19;
            WBEG0( ) ;
         }
         nGXsfl_19_Refreshing = 0;
      }

      protected void RFEG8( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid2Container.ClearRows();
         }
         wbStart = 41;
         nGXsfl_41_idx = 1;
         sGXsfl_41_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_41_idx), 4, 0)), 4, "0");
         SubsflControlProps_418( ) ;
         nGXsfl_41_Refreshing = 1;
         Grid2Container.AddObjectProperty("GridName", "Grid2");
         Grid2Container.AddObjectProperty("CmpContext", "");
         Grid2Container.AddObjectProperty("InMasterPage", "false");
         Grid2Container.AddObjectProperty("Class", "Grid");
         Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Backcolorstyle), 1, 0, ".", "")));
         Grid2Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Titleforecolor), 9, 0, ".", "")));
         Grid2Container.PageSize = subGrid2_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_418( ) ;
            /* Execute user event: E13EG8 */
            E13EG8 ();
            wbEnd = 41;
            WBEG0( ) ;
         }
         nGXsfl_41_Refreshing = 0;
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPEG0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcodigo_Enabled), 5, 0)));
         edtavCtldescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldescricao_Enabled), 5, 0)));
         edtavCtlcodigo2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcodigo2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcodigo2_Enabled), 5, 0)));
         edtavCtldescricao2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldescricao2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldescricao2_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E14EG2 */
         E14EG2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTAGEMRESULTADO_NAOCNFDMNCOD_htmlEG2( AV14WWPContext) ;
         GXVvCONTAGEMRESULTADO_NAOCNFCNTCOD_htmlEG2( AV14WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_checklist2"), AV15SDT_CheckList2);
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_checklist1"), AV5SDT_CheckList1);
            ajax_req_read_hidden_sdt(cgiGet( "vSDT_CHECKLIST1"), AV5SDT_CheckList1);
            ajax_req_read_hidden_sdt(cgiGet( "vSDT_ITEM"), AV6SDT_Item);
            ajax_req_read_hidden_sdt(cgiGet( "vSDT_CHECKLIST2"), AV15SDT_CheckList2);
            /* Read variables values. */
            dynavContagemresultado_naocnfdmncod.Name = dynavContagemresultado_naocnfdmncod_Internalname;
            dynavContagemresultado_naocnfdmncod.CurrentValue = cgiGet( dynavContagemresultado_naocnfdmncod_Internalname);
            AV9ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_naocnfdmncod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_NaoCnfDmnCod), 6, 0)));
            dynavContagemresultado_naocnfcntcod.Name = dynavContagemresultado_naocnfcntcod_Internalname;
            dynavContagemresultado_naocnfcntcod.CurrentValue = cgiGet( dynavContagemresultado_naocnfcntcod_Internalname);
            AV10ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_naocnfcntcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ContagemResultado_NaoCnfCntCod), 6, 0)));
            /* Read saved values. */
            nRC_GXsfl_19 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_19"), ",", "."));
            nRC_GXsfl_41 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_41"), ",", "."));
            AV31Refresh = StringUtil.StrToBool( cgiGet( "vREFRESH"));
            AV22Etapa = (short)(context.localUtil.CToN( cgiGet( "vETAPA"), ",", "."));
            AV16OkFlag = StringUtil.StrToBool( cgiGet( "vOKFLAG"));
            AV46GXV5 = (int)(context.localUtil.CToN( cgiGet( "vGXV5"), ",", "."));
            AV47GXV6 = (int)(context.localUtil.CToN( cgiGet( "vGXV6"), ",", "."));
            nRC_GXsfl_19 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_19"), ",", "."));
            nGXsfl_19_fel_idx = 0;
            while ( nGXsfl_19_fel_idx < nRC_GXsfl_19 )
            {
               nGXsfl_19_fel_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_19_fel_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_19_fel_idx+1));
               sGXsfl_19_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_192( ) ;
               AV34GXV1 = nGXsfl_19_fel_idx;
               if ( ( AV5SDT_CheckList1.Count >= AV34GXV1 ) && ( AV34GXV1 > 0 ) )
               {
                  AV5SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1));
               }
            }
            if ( nGXsfl_19_fel_idx == 0 )
            {
               nGXsfl_19_idx = 1;
               sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
               SubsflControlProps_192( ) ;
            }
            nGXsfl_19_fel_idx = 1;
            nRC_GXsfl_41 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_41"), ",", "."));
            nGXsfl_41_fel_idx = 0;
            while ( nGXsfl_41_fel_idx < nRC_GXsfl_41 )
            {
               nGXsfl_41_fel_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_41_fel_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_41_fel_idx+1));
               sGXsfl_41_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_41_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_418( ) ;
               AV38GXV2 = nGXsfl_41_fel_idx;
               if ( ( AV15SDT_CheckList2.Count >= AV38GXV2 ) && ( AV38GXV2 > 0 ) )
               {
                  AV15SDT_CheckList2.CurrentItem = ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2));
               }
            }
            if ( nGXsfl_41_fel_idx == 0 )
            {
               nGXsfl_41_idx = 1;
               sGXsfl_41_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_41_idx), 4, 0)), 4, "0");
               SubsflControlProps_418( ) ;
            }
            nGXsfl_41_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E14EG2 */
         E14EG2 ();
         if (returnInSub) return;
      }

      protected void E14EG2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV14WWPContext) ;
         /* Using cursor H00EG4 */
         pr_default.execute(2, new Object[] {AV7ContagemResultado_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00EG4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00EG4_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = H00EG4_A456ContagemResultado_Codigo[0];
            A428NaoConformidade_AreaTrabalhoCod = H00EG4_A428NaoConformidade_AreaTrabalhoCod[0];
            n428NaoConformidade_AreaTrabalhoCod = H00EG4_n428NaoConformidade_AreaTrabalhoCod[0];
            A429NaoConformidade_Tipo = H00EG4_A429NaoConformidade_Tipo[0];
            n429NaoConformidade_Tipo = H00EG4_n429NaoConformidade_Tipo[0];
            A602ContagemResultado_OSVinculada = H00EG4_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00EG4_n602ContagemResultado_OSVinculada[0];
            A490ContagemResultado_ContratadaCod = H00EG4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EG4_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = H00EG4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00EG4_n601ContagemResultado_Servico[0];
            A771ContagemResultado_StatusDmnVnc = H00EG4_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = H00EG4_n771ContagemResultado_StatusDmnVnc[0];
            A468ContagemResultado_NaoCnfDmnCod = H00EG4_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = H00EG4_n468ContagemResultado_NaoCnfDmnCod[0];
            A457ContagemResultado_Demanda = H00EG4_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00EG4_n457ContagemResultado_Demanda[0];
            A601ContagemResultado_Servico = H00EG4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00EG4_n601ContagemResultado_Servico[0];
            A771ContagemResultado_StatusDmnVnc = H00EG4_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = H00EG4_n771ContagemResultado_StatusDmnVnc[0];
            A428NaoConformidade_AreaTrabalhoCod = H00EG4_A428NaoConformidade_AreaTrabalhoCod[0];
            n428NaoConformidade_AreaTrabalhoCod = H00EG4_n428NaoConformidade_AreaTrabalhoCod[0];
            A429NaoConformidade_Tipo = H00EG4_A429NaoConformidade_Tipo[0];
            n429NaoConformidade_Tipo = H00EG4_n429NaoConformidade_Tipo[0];
            AV12ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContagemResultado_OSVinculada), 6, 0)));
            AV20Contratada_Codigo = A490ContagemResultado_ContratadaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Contratada_Codigo), 6, 0)));
            AV17Servico_Codigo = A601ContagemResultado_Servico;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0)));
            AV13StatusDmnPai = A771ContagemResultado_StatusDmnVnc;
            AV9ContagemResultado_NaoCnfDmnCod = A468ContagemResultado_NaoCnfDmnCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_NaoCnfDmnCod), 6, 0)));
            lblSubtitle_Caption = "CHECK LIST DA DEMANDA "+A457ContagemResultado_Demanda;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSubtitle_Internalname, "Caption", lblSubtitle_Caption);
            /* Using cursor H00EG5 */
            pr_default.execute(3, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A517ContagemResultado_Ultima = H00EG5_A517ContagemResultado_Ultima[0];
               A469ContagemResultado_NaoCnfCntCod = H00EG5_A469ContagemResultado_NaoCnfCntCod[0];
               n469ContagemResultado_NaoCnfCntCod = H00EG5_n469ContagemResultado_NaoCnfCntCod[0];
               AV10ContagemResultado_NaoCnfCntCod = A469ContagemResultado_NaoCnfCntCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ContagemResultado_NaoCnfCntCod), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(3);
            }
            pr_default.close(3);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         AV16OkFlag = true;
         bttBtnenter_Visible = (((AV22Etapa>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         if ( AV22Etapa == 0 )
         {
            bttBtnfechar_Jsonclick = "self.close()";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnfechar_Internalname, "Jsonclick", bttBtnfechar_Jsonclick);
         }
         AV30Caller = AV11WebSession.Get("Caller");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Caller", AV30Caller);
         if ( ( StringUtil.StrCmp(AV30Caller, "DmnSrv") == 0 ) || ( StringUtil.StrCmp(AV30Caller, "Mon") == 0 ) )
         {
            bttBtnfechar_Jsonclick = "self.close()";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnfechar_Internalname, "Jsonclick", bttBtnfechar_Jsonclick);
         }
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         bttBtnenter_Visible = (AV14WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         /* Execute user subroutine: 'CARREGASDT' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E15EG2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         dynavContagemresultado_naocnfdmncod.Enabled = (((AV22Etapa==1)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_naocnfdmncod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_naocnfdmncod.Enabled), 5, 0)));
         dynavContagemresultado_naocnfcntcod.Enabled = (((AV22Etapa==2)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_naocnfcntcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_naocnfcntcod.Enabled), 5, 0)));
         /* Execute user subroutine: 'BTNENTERCAPTION' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6SDT_Item", AV6SDT_Item);
      }

      public void GXEnter( )
      {
         /* Execute user event: E11EG2 */
         E11EG2 ();
         if (returnInSub) return;
      }

      protected void E11EG2( )
      {
         AV38GXV2 = nGXsfl_41_idx;
         if ( AV15SDT_CheckList2.Count >= AV38GXV2 )
         {
            AV15SDT_CheckList2.CurrentItem = ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2));
         }
         AV34GXV1 = nGXsfl_19_idx;
         if ( AV5SDT_CheckList1.Count >= AV34GXV1 )
         {
            AV5SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1));
         }
         /* Enter Routine */
         AV24SDT_CheckList.Clear();
         AV44GXV3 = 1;
         while ( AV44GXV3 <= AV5SDT_CheckList1.Count )
         {
            AV6SDT_Item = ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV44GXV3));
            AV24SDT_CheckList.Add(AV6SDT_Item, 0);
            AV44GXV3 = (int)(AV44GXV3+1);
         }
         AV45GXV4 = 1;
         while ( AV45GXV4 <= AV15SDT_CheckList2.Count )
         {
            AV6SDT_Item = ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV45GXV4));
            AV24SDT_CheckList.Add(AV6SDT_Item, 0);
            AV45GXV4 = (int)(AV45GXV4+1);
         }
         AV11WebSession.Set("CheckList", AV24SDT_CheckList.ToXml(false, true, "SDT_CheckList", "GxEv3Up14_MeetrikaVs3"));
         new prc_updchecklist(context ).execute(  AV7ContagemResultado_Codigo,  AV12ContagemResultado_OSVinculada,  AV9ContagemResultado_NaoCnfDmnCod,  AV10ContagemResultado_NaoCnfCntCod,  AV14WWPContext.gxTpr_Userid,  AV20Contratada_Codigo,  AV17Servico_Codigo, ref  AV22Etapa) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContagemResultado_OSVinculada), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_NaoCnfDmnCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ContagemResultado_NaoCnfCntCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Etapa", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Etapa), 4, 0)));
         if ( ( StringUtil.StrCmp(AV30Caller, "DmnSrv") == 0 ) || ( StringUtil.StrCmp(AV30Caller, "Mon") == 0 ) )
         {
            if ( AV31Refresh )
            {
               lblTbjava_Caption = "<script language=\"text/javascript\"> opener.location.reload(true); self.close();</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            }
            else
            {
               lblTbjava_Caption = "<script language=\"text/javascript\"> self.close();</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            }
         }
         else
         {
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6SDT_Item", AV6SDT_Item);
      }

      protected void E16EG2( )
      {
         AV34GXV1 = nGXsfl_19_idx;
         if ( AV5SDT_CheckList1.Count >= AV34GXV1 )
         {
            AV5SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1));
         }
         AV38GXV2 = nGXsfl_41_idx;
         if ( AV15SDT_CheckList2.Count >= AV38GXV2 )
         {
            AV15SDT_CheckList2.CurrentItem = ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2));
         }
         /* Ctlcumpre_Click Routine */
         /* Execute user subroutine: 'BTNENTERCAPTION' */
         S122 ();
         if (returnInSub) return;
         if ( (0==AV9ContagemResultado_NaoCnfDmnCod) && ( StringUtil.StrCmp(((SdtSDT_CheckList_Item)(AV5SDT_CheckList1.CurrentItem)).gxTpr_Cumpre, "N") == 0 ) )
         {
            bttBtnenter_Tooltiptext = "Informe a N�o conformidade da Demanda!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
            bttBtnenter_Caption = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6SDT_Item", AV6SDT_Item);
      }

      protected void E12EG2( )
      {
         AV34GXV1 = nGXsfl_19_idx;
         if ( AV5SDT_CheckList1.Count >= AV34GXV1 )
         {
            AV5SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1));
         }
         AV38GXV2 = nGXsfl_41_idx;
         if ( AV15SDT_CheckList2.Count >= AV38GXV2 )
         {
            AV15SDT_CheckList2.CurrentItem = ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2));
         }
         /* Ctlcumpre2_Click Routine */
         /* Execute user subroutine: 'BTNENTERCAPTION' */
         S122 ();
         if (returnInSub) return;
         if ( (0==AV10ContagemResultado_NaoCnfCntCod) )
         {
            if ( StringUtil.StrCmp(((SdtSDT_CheckList_Item)(AV15SDT_CheckList2.CurrentItem)).gxTpr_Cumpre, "N") == 0 )
            {
               bttBtnenter_Tooltiptext = "Informe a N�o conformidade da Contagem!";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Tooltiptext", bttBtnenter_Tooltiptext);
               GX_msglist.addItem("Informe a N�o conformidade da Contagem!");
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6SDT_Item", AV6SDT_Item);
      }

      protected void S122( )
      {
         /* 'BTNENTERCAPTION' Routine */
         bttBtnenter_Caption = "Confirmar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         AV31Refresh = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Refresh", AV31Refresh);
         if ( AV22Etapa == 1 )
         {
            AV16OkFlag = true;
            AV46GXV5 = 1;
            while ( AV46GXV5 <= AV5SDT_CheckList1.Count )
            {
               AV6SDT_Item = ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV46GXV5));
               if ( ( StringUtil.StrCmp(AV6SDT_Item.gxTpr_Cumpre, "N") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( AV6SDT_Item.gxTpr_Cumpre)) )
               {
                  AV16OkFlag = false;
                  if (true) break;
               }
               AV46GXV5 = (int)(AV46GXV5+1);
            }
            if ( AV16OkFlag )
            {
               AV31Refresh = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Refresh", AV31Refresh);
               bttBtnenter_Caption = "Encaminhar para Execu��o";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
            }
         }
         else
         {
            AV16OkFlag = true;
            AV47GXV6 = 1;
            while ( AV47GXV6 <= AV15SDT_CheckList2.Count )
            {
               AV6SDT_Item = ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV47GXV6));
               if ( ( StringUtil.StrCmp(AV6SDT_Item.gxTpr_Cumpre, "N") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( AV6SDT_Item.gxTpr_Cumpre)) )
               {
                  AV16OkFlag = false;
                  if (true) break;
               }
               AV47GXV6 = (int)(AV47GXV6+1);
            }
            if ( AV16OkFlag )
            {
               bttBtnenter_Caption = "Demanda adequada";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
               AV31Refresh = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Refresh", AV31Refresh);
            }
         }
      }

      protected void S112( )
      {
         /* 'CARREGASDT' Routine */
         AV48GXLvl129 = 0;
         /* Using cursor H00EG6 */
         pr_default.execute(4, new Object[] {AV7ContagemResultado_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A1868ContagemResultadoChckLst_OSCod = H00EG6_A1868ContagemResultadoChckLst_OSCod[0];
            A758CheckList_Codigo = H00EG6_A758CheckList_Codigo[0];
            A763CheckList_Descricao = H00EG6_A763CheckList_Descricao[0];
            A762ContagemResultadoChckLst_Cumpre = H00EG6_A762ContagemResultadoChckLst_Cumpre[0];
            A763CheckList_Descricao = H00EG6_A763CheckList_Descricao[0];
            AV48GXLvl129 = 1;
            AV6SDT_Item.gxTpr_Codigo = A758CheckList_Codigo;
            AV6SDT_Item.gxTpr_Descricao = A763CheckList_Descricao;
            AV6SDT_Item.gxTpr_Cumpre = A762ContagemResultadoChckLst_Cumpre;
            if ( StringUtil.StrCmp(StringUtil.Substring( A763CheckList_Descricao, 1, 1), "1") == 0 )
            {
               cmbavCtlcumpre.Enabled = (((AV22Etapa==1)) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlcumpre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlcumpre.Enabled), 5, 0)));
               AV5SDT_CheckList1.Add(AV6SDT_Item, 0);
               gx_BV19 = true;
            }
            else
            {
               cmbavCtlcumpre2.Enabled = (((AV22Etapa==2)) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlcumpre2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlcumpre2.Enabled), 5, 0)));
               AV15SDT_CheckList2.Add(AV6SDT_Item, 0);
               gx_BV41 = true;
            }
            AV6SDT_Item = new SdtSDT_CheckList_Item(context);
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( AV48GXLvl129 == 0 )
         {
            /* Using cursor H00EG7 */
            pr_default.execute(5);
            while ( (pr_default.getStatus(5) != 101) )
            {
               A1151CheckList_Ativo = H00EG7_A1151CheckList_Ativo[0];
               A1230CheckList_De = H00EG7_A1230CheckList_De[0];
               n1230CheckList_De = H00EG7_n1230CheckList_De[0];
               A758CheckList_Codigo = H00EG7_A758CheckList_Codigo[0];
               A763CheckList_Descricao = H00EG7_A763CheckList_Descricao[0];
               AV6SDT_Item.gxTpr_Codigo = A758CheckList_Codigo;
               AV6SDT_Item.gxTpr_Descricao = A763CheckList_Descricao;
               if ( StringUtil.StrCmp(StringUtil.Substring( A763CheckList_Descricao, 1, 1), "1") == 0 )
               {
                  cmbavCtlcumpre.Enabled = (((AV22Etapa==1)) ? 1 : 0);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlcumpre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlcumpre.Enabled), 5, 0)));
                  AV5SDT_CheckList1.Add(AV6SDT_Item, 0);
                  gx_BV19 = true;
               }
               else
               {
                  cmbavCtlcumpre2.Enabled = (((AV22Etapa==2)) ? 1 : 0);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlcumpre2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlcumpre2.Enabled), 5, 0)));
                  AV15SDT_CheckList2.Add(AV6SDT_Item, 0);
                  gx_BV41 = true;
               }
               AV6SDT_Item = new SdtSDT_CheckList_Item(context);
               pr_default.readNext(5);
            }
            pr_default.close(5);
         }
         if ( AV15SDT_CheckList2.Count == 0 )
         {
            /* Using cursor H00EG8 */
            pr_default.execute(6);
            while ( (pr_default.getStatus(6) != 101) )
            {
               A763CheckList_Descricao = H00EG8_A763CheckList_Descricao[0];
               A1151CheckList_Ativo = H00EG8_A1151CheckList_Ativo[0];
               A1230CheckList_De = H00EG8_A1230CheckList_De[0];
               n1230CheckList_De = H00EG8_n1230CheckList_De[0];
               A758CheckList_Codigo = H00EG8_A758CheckList_Codigo[0];
               AV6SDT_Item.gxTpr_Codigo = A758CheckList_Codigo;
               AV6SDT_Item.gxTpr_Descricao = A763CheckList_Descricao;
               cmbavCtlcumpre2.Enabled = (((AV22Etapa==2)) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlcumpre2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlcumpre2.Enabled), 5, 0)));
               AV15SDT_CheckList2.Add(AV6SDT_Item, 0);
               gx_BV41 = true;
               AV6SDT_Item = new SdtSDT_CheckList_Item(context);
               pr_default.readNext(6);
            }
            pr_default.close(6);
         }
         AV5SDT_CheckList1.Sort("Descricao");
         gx_BV19 = true;
         AV15SDT_CheckList2.Sort("Descricao");
         gx_BV41 = true;
      }

      private void E17EG2( )
      {
         /* Grid1_Load Routine */
         AV34GXV1 = 1;
         while ( AV34GXV1 <= AV5SDT_CheckList1.Count )
         {
            AV5SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 19;
            }
            sendrow_192( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_19_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(19, Grid1Row);
            }
            AV34GXV1 = (short)(AV34GXV1+1);
         }
      }

      private void E13EG8( )
      {
         /* Grid2_Load Routine */
         AV38GXV2 = 1;
         while ( AV38GXV2 <= AV15SDT_CheckList2.Count )
         {
            AV15SDT_CheckList2.CurrentItem = ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 41;
            }
            sendrow_418( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_41_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(41, Grid2Row);
            }
            AV38GXV2 = (short)(AV38GXV2+1);
         }
      }

      protected void wb_table1_4_EG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(70), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\">") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSubtitle_Internalname, lblSubtitle_Caption, "", "", lblSubtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_13_EG2( true) ;
         }
         else
         {
            wb_table2_13_EG2( false) ;
         }
         return  ;
      }

      protected void wb_table2_13_EG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_35_EG2( true) ;
         }
         else
         {
            wb_table3_35_EG2( false) ;
         }
         return  ;
      }

      protected void wb_table3_35_EG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(19), 2, 0)+","+"null"+");", bttBtnenter_Caption, bttBtnenter_Jsonclick, 5, bttBtnenter_Tooltiptext, "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CheckList.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(19), 2, 0)+","+"null"+");", "Fechar", bttBtnfechar_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_4_EG2e( true) ;
         }
         else
         {
            wb_table1_4_EG2e( false) ;
         }
      }

      protected void wb_table3_35_EG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTbletapa2_Internalname, tblTbletapa2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "QUALIDADE DA CONTAGEM", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            Grid2Container.SetWrapped(nGXWrapped);
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid2Container"+"DivS\" data-gxgridid=\"41\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid2_Internalname, subGrid2_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid2_Backcolorstyle == 0 )
               {
                  subGrid2_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid2_Class) > 0 )
                  {
                     subGrid2_Linesclass = subGrid2_Class+"Title";
                  }
               }
               else
               {
                  subGrid2_Titlebackstyle = 1;
                  if ( subGrid2_Backcolorstyle == 1 )
                  {
                     subGrid2_Titlebackcolor = subGrid2_Allbackcolor;
                     if ( StringUtil.Len( subGrid2_Class) > 0 )
                     {
                        subGrid2_Linesclass = subGrid2_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid2_Class) > 0 )
                     {
                        subGrid2_Linesclass = subGrid2_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid2_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid2_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Descri��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(69), 4, 0))+"px"+" class=\""+subGrid2_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Adequa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid2Container.AddObjectProperty("GridName", "Grid2");
            }
            else
            {
               Grid2Container.AddObjectProperty("GridName", "Grid2");
               Grid2Container.AddObjectProperty("Class", "Grid");
               Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Backcolorstyle), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Titleforecolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("CmpContext", "");
               Grid2Container.AddObjectProperty("InMasterPage", "false");
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcodigo2_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtldescricao2_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavCtlcumpre2.Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowselection), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Selectioncolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowhovering), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Hoveringcolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowcollapsing), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 41 )
         {
            wbEnd = 0;
            nRC_GXsfl_41 = (short)(nGXsfl_41_idx-1);
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV38GXV2 = nGXsfl_41_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid2Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid2", Grid2Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid2ContainerData", Grid2Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid2ContainerData"+"V", Grid2Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid2ContainerData"+"V"+"\" value='"+Grid2Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "N�o onformidade da Contagem", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CheckList.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_19_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_naocnfcntcod, dynavContagemresultado_naocnfcntcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10ContagemResultado_NaoCnfCntCod), 6, 0)), 1, dynavContagemresultado_naocnfcntcod_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e18eg1_client"+"'", "int", "", 1, dynavContagemresultado_naocnfcntcod.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WP_CheckList.htm");
            dynavContagemresultado_naocnfcntcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10ContagemResultado_NaoCnfCntCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_naocnfcntcod_Internalname, "Values", (String)(dynavContagemresultado_naocnfcntcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_35_EG2e( true) ;
         }
         else
         {
            wb_table3_35_EG2e( false) ;
         }
      }

      protected void wb_table2_13_EG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTbletapa1_Internalname, tblTbletapa1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle3_Internalname, "ITENS DE VERIFICA��O", "", "", lblViewtitle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_CheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"19\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Descri��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(69), 4, 0))+"px"+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Adequa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "Grid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Titleforecolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcodigo_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtldescricao_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavCtlcumpre.Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 19 )
         {
            wbEnd = 0;
            nRC_GXsfl_19 = (short)(nGXsfl_19_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV34GXV1 = nGXsfl_19_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "N�o conformidade da Demanda", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CheckList.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_19_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_naocnfdmncod, dynavContagemresultado_naocnfdmncod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_NaoCnfDmnCod), 6, 0)), 1, dynavContagemresultado_naocnfdmncod_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e19eg1_client"+"'", "int", "", 1, dynavContagemresultado_naocnfdmncod.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WP_CheckList.htm");
            dynavContagemresultado_naocnfdmncod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_NaoCnfDmnCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_naocnfdmncod_Internalname, "Values", (String)(dynavContagemresultado_naocnfdmncod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_13_EG2e( true) ;
         }
         else
         {
            wb_table2_13_EG2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
         AV22Etapa = Convert.ToInt16(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Etapa", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Etapa), 4, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEG2( ) ;
         WSEG2( ) ;
         WEEG2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621622271");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_checklist.js", "?2020621622271");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_192( )
      {
         edtavCtlcodigo_Internalname = "CTLCODIGO_"+sGXsfl_19_idx;
         edtavCtldescricao_Internalname = "CTLDESCRICAO_"+sGXsfl_19_idx;
         cmbavCtlcumpre_Internalname = "CTLCUMPRE_"+sGXsfl_19_idx;
      }

      protected void SubsflControlProps_fel_192( )
      {
         edtavCtlcodigo_Internalname = "CTLCODIGO_"+sGXsfl_19_fel_idx;
         edtavCtldescricao_Internalname = "CTLDESCRICAO_"+sGXsfl_19_fel_idx;
         cmbavCtlcumpre_Internalname = "CTLCUMPRE_"+sGXsfl_19_fel_idx;
      }

      protected void sendrow_192( )
      {
         SubsflControlProps_192( ) ;
         WBEG0( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0x0);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_19_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_19_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlcodigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Codigo), 6, 0, ",", "")),((edtavCtlcodigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Codigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlcodigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlcodigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldescricao_Internalname,((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Descricao,((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldescricao_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtldescricao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)19,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavCtlcumpre.Enabled!=0)&&(cmbavCtlcumpre.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 22,'',false,'"+sGXsfl_19_idx+"',19)\"" : " ");
         GXCCtl = "CTLCUMPRE_" + sGXsfl_19_idx;
         cmbavCtlcumpre.Name = GXCCtl;
         cmbavCtlcumpre.WebTags = "";
         cmbavCtlcumpre.addItem("", "---", 0);
         cmbavCtlcumpre.addItem("S", "Sim", 0);
         cmbavCtlcumpre.addItem("N", "N�o", 0);
         cmbavCtlcumpre.addItem("X", "N/A", 0);
         if ( cmbavCtlcumpre.ItemCount > 0 )
         {
            if ( ( AV34GXV1 > 0 ) && ( AV5SDT_CheckList1.Count >= AV34GXV1 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Cumpre)) )
            {
               ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Cumpre = cmbavCtlcumpre.getValidValue(((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Cumpre);
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavCtlcumpre,(String)cmbavCtlcumpre_Internalname,StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Cumpre),(short)1,(String)cmbavCtlcumpre_Jsonclick,(short)5,"'"+""+"'"+",false,"+"'"+"ECTLCUMPRE.CLICK."+sGXsfl_19_idx+"'",(String)"char",(String)"",(short)-1,cmbavCtlcumpre.Enabled,(short)1,(short)0,(short)69,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavCtlcumpre.Enabled!=0)&&(cmbavCtlcumpre.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavCtlcumpre.Enabled!=0)&&(cmbavCtlcumpre.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,22);\"" : " "),(String)"",(bool)true});
         cmbavCtlcumpre.CurrentValue = StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Cumpre);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlcumpre_Internalname, "Values", (String)(cmbavCtlcumpre.ToJavascriptSource()));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLCODIGO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sGXsfl_19_idx, context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLDESCRICAO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sGXsfl_19_idx, ((SdtSDT_CheckList_Item)AV5SDT_CheckList1.Item(AV34GXV1)).gxTpr_Descricao));
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_19_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
         sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
         SubsflControlProps_192( ) ;
         /* End function sendrow_192 */
      }

      protected void SubsflControlProps_418( )
      {
         edtavCtlcodigo2_Internalname = "CTLCODIGO2_"+sGXsfl_41_idx;
         edtavCtldescricao2_Internalname = "CTLDESCRICAO2_"+sGXsfl_41_idx;
         cmbavCtlcumpre2_Internalname = "CTLCUMPRE2_"+sGXsfl_41_idx;
      }

      protected void SubsflControlProps_fel_418( )
      {
         edtavCtlcodigo2_Internalname = "CTLCODIGO2_"+sGXsfl_41_fel_idx;
         edtavCtldescricao2_Internalname = "CTLDESCRICAO2_"+sGXsfl_41_fel_idx;
         cmbavCtlcumpre2_Internalname = "CTLCUMPRE2_"+sGXsfl_41_fel_idx;
      }

      protected void sendrow_418( )
      {
         SubsflControlProps_418( ) ;
         WBEG0( ) ;
         Grid2Row = GXWebRow.GetNew(context,Grid2Container);
         if ( subGrid2_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid2_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Odd";
            }
         }
         else if ( subGrid2_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid2_Backstyle = 0;
            subGrid2_Backcolor = subGrid2_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Uniform";
            }
         }
         else if ( subGrid2_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid2_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Odd";
            }
            subGrid2_Backcolor = (int)(0x0);
         }
         else if ( subGrid2_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid2_Backstyle = 1;
            if ( ((int)((nGXsfl_41_idx) % (2))) == 0 )
            {
               subGrid2_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Even";
               }
            }
            else
            {
               subGrid2_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Odd";
               }
            }
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid2_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_41_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlcodigo2_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Codigo), 6, 0, ",", "")),((edtavCtlcodigo2_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Codigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlcodigo2_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlcodigo2_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)41,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldescricao2_Internalname,((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Descricao,((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldescricao2_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtldescricao2_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)41,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavCtlcumpre2.Enabled!=0)&&(cmbavCtlcumpre2.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 44,'',false,'"+sGXsfl_41_idx+"',41)\"" : " ");
         GXCCtl = "CTLCUMPRE2_" + sGXsfl_41_idx;
         cmbavCtlcumpre2.Name = GXCCtl;
         cmbavCtlcumpre2.WebTags = "";
         cmbavCtlcumpre2.addItem("", "---", 0);
         cmbavCtlcumpre2.addItem("S", "Sim", 0);
         cmbavCtlcumpre2.addItem("N", "N�o", 0);
         cmbavCtlcumpre2.addItem("X", "N/A", 0);
         if ( cmbavCtlcumpre2.ItemCount > 0 )
         {
            if ( ( AV38GXV2 > 0 ) && ( AV15SDT_CheckList2.Count >= AV38GXV2 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Cumpre)) )
            {
               ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Cumpre = cmbavCtlcumpre2.getValidValue(((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Cumpre);
            }
         }
         /* ComboBox */
         Grid2Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavCtlcumpre2,(String)cmbavCtlcumpre2_Internalname,StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Cumpre),(short)1,(String)cmbavCtlcumpre2_Jsonclick,(short)5,"'"+""+"'"+",false,"+"'"+"ECTLCUMPRE2.CLICK."+sGXsfl_41_idx+"'",(String)"char",(String)"",(short)-1,cmbavCtlcumpre2.Enabled,(short)1,(short)0,(short)69,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavCtlcumpre2.Enabled!=0)&&(cmbavCtlcumpre2.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavCtlcumpre2.Enabled!=0)&&(cmbavCtlcumpre2.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,44);\"" : " "),(String)"",(bool)true});
         cmbavCtlcumpre2.CurrentValue = StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV15SDT_CheckList2.Item(AV38GXV2)).gxTpr_Cumpre);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlcumpre2_Internalname, "Values", (String)(cmbavCtlcumpre2.ToJavascriptSource()));
         Grid2Container.AddRow(Grid2Row);
         nGXsfl_41_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_41_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_41_idx+1));
         sGXsfl_41_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_41_idx), 4, 0)), 4, "0");
         SubsflControlProps_418( ) ;
         /* End function sendrow_418 */
      }

      protected void init_default_properties( )
      {
         lblSubtitle_Internalname = "SUBTITLE";
         lblViewtitle3_Internalname = "VIEWTITLE3";
         edtavCtlcodigo_Internalname = "CTLCODIGO";
         edtavCtldescricao_Internalname = "CTLDESCRICAO";
         cmbavCtlcumpre_Internalname = "CTLCUMPRE";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         dynavContagemresultado_naocnfdmncod_Internalname = "vCONTAGEMRESULTADO_NAOCNFDMNCOD";
         tblTbletapa1_Internalname = "TBLETAPA1";
         lblViewtitle_Internalname = "VIEWTITLE";
         edtavCtlcodigo2_Internalname = "CTLCODIGO2";
         edtavCtldescricao2_Internalname = "CTLDESCRICAO2";
         cmbavCtlcumpre2_Internalname = "CTLCUMPRE2";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         dynavContagemresultado_naocnfcntcod_Internalname = "vCONTAGEMRESULTADO_NAOCNFCNTCOD";
         tblTbletapa2_Internalname = "TBLETAPA2";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblTable1_Internalname = "TABLE1";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
         subGrid2_Internalname = "GRID2";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavCtlcumpre2_Jsonclick = "";
         cmbavCtlcumpre2.Visible = -1;
         edtavCtldescricao2_Jsonclick = "";
         edtavCtlcodigo2_Jsonclick = "";
         cmbavCtlcumpre_Jsonclick = "";
         cmbavCtlcumpre.Visible = -1;
         edtavCtldescricao_Jsonclick = "";
         edtavCtlcodigo_Jsonclick = "";
         dynavContagemresultado_naocnfdmncod_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         cmbavCtlcumpre.Enabled = 1;
         edtavCtldescricao_Enabled = 0;
         edtavCtlcodigo_Enabled = 0;
         subGrid1_Class = "Grid";
         dynavContagemresultado_naocnfcntcod_Jsonclick = "";
         subGrid2_Allowcollapsing = 0;
         subGrid2_Allowselection = 0;
         cmbavCtlcumpre2.Enabled = 1;
         edtavCtldescricao2_Enabled = 0;
         edtavCtlcodigo2_Enabled = 0;
         subGrid2_Class = "Grid";
         bttBtnenter_Visible = 1;
         cmbavCtlcumpre2.Enabled = 1;
         cmbavCtlcumpre.Enabled = 1;
         bttBtnenter_Caption = "Confirmar";
         bttBtnenter_Tooltiptext = "Confirmar";
         dynavContagemresultado_naocnfcntcod.Enabled = 1;
         dynavContagemresultado_naocnfdmncod.Enabled = 1;
         lblSubtitle_Caption = "CHECK LIST";
         subGrid2_Titleforecolor = (int)(0x000000);
         subGrid2_Backcolorstyle = 0;
         subGrid1_Titleforecolor = (int)(0x000000);
         subGrid1_Backcolorstyle = 0;
         edtavCtldescricao2_Enabled = -1;
         edtavCtlcodigo2_Enabled = -1;
         edtavCtldescricao_Enabled = -1;
         edtavCtlcodigo_Enabled = -1;
         lblTbjava_Caption = "Java";
         lblTbjava_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'GRID2_nFirstRecordOnPage',nv:0},{av:'GRID2_nEOF',nv:0},{av:'AV22Etapa',fld:'vETAPA',pic:'ZZZ9',nv:0},{av:'AV5SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:19,pic:'',nv:null},{av:'AV15SDT_CheckList2',fld:'vSDT_CHECKLIST2',grid:41,pic:'',nv:null}],oparms:[{av:'dynavContagemresultado_naocnfdmncod'},{av:'dynavContagemresultado_naocnfcntcod'},{ctrl:'BTNENTER',prop:'Caption'},{av:'AV31Refresh',fld:'vREFRESH',pic:'',nv:false},{av:'AV6SDT_Item',fld:'vSDT_ITEM',pic:'',nv:null}]}");
         setEventMetadata("ENTER","{handler:'E11EG2',iparms:[{av:'AV5SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:19,pic:'',nv:null},{av:'AV15SDT_CheckList2',fld:'vSDT_CHECKLIST2',grid:41,pic:'',nv:null},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV12ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV9ContagemResultado_NaoCnfDmnCod',fld:'vCONTAGEMRESULTADO_NAOCNFDMNCOD',pic:'ZZZZZ9',nv:0},{av:'AV10ContagemResultado_NaoCnfCntCod',fld:'vCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV14WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV20Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV17Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22Etapa',fld:'vETAPA',pic:'ZZZ9',nv:0},{av:'AV30Caller',fld:'vCALLER',pic:'',nv:''},{av:'AV31Refresh',fld:'vREFRESH',pic:'',nv:false}],oparms:[{av:'AV6SDT_Item',fld:'vSDT_ITEM',pic:'',nv:null},{av:'AV22Etapa',fld:'vETAPA',pic:'ZZZ9',nv:0},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         setEventMetadata("CTLCUMPRE.CLICK","{handler:'E16EG2',iparms:[{av:'AV9ContagemResultado_NaoCnfDmnCod',fld:'vCONTAGEMRESULTADO_NAOCNFDMNCOD',pic:'ZZZZZ9',nv:0},{av:'AV5SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:19,pic:'',nv:null},{av:'AV22Etapa',fld:'vETAPA',pic:'ZZZ9',nv:0},{av:'AV15SDT_CheckList2',fld:'vSDT_CHECKLIST2',grid:41,pic:'',nv:null}],oparms:[{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNENTER',prop:'Caption'},{av:'AV31Refresh',fld:'vREFRESH',pic:'',nv:false},{av:'AV6SDT_Item',fld:'vSDT_ITEM',pic:'',nv:null}]}");
         setEventMetadata("CTLCUMPRE2.CLICK","{handler:'E12EG2',iparms:[{av:'AV10ContagemResultado_NaoCnfCntCod',fld:'vCONTAGEMRESULTADO_NAOCNFCNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV15SDT_CheckList2',fld:'vSDT_CHECKLIST2',grid:41,pic:'',nv:null},{av:'AV22Etapa',fld:'vETAPA',pic:'ZZZ9',nv:0},{av:'AV5SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:19,pic:'',nv:null}],oparms:[{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNENTER',prop:'Caption'},{av:'AV31Refresh',fld:'vREFRESH',pic:'',nv:false},{av:'AV6SDT_Item',fld:'vSDT_ITEM',pic:'',nv:null}]}");
         setEventMetadata("VCONTAGEMRESULTADO_NAOCNFDMNCOD.CLICK","{handler:'E19EG1',iparms:[{av:'AV22Etapa',fld:'vETAPA',pic:'ZZZ9',nv:0},{av:'AV5SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:19,pic:'',nv:null},{av:'AV15SDT_CheckList2',fld:'vSDT_CHECKLIST2',grid:41,pic:'',nv:null}],oparms:[{ctrl:'BTNENTER',prop:'Caption'},{av:'AV31Refresh',fld:'vREFRESH',pic:'',nv:false},{av:'AV6SDT_Item',fld:'vSDT_ITEM',pic:'',nv:null}]}");
         setEventMetadata("VCONTAGEMRESULTADO_NAOCNFCNTCOD.CLICK","{handler:'E18EG1',iparms:[{av:'AV22Etapa',fld:'vETAPA',pic:'ZZZ9',nv:0},{av:'AV5SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:19,pic:'',nv:null},{av:'AV15SDT_CheckList2',fld:'vSDT_CHECKLIST2',grid:41,pic:'',nv:null}],oparms:[{ctrl:'BTNENTER',prop:'Caption'},{av:'AV31Refresh',fld:'vREFRESH',pic:'',nv:false},{av:'AV6SDT_Item',fld:'vSDT_ITEM',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV14WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV5SDT_CheckList1 = new GxObjectCollection( context, "SDT_CheckList.Item", "GxEv3Up14_MeetrikaVs3", "SdtSDT_CheckList_Item", "GeneXus.Programs");
         AV15SDT_CheckList2 = new GxObjectCollection( context, "SDT_CheckList.Item", "GxEv3Up14_MeetrikaVs3", "SdtSDT_CheckList_Item", "GeneXus.Programs");
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV30Caller = "";
         AV6SDT_Item = new SdtSDT_CheckList_Item(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00EG2_A426NaoConformidade_Codigo = new int[1] ;
         H00EG2_A427NaoConformidade_Nome = new String[] {""} ;
         H00EG2_A429NaoConformidade_Tipo = new short[1] ;
         H00EG2_n429NaoConformidade_Tipo = new bool[] {false} ;
         H00EG2_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00EG2_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         H00EG3_A426NaoConformidade_Codigo = new int[1] ;
         H00EG3_A427NaoConformidade_Nome = new String[] {""} ;
         H00EG3_A429NaoConformidade_Tipo = new short[1] ;
         H00EG3_n429NaoConformidade_Tipo = new bool[] {false} ;
         H00EG3_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00EG3_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         Grid1Container = new GXWebGrid( context);
         Grid2Container = new GXWebGrid( context);
         H00EG4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00EG4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00EG4_A456ContagemResultado_Codigo = new int[1] ;
         H00EG4_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00EG4_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         H00EG4_A429NaoConformidade_Tipo = new short[1] ;
         H00EG4_n429NaoConformidade_Tipo = new bool[] {false} ;
         H00EG4_A602ContagemResultado_OSVinculada = new int[1] ;
         H00EG4_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00EG4_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EG4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EG4_A601ContagemResultado_Servico = new int[1] ;
         H00EG4_n601ContagemResultado_Servico = new bool[] {false} ;
         H00EG4_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         H00EG4_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         H00EG4_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00EG4_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00EG4_A457ContagemResultado_Demanda = new String[] {""} ;
         H00EG4_n457ContagemResultado_Demanda = new bool[] {false} ;
         A771ContagemResultado_StatusDmnVnc = "";
         A457ContagemResultado_Demanda = "";
         AV13StatusDmnPai = "";
         H00EG5_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00EG5_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00EG5_A456ContagemResultado_Codigo = new int[1] ;
         H00EG5_A517ContagemResultado_Ultima = new bool[] {false} ;
         H00EG5_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         H00EG5_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         bttBtnfechar_Jsonclick = "";
         AV11WebSession = context.GetSession();
         AV24SDT_CheckList = new GxObjectCollection( context, "SDT_CheckList.Item", "GxEv3Up14_MeetrikaVs3", "SdtSDT_CheckList_Item", "GeneXus.Programs");
         H00EG6_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         H00EG6_A1868ContagemResultadoChckLst_OSCod = new int[1] ;
         H00EG6_A758CheckList_Codigo = new int[1] ;
         H00EG6_A763CheckList_Descricao = new String[] {""} ;
         H00EG6_A762ContagemResultadoChckLst_Cumpre = new String[] {""} ;
         A763CheckList_Descricao = "";
         A762ContagemResultadoChckLst_Cumpre = "";
         H00EG7_A1151CheckList_Ativo = new bool[] {false} ;
         H00EG7_A1230CheckList_De = new short[1] ;
         H00EG7_n1230CheckList_De = new bool[] {false} ;
         H00EG7_A758CheckList_Codigo = new int[1] ;
         H00EG7_A763CheckList_Descricao = new String[] {""} ;
         H00EG8_A763CheckList_Descricao = new String[] {""} ;
         H00EG8_A1151CheckList_Ativo = new bool[] {false} ;
         H00EG8_A1230CheckList_De = new short[1] ;
         H00EG8_n1230CheckList_De = new bool[] {false} ;
         H00EG8_A758CheckList_Codigo = new int[1] ;
         Grid1Row = new GXWebRow();
         Grid2Row = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblSubtitle_Jsonclick = "";
         TempTags = "";
         bttBtnenter_Jsonclick = "";
         lblViewtitle_Jsonclick = "";
         subGrid2_Linesclass = "";
         Grid2Column = new GXWebColumn();
         lblTextblock2_Jsonclick = "";
         lblViewtitle3_Jsonclick = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         lblTextblock1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_checklist__default(),
            new Object[][] {
                new Object[] {
               H00EG2_A426NaoConformidade_Codigo, H00EG2_A427NaoConformidade_Nome, H00EG2_A429NaoConformidade_Tipo, H00EG2_A428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               H00EG3_A426NaoConformidade_Codigo, H00EG3_A427NaoConformidade_Nome, H00EG3_A429NaoConformidade_Tipo, H00EG3_A428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               H00EG4_A1553ContagemResultado_CntSrvCod, H00EG4_n1553ContagemResultado_CntSrvCod, H00EG4_A456ContagemResultado_Codigo, H00EG4_A428NaoConformidade_AreaTrabalhoCod, H00EG4_n428NaoConformidade_AreaTrabalhoCod, H00EG4_A429NaoConformidade_Tipo, H00EG4_n429NaoConformidade_Tipo, H00EG4_A602ContagemResultado_OSVinculada, H00EG4_n602ContagemResultado_OSVinculada, H00EG4_A490ContagemResultado_ContratadaCod,
               H00EG4_n490ContagemResultado_ContratadaCod, H00EG4_A601ContagemResultado_Servico, H00EG4_n601ContagemResultado_Servico, H00EG4_A771ContagemResultado_StatusDmnVnc, H00EG4_n771ContagemResultado_StatusDmnVnc, H00EG4_A468ContagemResultado_NaoCnfDmnCod, H00EG4_n468ContagemResultado_NaoCnfDmnCod, H00EG4_A457ContagemResultado_Demanda, H00EG4_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00EG5_A473ContagemResultado_DataCnt, H00EG5_A511ContagemResultado_HoraCnt, H00EG5_A456ContagemResultado_Codigo, H00EG5_A517ContagemResultado_Ultima, H00EG5_A469ContagemResultado_NaoCnfCntCod, H00EG5_n469ContagemResultado_NaoCnfCntCod
               }
               , new Object[] {
               H00EG6_A761ContagemResultadoChckLst_Codigo, H00EG6_A1868ContagemResultadoChckLst_OSCod, H00EG6_A758CheckList_Codigo, H00EG6_A763CheckList_Descricao, H00EG6_A762ContagemResultadoChckLst_Cumpre
               }
               , new Object[] {
               H00EG7_A1151CheckList_Ativo, H00EG7_A1230CheckList_De, H00EG7_n1230CheckList_De, H00EG7_A758CheckList_Codigo, H00EG7_A763CheckList_Descricao
               }
               , new Object[] {
               H00EG8_A763CheckList_Descricao, H00EG8_A1151CheckList_Ativo, H00EG8_A1230CheckList_De, H00EG8_n1230CheckList_De, H00EG8_A758CheckList_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlcodigo_Enabled = 0;
         edtavCtldescricao_Enabled = 0;
         edtavCtlcodigo2_Enabled = 0;
         edtavCtldescricao2_Enabled = 0;
      }

      private short AV22Etapa ;
      private short wcpOAV22Etapa ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_19 ;
      private short nGXsfl_19_idx=1 ;
      private short nRC_GXsfl_41 ;
      private short nGXsfl_41_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV38GXV2 ;
      private short AV34GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_19_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short nGXsfl_41_Refreshing=0 ;
      private short subGrid2_Backcolorstyle ;
      private short nGXsfl_19_fel_idx=1 ;
      private short nGXsfl_41_fel_idx=1 ;
      private short A429NaoConformidade_Tipo ;
      private short GRID1_nEOF ;
      private short GRID2_nEOF ;
      private short AV48GXLvl129 ;
      private short A1230CheckList_De ;
      private short subGrid2_Titlebackstyle ;
      private short subGrid2_Allowselection ;
      private short subGrid2_Allowhovering ;
      private short subGrid2_Allowcollapsing ;
      private short subGrid2_Collapsed ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private short subGrid2_Backstyle ;
      private int AV7ContagemResultado_Codigo ;
      private int wcpOAV7ContagemResultado_Codigo ;
      private int AV12ContagemResultado_OSVinculada ;
      private int AV20Contratada_Codigo ;
      private int AV17Servico_Codigo ;
      private int AV46GXV5 ;
      private int AV47GXV6 ;
      private int lblTbjava_Visible ;
      private int gxdynajaxindex ;
      private int AV9ContagemResultado_NaoCnfDmnCod ;
      private int AV10ContagemResultado_NaoCnfCntCod ;
      private int subGrid1_Islastpage ;
      private int subGrid2_Islastpage ;
      private int edtavCtlcodigo_Enabled ;
      private int edtavCtldescricao_Enabled ;
      private int edtavCtlcodigo2_Enabled ;
      private int edtavCtldescricao2_Enabled ;
      private int subGrid1_Titleforecolor ;
      private int subGrid2_Titleforecolor ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int A428NaoConformidade_AreaTrabalhoCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int bttBtnenter_Visible ;
      private int AV44GXV3 ;
      private int AV45GXV4 ;
      private int A1868ContagemResultadoChckLst_OSCod ;
      private int A758CheckList_Codigo ;
      private int subGrid2_Titlebackcolor ;
      private int subGrid2_Allbackcolor ;
      private int subGrid2_Selectioncolor ;
      private int subGrid2_Hoveringcolor ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private int subGrid2_Backcolor ;
      private long GRID1_nCurrentRecord ;
      private long GRID2_nCurrentRecord ;
      private long GRID1_nFirstRecordOnPage ;
      private long GRID2_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_19_idx="0001" ;
      private String GXKey ;
      private String sGXsfl_41_idx="0001" ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV30Caller ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String dynavContagemresultado_naocnfdmncod_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavCtlcodigo_Internalname ;
      private String edtavCtldescricao_Internalname ;
      private String edtavCtlcodigo2_Internalname ;
      private String edtavCtldescricao2_Internalname ;
      private String dynavContagemresultado_naocnfcntcod_Internalname ;
      private String sGXsfl_19_fel_idx="0001" ;
      private String sGXsfl_41_fel_idx="0001" ;
      private String A771ContagemResultado_StatusDmnVnc ;
      private String AV13StatusDmnPai ;
      private String lblSubtitle_Caption ;
      private String lblSubtitle_Internalname ;
      private String bttBtnenter_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnenter_Tooltiptext ;
      private String bttBtnenter_Caption ;
      private String A762ContagemResultadoChckLst_Cumpre ;
      private String cmbavCtlcumpre_Internalname ;
      private String cmbavCtlcumpre2_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblSubtitle_Jsonclick ;
      private String TempTags ;
      private String bttBtnenter_Jsonclick ;
      private String tblTbletapa2_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String subGrid2_Internalname ;
      private String subGrid2_Class ;
      private String subGrid2_Linesclass ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String dynavContagemresultado_naocnfcntcod_Jsonclick ;
      private String tblTbletapa1_Internalname ;
      private String lblViewtitle3_Internalname ;
      private String lblViewtitle3_Jsonclick ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String dynavContagemresultado_naocnfdmncod_Jsonclick ;
      private String ROClassString ;
      private String edtavCtlcodigo_Jsonclick ;
      private String edtavCtldescricao_Jsonclick ;
      private String cmbavCtlcumpre_Jsonclick ;
      private String edtavCtlcodigo2_Jsonclick ;
      private String edtavCtldescricao2_Jsonclick ;
      private String cmbavCtlcumpre2_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV31Refresh ;
      private bool AV16OkFlag ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n428NaoConformidade_AreaTrabalhoCod ;
      private bool n429NaoConformidade_Tipo ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n771ContagemResultado_StatusDmnVnc ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool A517ContagemResultado_Ultima ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool gx_refresh_fired ;
      private bool gx_BV19 ;
      private bool gx_BV41 ;
      private bool A1151CheckList_Ativo ;
      private bool n1230CheckList_De ;
      private String A763CheckList_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private IGxSession AV11WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid Grid1Container ;
      private GXWebGrid Grid2Container ;
      private GXWebRow Grid1Row ;
      private GXWebRow Grid2Row ;
      private GXWebColumn Grid2Column ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavCtlcumpre ;
      private GXCombobox dynavContagemresultado_naocnfdmncod ;
      private GXCombobox cmbavCtlcumpre2 ;
      private GXCombobox dynavContagemresultado_naocnfcntcod ;
      private IDataStoreProvider pr_default ;
      private int[] H00EG2_A426NaoConformidade_Codigo ;
      private String[] H00EG2_A427NaoConformidade_Nome ;
      private short[] H00EG2_A429NaoConformidade_Tipo ;
      private bool[] H00EG2_n429NaoConformidade_Tipo ;
      private int[] H00EG2_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00EG2_n428NaoConformidade_AreaTrabalhoCod ;
      private int[] H00EG3_A426NaoConformidade_Codigo ;
      private String[] H00EG3_A427NaoConformidade_Nome ;
      private short[] H00EG3_A429NaoConformidade_Tipo ;
      private bool[] H00EG3_n429NaoConformidade_Tipo ;
      private int[] H00EG3_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00EG3_n428NaoConformidade_AreaTrabalhoCod ;
      private int[] H00EG4_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00EG4_n1553ContagemResultado_CntSrvCod ;
      private int[] H00EG4_A456ContagemResultado_Codigo ;
      private int[] H00EG4_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00EG4_n428NaoConformidade_AreaTrabalhoCod ;
      private short[] H00EG4_A429NaoConformidade_Tipo ;
      private bool[] H00EG4_n429NaoConformidade_Tipo ;
      private int[] H00EG4_A602ContagemResultado_OSVinculada ;
      private bool[] H00EG4_n602ContagemResultado_OSVinculada ;
      private int[] H00EG4_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EG4_n490ContagemResultado_ContratadaCod ;
      private int[] H00EG4_A601ContagemResultado_Servico ;
      private bool[] H00EG4_n601ContagemResultado_Servico ;
      private String[] H00EG4_A771ContagemResultado_StatusDmnVnc ;
      private bool[] H00EG4_n771ContagemResultado_StatusDmnVnc ;
      private int[] H00EG4_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00EG4_n468ContagemResultado_NaoCnfDmnCod ;
      private String[] H00EG4_A457ContagemResultado_Demanda ;
      private bool[] H00EG4_n457ContagemResultado_Demanda ;
      private DateTime[] H00EG5_A473ContagemResultado_DataCnt ;
      private String[] H00EG5_A511ContagemResultado_HoraCnt ;
      private int[] H00EG5_A456ContagemResultado_Codigo ;
      private bool[] H00EG5_A517ContagemResultado_Ultima ;
      private int[] H00EG5_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] H00EG5_n469ContagemResultado_NaoCnfCntCod ;
      private short[] H00EG6_A761ContagemResultadoChckLst_Codigo ;
      private int[] H00EG6_A1868ContagemResultadoChckLst_OSCod ;
      private int[] H00EG6_A758CheckList_Codigo ;
      private String[] H00EG6_A763CheckList_Descricao ;
      private String[] H00EG6_A762ContagemResultadoChckLst_Cumpre ;
      private bool[] H00EG7_A1151CheckList_Ativo ;
      private short[] H00EG7_A1230CheckList_De ;
      private bool[] H00EG7_n1230CheckList_De ;
      private int[] H00EG7_A758CheckList_Codigo ;
      private String[] H00EG7_A763CheckList_Descricao ;
      private String[] H00EG8_A763CheckList_Descricao ;
      private bool[] H00EG8_A1151CheckList_Ativo ;
      private short[] H00EG8_A1230CheckList_De ;
      private bool[] H00EG8_n1230CheckList_De ;
      private int[] H00EG8_A758CheckList_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CheckList_Item ))]
      private IGxCollection AV5SDT_CheckList1 ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CheckList_Item ))]
      private IGxCollection AV15SDT_CheckList2 ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CheckList_Item ))]
      private IGxCollection AV24SDT_CheckList ;
      private GXWebForm Form ;
      private SdtSDT_CheckList_Item AV6SDT_Item ;
      private wwpbaseobjects.SdtWWPContext AV14WWPContext ;
   }

   public class wp_checklist__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EG2 ;
          prmH00EG2 = new Object[] {
          new Object[] {"@AV14WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EG3 ;
          prmH00EG3 = new Object[] {
          new Object[] {"@AV14WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EG4 ;
          prmH00EG4 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EG5 ;
          prmH00EG5 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EG6 ;
          prmH00EG6 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EG7 ;
          prmH00EG7 = new Object[] {
          } ;
          Object[] prmH00EG8 ;
          prmH00EG8 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EG2", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome], [NaoConformidade_Tipo], [NaoConformidade_AreaTrabalhoCod] FROM [NaoConformidade] WITH (NOLOCK) WHERE ([NaoConformidade_Tipo] = 4) AND ([NaoConformidade_AreaTrabalhoCod] = @AV14WWPC_1Areatrabalho_codigo) ORDER BY [NaoConformidade_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EG2,0,0,true,false )
             ,new CursorDef("H00EG3", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome], [NaoConformidade_Tipo], [NaoConformidade_AreaTrabalhoCod] FROM [NaoConformidade] WITH (NOLOCK) WHERE ([NaoConformidade_Tipo] = 5) AND ([NaoConformidade_AreaTrabalhoCod] = @AV14WWPC_1Areatrabalho_codigo) ORDER BY [NaoConformidade_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EG3,0,0,true,false )
             ,new CursorDef("H00EG4", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T4.[NaoConformidade_AreaTrabalhoCod], T4.[NaoConformidade_Tipo], T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_ContratadaCod], T2.[Servico_Codigo] AS ContagemResultado_Servico, T3.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T1.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T1.[ContagemResultado_Demanda] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [NaoConformidade] T4 WITH (NOLOCK) ON T4.[NaoConformidade_Codigo] = T1.[ContagemResultado_NaoCnfDmnCod]) WHERE T1.[ContagemResultado_Codigo] = @AV7ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EG4,1,0,true,true )
             ,new CursorDef("H00EG5", "SELECT TOP 1 [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_NaoCnfCntCod] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EG5,1,0,false,true )
             ,new CursorDef("H00EG6", "SELECT T1.[ContagemResultadoChckLst_Codigo], T1.[ContagemResultadoChckLst_OSCod], T1.[CheckList_Codigo], T2.[CheckList_Descricao], T1.[ContagemResultadoChckLst_Cumpre] FROM ([ContagemResultadoChckLst] T1 WITH (NOLOCK) INNER JOIN [CheckList] T2 WITH (NOLOCK) ON T2.[CheckList_Codigo] = T1.[CheckList_Codigo]) WHERE T1.[ContagemResultadoChckLst_OSCod] = @AV7ContagemResultado_Codigo ORDER BY T1.[ContagemResultadoChckLst_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EG6,100,0,false,false )
             ,new CursorDef("H00EG7", "SELECT [CheckList_Ativo], [CheckList_De], [CheckList_Codigo], [CheckList_Descricao] FROM [CheckList] WITH (NOLOCK) WHERE ([CheckList_Ativo] = 1) AND ([CheckList_De] = 1) ORDER BY [CheckList_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EG7,100,0,false,false )
             ,new CursorDef("H00EG8", "SELECT [CheckList_Descricao], [CheckList_Ativo], [CheckList_De], [CheckList_Codigo] FROM [CheckList] WITH (NOLOCK) WHERE ([CheckList_Ativo] = 1) AND ([CheckList_Descricao] like '2%') AND ([CheckList_De] = 1) ORDER BY [CheckList_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EG8,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                return;
             case 3 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 5) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 1) ;
                return;
             case 5 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
