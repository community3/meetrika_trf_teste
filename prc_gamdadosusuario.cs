/*
               File: Prc_GAMDadosUsuario
        Description: Prc_GAMDados Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:56.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_gamdadosusuario : GXProcedure
   {
      public prc_gamdadosusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_gamdadosusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           out DateTime aP1_Usuario_DataCriacao ,
                           out DateTime aP2_Usuario_DataAlteracao )
      {
         this.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV11Usuario_DataCriacao = DateTime.MinValue ;
         this.AV12Usuario_DataAlteracao = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP1_Usuario_DataCriacao=this.AV11Usuario_DataCriacao;
         aP2_Usuario_DataAlteracao=this.AV12Usuario_DataAlteracao;
      }

      public DateTime executeUdp( int aP0_Usuario_Codigo ,
                                  out DateTime aP1_Usuario_DataCriacao )
      {
         this.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV11Usuario_DataCriacao = DateTime.MinValue ;
         this.AV12Usuario_DataAlteracao = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP1_Usuario_DataCriacao=this.AV11Usuario_DataCriacao;
         aP2_Usuario_DataAlteracao=this.AV12Usuario_DataAlteracao;
         return AV12Usuario_DataAlteracao ;
      }

      public void executeSubmit( int aP0_Usuario_Codigo ,
                                 out DateTime aP1_Usuario_DataCriacao ,
                                 out DateTime aP2_Usuario_DataAlteracao )
      {
         prc_gamdadosusuario objprc_gamdadosusuario;
         objprc_gamdadosusuario = new prc_gamdadosusuario();
         objprc_gamdadosusuario.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_gamdadosusuario.AV11Usuario_DataCriacao = DateTime.MinValue ;
         objprc_gamdadosusuario.AV12Usuario_DataAlteracao = DateTime.MinValue ;
         objprc_gamdadosusuario.context.SetSubmitInitialConfig(context);
         objprc_gamdadosusuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_gamdadosusuario);
         aP1_Usuario_DataCriacao=this.AV11Usuario_DataCriacao;
         aP2_Usuario_DataAlteracao=this.AV12Usuario_DataAlteracao;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_gamdadosusuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00D92 */
         pr_default.execute(0, new Object[] {AV8Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1Usuario_Codigo = P00D92_A1Usuario_Codigo[0];
            A341Usuario_UserGamGuid = P00D92_A341Usuario_UserGamGuid[0];
            AV9GamUser.load( A341Usuario_UserGamGuid);
            AV10Usuario_Nome = AV9GamUser.gxTpr_Name;
            AV11Usuario_DataCriacao = AV9GamUser.gxTpr_Datecreated;
            AV12Usuario_DataAlteracao = AV9GamUser.gxTpr_Dateupdated;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00D92_A1Usuario_Codigo = new int[1] ;
         P00D92_A341Usuario_UserGamGuid = new String[] {""} ;
         A341Usuario_UserGamGuid = "";
         AV9GamUser = new SdtGAMUser(context);
         AV10Usuario_Nome = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_gamdadosusuario__default(),
            new Object[][] {
                new Object[] {
               P00D92_A1Usuario_Codigo, P00D92_A341Usuario_UserGamGuid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Usuario_Codigo ;
      private int A1Usuario_Codigo ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private String AV10Usuario_Nome ;
      private DateTime AV12Usuario_DataAlteracao ;
      private DateTime AV11Usuario_DataCriacao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00D92_A1Usuario_Codigo ;
      private String[] P00D92_A341Usuario_UserGamGuid ;
      private DateTime aP1_Usuario_DataCriacao ;
      private DateTime aP2_Usuario_DataAlteracao ;
      private SdtGAMUser AV9GamUser ;
   }

   public class prc_gamdadosusuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00D92 ;
          prmP00D92 = new Object[] {
          new Object[] {"@AV8Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00D92", "SELECT [Usuario_Codigo], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV8Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00D92,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
