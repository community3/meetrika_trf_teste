/*
               File: ContratoServicosPrioridade
        Description: Prioridade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:25.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosprioridade : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action10") == 0 )
         {
            A1335ContratoServicosPrioridade_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
            A2066ContratoServicosPrioridade_Ordem = (short)(NumberUtil.Val( GetNextPar( ), "."));
            n2066ContratoServicosPrioridade_Ordem = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_10_3L162( A1335ContratoServicosPrioridade_CntSrvCod, A2066ContratoServicosPrioridade_Ordem) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel8"+"_"+"CONTRATOSERVICOSPRIORIDADE_ORDEM") == 0 )
         {
            Gx_mode = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            AV11Insert_ContratoServicosPrioridade_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContratoServicosPrioridade_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX8ASACONTRATOSERVICOSPRIORIDADE_ORDEM3L162( Gx_mode, AV11Insert_ContratoServicosPrioridade_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A1335ContratoServicosPrioridade_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A1335ContratoServicosPrioridade_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoServicosPrioridade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosPrioridade_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOSPRIORIDADE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosPrioridade_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Prioridade", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoServicosPrioridade_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoservicosprioridade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicosprioridade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoServicosPrioridade_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoServicosPrioridade_Codigo = aP1_ContratoServicosPrioridade_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3L162( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3L162e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0, ",", "")), ((edtContratoServicosPrioridade_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1336ContratoServicosPrioridade_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1336ContratoServicosPrioridade_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosPrioridade_Codigo_Visible, edtContratoServicosPrioridade_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosPrioridade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_CntSrvCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosPrioridade_CntSrvCod_Visible, edtContratoServicosPrioridade_CntSrvCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosPrioridade.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3L162( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3L162( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3L162e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_59_3L162( true) ;
         }
         return  ;
      }

      protected void wb_table3_59_3L162e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3L162e( true) ;
         }
         else
         {
            wb_table1_2_3L162e( false) ;
         }
      }

      protected void wb_table3_59_3L162( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_59_3L162e( true) ;
         }
         else
         {
            wb_table3_59_3L162e( false) ;
         }
      }

      protected void wb_table2_5_3L162( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_3L162( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_3L162e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3L162e( true) ;
         }
         else
         {
            wb_table2_5_3L162e( false) ;
         }
      }

      protected void wb_table4_13_3L162( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_ordem_Internalname, "Ordem", "", "", lblTextblockcontratoservicosprioridade_ordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_Ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0, ",", "")), ((edtContratoServicosPrioridade_Ordem_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9")) : context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_Ordem_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosPrioridade_Ordem_Enabled, 0, "text", "", 40, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Ordem", "right", false, "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_nome_Internalname, "Nome", "", "", lblTextblockcontratoservicosprioridade_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_Nome_Internalname, StringUtil.RTrim( A1337ContratoServicosPrioridade_Nome), StringUtil.RTrim( context.localUtil.Format( A1337ContratoServicosPrioridade_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosPrioridade_Nome_Enabled, 0, "text", "", 0, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_percvalorb_Internalname, "% do Valor B", "", "", lblTextblockcontratoservicosprioridade_percvalorb_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_PercValorB_Internalname, StringUtil.LTrim( StringUtil.NToC( A1338ContratoServicosPrioridade_PercValorB, 8, 2, ",", "")), ((edtContratoServicosPrioridade_PercValorB_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1338ContratoServicosPrioridade_PercValorB, "ZZ9.99 %")) : context.localUtil.Format( A1338ContratoServicosPrioridade_PercValorB, "ZZ9.99 %")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_PercValorB_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosPrioridade_PercValorB_Enabled, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_percprazo_Internalname, "% do Prazo", "", "", lblTextblockcontratoservicosprioridade_percprazo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_PercPrazo_Internalname, StringUtil.LTrim( StringUtil.NToC( A1339ContratoServicosPrioridade_PercPrazo, 8, 2, ",", "")), ((edtContratoServicosPrioridade_PercPrazo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1339ContratoServicosPrioridade_PercPrazo, "ZZ9.99 %")) : context.localUtil.Format( A1339ContratoServicosPrioridade_PercPrazo, "ZZ9.99 %")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_PercPrazo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosPrioridade_PercPrazo_Enabled, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_finalidade_Internalname, "Finalidade", "", "", lblTextblockcontratoservicosprioridade_finalidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosPrioridade_Finalidade_Internalname, A1359ContratoServicosPrioridade_Finalidade, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", 0, 1, edtContratoServicosPrioridade_Finalidade_Enabled, 0, 100, "%", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_peso_Internalname, "Peso", "", "", lblTextblockcontratoservicosprioridade_peso_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrioridade_Peso_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0, ",", "")), ((edtContratoServicosPrioridade_Peso_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2067ContratoServicosPrioridade_Peso), "ZZZ9")) : context.localUtil.Format( (decimal)(A2067ContratoServicosPrioridade_Peso), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrioridade_Peso_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosPrioridade_Peso_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_3L162e( true) ;
         }
         else
         {
            wb_table4_13_3L162e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113L2 */
         E113L2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A2066ContratoServicosPrioridade_Ordem = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Ordem_Internalname), ",", "."));
               n2066ContratoServicosPrioridade_Ordem = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
               A1337ContratoServicosPrioridade_Nome = StringUtil.Upper( cgiGet( edtContratoServicosPrioridade_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1337ContratoServicosPrioridade_Nome", A1337ContratoServicosPrioridade_Nome);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_PercValorB_Internalname), ",", ".") < -99.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_PercValorB_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSPRIORIDADE_PERCVALORB");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrioridade_PercValorB_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1338ContratoServicosPrioridade_PercValorB = 0;
                  n1338ContratoServicosPrioridade_PercValorB = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1338ContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( A1338ContratoServicosPrioridade_PercValorB, 6, 2)));
               }
               else
               {
                  A1338ContratoServicosPrioridade_PercValorB = context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_PercValorB_Internalname), ",", ".");
                  n1338ContratoServicosPrioridade_PercValorB = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1338ContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( A1338ContratoServicosPrioridade_PercValorB, 6, 2)));
               }
               n1338ContratoServicosPrioridade_PercValorB = ((Convert.ToDecimal(0)==A1338ContratoServicosPrioridade_PercValorB) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_PercPrazo_Internalname), ",", ".") < -99.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_PercPrazo_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSPRIORIDADE_PERCPRAZO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrioridade_PercPrazo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1339ContratoServicosPrioridade_PercPrazo = 0;
                  n1339ContratoServicosPrioridade_PercPrazo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1339ContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( A1339ContratoServicosPrioridade_PercPrazo, 6, 2)));
               }
               else
               {
                  A1339ContratoServicosPrioridade_PercPrazo = context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_PercPrazo_Internalname), ",", ".");
                  n1339ContratoServicosPrioridade_PercPrazo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1339ContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( A1339ContratoServicosPrioridade_PercPrazo, 6, 2)));
               }
               n1339ContratoServicosPrioridade_PercPrazo = ((Convert.ToDecimal(0)==A1339ContratoServicosPrioridade_PercPrazo) ? true : false);
               A1359ContratoServicosPrioridade_Finalidade = cgiGet( edtContratoServicosPrioridade_Finalidade_Internalname);
               n1359ContratoServicosPrioridade_Finalidade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1359ContratoServicosPrioridade_Finalidade", A1359ContratoServicosPrioridade_Finalidade);
               n1359ContratoServicosPrioridade_Finalidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1359ContratoServicosPrioridade_Finalidade)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Peso_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Peso_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSPRIORIDADE_PESO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrioridade_Peso_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2067ContratoServicosPrioridade_Peso = 0;
                  n2067ContratoServicosPrioridade_Peso = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2067ContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0)));
               }
               else
               {
                  A2067ContratoServicosPrioridade_Peso = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Peso_Internalname), ",", "."));
                  n2067ContratoServicosPrioridade_Peso = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2067ContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0)));
               }
               n2067ContratoServicosPrioridade_Peso = ((0==A2067ContratoServicosPrioridade_Peso) ? true : false);
               A1336ContratoServicosPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_CntSrvCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_CntSrvCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrioridade_CntSrvCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1335ContratoServicosPrioridade_CntSrvCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
               }
               else
               {
                  A1335ContratoServicosPrioridade_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_CntSrvCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
               }
               /* Read saved values. */
               Z1336ContratoServicosPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1336ContratoServicosPrioridade_Codigo"), ",", "."));
               Z2066ContratoServicosPrioridade_Ordem = (short)(context.localUtil.CToN( cgiGet( "Z2066ContratoServicosPrioridade_Ordem"), ",", "."));
               n2066ContratoServicosPrioridade_Ordem = ((0==A2066ContratoServicosPrioridade_Ordem) ? true : false);
               Z1337ContratoServicosPrioridade_Nome = cgiGet( "Z1337ContratoServicosPrioridade_Nome");
               Z1338ContratoServicosPrioridade_PercValorB = context.localUtil.CToN( cgiGet( "Z1338ContratoServicosPrioridade_PercValorB"), ",", ".");
               n1338ContratoServicosPrioridade_PercValorB = ((Convert.ToDecimal(0)==A1338ContratoServicosPrioridade_PercValorB) ? true : false);
               Z1339ContratoServicosPrioridade_PercPrazo = context.localUtil.CToN( cgiGet( "Z1339ContratoServicosPrioridade_PercPrazo"), ",", ".");
               n1339ContratoServicosPrioridade_PercPrazo = ((Convert.ToDecimal(0)==A1339ContratoServicosPrioridade_PercPrazo) ? true : false);
               Z2067ContratoServicosPrioridade_Peso = (short)(context.localUtil.CToN( cgiGet( "Z2067ContratoServicosPrioridade_Peso"), ",", "."));
               n2067ContratoServicosPrioridade_Peso = ((0==A2067ContratoServicosPrioridade_Peso) ? true : false);
               Z1335ContratoServicosPrioridade_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "Z1335ContratoServicosPrioridade_CntSrvCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1335ContratoServicosPrioridade_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "N1335ContratoServicosPrioridade_CntSrvCod"), ",", "."));
               AV7ContratoServicosPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOSPRIORIDADE_CODIGO"), ",", "."));
               AV11Insert_ContratoServicosPrioridade_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoServicosPrioridade";
               A2066ContratoServicosPrioridade_Ordem = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Ordem_Internalname), ",", "."));
               n2066ContratoServicosPrioridade_Ordem = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9");
               A1336ContratoServicosPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1336ContratoServicosPrioridade_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1336ContratoServicosPrioridade_Codigo != Z1336ContratoServicosPrioridade_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoservicosprioridade:[SecurityCheckFailed value for]"+"ContratoServicosPrioridade_Ordem:"+context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9"));
                  GXUtil.WriteLog("contratoservicosprioridade:[SecurityCheckFailed value for]"+"ContratoServicosPrioridade_Codigo:"+context.localUtil.Format( (decimal)(A1336ContratoServicosPrioridade_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contratoservicosprioridade:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1336ContratoServicosPrioridade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode162 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode162;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound162 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3L0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATOSERVICOSPRIORIDADE_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContratoServicosPrioridade_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113L2 */
                           E113L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123L2 */
                           E123L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123L2 */
            E123L2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3L162( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3L162( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3L0( )
      {
         BeforeValidate3L162( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3L162( ) ;
            }
            else
            {
               CheckExtendedTable3L162( ) ;
               CloseExtendedTableCursors3L162( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3L0( )
      {
      }

      protected void E113L2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ContratoServicosPrioridade_CntSrvCod") == 0 )
               {
                  AV11Insert_ContratoServicosPrioridade_CntSrvCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContratoServicosPrioridade_CntSrvCod), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
         edtContratoServicosPrioridade_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_Codigo_Visible), 5, 0)));
         edtContratoServicosPrioridade_CntSrvCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_CntSrvCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_CntSrvCod_Visible), 5, 0)));
      }

      protected void E123L2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratoservicosprioridade.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM3L162( short GX_JID )
      {
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2066ContratoServicosPrioridade_Ordem = T003L3_A2066ContratoServicosPrioridade_Ordem[0];
               Z1337ContratoServicosPrioridade_Nome = T003L3_A1337ContratoServicosPrioridade_Nome[0];
               Z1338ContratoServicosPrioridade_PercValorB = T003L3_A1338ContratoServicosPrioridade_PercValorB[0];
               Z1339ContratoServicosPrioridade_PercPrazo = T003L3_A1339ContratoServicosPrioridade_PercPrazo[0];
               Z2067ContratoServicosPrioridade_Peso = T003L3_A2067ContratoServicosPrioridade_Peso[0];
               Z1335ContratoServicosPrioridade_CntSrvCod = T003L3_A1335ContratoServicosPrioridade_CntSrvCod[0];
            }
            else
            {
               Z2066ContratoServicosPrioridade_Ordem = A2066ContratoServicosPrioridade_Ordem;
               Z1337ContratoServicosPrioridade_Nome = A1337ContratoServicosPrioridade_Nome;
               Z1338ContratoServicosPrioridade_PercValorB = A1338ContratoServicosPrioridade_PercValorB;
               Z1339ContratoServicosPrioridade_PercPrazo = A1339ContratoServicosPrioridade_PercPrazo;
               Z2067ContratoServicosPrioridade_Peso = A2067ContratoServicosPrioridade_Peso;
               Z1335ContratoServicosPrioridade_CntSrvCod = A1335ContratoServicosPrioridade_CntSrvCod;
            }
         }
         if ( GX_JID == -11 )
         {
            Z1336ContratoServicosPrioridade_Codigo = A1336ContratoServicosPrioridade_Codigo;
            Z2066ContratoServicosPrioridade_Ordem = A2066ContratoServicosPrioridade_Ordem;
            Z1337ContratoServicosPrioridade_Nome = A1337ContratoServicosPrioridade_Nome;
            Z1359ContratoServicosPrioridade_Finalidade = A1359ContratoServicosPrioridade_Finalidade;
            Z1338ContratoServicosPrioridade_PercValorB = A1338ContratoServicosPrioridade_PercValorB;
            Z1339ContratoServicosPrioridade_PercPrazo = A1339ContratoServicosPrioridade_PercPrazo;
            Z2067ContratoServicosPrioridade_Peso = A2067ContratoServicosPrioridade_Peso;
            Z1335ContratoServicosPrioridade_CntSrvCod = A1335ContratoServicosPrioridade_CntSrvCod;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContratoServicosPrioridade_Ordem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Ordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_Ordem_Enabled), 5, 0)));
         edtContratoServicosPrioridade_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_Codigo_Enabled), 5, 0)));
         AV13Pgmname = "ContratoServicosPrioridade";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         edtContratoServicosPrioridade_Ordem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Ordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_Ordem_Enabled), 5, 0)));
         edtContratoServicosPrioridade_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoServicosPrioridade_Codigo) )
         {
            A1336ContratoServicosPrioridade_Codigo = AV7ContratoServicosPrioridade_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContratoServicosPrioridade_CntSrvCod) )
         {
            edtContratoServicosPrioridade_CntSrvCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_CntSrvCod_Enabled), 5, 0)));
         }
         else
         {
            edtContratoServicosPrioridade_CntSrvCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_CntSrvCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContratoServicosPrioridade_CntSrvCod) )
         {
            A1335ContratoServicosPrioridade_CntSrvCod = AV11Insert_ContratoServicosPrioridade_CntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            GXt_int1 = A2066ContratoServicosPrioridade_Ordem;
            new prc_maxprioridadeordem(context ).execute( ref  AV11Insert_ContratoServicosPrioridade_CntSrvCod, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContratoServicosPrioridade_CntSrvCod), 6, 0)));
            A2066ContratoServicosPrioridade_Ordem = (short)(GXt_int1+1);
            n2066ContratoServicosPrioridade_Ordem = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load3L162( )
      {
         /* Using cursor T003L5 */
         pr_default.execute(3, new Object[] {A1336ContratoServicosPrioridade_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound162 = 1;
            A2066ContratoServicosPrioridade_Ordem = T003L5_A2066ContratoServicosPrioridade_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
            n2066ContratoServicosPrioridade_Ordem = T003L5_n2066ContratoServicosPrioridade_Ordem[0];
            A1337ContratoServicosPrioridade_Nome = T003L5_A1337ContratoServicosPrioridade_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1337ContratoServicosPrioridade_Nome", A1337ContratoServicosPrioridade_Nome);
            A1359ContratoServicosPrioridade_Finalidade = T003L5_A1359ContratoServicosPrioridade_Finalidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1359ContratoServicosPrioridade_Finalidade", A1359ContratoServicosPrioridade_Finalidade);
            n1359ContratoServicosPrioridade_Finalidade = T003L5_n1359ContratoServicosPrioridade_Finalidade[0];
            A1338ContratoServicosPrioridade_PercValorB = T003L5_A1338ContratoServicosPrioridade_PercValorB[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1338ContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( A1338ContratoServicosPrioridade_PercValorB, 6, 2)));
            n1338ContratoServicosPrioridade_PercValorB = T003L5_n1338ContratoServicosPrioridade_PercValorB[0];
            A1339ContratoServicosPrioridade_PercPrazo = T003L5_A1339ContratoServicosPrioridade_PercPrazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1339ContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( A1339ContratoServicosPrioridade_PercPrazo, 6, 2)));
            n1339ContratoServicosPrioridade_PercPrazo = T003L5_n1339ContratoServicosPrioridade_PercPrazo[0];
            A2067ContratoServicosPrioridade_Peso = T003L5_A2067ContratoServicosPrioridade_Peso[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2067ContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0)));
            n2067ContratoServicosPrioridade_Peso = T003L5_n2067ContratoServicosPrioridade_Peso[0];
            A1335ContratoServicosPrioridade_CntSrvCod = T003L5_A1335ContratoServicosPrioridade_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
            ZM3L162( -11) ;
         }
         pr_default.close(3);
         OnLoadActions3L162( ) ;
      }

      protected void OnLoadActions3L162( )
      {
      }

      protected void CheckExtendedTable3L162( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003L4 */
         pr_default.execute(2, new Object[] {A1335ContratoServicosPrioridade_CntSrvCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Prioridades_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrioridade_CntSrvCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors3L162( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_12( int A1335ContratoServicosPrioridade_CntSrvCod )
      {
         /* Using cursor T003L6 */
         pr_default.execute(4, new Object[] {A1335ContratoServicosPrioridade_CntSrvCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Prioridades_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrioridade_CntSrvCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey3L162( )
      {
         /* Using cursor T003L7 */
         pr_default.execute(5, new Object[] {A1336ContratoServicosPrioridade_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound162 = 1;
         }
         else
         {
            RcdFound162 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003L3 */
         pr_default.execute(1, new Object[] {A1336ContratoServicosPrioridade_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3L162( 11) ;
            RcdFound162 = 1;
            A1336ContratoServicosPrioridade_Codigo = T003L3_A1336ContratoServicosPrioridade_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
            A2066ContratoServicosPrioridade_Ordem = T003L3_A2066ContratoServicosPrioridade_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
            n2066ContratoServicosPrioridade_Ordem = T003L3_n2066ContratoServicosPrioridade_Ordem[0];
            A1337ContratoServicosPrioridade_Nome = T003L3_A1337ContratoServicosPrioridade_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1337ContratoServicosPrioridade_Nome", A1337ContratoServicosPrioridade_Nome);
            A1359ContratoServicosPrioridade_Finalidade = T003L3_A1359ContratoServicosPrioridade_Finalidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1359ContratoServicosPrioridade_Finalidade", A1359ContratoServicosPrioridade_Finalidade);
            n1359ContratoServicosPrioridade_Finalidade = T003L3_n1359ContratoServicosPrioridade_Finalidade[0];
            A1338ContratoServicosPrioridade_PercValorB = T003L3_A1338ContratoServicosPrioridade_PercValorB[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1338ContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( A1338ContratoServicosPrioridade_PercValorB, 6, 2)));
            n1338ContratoServicosPrioridade_PercValorB = T003L3_n1338ContratoServicosPrioridade_PercValorB[0];
            A1339ContratoServicosPrioridade_PercPrazo = T003L3_A1339ContratoServicosPrioridade_PercPrazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1339ContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( A1339ContratoServicosPrioridade_PercPrazo, 6, 2)));
            n1339ContratoServicosPrioridade_PercPrazo = T003L3_n1339ContratoServicosPrioridade_PercPrazo[0];
            A2067ContratoServicosPrioridade_Peso = T003L3_A2067ContratoServicosPrioridade_Peso[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2067ContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0)));
            n2067ContratoServicosPrioridade_Peso = T003L3_n2067ContratoServicosPrioridade_Peso[0];
            A1335ContratoServicosPrioridade_CntSrvCod = T003L3_A1335ContratoServicosPrioridade_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
            Z1336ContratoServicosPrioridade_Codigo = A1336ContratoServicosPrioridade_Codigo;
            sMode162 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3L162( ) ;
            if ( AnyError == 1 )
            {
               RcdFound162 = 0;
               InitializeNonKey3L162( ) ;
            }
            Gx_mode = sMode162;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound162 = 0;
            InitializeNonKey3L162( ) ;
            sMode162 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode162;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3L162( ) ;
         if ( RcdFound162 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound162 = 0;
         /* Using cursor T003L8 */
         pr_default.execute(6, new Object[] {A1336ContratoServicosPrioridade_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T003L8_A1336ContratoServicosPrioridade_Codigo[0] < A1336ContratoServicosPrioridade_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T003L8_A1336ContratoServicosPrioridade_Codigo[0] > A1336ContratoServicosPrioridade_Codigo ) ) )
            {
               A1336ContratoServicosPrioridade_Codigo = T003L8_A1336ContratoServicosPrioridade_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
               RcdFound162 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound162 = 0;
         /* Using cursor T003L9 */
         pr_default.execute(7, new Object[] {A1336ContratoServicosPrioridade_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T003L9_A1336ContratoServicosPrioridade_Codigo[0] > A1336ContratoServicosPrioridade_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T003L9_A1336ContratoServicosPrioridade_Codigo[0] < A1336ContratoServicosPrioridade_Codigo ) ) )
            {
               A1336ContratoServicosPrioridade_Codigo = T003L9_A1336ContratoServicosPrioridade_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
               RcdFound162 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3L162( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoServicosPrioridade_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3L162( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound162 == 1 )
            {
               if ( A1336ContratoServicosPrioridade_Codigo != Z1336ContratoServicosPrioridade_Codigo )
               {
                  A1336ContratoServicosPrioridade_Codigo = Z1336ContratoServicosPrioridade_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOSERVICOSPRIORIDADE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrioridade_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoServicosPrioridade_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3L162( ) ;
                  GX_FocusControl = edtContratoServicosPrioridade_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1336ContratoServicosPrioridade_Codigo != Z1336ContratoServicosPrioridade_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratoServicosPrioridade_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3L162( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOSPRIORIDADE_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContratoServicosPrioridade_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratoServicosPrioridade_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3L162( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1336ContratoServicosPrioridade_Codigo != Z1336ContratoServicosPrioridade_Codigo )
         {
            A1336ContratoServicosPrioridade_Codigo = Z1336ContratoServicosPrioridade_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOSERVICOSPRIORIDADE_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrioridade_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoServicosPrioridade_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3L162( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003L2 */
            pr_default.execute(0, new Object[] {A1336ContratoServicosPrioridade_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosPrioridade"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z2066ContratoServicosPrioridade_Ordem != T003L2_A2066ContratoServicosPrioridade_Ordem[0] ) || ( StringUtil.StrCmp(Z1337ContratoServicosPrioridade_Nome, T003L2_A1337ContratoServicosPrioridade_Nome[0]) != 0 ) || ( Z1338ContratoServicosPrioridade_PercValorB != T003L2_A1338ContratoServicosPrioridade_PercValorB[0] ) || ( Z1339ContratoServicosPrioridade_PercPrazo != T003L2_A1339ContratoServicosPrioridade_PercPrazo[0] ) || ( Z2067ContratoServicosPrioridade_Peso != T003L2_A2067ContratoServicosPrioridade_Peso[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1335ContratoServicosPrioridade_CntSrvCod != T003L2_A1335ContratoServicosPrioridade_CntSrvCod[0] ) )
            {
               if ( Z2066ContratoServicosPrioridade_Ordem != T003L2_A2066ContratoServicosPrioridade_Ordem[0] )
               {
                  GXUtil.WriteLog("contratoservicosprioridade:[seudo value changed for attri]"+"ContratoServicosPrioridade_Ordem");
                  GXUtil.WriteLogRaw("Old: ",Z2066ContratoServicosPrioridade_Ordem);
                  GXUtil.WriteLogRaw("Current: ",T003L2_A2066ContratoServicosPrioridade_Ordem[0]);
               }
               if ( StringUtil.StrCmp(Z1337ContratoServicosPrioridade_Nome, T003L2_A1337ContratoServicosPrioridade_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosprioridade:[seudo value changed for attri]"+"ContratoServicosPrioridade_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z1337ContratoServicosPrioridade_Nome);
                  GXUtil.WriteLogRaw("Current: ",T003L2_A1337ContratoServicosPrioridade_Nome[0]);
               }
               if ( Z1338ContratoServicosPrioridade_PercValorB != T003L2_A1338ContratoServicosPrioridade_PercValorB[0] )
               {
                  GXUtil.WriteLog("contratoservicosprioridade:[seudo value changed for attri]"+"ContratoServicosPrioridade_PercValorB");
                  GXUtil.WriteLogRaw("Old: ",Z1338ContratoServicosPrioridade_PercValorB);
                  GXUtil.WriteLogRaw("Current: ",T003L2_A1338ContratoServicosPrioridade_PercValorB[0]);
               }
               if ( Z1339ContratoServicosPrioridade_PercPrazo != T003L2_A1339ContratoServicosPrioridade_PercPrazo[0] )
               {
                  GXUtil.WriteLog("contratoservicosprioridade:[seudo value changed for attri]"+"ContratoServicosPrioridade_PercPrazo");
                  GXUtil.WriteLogRaw("Old: ",Z1339ContratoServicosPrioridade_PercPrazo);
                  GXUtil.WriteLogRaw("Current: ",T003L2_A1339ContratoServicosPrioridade_PercPrazo[0]);
               }
               if ( Z2067ContratoServicosPrioridade_Peso != T003L2_A2067ContratoServicosPrioridade_Peso[0] )
               {
                  GXUtil.WriteLog("contratoservicosprioridade:[seudo value changed for attri]"+"ContratoServicosPrioridade_Peso");
                  GXUtil.WriteLogRaw("Old: ",Z2067ContratoServicosPrioridade_Peso);
                  GXUtil.WriteLogRaw("Current: ",T003L2_A2067ContratoServicosPrioridade_Peso[0]);
               }
               if ( Z1335ContratoServicosPrioridade_CntSrvCod != T003L2_A1335ContratoServicosPrioridade_CntSrvCod[0] )
               {
                  GXUtil.WriteLog("contratoservicosprioridade:[seudo value changed for attri]"+"ContratoServicosPrioridade_CntSrvCod");
                  GXUtil.WriteLogRaw("Old: ",Z1335ContratoServicosPrioridade_CntSrvCod);
                  GXUtil.WriteLogRaw("Current: ",T003L2_A1335ContratoServicosPrioridade_CntSrvCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosPrioridade"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3L162( )
      {
         BeforeValidate3L162( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3L162( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3L162( 0) ;
            CheckOptimisticConcurrency3L162( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3L162( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3L162( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003L10 */
                     pr_default.execute(8, new Object[] {n2066ContratoServicosPrioridade_Ordem, A2066ContratoServicosPrioridade_Ordem, A1337ContratoServicosPrioridade_Nome, n1359ContratoServicosPrioridade_Finalidade, A1359ContratoServicosPrioridade_Finalidade, n1338ContratoServicosPrioridade_PercValorB, A1338ContratoServicosPrioridade_PercValorB, n1339ContratoServicosPrioridade_PercPrazo, A1339ContratoServicosPrioridade_PercPrazo, n2067ContratoServicosPrioridade_Peso, A2067ContratoServicosPrioridade_Peso, A1335ContratoServicosPrioridade_CntSrvCod});
                     A1336ContratoServicosPrioridade_Codigo = T003L10_A1336ContratoServicosPrioridade_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3L0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3L162( ) ;
            }
            EndLevel3L162( ) ;
         }
         CloseExtendedTableCursors3L162( ) ;
      }

      protected void Update3L162( )
      {
         BeforeValidate3L162( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3L162( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3L162( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3L162( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3L162( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003L11 */
                     pr_default.execute(9, new Object[] {n2066ContratoServicosPrioridade_Ordem, A2066ContratoServicosPrioridade_Ordem, A1337ContratoServicosPrioridade_Nome, n1359ContratoServicosPrioridade_Finalidade, A1359ContratoServicosPrioridade_Finalidade, n1338ContratoServicosPrioridade_PercValorB, A1338ContratoServicosPrioridade_PercValorB, n1339ContratoServicosPrioridade_PercPrazo, A1339ContratoServicosPrioridade_PercPrazo, n2067ContratoServicosPrioridade_Peso, A2067ContratoServicosPrioridade_Peso, A1335ContratoServicosPrioridade_CntSrvCod, A1336ContratoServicosPrioridade_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosPrioridade"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3L162( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3L162( ) ;
         }
         CloseExtendedTableCursors3L162( ) ;
      }

      protected void DeferredUpdate3L162( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3L162( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3L162( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3L162( ) ;
            AfterConfirm3L162( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3L162( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003L12 */
                  pr_default.execute(10, new Object[] {A1336ContratoServicosPrioridade_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode162 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3L162( ) ;
         Gx_mode = sMode162;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3L162( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel3L162( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3L162( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContratoServicosPrioridade");
            if ( AnyError == 0 )
            {
               ConfirmValues3L0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContratoServicosPrioridade");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3L162( )
      {
         /* Scan By routine */
         /* Using cursor T003L13 */
         pr_default.execute(11);
         RcdFound162 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound162 = 1;
            A1336ContratoServicosPrioridade_Codigo = T003L13_A1336ContratoServicosPrioridade_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3L162( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound162 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound162 = 1;
            A1336ContratoServicosPrioridade_Codigo = T003L13_A1336ContratoServicosPrioridade_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3L162( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm3L162( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3L162( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3L162( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3L162( )
      {
         /* Before Delete Rules */
         new prc_dltcntsrvprioridade(context ).execute(  A1335ContratoServicosPrioridade_CntSrvCod,  A2066ContratoServicosPrioridade_Ordem) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
      }

      protected void BeforeComplete3L162( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3L162( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3L162( )
      {
         edtContratoServicosPrioridade_Ordem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Ordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_Ordem_Enabled), 5, 0)));
         edtContratoServicosPrioridade_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_Nome_Enabled), 5, 0)));
         edtContratoServicosPrioridade_PercValorB_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_PercValorB_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_PercValorB_Enabled), 5, 0)));
         edtContratoServicosPrioridade_PercPrazo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_PercPrazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_PercPrazo_Enabled), 5, 0)));
         edtContratoServicosPrioridade_Finalidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Finalidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_Finalidade_Enabled), 5, 0)));
         edtContratoServicosPrioridade_Peso_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Peso_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_Peso_Enabled), 5, 0)));
         edtContratoServicosPrioridade_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_Codigo_Enabled), 5, 0)));
         edtContratoServicosPrioridade_CntSrvCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrioridade_CntSrvCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3L0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299312634");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicosPrioridade_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1336ContratoServicosPrioridade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2066ContratoServicosPrioridade_Ordem), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1337ContratoServicosPrioridade_Nome", StringUtil.RTrim( Z1337ContratoServicosPrioridade_Nome));
         GxWebStd.gx_hidden_field( context, "Z1338ContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.NToC( Z1338ContratoServicosPrioridade_PercValorB, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1339ContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.NToC( Z1339ContratoServicosPrioridade_PercPrazo, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2067ContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2067ContratoServicosPrioridade_Peso), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1335ContratoServicosPrioridade_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOSPRIORIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosPrioridade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ContratoServicosPrioridade_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOSPRIORIDADE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosPrioridade_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoServicosPrioridade";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1336ContratoServicosPrioridade_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicosprioridade:[SendSecurityCheck value for]"+"ContratoServicosPrioridade_Ordem:"+context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9"));
         GXUtil.WriteLog("contratoservicosprioridade:[SendSecurityCheck value for]"+"ContratoServicosPrioridade_Codigo:"+context.localUtil.Format( (decimal)(A1336ContratoServicosPrioridade_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contratoservicosprioridade:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoservicosprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicosPrioridade_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosPrioridade" ;
      }

      public override String GetPgmdesc( )
      {
         return "Prioridade" ;
      }

      protected void InitializeNonKey3L162( )
      {
         A1335ContratoServicosPrioridade_CntSrvCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
         A2066ContratoServicosPrioridade_Ordem = 0;
         n2066ContratoServicosPrioridade_Ordem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
         n2066ContratoServicosPrioridade_Ordem = ((0==A2066ContratoServicosPrioridade_Ordem) ? true : false);
         A1337ContratoServicosPrioridade_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1337ContratoServicosPrioridade_Nome", A1337ContratoServicosPrioridade_Nome);
         A1359ContratoServicosPrioridade_Finalidade = "";
         n1359ContratoServicosPrioridade_Finalidade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1359ContratoServicosPrioridade_Finalidade", A1359ContratoServicosPrioridade_Finalidade);
         n1359ContratoServicosPrioridade_Finalidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1359ContratoServicosPrioridade_Finalidade)) ? true : false);
         A1338ContratoServicosPrioridade_PercValorB = 0;
         n1338ContratoServicosPrioridade_PercValorB = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1338ContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( A1338ContratoServicosPrioridade_PercValorB, 6, 2)));
         n1338ContratoServicosPrioridade_PercValorB = ((Convert.ToDecimal(0)==A1338ContratoServicosPrioridade_PercValorB) ? true : false);
         A1339ContratoServicosPrioridade_PercPrazo = 0;
         n1339ContratoServicosPrioridade_PercPrazo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1339ContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( A1339ContratoServicosPrioridade_PercPrazo, 6, 2)));
         n1339ContratoServicosPrioridade_PercPrazo = ((Convert.ToDecimal(0)==A1339ContratoServicosPrioridade_PercPrazo) ? true : false);
         A2067ContratoServicosPrioridade_Peso = 0;
         n2067ContratoServicosPrioridade_Peso = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2067ContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0)));
         n2067ContratoServicosPrioridade_Peso = ((0==A2067ContratoServicosPrioridade_Peso) ? true : false);
         Z2066ContratoServicosPrioridade_Ordem = 0;
         Z1337ContratoServicosPrioridade_Nome = "";
         Z1338ContratoServicosPrioridade_PercValorB = 0;
         Z1339ContratoServicosPrioridade_PercPrazo = 0;
         Z2067ContratoServicosPrioridade_Peso = 0;
         Z1335ContratoServicosPrioridade_CntSrvCod = 0;
      }

      protected void InitAll3L162( )
      {
         A1336ContratoServicosPrioridade_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1336ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)));
         InitializeNonKey3L162( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2066ContratoServicosPrioridade_Ordem = i2066ContratoServicosPrioridade_Ordem;
         n2066ContratoServicosPrioridade_Ordem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299312653");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoservicosprioridade.js", "?20205299312653");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratoservicosprioridade_ordem_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_ORDEM";
         edtContratoServicosPrioridade_Ordem_Internalname = "CONTRATOSERVICOSPRIORIDADE_ORDEM";
         lblTextblockcontratoservicosprioridade_nome_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_NOME";
         edtContratoServicosPrioridade_Nome_Internalname = "CONTRATOSERVICOSPRIORIDADE_NOME";
         lblTextblockcontratoservicosprioridade_percvalorb_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_PERCVALORB";
         edtContratoServicosPrioridade_PercValorB_Internalname = "CONTRATOSERVICOSPRIORIDADE_PERCVALORB";
         lblTextblockcontratoservicosprioridade_percprazo_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_PERCPRAZO";
         edtContratoServicosPrioridade_PercPrazo_Internalname = "CONTRATOSERVICOSPRIORIDADE_PERCPRAZO";
         lblTextblockcontratoservicosprioridade_finalidade_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_FINALIDADE";
         edtContratoServicosPrioridade_Finalidade_Internalname = "CONTRATOSERVICOSPRIORIDADE_FINALIDADE";
         lblTextblockcontratoservicosprioridade_peso_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_PESO";
         edtContratoServicosPrioridade_Peso_Internalname = "CONTRATOSERVICOSPRIORIDADE_PESO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratoServicosPrioridade_Codigo_Internalname = "CONTRATOSERVICOSPRIORIDADE_CODIGO";
         edtContratoServicosPrioridade_CntSrvCod_Internalname = "CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Prioridade";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Prioridade";
         edtContratoServicosPrioridade_Peso_Jsonclick = "";
         edtContratoServicosPrioridade_Peso_Enabled = 1;
         edtContratoServicosPrioridade_Finalidade_Enabled = 1;
         edtContratoServicosPrioridade_PercPrazo_Jsonclick = "";
         edtContratoServicosPrioridade_PercPrazo_Enabled = 1;
         edtContratoServicosPrioridade_PercValorB_Jsonclick = "";
         edtContratoServicosPrioridade_PercValorB_Enabled = 1;
         edtContratoServicosPrioridade_Nome_Jsonclick = "";
         edtContratoServicosPrioridade_Nome_Enabled = 1;
         edtContratoServicosPrioridade_Ordem_Jsonclick = "";
         edtContratoServicosPrioridade_Ordem_Enabled = 0;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContratoServicosPrioridade_CntSrvCod_Jsonclick = "";
         edtContratoServicosPrioridade_CntSrvCod_Enabled = 1;
         edtContratoServicosPrioridade_CntSrvCod_Visible = 1;
         edtContratoServicosPrioridade_Codigo_Jsonclick = "";
         edtContratoServicosPrioridade_Codigo_Enabled = 0;
         edtContratoServicosPrioridade_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GX8ASACONTRATOSERVICOSPRIORIDADE_ORDEM3L162( String Gx_mode ,
                                                                  int AV11Insert_ContratoServicosPrioridade_CntSrvCod )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            GXt_int1 = A2066ContratoServicosPrioridade_Ordem;
            new prc_maxprioridadeordem(context ).execute( ref  AV11Insert_ContratoServicosPrioridade_CntSrvCod, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContratoServicosPrioridade_CntSrvCod), 6, 0)));
            A2066ContratoServicosPrioridade_Ordem = (short)(GXt_int1+1);
            n2066ContratoServicosPrioridade_Ordem = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_10_3L162( int A1335ContratoServicosPrioridade_CntSrvCod ,
                                  short A2066ContratoServicosPrioridade_Ordem )
      {
         new prc_dltcntsrvprioridade(context ).execute(  A1335ContratoServicosPrioridade_CntSrvCod,  A2066ContratoServicosPrioridade_Ordem) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1335ContratoServicosPrioridade_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2066ContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Contratoservicosprioridade_cntsrvcod( int GX_Parm1 )
      {
         A1335ContratoServicosPrioridade_CntSrvCod = GX_Parm1;
         /* Using cursor T003L14 */
         pr_default.execute(12, new Object[] {A1335ContratoServicosPrioridade_CntSrvCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Prioridades_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrioridade_CntSrvCod_Internalname;
         }
         pr_default.close(12);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoServicosPrioridade_Codigo',fld:'vCONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123L2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1337ContratoServicosPrioridade_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblockcontratoservicosprioridade_ordem_Jsonclick = "";
         lblTextblockcontratoservicosprioridade_nome_Jsonclick = "";
         A1337ContratoServicosPrioridade_Nome = "";
         lblTextblockcontratoservicosprioridade_percvalorb_Jsonclick = "";
         lblTextblockcontratoservicosprioridade_percprazo_Jsonclick = "";
         lblTextblockcontratoservicosprioridade_finalidade_Jsonclick = "";
         A1359ContratoServicosPrioridade_Finalidade = "";
         lblTextblockcontratoservicosprioridade_peso_Jsonclick = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode162 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1359ContratoServicosPrioridade_Finalidade = "";
         T003L5_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         T003L5_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         T003L5_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         T003L5_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         T003L5_A1359ContratoServicosPrioridade_Finalidade = new String[] {""} ;
         T003L5_n1359ContratoServicosPrioridade_Finalidade = new bool[] {false} ;
         T003L5_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         T003L5_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         T003L5_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         T003L5_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         T003L5_A2067ContratoServicosPrioridade_Peso = new short[1] ;
         T003L5_n2067ContratoServicosPrioridade_Peso = new bool[] {false} ;
         T003L5_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         T003L4_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         T003L6_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         T003L7_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         T003L3_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         T003L3_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         T003L3_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         T003L3_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         T003L3_A1359ContratoServicosPrioridade_Finalidade = new String[] {""} ;
         T003L3_n1359ContratoServicosPrioridade_Finalidade = new bool[] {false} ;
         T003L3_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         T003L3_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         T003L3_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         T003L3_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         T003L3_A2067ContratoServicosPrioridade_Peso = new short[1] ;
         T003L3_n2067ContratoServicosPrioridade_Peso = new bool[] {false} ;
         T003L3_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         T003L8_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         T003L9_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         T003L2_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         T003L2_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         T003L2_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         T003L2_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         T003L2_A1359ContratoServicosPrioridade_Finalidade = new String[] {""} ;
         T003L2_n1359ContratoServicosPrioridade_Finalidade = new bool[] {false} ;
         T003L2_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         T003L2_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         T003L2_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         T003L2_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         T003L2_A2067ContratoServicosPrioridade_Peso = new short[1] ;
         T003L2_n2067ContratoServicosPrioridade_Peso = new bool[] {false} ;
         T003L2_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         T003L10_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         T003L13_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T003L14_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosprioridade__default(),
            new Object[][] {
                new Object[] {
               T003L2_A1336ContratoServicosPrioridade_Codigo, T003L2_A2066ContratoServicosPrioridade_Ordem, T003L2_n2066ContratoServicosPrioridade_Ordem, T003L2_A1337ContratoServicosPrioridade_Nome, T003L2_A1359ContratoServicosPrioridade_Finalidade, T003L2_n1359ContratoServicosPrioridade_Finalidade, T003L2_A1338ContratoServicosPrioridade_PercValorB, T003L2_n1338ContratoServicosPrioridade_PercValorB, T003L2_A1339ContratoServicosPrioridade_PercPrazo, T003L2_n1339ContratoServicosPrioridade_PercPrazo,
               T003L2_A2067ContratoServicosPrioridade_Peso, T003L2_n2067ContratoServicosPrioridade_Peso, T003L2_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               T003L3_A1336ContratoServicosPrioridade_Codigo, T003L3_A2066ContratoServicosPrioridade_Ordem, T003L3_n2066ContratoServicosPrioridade_Ordem, T003L3_A1337ContratoServicosPrioridade_Nome, T003L3_A1359ContratoServicosPrioridade_Finalidade, T003L3_n1359ContratoServicosPrioridade_Finalidade, T003L3_A1338ContratoServicosPrioridade_PercValorB, T003L3_n1338ContratoServicosPrioridade_PercValorB, T003L3_A1339ContratoServicosPrioridade_PercPrazo, T003L3_n1339ContratoServicosPrioridade_PercPrazo,
               T003L3_A2067ContratoServicosPrioridade_Peso, T003L3_n2067ContratoServicosPrioridade_Peso, T003L3_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               T003L4_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               T003L5_A1336ContratoServicosPrioridade_Codigo, T003L5_A2066ContratoServicosPrioridade_Ordem, T003L5_n2066ContratoServicosPrioridade_Ordem, T003L5_A1337ContratoServicosPrioridade_Nome, T003L5_A1359ContratoServicosPrioridade_Finalidade, T003L5_n1359ContratoServicosPrioridade_Finalidade, T003L5_A1338ContratoServicosPrioridade_PercValorB, T003L5_n1338ContratoServicosPrioridade_PercValorB, T003L5_A1339ContratoServicosPrioridade_PercPrazo, T003L5_n1339ContratoServicosPrioridade_PercPrazo,
               T003L5_A2067ContratoServicosPrioridade_Peso, T003L5_n2067ContratoServicosPrioridade_Peso, T003L5_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               T003L6_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               T003L7_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               T003L8_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               T003L9_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               T003L10_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003L13_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               T003L14_A1335ContratoServicosPrioridade_CntSrvCod
               }
            }
         );
         AV13Pgmname = "ContratoServicosPrioridade";
      }

      private short Z2066ContratoServicosPrioridade_Ordem ;
      private short Z2067ContratoServicosPrioridade_Peso ;
      private short GxWebError ;
      private short A2066ContratoServicosPrioridade_Ordem ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A2067ContratoServicosPrioridade_Peso ;
      private short RcdFound162 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short i2066ContratoServicosPrioridade_Ordem ;
      private short GXt_int1 ;
      private short wbTemp ;
      private int wcpOAV7ContratoServicosPrioridade_Codigo ;
      private int Z1336ContratoServicosPrioridade_Codigo ;
      private int Z1335ContratoServicosPrioridade_CntSrvCod ;
      private int N1335ContratoServicosPrioridade_CntSrvCod ;
      private int A1335ContratoServicosPrioridade_CntSrvCod ;
      private int AV11Insert_ContratoServicosPrioridade_CntSrvCod ;
      private int AV7ContratoServicosPrioridade_Codigo ;
      private int trnEnded ;
      private int A1336ContratoServicosPrioridade_Codigo ;
      private int edtContratoServicosPrioridade_Codigo_Enabled ;
      private int edtContratoServicosPrioridade_Codigo_Visible ;
      private int edtContratoServicosPrioridade_CntSrvCod_Visible ;
      private int edtContratoServicosPrioridade_CntSrvCod_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtContratoServicosPrioridade_Ordem_Enabled ;
      private int edtContratoServicosPrioridade_Nome_Enabled ;
      private int edtContratoServicosPrioridade_PercValorB_Enabled ;
      private int edtContratoServicosPrioridade_PercPrazo_Enabled ;
      private int edtContratoServicosPrioridade_Finalidade_Enabled ;
      private int edtContratoServicosPrioridade_Peso_Enabled ;
      private int AV14GXV1 ;
      private int idxLst ;
      private decimal Z1338ContratoServicosPrioridade_PercValorB ;
      private decimal Z1339ContratoServicosPrioridade_PercPrazo ;
      private decimal A1338ContratoServicosPrioridade_PercValorB ;
      private decimal A1339ContratoServicosPrioridade_PercPrazo ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1337ContratoServicosPrioridade_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoServicosPrioridade_Nome_Internalname ;
      private String edtContratoServicosPrioridade_Codigo_Internalname ;
      private String edtContratoServicosPrioridade_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtContratoServicosPrioridade_CntSrvCod_Internalname ;
      private String edtContratoServicosPrioridade_CntSrvCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratoservicosprioridade_ordem_Internalname ;
      private String lblTextblockcontratoservicosprioridade_ordem_Jsonclick ;
      private String edtContratoServicosPrioridade_Ordem_Internalname ;
      private String edtContratoServicosPrioridade_Ordem_Jsonclick ;
      private String lblTextblockcontratoservicosprioridade_nome_Internalname ;
      private String lblTextblockcontratoservicosprioridade_nome_Jsonclick ;
      private String A1337ContratoServicosPrioridade_Nome ;
      private String edtContratoServicosPrioridade_Nome_Jsonclick ;
      private String lblTextblockcontratoservicosprioridade_percvalorb_Internalname ;
      private String lblTextblockcontratoservicosprioridade_percvalorb_Jsonclick ;
      private String edtContratoServicosPrioridade_PercValorB_Internalname ;
      private String edtContratoServicosPrioridade_PercValorB_Jsonclick ;
      private String lblTextblockcontratoservicosprioridade_percprazo_Internalname ;
      private String lblTextblockcontratoservicosprioridade_percprazo_Jsonclick ;
      private String edtContratoServicosPrioridade_PercPrazo_Internalname ;
      private String edtContratoServicosPrioridade_PercPrazo_Jsonclick ;
      private String lblTextblockcontratoservicosprioridade_finalidade_Internalname ;
      private String lblTextblockcontratoservicosprioridade_finalidade_Jsonclick ;
      private String edtContratoServicosPrioridade_Finalidade_Internalname ;
      private String lblTextblockcontratoservicosprioridade_peso_Internalname ;
      private String lblTextblockcontratoservicosprioridade_peso_Jsonclick ;
      private String edtContratoServicosPrioridade_Peso_Internalname ;
      private String edtContratoServicosPrioridade_Peso_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode162 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool n2066ContratoServicosPrioridade_Ordem ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1338ContratoServicosPrioridade_PercValorB ;
      private bool n1339ContratoServicosPrioridade_PercPrazo ;
      private bool n1359ContratoServicosPrioridade_Finalidade ;
      private bool n2067ContratoServicosPrioridade_Peso ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A1359ContratoServicosPrioridade_Finalidade ;
      private String Z1359ContratoServicosPrioridade_Finalidade ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T003L5_A1336ContratoServicosPrioridade_Codigo ;
      private short[] T003L5_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] T003L5_n2066ContratoServicosPrioridade_Ordem ;
      private String[] T003L5_A1337ContratoServicosPrioridade_Nome ;
      private String[] T003L5_A1359ContratoServicosPrioridade_Finalidade ;
      private bool[] T003L5_n1359ContratoServicosPrioridade_Finalidade ;
      private decimal[] T003L5_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] T003L5_n1338ContratoServicosPrioridade_PercValorB ;
      private decimal[] T003L5_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] T003L5_n1339ContratoServicosPrioridade_PercPrazo ;
      private short[] T003L5_A2067ContratoServicosPrioridade_Peso ;
      private bool[] T003L5_n2067ContratoServicosPrioridade_Peso ;
      private int[] T003L5_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] T003L4_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] T003L6_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] T003L7_A1336ContratoServicosPrioridade_Codigo ;
      private int[] T003L3_A1336ContratoServicosPrioridade_Codigo ;
      private short[] T003L3_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] T003L3_n2066ContratoServicosPrioridade_Ordem ;
      private String[] T003L3_A1337ContratoServicosPrioridade_Nome ;
      private String[] T003L3_A1359ContratoServicosPrioridade_Finalidade ;
      private bool[] T003L3_n1359ContratoServicosPrioridade_Finalidade ;
      private decimal[] T003L3_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] T003L3_n1338ContratoServicosPrioridade_PercValorB ;
      private decimal[] T003L3_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] T003L3_n1339ContratoServicosPrioridade_PercPrazo ;
      private short[] T003L3_A2067ContratoServicosPrioridade_Peso ;
      private bool[] T003L3_n2067ContratoServicosPrioridade_Peso ;
      private int[] T003L3_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] T003L8_A1336ContratoServicosPrioridade_Codigo ;
      private int[] T003L9_A1336ContratoServicosPrioridade_Codigo ;
      private int[] T003L2_A1336ContratoServicosPrioridade_Codigo ;
      private short[] T003L2_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] T003L2_n2066ContratoServicosPrioridade_Ordem ;
      private String[] T003L2_A1337ContratoServicosPrioridade_Nome ;
      private String[] T003L2_A1359ContratoServicosPrioridade_Finalidade ;
      private bool[] T003L2_n1359ContratoServicosPrioridade_Finalidade ;
      private decimal[] T003L2_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] T003L2_n1338ContratoServicosPrioridade_PercValorB ;
      private decimal[] T003L2_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] T003L2_n1339ContratoServicosPrioridade_PercPrazo ;
      private short[] T003L2_A2067ContratoServicosPrioridade_Peso ;
      private bool[] T003L2_n2067ContratoServicosPrioridade_Peso ;
      private int[] T003L2_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] T003L10_A1336ContratoServicosPrioridade_Codigo ;
      private int[] T003L13_A1336ContratoServicosPrioridade_Codigo ;
      private int[] T003L14_A1335ContratoServicosPrioridade_CntSrvCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class contratoservicosprioridade__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003L5 ;
          prmT003L5 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003L4 ;
          prmT003L4 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003L6 ;
          prmT003L6 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003L7 ;
          prmT003L7 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003L3 ;
          prmT003L3 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003L8 ;
          prmT003L8 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003L9 ;
          prmT003L9 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003L2 ;
          prmT003L2 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003L10 ;
          prmT003L10 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrioridade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@ContratoServicosPrioridade_Finalidade",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosPrioridade_PercValorB",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosPrioridade_PercPrazo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosPrioridade_Peso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosPrioridade_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003L11 ;
          prmT003L11 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrioridade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@ContratoServicosPrioridade_Finalidade",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosPrioridade_PercValorB",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosPrioridade_PercPrazo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosPrioridade_Peso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosPrioridade_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003L12 ;
          prmT003L12 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003L13 ;
          prmT003L13 = new Object[] {
          } ;
          Object[] prmT003L14 ;
          prmT003L14 = new Object[] {
          new Object[] {"@ContratoServicosPrioridade_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003L2", "SELECT [ContratoServicosPrioridade_Codigo], [ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Finalidade], [ContratoServicosPrioridade_PercValorB], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_Peso], [ContratoServicosPrioridade_CntSrvCod] AS ContratoServicosPrioridade_CntSrvCod FROM [ContratoServicosPrioridade] WITH (UPDLOCK) WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003L2,1,0,true,false )
             ,new CursorDef("T003L3", "SELECT [ContratoServicosPrioridade_Codigo], [ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Finalidade], [ContratoServicosPrioridade_PercValorB], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_Peso], [ContratoServicosPrioridade_CntSrvCod] AS ContratoServicosPrioridade_CntSrvCod FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003L3,1,0,true,false )
             ,new CursorDef("T003L4", "SELECT [ContratoServicos_Codigo] AS ContratoServicosPrioridade_CntSrvCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosPrioridade_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003L4,1,0,true,false )
             ,new CursorDef("T003L5", "SELECT TM1.[ContratoServicosPrioridade_Codigo], TM1.[ContratoServicosPrioridade_Ordem], TM1.[ContratoServicosPrioridade_Nome], TM1.[ContratoServicosPrioridade_Finalidade], TM1.[ContratoServicosPrioridade_PercValorB], TM1.[ContratoServicosPrioridade_PercPrazo], TM1.[ContratoServicosPrioridade_Peso], TM1.[ContratoServicosPrioridade_CntSrvCod] AS ContratoServicosPrioridade_CntSrvCod FROM [ContratoServicosPrioridade] TM1 WITH (NOLOCK) WHERE TM1.[ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo ORDER BY TM1.[ContratoServicosPrioridade_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003L5,100,0,true,false )
             ,new CursorDef("T003L6", "SELECT [ContratoServicos_Codigo] AS ContratoServicosPrioridade_CntSrvCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosPrioridade_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003L6,1,0,true,false )
             ,new CursorDef("T003L7", "SELECT [ContratoServicosPrioridade_Codigo] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003L7,1,0,true,false )
             ,new CursorDef("T003L8", "SELECT TOP 1 [ContratoServicosPrioridade_Codigo] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE ( [ContratoServicosPrioridade_Codigo] > @ContratoServicosPrioridade_Codigo) ORDER BY [ContratoServicosPrioridade_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003L8,1,0,true,true )
             ,new CursorDef("T003L9", "SELECT TOP 1 [ContratoServicosPrioridade_Codigo] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE ( [ContratoServicosPrioridade_Codigo] < @ContratoServicosPrioridade_Codigo) ORDER BY [ContratoServicosPrioridade_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003L9,1,0,true,true )
             ,new CursorDef("T003L10", "INSERT INTO [ContratoServicosPrioridade]([ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Finalidade], [ContratoServicosPrioridade_PercValorB], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_Peso], [ContratoServicosPrioridade_CntSrvCod]) VALUES(@ContratoServicosPrioridade_Ordem, @ContratoServicosPrioridade_Nome, @ContratoServicosPrioridade_Finalidade, @ContratoServicosPrioridade_PercValorB, @ContratoServicosPrioridade_PercPrazo, @ContratoServicosPrioridade_Peso, @ContratoServicosPrioridade_CntSrvCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003L10)
             ,new CursorDef("T003L11", "UPDATE [ContratoServicosPrioridade] SET [ContratoServicosPrioridade_Ordem]=@ContratoServicosPrioridade_Ordem, [ContratoServicosPrioridade_Nome]=@ContratoServicosPrioridade_Nome, [ContratoServicosPrioridade_Finalidade]=@ContratoServicosPrioridade_Finalidade, [ContratoServicosPrioridade_PercValorB]=@ContratoServicosPrioridade_PercValorB, [ContratoServicosPrioridade_PercPrazo]=@ContratoServicosPrioridade_PercPrazo, [ContratoServicosPrioridade_Peso]=@ContratoServicosPrioridade_Peso, [ContratoServicosPrioridade_CntSrvCod]=@ContratoServicosPrioridade_CntSrvCod  WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo", GxErrorMask.GX_NOMASK,prmT003L11)
             ,new CursorDef("T003L12", "DELETE FROM [ContratoServicosPrioridade]  WHERE [ContratoServicosPrioridade_Codigo] = @ContratoServicosPrioridade_Codigo", GxErrorMask.GX_NOMASK,prmT003L12)
             ,new CursorDef("T003L13", "SELECT [ContratoServicosPrioridade_Codigo] FROM [ContratoServicosPrioridade] WITH (NOLOCK) ORDER BY [ContratoServicosPrioridade_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003L13,100,0,true,false )
             ,new CursorDef("T003L14", "SELECT [ContratoServicos_Codigo] AS ContratoServicosPrioridade_CntSrvCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosPrioridade_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003L14,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
