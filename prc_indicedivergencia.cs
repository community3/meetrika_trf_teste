/*
               File: PRC_IndiceDivergencia
        Description: IIndice Divergencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:42.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_indicedivergencia : GXProcedure
   {
      public prc_indicedivergencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_indicedivergencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           out decimal aP1_Contrato_IndiceDivergencia )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8Contrato_IndiceDivergencia = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_Contrato_IndiceDivergencia=this.AV8Contrato_IndiceDivergencia;
      }

      public decimal executeUdp( ref int aP0_ContratoServicos_Codigo )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8Contrato_IndiceDivergencia = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_Contrato_IndiceDivergencia=this.AV8Contrato_IndiceDivergencia;
         return AV8Contrato_IndiceDivergencia ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 out decimal aP1_Contrato_IndiceDivergencia )
      {
         prc_indicedivergencia objprc_indicedivergencia;
         objprc_indicedivergencia = new prc_indicedivergencia();
         objprc_indicedivergencia.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_indicedivergencia.AV8Contrato_IndiceDivergencia = 0 ;
         objprc_indicedivergencia.context.SetSubmitInitialConfig(context);
         objprc_indicedivergencia.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_indicedivergencia);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_Contrato_IndiceDivergencia=this.AV8Contrato_IndiceDivergencia;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_indicedivergencia)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P007G2 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P007G2_A74Contrato_Codigo[0];
            A1455ContratoServicos_IndiceDivergencia = P007G2_A1455ContratoServicos_IndiceDivergencia[0];
            n1455ContratoServicos_IndiceDivergencia = P007G2_n1455ContratoServicos_IndiceDivergencia[0];
            A453Contrato_IndiceDivergencia = P007G2_A453Contrato_IndiceDivergencia[0];
            A453Contrato_IndiceDivergencia = P007G2_A453Contrato_IndiceDivergencia[0];
            OV8Contrato_IndiceDivergencia = AV8Contrato_IndiceDivergencia;
            if ( ( A1455ContratoServicos_IndiceDivergencia > Convert.ToDecimal( 0 )) )
            {
               AV8Contrato_IndiceDivergencia = A1455ContratoServicos_IndiceDivergencia;
            }
            else
            {
               AV8Contrato_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P007G2_A74Contrato_Codigo = new int[1] ;
         P007G2_A160ContratoServicos_Codigo = new int[1] ;
         P007G2_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         P007G2_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         P007G2_A453Contrato_IndiceDivergencia = new decimal[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_indicedivergencia__default(),
            new Object[][] {
                new Object[] {
               P007G2_A74Contrato_Codigo, P007G2_A160ContratoServicos_Codigo, P007G2_A1455ContratoServicos_IndiceDivergencia, P007G2_n1455ContratoServicos_IndiceDivergencia, P007G2_A453Contrato_IndiceDivergencia
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A160ContratoServicos_Codigo ;
      private int A74Contrato_Codigo ;
      private decimal AV8Contrato_IndiceDivergencia ;
      private decimal A1455ContratoServicos_IndiceDivergencia ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal OV8Contrato_IndiceDivergencia ;
      private String scmdbuf ;
      private bool n1455ContratoServicos_IndiceDivergencia ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P007G2_A74Contrato_Codigo ;
      private int[] P007G2_A160ContratoServicos_Codigo ;
      private decimal[] P007G2_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] P007G2_n1455ContratoServicos_IndiceDivergencia ;
      private decimal[] P007G2_A453Contrato_IndiceDivergencia ;
      private decimal aP1_Contrato_IndiceDivergencia ;
   }

   public class prc_indicedivergencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007G2 ;
          prmP007G2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007G2", "SELECT TOP 1 T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[ContratoServicos_IndiceDivergencia], T2.[Contrato_IndiceDivergencia] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007G2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
