/*
               File: PromptNaoConformidade
        Description: Selecione N�o Conformidade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:18:42.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptnaoconformidade : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptnaoconformidade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptnaoconformidade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutNaoConformidade_Codigo ,
                           ref String aP1_InOutNaoConformidade_Nome )
      {
         this.AV7InOutNaoConformidade_Codigo = aP0_InOutNaoConformidade_Codigo;
         this.AV8InOutNaoConformidade_Nome = aP1_InOutNaoConformidade_Nome;
         executePrivate();
         aP0_InOutNaoConformidade_Codigo=this.AV7InOutNaoConformidade_Codigo;
         aP1_InOutNaoConformidade_Nome=this.AV8InOutNaoConformidade_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavNaoconformidade_tipo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavNaoconformidade_tipo2 = new GXCombobox();
         cmbNaoConformidade_Tipo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_59 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_59_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_59_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV17NaoConformidade_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NaoConformidade_Nome1", AV17NaoConformidade_Nome1);
               AV18NaoConformidade_Tipo1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21NaoConformidade_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NaoConformidade_Nome2", AV21NaoConformidade_Nome2);
               AV22NaoConformidade_Tipo2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV28TFNaoConformidade_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFNaoConformidade_Nome", AV28TFNaoConformidade_Nome);
               AV29TFNaoConformidade_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFNaoConformidade_Nome_Sel", AV29TFNaoConformidade_Nome_Sel);
               AV30ddo_NaoConformidade_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_NaoConformidade_NomeTitleControlIdToReplace", AV30ddo_NaoConformidade_NomeTitleControlIdToReplace);
               AV34ddo_NaoConformidade_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_NaoConformidade_TipoTitleControlIdToReplace", AV34ddo_NaoConformidade_TipoTitleControlIdToReplace);
               AV15NaoConformidade_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV33TFNaoConformidade_Tipo_Sels);
               AV42Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV28TFNaoConformidade_Nome, AV29TFNaoConformidade_Nome_Sel, AV30ddo_NaoConformidade_NomeTitleControlIdToReplace, AV34ddo_NaoConformidade_TipoTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV33TFNaoConformidade_Tipo_Sels, AV42Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutNaoConformidade_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutNaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutNaoConformidade_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutNaoConformidade_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutNaoConformidade_Nome", AV8InOutNaoConformidade_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAAM2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV42Pgmname = "PromptNaoConformidade";
               context.Gx_err = 0;
               WSAM2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEAM2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823184232");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptnaoconformidade.aspx") + "?" + UrlEncode("" +AV7InOutNaoConformidade_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutNaoConformidade_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vNAOCONFORMIDADE_NOME1", StringUtil.RTrim( AV17NaoConformidade_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vNAOCONFORMIDADE_TIPO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18NaoConformidade_Tipo1), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vNAOCONFORMIDADE_NOME2", StringUtil.RTrim( AV21NaoConformidade_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vNAOCONFORMIDADE_TIPO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22NaoConformidade_Tipo2), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNAOCONFORMIDADE_NOME", StringUtil.RTrim( AV28TFNaoConformidade_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNAOCONFORMIDADE_NOME_SEL", StringUtil.RTrim( AV29TFNaoConformidade_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_59", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_59), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV35DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV35DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNAOCONFORMIDADE_NOMETITLEFILTERDATA", AV27NaoConformidade_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNAOCONFORMIDADE_NOMETITLEFILTERDATA", AV27NaoConformidade_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNAOCONFORMIDADE_TIPOTITLEFILTERDATA", AV31NaoConformidade_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNAOCONFORMIDADE_TIPOTITLEFILTERDATA", AV31NaoConformidade_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFNAOCONFORMIDADE_TIPO_SELS", AV33TFNaoConformidade_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFNAOCONFORMIDADE_TIPO_SELS", AV33TFNaoConformidade_Tipo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV42Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTNAOCONFORMIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutNaoConformidade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTNAOCONFORMIDADE_NOME", StringUtil.RTrim( AV8InOutNaoConformidade_Nome));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Caption", StringUtil.RTrim( Ddo_naoconformidade_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Tooltip", StringUtil.RTrim( Ddo_naoconformidade_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Cls", StringUtil.RTrim( Ddo_naoconformidade_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_naoconformidade_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_naoconformidade_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_naoconformidade_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_naoconformidade_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_naoconformidade_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_naoconformidade_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Sortedstatus", StringUtil.RTrim( Ddo_naoconformidade_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Includefilter", StringUtil.BoolToStr( Ddo_naoconformidade_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Filtertype", StringUtil.RTrim( Ddo_naoconformidade_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_naoconformidade_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_naoconformidade_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Datalisttype", StringUtil.RTrim( Ddo_naoconformidade_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_naoconformidade_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Datalistproc", StringUtil.RTrim( Ddo_naoconformidade_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_naoconformidade_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Sortasc", StringUtil.RTrim( Ddo_naoconformidade_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Sortdsc", StringUtil.RTrim( Ddo_naoconformidade_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Loadingdata", StringUtil.RTrim( Ddo_naoconformidade_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Cleanfilter", StringUtil.RTrim( Ddo_naoconformidade_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_naoconformidade_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Rangefilterto", StringUtil.RTrim( Ddo_naoconformidade_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Noresultsfound", StringUtil.RTrim( Ddo_naoconformidade_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_naoconformidade_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Caption", StringUtil.RTrim( Ddo_naoconformidade_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Tooltip", StringUtil.RTrim( Ddo_naoconformidade_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Cls", StringUtil.RTrim( Ddo_naoconformidade_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_naoconformidade_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_naoconformidade_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_naoconformidade_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_naoconformidade_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_naoconformidade_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_naoconformidade_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_naoconformidade_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Filterisrange", StringUtil.BoolToStr( Ddo_naoconformidade_tipo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_naoconformidade_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Datalisttype", StringUtil.RTrim( Ddo_naoconformidade_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_naoconformidade_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_naoconformidade_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_naoconformidade_tipo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Sortasc", StringUtil.RTrim( Ddo_naoconformidade_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Sortdsc", StringUtil.RTrim( Ddo_naoconformidade_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Loadingdata", StringUtil.RTrim( Ddo_naoconformidade_tipo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_naoconformidade_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Rangefilterfrom", StringUtil.RTrim( Ddo_naoconformidade_tipo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Rangefilterto", StringUtil.RTrim( Ddo_naoconformidade_tipo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Noresultsfound", StringUtil.RTrim( Ddo_naoconformidade_tipo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_naoconformidade_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Activeeventkey", StringUtil.RTrim( Ddo_naoconformidade_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_naoconformidade_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_naoconformidade_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_naoconformidade_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NAOCONFORMIDADE_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_naoconformidade_tipo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormAM2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptNaoConformidade" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione N�o Conformidade" ;
      }

      protected void WBAM0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_AM2( true) ;
         }
         else
         {
            wb_table1_2_AM2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AM2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_59_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(68, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnaoconformidade_nome_Internalname, StringUtil.RTrim( AV28TFNaoConformidade_Nome), StringUtil.RTrim( context.localUtil.Format( AV28TFNaoConformidade_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnaoconformidade_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfnaoconformidade_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptNaoConformidade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnaoconformidade_nome_sel_Internalname, StringUtil.RTrim( AV29TFNaoConformidade_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV29TFNaoConformidade_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnaoconformidade_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfnaoconformidade_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptNaoConformidade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NAOCONFORMIDADE_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_59_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Internalname, AV30ddo_NaoConformidade_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", 0, edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptNaoConformidade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NAOCONFORMIDADE_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_59_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Internalname, AV34ddo_NaoConformidade_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", 0, edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptNaoConformidade.htm");
         }
         wbLoad = true;
      }

      protected void STARTAM2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione N�o Conformidade", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPAM0( ) ;
      }

      protected void WSAM2( )
      {
         STARTAM2( ) ;
         EVTAM2( ) ;
      }

      protected void EVTAM2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11AM2 */
                           E11AM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_NAOCONFORMIDADE_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12AM2 */
                           E12AM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_NAOCONFORMIDADE_TIPO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13AM2 */
                           E13AM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14AM2 */
                           E14AM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15AM2 */
                           E15AM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16AM2 */
                           E16AM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17AM2 */
                           E17AM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18AM2 */
                           E18AM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19AM2 */
                           E19AM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20AM2 */
                           E20AM2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_59_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_59_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_59_idx), 4, 0)), 4, "0");
                           SubsflControlProps_592( ) ;
                           AV25Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25Select)) ? AV41Select_GXI : context.convertURL( context.PathToRelativeUrl( AV25Select))));
                           A426NaoConformidade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtNaoConformidade_Codigo_Internalname), ",", "."));
                           A427NaoConformidade_Nome = StringUtil.Upper( cgiGet( edtNaoConformidade_Nome_Internalname));
                           cmbNaoConformidade_Tipo.Name = cmbNaoConformidade_Tipo_Internalname;
                           cmbNaoConformidade_Tipo.CurrentValue = cgiGet( cmbNaoConformidade_Tipo_Internalname);
                           A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cgiGet( cmbNaoConformidade_Tipo_Internalname), "."));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E21AM2 */
                                 E21AM2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E22AM2 */
                                 E22AM2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23AM2 */
                                 E23AM2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Naoconformidade_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vNAOCONFORMIDADE_NOME1"), AV17NaoConformidade_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Naoconformidade_tipo1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vNAOCONFORMIDADE_TIPO1"), ",", ".") != Convert.ToDecimal( AV18NaoConformidade_Tipo1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Naoconformidade_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vNAOCONFORMIDADE_NOME2"), AV21NaoConformidade_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Naoconformidade_tipo2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vNAOCONFORMIDADE_TIPO2"), ",", ".") != Convert.ToDecimal( AV22NaoConformidade_Tipo2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnaoconformidade_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNAOCONFORMIDADE_NOME"), AV28TFNaoConformidade_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnaoconformidade_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNAOCONFORMIDADE_NOME_SEL"), AV29TFNaoConformidade_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E24AM2 */
                                       E24AM2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEAM2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormAM2( ) ;
            }
         }
      }

      protected void PAAM2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("NAOCONFORMIDADE_NOME", "Conformidade", 0);
            cmbavDynamicfiltersselector1.addItem("NAOCONFORMIDADE_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavNaoconformidade_tipo1.Name = "vNAOCONFORMIDADE_TIPO1";
            cmbavNaoconformidade_tipo1.WebTags = "";
            cmbavNaoconformidade_tipo1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavNaoconformidade_tipo1.addItem("1", "Demandas", 0);
            cmbavNaoconformidade_tipo1.addItem("2", "Itens da Contagem", 0);
            cmbavNaoconformidade_tipo1.addItem("3", "Contagens", 0);
            cmbavNaoconformidade_tipo1.addItem("4", "Check List Demandas", 0);
            cmbavNaoconformidade_tipo1.addItem("5", "Check List Contagens", 0);
            cmbavNaoconformidade_tipo1.addItem("6", "Homologa��o", 0);
            if ( cmbavNaoconformidade_tipo1.ItemCount > 0 )
            {
               AV18NaoConformidade_Tipo1 = (short)(NumberUtil.Val( cmbavNaoconformidade_tipo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("NAOCONFORMIDADE_NOME", "Conformidade", 0);
            cmbavDynamicfiltersselector2.addItem("NAOCONFORMIDADE_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavNaoconformidade_tipo2.Name = "vNAOCONFORMIDADE_TIPO2";
            cmbavNaoconformidade_tipo2.WebTags = "";
            cmbavNaoconformidade_tipo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavNaoconformidade_tipo2.addItem("1", "Demandas", 0);
            cmbavNaoconformidade_tipo2.addItem("2", "Itens da Contagem", 0);
            cmbavNaoconformidade_tipo2.addItem("3", "Contagens", 0);
            cmbavNaoconformidade_tipo2.addItem("4", "Check List Demandas", 0);
            cmbavNaoconformidade_tipo2.addItem("5", "Check List Contagens", 0);
            cmbavNaoconformidade_tipo2.addItem("6", "Homologa��o", 0);
            if ( cmbavNaoconformidade_tipo2.ItemCount > 0 )
            {
               AV22NaoConformidade_Tipo2 = (short)(NumberUtil.Val( cmbavNaoconformidade_tipo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
            }
            GXCCtl = "NAOCONFORMIDADE_TIPO_" + sGXsfl_59_idx;
            cmbNaoConformidade_Tipo.Name = GXCCtl;
            cmbNaoConformidade_Tipo.WebTags = "";
            cmbNaoConformidade_Tipo.addItem("1", "Demandas", 0);
            cmbNaoConformidade_Tipo.addItem("2", "Itens da Contagem", 0);
            cmbNaoConformidade_Tipo.addItem("3", "Contagens", 0);
            cmbNaoConformidade_Tipo.addItem("4", "Check List Demandas", 0);
            cmbNaoConformidade_Tipo.addItem("5", "Check List Contagens", 0);
            cmbNaoConformidade_Tipo.addItem("6", "Homologa��o", 0);
            if ( cmbNaoConformidade_Tipo.ItemCount > 0 )
            {
               A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cmbNaoConformidade_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0))), "."));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_592( ) ;
         while ( nGXsfl_59_idx <= nRC_GXsfl_59 )
         {
            sendrow_592( ) ;
            nGXsfl_59_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_59_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_59_idx+1));
            sGXsfl_59_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_59_idx), 4, 0)), 4, "0");
            SubsflControlProps_592( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV17NaoConformidade_Nome1 ,
                                       short AV18NaoConformidade_Tipo1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       String AV21NaoConformidade_Nome2 ,
                                       short AV22NaoConformidade_Tipo2 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       String AV28TFNaoConformidade_Nome ,
                                       String AV29TFNaoConformidade_Nome_Sel ,
                                       String AV30ddo_NaoConformidade_NomeTitleControlIdToReplace ,
                                       String AV34ddo_NaoConformidade_TipoTitleControlIdToReplace ,
                                       int AV15NaoConformidade_AreaTrabalhoCod ,
                                       IGxCollection AV33TFNaoConformidade_Tipo_Sels ,
                                       String AV42Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFAM2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A426NaoConformidade_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A427NaoConformidade_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_NOME", StringUtil.RTrim( A427NaoConformidade_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_TIPO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A429NaoConformidade_Tipo), "Z9")));
         GxWebStd.gx_hidden_field( context, "NAOCONFORMIDADE_TIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A429NaoConformidade_Tipo), 2, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavNaoconformidade_tipo1.ItemCount > 0 )
         {
            AV18NaoConformidade_Tipo1 = (short)(NumberUtil.Val( cmbavNaoconformidade_tipo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavNaoconformidade_tipo2.ItemCount > 0 )
         {
            AV22NaoConformidade_Tipo2 = (short)(NumberUtil.Val( cmbavNaoconformidade_tipo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAM2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV42Pgmname = "PromptNaoConformidade";
         context.Gx_err = 0;
      }

      protected void RFAM2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 59;
         /* Execute user event: E22AM2 */
         E22AM2 ();
         nGXsfl_59_idx = 1;
         sGXsfl_59_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_59_idx), 4, 0)), 4, "0");
         SubsflControlProps_592( ) ;
         nGXsfl_59_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_592( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A429NaoConformidade_Tipo ,
                                                 AV33TFNaoConformidade_Tipo_Sels ,
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17NaoConformidade_Nome1 ,
                                                 AV18NaoConformidade_Tipo1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21NaoConformidade_Nome2 ,
                                                 AV22NaoConformidade_Tipo2 ,
                                                 AV29TFNaoConformidade_Nome_Sel ,
                                                 AV28TFNaoConformidade_Nome ,
                                                 AV33TFNaoConformidade_Tipo_Sels.Count ,
                                                 A427NaoConformidade_Nome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A428NaoConformidade_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV17NaoConformidade_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17NaoConformidade_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NaoConformidade_Nome1", AV17NaoConformidade_Nome1);
            lV21NaoConformidade_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV21NaoConformidade_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NaoConformidade_Nome2", AV21NaoConformidade_Nome2);
            lV28TFNaoConformidade_Nome = StringUtil.PadR( StringUtil.RTrim( AV28TFNaoConformidade_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFNaoConformidade_Nome", AV28TFNaoConformidade_Nome);
            /* Using cursor H00AM2 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, lV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, lV28TFNaoConformidade_Nome, AV29TFNaoConformidade_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_59_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A428NaoConformidade_AreaTrabalhoCod = H00AM2_A428NaoConformidade_AreaTrabalhoCod[0];
               A429NaoConformidade_Tipo = H00AM2_A429NaoConformidade_Tipo[0];
               A427NaoConformidade_Nome = H00AM2_A427NaoConformidade_Nome[0];
               A426NaoConformidade_Codigo = H00AM2_A426NaoConformidade_Codigo[0];
               /* Execute user event: E23AM2 */
               E23AM2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 59;
            WBAM0( ) ;
         }
         nGXsfl_59_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A429NaoConformidade_Tipo ,
                                              AV33TFNaoConformidade_Tipo_Sels ,
                                              AV16DynamicFiltersSelector1 ,
                                              AV17NaoConformidade_Nome1 ,
                                              AV18NaoConformidade_Tipo1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21NaoConformidade_Nome2 ,
                                              AV22NaoConformidade_Tipo2 ,
                                              AV29TFNaoConformidade_Nome_Sel ,
                                              AV28TFNaoConformidade_Nome ,
                                              AV33TFNaoConformidade_Tipo_Sels.Count ,
                                              A427NaoConformidade_Nome ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A428NaoConformidade_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV17NaoConformidade_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17NaoConformidade_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NaoConformidade_Nome1", AV17NaoConformidade_Nome1);
         lV21NaoConformidade_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV21NaoConformidade_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NaoConformidade_Nome2", AV21NaoConformidade_Nome2);
         lV28TFNaoConformidade_Nome = StringUtil.PadR( StringUtil.RTrim( AV28TFNaoConformidade_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFNaoConformidade_Nome", AV28TFNaoConformidade_Nome);
         /* Using cursor H00AM3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, lV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, lV28TFNaoConformidade_Nome, AV29TFNaoConformidade_Nome_Sel});
         GRID_nRecordCount = H00AM3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV28TFNaoConformidade_Nome, AV29TFNaoConformidade_Nome_Sel, AV30ddo_NaoConformidade_NomeTitleControlIdToReplace, AV34ddo_NaoConformidade_TipoTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV33TFNaoConformidade_Tipo_Sels, AV42Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV28TFNaoConformidade_Nome, AV29TFNaoConformidade_Nome_Sel, AV30ddo_NaoConformidade_NomeTitleControlIdToReplace, AV34ddo_NaoConformidade_TipoTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV33TFNaoConformidade_Tipo_Sels, AV42Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV28TFNaoConformidade_Nome, AV29TFNaoConformidade_Nome_Sel, AV30ddo_NaoConformidade_NomeTitleControlIdToReplace, AV34ddo_NaoConformidade_TipoTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV33TFNaoConformidade_Tipo_Sels, AV42Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV28TFNaoConformidade_Nome, AV29TFNaoConformidade_Nome_Sel, AV30ddo_NaoConformidade_NomeTitleControlIdToReplace, AV34ddo_NaoConformidade_TipoTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV33TFNaoConformidade_Tipo_Sels, AV42Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV28TFNaoConformidade_Nome, AV29TFNaoConformidade_Nome_Sel, AV30ddo_NaoConformidade_NomeTitleControlIdToReplace, AV34ddo_NaoConformidade_TipoTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV33TFNaoConformidade_Tipo_Sels, AV42Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPAM0( )
      {
         /* Before Start, stand alone formulas. */
         AV42Pgmname = "PromptNaoConformidade";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E21AM2 */
         E21AM2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV35DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vNAOCONFORMIDADE_NOMETITLEFILTERDATA"), AV27NaoConformidade_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNAOCONFORMIDADE_TIPOTITLEFILTERDATA"), AV31NaoConformidade_TipoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavNaoconformidade_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavNaoconformidade_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vNAOCONFORMIDADE_AREATRABALHOCOD");
               GX_FocusControl = edtavNaoconformidade_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV15NaoConformidade_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV15NaoConformidade_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavNaoconformidade_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV17NaoConformidade_Nome1 = StringUtil.Upper( cgiGet( edtavNaoconformidade_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NaoConformidade_Nome1", AV17NaoConformidade_Nome1);
            cmbavNaoconformidade_tipo1.Name = cmbavNaoconformidade_tipo1_Internalname;
            cmbavNaoconformidade_tipo1.CurrentValue = cgiGet( cmbavNaoconformidade_tipo1_Internalname);
            AV18NaoConformidade_Tipo1 = (short)(NumberUtil.Val( cgiGet( cmbavNaoconformidade_tipo1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            AV21NaoConformidade_Nome2 = StringUtil.Upper( cgiGet( edtavNaoconformidade_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NaoConformidade_Nome2", AV21NaoConformidade_Nome2);
            cmbavNaoconformidade_tipo2.Name = cmbavNaoconformidade_tipo2_Internalname;
            cmbavNaoconformidade_tipo2.CurrentValue = cgiGet( cmbavNaoconformidade_tipo2_Internalname);
            AV22NaoConformidade_Tipo2 = (short)(NumberUtil.Val( cgiGet( cmbavNaoconformidade_tipo2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV28TFNaoConformidade_Nome = StringUtil.Upper( cgiGet( edtavTfnaoconformidade_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFNaoConformidade_Nome", AV28TFNaoConformidade_Nome);
            AV29TFNaoConformidade_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfnaoconformidade_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFNaoConformidade_Nome_Sel", AV29TFNaoConformidade_Nome_Sel);
            AV30ddo_NaoConformidade_NomeTitleControlIdToReplace = cgiGet( edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_NaoConformidade_NomeTitleControlIdToReplace", AV30ddo_NaoConformidade_NomeTitleControlIdToReplace);
            AV34ddo_NaoConformidade_TipoTitleControlIdToReplace = cgiGet( edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_NaoConformidade_TipoTitleControlIdToReplace", AV34ddo_NaoConformidade_TipoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_59 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_59"), ",", "."));
            AV37GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV38GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_naoconformidade_nome_Caption = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Caption");
            Ddo_naoconformidade_nome_Tooltip = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Tooltip");
            Ddo_naoconformidade_nome_Cls = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Cls");
            Ddo_naoconformidade_nome_Filteredtext_set = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Filteredtext_set");
            Ddo_naoconformidade_nome_Selectedvalue_set = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Selectedvalue_set");
            Ddo_naoconformidade_nome_Dropdownoptionstype = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Dropdownoptionstype");
            Ddo_naoconformidade_nome_Titlecontrolidtoreplace = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Titlecontrolidtoreplace");
            Ddo_naoconformidade_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Includesortasc"));
            Ddo_naoconformidade_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Includesortdsc"));
            Ddo_naoconformidade_nome_Sortedstatus = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Sortedstatus");
            Ddo_naoconformidade_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Includefilter"));
            Ddo_naoconformidade_nome_Filtertype = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Filtertype");
            Ddo_naoconformidade_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Filterisrange"));
            Ddo_naoconformidade_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Includedatalist"));
            Ddo_naoconformidade_nome_Datalisttype = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Datalisttype");
            Ddo_naoconformidade_nome_Datalistfixedvalues = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Datalistfixedvalues");
            Ddo_naoconformidade_nome_Datalistproc = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Datalistproc");
            Ddo_naoconformidade_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_NAOCONFORMIDADE_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_naoconformidade_nome_Sortasc = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Sortasc");
            Ddo_naoconformidade_nome_Sortdsc = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Sortdsc");
            Ddo_naoconformidade_nome_Loadingdata = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Loadingdata");
            Ddo_naoconformidade_nome_Cleanfilter = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Cleanfilter");
            Ddo_naoconformidade_nome_Rangefilterfrom = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Rangefilterfrom");
            Ddo_naoconformidade_nome_Rangefilterto = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Rangefilterto");
            Ddo_naoconformidade_nome_Noresultsfound = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Noresultsfound");
            Ddo_naoconformidade_nome_Searchbuttontext = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Searchbuttontext");
            Ddo_naoconformidade_tipo_Caption = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Caption");
            Ddo_naoconformidade_tipo_Tooltip = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Tooltip");
            Ddo_naoconformidade_tipo_Cls = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Cls");
            Ddo_naoconformidade_tipo_Selectedvalue_set = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Selectedvalue_set");
            Ddo_naoconformidade_tipo_Dropdownoptionstype = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Dropdownoptionstype");
            Ddo_naoconformidade_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Titlecontrolidtoreplace");
            Ddo_naoconformidade_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Includesortasc"));
            Ddo_naoconformidade_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Includesortdsc"));
            Ddo_naoconformidade_tipo_Sortedstatus = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Sortedstatus");
            Ddo_naoconformidade_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Includefilter"));
            Ddo_naoconformidade_tipo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Filterisrange"));
            Ddo_naoconformidade_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Includedatalist"));
            Ddo_naoconformidade_tipo_Datalisttype = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Datalisttype");
            Ddo_naoconformidade_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Allowmultipleselection"));
            Ddo_naoconformidade_tipo_Datalistfixedvalues = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Datalistfixedvalues");
            Ddo_naoconformidade_tipo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_naoconformidade_tipo_Sortasc = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Sortasc");
            Ddo_naoconformidade_tipo_Sortdsc = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Sortdsc");
            Ddo_naoconformidade_tipo_Loadingdata = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Loadingdata");
            Ddo_naoconformidade_tipo_Cleanfilter = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Cleanfilter");
            Ddo_naoconformidade_tipo_Rangefilterfrom = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Rangefilterfrom");
            Ddo_naoconformidade_tipo_Rangefilterto = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Rangefilterto");
            Ddo_naoconformidade_tipo_Noresultsfound = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Noresultsfound");
            Ddo_naoconformidade_tipo_Searchbuttontext = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_naoconformidade_nome_Activeeventkey = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Activeeventkey");
            Ddo_naoconformidade_nome_Filteredtext_get = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Filteredtext_get");
            Ddo_naoconformidade_nome_Selectedvalue_get = cgiGet( "DDO_NAOCONFORMIDADE_NOME_Selectedvalue_get");
            Ddo_naoconformidade_tipo_Activeeventkey = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Activeeventkey");
            Ddo_naoconformidade_tipo_Selectedvalue_get = cgiGet( "DDO_NAOCONFORMIDADE_TIPO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vNAOCONFORMIDADE_NOME1"), AV17NaoConformidade_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vNAOCONFORMIDADE_TIPO1"), ",", ".") != Convert.ToDecimal( AV18NaoConformidade_Tipo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vNAOCONFORMIDADE_NOME2"), AV21NaoConformidade_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vNAOCONFORMIDADE_TIPO2"), ",", ".") != Convert.ToDecimal( AV22NaoConformidade_Tipo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNAOCONFORMIDADE_NOME"), AV28TFNaoConformidade_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNAOCONFORMIDADE_NOME_SEL"), AV29TFNaoConformidade_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E21AM2 */
         E21AM2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21AM2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV18NaoConformidade_Tipo1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
         AV16DynamicFiltersSelector1 = "NAOCONFORMIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22NaoConformidade_Tipo2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
         AV20DynamicFiltersSelector2 = "NAOCONFORMIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfnaoconformidade_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnaoconformidade_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnaoconformidade_nome_Visible), 5, 0)));
         edtavTfnaoconformidade_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnaoconformidade_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnaoconformidade_nome_sel_Visible), 5, 0)));
         Ddo_naoconformidade_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_NaoConformidade_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "TitleControlIdToReplace", Ddo_naoconformidade_nome_Titlecontrolidtoreplace);
         AV30ddo_NaoConformidade_NomeTitleControlIdToReplace = Ddo_naoconformidade_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_NaoConformidade_NomeTitleControlIdToReplace", AV30ddo_NaoConformidade_NomeTitleControlIdToReplace);
         edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_naoconformidade_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_NaoConformidade_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "TitleControlIdToReplace", Ddo_naoconformidade_tipo_Titlecontrolidtoreplace);
         AV34ddo_NaoConformidade_TipoTitleControlIdToReplace = Ddo_naoconformidade_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_NaoConformidade_TipoTitleControlIdToReplace", AV34ddo_NaoConformidade_TipoTitleControlIdToReplace);
         edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione N�o Conformidade";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Conformidade", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV35DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV35DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E22AM2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV27NaoConformidade_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV31NaoConformidade_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtNaoConformidade_Nome_Titleformat = 2;
         edtNaoConformidade_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV30ddo_NaoConformidade_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNaoConformidade_Nome_Internalname, "Title", edtNaoConformidade_Nome_Title);
         cmbNaoConformidade_Tipo_Titleformat = 2;
         cmbNaoConformidade_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV34ddo_NaoConformidade_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbNaoConformidade_Tipo_Internalname, "Title", cmbNaoConformidade_Tipo.Title.Text);
         AV37GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37GridCurrentPage), 10, 0)));
         AV38GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27NaoConformidade_NomeTitleFilterData", AV27NaoConformidade_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31NaoConformidade_TipoTitleFilterData", AV31NaoConformidade_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11AM2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV36PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV36PageToGo) ;
         }
      }

      protected void E12AM2( )
      {
         /* Ddo_naoconformidade_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_naoconformidade_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "SortedStatus", Ddo_naoconformidade_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "SortedStatus", Ddo_naoconformidade_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV28TFNaoConformidade_Nome = Ddo_naoconformidade_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFNaoConformidade_Nome", AV28TFNaoConformidade_Nome);
            AV29TFNaoConformidade_Nome_Sel = Ddo_naoconformidade_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFNaoConformidade_Nome_Sel", AV29TFNaoConformidade_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13AM2( )
      {
         /* Ddo_naoconformidade_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_naoconformidade_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "SortedStatus", Ddo_naoconformidade_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_naoconformidade_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "SortedStatus", Ddo_naoconformidade_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_naoconformidade_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV32TFNaoConformidade_Tipo_SelsJson = Ddo_naoconformidade_tipo_Selectedvalue_get;
            AV33TFNaoConformidade_Tipo_Sels.FromJSonString(StringUtil.StringReplace( AV32TFNaoConformidade_Tipo_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33TFNaoConformidade_Tipo_Sels", AV33TFNaoConformidade_Tipo_Sels);
      }

      private void E23AM2( )
      {
         /* Grid_Load Routine */
         AV25Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV25Select);
         AV41Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 59;
         }
         sendrow_592( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_59_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(59, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E24AM2 */
         E24AM2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24AM2( )
      {
         /* Enter Routine */
         AV7InOutNaoConformidade_Codigo = A426NaoConformidade_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutNaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutNaoConformidade_Codigo), 6, 0)));
         AV8InOutNaoConformidade_Nome = A427NaoConformidade_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutNaoConformidade_Nome", AV8InOutNaoConformidade_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutNaoConformidade_Codigo,(String)AV8InOutNaoConformidade_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E14AM2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E18AM2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15AM2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV28TFNaoConformidade_Nome, AV29TFNaoConformidade_Nome_Sel, AV30ddo_NaoConformidade_NomeTitleControlIdToReplace, AV34ddo_NaoConformidade_TipoTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV33TFNaoConformidade_Tipo_Sels, AV42Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavNaoconformidade_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Values", cmbavNaoconformidade_tipo1.ToJavascriptSource());
         cmbavNaoconformidade_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Values", cmbavNaoconformidade_tipo2.ToJavascriptSource());
      }

      protected void E19AM2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16AM2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17NaoConformidade_Nome1, AV18NaoConformidade_Tipo1, AV20DynamicFiltersSelector2, AV21NaoConformidade_Nome2, AV22NaoConformidade_Tipo2, AV19DynamicFiltersEnabled2, AV28TFNaoConformidade_Nome, AV29TFNaoConformidade_Nome_Sel, AV30ddo_NaoConformidade_NomeTitleControlIdToReplace, AV34ddo_NaoConformidade_TipoTitleControlIdToReplace, AV15NaoConformidade_AreaTrabalhoCod, AV33TFNaoConformidade_Tipo_Sels, AV42Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavNaoconformidade_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Values", cmbavNaoconformidade_tipo1.ToJavascriptSource());
         cmbavNaoconformidade_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Values", cmbavNaoconformidade_tipo2.ToJavascriptSource());
      }

      protected void E20AM2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17AM2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33TFNaoConformidade_Tipo_Sels", AV33TFNaoConformidade_Tipo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavNaoconformidade_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Values", cmbavNaoconformidade_tipo1.ToJavascriptSource());
         cmbavNaoconformidade_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Values", cmbavNaoconformidade_tipo2.ToJavascriptSource());
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_naoconformidade_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "SortedStatus", Ddo_naoconformidade_nome_Sortedstatus);
         Ddo_naoconformidade_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "SortedStatus", Ddo_naoconformidade_tipo_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_naoconformidade_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "SortedStatus", Ddo_naoconformidade_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_naoconformidade_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "SortedStatus", Ddo_naoconformidade_tipo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavNaoconformidade_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNaoconformidade_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNaoconformidade_nome1_Visible), 5, 0)));
         cmbavNaoconformidade_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNaoconformidade_tipo1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_NOME") == 0 )
         {
            edtavNaoconformidade_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNaoconformidade_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNaoconformidade_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_TIPO") == 0 )
         {
            cmbavNaoconformidade_tipo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNaoconformidade_tipo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavNaoconformidade_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNaoconformidade_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNaoconformidade_nome2_Visible), 5, 0)));
         cmbavNaoconformidade_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNaoconformidade_tipo2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_NOME") == 0 )
         {
            edtavNaoconformidade_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNaoconformidade_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNaoconformidade_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_TIPO") == 0 )
         {
            cmbavNaoconformidade_tipo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavNaoconformidade_tipo2.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "NAOCONFORMIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21NaoConformidade_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NaoConformidade_Nome2", AV21NaoConformidade_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15NaoConformidade_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15NaoConformidade_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0)));
         AV28TFNaoConformidade_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFNaoConformidade_Nome", AV28TFNaoConformidade_Nome);
         Ddo_naoconformidade_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "FilteredText_set", Ddo_naoconformidade_nome_Filteredtext_set);
         AV29TFNaoConformidade_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFNaoConformidade_Nome_Sel", AV29TFNaoConformidade_Nome_Sel);
         Ddo_naoconformidade_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_nome_Internalname, "SelectedValue_set", Ddo_naoconformidade_nome_Selectedvalue_set);
         AV33TFNaoConformidade_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_naoconformidade_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_naoconformidade_tipo_Internalname, "SelectedValue_set", Ddo_naoconformidade_tipo_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "NAOCONFORMIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17NaoConformidade_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NaoConformidade_Nome1", AV17NaoConformidade_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S132( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_NOME") == 0 )
            {
               AV17NaoConformidade_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NaoConformidade_Nome1", AV17NaoConformidade_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_TIPO") == 0 )
            {
               AV18NaoConformidade_Tipo1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NaoConformidade_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_NOME") == 0 )
               {
                  AV21NaoConformidade_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NaoConformidade_Nome2", AV21NaoConformidade_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_TIPO") == 0 )
               {
                  AV22NaoConformidade_Tipo2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NaoConformidade_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV15NaoConformidade_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "NAOCONFORMIDADE_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFNaoConformidade_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNAOCONFORMIDADE_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV28TFNaoConformidade_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFNaoConformidade_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNAOCONFORMIDADE_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV29TFNaoConformidade_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV33TFNaoConformidade_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNAOCONFORMIDADE_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV33TFNaoConformidade_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV42Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17NaoConformidade_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17NaoConformidade_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_TIPO") == 0 ) && ! (0==AV18NaoConformidade_Tipo1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21NaoConformidade_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21NaoConformidade_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_TIPO") == 0 ) && ! (0==AV22NaoConformidade_Tipo2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_AM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_AM2( true) ;
         }
         else
         {
            wb_table2_5_AM2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_AM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_53_AM2( true) ;
         }
         else
         {
            wb_table3_53_AM2( false) ;
         }
         return  ;
      }

      protected void wb_table3_53_AM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AM2e( true) ;
         }
         else
         {
            wb_table1_2_AM2e( false) ;
         }
      }

      protected void wb_table3_53_AM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_56_AM2( true) ;
         }
         else
         {
            wb_table4_56_AM2( false) ;
         }
         return  ;
      }

      protected void wb_table4_56_AM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_53_AM2e( true) ;
         }
         else
         {
            wb_table3_53_AM2e( false) ;
         }
      }

      protected void wb_table4_56_AM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"59\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Conformidade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNaoConformidade_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtNaoConformidade_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNaoConformidade_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbNaoConformidade_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbNaoConformidade_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbNaoConformidade_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV25Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A426NaoConformidade_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A427NaoConformidade_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNaoConformidade_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNaoConformidade_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A429NaoConformidade_Tipo), 2, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbNaoConformidade_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbNaoConformidade_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 59 )
         {
            wbEnd = 0;
            nRC_GXsfl_59 = (short)(nGXsfl_59_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_56_AM2e( true) ;
         }
         else
         {
            wb_table4_56_AM2e( false) ;
         }
      }

      protected void wb_table2_5_AM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_59_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptNaoConformidade.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_AM2( true) ;
         }
         else
         {
            wb_table5_14_AM2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_AM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_AM2e( true) ;
         }
         else
         {
            wb_table2_5_AM2e( false) ;
         }
      }

      protected void wb_table5_14_AM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextnaoconformidade_areatrabalhocod_Internalname, "de Trabalho", "", "", lblFiltertextnaoconformidade_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNaoconformidade_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15NaoConformidade_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNaoconformidade_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_AM2( true) ;
         }
         else
         {
            wb_table6_23_AM2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_AM2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_AM2e( true) ;
         }
         else
         {
            wb_table5_14_AM2e( false) ;
         }
      }

      protected void wb_table6_23_AM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_59_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_PromptNaoConformidade.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNaoconformidade_nome1_Internalname, StringUtil.RTrim( AV17NaoConformidade_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17NaoConformidade_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNaoconformidade_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavNaoconformidade_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptNaoConformidade.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_59_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavNaoconformidade_tipo1, cmbavNaoconformidade_tipo1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0)), 1, cmbavNaoconformidade_tipo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavNaoconformidade_tipo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_PromptNaoConformidade.htm");
            cmbavNaoconformidade_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18NaoConformidade_Tipo1), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo1_Internalname, "Values", (String)(cmbavNaoconformidade_tipo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_59_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptNaoConformidade.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNaoconformidade_nome2_Internalname, StringUtil.RTrim( AV21NaoConformidade_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21NaoConformidade_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNaoconformidade_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavNaoconformidade_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptNaoConformidade.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_59_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavNaoconformidade_tipo2, cmbavNaoconformidade_tipo2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0)), 1, cmbavNaoconformidade_tipo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavNaoconformidade_tipo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_PromptNaoConformidade.htm");
            cmbavNaoconformidade_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22NaoConformidade_Tipo2), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNaoconformidade_tipo2_Internalname, "Values", (String)(cmbavNaoconformidade_tipo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_AM2e( true) ;
         }
         else
         {
            wb_table6_23_AM2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutNaoConformidade_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutNaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutNaoConformidade_Codigo), 6, 0)));
         AV8InOutNaoConformidade_Nome = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutNaoConformidade_Nome", AV8InOutNaoConformidade_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAM2( ) ;
         WSAM2( ) ;
         WEAM2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823184557");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptnaoconformidade.js", "?202042823184557");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_592( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_59_idx;
         edtNaoConformidade_Codigo_Internalname = "NAOCONFORMIDADE_CODIGO_"+sGXsfl_59_idx;
         edtNaoConformidade_Nome_Internalname = "NAOCONFORMIDADE_NOME_"+sGXsfl_59_idx;
         cmbNaoConformidade_Tipo_Internalname = "NAOCONFORMIDADE_TIPO_"+sGXsfl_59_idx;
      }

      protected void SubsflControlProps_fel_592( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_59_fel_idx;
         edtNaoConformidade_Codigo_Internalname = "NAOCONFORMIDADE_CODIGO_"+sGXsfl_59_fel_idx;
         edtNaoConformidade_Nome_Internalname = "NAOCONFORMIDADE_NOME_"+sGXsfl_59_fel_idx;
         cmbNaoConformidade_Tipo_Internalname = "NAOCONFORMIDADE_TIPO_"+sGXsfl_59_fel_idx;
      }

      protected void sendrow_592( )
      {
         SubsflControlProps_592( ) ;
         WBAM0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_59_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_59_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_59_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 60,'',false,'',59)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV25Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV41Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV25Select)) ? AV41Select_GXI : context.PathToRelativeUrl( AV25Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_59_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV25Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNaoConformidade_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A426NaoConformidade_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNaoConformidade_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)59,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNaoConformidade_Nome_Internalname,StringUtil.RTrim( A427NaoConformidade_Nome),StringUtil.RTrim( context.localUtil.Format( A427NaoConformidade_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNaoConformidade_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)59,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_59_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "NAOCONFORMIDADE_TIPO_" + sGXsfl_59_idx;
               cmbNaoConformidade_Tipo.Name = GXCCtl;
               cmbNaoConformidade_Tipo.WebTags = "";
               cmbNaoConformidade_Tipo.addItem("1", "Demandas", 0);
               cmbNaoConformidade_Tipo.addItem("2", "Itens da Contagem", 0);
               cmbNaoConformidade_Tipo.addItem("3", "Contagens", 0);
               cmbNaoConformidade_Tipo.addItem("4", "Check List Demandas", 0);
               cmbNaoConformidade_Tipo.addItem("5", "Check List Contagens", 0);
               cmbNaoConformidade_Tipo.addItem("6", "Homologa��o", 0);
               if ( cmbNaoConformidade_Tipo.ItemCount > 0 )
               {
                  A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cmbNaoConformidade_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbNaoConformidade_Tipo,(String)cmbNaoConformidade_Tipo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)),(short)1,(String)cmbNaoConformidade_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbNaoConformidade_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbNaoConformidade_Tipo_Internalname, "Values", (String)(cmbNaoConformidade_Tipo.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_CODIGO"+"_"+sGXsfl_59_idx, GetSecureSignedToken( sGXsfl_59_idx, context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_NOME"+"_"+sGXsfl_59_idx, GetSecureSignedToken( sGXsfl_59_idx, StringUtil.RTrim( context.localUtil.Format( A427NaoConformidade_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_NAOCONFORMIDADE_TIPO"+"_"+sGXsfl_59_idx, GetSecureSignedToken( sGXsfl_59_idx, context.localUtil.Format( (decimal)(A429NaoConformidade_Tipo), "Z9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_59_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_59_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_59_idx+1));
            sGXsfl_59_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_59_idx), 4, 0)), 4, "0");
            SubsflControlProps_592( ) ;
         }
         /* End function sendrow_592 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextnaoconformidade_areatrabalhocod_Internalname = "FILTERTEXTNAOCONFORMIDADE_AREATRABALHOCOD";
         edtavNaoconformidade_areatrabalhocod_Internalname = "vNAOCONFORMIDADE_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavNaoconformidade_nome1_Internalname = "vNAOCONFORMIDADE_NOME1";
         cmbavNaoconformidade_tipo1_Internalname = "vNAOCONFORMIDADE_TIPO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavNaoconformidade_nome2_Internalname = "vNAOCONFORMIDADE_NOME2";
         cmbavNaoconformidade_tipo2_Internalname = "vNAOCONFORMIDADE_TIPO2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtNaoConformidade_Codigo_Internalname = "NAOCONFORMIDADE_CODIGO";
         edtNaoConformidade_Nome_Internalname = "NAOCONFORMIDADE_NOME";
         cmbNaoConformidade_Tipo_Internalname = "NAOCONFORMIDADE_TIPO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfnaoconformidade_nome_Internalname = "vTFNAOCONFORMIDADE_NOME";
         edtavTfnaoconformidade_nome_sel_Internalname = "vTFNAOCONFORMIDADE_NOME_SEL";
         Ddo_naoconformidade_nome_Internalname = "DDO_NAOCONFORMIDADE_NOME";
         edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Internalname = "vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE";
         Ddo_naoconformidade_tipo_Internalname = "DDO_NAOCONFORMIDADE_TIPO";
         edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Internalname = "vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbNaoConformidade_Tipo_Jsonclick = "";
         edtNaoConformidade_Nome_Jsonclick = "";
         edtNaoConformidade_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         cmbavNaoconformidade_tipo2_Jsonclick = "";
         edtavNaoconformidade_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavNaoconformidade_tipo1_Jsonclick = "";
         edtavNaoconformidade_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavNaoconformidade_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         cmbNaoConformidade_Tipo_Titleformat = 0;
         edtNaoConformidade_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavNaoconformidade_tipo2.Visible = 1;
         edtavNaoconformidade_nome2_Visible = 1;
         cmbavNaoconformidade_tipo1.Visible = 1;
         edtavNaoconformidade_nome1_Visible = 1;
         cmbNaoConformidade_Tipo.Title.Text = "Tipo";
         edtNaoConformidade_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfnaoconformidade_nome_sel_Jsonclick = "";
         edtavTfnaoconformidade_nome_sel_Visible = 1;
         edtavTfnaoconformidade_nome_Jsonclick = "";
         edtavTfnaoconformidade_nome_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_naoconformidade_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_naoconformidade_tipo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_naoconformidade_tipo_Rangefilterto = "At�";
         Ddo_naoconformidade_tipo_Rangefilterfrom = "Desde";
         Ddo_naoconformidade_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_naoconformidade_tipo_Loadingdata = "Carregando dados...";
         Ddo_naoconformidade_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_naoconformidade_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_naoconformidade_tipo_Datalistupdateminimumcharacters = 0;
         Ddo_naoconformidade_tipo_Datalistfixedvalues = "1:Demandas,2:Itens da Contagem,3:Contagens,4:Check List Demandas,5:Check List Contagens,6:Homologa��o";
         Ddo_naoconformidade_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_naoconformidade_tipo_Datalisttype = "FixedValues";
         Ddo_naoconformidade_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_naoconformidade_tipo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_naoconformidade_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_naoconformidade_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_tipo_Titlecontrolidtoreplace = "";
         Ddo_naoconformidade_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_naoconformidade_tipo_Cls = "ColumnSettings";
         Ddo_naoconformidade_tipo_Tooltip = "Op��es";
         Ddo_naoconformidade_tipo_Caption = "";
         Ddo_naoconformidade_nome_Searchbuttontext = "Pesquisar";
         Ddo_naoconformidade_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_naoconformidade_nome_Rangefilterto = "At�";
         Ddo_naoconformidade_nome_Rangefilterfrom = "Desde";
         Ddo_naoconformidade_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_naoconformidade_nome_Loadingdata = "Carregando dados...";
         Ddo_naoconformidade_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_naoconformidade_nome_Sortasc = "Ordenar de A � Z";
         Ddo_naoconformidade_nome_Datalistupdateminimumcharacters = 0;
         Ddo_naoconformidade_nome_Datalistproc = "GetPromptNaoConformidadeFilterData";
         Ddo_naoconformidade_nome_Datalistfixedvalues = "";
         Ddo_naoconformidade_nome_Datalisttype = "Dynamic";
         Ddo_naoconformidade_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_naoconformidade_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_naoconformidade_nome_Filtertype = "Character";
         Ddo_naoconformidade_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_naoconformidade_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_naoconformidade_nome_Titlecontrolidtoreplace = "";
         Ddo_naoconformidade_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_naoconformidade_nome_Cls = "ColumnSettings";
         Ddo_naoconformidade_nome_Tooltip = "Op��es";
         Ddo_naoconformidade_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione N�o Conformidade";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV28TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV29TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV33TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0}],oparms:[{av:'AV27NaoConformidade_NomeTitleFilterData',fld:'vNAOCONFORMIDADE_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV31NaoConformidade_TipoTitleFilterData',fld:'vNAOCONFORMIDADE_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtNaoConformidade_Nome_Titleformat',ctrl:'NAOCONFORMIDADE_NOME',prop:'Titleformat'},{av:'edtNaoConformidade_Nome_Title',ctrl:'NAOCONFORMIDADE_NOME',prop:'Title'},{av:'cmbNaoConformidade_Tipo'},{av:'AV37GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV38GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11AM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV28TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV29TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV30ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_NAOCONFORMIDADE_NOME.ONOPTIONCLICKED","{handler:'E12AM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV28TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV29TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV30ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_naoconformidade_nome_Activeeventkey',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'ActiveEventKey'},{av:'Ddo_naoconformidade_nome_Filteredtext_get',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'FilteredText_get'},{av:'Ddo_naoconformidade_nome_Selectedvalue_get',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_naoconformidade_nome_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'SortedStatus'},{av:'AV28TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV29TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_naoconformidade_tipo_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NAOCONFORMIDADE_TIPO.ONOPTIONCLICKED","{handler:'E13AM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV28TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV29TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV30ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_naoconformidade_tipo_Activeeventkey',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'ActiveEventKey'},{av:'Ddo_naoconformidade_tipo_Selectedvalue_get',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_naoconformidade_tipo_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'SortedStatus'},{av:'AV33TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'Ddo_naoconformidade_nome_Sortedstatus',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E23AM2',iparms:[],oparms:[{av:'AV25Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E24AM2',iparms:[{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A427NaoConformidade_Nome',fld:'NAOCONFORMIDADE_NOME',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutNaoConformidade_Codigo',fld:'vINOUTNAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutNaoConformidade_Nome',fld:'vINOUTNAOCONFORMIDADE_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14AM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV28TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV29TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV30ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E18AM2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15AM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV28TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV29TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV30ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'edtavNaoconformidade_nome2_Visible',ctrl:'vNAOCONFORMIDADE_NOME2',prop:'Visible'},{av:'cmbavNaoconformidade_tipo2'},{av:'edtavNaoconformidade_nome1_Visible',ctrl:'vNAOCONFORMIDADE_NOME1',prop:'Visible'},{av:'cmbavNaoconformidade_tipo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E19AM2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavNaoconformidade_nome1_Visible',ctrl:'vNAOCONFORMIDADE_NOME1',prop:'Visible'},{av:'cmbavNaoconformidade_tipo1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16AM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV28TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV29TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV30ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'edtavNaoconformidade_nome2_Visible',ctrl:'vNAOCONFORMIDADE_NOME2',prop:'Visible'},{av:'cmbavNaoconformidade_tipo2'},{av:'edtavNaoconformidade_nome1_Visible',ctrl:'vNAOCONFORMIDADE_NOME1',prop:'Visible'},{av:'cmbavNaoconformidade_tipo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20AM2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavNaoconformidade_nome2_Visible',ctrl:'vNAOCONFORMIDADE_NOME2',prop:'Visible'},{av:'cmbavNaoconformidade_tipo2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E17AM2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV28TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'AV29TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV30ddo_NaoConformidade_NomeTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34ddo_NaoConformidade_TipoTitleControlIdToReplace',fld:'vDDO_NAOCONFORMIDADE_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV33TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV15NaoConformidade_AreaTrabalhoCod',fld:'vNAOCONFORMIDADE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV28TFNaoConformidade_Nome',fld:'vTFNAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'Ddo_naoconformidade_nome_Filteredtext_set',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'FilteredText_set'},{av:'AV29TFNaoConformidade_Nome_Sel',fld:'vTFNAOCONFORMIDADE_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_naoconformidade_nome_Selectedvalue_set',ctrl:'DDO_NAOCONFORMIDADE_NOME',prop:'SelectedValue_set'},{av:'AV33TFNaoConformidade_Tipo_Sels',fld:'vTFNAOCONFORMIDADE_TIPO_SELS',pic:'',nv:null},{av:'Ddo_naoconformidade_tipo_Selectedvalue_set',ctrl:'DDO_NAOCONFORMIDADE_TIPO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17NaoConformidade_Nome1',fld:'vNAOCONFORMIDADE_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavNaoconformidade_nome1_Visible',ctrl:'vNAOCONFORMIDADE_NOME1',prop:'Visible'},{av:'cmbavNaoconformidade_tipo1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21NaoConformidade_Nome2',fld:'vNAOCONFORMIDADE_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV18NaoConformidade_Tipo1',fld:'vNAOCONFORMIDADE_TIPO1',pic:'Z9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22NaoConformidade_Tipo2',fld:'vNAOCONFORMIDADE_TIPO2',pic:'Z9',nv:0},{av:'edtavNaoconformidade_nome2_Visible',ctrl:'vNAOCONFORMIDADE_NOME2',prop:'Visible'},{av:'cmbavNaoconformidade_tipo2'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutNaoConformidade_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_naoconformidade_nome_Activeeventkey = "";
         Ddo_naoconformidade_nome_Filteredtext_get = "";
         Ddo_naoconformidade_nome_Selectedvalue_get = "";
         Ddo_naoconformidade_tipo_Activeeventkey = "";
         Ddo_naoconformidade_tipo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV17NaoConformidade_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV21NaoConformidade_Nome2 = "";
         AV28TFNaoConformidade_Nome = "";
         AV29TFNaoConformidade_Nome_Sel = "";
         AV30ddo_NaoConformidade_NomeTitleControlIdToReplace = "";
         AV34ddo_NaoConformidade_TipoTitleControlIdToReplace = "";
         AV33TFNaoConformidade_Tipo_Sels = new GxSimpleCollection();
         AV42Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV35DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV27NaoConformidade_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV31NaoConformidade_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_naoconformidade_nome_Filteredtext_set = "";
         Ddo_naoconformidade_nome_Selectedvalue_set = "";
         Ddo_naoconformidade_nome_Sortedstatus = "";
         Ddo_naoconformidade_tipo_Selectedvalue_set = "";
         Ddo_naoconformidade_tipo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV25Select = "";
         AV41Select_GXI = "";
         A427NaoConformidade_Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         lV17NaoConformidade_Nome1 = "";
         lV21NaoConformidade_Nome2 = "";
         lV28TFNaoConformidade_Nome = "";
         H00AM2_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00AM2_A429NaoConformidade_Tipo = new short[1] ;
         H00AM2_A427NaoConformidade_Nome = new String[] {""} ;
         H00AM2_A426NaoConformidade_Codigo = new int[1] ;
         H00AM3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV32TFNaoConformidade_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextnaoconformidade_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptnaoconformidade__default(),
            new Object[][] {
                new Object[] {
               H00AM2_A428NaoConformidade_AreaTrabalhoCod, H00AM2_A429NaoConformidade_Tipo, H00AM2_A427NaoConformidade_Nome, H00AM2_A426NaoConformidade_Codigo
               }
               , new Object[] {
               H00AM3_AGRID_nRecordCount
               }
            }
         );
         AV42Pgmname = "PromptNaoConformidade";
         /* GeneXus formulas. */
         AV42Pgmname = "PromptNaoConformidade";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_59 ;
      private short nGXsfl_59_idx=1 ;
      private short AV13OrderedBy ;
      private short AV18NaoConformidade_Tipo1 ;
      private short AV22NaoConformidade_Tipo2 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A429NaoConformidade_Tipo ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_59_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtNaoConformidade_Nome_Titleformat ;
      private short cmbNaoConformidade_Tipo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutNaoConformidade_Codigo ;
      private int wcpOAV7InOutNaoConformidade_Codigo ;
      private int subGrid_Rows ;
      private int AV15NaoConformidade_AreaTrabalhoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_naoconformidade_nome_Datalistupdateminimumcharacters ;
      private int Ddo_naoconformidade_tipo_Datalistupdateminimumcharacters ;
      private int edtavTfnaoconformidade_nome_Visible ;
      private int edtavTfnaoconformidade_nome_sel_Visible ;
      private int edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Visible ;
      private int A426NaoConformidade_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV33TFNaoConformidade_Tipo_Sels_Count ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A428NaoConformidade_AreaTrabalhoCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV36PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavNaoconformidade_nome1_Visible ;
      private int edtavNaoconformidade_nome2_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV37GridCurrentPage ;
      private long AV38GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV8InOutNaoConformidade_Nome ;
      private String wcpOAV8InOutNaoConformidade_Nome ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_naoconformidade_nome_Activeeventkey ;
      private String Ddo_naoconformidade_nome_Filteredtext_get ;
      private String Ddo_naoconformidade_nome_Selectedvalue_get ;
      private String Ddo_naoconformidade_tipo_Activeeventkey ;
      private String Ddo_naoconformidade_tipo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_59_idx="0001" ;
      private String AV17NaoConformidade_Nome1 ;
      private String AV21NaoConformidade_Nome2 ;
      private String AV28TFNaoConformidade_Nome ;
      private String AV29TFNaoConformidade_Nome_Sel ;
      private String AV42Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_naoconformidade_nome_Caption ;
      private String Ddo_naoconformidade_nome_Tooltip ;
      private String Ddo_naoconformidade_nome_Cls ;
      private String Ddo_naoconformidade_nome_Filteredtext_set ;
      private String Ddo_naoconformidade_nome_Selectedvalue_set ;
      private String Ddo_naoconformidade_nome_Dropdownoptionstype ;
      private String Ddo_naoconformidade_nome_Titlecontrolidtoreplace ;
      private String Ddo_naoconformidade_nome_Sortedstatus ;
      private String Ddo_naoconformidade_nome_Filtertype ;
      private String Ddo_naoconformidade_nome_Datalisttype ;
      private String Ddo_naoconformidade_nome_Datalistfixedvalues ;
      private String Ddo_naoconformidade_nome_Datalistproc ;
      private String Ddo_naoconformidade_nome_Sortasc ;
      private String Ddo_naoconformidade_nome_Sortdsc ;
      private String Ddo_naoconformidade_nome_Loadingdata ;
      private String Ddo_naoconformidade_nome_Cleanfilter ;
      private String Ddo_naoconformidade_nome_Rangefilterfrom ;
      private String Ddo_naoconformidade_nome_Rangefilterto ;
      private String Ddo_naoconformidade_nome_Noresultsfound ;
      private String Ddo_naoconformidade_nome_Searchbuttontext ;
      private String Ddo_naoconformidade_tipo_Caption ;
      private String Ddo_naoconformidade_tipo_Tooltip ;
      private String Ddo_naoconformidade_tipo_Cls ;
      private String Ddo_naoconformidade_tipo_Selectedvalue_set ;
      private String Ddo_naoconformidade_tipo_Dropdownoptionstype ;
      private String Ddo_naoconformidade_tipo_Titlecontrolidtoreplace ;
      private String Ddo_naoconformidade_tipo_Sortedstatus ;
      private String Ddo_naoconformidade_tipo_Datalisttype ;
      private String Ddo_naoconformidade_tipo_Datalistfixedvalues ;
      private String Ddo_naoconformidade_tipo_Sortasc ;
      private String Ddo_naoconformidade_tipo_Sortdsc ;
      private String Ddo_naoconformidade_tipo_Loadingdata ;
      private String Ddo_naoconformidade_tipo_Cleanfilter ;
      private String Ddo_naoconformidade_tipo_Rangefilterfrom ;
      private String Ddo_naoconformidade_tipo_Rangefilterto ;
      private String Ddo_naoconformidade_tipo_Noresultsfound ;
      private String Ddo_naoconformidade_tipo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfnaoconformidade_nome_Internalname ;
      private String edtavTfnaoconformidade_nome_Jsonclick ;
      private String edtavTfnaoconformidade_nome_sel_Internalname ;
      private String edtavTfnaoconformidade_nome_sel_Jsonclick ;
      private String edtavDdo_naoconformidade_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_naoconformidade_tipotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtNaoConformidade_Codigo_Internalname ;
      private String A427NaoConformidade_Nome ;
      private String edtNaoConformidade_Nome_Internalname ;
      private String cmbNaoConformidade_Tipo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV17NaoConformidade_Nome1 ;
      private String lV21NaoConformidade_Nome2 ;
      private String lV28TFNaoConformidade_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavNaoconformidade_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavNaoconformidade_nome1_Internalname ;
      private String cmbavNaoconformidade_tipo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavNaoconformidade_nome2_Internalname ;
      private String cmbavNaoconformidade_tipo2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_naoconformidade_nome_Internalname ;
      private String Ddo_naoconformidade_tipo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtNaoConformidade_Nome_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextnaoconformidade_areatrabalhocod_Internalname ;
      private String lblFiltertextnaoconformidade_areatrabalhocod_Jsonclick ;
      private String edtavNaoconformidade_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavNaoconformidade_nome1_Jsonclick ;
      private String cmbavNaoconformidade_tipo1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavNaoconformidade_nome2_Jsonclick ;
      private String cmbavNaoconformidade_tipo2_Jsonclick ;
      private String sGXsfl_59_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtNaoConformidade_Codigo_Jsonclick ;
      private String edtNaoConformidade_Nome_Jsonclick ;
      private String cmbNaoConformidade_Tipo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_naoconformidade_nome_Includesortasc ;
      private bool Ddo_naoconformidade_nome_Includesortdsc ;
      private bool Ddo_naoconformidade_nome_Includefilter ;
      private bool Ddo_naoconformidade_nome_Filterisrange ;
      private bool Ddo_naoconformidade_nome_Includedatalist ;
      private bool Ddo_naoconformidade_tipo_Includesortasc ;
      private bool Ddo_naoconformidade_tipo_Includesortdsc ;
      private bool Ddo_naoconformidade_tipo_Includefilter ;
      private bool Ddo_naoconformidade_tipo_Filterisrange ;
      private bool Ddo_naoconformidade_tipo_Includedatalist ;
      private bool Ddo_naoconformidade_tipo_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV25Select_IsBlob ;
      private String AV32TFNaoConformidade_Tipo_SelsJson ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV30ddo_NaoConformidade_NomeTitleControlIdToReplace ;
      private String AV34ddo_NaoConformidade_TipoTitleControlIdToReplace ;
      private String AV41Select_GXI ;
      private String AV25Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutNaoConformidade_Codigo ;
      private String aP1_InOutNaoConformidade_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavNaoconformidade_tipo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavNaoconformidade_tipo2 ;
      private GXCombobox cmbNaoConformidade_Tipo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H00AM2_A428NaoConformidade_AreaTrabalhoCod ;
      private short[] H00AM2_A429NaoConformidade_Tipo ;
      private String[] H00AM2_A427NaoConformidade_Nome ;
      private int[] H00AM2_A426NaoConformidade_Codigo ;
      private long[] H00AM3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV33TFNaoConformidade_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV27NaoConformidade_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV31NaoConformidade_TipoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV35DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptnaoconformidade__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00AM2( IGxContext context ,
                                             short A429NaoConformidade_Tipo ,
                                             IGxCollection AV33TFNaoConformidade_Tipo_Sels ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV17NaoConformidade_Nome1 ,
                                             short AV18NaoConformidade_Tipo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV21NaoConformidade_Nome2 ,
                                             short AV22NaoConformidade_Tipo2 ,
                                             String AV29TFNaoConformidade_Nome_Sel ,
                                             String AV28TFNaoConformidade_Nome ,
                                             int AV33TFNaoConformidade_Tipo_Sels_Count ,
                                             String A427NaoConformidade_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A428NaoConformidade_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [NaoConformidade_AreaTrabalhoCod], [NaoConformidade_Tipo], [NaoConformidade_Nome], [NaoConformidade_Codigo]";
         sFromString = " FROM [NaoConformidade] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([NaoConformidade_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17NaoConformidade_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like '%' + @lV17NaoConformidade_Nome1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_TIPO") == 0 ) && ( ! (0==AV18NaoConformidade_Tipo1) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Tipo] = @AV18NaoConformidade_Tipo1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21NaoConformidade_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like '%' + @lV21NaoConformidade_Nome2)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_TIPO") == 0 ) && ( ! (0==AV22NaoConformidade_Tipo2) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Tipo] = @AV22NaoConformidade_Tipo2)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV29TFNaoConformidade_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFNaoConformidade_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like @lV28TFNaoConformidade_Nome)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFNaoConformidade_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] = @AV29TFNaoConformidade_Nome_Sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV33TFNaoConformidade_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV33TFNaoConformidade_Tipo_Sels, "[NaoConformidade_Tipo] IN (", ")") + ")";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Tipo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Tipo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [NaoConformidade_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00AM3( IGxContext context ,
                                             short A429NaoConformidade_Tipo ,
                                             IGxCollection AV33TFNaoConformidade_Tipo_Sels ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV17NaoConformidade_Nome1 ,
                                             short AV18NaoConformidade_Tipo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV21NaoConformidade_Nome2 ,
                                             short AV22NaoConformidade_Tipo2 ,
                                             String AV29TFNaoConformidade_Nome_Sel ,
                                             String AV28TFNaoConformidade_Nome ,
                                             int AV33TFNaoConformidade_Tipo_Sels_Count ,
                                             String A427NaoConformidade_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A428NaoConformidade_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [NaoConformidade] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([NaoConformidade_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17NaoConformidade_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like '%' + @lV17NaoConformidade_Nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "NAOCONFORMIDADE_TIPO") == 0 ) && ( ! (0==AV18NaoConformidade_Tipo1) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Tipo] = @AV18NaoConformidade_Tipo1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21NaoConformidade_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like '%' + @lV21NaoConformidade_Nome2)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "NAOCONFORMIDADE_TIPO") == 0 ) && ( ! (0==AV22NaoConformidade_Tipo2) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Tipo] = @AV22NaoConformidade_Tipo2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV29TFNaoConformidade_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFNaoConformidade_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like @lV28TFNaoConformidade_Nome)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFNaoConformidade_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] = @AV29TFNaoConformidade_Nome_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV33TFNaoConformidade_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV33TFNaoConformidade_Tipo_Sels, "[NaoConformidade_Tipo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00AM2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (bool)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
               case 1 :
                     return conditional_H00AM3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (bool)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AM2 ;
          prmH00AM2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17NaoConformidade_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV18NaoConformidade_Tipo1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV21NaoConformidade_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV22NaoConformidade_Tipo2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV28TFNaoConformidade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV29TFNaoConformidade_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00AM3 ;
          prmH00AM3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17NaoConformidade_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV18NaoConformidade_Tipo1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV21NaoConformidade_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV22NaoConformidade_Tipo2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV28TFNaoConformidade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV29TFNaoConformidade_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AM2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AM2,11,0,true,false )
             ,new CursorDef("H00AM3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AM3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

}
