/*
               File: type_SdtSDT_UsuarioSistema
        Description: SDT_UsuarioSistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:8.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_UsuarioSistema" )]
   [XmlType(TypeName =  "SDT_UsuarioSistema" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_UsuarioSistema : GxUserType
   {
      public SdtSDT_UsuarioSistema( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_UsuarioSistema_Name = "";
         gxTv_SdtSDT_UsuarioSistema_Email = "";
         gxTv_SdtSDT_UsuarioSistema_Cpf = "";
         gxTv_SdtSDT_UsuarioSistema_Created_at = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_UsuarioSistema_Updated_at = (DateTime)(DateTime.MinValue);
      }

      public SdtSDT_UsuarioSistema( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_UsuarioSistema deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_UsuarioSistema)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_UsuarioSistema obj ;
         obj = this;
         obj.gxTpr_External_id = deserialized.gxTpr_External_id;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Email = deserialized.gxTpr_Email;
         obj.gxTpr_Cpf = deserialized.gxTpr_Cpf;
         obj.gxTpr_Created_at = deserialized.gxTpr_Created_at;
         obj.gxTpr_Updated_at = deserialized.gxTpr_Updated_at;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "external_id") )
               {
                  gxTv_SdtSDT_UsuarioSistema_External_id = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "name") )
               {
                  gxTv_SdtSDT_UsuarioSistema_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "email") )
               {
                  gxTv_SdtSDT_UsuarioSistema_Email = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "cpf") )
               {
                  gxTv_SdtSDT_UsuarioSistema_Cpf = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "created_at") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_UsuarioSistema_Created_at = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_UsuarioSistema_Created_at = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "updated_at") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_UsuarioSistema_Updated_at = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_UsuarioSistema_Updated_at = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_UsuarioSistema";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("external_id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_UsuarioSistema_External_id), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("name", StringUtil.RTrim( gxTv_SdtSDT_UsuarioSistema_Name));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("email", StringUtil.RTrim( gxTv_SdtSDT_UsuarioSistema_Email));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("cpf", StringUtil.RTrim( gxTv_SdtSDT_UsuarioSistema_Cpf));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_UsuarioSistema_Created_at) )
         {
            oWriter.WriteStartElement("created_at");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_UsuarioSistema_Created_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_UsuarioSistema_Created_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_UsuarioSistema_Created_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_UsuarioSistema_Created_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_UsuarioSistema_Created_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_UsuarioSistema_Created_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("created_at", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_UsuarioSistema_Updated_at) )
         {
            oWriter.WriteStartElement("updated_at");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_UsuarioSistema_Updated_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_UsuarioSistema_Updated_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_UsuarioSistema_Updated_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_UsuarioSistema_Updated_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_UsuarioSistema_Updated_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_UsuarioSistema_Updated_at)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("updated_at", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("external_id", gxTv_SdtSDT_UsuarioSistema_External_id, false);
         AddObjectProperty("name", gxTv_SdtSDT_UsuarioSistema_Name, false);
         AddObjectProperty("email", gxTv_SdtSDT_UsuarioSistema_Email, false);
         AddObjectProperty("cpf", gxTv_SdtSDT_UsuarioSistema_Cpf, false);
         datetime_STZ = gxTv_SdtSDT_UsuarioSistema_Created_at;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("created_at", sDateCnv, false);
         datetime_STZ = gxTv_SdtSDT_UsuarioSistema_Updated_at;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("updated_at", sDateCnv, false);
         return  ;
      }

      [  SoapElement( ElementName = "external_id" )]
      [  XmlElement( ElementName = "external_id"   )]
      public int gxTpr_External_id
      {
         get {
            return gxTv_SdtSDT_UsuarioSistema_External_id ;
         }

         set {
            gxTv_SdtSDT_UsuarioSistema_External_id = (int)(value);
         }

      }

      [  SoapElement( ElementName = "name" )]
      [  XmlElement( ElementName = "name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtSDT_UsuarioSistema_Name ;
         }

         set {
            gxTv_SdtSDT_UsuarioSistema_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "email" )]
      [  XmlElement( ElementName = "email"   )]
      public String gxTpr_Email
      {
         get {
            return gxTv_SdtSDT_UsuarioSistema_Email ;
         }

         set {
            gxTv_SdtSDT_UsuarioSistema_Email = (String)(value);
         }

      }

      [  SoapElement( ElementName = "cpf" )]
      [  XmlElement( ElementName = "cpf"   )]
      public String gxTpr_Cpf
      {
         get {
            return gxTv_SdtSDT_UsuarioSistema_Cpf ;
         }

         set {
            gxTv_SdtSDT_UsuarioSistema_Cpf = (String)(value);
         }

      }

      [  SoapElement( ElementName = "created_at" )]
      [  XmlElement( ElementName = "created_at"  , IsNullable=true )]
      public string gxTpr_Created_at_Nullable
      {
         get {
            if ( gxTv_SdtSDT_UsuarioSistema_Created_at == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_UsuarioSistema_Created_at).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_UsuarioSistema_Created_at = DateTime.MinValue;
            else
               gxTv_SdtSDT_UsuarioSistema_Created_at = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Created_at
      {
         get {
            return gxTv_SdtSDT_UsuarioSistema_Created_at ;
         }

         set {
            gxTv_SdtSDT_UsuarioSistema_Created_at = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "updated_at" )]
      [  XmlElement( ElementName = "updated_at"  , IsNullable=true )]
      public string gxTpr_Updated_at_Nullable
      {
         get {
            if ( gxTv_SdtSDT_UsuarioSistema_Updated_at == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_UsuarioSistema_Updated_at).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_UsuarioSistema_Updated_at = DateTime.MinValue;
            else
               gxTv_SdtSDT_UsuarioSistema_Updated_at = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Updated_at
      {
         get {
            return gxTv_SdtSDT_UsuarioSistema_Updated_at ;
         }

         set {
            gxTv_SdtSDT_UsuarioSistema_Updated_at = (DateTime)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_UsuarioSistema_Name = "";
         gxTv_SdtSDT_UsuarioSistema_Email = "";
         gxTv_SdtSDT_UsuarioSistema_Cpf = "";
         gxTv_SdtSDT_UsuarioSistema_Created_at = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_UsuarioSistema_Updated_at = (DateTime)(DateTime.MinValue);
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_UsuarioSistema_External_id ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtSDT_UsuarioSistema_Created_at ;
      protected DateTime gxTv_SdtSDT_UsuarioSistema_Updated_at ;
      protected DateTime datetime_STZ ;
      protected String gxTv_SdtSDT_UsuarioSistema_Name ;
      protected String gxTv_SdtSDT_UsuarioSistema_Email ;
      protected String gxTv_SdtSDT_UsuarioSistema_Cpf ;
   }

   [DataContract(Name = @"SDT_UsuarioSistema", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_UsuarioSistema_RESTInterface : GxGenericCollectionItem<SdtSDT_UsuarioSistema>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_UsuarioSistema_RESTInterface( ) : base()
      {
      }

      public SdtSDT_UsuarioSistema_RESTInterface( SdtSDT_UsuarioSistema psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "external_id" , Order = 0 )]
      public Nullable<int> gxTpr_External_id
      {
         get {
            return sdt.gxTpr_External_id ;
         }

         set {
            sdt.gxTpr_External_id = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "name" , Order = 1 )]
      public String gxTpr_Name
      {
         get {
            return sdt.gxTpr_Name ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "email" , Order = 2 )]
      public String gxTpr_Email
      {
         get {
            return sdt.gxTpr_Email ;
         }

         set {
            sdt.gxTpr_Email = (String)(value);
         }

      }

      [DataMember( Name = "cpf" , Order = 3 )]
      public String gxTpr_Cpf
      {
         get {
            return sdt.gxTpr_Cpf ;
         }

         set {
            sdt.gxTpr_Cpf = (String)(value);
         }

      }

      [DataMember( Name = "created_at" , Order = 4 )]
      public String gxTpr_Created_at
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Created_at) ;
         }

         set {
            sdt.gxTpr_Created_at = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "updated_at" , Order = 5 )]
      public String gxTpr_Updated_at
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Updated_at) ;
         }

         set {
            sdt.gxTpr_Updated_at = DateTimeUtil.CToT2( (String)(value));
         }

      }

      public SdtSDT_UsuarioSistema sdt
      {
         get {
            return (SdtSDT_UsuarioSistema)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_UsuarioSistema() ;
         }
      }

   }

}
