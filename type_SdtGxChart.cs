/*
               File: type_SdtGxChart
        Description: GxChart
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:56.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GxChart" )]
   [XmlType(TypeName =  "GxChart" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtGxChart_Serie ))]
   [Serializable]
   public class SdtGxChart : GxUserType
   {
      public SdtGxChart( )
      {
         /* Constructor for serialization */
      }

      public SdtGxChart( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGxChart deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGxChart)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGxChart obj ;
         obj = this;
         obj.gxTpr_Categories = deserialized.gxTpr_Categories;
         obj.gxTpr_Series = deserialized.gxTpr_Series;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Categories") )
               {
                  if ( gxTv_SdtGxChart_Categories == null )
                  {
                     gxTv_SdtGxChart_Categories = new GxSimpleCollection();
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtGxChart_Categories.readxmlcollection(oReader, "Categories", "Item");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Series") )
               {
                  if ( gxTv_SdtGxChart_Series == null )
                  {
                     gxTv_SdtGxChart_Series = new GxObjectCollection( context, "GxChart.Serie", "GxEv3Up14_MeetrikaVs3", "SdtGxChart_Serie", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtGxChart_Series.readxmlcollection(oReader, "Series", "Serie");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GxChart";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         if ( gxTv_SdtGxChart_Categories != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtGxChart_Categories.writexmlcollection(oWriter, "Categories", sNameSpace1, "Item", sNameSpace1);
         }
         if ( gxTv_SdtGxChart_Series != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtGxChart_Series.writexmlcollection(oWriter, "Series", sNameSpace1, "Serie", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         if ( gxTv_SdtGxChart_Categories != null )
         {
            AddObjectProperty("Categories", gxTv_SdtGxChart_Categories, false);
         }
         if ( gxTv_SdtGxChart_Series != null )
         {
            AddObjectProperty("Series", gxTv_SdtGxChart_Series, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Categories" )]
      [  XmlArray( ElementName = "Categories"  )]
      [  XmlArrayItemAttribute( Type= typeof( String ), ElementName= "Item"  , IsNullable=false)]
      public GxSimpleCollection gxTpr_Categories_GxSimpleCollection
      {
         get {
            if ( gxTv_SdtGxChart_Categories == null )
            {
               gxTv_SdtGxChart_Categories = new GxSimpleCollection();
            }
            return (GxSimpleCollection)gxTv_SdtGxChart_Categories ;
         }

         set {
            if ( gxTv_SdtGxChart_Categories == null )
            {
               gxTv_SdtGxChart_Categories = new GxSimpleCollection();
            }
            gxTv_SdtGxChart_Categories = (GxSimpleCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Categories
      {
         get {
            if ( gxTv_SdtGxChart_Categories == null )
            {
               gxTv_SdtGxChart_Categories = new GxSimpleCollection();
            }
            return gxTv_SdtGxChart_Categories ;
         }

         set {
            gxTv_SdtGxChart_Categories = value;
         }

      }

      public void gxTv_SdtGxChart_Categories_SetNull( )
      {
         gxTv_SdtGxChart_Categories = null;
         return  ;
      }

      public bool gxTv_SdtGxChart_Categories_IsNull( )
      {
         if ( gxTv_SdtGxChart_Categories == null )
         {
            return true ;
         }
         return false ;
      }

      public class gxTv_SdtGxChart_Series_SdtGxChart_Serie_80compatibility:SdtGxChart_Serie {}
      [  SoapElement( ElementName = "Series" )]
      [  XmlArray( ElementName = "Series"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtGxChart_Serie ), ElementName= "Serie"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtGxChart_Series_SdtGxChart_Serie_80compatibility ), ElementName= "GxChart.Serie"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Series_GxObjectCollection
      {
         get {
            if ( gxTv_SdtGxChart_Series == null )
            {
               gxTv_SdtGxChart_Series = new GxObjectCollection( context, "GxChart.Serie", "GxEv3Up14_MeetrikaVs3", "SdtGxChart_Serie", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtGxChart_Series ;
         }

         set {
            if ( gxTv_SdtGxChart_Series == null )
            {
               gxTv_SdtGxChart_Series = new GxObjectCollection( context, "GxChart.Serie", "GxEv3Up14_MeetrikaVs3", "SdtGxChart_Serie", "GeneXus.Programs");
            }
            gxTv_SdtGxChart_Series = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Series
      {
         get {
            if ( gxTv_SdtGxChart_Series == null )
            {
               gxTv_SdtGxChart_Series = new GxObjectCollection( context, "GxChart.Serie", "GxEv3Up14_MeetrikaVs3", "SdtGxChart_Serie", "GeneXus.Programs");
            }
            return gxTv_SdtGxChart_Series ;
         }

         set {
            gxTv_SdtGxChart_Series = value;
         }

      }

      public void gxTv_SdtGxChart_Series_SetNull( )
      {
         gxTv_SdtGxChart_Series = null;
         return  ;
      }

      public bool gxTv_SdtGxChart_Series_IsNull( )
      {
         if ( gxTv_SdtGxChart_Series == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( String ))]
      protected IGxCollection gxTv_SdtGxChart_Categories=null ;
      [ObjectCollection(ItemType=typeof( SdtGxChart_Serie ))]
      protected IGxCollection gxTv_SdtGxChart_Series=null ;
   }

   [DataContract(Name = @"GxChart", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtGxChart_RESTInterface : GxGenericCollectionItem<SdtGxChart>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGxChart_RESTInterface( ) : base()
      {
      }

      public SdtGxChart_RESTInterface( SdtGxChart psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Categories" , Order = 0 )]
      public GxSimpleCollection gxTpr_Categories
      {
         get {
            return (GxSimpleCollection)(sdt.gxTpr_Categories) ;
         }

         set {
            sdt.gxTpr_Categories = value;
         }

      }

      [DataMember( Name = "Series" , Order = 1 )]
      public GxGenericCollection<SdtGxChart_Serie_RESTInterface> gxTpr_Series
      {
         get {
            return new GxGenericCollection<SdtGxChart_Serie_RESTInterface>(sdt.gxTpr_Series) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Series);
         }

      }

      public SdtGxChart sdt
      {
         get {
            return (SdtGxChart)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGxChart() ;
         }
      }

   }

}
