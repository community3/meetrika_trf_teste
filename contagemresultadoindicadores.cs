/*
               File: ContagemResultadoIndicadores
        Description: Contagem Resultado Indicadores
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:34.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoindicadores : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A1314ContagemResultadoIndicadores_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A1314ContagemResultadoIndicadores_DemandaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_6") == 0 )
         {
            A1315ContagemResultadoIndicadores_IndicadorCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_6( A1315ContagemResultadoIndicadores_IndicadorCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A1316ContagemResultadoIndicadores_LoteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1316ContagemResultadoIndicadores_LoteCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1316ContagemResultadoIndicadores_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A1316ContagemResultadoIndicadores_LoteCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynContagemResultadoIndicadores_LoteCod.Name = "CONTAGEMRESULTADOINDICADORES_LOTECOD";
         dynContagemResultadoIndicadores_LoteCod.WebTags = "";
         dynContagemResultadoIndicadores_LoteCod.removeAllItems();
         /* Using cursor T003J7 */
         pr_default.execute(5);
         while ( (pr_default.getStatus(5) != 101) )
         {
            dynContagemResultadoIndicadores_LoteCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T003J7_A596Lote_Codigo[0]), 6, 0)), T003J7_A563Lote_Nome[0], 0);
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( dynContagemResultadoIndicadores_LoteCod.ItemCount > 0 )
         {
            A1316ContagemResultadoIndicadores_LoteCod = (int)(NumberUtil.Val( dynContagemResultadoIndicadores_LoteCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0))), "."));
            n1316ContagemResultadoIndicadores_LoteCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1316ContagemResultadoIndicadores_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado Indicadores", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadoindicadores( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoindicadores( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContagemResultadoIndicadores_LoteCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContagemResultadoIndicadores_LoteCod.ItemCount > 0 )
         {
            A1316ContagemResultadoIndicadores_LoteCod = (int)(NumberUtil.Val( dynContagemResultadoIndicadores_LoteCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0))), "."));
            n1316ContagemResultadoIndicadores_LoteCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1316ContagemResultadoIndicadores_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3J160( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3J160e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3J160( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3J160( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3J160e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Resultado Indicadores", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemResultadoIndicadores.htm");
            wb_table3_28_3J160( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_3J160e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3J160e( true) ;
         }
         else
         {
            wb_table1_2_3J160e( false) ;
         }
      }

      protected void wb_table3_28_3J160( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_3J160( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_3J160e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoIndicadores.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoIndicadores.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_3J160e( true) ;
         }
         else
         {
            wb_table3_28_3J160e( false) ;
         }
      }

      protected void wb_table4_34_3J160( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoindicadores_demandacod_Internalname, "Demanda", "", "", lblTextblockcontagemresultadoindicadores_demandacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoIndicadores_DemandaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0, ",", "")), ((edtContagemResultadoIndicadores_DemandaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoIndicadores_DemandaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoIndicadores_DemandaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoindicadores_indicadorcod_Internalname, "Indicador", "", "", lblTextblockcontagemresultadoindicadores_indicadorcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoIndicadores_IndicadorCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0, ",", "")), ((edtContagemResultadoIndicadores_IndicadorCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoIndicadores_IndicadorCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoIndicadores_IndicadorCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoindicadores_data_Internalname, "Data", "", "", lblTextblockcontagemresultadoindicadores_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoIndicadores_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoIndicadores_Data_Internalname, context.localUtil.Format(A1317ContagemResultadoIndicadores_Data, "99/99/99"), context.localUtil.Format( A1317ContagemResultadoIndicadores_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoIndicadores_Data_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoIndicadores_Data_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoIndicadores.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoIndicadores_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoIndicadores_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoindicadores_solicitada_Internalname, "Solicitada", "", "", lblTextblockcontagemresultadoindicadores_solicitada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoIndicadores_Solicitada_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoIndicadores_Solicitada_Internalname, context.localUtil.Format(A1318ContagemResultadoIndicadores_Solicitada, "99/99/99"), context.localUtil.Format( A1318ContagemResultadoIndicadores_Solicitada, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoIndicadores_Solicitada_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoIndicadores_Solicitada_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoIndicadores.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoIndicadores_Solicitada_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoIndicadores_Solicitada_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoindicadores_prazo_Internalname, "Prazo", "", "", lblTextblockcontagemresultadoindicadores_prazo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoIndicadores_Prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1319ContagemResultadoIndicadores_Prazo), 4, 0, ",", "")), ((edtContagemResultadoIndicadores_Prazo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1319ContagemResultadoIndicadores_Prazo), "ZZZ9")) : context.localUtil.Format( (decimal)(A1319ContagemResultadoIndicadores_Prazo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoIndicadores_Prazo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoIndicadores_Prazo_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoindicadores_entregue_Internalname, "Entregue", "", "", lblTextblockcontagemresultadoindicadores_entregue_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoIndicadores_Entregue_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoIndicadores_Entregue_Internalname, context.localUtil.Format(A1324ContagemResultadoIndicadores_Entregue, "99/99/99"), context.localUtil.Format( A1324ContagemResultadoIndicadores_Entregue, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoIndicadores_Entregue_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoIndicadores_Entregue_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoIndicadores.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoIndicadores_Entregue_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoIndicadores_Entregue_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoindicadores_atraso_Internalname, "Atraso", "", "", lblTextblockcontagemresultadoindicadores_atraso_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoIndicadores_Atraso_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1321ContagemResultadoIndicadores_Atraso), 4, 0, ",", "")), ((edtContagemResultadoIndicadores_Atraso_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1321ContagemResultadoIndicadores_Atraso), "ZZZ9")) : context.localUtil.Format( (decimal)(A1321ContagemResultadoIndicadores_Atraso), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoIndicadores_Atraso_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoIndicadores_Atraso_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoindicadores_reduz_Internalname, "Reduz", "", "", lblTextblockcontagemresultadoindicadores_reduz_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoIndicadores_Reduz_Internalname, StringUtil.LTrim( StringUtil.NToC( A1322ContagemResultadoIndicadores_Reduz, 6, 2, ",", "")), ((edtContagemResultadoIndicadores_Reduz_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1322ContagemResultadoIndicadores_Reduz, "ZZ9.99")) : context.localUtil.Format( A1322ContagemResultadoIndicadores_Reduz, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoIndicadores_Reduz_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoIndicadores_Reduz_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoindicadores_valor_Internalname, "R$", "", "", lblTextblockcontagemresultadoindicadores_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoIndicadores_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A1323ContagemResultadoIndicadores_Valor, 18, 5, ",", "")), ((edtContagemResultadoIndicadores_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1323ContagemResultadoIndicadores_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1323ContagemResultadoIndicadores_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoIndicadores_Valor_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoIndicadores_Valor_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoindicadores_lotecod_Internalname, "Nome", "", "", lblTextblockcontagemresultadoindicadores_lotecod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoIndicadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagemResultadoIndicadores_LoteCod, dynContagemResultadoIndicadores_LoteCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0)), 1, dynContagemResultadoIndicadores_LoteCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContagemResultadoIndicadores_LoteCod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", "", true, "HLP_ContagemResultadoIndicadores.htm");
            dynContagemResultadoIndicadores_LoteCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultadoIndicadores_LoteCod_Internalname, "Values", (String)(dynContagemResultadoIndicadores_LoteCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_3J160e( true) ;
         }
         else
         {
            wb_table4_34_3J160e( false) ;
         }
      }

      protected void wb_table2_5_3J160( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoIndicadores.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3J160e( true) ;
         }
         else
         {
            wb_table2_5_3J160e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_DemandaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_DemandaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOINDICADORES_DEMANDACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1314ContagemResultadoIndicadores_DemandaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
               }
               else
               {
                  A1314ContagemResultadoIndicadores_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_DemandaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_IndicadorCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_IndicadorCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOINDICADORES_INDICADORCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoIndicadores_IndicadorCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1315ContagemResultadoIndicadores_IndicadorCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
               }
               else
               {
                  A1315ContagemResultadoIndicadores_IndicadorCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_IndicadorCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContagemResultadoIndicadores_Data_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data"}), 1, "CONTAGEMRESULTADOINDICADORES_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoIndicadores_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1317ContagemResultadoIndicadores_Data = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1317ContagemResultadoIndicadores_Data", context.localUtil.Format(A1317ContagemResultadoIndicadores_Data, "99/99/99"));
               }
               else
               {
                  A1317ContagemResultadoIndicadores_Data = context.localUtil.CToD( cgiGet( edtContagemResultadoIndicadores_Data_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1317ContagemResultadoIndicadores_Data", context.localUtil.Format(A1317ContagemResultadoIndicadores_Data, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContagemResultadoIndicadores_Solicitada_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Solicitada"}), 1, "CONTAGEMRESULTADOINDICADORES_SOLICITADA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoIndicadores_Solicitada_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1318ContagemResultadoIndicadores_Solicitada = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1318ContagemResultadoIndicadores_Solicitada", context.localUtil.Format(A1318ContagemResultadoIndicadores_Solicitada, "99/99/99"));
               }
               else
               {
                  A1318ContagemResultadoIndicadores_Solicitada = context.localUtil.CToD( cgiGet( edtContagemResultadoIndicadores_Solicitada_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1318ContagemResultadoIndicadores_Solicitada", context.localUtil.Format(A1318ContagemResultadoIndicadores_Solicitada, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Prazo_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOINDICADORES_PRAZO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoIndicadores_Prazo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1319ContagemResultadoIndicadores_Prazo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1319ContagemResultadoIndicadores_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1319ContagemResultadoIndicadores_Prazo), 4, 0)));
               }
               else
               {
                  A1319ContagemResultadoIndicadores_Prazo = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Prazo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1319ContagemResultadoIndicadores_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1319ContagemResultadoIndicadores_Prazo), 4, 0)));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContagemResultadoIndicadores_Entregue_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Entregue"}), 1, "CONTAGEMRESULTADOINDICADORES_ENTREGUE");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoIndicadores_Entregue_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1324ContagemResultadoIndicadores_Entregue = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1324ContagemResultadoIndicadores_Entregue", context.localUtil.Format(A1324ContagemResultadoIndicadores_Entregue, "99/99/99"));
               }
               else
               {
                  A1324ContagemResultadoIndicadores_Entregue = context.localUtil.CToD( cgiGet( edtContagemResultadoIndicadores_Entregue_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1324ContagemResultadoIndicadores_Entregue", context.localUtil.Format(A1324ContagemResultadoIndicadores_Entregue, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Atraso_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Atraso_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOINDICADORES_ATRASO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoIndicadores_Atraso_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1321ContagemResultadoIndicadores_Atraso = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1321ContagemResultadoIndicadores_Atraso", StringUtil.LTrim( StringUtil.Str( (decimal)(A1321ContagemResultadoIndicadores_Atraso), 4, 0)));
               }
               else
               {
                  A1321ContagemResultadoIndicadores_Atraso = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Atraso_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1321ContagemResultadoIndicadores_Atraso", StringUtil.LTrim( StringUtil.Str( (decimal)(A1321ContagemResultadoIndicadores_Atraso), 4, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Reduz_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Reduz_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOINDICADORES_REDUZ");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoIndicadores_Reduz_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1322ContagemResultadoIndicadores_Reduz = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1322ContagemResultadoIndicadores_Reduz", StringUtil.LTrim( StringUtil.Str( A1322ContagemResultadoIndicadores_Reduz, 6, 2)));
               }
               else
               {
                  A1322ContagemResultadoIndicadores_Reduz = context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Reduz_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1322ContagemResultadoIndicadores_Reduz", StringUtil.LTrim( StringUtil.Str( A1322ContagemResultadoIndicadores_Reduz, 6, 2)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOINDICADORES_VALOR");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoIndicadores_Valor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1323ContagemResultadoIndicadores_Valor = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1323ContagemResultadoIndicadores_Valor", StringUtil.LTrim( StringUtil.Str( A1323ContagemResultadoIndicadores_Valor, 18, 5)));
               }
               else
               {
                  A1323ContagemResultadoIndicadores_Valor = context.localUtil.CToN( cgiGet( edtContagemResultadoIndicadores_Valor_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1323ContagemResultadoIndicadores_Valor", StringUtil.LTrim( StringUtil.Str( A1323ContagemResultadoIndicadores_Valor, 18, 5)));
               }
               dynContagemResultadoIndicadores_LoteCod.CurrentValue = cgiGet( dynContagemResultadoIndicadores_LoteCod_Internalname);
               A1316ContagemResultadoIndicadores_LoteCod = (int)(NumberUtil.Val( cgiGet( dynContagemResultadoIndicadores_LoteCod_Internalname), "."));
               n1316ContagemResultadoIndicadores_LoteCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1316ContagemResultadoIndicadores_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0)));
               n1316ContagemResultadoIndicadores_LoteCod = ((0==A1316ContagemResultadoIndicadores_LoteCod) ? true : false);
               /* Read saved values. */
               Z1314ContagemResultadoIndicadores_DemandaCod = (int)(context.localUtil.CToN( cgiGet( "Z1314ContagemResultadoIndicadores_DemandaCod"), ",", "."));
               Z1315ContagemResultadoIndicadores_IndicadorCod = (int)(context.localUtil.CToN( cgiGet( "Z1315ContagemResultadoIndicadores_IndicadorCod"), ",", "."));
               Z1317ContagemResultadoIndicadores_Data = context.localUtil.CToD( cgiGet( "Z1317ContagemResultadoIndicadores_Data"), 0);
               Z1318ContagemResultadoIndicadores_Solicitada = context.localUtil.CToD( cgiGet( "Z1318ContagemResultadoIndicadores_Solicitada"), 0);
               Z1319ContagemResultadoIndicadores_Prazo = (short)(context.localUtil.CToN( cgiGet( "Z1319ContagemResultadoIndicadores_Prazo"), ",", "."));
               Z1324ContagemResultadoIndicadores_Entregue = context.localUtil.CToD( cgiGet( "Z1324ContagemResultadoIndicadores_Entregue"), 0);
               Z1321ContagemResultadoIndicadores_Atraso = (short)(context.localUtil.CToN( cgiGet( "Z1321ContagemResultadoIndicadores_Atraso"), ",", "."));
               Z1322ContagemResultadoIndicadores_Reduz = context.localUtil.CToN( cgiGet( "Z1322ContagemResultadoIndicadores_Reduz"), ",", ".");
               Z1323ContagemResultadoIndicadores_Valor = context.localUtil.CToN( cgiGet( "Z1323ContagemResultadoIndicadores_Valor"), ",", ".");
               Z1316ContagemResultadoIndicadores_LoteCod = (int)(context.localUtil.CToN( cgiGet( "Z1316ContagemResultadoIndicadores_LoteCod"), ",", "."));
               n1316ContagemResultadoIndicadores_LoteCod = ((0==A1316ContagemResultadoIndicadores_LoteCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1314ContagemResultadoIndicadores_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
                  A1315ContagemResultadoIndicadores_IndicadorCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3J160( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes3J160( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption3J0( )
      {
      }

      protected void ZM3J160( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1317ContagemResultadoIndicadores_Data = T003J3_A1317ContagemResultadoIndicadores_Data[0];
               Z1318ContagemResultadoIndicadores_Solicitada = T003J3_A1318ContagemResultadoIndicadores_Solicitada[0];
               Z1319ContagemResultadoIndicadores_Prazo = T003J3_A1319ContagemResultadoIndicadores_Prazo[0];
               Z1324ContagemResultadoIndicadores_Entregue = T003J3_A1324ContagemResultadoIndicadores_Entregue[0];
               Z1321ContagemResultadoIndicadores_Atraso = T003J3_A1321ContagemResultadoIndicadores_Atraso[0];
               Z1322ContagemResultadoIndicadores_Reduz = T003J3_A1322ContagemResultadoIndicadores_Reduz[0];
               Z1323ContagemResultadoIndicadores_Valor = T003J3_A1323ContagemResultadoIndicadores_Valor[0];
               Z1316ContagemResultadoIndicadores_LoteCod = T003J3_A1316ContagemResultadoIndicadores_LoteCod[0];
            }
            else
            {
               Z1317ContagemResultadoIndicadores_Data = A1317ContagemResultadoIndicadores_Data;
               Z1318ContagemResultadoIndicadores_Solicitada = A1318ContagemResultadoIndicadores_Solicitada;
               Z1319ContagemResultadoIndicadores_Prazo = A1319ContagemResultadoIndicadores_Prazo;
               Z1324ContagemResultadoIndicadores_Entregue = A1324ContagemResultadoIndicadores_Entregue;
               Z1321ContagemResultadoIndicadores_Atraso = A1321ContagemResultadoIndicadores_Atraso;
               Z1322ContagemResultadoIndicadores_Reduz = A1322ContagemResultadoIndicadores_Reduz;
               Z1323ContagemResultadoIndicadores_Valor = A1323ContagemResultadoIndicadores_Valor;
               Z1316ContagemResultadoIndicadores_LoteCod = A1316ContagemResultadoIndicadores_LoteCod;
            }
         }
         if ( GX_JID == -4 )
         {
            Z1317ContagemResultadoIndicadores_Data = A1317ContagemResultadoIndicadores_Data;
            Z1318ContagemResultadoIndicadores_Solicitada = A1318ContagemResultadoIndicadores_Solicitada;
            Z1319ContagemResultadoIndicadores_Prazo = A1319ContagemResultadoIndicadores_Prazo;
            Z1324ContagemResultadoIndicadores_Entregue = A1324ContagemResultadoIndicadores_Entregue;
            Z1321ContagemResultadoIndicadores_Atraso = A1321ContagemResultadoIndicadores_Atraso;
            Z1322ContagemResultadoIndicadores_Reduz = A1322ContagemResultadoIndicadores_Reduz;
            Z1323ContagemResultadoIndicadores_Valor = A1323ContagemResultadoIndicadores_Valor;
            Z1314ContagemResultadoIndicadores_DemandaCod = A1314ContagemResultadoIndicadores_DemandaCod;
            Z1315ContagemResultadoIndicadores_IndicadorCod = A1315ContagemResultadoIndicadores_IndicadorCod;
            Z1316ContagemResultadoIndicadores_LoteCod = A1316ContagemResultadoIndicadores_LoteCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load3J160( )
      {
         /* Using cursor T003J8 */
         pr_default.execute(6, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound160 = 1;
            A1317ContagemResultadoIndicadores_Data = T003J8_A1317ContagemResultadoIndicadores_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1317ContagemResultadoIndicadores_Data", context.localUtil.Format(A1317ContagemResultadoIndicadores_Data, "99/99/99"));
            A1318ContagemResultadoIndicadores_Solicitada = T003J8_A1318ContagemResultadoIndicadores_Solicitada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1318ContagemResultadoIndicadores_Solicitada", context.localUtil.Format(A1318ContagemResultadoIndicadores_Solicitada, "99/99/99"));
            A1319ContagemResultadoIndicadores_Prazo = T003J8_A1319ContagemResultadoIndicadores_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1319ContagemResultadoIndicadores_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1319ContagemResultadoIndicadores_Prazo), 4, 0)));
            A1324ContagemResultadoIndicadores_Entregue = T003J8_A1324ContagemResultadoIndicadores_Entregue[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1324ContagemResultadoIndicadores_Entregue", context.localUtil.Format(A1324ContagemResultadoIndicadores_Entregue, "99/99/99"));
            A1321ContagemResultadoIndicadores_Atraso = T003J8_A1321ContagemResultadoIndicadores_Atraso[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1321ContagemResultadoIndicadores_Atraso", StringUtil.LTrim( StringUtil.Str( (decimal)(A1321ContagemResultadoIndicadores_Atraso), 4, 0)));
            A1322ContagemResultadoIndicadores_Reduz = T003J8_A1322ContagemResultadoIndicadores_Reduz[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1322ContagemResultadoIndicadores_Reduz", StringUtil.LTrim( StringUtil.Str( A1322ContagemResultadoIndicadores_Reduz, 6, 2)));
            A1323ContagemResultadoIndicadores_Valor = T003J8_A1323ContagemResultadoIndicadores_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1323ContagemResultadoIndicadores_Valor", StringUtil.LTrim( StringUtil.Str( A1323ContagemResultadoIndicadores_Valor, 18, 5)));
            A1316ContagemResultadoIndicadores_LoteCod = T003J8_A1316ContagemResultadoIndicadores_LoteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1316ContagemResultadoIndicadores_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0)));
            n1316ContagemResultadoIndicadores_LoteCod = T003J8_n1316ContagemResultadoIndicadores_LoteCod[0];
            ZM3J160( -4) ;
         }
         pr_default.close(6);
         OnLoadActions3J160( ) ;
      }

      protected void OnLoadActions3J160( )
      {
      }

      protected void CheckExtendedTable3J160( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003J4 */
         pr_default.execute(2, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Contagem Resultado Indicadores'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_DEMANDACOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T003J5 */
         pr_default.execute(3, new Object[] {A1315ContagemResultadoIndicadores_IndicadorCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resiltado Indicadores_Contrato Servicos Indicador'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_INDICADORCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_IndicadorCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A1317ContagemResultadoIndicadores_Data) || ( A1317ContagemResultadoIndicadores_Data >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOINDICADORES_DATA");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A1318ContagemResultadoIndicadores_Solicitada) || ( A1318ContagemResultadoIndicadores_Solicitada >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Solicitada fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOINDICADORES_SOLICITADA");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_Solicitada_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A1324ContagemResultadoIndicadores_Entregue) || ( A1324ContagemResultadoIndicadores_Entregue >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Entregue fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOINDICADORES_ENTREGUE");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_Entregue_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T003J6 */
         pr_default.execute(4, new Object[] {n1316ContagemResultadoIndicadores_LoteCod, A1316ContagemResultadoIndicadores_LoteCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A1316ContagemResultadoIndicadores_LoteCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resutlado Indicadores_Lote'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_LOTECOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultadoIndicadores_LoteCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors3J160( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_5( int A1314ContagemResultadoIndicadores_DemandaCod )
      {
         /* Using cursor T003J9 */
         pr_default.execute(7, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Contagem Resultado Indicadores'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_DEMANDACOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_6( int A1315ContagemResultadoIndicadores_IndicadorCod )
      {
         /* Using cursor T003J10 */
         pr_default.execute(8, new Object[] {A1315ContagemResultadoIndicadores_IndicadorCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resiltado Indicadores_Contrato Servicos Indicador'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_INDICADORCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_IndicadorCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_7( int A1316ContagemResultadoIndicadores_LoteCod )
      {
         /* Using cursor T003J11 */
         pr_default.execute(9, new Object[] {n1316ContagemResultadoIndicadores_LoteCod, A1316ContagemResultadoIndicadores_LoteCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A1316ContagemResultadoIndicadores_LoteCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resutlado Indicadores_Lote'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_LOTECOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultadoIndicadores_LoteCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void GetKey3J160( )
      {
         /* Using cursor T003J12 */
         pr_default.execute(10, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound160 = 1;
         }
         else
         {
            RcdFound160 = 0;
         }
         pr_default.close(10);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003J3 */
         pr_default.execute(1, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3J160( 4) ;
            RcdFound160 = 1;
            A1317ContagemResultadoIndicadores_Data = T003J3_A1317ContagemResultadoIndicadores_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1317ContagemResultadoIndicadores_Data", context.localUtil.Format(A1317ContagemResultadoIndicadores_Data, "99/99/99"));
            A1318ContagemResultadoIndicadores_Solicitada = T003J3_A1318ContagemResultadoIndicadores_Solicitada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1318ContagemResultadoIndicadores_Solicitada", context.localUtil.Format(A1318ContagemResultadoIndicadores_Solicitada, "99/99/99"));
            A1319ContagemResultadoIndicadores_Prazo = T003J3_A1319ContagemResultadoIndicadores_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1319ContagemResultadoIndicadores_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1319ContagemResultadoIndicadores_Prazo), 4, 0)));
            A1324ContagemResultadoIndicadores_Entregue = T003J3_A1324ContagemResultadoIndicadores_Entregue[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1324ContagemResultadoIndicadores_Entregue", context.localUtil.Format(A1324ContagemResultadoIndicadores_Entregue, "99/99/99"));
            A1321ContagemResultadoIndicadores_Atraso = T003J3_A1321ContagemResultadoIndicadores_Atraso[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1321ContagemResultadoIndicadores_Atraso", StringUtil.LTrim( StringUtil.Str( (decimal)(A1321ContagemResultadoIndicadores_Atraso), 4, 0)));
            A1322ContagemResultadoIndicadores_Reduz = T003J3_A1322ContagemResultadoIndicadores_Reduz[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1322ContagemResultadoIndicadores_Reduz", StringUtil.LTrim( StringUtil.Str( A1322ContagemResultadoIndicadores_Reduz, 6, 2)));
            A1323ContagemResultadoIndicadores_Valor = T003J3_A1323ContagemResultadoIndicadores_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1323ContagemResultadoIndicadores_Valor", StringUtil.LTrim( StringUtil.Str( A1323ContagemResultadoIndicadores_Valor, 18, 5)));
            A1314ContagemResultadoIndicadores_DemandaCod = T003J3_A1314ContagemResultadoIndicadores_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
            A1315ContagemResultadoIndicadores_IndicadorCod = T003J3_A1315ContagemResultadoIndicadores_IndicadorCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
            A1316ContagemResultadoIndicadores_LoteCod = T003J3_A1316ContagemResultadoIndicadores_LoteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1316ContagemResultadoIndicadores_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0)));
            n1316ContagemResultadoIndicadores_LoteCod = T003J3_n1316ContagemResultadoIndicadores_LoteCod[0];
            Z1314ContagemResultadoIndicadores_DemandaCod = A1314ContagemResultadoIndicadores_DemandaCod;
            Z1315ContagemResultadoIndicadores_IndicadorCod = A1315ContagemResultadoIndicadores_IndicadorCod;
            sMode160 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load3J160( ) ;
            if ( AnyError == 1 )
            {
               RcdFound160 = 0;
               InitializeNonKey3J160( ) ;
            }
            Gx_mode = sMode160;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound160 = 0;
            InitializeNonKey3J160( ) ;
            sMode160 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode160;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3J160( ) ;
         if ( RcdFound160 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound160 = 0;
         /* Using cursor T003J13 */
         pr_default.execute(11, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T003J13_A1314ContagemResultadoIndicadores_DemandaCod[0] < A1314ContagemResultadoIndicadores_DemandaCod ) || ( T003J13_A1314ContagemResultadoIndicadores_DemandaCod[0] == A1314ContagemResultadoIndicadores_DemandaCod ) && ( T003J13_A1315ContagemResultadoIndicadores_IndicadorCod[0] < A1315ContagemResultadoIndicadores_IndicadorCod ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T003J13_A1314ContagemResultadoIndicadores_DemandaCod[0] > A1314ContagemResultadoIndicadores_DemandaCod ) || ( T003J13_A1314ContagemResultadoIndicadores_DemandaCod[0] == A1314ContagemResultadoIndicadores_DemandaCod ) && ( T003J13_A1315ContagemResultadoIndicadores_IndicadorCod[0] > A1315ContagemResultadoIndicadores_IndicadorCod ) ) )
            {
               A1314ContagemResultadoIndicadores_DemandaCod = T003J13_A1314ContagemResultadoIndicadores_DemandaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
               A1315ContagemResultadoIndicadores_IndicadorCod = T003J13_A1315ContagemResultadoIndicadores_IndicadorCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
               RcdFound160 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void move_previous( )
      {
         RcdFound160 = 0;
         /* Using cursor T003J14 */
         pr_default.execute(12, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T003J14_A1314ContagemResultadoIndicadores_DemandaCod[0] > A1314ContagemResultadoIndicadores_DemandaCod ) || ( T003J14_A1314ContagemResultadoIndicadores_DemandaCod[0] == A1314ContagemResultadoIndicadores_DemandaCod ) && ( T003J14_A1315ContagemResultadoIndicadores_IndicadorCod[0] > A1315ContagemResultadoIndicadores_IndicadorCod ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T003J14_A1314ContagemResultadoIndicadores_DemandaCod[0] < A1314ContagemResultadoIndicadores_DemandaCod ) || ( T003J14_A1314ContagemResultadoIndicadores_DemandaCod[0] == A1314ContagemResultadoIndicadores_DemandaCod ) && ( T003J14_A1315ContagemResultadoIndicadores_IndicadorCod[0] < A1315ContagemResultadoIndicadores_IndicadorCod ) ) )
            {
               A1314ContagemResultadoIndicadores_DemandaCod = T003J14_A1314ContagemResultadoIndicadores_DemandaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
               A1315ContagemResultadoIndicadores_IndicadorCod = T003J14_A1315ContagemResultadoIndicadores_IndicadorCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
               RcdFound160 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3J160( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3J160( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound160 == 1 )
            {
               if ( ( A1314ContagemResultadoIndicadores_DemandaCod != Z1314ContagemResultadoIndicadores_DemandaCod ) || ( A1315ContagemResultadoIndicadores_IndicadorCod != Z1315ContagemResultadoIndicadores_IndicadorCod ) )
               {
                  A1314ContagemResultadoIndicadores_DemandaCod = Z1314ContagemResultadoIndicadores_DemandaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
                  A1315ContagemResultadoIndicadores_IndicadorCod = Z1315ContagemResultadoIndicadores_IndicadorCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_DEMANDACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update3J160( ) ;
                  GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A1314ContagemResultadoIndicadores_DemandaCod != Z1314ContagemResultadoIndicadores_DemandaCod ) || ( A1315ContagemResultadoIndicadores_IndicadorCod != Z1315ContagemResultadoIndicadores_IndicadorCod ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3J160( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADOINDICADORES_DEMANDACOD");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3J160( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A1314ContagemResultadoIndicadores_DemandaCod != Z1314ContagemResultadoIndicadores_DemandaCod ) || ( A1315ContagemResultadoIndicadores_IndicadorCod != Z1315ContagemResultadoIndicadores_IndicadorCod ) )
         {
            A1314ContagemResultadoIndicadores_DemandaCod = Z1314ContagemResultadoIndicadores_DemandaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
            A1315ContagemResultadoIndicadores_IndicadorCod = Z1315ContagemResultadoIndicadores_IndicadorCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADOINDICADORES_DEMANDACOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound160 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_DEMANDACOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemResultadoIndicadores_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3J160( ) ;
         if ( RcdFound160 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoIndicadores_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3J160( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound160 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoIndicadores_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound160 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoIndicadores_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3J160( ) ;
         if ( RcdFound160 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound160 != 0 )
            {
               ScanNext3J160( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoIndicadores_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3J160( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency3J160( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003J2 */
            pr_default.execute(0, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoIndicadores"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1317ContagemResultadoIndicadores_Data != T003J2_A1317ContagemResultadoIndicadores_Data[0] ) || ( Z1318ContagemResultadoIndicadores_Solicitada != T003J2_A1318ContagemResultadoIndicadores_Solicitada[0] ) || ( Z1319ContagemResultadoIndicadores_Prazo != T003J2_A1319ContagemResultadoIndicadores_Prazo[0] ) || ( Z1324ContagemResultadoIndicadores_Entregue != T003J2_A1324ContagemResultadoIndicadores_Entregue[0] ) || ( Z1321ContagemResultadoIndicadores_Atraso != T003J2_A1321ContagemResultadoIndicadores_Atraso[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1322ContagemResultadoIndicadores_Reduz != T003J2_A1322ContagemResultadoIndicadores_Reduz[0] ) || ( Z1323ContagemResultadoIndicadores_Valor != T003J2_A1323ContagemResultadoIndicadores_Valor[0] ) || ( Z1316ContagemResultadoIndicadores_LoteCod != T003J2_A1316ContagemResultadoIndicadores_LoteCod[0] ) )
            {
               if ( Z1317ContagemResultadoIndicadores_Data != T003J2_A1317ContagemResultadoIndicadores_Data[0] )
               {
                  GXUtil.WriteLog("contagemresultadoindicadores:[seudo value changed for attri]"+"ContagemResultadoIndicadores_Data");
                  GXUtil.WriteLogRaw("Old: ",Z1317ContagemResultadoIndicadores_Data);
                  GXUtil.WriteLogRaw("Current: ",T003J2_A1317ContagemResultadoIndicadores_Data[0]);
               }
               if ( Z1318ContagemResultadoIndicadores_Solicitada != T003J2_A1318ContagemResultadoIndicadores_Solicitada[0] )
               {
                  GXUtil.WriteLog("contagemresultadoindicadores:[seudo value changed for attri]"+"ContagemResultadoIndicadores_Solicitada");
                  GXUtil.WriteLogRaw("Old: ",Z1318ContagemResultadoIndicadores_Solicitada);
                  GXUtil.WriteLogRaw("Current: ",T003J2_A1318ContagemResultadoIndicadores_Solicitada[0]);
               }
               if ( Z1319ContagemResultadoIndicadores_Prazo != T003J2_A1319ContagemResultadoIndicadores_Prazo[0] )
               {
                  GXUtil.WriteLog("contagemresultadoindicadores:[seudo value changed for attri]"+"ContagemResultadoIndicadores_Prazo");
                  GXUtil.WriteLogRaw("Old: ",Z1319ContagemResultadoIndicadores_Prazo);
                  GXUtil.WriteLogRaw("Current: ",T003J2_A1319ContagemResultadoIndicadores_Prazo[0]);
               }
               if ( Z1324ContagemResultadoIndicadores_Entregue != T003J2_A1324ContagemResultadoIndicadores_Entregue[0] )
               {
                  GXUtil.WriteLog("contagemresultadoindicadores:[seudo value changed for attri]"+"ContagemResultadoIndicadores_Entregue");
                  GXUtil.WriteLogRaw("Old: ",Z1324ContagemResultadoIndicadores_Entregue);
                  GXUtil.WriteLogRaw("Current: ",T003J2_A1324ContagemResultadoIndicadores_Entregue[0]);
               }
               if ( Z1321ContagemResultadoIndicadores_Atraso != T003J2_A1321ContagemResultadoIndicadores_Atraso[0] )
               {
                  GXUtil.WriteLog("contagemresultadoindicadores:[seudo value changed for attri]"+"ContagemResultadoIndicadores_Atraso");
                  GXUtil.WriteLogRaw("Old: ",Z1321ContagemResultadoIndicadores_Atraso);
                  GXUtil.WriteLogRaw("Current: ",T003J2_A1321ContagemResultadoIndicadores_Atraso[0]);
               }
               if ( Z1322ContagemResultadoIndicadores_Reduz != T003J2_A1322ContagemResultadoIndicadores_Reduz[0] )
               {
                  GXUtil.WriteLog("contagemresultadoindicadores:[seudo value changed for attri]"+"ContagemResultadoIndicadores_Reduz");
                  GXUtil.WriteLogRaw("Old: ",Z1322ContagemResultadoIndicadores_Reduz);
                  GXUtil.WriteLogRaw("Current: ",T003J2_A1322ContagemResultadoIndicadores_Reduz[0]);
               }
               if ( Z1323ContagemResultadoIndicadores_Valor != T003J2_A1323ContagemResultadoIndicadores_Valor[0] )
               {
                  GXUtil.WriteLog("contagemresultadoindicadores:[seudo value changed for attri]"+"ContagemResultadoIndicadores_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z1323ContagemResultadoIndicadores_Valor);
                  GXUtil.WriteLogRaw("Current: ",T003J2_A1323ContagemResultadoIndicadores_Valor[0]);
               }
               if ( Z1316ContagemResultadoIndicadores_LoteCod != T003J2_A1316ContagemResultadoIndicadores_LoteCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadoindicadores:[seudo value changed for attri]"+"ContagemResultadoIndicadores_LoteCod");
                  GXUtil.WriteLogRaw("Old: ",Z1316ContagemResultadoIndicadores_LoteCod);
                  GXUtil.WriteLogRaw("Current: ",T003J2_A1316ContagemResultadoIndicadores_LoteCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoIndicadores"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3J160( )
      {
         BeforeValidate3J160( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3J160( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3J160( 0) ;
            CheckOptimisticConcurrency3J160( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3J160( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3J160( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003J15 */
                     pr_default.execute(13, new Object[] {A1317ContagemResultadoIndicadores_Data, A1318ContagemResultadoIndicadores_Solicitada, A1319ContagemResultadoIndicadores_Prazo, A1324ContagemResultadoIndicadores_Entregue, A1321ContagemResultadoIndicadores_Atraso, A1322ContagemResultadoIndicadores_Reduz, A1323ContagemResultadoIndicadores_Valor, A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod, n1316ContagemResultadoIndicadores_LoteCod, A1316ContagemResultadoIndicadores_LoteCod});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
                     if ( (pr_default.getStatus(13) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3J0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3J160( ) ;
            }
            EndLevel3J160( ) ;
         }
         CloseExtendedTableCursors3J160( ) ;
      }

      protected void Update3J160( )
      {
         BeforeValidate3J160( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3J160( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3J160( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3J160( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3J160( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003J16 */
                     pr_default.execute(14, new Object[] {A1317ContagemResultadoIndicadores_Data, A1318ContagemResultadoIndicadores_Solicitada, A1319ContagemResultadoIndicadores_Prazo, A1324ContagemResultadoIndicadores_Entregue, A1321ContagemResultadoIndicadores_Atraso, A1322ContagemResultadoIndicadores_Reduz, A1323ContagemResultadoIndicadores_Valor, n1316ContagemResultadoIndicadores_LoteCod, A1316ContagemResultadoIndicadores_LoteCod, A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
                     if ( (pr_default.getStatus(14) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoIndicadores"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3J160( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption3J0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3J160( ) ;
         }
         CloseExtendedTableCursors3J160( ) ;
      }

      protected void DeferredUpdate3J160( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate3J160( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3J160( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3J160( ) ;
            AfterConfirm3J160( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3J160( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003J17 */
                  pr_default.execute(15, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
                  pr_default.close(15);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound160 == 0 )
                        {
                           InitAll3J160( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption3J0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode160 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel3J160( ) ;
         Gx_mode = sMode160;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls3J160( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel3J160( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3J160( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContagemResultadoIndicadores");
            if ( AnyError == 0 )
            {
               ConfirmValues3J0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContagemResultadoIndicadores");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3J160( )
      {
         /* Using cursor T003J18 */
         pr_default.execute(16);
         RcdFound160 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound160 = 1;
            A1314ContagemResultadoIndicadores_DemandaCod = T003J18_A1314ContagemResultadoIndicadores_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
            A1315ContagemResultadoIndicadores_IndicadorCod = T003J18_A1315ContagemResultadoIndicadores_IndicadorCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3J160( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound160 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound160 = 1;
            A1314ContagemResultadoIndicadores_DemandaCod = T003J18_A1314ContagemResultadoIndicadores_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
            A1315ContagemResultadoIndicadores_IndicadorCod = T003J18_A1315ContagemResultadoIndicadores_IndicadorCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
         }
      }

      protected void ScanEnd3J160( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm3J160( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3J160( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3J160( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3J160( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3J160( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3J160( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3J160( )
      {
         edtContagemResultadoIndicadores_DemandaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoIndicadores_DemandaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoIndicadores_DemandaCod_Enabled), 5, 0)));
         edtContagemResultadoIndicadores_IndicadorCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoIndicadores_IndicadorCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoIndicadores_IndicadorCod_Enabled), 5, 0)));
         edtContagemResultadoIndicadores_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoIndicadores_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoIndicadores_Data_Enabled), 5, 0)));
         edtContagemResultadoIndicadores_Solicitada_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoIndicadores_Solicitada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoIndicadores_Solicitada_Enabled), 5, 0)));
         edtContagemResultadoIndicadores_Prazo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoIndicadores_Prazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoIndicadores_Prazo_Enabled), 5, 0)));
         edtContagemResultadoIndicadores_Entregue_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoIndicadores_Entregue_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoIndicadores_Entregue_Enabled), 5, 0)));
         edtContagemResultadoIndicadores_Atraso_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoIndicadores_Atraso_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoIndicadores_Atraso_Enabled), 5, 0)));
         edtContagemResultadoIndicadores_Reduz_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoIndicadores_Reduz_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoIndicadores_Reduz_Enabled), 5, 0)));
         edtContagemResultadoIndicadores_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoIndicadores_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoIndicadores_Valor_Enabled), 5, 0)));
         dynContagemResultadoIndicadores_LoteCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultadoIndicadores_LoteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultadoIndicadores_LoteCod.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3J0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216173591");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoindicadores.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1314ContagemResultadoIndicadores_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1315ContagemResultadoIndicadores_IndicadorCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1317ContagemResultadoIndicadores_Data", context.localUtil.DToC( Z1317ContagemResultadoIndicadores_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1318ContagemResultadoIndicadores_Solicitada", context.localUtil.DToC( Z1318ContagemResultadoIndicadores_Solicitada, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1319ContagemResultadoIndicadores_Prazo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1319ContagemResultadoIndicadores_Prazo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1324ContagemResultadoIndicadores_Entregue", context.localUtil.DToC( Z1324ContagemResultadoIndicadores_Entregue, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1321ContagemResultadoIndicadores_Atraso", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1321ContagemResultadoIndicadores_Atraso), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1322ContagemResultadoIndicadores_Reduz", StringUtil.LTrim( StringUtil.NToC( Z1322ContagemResultadoIndicadores_Reduz, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1323ContagemResultadoIndicadores_Valor", StringUtil.LTrim( StringUtil.NToC( Z1323ContagemResultadoIndicadores_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1316ContagemResultadoIndicadores_LoteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1316ContagemResultadoIndicadores_LoteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadoindicadores.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoIndicadores" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Indicadores" ;
      }

      protected void InitializeNonKey3J160( )
      {
         A1317ContagemResultadoIndicadores_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1317ContagemResultadoIndicadores_Data", context.localUtil.Format(A1317ContagemResultadoIndicadores_Data, "99/99/99"));
         A1318ContagemResultadoIndicadores_Solicitada = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1318ContagemResultadoIndicadores_Solicitada", context.localUtil.Format(A1318ContagemResultadoIndicadores_Solicitada, "99/99/99"));
         A1319ContagemResultadoIndicadores_Prazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1319ContagemResultadoIndicadores_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1319ContagemResultadoIndicadores_Prazo), 4, 0)));
         A1324ContagemResultadoIndicadores_Entregue = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1324ContagemResultadoIndicadores_Entregue", context.localUtil.Format(A1324ContagemResultadoIndicadores_Entregue, "99/99/99"));
         A1321ContagemResultadoIndicadores_Atraso = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1321ContagemResultadoIndicadores_Atraso", StringUtil.LTrim( StringUtil.Str( (decimal)(A1321ContagemResultadoIndicadores_Atraso), 4, 0)));
         A1322ContagemResultadoIndicadores_Reduz = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1322ContagemResultadoIndicadores_Reduz", StringUtil.LTrim( StringUtil.Str( A1322ContagemResultadoIndicadores_Reduz, 6, 2)));
         A1323ContagemResultadoIndicadores_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1323ContagemResultadoIndicadores_Valor", StringUtil.LTrim( StringUtil.Str( A1323ContagemResultadoIndicadores_Valor, 18, 5)));
         A1316ContagemResultadoIndicadores_LoteCod = 0;
         n1316ContagemResultadoIndicadores_LoteCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1316ContagemResultadoIndicadores_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0)));
         n1316ContagemResultadoIndicadores_LoteCod = ((0==A1316ContagemResultadoIndicadores_LoteCod) ? true : false);
         Z1317ContagemResultadoIndicadores_Data = DateTime.MinValue;
         Z1318ContagemResultadoIndicadores_Solicitada = DateTime.MinValue;
         Z1319ContagemResultadoIndicadores_Prazo = 0;
         Z1324ContagemResultadoIndicadores_Entregue = DateTime.MinValue;
         Z1321ContagemResultadoIndicadores_Atraso = 0;
         Z1322ContagemResultadoIndicadores_Reduz = 0;
         Z1323ContagemResultadoIndicadores_Valor = 0;
         Z1316ContagemResultadoIndicadores_LoteCod = 0;
      }

      protected void InitAll3J160( )
      {
         A1314ContagemResultadoIndicadores_DemandaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1314ContagemResultadoIndicadores_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1314ContagemResultadoIndicadores_DemandaCod), 6, 0)));
         A1315ContagemResultadoIndicadores_IndicadorCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1315ContagemResultadoIndicadores_IndicadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1315ContagemResultadoIndicadores_IndicadorCod), 6, 0)));
         InitializeNonKey3J160( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621617360");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadoindicadores.js", "?2020621617360");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemresultadoindicadores_demandacod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOINDICADORES_DEMANDACOD";
         edtContagemResultadoIndicadores_DemandaCod_Internalname = "CONTAGEMRESULTADOINDICADORES_DEMANDACOD";
         lblTextblockcontagemresultadoindicadores_indicadorcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOINDICADORES_INDICADORCOD";
         edtContagemResultadoIndicadores_IndicadorCod_Internalname = "CONTAGEMRESULTADOINDICADORES_INDICADORCOD";
         lblTextblockcontagemresultadoindicadores_data_Internalname = "TEXTBLOCKCONTAGEMRESULTADOINDICADORES_DATA";
         edtContagemResultadoIndicadores_Data_Internalname = "CONTAGEMRESULTADOINDICADORES_DATA";
         lblTextblockcontagemresultadoindicadores_solicitada_Internalname = "TEXTBLOCKCONTAGEMRESULTADOINDICADORES_SOLICITADA";
         edtContagemResultadoIndicadores_Solicitada_Internalname = "CONTAGEMRESULTADOINDICADORES_SOLICITADA";
         lblTextblockcontagemresultadoindicadores_prazo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOINDICADORES_PRAZO";
         edtContagemResultadoIndicadores_Prazo_Internalname = "CONTAGEMRESULTADOINDICADORES_PRAZO";
         lblTextblockcontagemresultadoindicadores_entregue_Internalname = "TEXTBLOCKCONTAGEMRESULTADOINDICADORES_ENTREGUE";
         edtContagemResultadoIndicadores_Entregue_Internalname = "CONTAGEMRESULTADOINDICADORES_ENTREGUE";
         lblTextblockcontagemresultadoindicadores_atraso_Internalname = "TEXTBLOCKCONTAGEMRESULTADOINDICADORES_ATRASO";
         edtContagemResultadoIndicadores_Atraso_Internalname = "CONTAGEMRESULTADOINDICADORES_ATRASO";
         lblTextblockcontagemresultadoindicadores_reduz_Internalname = "TEXTBLOCKCONTAGEMRESULTADOINDICADORES_REDUZ";
         edtContagemResultadoIndicadores_Reduz_Internalname = "CONTAGEMRESULTADOINDICADORES_REDUZ";
         lblTextblockcontagemresultadoindicadores_valor_Internalname = "TEXTBLOCKCONTAGEMRESULTADOINDICADORES_VALOR";
         edtContagemResultadoIndicadores_Valor_Internalname = "CONTAGEMRESULTADOINDICADORES_VALOR";
         lblTextblockcontagemresultadoindicadores_lotecod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOINDICADORES_LOTECOD";
         dynContagemResultadoIndicadores_LoteCod_Internalname = "CONTAGEMRESULTADOINDICADORES_LOTECOD";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado Indicadores";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         dynContagemResultadoIndicadores_LoteCod_Jsonclick = "";
         dynContagemResultadoIndicadores_LoteCod.Enabled = 1;
         edtContagemResultadoIndicadores_Valor_Jsonclick = "";
         edtContagemResultadoIndicadores_Valor_Enabled = 1;
         edtContagemResultadoIndicadores_Reduz_Jsonclick = "";
         edtContagemResultadoIndicadores_Reduz_Enabled = 1;
         edtContagemResultadoIndicadores_Atraso_Jsonclick = "";
         edtContagemResultadoIndicadores_Atraso_Enabled = 1;
         edtContagemResultadoIndicadores_Entregue_Jsonclick = "";
         edtContagemResultadoIndicadores_Entregue_Enabled = 1;
         edtContagemResultadoIndicadores_Prazo_Jsonclick = "";
         edtContagemResultadoIndicadores_Prazo_Enabled = 1;
         edtContagemResultadoIndicadores_Solicitada_Jsonclick = "";
         edtContagemResultadoIndicadores_Solicitada_Enabled = 1;
         edtContagemResultadoIndicadores_Data_Jsonclick = "";
         edtContagemResultadoIndicadores_Data_Enabled = 1;
         edtContagemResultadoIndicadores_IndicadorCod_Jsonclick = "";
         edtContagemResultadoIndicadores_IndicadorCod_Enabled = 1;
         edtContagemResultadoIndicadores_DemandaCod_Jsonclick = "";
         edtContagemResultadoIndicadores_DemandaCod_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTAGEMRESULTADOINDICADORES_LOTECOD3J1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADOINDICADORES_LOTECOD_data3J1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADOINDICADORES_LOTECOD_html3J1( )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADOINDICADORES_LOTECOD_data3J1( ) ;
         gxdynajaxindex = 1;
         dynContagemResultadoIndicadores_LoteCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultadoIndicadores_LoteCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADOINDICADORES_LOTECOD_data3J1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T003J19 */
         pr_default.execute(17);
         while ( (pr_default.getStatus(17) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T003J19_A596Lote_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T003J19_A563Lote_Nome[0]));
            pr_default.readNext(17);
         }
         pr_default.close(17);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T003J20 */
         pr_default.execute(18, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Contagem Resultado Indicadores'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_DEMANDACOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(18);
         /* Using cursor T003J21 */
         pr_default.execute(19, new Object[] {A1315ContagemResultadoIndicadores_IndicadorCod});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resiltado Indicadores_Contrato Servicos Indicador'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_INDICADORCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_IndicadorCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(19);
         GX_FocusControl = edtContagemResultadoIndicadores_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemresultadoindicadores_demandacod( int GX_Parm1 )
      {
         A1314ContagemResultadoIndicadores_DemandaCod = GX_Parm1;
         /* Using cursor T003J20 */
         pr_default.execute(18, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Contagem Resultado Indicadores'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_DEMANDACOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_DemandaCod_Internalname;
         }
         pr_default.close(18);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoindicadores_indicadorcod( int GX_Parm1 ,
                                                                   int GX_Parm2 ,
                                                                   DateTime GX_Parm3 ,
                                                                   DateTime GX_Parm4 ,
                                                                   short GX_Parm5 ,
                                                                   DateTime GX_Parm6 ,
                                                                   short GX_Parm7 ,
                                                                   decimal GX_Parm8 ,
                                                                   decimal GX_Parm9 ,
                                                                   GXCombobox dynGX_Parm10 )
      {
         A1314ContagemResultadoIndicadores_DemandaCod = GX_Parm1;
         A1315ContagemResultadoIndicadores_IndicadorCod = GX_Parm2;
         A1317ContagemResultadoIndicadores_Data = GX_Parm3;
         A1318ContagemResultadoIndicadores_Solicitada = GX_Parm4;
         A1319ContagemResultadoIndicadores_Prazo = GX_Parm5;
         A1324ContagemResultadoIndicadores_Entregue = GX_Parm6;
         A1321ContagemResultadoIndicadores_Atraso = GX_Parm7;
         A1322ContagemResultadoIndicadores_Reduz = GX_Parm8;
         A1323ContagemResultadoIndicadores_Valor = GX_Parm9;
         dynContagemResultadoIndicadores_LoteCod = dynGX_Parm10;
         A1316ContagemResultadoIndicadores_LoteCod = (int)(NumberUtil.Val( dynContagemResultadoIndicadores_LoteCod.CurrentValue, "."));
         n1316ContagemResultadoIndicadores_LoteCod = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T003J21 */
         pr_default.execute(19, new Object[] {A1315ContagemResultadoIndicadores_IndicadorCod});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resiltado Indicadores_Contrato Servicos Indicador'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_INDICADORCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoIndicadores_IndicadorCod_Internalname;
         }
         pr_default.close(19);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynContagemResultadoIndicadores_LoteCod.removeAllItems();
         /* Using cursor T003J22 */
         pr_default.execute(20);
         while ( (pr_default.getStatus(20) != 101) )
         {
            dynContagemResultadoIndicadores_LoteCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T003J22_A596Lote_Codigo[0]), 6, 0)), T003J22_A563Lote_Nome[0], 0);
            pr_default.readNext(20);
         }
         pr_default.close(20);
         dynContagemResultadoIndicadores_LoteCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0));
         isValidOutput.Add(context.localUtil.Format(A1317ContagemResultadoIndicadores_Data, "99/99/99"));
         isValidOutput.Add(context.localUtil.Format(A1318ContagemResultadoIndicadores_Solicitada, "99/99/99"));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1319ContagemResultadoIndicadores_Prazo), 4, 0, ".", "")));
         isValidOutput.Add(context.localUtil.Format(A1324ContagemResultadoIndicadores_Entregue, "99/99/99"));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1321ContagemResultadoIndicadores_Atraso), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1322ContagemResultadoIndicadores_Reduz, 6, 2, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1323ContagemResultadoIndicadores_Valor, 18, 5, ".", "")));
         if ( dynContagemResultadoIndicadores_LoteCod.ItemCount > 0 )
         {
            A1316ContagemResultadoIndicadores_LoteCod = (int)(NumberUtil.Val( dynContagemResultadoIndicadores_LoteCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0))), "."));
            n1316ContagemResultadoIndicadores_LoteCod = false;
         }
         dynContagemResultadoIndicadores_LoteCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1316ContagemResultadoIndicadores_LoteCod), 6, 0));
         isValidOutput.Add(dynContagemResultadoIndicadores_LoteCod);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1314ContagemResultadoIndicadores_DemandaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1315ContagemResultadoIndicadores_IndicadorCod), 6, 0, ",", "")));
         isValidOutput.Add(context.localUtil.DToC( Z1317ContagemResultadoIndicadores_Data, 0, "/"));
         isValidOutput.Add(context.localUtil.DToC( Z1318ContagemResultadoIndicadores_Solicitada, 0, "/"));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1319ContagemResultadoIndicadores_Prazo), 4, 0, ",", "")));
         isValidOutput.Add(context.localUtil.DToC( Z1324ContagemResultadoIndicadores_Entregue, 0, "/"));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1321ContagemResultadoIndicadores_Atraso), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z1322ContagemResultadoIndicadores_Reduz, 6, 2, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z1323ContagemResultadoIndicadores_Valor, 18, 5, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1316ContagemResultadoIndicadores_LoteCod), 6, 0, ",", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoindicadores_lotecod( GXCombobox dynGX_Parm1 )
      {
         dynContagemResultadoIndicadores_LoteCod = dynGX_Parm1;
         A1316ContagemResultadoIndicadores_LoteCod = (int)(NumberUtil.Val( dynContagemResultadoIndicadores_LoteCod.CurrentValue, "."));
         n1316ContagemResultadoIndicadores_LoteCod = false;
         /* Using cursor T003J23 */
         pr_default.execute(21, new Object[] {n1316ContagemResultadoIndicadores_LoteCod, A1316ContagemResultadoIndicadores_LoteCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            if ( ! ( (0==A1316ContagemResultadoIndicadores_LoteCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resutlado Indicadores_Lote'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOINDICADORES_LOTECOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultadoIndicadores_LoteCod_Internalname;
            }
         }
         pr_default.close(21);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(18);
         pr_default.close(19);
         pr_default.close(21);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1317ContagemResultadoIndicadores_Data = DateTime.MinValue;
         Z1318ContagemResultadoIndicadores_Solicitada = DateTime.MinValue;
         Z1324ContagemResultadoIndicadores_Entregue = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T003J7_A596Lote_Codigo = new int[1] ;
         T003J7_A563Lote_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemresultadoindicadores_demandacod_Jsonclick = "";
         lblTextblockcontagemresultadoindicadores_indicadorcod_Jsonclick = "";
         lblTextblockcontagemresultadoindicadores_data_Jsonclick = "";
         A1317ContagemResultadoIndicadores_Data = DateTime.MinValue;
         lblTextblockcontagemresultadoindicadores_solicitada_Jsonclick = "";
         A1318ContagemResultadoIndicadores_Solicitada = DateTime.MinValue;
         lblTextblockcontagemresultadoindicadores_prazo_Jsonclick = "";
         lblTextblockcontagemresultadoindicadores_entregue_Jsonclick = "";
         A1324ContagemResultadoIndicadores_Entregue = DateTime.MinValue;
         lblTextblockcontagemresultadoindicadores_atraso_Jsonclick = "";
         lblTextblockcontagemresultadoindicadores_reduz_Jsonclick = "";
         lblTextblockcontagemresultadoindicadores_valor_Jsonclick = "";
         lblTextblockcontagemresultadoindicadores_lotecod_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T003J8_A1317ContagemResultadoIndicadores_Data = new DateTime[] {DateTime.MinValue} ;
         T003J8_A1318ContagemResultadoIndicadores_Solicitada = new DateTime[] {DateTime.MinValue} ;
         T003J8_A1319ContagemResultadoIndicadores_Prazo = new short[1] ;
         T003J8_A1324ContagemResultadoIndicadores_Entregue = new DateTime[] {DateTime.MinValue} ;
         T003J8_A1321ContagemResultadoIndicadores_Atraso = new short[1] ;
         T003J8_A1322ContagemResultadoIndicadores_Reduz = new decimal[1] ;
         T003J8_A1323ContagemResultadoIndicadores_Valor = new decimal[1] ;
         T003J8_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T003J8_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         T003J8_A1316ContagemResultadoIndicadores_LoteCod = new int[1] ;
         T003J8_n1316ContagemResultadoIndicadores_LoteCod = new bool[] {false} ;
         T003J4_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T003J5_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         T003J6_A1316ContagemResultadoIndicadores_LoteCod = new int[1] ;
         T003J6_n1316ContagemResultadoIndicadores_LoteCod = new bool[] {false} ;
         T003J9_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T003J10_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         T003J11_A1316ContagemResultadoIndicadores_LoteCod = new int[1] ;
         T003J11_n1316ContagemResultadoIndicadores_LoteCod = new bool[] {false} ;
         T003J12_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T003J12_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         T003J3_A1317ContagemResultadoIndicadores_Data = new DateTime[] {DateTime.MinValue} ;
         T003J3_A1318ContagemResultadoIndicadores_Solicitada = new DateTime[] {DateTime.MinValue} ;
         T003J3_A1319ContagemResultadoIndicadores_Prazo = new short[1] ;
         T003J3_A1324ContagemResultadoIndicadores_Entregue = new DateTime[] {DateTime.MinValue} ;
         T003J3_A1321ContagemResultadoIndicadores_Atraso = new short[1] ;
         T003J3_A1322ContagemResultadoIndicadores_Reduz = new decimal[1] ;
         T003J3_A1323ContagemResultadoIndicadores_Valor = new decimal[1] ;
         T003J3_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T003J3_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         T003J3_A1316ContagemResultadoIndicadores_LoteCod = new int[1] ;
         T003J3_n1316ContagemResultadoIndicadores_LoteCod = new bool[] {false} ;
         sMode160 = "";
         T003J13_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T003J13_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         T003J14_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T003J14_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         T003J2_A1317ContagemResultadoIndicadores_Data = new DateTime[] {DateTime.MinValue} ;
         T003J2_A1318ContagemResultadoIndicadores_Solicitada = new DateTime[] {DateTime.MinValue} ;
         T003J2_A1319ContagemResultadoIndicadores_Prazo = new short[1] ;
         T003J2_A1324ContagemResultadoIndicadores_Entregue = new DateTime[] {DateTime.MinValue} ;
         T003J2_A1321ContagemResultadoIndicadores_Atraso = new short[1] ;
         T003J2_A1322ContagemResultadoIndicadores_Reduz = new decimal[1] ;
         T003J2_A1323ContagemResultadoIndicadores_Valor = new decimal[1] ;
         T003J2_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T003J2_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         T003J2_A1316ContagemResultadoIndicadores_LoteCod = new int[1] ;
         T003J2_n1316ContagemResultadoIndicadores_LoteCod = new bool[] {false} ;
         T003J18_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T003J18_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T003J19_A596Lote_Codigo = new int[1] ;
         T003J19_A563Lote_Nome = new String[] {""} ;
         T003J20_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T003J21_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         T003J22_A596Lote_Codigo = new int[1] ;
         T003J22_A563Lote_Nome = new String[] {""} ;
         T003J23_A1316ContagemResultadoIndicadores_LoteCod = new int[1] ;
         T003J23_n1316ContagemResultadoIndicadores_LoteCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoindicadores__default(),
            new Object[][] {
                new Object[] {
               T003J2_A1317ContagemResultadoIndicadores_Data, T003J2_A1318ContagemResultadoIndicadores_Solicitada, T003J2_A1319ContagemResultadoIndicadores_Prazo, T003J2_A1324ContagemResultadoIndicadores_Entregue, T003J2_A1321ContagemResultadoIndicadores_Atraso, T003J2_A1322ContagemResultadoIndicadores_Reduz, T003J2_A1323ContagemResultadoIndicadores_Valor, T003J2_A1314ContagemResultadoIndicadores_DemandaCod, T003J2_A1315ContagemResultadoIndicadores_IndicadorCod, T003J2_A1316ContagemResultadoIndicadores_LoteCod,
               T003J2_n1316ContagemResultadoIndicadores_LoteCod
               }
               , new Object[] {
               T003J3_A1317ContagemResultadoIndicadores_Data, T003J3_A1318ContagemResultadoIndicadores_Solicitada, T003J3_A1319ContagemResultadoIndicadores_Prazo, T003J3_A1324ContagemResultadoIndicadores_Entregue, T003J3_A1321ContagemResultadoIndicadores_Atraso, T003J3_A1322ContagemResultadoIndicadores_Reduz, T003J3_A1323ContagemResultadoIndicadores_Valor, T003J3_A1314ContagemResultadoIndicadores_DemandaCod, T003J3_A1315ContagemResultadoIndicadores_IndicadorCod, T003J3_A1316ContagemResultadoIndicadores_LoteCod,
               T003J3_n1316ContagemResultadoIndicadores_LoteCod
               }
               , new Object[] {
               T003J4_A1314ContagemResultadoIndicadores_DemandaCod
               }
               , new Object[] {
               T003J5_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               T003J6_A1316ContagemResultadoIndicadores_LoteCod
               }
               , new Object[] {
               T003J7_A596Lote_Codigo, T003J7_A563Lote_Nome
               }
               , new Object[] {
               T003J8_A1317ContagemResultadoIndicadores_Data, T003J8_A1318ContagemResultadoIndicadores_Solicitada, T003J8_A1319ContagemResultadoIndicadores_Prazo, T003J8_A1324ContagemResultadoIndicadores_Entregue, T003J8_A1321ContagemResultadoIndicadores_Atraso, T003J8_A1322ContagemResultadoIndicadores_Reduz, T003J8_A1323ContagemResultadoIndicadores_Valor, T003J8_A1314ContagemResultadoIndicadores_DemandaCod, T003J8_A1315ContagemResultadoIndicadores_IndicadorCod, T003J8_A1316ContagemResultadoIndicadores_LoteCod,
               T003J8_n1316ContagemResultadoIndicadores_LoteCod
               }
               , new Object[] {
               T003J9_A1314ContagemResultadoIndicadores_DemandaCod
               }
               , new Object[] {
               T003J10_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               T003J11_A1316ContagemResultadoIndicadores_LoteCod
               }
               , new Object[] {
               T003J12_A1314ContagemResultadoIndicadores_DemandaCod, T003J12_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               T003J13_A1314ContagemResultadoIndicadores_DemandaCod, T003J13_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               T003J14_A1314ContagemResultadoIndicadores_DemandaCod, T003J14_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003J18_A1314ContagemResultadoIndicadores_DemandaCod, T003J18_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               T003J19_A596Lote_Codigo, T003J19_A563Lote_Nome
               }
               , new Object[] {
               T003J20_A1314ContagemResultadoIndicadores_DemandaCod
               }
               , new Object[] {
               T003J21_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               T003J22_A596Lote_Codigo, T003J22_A563Lote_Nome
               }
               , new Object[] {
               T003J23_A1316ContagemResultadoIndicadores_LoteCod
               }
            }
         );
      }

      private short Z1319ContagemResultadoIndicadores_Prazo ;
      private short Z1321ContagemResultadoIndicadores_Atraso ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1319ContagemResultadoIndicadores_Prazo ;
      private short A1321ContagemResultadoIndicadores_Atraso ;
      private short GX_JID ;
      private short RcdFound160 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1314ContagemResultadoIndicadores_DemandaCod ;
      private int Z1315ContagemResultadoIndicadores_IndicadorCod ;
      private int Z1316ContagemResultadoIndicadores_LoteCod ;
      private int A1314ContagemResultadoIndicadores_DemandaCod ;
      private int A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int A1316ContagemResultadoIndicadores_LoteCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtContagemResultadoIndicadores_DemandaCod_Enabled ;
      private int edtContagemResultadoIndicadores_IndicadorCod_Enabled ;
      private int edtContagemResultadoIndicadores_Data_Enabled ;
      private int edtContagemResultadoIndicadores_Solicitada_Enabled ;
      private int edtContagemResultadoIndicadores_Prazo_Enabled ;
      private int edtContagemResultadoIndicadores_Entregue_Enabled ;
      private int edtContagemResultadoIndicadores_Atraso_Enabled ;
      private int edtContagemResultadoIndicadores_Reduz_Enabled ;
      private int edtContagemResultadoIndicadores_Valor_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z1322ContagemResultadoIndicadores_Reduz ;
      private decimal Z1323ContagemResultadoIndicadores_Valor ;
      private decimal A1322ContagemResultadoIndicadores_Reduz ;
      private decimal A1323ContagemResultadoIndicadores_Valor ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoIndicadores_DemandaCod_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemresultadoindicadores_demandacod_Internalname ;
      private String lblTextblockcontagemresultadoindicadores_demandacod_Jsonclick ;
      private String edtContagemResultadoIndicadores_DemandaCod_Jsonclick ;
      private String lblTextblockcontagemresultadoindicadores_indicadorcod_Internalname ;
      private String lblTextblockcontagemresultadoindicadores_indicadorcod_Jsonclick ;
      private String edtContagemResultadoIndicadores_IndicadorCod_Internalname ;
      private String edtContagemResultadoIndicadores_IndicadorCod_Jsonclick ;
      private String lblTextblockcontagemresultadoindicadores_data_Internalname ;
      private String lblTextblockcontagemresultadoindicadores_data_Jsonclick ;
      private String edtContagemResultadoIndicadores_Data_Internalname ;
      private String edtContagemResultadoIndicadores_Data_Jsonclick ;
      private String lblTextblockcontagemresultadoindicadores_solicitada_Internalname ;
      private String lblTextblockcontagemresultadoindicadores_solicitada_Jsonclick ;
      private String edtContagemResultadoIndicadores_Solicitada_Internalname ;
      private String edtContagemResultadoIndicadores_Solicitada_Jsonclick ;
      private String lblTextblockcontagemresultadoindicadores_prazo_Internalname ;
      private String lblTextblockcontagemresultadoindicadores_prazo_Jsonclick ;
      private String edtContagemResultadoIndicadores_Prazo_Internalname ;
      private String edtContagemResultadoIndicadores_Prazo_Jsonclick ;
      private String lblTextblockcontagemresultadoindicadores_entregue_Internalname ;
      private String lblTextblockcontagemresultadoindicadores_entregue_Jsonclick ;
      private String edtContagemResultadoIndicadores_Entregue_Internalname ;
      private String edtContagemResultadoIndicadores_Entregue_Jsonclick ;
      private String lblTextblockcontagemresultadoindicadores_atraso_Internalname ;
      private String lblTextblockcontagemresultadoindicadores_atraso_Jsonclick ;
      private String edtContagemResultadoIndicadores_Atraso_Internalname ;
      private String edtContagemResultadoIndicadores_Atraso_Jsonclick ;
      private String lblTextblockcontagemresultadoindicadores_reduz_Internalname ;
      private String lblTextblockcontagemresultadoindicadores_reduz_Jsonclick ;
      private String edtContagemResultadoIndicadores_Reduz_Internalname ;
      private String edtContagemResultadoIndicadores_Reduz_Jsonclick ;
      private String lblTextblockcontagemresultadoindicadores_valor_Internalname ;
      private String lblTextblockcontagemresultadoindicadores_valor_Jsonclick ;
      private String edtContagemResultadoIndicadores_Valor_Internalname ;
      private String edtContagemResultadoIndicadores_Valor_Jsonclick ;
      private String lblTextblockcontagemresultadoindicadores_lotecod_Internalname ;
      private String lblTextblockcontagemresultadoindicadores_lotecod_Jsonclick ;
      private String dynContagemResultadoIndicadores_LoteCod_Internalname ;
      private String dynContagemResultadoIndicadores_LoteCod_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode160 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private DateTime Z1317ContagemResultadoIndicadores_Data ;
      private DateTime Z1318ContagemResultadoIndicadores_Solicitada ;
      private DateTime Z1324ContagemResultadoIndicadores_Entregue ;
      private DateTime A1317ContagemResultadoIndicadores_Data ;
      private DateTime A1318ContagemResultadoIndicadores_Solicitada ;
      private DateTime A1324ContagemResultadoIndicadores_Entregue ;
      private bool entryPointCalled ;
      private bool n1316ContagemResultadoIndicadores_LoteCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Gx_longc ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T003J7_A596Lote_Codigo ;
      private String[] T003J7_A563Lote_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynContagemResultadoIndicadores_LoteCod ;
      private DateTime[] T003J8_A1317ContagemResultadoIndicadores_Data ;
      private DateTime[] T003J8_A1318ContagemResultadoIndicadores_Solicitada ;
      private short[] T003J8_A1319ContagemResultadoIndicadores_Prazo ;
      private DateTime[] T003J8_A1324ContagemResultadoIndicadores_Entregue ;
      private short[] T003J8_A1321ContagemResultadoIndicadores_Atraso ;
      private decimal[] T003J8_A1322ContagemResultadoIndicadores_Reduz ;
      private decimal[] T003J8_A1323ContagemResultadoIndicadores_Valor ;
      private int[] T003J8_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T003J8_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] T003J8_A1316ContagemResultadoIndicadores_LoteCod ;
      private bool[] T003J8_n1316ContagemResultadoIndicadores_LoteCod ;
      private int[] T003J4_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T003J5_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] T003J6_A1316ContagemResultadoIndicadores_LoteCod ;
      private bool[] T003J6_n1316ContagemResultadoIndicadores_LoteCod ;
      private int[] T003J9_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T003J10_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] T003J11_A1316ContagemResultadoIndicadores_LoteCod ;
      private bool[] T003J11_n1316ContagemResultadoIndicadores_LoteCod ;
      private int[] T003J12_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T003J12_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private DateTime[] T003J3_A1317ContagemResultadoIndicadores_Data ;
      private DateTime[] T003J3_A1318ContagemResultadoIndicadores_Solicitada ;
      private short[] T003J3_A1319ContagemResultadoIndicadores_Prazo ;
      private DateTime[] T003J3_A1324ContagemResultadoIndicadores_Entregue ;
      private short[] T003J3_A1321ContagemResultadoIndicadores_Atraso ;
      private decimal[] T003J3_A1322ContagemResultadoIndicadores_Reduz ;
      private decimal[] T003J3_A1323ContagemResultadoIndicadores_Valor ;
      private int[] T003J3_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T003J3_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] T003J3_A1316ContagemResultadoIndicadores_LoteCod ;
      private bool[] T003J3_n1316ContagemResultadoIndicadores_LoteCod ;
      private int[] T003J13_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T003J13_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] T003J14_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T003J14_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private DateTime[] T003J2_A1317ContagemResultadoIndicadores_Data ;
      private DateTime[] T003J2_A1318ContagemResultadoIndicadores_Solicitada ;
      private short[] T003J2_A1319ContagemResultadoIndicadores_Prazo ;
      private DateTime[] T003J2_A1324ContagemResultadoIndicadores_Entregue ;
      private short[] T003J2_A1321ContagemResultadoIndicadores_Atraso ;
      private decimal[] T003J2_A1322ContagemResultadoIndicadores_Reduz ;
      private decimal[] T003J2_A1323ContagemResultadoIndicadores_Valor ;
      private int[] T003J2_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T003J2_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] T003J2_A1316ContagemResultadoIndicadores_LoteCod ;
      private bool[] T003J2_n1316ContagemResultadoIndicadores_LoteCod ;
      private int[] T003J18_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T003J18_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] T003J19_A596Lote_Codigo ;
      private String[] T003J19_A563Lote_Nome ;
      private int[] T003J20_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T003J21_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] T003J22_A596Lote_Codigo ;
      private String[] T003J22_A563Lote_Nome ;
      private int[] T003J23_A1316ContagemResultadoIndicadores_LoteCod ;
      private bool[] T003J23_n1316ContagemResultadoIndicadores_LoteCod ;
      private GXWebForm Form ;
   }

   public class contagemresultadoindicadores__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003J7 ;
          prmT003J7 = new Object[] {
          } ;
          Object[] prmT003J8 ;
          prmT003J8 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J4 ;
          prmT003J4 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J5 ;
          prmT003J5 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J6 ;
          prmT003J6 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_LoteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J9 ;
          prmT003J9 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J10 ;
          prmT003J10 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J11 ;
          prmT003J11 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_LoteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J12 ;
          prmT003J12 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J3 ;
          prmT003J3 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J13 ;
          prmT003J13 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J14 ;
          prmT003J14 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J2 ;
          prmT003J2 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J15 ;
          prmT003J15 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Solicitada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Entregue",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Atraso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultadoIndicadores_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_LoteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J16 ;
          prmT003J16 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Solicitada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Entregue",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Atraso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultadoIndicadores_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoIndicadores_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J17 ;
          prmT003J17 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J18 ;
          prmT003J18 = new Object[] {
          } ;
          Object[] prmT003J19 ;
          prmT003J19 = new Object[] {
          } ;
          Object[] prmT003J20 ;
          prmT003J20 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J21 ;
          prmT003J21 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003J22 ;
          prmT003J22 = new Object[] {
          } ;
          Object[] prmT003J23 ;
          prmT003J23 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_LoteCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003J2", "SELECT [ContagemResultadoIndicadores_Data], [ContagemResultadoIndicadores_Solicitada], [ContagemResultadoIndicadores_Prazo], [ContagemResultadoIndicadores_Entregue], [ContagemResultadoIndicadores_Atraso], [ContagemResultadoIndicadores_Reduz], [ContagemResultadoIndicadores_Valor], [ContagemResultadoIndicadores_DemandaCod] AS ContagemResultadoIndicadores_DemandaCod, [ContagemResultadoIndicadores_IndicadorCod] AS ContagemResultadoIndicadores_IndicadorCod, [ContagemResultadoIndicadores_LoteCod] AS ContagemResultadoIndicadores_LoteCod FROM [ContagemResultadoIndicadores] WITH (UPDLOCK) WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod AND [ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J2,1,0,true,false )
             ,new CursorDef("T003J3", "SELECT [ContagemResultadoIndicadores_Data], [ContagemResultadoIndicadores_Solicitada], [ContagemResultadoIndicadores_Prazo], [ContagemResultadoIndicadores_Entregue], [ContagemResultadoIndicadores_Atraso], [ContagemResultadoIndicadores_Reduz], [ContagemResultadoIndicadores_Valor], [ContagemResultadoIndicadores_DemandaCod] AS ContagemResultadoIndicadores_DemandaCod, [ContagemResultadoIndicadores_IndicadorCod] AS ContagemResultadoIndicadores_IndicadorCod, [ContagemResultadoIndicadores_LoteCod] AS ContagemResultadoIndicadores_LoteCod FROM [ContagemResultadoIndicadores] WITH (NOLOCK) WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod AND [ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J3,1,0,true,false )
             ,new CursorDef("T003J4", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoIndicadores_DemandaCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoIndicadores_DemandaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J4,1,0,true,false )
             ,new CursorDef("T003J5", "SELECT [ContratoServicosIndicador_Codigo] AS ContagemResultadoIndicadores_IndicadorCod FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContagemResultadoIndicadores_IndicadorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J5,1,0,true,false )
             ,new CursorDef("T003J6", "SELECT [Lote_Codigo] AS ContagemResultadoIndicadores_LoteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @ContagemResultadoIndicadores_LoteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J6,1,0,true,false )
             ,new CursorDef("T003J7", "SELECT [Lote_Codigo], [Lote_Nome] FROM [Lote] WITH (NOLOCK) ORDER BY [Lote_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J7,0,0,true,false )
             ,new CursorDef("T003J8", "SELECT TM1.[ContagemResultadoIndicadores_Data], TM1.[ContagemResultadoIndicadores_Solicitada], TM1.[ContagemResultadoIndicadores_Prazo], TM1.[ContagemResultadoIndicadores_Entregue], TM1.[ContagemResultadoIndicadores_Atraso], TM1.[ContagemResultadoIndicadores_Reduz], TM1.[ContagemResultadoIndicadores_Valor], TM1.[ContagemResultadoIndicadores_DemandaCod] AS ContagemResultadoIndicadores_DemandaCod, TM1.[ContagemResultadoIndicadores_IndicadorCod] AS ContagemResultadoIndicadores_IndicadorCod, TM1.[ContagemResultadoIndicadores_LoteCod] AS ContagemResultadoIndicadores_LoteCod FROM [ContagemResultadoIndicadores] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod and TM1.[ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod ORDER BY TM1.[ContagemResultadoIndicadores_DemandaCod], TM1.[ContagemResultadoIndicadores_IndicadorCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003J8,100,0,true,false )
             ,new CursorDef("T003J9", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoIndicadores_DemandaCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoIndicadores_DemandaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J9,1,0,true,false )
             ,new CursorDef("T003J10", "SELECT [ContratoServicosIndicador_Codigo] AS ContagemResultadoIndicadores_IndicadorCod FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContagemResultadoIndicadores_IndicadorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J10,1,0,true,false )
             ,new CursorDef("T003J11", "SELECT [Lote_Codigo] AS ContagemResultadoIndicadores_LoteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @ContagemResultadoIndicadores_LoteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J11,1,0,true,false )
             ,new CursorDef("T003J12", "SELECT [ContagemResultadoIndicadores_DemandaCod] AS ContagemResultadoIndicadores_DemandaCod, [ContagemResultadoIndicadores_IndicadorCod] AS ContagemResultadoIndicadores_IndicadorCod FROM [ContagemResultadoIndicadores] WITH (NOLOCK) WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod AND [ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003J12,1,0,true,false )
             ,new CursorDef("T003J13", "SELECT TOP 1 [ContagemResultadoIndicadores_DemandaCod] AS ContagemResultadoIndicadores_DemandaCod, [ContagemResultadoIndicadores_IndicadorCod] AS ContagemResultadoIndicadores_IndicadorCod FROM [ContagemResultadoIndicadores] WITH (NOLOCK) WHERE ( [ContagemResultadoIndicadores_DemandaCod] > @ContagemResultadoIndicadores_DemandaCod or [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod and [ContagemResultadoIndicadores_IndicadorCod] > @ContagemResultadoIndicadores_IndicadorCod) ORDER BY [ContagemResultadoIndicadores_DemandaCod], [ContagemResultadoIndicadores_IndicadorCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003J13,1,0,true,true )
             ,new CursorDef("T003J14", "SELECT TOP 1 [ContagemResultadoIndicadores_DemandaCod] AS ContagemResultadoIndicadores_DemandaCod, [ContagemResultadoIndicadores_IndicadorCod] AS ContagemResultadoIndicadores_IndicadorCod FROM [ContagemResultadoIndicadores] WITH (NOLOCK) WHERE ( [ContagemResultadoIndicadores_DemandaCod] < @ContagemResultadoIndicadores_DemandaCod or [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod and [ContagemResultadoIndicadores_IndicadorCod] < @ContagemResultadoIndicadores_IndicadorCod) ORDER BY [ContagemResultadoIndicadores_DemandaCod] DESC, [ContagemResultadoIndicadores_IndicadorCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003J14,1,0,true,true )
             ,new CursorDef("T003J15", "INSERT INTO [ContagemResultadoIndicadores]([ContagemResultadoIndicadores_Data], [ContagemResultadoIndicadores_Solicitada], [ContagemResultadoIndicadores_Prazo], [ContagemResultadoIndicadores_Entregue], [ContagemResultadoIndicadores_Atraso], [ContagemResultadoIndicadores_Reduz], [ContagemResultadoIndicadores_Valor], [ContagemResultadoIndicadores_DemandaCod], [ContagemResultadoIndicadores_IndicadorCod], [ContagemResultadoIndicadores_LoteCod]) VALUES(@ContagemResultadoIndicadores_Data, @ContagemResultadoIndicadores_Solicitada, @ContagemResultadoIndicadores_Prazo, @ContagemResultadoIndicadores_Entregue, @ContagemResultadoIndicadores_Atraso, @ContagemResultadoIndicadores_Reduz, @ContagemResultadoIndicadores_Valor, @ContagemResultadoIndicadores_DemandaCod, @ContagemResultadoIndicadores_IndicadorCod, @ContagemResultadoIndicadores_LoteCod)", GxErrorMask.GX_NOMASK,prmT003J15)
             ,new CursorDef("T003J16", "UPDATE [ContagemResultadoIndicadores] SET [ContagemResultadoIndicadores_Data]=@ContagemResultadoIndicadores_Data, [ContagemResultadoIndicadores_Solicitada]=@ContagemResultadoIndicadores_Solicitada, [ContagemResultadoIndicadores_Prazo]=@ContagemResultadoIndicadores_Prazo, [ContagemResultadoIndicadores_Entregue]=@ContagemResultadoIndicadores_Entregue, [ContagemResultadoIndicadores_Atraso]=@ContagemResultadoIndicadores_Atraso, [ContagemResultadoIndicadores_Reduz]=@ContagemResultadoIndicadores_Reduz, [ContagemResultadoIndicadores_Valor]=@ContagemResultadoIndicadores_Valor, [ContagemResultadoIndicadores_LoteCod]=@ContagemResultadoIndicadores_LoteCod  WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod AND [ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod", GxErrorMask.GX_NOMASK,prmT003J16)
             ,new CursorDef("T003J17", "DELETE FROM [ContagemResultadoIndicadores]  WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod AND [ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod", GxErrorMask.GX_NOMASK,prmT003J17)
             ,new CursorDef("T003J18", "SELECT [ContagemResultadoIndicadores_DemandaCod] AS ContagemResultadoIndicadores_DemandaCod, [ContagemResultadoIndicadores_IndicadorCod] AS ContagemResultadoIndicadores_IndicadorCod FROM [ContagemResultadoIndicadores] WITH (NOLOCK) ORDER BY [ContagemResultadoIndicadores_DemandaCod], [ContagemResultadoIndicadores_IndicadorCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003J18,100,0,true,false )
             ,new CursorDef("T003J19", "SELECT [Lote_Codigo], [Lote_Nome] FROM [Lote] WITH (NOLOCK) ORDER BY [Lote_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J19,0,0,true,false )
             ,new CursorDef("T003J20", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoIndicadores_DemandaCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoIndicadores_DemandaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J20,1,0,true,false )
             ,new CursorDef("T003J21", "SELECT [ContratoServicosIndicador_Codigo] AS ContagemResultadoIndicadores_IndicadorCod FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContagemResultadoIndicadores_IndicadorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J21,1,0,true,false )
             ,new CursorDef("T003J22", "SELECT [Lote_Codigo], [Lote_Nome] FROM [Lote] WITH (NOLOCK) ORDER BY [Lote_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J22,0,0,true,false )
             ,new CursorDef("T003J23", "SELECT [Lote_Codigo] AS ContagemResultadoIndicadores_LoteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @ContagemResultadoIndicadores_LoteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003J23,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((int[]) buf[9])[0] = rslt.getInt(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((int[]) buf[9])[0] = rslt.getInt(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 6 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((int[]) buf[9])[0] = rslt.getInt(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                stmt.SetParameter(9, (int)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[10]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[8]);
                }
                stmt.SetParameter(9, (int)parms[9]);
                stmt.SetParameter(10, (int)parms[10]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
