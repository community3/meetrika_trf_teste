/*
               File: SolicitacaoServicoPDF
        Description: Solicitacao Servico PDF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:2:30.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacaoservicopdf : GXProcedure
   {
      public solicitacaoservicopdf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacaoservicopdf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Solicitacoes_Codigo )
      {
         this.AV11Solicitacoes_Codigo = aP0_Solicitacoes_Codigo;
         initialize();
         executePrivate();
         aP0_Solicitacoes_Codigo=this.AV11Solicitacoes_Codigo;
      }

      public int executeUdp( )
      {
         this.AV11Solicitacoes_Codigo = aP0_Solicitacoes_Codigo;
         initialize();
         executePrivate();
         aP0_Solicitacoes_Codigo=this.AV11Solicitacoes_Codigo;
         return AV11Solicitacoes_Codigo ;
      }

      public void executeSubmit( ref int aP0_Solicitacoes_Codigo )
      {
         solicitacaoservicopdf objsolicitacaoservicopdf;
         objsolicitacaoservicopdf = new solicitacaoservicopdf();
         objsolicitacaoservicopdf.AV11Solicitacoes_Codigo = aP0_Solicitacoes_Codigo;
         objsolicitacaoservicopdf.context.SetSubmitInitialConfig(context);
         objsolicitacaoservicopdf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objsolicitacaoservicopdf);
         aP0_Solicitacoes_Codigo=this.AV11Solicitacoes_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((solicitacaoservicopdf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName(AV10NomeArquivo) ;
         getPrinter().GxSetDocFormat("PDF") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV10NomeArquivo = "Solicita��o Servi�o PDF";
            /* Execute user subroutine: 'CONSULTAPRINT1' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            H2K0( false, 169) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contratada", 25, Gx_line+0, 80, Gx_line+14, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Servi�o", 25, Gx_line+17, 64, Gx_line+31, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Sistema", 25, Gx_line+33, 65, Gx_line+47, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Objetivo Geral da Solcita��o", 25, Gx_line+50, 167, Gx_line+64, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV20Contratada_PessoaNom, "@!")), 117, Gx_line+0, 639, Gx_line+15, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(AV21Servico_Descricao, 117, Gx_line+17, 1446, Gx_line+32, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22Sistema_Nome, "@!")), 117, Gx_line+33, 639, Gx_line+48, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(AV15Solicitacoes_Objetivo, 25, Gx_line+83, 808, Gx_line+166, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+169);
            /* Execute user subroutine: 'BUSCA_NOME1' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H2K0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CONSULTAPRINT1' Routine */
         /* Using cursor P002K2 */
         pr_default.execute(0, new Object[] {AV11Solicitacoes_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A439Solicitacoes_Codigo = P002K2_A439Solicitacoes_Codigo[0];
            A39Contratada_Codigo = P002K2_A39Contratada_Codigo[0];
            A155Servico_Codigo = P002K2_A155Servico_Codigo[0];
            A127Sistema_Codigo = P002K2_A127Sistema_Codigo[0];
            A441Solicitacoes_Objetivo = P002K2_A441Solicitacoes_Objetivo[0];
            n441Solicitacoes_Objetivo = P002K2_n441Solicitacoes_Objetivo[0];
            AV12Contratada_codigo = A39Contratada_Codigo;
            AV13Servico_Codigo = A155Servico_Codigo;
            AV14Sistema_Codigo = A127Sistema_Codigo;
            AV15Solicitacoes_Objetivo = A441Solicitacoes_Objetivo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'BUSCA_NOME1' Routine */
         /* Using cursor P002K3 */
         pr_default.execute(1, new Object[] {AV12Contratada_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A40Contratada_PessoaCod = P002K3_A40Contratada_PessoaCod[0];
            A39Contratada_Codigo = P002K3_A39Contratada_Codigo[0];
            A41Contratada_PessoaNom = P002K3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P002K3_n41Contratada_PessoaNom[0];
            A41Contratada_PessoaNom = P002K3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P002K3_n41Contratada_PessoaNom[0];
            AV20Contratada_PessoaNom = A41Contratada_PessoaNom;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         /* Using cursor P002K4 */
         pr_default.execute(2, new Object[] {AV13Servico_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A155Servico_Codigo = P002K4_A155Servico_Codigo[0];
            A156Servico_Descricao = P002K4_A156Servico_Descricao[0];
            n156Servico_Descricao = P002K4_n156Servico_Descricao[0];
            AV21Servico_Descricao = A156Servico_Descricao;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         /* Using cursor P002K5 */
         pr_default.execute(3, new Object[] {AV14Sistema_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A127Sistema_Codigo = P002K5_A127Sistema_Codigo[0];
            A416Sistema_Nome = P002K5_A416Sistema_Nome[0];
            AV22Sistema_Nome = A416Sistema_Nome;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
         /* Using cursor P002K6 */
         pr_default.execute(4, new Object[] {AV18FuncaoUsuario_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A161FuncaoUsuario_Codigo = P002K6_A161FuncaoUsuario_Codigo[0];
            A162FuncaoUsuario_Nome = P002K6_A162FuncaoUsuario_Nome[0];
            AV19FuncaoUsuario_Nome = A162FuncaoUsuario_Nome;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
      }

      protected void H2K0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxDrawBitMap(AV8ImgLogo, 50, Gx_line+33, 82, Gx_line+48) ;
               getPrinter().GxDrawBitMap(AV9ImgMD, 150, Gx_line+33, 182, Gx_line+48) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+100);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10NomeArquivo = "";
         AV20Contratada_PessoaNom = "";
         AV21Servico_Descricao = "";
         AV22Sistema_Nome = "";
         AV15Solicitacoes_Objetivo = "";
         scmdbuf = "";
         P002K2_A439Solicitacoes_Codigo = new int[1] ;
         P002K2_A39Contratada_Codigo = new int[1] ;
         P002K2_A155Servico_Codigo = new int[1] ;
         P002K2_A127Sistema_Codigo = new int[1] ;
         P002K2_A441Solicitacoes_Objetivo = new String[] {""} ;
         P002K2_n441Solicitacoes_Objetivo = new bool[] {false} ;
         A441Solicitacoes_Objetivo = "";
         P002K3_A40Contratada_PessoaCod = new int[1] ;
         P002K3_A39Contratada_Codigo = new int[1] ;
         P002K3_A41Contratada_PessoaNom = new String[] {""} ;
         P002K3_n41Contratada_PessoaNom = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         P002K4_A155Servico_Codigo = new int[1] ;
         P002K4_A156Servico_Descricao = new String[] {""} ;
         P002K4_n156Servico_Descricao = new bool[] {false} ;
         A156Servico_Descricao = "";
         P002K5_A127Sistema_Codigo = new int[1] ;
         P002K5_A416Sistema_Nome = new String[] {""} ;
         A416Sistema_Nome = "";
         P002K6_A161FuncaoUsuario_Codigo = new int[1] ;
         P002K6_A162FuncaoUsuario_Nome = new String[] {""} ;
         A162FuncaoUsuario_Nome = "";
         AV19FuncaoUsuario_Nome = "";
         AV8ImgLogo = "";
         AV9ImgMD = "";
         AV8ImgLogo = "";
         AV9ImgMD = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacaoservicopdf__default(),
            new Object[][] {
                new Object[] {
               P002K2_A439Solicitacoes_Codigo, P002K2_A39Contratada_Codigo, P002K2_A155Servico_Codigo, P002K2_A127Sistema_Codigo, P002K2_A441Solicitacoes_Objetivo, P002K2_n441Solicitacoes_Objetivo
               }
               , new Object[] {
               P002K3_A40Contratada_PessoaCod, P002K3_A39Contratada_Codigo, P002K3_A41Contratada_PessoaNom, P002K3_n41Contratada_PessoaNom
               }
               , new Object[] {
               P002K4_A155Servico_Codigo, P002K4_A156Servico_Descricao, P002K4_n156Servico_Descricao
               }
               , new Object[] {
               P002K5_A127Sistema_Codigo, P002K5_A416Sistema_Nome
               }
               , new Object[] {
               P002K6_A161FuncaoUsuario_Codigo, P002K6_A162FuncaoUsuario_Nome
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private int AV11Solicitacoes_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int Gx_OldLine ;
      private int A439Solicitacoes_Codigo ;
      private int A39Contratada_Codigo ;
      private int A155Servico_Codigo ;
      private int A127Sistema_Codigo ;
      private int AV12Contratada_codigo ;
      private int AV13Servico_Codigo ;
      private int AV14Sistema_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int AV18FuncaoUsuario_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private String AV10NomeArquivo ;
      private String AV20Contratada_PessoaNom ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private bool returnInSub ;
      private bool n441Solicitacoes_Objetivo ;
      private bool n41Contratada_PessoaNom ;
      private bool n156Servico_Descricao ;
      private String AV21Servico_Descricao ;
      private String AV15Solicitacoes_Objetivo ;
      private String A441Solicitacoes_Objetivo ;
      private String A156Servico_Descricao ;
      private String AV22Sistema_Nome ;
      private String A416Sistema_Nome ;
      private String A162FuncaoUsuario_Nome ;
      private String AV19FuncaoUsuario_Nome ;
      private String Imglogo ;
      private String Imgmd ;
      private String AV8ImgLogo ;
      private String AV9ImgMD ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Solicitacoes_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P002K2_A439Solicitacoes_Codigo ;
      private int[] P002K2_A39Contratada_Codigo ;
      private int[] P002K2_A155Servico_Codigo ;
      private int[] P002K2_A127Sistema_Codigo ;
      private String[] P002K2_A441Solicitacoes_Objetivo ;
      private bool[] P002K2_n441Solicitacoes_Objetivo ;
      private int[] P002K3_A40Contratada_PessoaCod ;
      private int[] P002K3_A39Contratada_Codigo ;
      private String[] P002K3_A41Contratada_PessoaNom ;
      private bool[] P002K3_n41Contratada_PessoaNom ;
      private int[] P002K4_A155Servico_Codigo ;
      private String[] P002K4_A156Servico_Descricao ;
      private bool[] P002K4_n156Servico_Descricao ;
      private int[] P002K5_A127Sistema_Codigo ;
      private String[] P002K5_A416Sistema_Nome ;
      private int[] P002K6_A161FuncaoUsuario_Codigo ;
      private String[] P002K6_A162FuncaoUsuario_Nome ;
   }

   public class solicitacaoservicopdf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002K2 ;
          prmP002K2 = new Object[] {
          new Object[] {"@AV11Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002K3 ;
          prmP002K3 = new Object[] {
          new Object[] {"@AV12Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002K4 ;
          prmP002K4 = new Object[] {
          new Object[] {"@AV13Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002K5 ;
          prmP002K5 = new Object[] {
          new Object[] {"@AV14Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002K6 ;
          prmP002K6 = new Object[] {
          new Object[] {"@AV18FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002K2", "SELECT [Solicitacoes_Codigo], [Contratada_Codigo], [Servico_Codigo], [Sistema_Codigo], [Solicitacoes_Objetivo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @AV11Solicitacoes_Codigo ORDER BY [Solicitacoes_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002K2,1,0,false,true )
             ,new CursorDef("P002K3", "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_Codigo] = @AV12Contratada_codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002K3,1,0,false,true )
             ,new CursorDef("P002K4", "SELECT [Servico_Codigo], [Servico_Descricao] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV13Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002K4,1,0,false,true )
             ,new CursorDef("P002K5", "SELECT [Sistema_Codigo], [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV14Sistema_Codigo ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002K5,1,0,false,true )
             ,new CursorDef("P002K6", "SELECT [FuncaoUsuario_Codigo], [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @AV18FuncaoUsuario_Codigo ORDER BY [FuncaoUsuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002K6,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
