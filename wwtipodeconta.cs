/*
               File: WWTipodeConta
        Description:  Tipo de Contas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:25:45.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwtipodeconta : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwtipodeconta( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwtipodeconta( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         dynavTipodeconta_codigo1 = new GXCombobox();
         dynavTipodeconta_contapaicod1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         dynavTipodeconta_codigo2 = new GXCombobox();
         dynavTipodeconta_contapaicod2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         dynavTipodeconta_codigo3 = new GXCombobox();
         dynavTipodeconta_contapaicod3 = new GXCombobox();
         cmbTipodeConta_Tipo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTIPODECONTA_CODIGO1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvTIPODECONTA_CODIGO1FH2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTIPODECONTA_CONTAPAICOD1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvTIPODECONTA_CONTAPAICOD1FH2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTIPODECONTA_CODIGO2") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvTIPODECONTA_CODIGO2FH2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTIPODECONTA_CONTAPAICOD2") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvTIPODECONTA_CONTAPAICOD2FH2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTIPODECONTA_CODIGO3") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvTIPODECONTA_CODIGO3FH2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTIPODECONTA_CONTAPAICOD3") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvTIPODECONTA_CONTAPAICOD3FH2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_79 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_79_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_79_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV31TipodeConta_Codigo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TipodeConta_Codigo1", AV31TipodeConta_Codigo1);
               AV17TipodeConta_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipodeConta_Nome1", AV17TipodeConta_Nome1);
               AV34TipoDeConta_ContaPaiCod1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDeConta_ContaPaiCod1", AV34TipoDeConta_ContaPaiCod1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV32TipodeConta_Codigo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TipodeConta_Codigo2", AV32TipodeConta_Codigo2);
               AV21TipodeConta_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipodeConta_Nome2", AV21TipodeConta_Nome2);
               AV35TipoDeConta_ContaPaiCod2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TipoDeConta_ContaPaiCod2", AV35TipoDeConta_ContaPaiCod2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV33TipodeConta_Codigo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TipodeConta_Codigo3", AV33TipodeConta_Codigo3);
               AV25TipodeConta_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipodeConta_Nome3", AV25TipodeConta_Nome3);
               AV36TipoDeConta_ContaPaiCod3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TipoDeConta_ContaPaiCod3", AV36TipoDeConta_ContaPaiCod3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV40TFTipodeConta_Codigo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipodeConta_Codigo", AV40TFTipodeConta_Codigo);
               AV41TFTipodeConta_Codigo_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipodeConta_Codigo_Sel", AV41TFTipodeConta_Codigo_Sel);
               AV44TFTipodeConta_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFTipodeConta_Nome", AV44TFTipodeConta_Nome);
               AV45TFTipodeConta_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFTipodeConta_Nome_Sel", AV45TFTipodeConta_Nome_Sel);
               AV52TFTipoDeConta_ContaPaiCod = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFTipoDeConta_ContaPaiCod", AV52TFTipoDeConta_ContaPaiCod);
               AV53TFTipoDeConta_ContaPaiCod_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFTipoDeConta_ContaPaiCod_Sel", AV53TFTipoDeConta_ContaPaiCod_Sel);
               AV42ddo_TipodeConta_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_TipodeConta_CodigoTitleControlIdToReplace", AV42ddo_TipodeConta_CodigoTitleControlIdToReplace);
               AV46ddo_TipodeConta_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_TipodeConta_NomeTitleControlIdToReplace", AV46ddo_TipodeConta_NomeTitleControlIdToReplace);
               AV50ddo_TipodeConta_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_TipodeConta_TipoTitleControlIdToReplace", AV50ddo_TipodeConta_TipoTitleControlIdToReplace);
               AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace", AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV49TFTipodeConta_Tipo_Sels);
               AV84Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A870TipodeConta_Codigo = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31TipodeConta_Codigo1, AV17TipodeConta_Nome1, AV34TipoDeConta_ContaPaiCod1, AV19DynamicFiltersSelector2, AV32TipodeConta_Codigo2, AV21TipodeConta_Nome2, AV35TipoDeConta_ContaPaiCod2, AV23DynamicFiltersSelector3, AV33TipodeConta_Codigo3, AV25TipodeConta_Nome3, AV36TipoDeConta_ContaPaiCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV40TFTipodeConta_Codigo, AV41TFTipodeConta_Codigo_Sel, AV44TFTipodeConta_Nome, AV45TFTipodeConta_Nome_Sel, AV52TFTipoDeConta_ContaPaiCod, AV53TFTipoDeConta_ContaPaiCod_Sel, AV42ddo_TipodeConta_CodigoTitleControlIdToReplace, AV46ddo_TipodeConta_NomeTitleControlIdToReplace, AV50ddo_TipodeConta_TipoTitleControlIdToReplace, AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace, AV49TFTipodeConta_Tipo_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A870TipodeConta_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAFH2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTFH2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813254555");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwtipodeconta.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODECONTA_CODIGO1", StringUtil.RTrim( AV31TipodeConta_Codigo1));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODECONTA_NOME1", StringUtil.RTrim( AV17TipodeConta_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODECONTA_CONTAPAICOD1", StringUtil.RTrim( AV34TipoDeConta_ContaPaiCod1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODECONTA_CODIGO2", StringUtil.RTrim( AV32TipodeConta_Codigo2));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODECONTA_NOME2", StringUtil.RTrim( AV21TipodeConta_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODECONTA_CONTAPAICOD2", StringUtil.RTrim( AV35TipoDeConta_ContaPaiCod2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODECONTA_CODIGO3", StringUtil.RTrim( AV33TipodeConta_Codigo3));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODECONTA_NOME3", StringUtil.RTrim( AV25TipodeConta_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODECONTA_CONTAPAICOD3", StringUtil.RTrim( AV36TipoDeConta_ContaPaiCod3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODECONTA_CODIGO", StringUtil.RTrim( AV40TFTipodeConta_Codigo));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODECONTA_CODIGO_SEL", StringUtil.RTrim( AV41TFTipodeConta_Codigo_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODECONTA_NOME", StringUtil.RTrim( AV44TFTipodeConta_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODECONTA_NOME_SEL", StringUtil.RTrim( AV45TFTipodeConta_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODECONTA_CONTAPAICOD", StringUtil.RTrim( AV52TFTipoDeConta_ContaPaiCod));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODECONTA_CONTAPAICOD_SEL", StringUtil.RTrim( AV53TFTipoDeConta_ContaPaiCod_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_79", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_79), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV55DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV55DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPODECONTA_CODIGOTITLEFILTERDATA", AV39TipodeConta_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPODECONTA_CODIGOTITLEFILTERDATA", AV39TipodeConta_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPODECONTA_NOMETITLEFILTERDATA", AV43TipodeConta_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPODECONTA_NOMETITLEFILTERDATA", AV43TipodeConta_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPODECONTA_TIPOTITLEFILTERDATA", AV47TipodeConta_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPODECONTA_TIPOTITLEFILTERDATA", AV47TipodeConta_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPODECONTA_CONTAPAICODTITLEFILTERDATA", AV51TipoDeConta_ContaPaiCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPODECONTA_CONTAPAICODTITLEFILTERDATA", AV51TipoDeConta_ContaPaiCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFTIPODECONTA_TIPO_SELS", AV49TFTipodeConta_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFTIPODECONTA_TIPO_SELS", AV49TFTipodeConta_Tipo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV84Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Caption", StringUtil.RTrim( Ddo_tipodeconta_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_tipodeconta_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Cls", StringUtil.RTrim( Ddo_tipodeconta_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_tipodeconta_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Selectedvalue_set", StringUtil.RTrim( Ddo_tipodeconta_codigo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_tipodeconta_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tipodeconta_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_tipodeconta_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_tipodeconta_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_tipodeconta_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_tipodeconta_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_tipodeconta_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_tipodeconta_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_tipodeconta_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Datalisttype", StringUtil.RTrim( Ddo_tipodeconta_codigo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Datalistproc", StringUtil.RTrim( Ddo_tipodeconta_codigo_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tipodeconta_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_tipodeconta_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_tipodeconta_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_tipodeconta_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_tipodeconta_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_tipodeconta_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_tipodeconta_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Caption", StringUtil.RTrim( Ddo_tipodeconta_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Tooltip", StringUtil.RTrim( Ddo_tipodeconta_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Cls", StringUtil.RTrim( Ddo_tipodeconta_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_tipodeconta_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_tipodeconta_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_tipodeconta_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tipodeconta_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_tipodeconta_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_tipodeconta_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Sortedstatus", StringUtil.RTrim( Ddo_tipodeconta_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Includefilter", StringUtil.BoolToStr( Ddo_tipodeconta_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Filtertype", StringUtil.RTrim( Ddo_tipodeconta_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_tipodeconta_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_tipodeconta_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Datalisttype", StringUtil.RTrim( Ddo_tipodeconta_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Datalistproc", StringUtil.RTrim( Ddo_tipodeconta_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tipodeconta_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Sortasc", StringUtil.RTrim( Ddo_tipodeconta_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Sortdsc", StringUtil.RTrim( Ddo_tipodeconta_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Loadingdata", StringUtil.RTrim( Ddo_tipodeconta_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Cleanfilter", StringUtil.RTrim( Ddo_tipodeconta_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Noresultsfound", StringUtil.RTrim( Ddo_tipodeconta_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_tipodeconta_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Caption", StringUtil.RTrim( Ddo_tipodeconta_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Tooltip", StringUtil.RTrim( Ddo_tipodeconta_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Cls", StringUtil.RTrim( Ddo_tipodeconta_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_tipodeconta_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_tipodeconta_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tipodeconta_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_tipodeconta_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_tipodeconta_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_tipodeconta_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_tipodeconta_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_tipodeconta_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Datalisttype", StringUtil.RTrim( Ddo_tipodeconta_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_tipodeconta_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_tipodeconta_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Sortasc", StringUtil.RTrim( Ddo_tipodeconta_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Sortdsc", StringUtil.RTrim( Ddo_tipodeconta_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_tipodeconta_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_tipodeconta_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Caption", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Tooltip", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Cls", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Filteredtext_set", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Selectedvalue_set", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Includesortasc", StringUtil.BoolToStr( Ddo_tipodeconta_contapaicod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Includesortdsc", StringUtil.BoolToStr( Ddo_tipodeconta_contapaicod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Sortedstatus", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Includefilter", StringUtil.BoolToStr( Ddo_tipodeconta_contapaicod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Filtertype", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Filterisrange", StringUtil.BoolToStr( Ddo_tipodeconta_contapaicod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Includedatalist", StringUtil.BoolToStr( Ddo_tipodeconta_contapaicod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Datalisttype", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Datalistproc", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tipodeconta_contapaicod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Sortasc", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Sortdsc", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Loadingdata", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Cleanfilter", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Noresultsfound", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Searchbuttontext", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_tipodeconta_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_tipodeconta_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CODIGO_Selectedvalue_get", StringUtil.RTrim( Ddo_tipodeconta_codigo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Activeeventkey", StringUtil.RTrim( Ddo_tipodeconta_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_tipodeconta_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_tipodeconta_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_tipodeconta_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_tipodeconta_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Activeeventkey", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Filteredtext_get", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODECONTA_CONTAPAICOD_Selectedvalue_get", StringUtil.RTrim( Ddo_tipodeconta_contapaicod_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEFH2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTFH2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwtipodeconta.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWTipodeConta" ;
      }

      public override String GetPgmdesc( )
      {
         return " Tipo de Contas" ;
      }

      protected void WBFH0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_FH2( true) ;
         }
         else
         {
            wb_table1_2_FH2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_FH2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(90, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(91, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodeconta_codigo_Internalname, StringUtil.RTrim( AV40TFTipodeConta_Codigo), StringUtil.RTrim( context.localUtil.Format( AV40TFTipodeConta_Codigo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodeconta_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodeconta_codigo_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipodeConta.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodeconta_codigo_sel_Internalname, StringUtil.RTrim( AV41TFTipodeConta_Codigo_Sel), StringUtil.RTrim( context.localUtil.Format( AV41TFTipodeConta_Codigo_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodeconta_codigo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodeconta_codigo_sel_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipodeConta.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodeconta_nome_Internalname, StringUtil.RTrim( AV44TFTipodeConta_Nome), StringUtil.RTrim( context.localUtil.Format( AV44TFTipodeConta_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodeconta_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodeconta_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipodeConta.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodeconta_nome_sel_Internalname, StringUtil.RTrim( AV45TFTipodeConta_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV45TFTipodeConta_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodeconta_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodeconta_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipodeConta.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodeconta_contapaicod_Internalname, StringUtil.RTrim( AV52TFTipoDeConta_ContaPaiCod), StringUtil.RTrim( context.localUtil.Format( AV52TFTipoDeConta_ContaPaiCod, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodeconta_contapaicod_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodeconta_contapaicod_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipodeConta.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodeconta_contapaicod_sel_Internalname, StringUtil.RTrim( AV53TFTipoDeConta_ContaPaiCod_Sel), StringUtil.RTrim( context.localUtil.Format( AV53TFTipoDeConta_ContaPaiCod_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodeconta_contapaicod_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodeconta_contapaicod_sel_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipodeConta.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPODECONTA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tipodeconta_codigotitlecontrolidtoreplace_Internalname, AV42ddo_TipodeConta_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", 0, edtavDdo_tipodeconta_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTipodeConta.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPODECONTA_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tipodeconta_nometitlecontrolidtoreplace_Internalname, AV46ddo_TipodeConta_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavDdo_tipodeconta_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTipodeConta.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPODECONTA_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tipodeconta_tipotitlecontrolidtoreplace_Internalname, AV50ddo_TipodeConta_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", 0, edtavDdo_tipodeconta_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTipodeConta.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPODECONTA_CONTAPAICODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tipodeconta_contapaicodtitlecontrolidtoreplace_Internalname, AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavDdo_tipodeconta_contapaicodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTipodeConta.htm");
         }
         wbLoad = true;
      }

      protected void STARTFH2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Tipo de Contas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPFH0( ) ;
      }

      protected void WSFH2( )
      {
         STARTFH2( ) ;
         EVTFH2( ) ;
      }

      protected void EVTFH2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11FH2 */
                              E11FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TIPODECONTA_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12FH2 */
                              E12FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TIPODECONTA_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13FH2 */
                              E13FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TIPODECONTA_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14FH2 */
                              E14FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TIPODECONTA_CONTAPAICOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15FH2 */
                              E15FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16FH2 */
                              E16FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17FH2 */
                              E17FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18FH2 */
                              E18FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19FH2 */
                              E19FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20FH2 */
                              E20FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21FH2 */
                              E21FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22FH2 */
                              E22FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23FH2 */
                              E23FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24FH2 */
                              E24FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25FH2 */
                              E25FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26FH2 */
                              E26FH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_79_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
                              SubsflControlProps_792( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV82Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV83Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A870TipodeConta_Codigo = cgiGet( edtTipodeConta_Codigo_Internalname);
                              A871TipodeConta_Nome = StringUtil.Upper( cgiGet( edtTipodeConta_Nome_Internalname));
                              cmbTipodeConta_Tipo.Name = cmbTipodeConta_Tipo_Internalname;
                              cmbTipodeConta_Tipo.CurrentValue = cgiGet( cmbTipodeConta_Tipo_Internalname);
                              A873TipodeConta_Tipo = cgiGet( cmbTipodeConta_Tipo_Internalname);
                              A885TipoDeConta_ContaPaiCod = cgiGet( edtTipoDeConta_ContaPaiCod_Internalname);
                              n885TipoDeConta_ContaPaiCod = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27FH2 */
                                    E27FH2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28FH2 */
                                    E28FH2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29FH2 */
                                    E29FH2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodeconta_codigo1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CODIGO1"), AV31TipodeConta_Codigo1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodeconta_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_NOME1"), AV17TipodeConta_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodeconta_contapaicod1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CONTAPAICOD1"), AV34TipoDeConta_ContaPaiCod1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodeconta_codigo2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CODIGO2"), AV32TipodeConta_Codigo2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodeconta_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_NOME2"), AV21TipodeConta_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodeconta_contapaicod2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CONTAPAICOD2"), AV35TipoDeConta_ContaPaiCod2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodeconta_codigo3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CODIGO3"), AV33TipodeConta_Codigo3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodeconta_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_NOME3"), AV25TipodeConta_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodeconta_contapaicod3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CONTAPAICOD3"), AV36TipoDeConta_ContaPaiCod3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftipodeconta_codigo Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_CODIGO"), AV40TFTipodeConta_Codigo) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftipodeconta_codigo_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_CODIGO_SEL"), AV41TFTipodeConta_Codigo_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftipodeconta_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_NOME"), AV44TFTipodeConta_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftipodeconta_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_NOME_SEL"), AV45TFTipodeConta_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftipodeconta_contapaicod Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_CONTAPAICOD"), AV52TFTipoDeConta_ContaPaiCod) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftipodeconta_contapaicod_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_CONTAPAICOD_SEL"), AV53TFTipoDeConta_ContaPaiCod_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEFH2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAFH2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("TIPODECONTA_CODIGO", "Conta", 0);
            cmbavDynamicfiltersselector1.addItem("TIPODECONTA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("TIPODECONTA_CONTAPAICOD", "Conta Pai", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            dynavTipodeconta_codigo1.Name = "vTIPODECONTA_CODIGO1";
            dynavTipodeconta_codigo1.WebTags = "";
            dynavTipodeconta_contapaicod1.Name = "vTIPODECONTA_CONTAPAICOD1";
            dynavTipodeconta_contapaicod1.WebTags = "";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("TIPODECONTA_CODIGO", "Conta", 0);
            cmbavDynamicfiltersselector2.addItem("TIPODECONTA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("TIPODECONTA_CONTAPAICOD", "Conta Pai", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            dynavTipodeconta_codigo2.Name = "vTIPODECONTA_CODIGO2";
            dynavTipodeconta_codigo2.WebTags = "";
            dynavTipodeconta_contapaicod2.Name = "vTIPODECONTA_CONTAPAICOD2";
            dynavTipodeconta_contapaicod2.WebTags = "";
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("TIPODECONTA_CODIGO", "Conta", 0);
            cmbavDynamicfiltersselector3.addItem("TIPODECONTA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("TIPODECONTA_CONTAPAICOD", "Conta Pai", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            dynavTipodeconta_codigo3.Name = "vTIPODECONTA_CODIGO3";
            dynavTipodeconta_codigo3.WebTags = "";
            dynavTipodeconta_contapaicod3.Name = "vTIPODECONTA_CONTAPAICOD3";
            dynavTipodeconta_contapaicod3.WebTags = "";
            GXCCtl = "TIPODECONTA_TIPO_" + sGXsfl_79_idx;
            cmbTipodeConta_Tipo.Name = GXCCtl;
            cmbTipodeConta_Tipo.WebTags = "";
            cmbTipodeConta_Tipo.addItem("", "(Nenhum)", 0);
            cmbTipodeConta_Tipo.addItem("D", "D�bito", 0);
            cmbTipodeConta_Tipo.addItem("C", "Cr�dito", 0);
            if ( cmbTipodeConta_Tipo.ItemCount > 0 )
            {
               A873TipodeConta_Tipo = cmbTipodeConta_Tipo.getValidValue(A873TipodeConta_Tipo);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvTIPODECONTA_CODIGO1FH2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPODECONTA_CODIGO1_dataFH2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPODECONTA_CODIGO1_htmlFH2( )
      {
         String gxdynajaxvalue ;
         GXDLVvTIPODECONTA_CODIGO1_dataFH2( ) ;
         gxdynajaxindex = 1;
         dynavTipodeconta_codigo1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavTipodeconta_codigo1.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipodeconta_codigo1.ItemCount > 0 )
         {
            AV31TipodeConta_Codigo1 = dynavTipodeconta_codigo1.getValidValue(AV31TipodeConta_Codigo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TipodeConta_Codigo1", AV31TipodeConta_Codigo1);
         }
      }

      protected void GXDLVvTIPODECONTA_CODIGO1_dataFH2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00FH2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00FH2_A870TipodeConta_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FH2_A871TipodeConta_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvTIPODECONTA_CONTAPAICOD1FH2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPODECONTA_CONTAPAICOD1_dataFH2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPODECONTA_CONTAPAICOD1_htmlFH2( )
      {
         String gxdynajaxvalue ;
         GXDLVvTIPODECONTA_CONTAPAICOD1_dataFH2( ) ;
         gxdynajaxindex = 1;
         dynavTipodeconta_contapaicod1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavTipodeconta_contapaicod1.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipodeconta_contapaicod1.ItemCount > 0 )
         {
            AV34TipoDeConta_ContaPaiCod1 = dynavTipodeconta_contapaicod1.getValidValue(AV34TipoDeConta_ContaPaiCod1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDeConta_ContaPaiCod1", AV34TipoDeConta_ContaPaiCod1);
         }
      }

      protected void GXDLVvTIPODECONTA_CONTAPAICOD1_dataFH2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00FH3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00FH3_A870TipodeConta_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FH3_A871TipodeConta_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvTIPODECONTA_CODIGO2FH2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPODECONTA_CODIGO2_dataFH2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPODECONTA_CODIGO2_htmlFH2( )
      {
         String gxdynajaxvalue ;
         GXDLVvTIPODECONTA_CODIGO2_dataFH2( ) ;
         gxdynajaxindex = 1;
         dynavTipodeconta_codigo2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavTipodeconta_codigo2.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipodeconta_codigo2.ItemCount > 0 )
         {
            AV32TipodeConta_Codigo2 = dynavTipodeconta_codigo2.getValidValue(AV32TipodeConta_Codigo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TipodeConta_Codigo2", AV32TipodeConta_Codigo2);
         }
      }

      protected void GXDLVvTIPODECONTA_CODIGO2_dataFH2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00FH4 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00FH4_A870TipodeConta_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FH4_A871TipodeConta_Nome[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvTIPODECONTA_CONTAPAICOD2FH2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPODECONTA_CONTAPAICOD2_dataFH2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPODECONTA_CONTAPAICOD2_htmlFH2( )
      {
         String gxdynajaxvalue ;
         GXDLVvTIPODECONTA_CONTAPAICOD2_dataFH2( ) ;
         gxdynajaxindex = 1;
         dynavTipodeconta_contapaicod2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavTipodeconta_contapaicod2.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipodeconta_contapaicod2.ItemCount > 0 )
         {
            AV35TipoDeConta_ContaPaiCod2 = dynavTipodeconta_contapaicod2.getValidValue(AV35TipoDeConta_ContaPaiCod2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TipoDeConta_ContaPaiCod2", AV35TipoDeConta_ContaPaiCod2);
         }
      }

      protected void GXDLVvTIPODECONTA_CONTAPAICOD2_dataFH2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00FH5 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00FH5_A870TipodeConta_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FH5_A871TipodeConta_Nome[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvTIPODECONTA_CODIGO3FH2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPODECONTA_CODIGO3_dataFH2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPODECONTA_CODIGO3_htmlFH2( )
      {
         String gxdynajaxvalue ;
         GXDLVvTIPODECONTA_CODIGO3_dataFH2( ) ;
         gxdynajaxindex = 1;
         dynavTipodeconta_codigo3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavTipodeconta_codigo3.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipodeconta_codigo3.ItemCount > 0 )
         {
            AV33TipodeConta_Codigo3 = dynavTipodeconta_codigo3.getValidValue(AV33TipodeConta_Codigo3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TipodeConta_Codigo3", AV33TipodeConta_Codigo3);
         }
      }

      protected void GXDLVvTIPODECONTA_CODIGO3_dataFH2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00FH6 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00FH6_A870TipodeConta_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FH6_A871TipodeConta_Nome[0]));
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVvTIPODECONTA_CONTAPAICOD3FH2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPODECONTA_CONTAPAICOD3_dataFH2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPODECONTA_CONTAPAICOD3_htmlFH2( )
      {
         String gxdynajaxvalue ;
         GXDLVvTIPODECONTA_CONTAPAICOD3_dataFH2( ) ;
         gxdynajaxindex = 1;
         dynavTipodeconta_contapaicod3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavTipodeconta_contapaicod3.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipodeconta_contapaicod3.ItemCount > 0 )
         {
            AV36TipoDeConta_ContaPaiCod3 = dynavTipodeconta_contapaicod3.getValidValue(AV36TipoDeConta_ContaPaiCod3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TipoDeConta_ContaPaiCod3", AV36TipoDeConta_ContaPaiCod3);
         }
      }

      protected void GXDLVvTIPODECONTA_CONTAPAICOD3_dataFH2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00FH7 */
         pr_default.execute(5);
         while ( (pr_default.getStatus(5) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00FH7_A870TipodeConta_Codigo[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FH7_A871TipodeConta_Nome[0]));
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_792( ) ;
         while ( nGXsfl_79_idx <= nRC_GXsfl_79 )
         {
            sendrow_792( ) ;
            nGXsfl_79_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_79_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_79_idx+1));
            sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
            SubsflControlProps_792( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV31TipodeConta_Codigo1 ,
                                       String AV17TipodeConta_Nome1 ,
                                       String AV34TipoDeConta_ContaPaiCod1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV32TipodeConta_Codigo2 ,
                                       String AV21TipodeConta_Nome2 ,
                                       String AV35TipoDeConta_ContaPaiCod2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV33TipodeConta_Codigo3 ,
                                       String AV25TipodeConta_Nome3 ,
                                       String AV36TipoDeConta_ContaPaiCod3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV40TFTipodeConta_Codigo ,
                                       String AV41TFTipodeConta_Codigo_Sel ,
                                       String AV44TFTipodeConta_Nome ,
                                       String AV45TFTipodeConta_Nome_Sel ,
                                       String AV52TFTipoDeConta_ContaPaiCod ,
                                       String AV53TFTipoDeConta_ContaPaiCod_Sel ,
                                       String AV42ddo_TipodeConta_CodigoTitleControlIdToReplace ,
                                       String AV46ddo_TipodeConta_NomeTitleControlIdToReplace ,
                                       String AV50ddo_TipodeConta_TipoTitleControlIdToReplace ,
                                       String AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace ,
                                       IGxCollection AV49TFTipodeConta_Tipo_Sels ,
                                       String AV84Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       String A870TipodeConta_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFFH2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_TIPODECONTA_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A870TipodeConta_Codigo, ""))));
         GxWebStd.gx_hidden_field( context, "TIPODECONTA_CODIGO", StringUtil.RTrim( A870TipodeConta_Codigo));
         GxWebStd.gx_hidden_field( context, "gxhash_TIPODECONTA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A871TipodeConta_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "TIPODECONTA_NOME", StringUtil.RTrim( A871TipodeConta_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_TIPODECONTA_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A873TipodeConta_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "TIPODECONTA_TIPO", StringUtil.RTrim( A873TipodeConta_Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_TIPODECONTA_CONTAPAICOD", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A885TipoDeConta_ContaPaiCod, ""))));
         GxWebStd.gx_hidden_field( context, "TIPODECONTA_CONTAPAICOD", StringUtil.RTrim( A885TipoDeConta_ContaPaiCod));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( dynavTipodeconta_codigo1.ItemCount > 0 )
         {
            AV31TipodeConta_Codigo1 = dynavTipodeconta_codigo1.getValidValue(AV31TipodeConta_Codigo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TipodeConta_Codigo1", AV31TipodeConta_Codigo1);
         }
         if ( dynavTipodeconta_contapaicod1.ItemCount > 0 )
         {
            AV34TipoDeConta_ContaPaiCod1 = dynavTipodeconta_contapaicod1.getValidValue(AV34TipoDeConta_ContaPaiCod1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDeConta_ContaPaiCod1", AV34TipoDeConta_ContaPaiCod1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( dynavTipodeconta_codigo2.ItemCount > 0 )
         {
            AV32TipodeConta_Codigo2 = dynavTipodeconta_codigo2.getValidValue(AV32TipodeConta_Codigo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TipodeConta_Codigo2", AV32TipodeConta_Codigo2);
         }
         if ( dynavTipodeconta_contapaicod2.ItemCount > 0 )
         {
            AV35TipoDeConta_ContaPaiCod2 = dynavTipodeconta_contapaicod2.getValidValue(AV35TipoDeConta_ContaPaiCod2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TipoDeConta_ContaPaiCod2", AV35TipoDeConta_ContaPaiCod2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( dynavTipodeconta_codigo3.ItemCount > 0 )
         {
            AV33TipodeConta_Codigo3 = dynavTipodeconta_codigo3.getValidValue(AV33TipodeConta_Codigo3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TipodeConta_Codigo3", AV33TipodeConta_Codigo3);
         }
         if ( dynavTipodeconta_contapaicod3.ItemCount > 0 )
         {
            AV36TipoDeConta_ContaPaiCod3 = dynavTipodeconta_contapaicod3.getValidValue(AV36TipoDeConta_ContaPaiCod3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TipoDeConta_ContaPaiCod3", AV36TipoDeConta_ContaPaiCod3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFFH2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV84Pgmname = "WWTipodeConta";
         context.Gx_err = 0;
      }

      protected void RFFH2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 79;
         /* Execute user event: E28FH2 */
         E28FH2 ();
         nGXsfl_79_idx = 1;
         sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
         SubsflControlProps_792( ) ;
         nGXsfl_79_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_792( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(6, new Object[]{ new Object[]{
                                                 A873TipodeConta_Tipo ,
                                                 AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels ,
                                                 AV61WWTipodeContaDS_1_Dynamicfiltersselector1 ,
                                                 AV62WWTipodeContaDS_2_Tipodeconta_codigo1 ,
                                                 AV63WWTipodeContaDS_3_Tipodeconta_nome1 ,
                                                 AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 ,
                                                 AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 ,
                                                 AV66WWTipodeContaDS_6_Dynamicfiltersselector2 ,
                                                 AV67WWTipodeContaDS_7_Tipodeconta_codigo2 ,
                                                 AV68WWTipodeContaDS_8_Tipodeconta_nome2 ,
                                                 AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 ,
                                                 AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 ,
                                                 AV71WWTipodeContaDS_11_Dynamicfiltersselector3 ,
                                                 AV72WWTipodeContaDS_12_Tipodeconta_codigo3 ,
                                                 AV73WWTipodeContaDS_13_Tipodeconta_nome3 ,
                                                 AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 ,
                                                 AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel ,
                                                 AV75WWTipodeContaDS_15_Tftipodeconta_codigo ,
                                                 AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel ,
                                                 AV77WWTipodeContaDS_17_Tftipodeconta_nome ,
                                                 AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels.Count ,
                                                 AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ,
                                                 AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod ,
                                                 A870TipodeConta_Codigo ,
                                                 A871TipodeConta_Nome ,
                                                 A885TipoDeConta_ContaPaiCod ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV62WWTipodeContaDS_2_Tipodeconta_codigo1 = StringUtil.PadR( StringUtil.RTrim( AV62WWTipodeContaDS_2_Tipodeconta_codigo1), 20, "%");
            lV63WWTipodeContaDS_3_Tipodeconta_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWTipodeContaDS_3_Tipodeconta_nome1), 50, "%");
            lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 = StringUtil.PadR( StringUtil.RTrim( AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1), 20, "%");
            lV67WWTipodeContaDS_7_Tipodeconta_codigo2 = StringUtil.PadR( StringUtil.RTrim( AV67WWTipodeContaDS_7_Tipodeconta_codigo2), 20, "%");
            lV68WWTipodeContaDS_8_Tipodeconta_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWTipodeContaDS_8_Tipodeconta_nome2), 50, "%");
            lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 = StringUtil.PadR( StringUtil.RTrim( AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2), 20, "%");
            lV72WWTipodeContaDS_12_Tipodeconta_codigo3 = StringUtil.PadR( StringUtil.RTrim( AV72WWTipodeContaDS_12_Tipodeconta_codigo3), 20, "%");
            lV73WWTipodeContaDS_13_Tipodeconta_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWTipodeContaDS_13_Tipodeconta_nome3), 50, "%");
            lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 = StringUtil.PadR( StringUtil.RTrim( AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3), 20, "%");
            lV75WWTipodeContaDS_15_Tftipodeconta_codigo = StringUtil.PadR( StringUtil.RTrim( AV75WWTipodeContaDS_15_Tftipodeconta_codigo), 20, "%");
            lV77WWTipodeContaDS_17_Tftipodeconta_nome = StringUtil.PadR( StringUtil.RTrim( AV77WWTipodeContaDS_17_Tftipodeconta_nome), 50, "%");
            lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod = StringUtil.PadR( StringUtil.RTrim( AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod), 20, "%");
            /* Using cursor H00FH8 */
            pr_default.execute(6, new Object[] {lV62WWTipodeContaDS_2_Tipodeconta_codigo1, lV63WWTipodeContaDS_3_Tipodeconta_nome1, lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1, lV67WWTipodeContaDS_7_Tipodeconta_codigo2, lV68WWTipodeContaDS_8_Tipodeconta_nome2, lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2, lV72WWTipodeContaDS_12_Tipodeconta_codigo3, lV73WWTipodeContaDS_13_Tipodeconta_nome3, lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3, lV75WWTipodeContaDS_15_Tftipodeconta_codigo, AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel, lV77WWTipodeContaDS_17_Tftipodeconta_nome, AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel, lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod, AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_79_idx = 1;
            while ( ( (pr_default.getStatus(6) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A885TipoDeConta_ContaPaiCod = H00FH8_A885TipoDeConta_ContaPaiCod[0];
               n885TipoDeConta_ContaPaiCod = H00FH8_n885TipoDeConta_ContaPaiCod[0];
               A873TipodeConta_Tipo = H00FH8_A873TipodeConta_Tipo[0];
               A871TipodeConta_Nome = H00FH8_A871TipodeConta_Nome[0];
               A870TipodeConta_Codigo = H00FH8_A870TipodeConta_Codigo[0];
               /* Execute user event: E29FH2 */
               E29FH2 ();
               pr_default.readNext(6);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(6) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(6);
            wbEnd = 79;
            WBFH0( ) ;
         }
         nGXsfl_79_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV61WWTipodeContaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWTipodeContaDS_2_Tipodeconta_codigo1 = AV31TipodeConta_Codigo1;
         AV63WWTipodeContaDS_3_Tipodeconta_nome1 = AV17TipodeConta_Nome1;
         AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 = AV34TipoDeConta_ContaPaiCod1;
         AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV66WWTipodeContaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV67WWTipodeContaDS_7_Tipodeconta_codigo2 = AV32TipodeConta_Codigo2;
         AV68WWTipodeContaDS_8_Tipodeconta_nome2 = AV21TipodeConta_Nome2;
         AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 = AV35TipoDeConta_ContaPaiCod2;
         AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV71WWTipodeContaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV72WWTipodeContaDS_12_Tipodeconta_codigo3 = AV33TipodeConta_Codigo3;
         AV73WWTipodeContaDS_13_Tipodeconta_nome3 = AV25TipodeConta_Nome3;
         AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 = AV36TipoDeConta_ContaPaiCod3;
         AV75WWTipodeContaDS_15_Tftipodeconta_codigo = AV40TFTipodeConta_Codigo;
         AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel = AV41TFTipodeConta_Codigo_Sel;
         AV77WWTipodeContaDS_17_Tftipodeconta_nome = AV44TFTipodeConta_Nome;
         AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel = AV45TFTipodeConta_Nome_Sel;
         AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels = AV49TFTipodeConta_Tipo_Sels;
         AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod = AV52TFTipoDeConta_ContaPaiCod;
         AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = AV53TFTipoDeConta_ContaPaiCod_Sel;
         pr_default.dynParam(7, new Object[]{ new Object[]{
                                              A873TipodeConta_Tipo ,
                                              AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels ,
                                              AV61WWTipodeContaDS_1_Dynamicfiltersselector1 ,
                                              AV62WWTipodeContaDS_2_Tipodeconta_codigo1 ,
                                              AV63WWTipodeContaDS_3_Tipodeconta_nome1 ,
                                              AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 ,
                                              AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 ,
                                              AV66WWTipodeContaDS_6_Dynamicfiltersselector2 ,
                                              AV67WWTipodeContaDS_7_Tipodeconta_codigo2 ,
                                              AV68WWTipodeContaDS_8_Tipodeconta_nome2 ,
                                              AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 ,
                                              AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 ,
                                              AV71WWTipodeContaDS_11_Dynamicfiltersselector3 ,
                                              AV72WWTipodeContaDS_12_Tipodeconta_codigo3 ,
                                              AV73WWTipodeContaDS_13_Tipodeconta_nome3 ,
                                              AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 ,
                                              AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel ,
                                              AV75WWTipodeContaDS_15_Tftipodeconta_codigo ,
                                              AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel ,
                                              AV77WWTipodeContaDS_17_Tftipodeconta_nome ,
                                              AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels.Count ,
                                              AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ,
                                              AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod ,
                                              A870TipodeConta_Codigo ,
                                              A871TipodeConta_Nome ,
                                              A885TipoDeConta_ContaPaiCod ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV62WWTipodeContaDS_2_Tipodeconta_codigo1 = StringUtil.PadR( StringUtil.RTrim( AV62WWTipodeContaDS_2_Tipodeconta_codigo1), 20, "%");
         lV63WWTipodeContaDS_3_Tipodeconta_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWTipodeContaDS_3_Tipodeconta_nome1), 50, "%");
         lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 = StringUtil.PadR( StringUtil.RTrim( AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1), 20, "%");
         lV67WWTipodeContaDS_7_Tipodeconta_codigo2 = StringUtil.PadR( StringUtil.RTrim( AV67WWTipodeContaDS_7_Tipodeconta_codigo2), 20, "%");
         lV68WWTipodeContaDS_8_Tipodeconta_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWTipodeContaDS_8_Tipodeconta_nome2), 50, "%");
         lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 = StringUtil.PadR( StringUtil.RTrim( AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2), 20, "%");
         lV72WWTipodeContaDS_12_Tipodeconta_codigo3 = StringUtil.PadR( StringUtil.RTrim( AV72WWTipodeContaDS_12_Tipodeconta_codigo3), 20, "%");
         lV73WWTipodeContaDS_13_Tipodeconta_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWTipodeContaDS_13_Tipodeconta_nome3), 50, "%");
         lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 = StringUtil.PadR( StringUtil.RTrim( AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3), 20, "%");
         lV75WWTipodeContaDS_15_Tftipodeconta_codigo = StringUtil.PadR( StringUtil.RTrim( AV75WWTipodeContaDS_15_Tftipodeconta_codigo), 20, "%");
         lV77WWTipodeContaDS_17_Tftipodeconta_nome = StringUtil.PadR( StringUtil.RTrim( AV77WWTipodeContaDS_17_Tftipodeconta_nome), 50, "%");
         lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod = StringUtil.PadR( StringUtil.RTrim( AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod), 20, "%");
         /* Using cursor H00FH9 */
         pr_default.execute(7, new Object[] {lV62WWTipodeContaDS_2_Tipodeconta_codigo1, lV63WWTipodeContaDS_3_Tipodeconta_nome1, lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1, lV67WWTipodeContaDS_7_Tipodeconta_codigo2, lV68WWTipodeContaDS_8_Tipodeconta_nome2, lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2, lV72WWTipodeContaDS_12_Tipodeconta_codigo3, lV73WWTipodeContaDS_13_Tipodeconta_nome3, lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3, lV75WWTipodeContaDS_15_Tftipodeconta_codigo, AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel, lV77WWTipodeContaDS_17_Tftipodeconta_nome, AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel, lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod, AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel});
         GRID_nRecordCount = H00FH9_AGRID_nRecordCount[0];
         pr_default.close(7);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV61WWTipodeContaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWTipodeContaDS_2_Tipodeconta_codigo1 = AV31TipodeConta_Codigo1;
         AV63WWTipodeContaDS_3_Tipodeconta_nome1 = AV17TipodeConta_Nome1;
         AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 = AV34TipoDeConta_ContaPaiCod1;
         AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV66WWTipodeContaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV67WWTipodeContaDS_7_Tipodeconta_codigo2 = AV32TipodeConta_Codigo2;
         AV68WWTipodeContaDS_8_Tipodeconta_nome2 = AV21TipodeConta_Nome2;
         AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 = AV35TipoDeConta_ContaPaiCod2;
         AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV71WWTipodeContaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV72WWTipodeContaDS_12_Tipodeconta_codigo3 = AV33TipodeConta_Codigo3;
         AV73WWTipodeContaDS_13_Tipodeconta_nome3 = AV25TipodeConta_Nome3;
         AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 = AV36TipoDeConta_ContaPaiCod3;
         AV75WWTipodeContaDS_15_Tftipodeconta_codigo = AV40TFTipodeConta_Codigo;
         AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel = AV41TFTipodeConta_Codigo_Sel;
         AV77WWTipodeContaDS_17_Tftipodeconta_nome = AV44TFTipodeConta_Nome;
         AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel = AV45TFTipodeConta_Nome_Sel;
         AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels = AV49TFTipodeConta_Tipo_Sels;
         AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod = AV52TFTipoDeConta_ContaPaiCod;
         AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = AV53TFTipoDeConta_ContaPaiCod_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31TipodeConta_Codigo1, AV17TipodeConta_Nome1, AV34TipoDeConta_ContaPaiCod1, AV19DynamicFiltersSelector2, AV32TipodeConta_Codigo2, AV21TipodeConta_Nome2, AV35TipoDeConta_ContaPaiCod2, AV23DynamicFiltersSelector3, AV33TipodeConta_Codigo3, AV25TipodeConta_Nome3, AV36TipoDeConta_ContaPaiCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV40TFTipodeConta_Codigo, AV41TFTipodeConta_Codigo_Sel, AV44TFTipodeConta_Nome, AV45TFTipodeConta_Nome_Sel, AV52TFTipoDeConta_ContaPaiCod, AV53TFTipoDeConta_ContaPaiCod_Sel, AV42ddo_TipodeConta_CodigoTitleControlIdToReplace, AV46ddo_TipodeConta_NomeTitleControlIdToReplace, AV50ddo_TipodeConta_TipoTitleControlIdToReplace, AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace, AV49TFTipodeConta_Tipo_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A870TipodeConta_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV61WWTipodeContaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWTipodeContaDS_2_Tipodeconta_codigo1 = AV31TipodeConta_Codigo1;
         AV63WWTipodeContaDS_3_Tipodeconta_nome1 = AV17TipodeConta_Nome1;
         AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 = AV34TipoDeConta_ContaPaiCod1;
         AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV66WWTipodeContaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV67WWTipodeContaDS_7_Tipodeconta_codigo2 = AV32TipodeConta_Codigo2;
         AV68WWTipodeContaDS_8_Tipodeconta_nome2 = AV21TipodeConta_Nome2;
         AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 = AV35TipoDeConta_ContaPaiCod2;
         AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV71WWTipodeContaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV72WWTipodeContaDS_12_Tipodeconta_codigo3 = AV33TipodeConta_Codigo3;
         AV73WWTipodeContaDS_13_Tipodeconta_nome3 = AV25TipodeConta_Nome3;
         AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 = AV36TipoDeConta_ContaPaiCod3;
         AV75WWTipodeContaDS_15_Tftipodeconta_codigo = AV40TFTipodeConta_Codigo;
         AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel = AV41TFTipodeConta_Codigo_Sel;
         AV77WWTipodeContaDS_17_Tftipodeconta_nome = AV44TFTipodeConta_Nome;
         AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel = AV45TFTipodeConta_Nome_Sel;
         AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels = AV49TFTipodeConta_Tipo_Sels;
         AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod = AV52TFTipoDeConta_ContaPaiCod;
         AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = AV53TFTipoDeConta_ContaPaiCod_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31TipodeConta_Codigo1, AV17TipodeConta_Nome1, AV34TipoDeConta_ContaPaiCod1, AV19DynamicFiltersSelector2, AV32TipodeConta_Codigo2, AV21TipodeConta_Nome2, AV35TipoDeConta_ContaPaiCod2, AV23DynamicFiltersSelector3, AV33TipodeConta_Codigo3, AV25TipodeConta_Nome3, AV36TipoDeConta_ContaPaiCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV40TFTipodeConta_Codigo, AV41TFTipodeConta_Codigo_Sel, AV44TFTipodeConta_Nome, AV45TFTipodeConta_Nome_Sel, AV52TFTipoDeConta_ContaPaiCod, AV53TFTipoDeConta_ContaPaiCod_Sel, AV42ddo_TipodeConta_CodigoTitleControlIdToReplace, AV46ddo_TipodeConta_NomeTitleControlIdToReplace, AV50ddo_TipodeConta_TipoTitleControlIdToReplace, AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace, AV49TFTipodeConta_Tipo_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A870TipodeConta_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV61WWTipodeContaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWTipodeContaDS_2_Tipodeconta_codigo1 = AV31TipodeConta_Codigo1;
         AV63WWTipodeContaDS_3_Tipodeconta_nome1 = AV17TipodeConta_Nome1;
         AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 = AV34TipoDeConta_ContaPaiCod1;
         AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV66WWTipodeContaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV67WWTipodeContaDS_7_Tipodeconta_codigo2 = AV32TipodeConta_Codigo2;
         AV68WWTipodeContaDS_8_Tipodeconta_nome2 = AV21TipodeConta_Nome2;
         AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 = AV35TipoDeConta_ContaPaiCod2;
         AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV71WWTipodeContaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV72WWTipodeContaDS_12_Tipodeconta_codigo3 = AV33TipodeConta_Codigo3;
         AV73WWTipodeContaDS_13_Tipodeconta_nome3 = AV25TipodeConta_Nome3;
         AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 = AV36TipoDeConta_ContaPaiCod3;
         AV75WWTipodeContaDS_15_Tftipodeconta_codigo = AV40TFTipodeConta_Codigo;
         AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel = AV41TFTipodeConta_Codigo_Sel;
         AV77WWTipodeContaDS_17_Tftipodeconta_nome = AV44TFTipodeConta_Nome;
         AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel = AV45TFTipodeConta_Nome_Sel;
         AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels = AV49TFTipodeConta_Tipo_Sels;
         AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod = AV52TFTipoDeConta_ContaPaiCod;
         AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = AV53TFTipoDeConta_ContaPaiCod_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31TipodeConta_Codigo1, AV17TipodeConta_Nome1, AV34TipoDeConta_ContaPaiCod1, AV19DynamicFiltersSelector2, AV32TipodeConta_Codigo2, AV21TipodeConta_Nome2, AV35TipoDeConta_ContaPaiCod2, AV23DynamicFiltersSelector3, AV33TipodeConta_Codigo3, AV25TipodeConta_Nome3, AV36TipoDeConta_ContaPaiCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV40TFTipodeConta_Codigo, AV41TFTipodeConta_Codigo_Sel, AV44TFTipodeConta_Nome, AV45TFTipodeConta_Nome_Sel, AV52TFTipoDeConta_ContaPaiCod, AV53TFTipoDeConta_ContaPaiCod_Sel, AV42ddo_TipodeConta_CodigoTitleControlIdToReplace, AV46ddo_TipodeConta_NomeTitleControlIdToReplace, AV50ddo_TipodeConta_TipoTitleControlIdToReplace, AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace, AV49TFTipodeConta_Tipo_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A870TipodeConta_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV61WWTipodeContaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWTipodeContaDS_2_Tipodeconta_codigo1 = AV31TipodeConta_Codigo1;
         AV63WWTipodeContaDS_3_Tipodeconta_nome1 = AV17TipodeConta_Nome1;
         AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 = AV34TipoDeConta_ContaPaiCod1;
         AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV66WWTipodeContaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV67WWTipodeContaDS_7_Tipodeconta_codigo2 = AV32TipodeConta_Codigo2;
         AV68WWTipodeContaDS_8_Tipodeconta_nome2 = AV21TipodeConta_Nome2;
         AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 = AV35TipoDeConta_ContaPaiCod2;
         AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV71WWTipodeContaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV72WWTipodeContaDS_12_Tipodeconta_codigo3 = AV33TipodeConta_Codigo3;
         AV73WWTipodeContaDS_13_Tipodeconta_nome3 = AV25TipodeConta_Nome3;
         AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 = AV36TipoDeConta_ContaPaiCod3;
         AV75WWTipodeContaDS_15_Tftipodeconta_codigo = AV40TFTipodeConta_Codigo;
         AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel = AV41TFTipodeConta_Codigo_Sel;
         AV77WWTipodeContaDS_17_Tftipodeconta_nome = AV44TFTipodeConta_Nome;
         AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel = AV45TFTipodeConta_Nome_Sel;
         AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels = AV49TFTipodeConta_Tipo_Sels;
         AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod = AV52TFTipoDeConta_ContaPaiCod;
         AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = AV53TFTipoDeConta_ContaPaiCod_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31TipodeConta_Codigo1, AV17TipodeConta_Nome1, AV34TipoDeConta_ContaPaiCod1, AV19DynamicFiltersSelector2, AV32TipodeConta_Codigo2, AV21TipodeConta_Nome2, AV35TipoDeConta_ContaPaiCod2, AV23DynamicFiltersSelector3, AV33TipodeConta_Codigo3, AV25TipodeConta_Nome3, AV36TipoDeConta_ContaPaiCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV40TFTipodeConta_Codigo, AV41TFTipodeConta_Codigo_Sel, AV44TFTipodeConta_Nome, AV45TFTipodeConta_Nome_Sel, AV52TFTipoDeConta_ContaPaiCod, AV53TFTipoDeConta_ContaPaiCod_Sel, AV42ddo_TipodeConta_CodigoTitleControlIdToReplace, AV46ddo_TipodeConta_NomeTitleControlIdToReplace, AV50ddo_TipodeConta_TipoTitleControlIdToReplace, AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace, AV49TFTipodeConta_Tipo_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A870TipodeConta_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV61WWTipodeContaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWTipodeContaDS_2_Tipodeconta_codigo1 = AV31TipodeConta_Codigo1;
         AV63WWTipodeContaDS_3_Tipodeconta_nome1 = AV17TipodeConta_Nome1;
         AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 = AV34TipoDeConta_ContaPaiCod1;
         AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV66WWTipodeContaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV67WWTipodeContaDS_7_Tipodeconta_codigo2 = AV32TipodeConta_Codigo2;
         AV68WWTipodeContaDS_8_Tipodeconta_nome2 = AV21TipodeConta_Nome2;
         AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 = AV35TipoDeConta_ContaPaiCod2;
         AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV71WWTipodeContaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV72WWTipodeContaDS_12_Tipodeconta_codigo3 = AV33TipodeConta_Codigo3;
         AV73WWTipodeContaDS_13_Tipodeconta_nome3 = AV25TipodeConta_Nome3;
         AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 = AV36TipoDeConta_ContaPaiCod3;
         AV75WWTipodeContaDS_15_Tftipodeconta_codigo = AV40TFTipodeConta_Codigo;
         AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel = AV41TFTipodeConta_Codigo_Sel;
         AV77WWTipodeContaDS_17_Tftipodeconta_nome = AV44TFTipodeConta_Nome;
         AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel = AV45TFTipodeConta_Nome_Sel;
         AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels = AV49TFTipodeConta_Tipo_Sels;
         AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod = AV52TFTipoDeConta_ContaPaiCod;
         AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = AV53TFTipoDeConta_ContaPaiCod_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31TipodeConta_Codigo1, AV17TipodeConta_Nome1, AV34TipoDeConta_ContaPaiCod1, AV19DynamicFiltersSelector2, AV32TipodeConta_Codigo2, AV21TipodeConta_Nome2, AV35TipoDeConta_ContaPaiCod2, AV23DynamicFiltersSelector3, AV33TipodeConta_Codigo3, AV25TipodeConta_Nome3, AV36TipoDeConta_ContaPaiCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV40TFTipodeConta_Codigo, AV41TFTipodeConta_Codigo_Sel, AV44TFTipodeConta_Nome, AV45TFTipodeConta_Nome_Sel, AV52TFTipoDeConta_ContaPaiCod, AV53TFTipoDeConta_ContaPaiCod_Sel, AV42ddo_TipodeConta_CodigoTitleControlIdToReplace, AV46ddo_TipodeConta_NomeTitleControlIdToReplace, AV50ddo_TipodeConta_TipoTitleControlIdToReplace, AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace, AV49TFTipodeConta_Tipo_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A870TipodeConta_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPFH0( )
      {
         /* Before Start, stand alone formulas. */
         AV84Pgmname = "WWTipodeConta";
         context.Gx_err = 0;
         GXVvTIPODECONTA_CODIGO1_htmlFH2( ) ;
         GXVvTIPODECONTA_CONTAPAICOD1_htmlFH2( ) ;
         GXVvTIPODECONTA_CODIGO2_htmlFH2( ) ;
         GXVvTIPODECONTA_CONTAPAICOD2_htmlFH2( ) ;
         GXVvTIPODECONTA_CODIGO3_htmlFH2( ) ;
         GXVvTIPODECONTA_CONTAPAICOD3_htmlFH2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E27FH2 */
         E27FH2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV55DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPODECONTA_CODIGOTITLEFILTERDATA"), AV39TipodeConta_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPODECONTA_NOMETITLEFILTERDATA"), AV43TipodeConta_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPODECONTA_TIPOTITLEFILTERDATA"), AV47TipodeConta_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPODECONTA_CONTAPAICODTITLEFILTERDATA"), AV51TipoDeConta_ContaPaiCodTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            dynavTipodeconta_codigo1.Name = dynavTipodeconta_codigo1_Internalname;
            dynavTipodeconta_codigo1.CurrentValue = cgiGet( dynavTipodeconta_codigo1_Internalname);
            AV31TipodeConta_Codigo1 = cgiGet( dynavTipodeconta_codigo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TipodeConta_Codigo1", AV31TipodeConta_Codigo1);
            AV17TipodeConta_Nome1 = StringUtil.Upper( cgiGet( edtavTipodeconta_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipodeConta_Nome1", AV17TipodeConta_Nome1);
            dynavTipodeconta_contapaicod1.Name = dynavTipodeconta_contapaicod1_Internalname;
            dynavTipodeconta_contapaicod1.CurrentValue = cgiGet( dynavTipodeconta_contapaicod1_Internalname);
            AV34TipoDeConta_ContaPaiCod1 = cgiGet( dynavTipodeconta_contapaicod1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDeConta_ContaPaiCod1", AV34TipoDeConta_ContaPaiCod1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            dynavTipodeconta_codigo2.Name = dynavTipodeconta_codigo2_Internalname;
            dynavTipodeconta_codigo2.CurrentValue = cgiGet( dynavTipodeconta_codigo2_Internalname);
            AV32TipodeConta_Codigo2 = cgiGet( dynavTipodeconta_codigo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TipodeConta_Codigo2", AV32TipodeConta_Codigo2);
            AV21TipodeConta_Nome2 = StringUtil.Upper( cgiGet( edtavTipodeconta_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipodeConta_Nome2", AV21TipodeConta_Nome2);
            dynavTipodeconta_contapaicod2.Name = dynavTipodeconta_contapaicod2_Internalname;
            dynavTipodeconta_contapaicod2.CurrentValue = cgiGet( dynavTipodeconta_contapaicod2_Internalname);
            AV35TipoDeConta_ContaPaiCod2 = cgiGet( dynavTipodeconta_contapaicod2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TipoDeConta_ContaPaiCod2", AV35TipoDeConta_ContaPaiCod2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            dynavTipodeconta_codigo3.Name = dynavTipodeconta_codigo3_Internalname;
            dynavTipodeconta_codigo3.CurrentValue = cgiGet( dynavTipodeconta_codigo3_Internalname);
            AV33TipodeConta_Codigo3 = cgiGet( dynavTipodeconta_codigo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TipodeConta_Codigo3", AV33TipodeConta_Codigo3);
            AV25TipodeConta_Nome3 = StringUtil.Upper( cgiGet( edtavTipodeconta_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipodeConta_Nome3", AV25TipodeConta_Nome3);
            dynavTipodeconta_contapaicod3.Name = dynavTipodeconta_contapaicod3_Internalname;
            dynavTipodeconta_contapaicod3.CurrentValue = cgiGet( dynavTipodeconta_contapaicod3_Internalname);
            AV36TipoDeConta_ContaPaiCod3 = cgiGet( dynavTipodeconta_contapaicod3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TipoDeConta_ContaPaiCod3", AV36TipoDeConta_ContaPaiCod3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV40TFTipodeConta_Codigo = cgiGet( edtavTftipodeconta_codigo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipodeConta_Codigo", AV40TFTipodeConta_Codigo);
            AV41TFTipodeConta_Codigo_Sel = cgiGet( edtavTftipodeconta_codigo_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipodeConta_Codigo_Sel", AV41TFTipodeConta_Codigo_Sel);
            AV44TFTipodeConta_Nome = StringUtil.Upper( cgiGet( edtavTftipodeconta_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFTipodeConta_Nome", AV44TFTipodeConta_Nome);
            AV45TFTipodeConta_Nome_Sel = StringUtil.Upper( cgiGet( edtavTftipodeconta_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFTipodeConta_Nome_Sel", AV45TFTipodeConta_Nome_Sel);
            AV52TFTipoDeConta_ContaPaiCod = cgiGet( edtavTftipodeconta_contapaicod_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFTipoDeConta_ContaPaiCod", AV52TFTipoDeConta_ContaPaiCod);
            AV53TFTipoDeConta_ContaPaiCod_Sel = cgiGet( edtavTftipodeconta_contapaicod_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFTipoDeConta_ContaPaiCod_Sel", AV53TFTipoDeConta_ContaPaiCod_Sel);
            AV42ddo_TipodeConta_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_tipodeconta_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_TipodeConta_CodigoTitleControlIdToReplace", AV42ddo_TipodeConta_CodigoTitleControlIdToReplace);
            AV46ddo_TipodeConta_NomeTitleControlIdToReplace = cgiGet( edtavDdo_tipodeconta_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_TipodeConta_NomeTitleControlIdToReplace", AV46ddo_TipodeConta_NomeTitleControlIdToReplace);
            AV50ddo_TipodeConta_TipoTitleControlIdToReplace = cgiGet( edtavDdo_tipodeconta_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_TipodeConta_TipoTitleControlIdToReplace", AV50ddo_TipodeConta_TipoTitleControlIdToReplace);
            AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace = cgiGet( edtavDdo_tipodeconta_contapaicodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace", AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_79 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_79"), ",", "."));
            AV57GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV58GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_tipodeconta_codigo_Caption = cgiGet( "DDO_TIPODECONTA_CODIGO_Caption");
            Ddo_tipodeconta_codigo_Tooltip = cgiGet( "DDO_TIPODECONTA_CODIGO_Tooltip");
            Ddo_tipodeconta_codigo_Cls = cgiGet( "DDO_TIPODECONTA_CODIGO_Cls");
            Ddo_tipodeconta_codigo_Filteredtext_set = cgiGet( "DDO_TIPODECONTA_CODIGO_Filteredtext_set");
            Ddo_tipodeconta_codigo_Selectedvalue_set = cgiGet( "DDO_TIPODECONTA_CODIGO_Selectedvalue_set");
            Ddo_tipodeconta_codigo_Dropdownoptionstype = cgiGet( "DDO_TIPODECONTA_CODIGO_Dropdownoptionstype");
            Ddo_tipodeconta_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_TIPODECONTA_CODIGO_Titlecontrolidtoreplace");
            Ddo_tipodeconta_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_CODIGO_Includesortasc"));
            Ddo_tipodeconta_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_CODIGO_Includesortdsc"));
            Ddo_tipodeconta_codigo_Sortedstatus = cgiGet( "DDO_TIPODECONTA_CODIGO_Sortedstatus");
            Ddo_tipodeconta_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_CODIGO_Includefilter"));
            Ddo_tipodeconta_codigo_Filtertype = cgiGet( "DDO_TIPODECONTA_CODIGO_Filtertype");
            Ddo_tipodeconta_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_CODIGO_Filterisrange"));
            Ddo_tipodeconta_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_CODIGO_Includedatalist"));
            Ddo_tipodeconta_codigo_Datalisttype = cgiGet( "DDO_TIPODECONTA_CODIGO_Datalisttype");
            Ddo_tipodeconta_codigo_Datalistproc = cgiGet( "DDO_TIPODECONTA_CODIGO_Datalistproc");
            Ddo_tipodeconta_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TIPODECONTA_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tipodeconta_codigo_Sortasc = cgiGet( "DDO_TIPODECONTA_CODIGO_Sortasc");
            Ddo_tipodeconta_codigo_Sortdsc = cgiGet( "DDO_TIPODECONTA_CODIGO_Sortdsc");
            Ddo_tipodeconta_codigo_Loadingdata = cgiGet( "DDO_TIPODECONTA_CODIGO_Loadingdata");
            Ddo_tipodeconta_codigo_Cleanfilter = cgiGet( "DDO_TIPODECONTA_CODIGO_Cleanfilter");
            Ddo_tipodeconta_codigo_Noresultsfound = cgiGet( "DDO_TIPODECONTA_CODIGO_Noresultsfound");
            Ddo_tipodeconta_codigo_Searchbuttontext = cgiGet( "DDO_TIPODECONTA_CODIGO_Searchbuttontext");
            Ddo_tipodeconta_nome_Caption = cgiGet( "DDO_TIPODECONTA_NOME_Caption");
            Ddo_tipodeconta_nome_Tooltip = cgiGet( "DDO_TIPODECONTA_NOME_Tooltip");
            Ddo_tipodeconta_nome_Cls = cgiGet( "DDO_TIPODECONTA_NOME_Cls");
            Ddo_tipodeconta_nome_Filteredtext_set = cgiGet( "DDO_TIPODECONTA_NOME_Filteredtext_set");
            Ddo_tipodeconta_nome_Selectedvalue_set = cgiGet( "DDO_TIPODECONTA_NOME_Selectedvalue_set");
            Ddo_tipodeconta_nome_Dropdownoptionstype = cgiGet( "DDO_TIPODECONTA_NOME_Dropdownoptionstype");
            Ddo_tipodeconta_nome_Titlecontrolidtoreplace = cgiGet( "DDO_TIPODECONTA_NOME_Titlecontrolidtoreplace");
            Ddo_tipodeconta_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_NOME_Includesortasc"));
            Ddo_tipodeconta_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_NOME_Includesortdsc"));
            Ddo_tipodeconta_nome_Sortedstatus = cgiGet( "DDO_TIPODECONTA_NOME_Sortedstatus");
            Ddo_tipodeconta_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_NOME_Includefilter"));
            Ddo_tipodeconta_nome_Filtertype = cgiGet( "DDO_TIPODECONTA_NOME_Filtertype");
            Ddo_tipodeconta_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_NOME_Filterisrange"));
            Ddo_tipodeconta_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_NOME_Includedatalist"));
            Ddo_tipodeconta_nome_Datalisttype = cgiGet( "DDO_TIPODECONTA_NOME_Datalisttype");
            Ddo_tipodeconta_nome_Datalistproc = cgiGet( "DDO_TIPODECONTA_NOME_Datalistproc");
            Ddo_tipodeconta_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TIPODECONTA_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tipodeconta_nome_Sortasc = cgiGet( "DDO_TIPODECONTA_NOME_Sortasc");
            Ddo_tipodeconta_nome_Sortdsc = cgiGet( "DDO_TIPODECONTA_NOME_Sortdsc");
            Ddo_tipodeconta_nome_Loadingdata = cgiGet( "DDO_TIPODECONTA_NOME_Loadingdata");
            Ddo_tipodeconta_nome_Cleanfilter = cgiGet( "DDO_TIPODECONTA_NOME_Cleanfilter");
            Ddo_tipodeconta_nome_Noresultsfound = cgiGet( "DDO_TIPODECONTA_NOME_Noresultsfound");
            Ddo_tipodeconta_nome_Searchbuttontext = cgiGet( "DDO_TIPODECONTA_NOME_Searchbuttontext");
            Ddo_tipodeconta_tipo_Caption = cgiGet( "DDO_TIPODECONTA_TIPO_Caption");
            Ddo_tipodeconta_tipo_Tooltip = cgiGet( "DDO_TIPODECONTA_TIPO_Tooltip");
            Ddo_tipodeconta_tipo_Cls = cgiGet( "DDO_TIPODECONTA_TIPO_Cls");
            Ddo_tipodeconta_tipo_Selectedvalue_set = cgiGet( "DDO_TIPODECONTA_TIPO_Selectedvalue_set");
            Ddo_tipodeconta_tipo_Dropdownoptionstype = cgiGet( "DDO_TIPODECONTA_TIPO_Dropdownoptionstype");
            Ddo_tipodeconta_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_TIPODECONTA_TIPO_Titlecontrolidtoreplace");
            Ddo_tipodeconta_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_TIPO_Includesortasc"));
            Ddo_tipodeconta_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_TIPO_Includesortdsc"));
            Ddo_tipodeconta_tipo_Sortedstatus = cgiGet( "DDO_TIPODECONTA_TIPO_Sortedstatus");
            Ddo_tipodeconta_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_TIPO_Includefilter"));
            Ddo_tipodeconta_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_TIPO_Includedatalist"));
            Ddo_tipodeconta_tipo_Datalisttype = cgiGet( "DDO_TIPODECONTA_TIPO_Datalisttype");
            Ddo_tipodeconta_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_TIPO_Allowmultipleselection"));
            Ddo_tipodeconta_tipo_Datalistfixedvalues = cgiGet( "DDO_TIPODECONTA_TIPO_Datalistfixedvalues");
            Ddo_tipodeconta_tipo_Sortasc = cgiGet( "DDO_TIPODECONTA_TIPO_Sortasc");
            Ddo_tipodeconta_tipo_Sortdsc = cgiGet( "DDO_TIPODECONTA_TIPO_Sortdsc");
            Ddo_tipodeconta_tipo_Cleanfilter = cgiGet( "DDO_TIPODECONTA_TIPO_Cleanfilter");
            Ddo_tipodeconta_tipo_Searchbuttontext = cgiGet( "DDO_TIPODECONTA_TIPO_Searchbuttontext");
            Ddo_tipodeconta_contapaicod_Caption = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Caption");
            Ddo_tipodeconta_contapaicod_Tooltip = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Tooltip");
            Ddo_tipodeconta_contapaicod_Cls = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Cls");
            Ddo_tipodeconta_contapaicod_Filteredtext_set = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Filteredtext_set");
            Ddo_tipodeconta_contapaicod_Selectedvalue_set = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Selectedvalue_set");
            Ddo_tipodeconta_contapaicod_Dropdownoptionstype = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Dropdownoptionstype");
            Ddo_tipodeconta_contapaicod_Titlecontrolidtoreplace = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Titlecontrolidtoreplace");
            Ddo_tipodeconta_contapaicod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Includesortasc"));
            Ddo_tipodeconta_contapaicod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Includesortdsc"));
            Ddo_tipodeconta_contapaicod_Sortedstatus = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Sortedstatus");
            Ddo_tipodeconta_contapaicod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Includefilter"));
            Ddo_tipodeconta_contapaicod_Filtertype = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Filtertype");
            Ddo_tipodeconta_contapaicod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Filterisrange"));
            Ddo_tipodeconta_contapaicod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Includedatalist"));
            Ddo_tipodeconta_contapaicod_Datalisttype = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Datalisttype");
            Ddo_tipodeconta_contapaicod_Datalistproc = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Datalistproc");
            Ddo_tipodeconta_contapaicod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tipodeconta_contapaicod_Sortasc = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Sortasc");
            Ddo_tipodeconta_contapaicod_Sortdsc = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Sortdsc");
            Ddo_tipodeconta_contapaicod_Loadingdata = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Loadingdata");
            Ddo_tipodeconta_contapaicod_Cleanfilter = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Cleanfilter");
            Ddo_tipodeconta_contapaicod_Noresultsfound = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Noresultsfound");
            Ddo_tipodeconta_contapaicod_Searchbuttontext = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_tipodeconta_codigo_Activeeventkey = cgiGet( "DDO_TIPODECONTA_CODIGO_Activeeventkey");
            Ddo_tipodeconta_codigo_Filteredtext_get = cgiGet( "DDO_TIPODECONTA_CODIGO_Filteredtext_get");
            Ddo_tipodeconta_codigo_Selectedvalue_get = cgiGet( "DDO_TIPODECONTA_CODIGO_Selectedvalue_get");
            Ddo_tipodeconta_nome_Activeeventkey = cgiGet( "DDO_TIPODECONTA_NOME_Activeeventkey");
            Ddo_tipodeconta_nome_Filteredtext_get = cgiGet( "DDO_TIPODECONTA_NOME_Filteredtext_get");
            Ddo_tipodeconta_nome_Selectedvalue_get = cgiGet( "DDO_TIPODECONTA_NOME_Selectedvalue_get");
            Ddo_tipodeconta_tipo_Activeeventkey = cgiGet( "DDO_TIPODECONTA_TIPO_Activeeventkey");
            Ddo_tipodeconta_tipo_Selectedvalue_get = cgiGet( "DDO_TIPODECONTA_TIPO_Selectedvalue_get");
            Ddo_tipodeconta_contapaicod_Activeeventkey = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Activeeventkey");
            Ddo_tipodeconta_contapaicod_Filteredtext_get = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Filteredtext_get");
            Ddo_tipodeconta_contapaicod_Selectedvalue_get = cgiGet( "DDO_TIPODECONTA_CONTAPAICOD_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CODIGO1"), AV31TipodeConta_Codigo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_NOME1"), AV17TipodeConta_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CONTAPAICOD1"), AV34TipoDeConta_ContaPaiCod1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CODIGO2"), AV32TipodeConta_Codigo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_NOME2"), AV21TipodeConta_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CONTAPAICOD2"), AV35TipoDeConta_ContaPaiCod2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CODIGO3"), AV33TipodeConta_Codigo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_NOME3"), AV25TipodeConta_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODECONTA_CONTAPAICOD3"), AV36TipoDeConta_ContaPaiCod3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_CODIGO"), AV40TFTipodeConta_Codigo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_CODIGO_SEL"), AV41TFTipodeConta_Codigo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_NOME"), AV44TFTipodeConta_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_NOME_SEL"), AV45TFTipodeConta_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_CONTAPAICOD"), AV52TFTipoDeConta_ContaPaiCod) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODECONTA_CONTAPAICOD_SEL"), AV53TFTipoDeConta_ContaPaiCod_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E27FH2 */
         E27FH2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27FH2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "TIPODECONTA_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "TIPODECONTA_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "TIPODECONTA_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTftipodeconta_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodeconta_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodeconta_codigo_Visible), 5, 0)));
         edtavTftipodeconta_codigo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodeconta_codigo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodeconta_codigo_sel_Visible), 5, 0)));
         edtavTftipodeconta_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodeconta_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodeconta_nome_Visible), 5, 0)));
         edtavTftipodeconta_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodeconta_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodeconta_nome_sel_Visible), 5, 0)));
         edtavTftipodeconta_contapaicod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodeconta_contapaicod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodeconta_contapaicod_Visible), 5, 0)));
         edtavTftipodeconta_contapaicod_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodeconta_contapaicod_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodeconta_contapaicod_sel_Visible), 5, 0)));
         Ddo_tipodeconta_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_TipodeConta_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_codigo_Internalname, "TitleControlIdToReplace", Ddo_tipodeconta_codigo_Titlecontrolidtoreplace);
         AV42ddo_TipodeConta_CodigoTitleControlIdToReplace = Ddo_tipodeconta_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_TipodeConta_CodigoTitleControlIdToReplace", AV42ddo_TipodeConta_CodigoTitleControlIdToReplace);
         edtavDdo_tipodeconta_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tipodeconta_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tipodeconta_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tipodeconta_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_TipodeConta_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_nome_Internalname, "TitleControlIdToReplace", Ddo_tipodeconta_nome_Titlecontrolidtoreplace);
         AV46ddo_TipodeConta_NomeTitleControlIdToReplace = Ddo_tipodeconta_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_TipodeConta_NomeTitleControlIdToReplace", AV46ddo_TipodeConta_NomeTitleControlIdToReplace);
         edtavDdo_tipodeconta_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tipodeconta_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tipodeconta_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tipodeconta_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_TipodeConta_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_tipo_Internalname, "TitleControlIdToReplace", Ddo_tipodeconta_tipo_Titlecontrolidtoreplace);
         AV50ddo_TipodeConta_TipoTitleControlIdToReplace = Ddo_tipodeconta_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_TipodeConta_TipoTitleControlIdToReplace", AV50ddo_TipodeConta_TipoTitleControlIdToReplace);
         edtavDdo_tipodeconta_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tipodeconta_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tipodeconta_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tipodeconta_contapaicod_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoDeConta_ContaPaiCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_contapaicod_Internalname, "TitleControlIdToReplace", Ddo_tipodeconta_contapaicod_Titlecontrolidtoreplace);
         AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace = Ddo_tipodeconta_contapaicod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace", AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace);
         edtavDdo_tipodeconta_contapaicodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tipodeconta_contapaicodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tipodeconta_contapaicodtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Tipo de Contas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Conta", 0);
         cmbavOrderedby.addItem("3", "Tipo", 0);
         cmbavOrderedby.addItem("4", "Conta Pai", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV55DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV55DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E28FH2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV39TipodeConta_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43TipodeConta_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47TipodeConta_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51TipoDeConta_ContaPaiCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtTipodeConta_Codigo_Titleformat = 2;
         edtTipodeConta_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Conta", AV42ddo_TipodeConta_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipodeConta_Codigo_Internalname, "Title", edtTipodeConta_Codigo_Title);
         edtTipodeConta_Nome_Titleformat = 2;
         edtTipodeConta_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV46ddo_TipodeConta_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipodeConta_Nome_Internalname, "Title", edtTipodeConta_Nome_Title);
         cmbTipodeConta_Tipo_Titleformat = 2;
         cmbTipodeConta_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV50ddo_TipodeConta_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipodeConta_Tipo_Internalname, "Title", cmbTipodeConta_Tipo.Title.Text);
         edtTipoDeConta_ContaPaiCod_Titleformat = 2;
         edtTipoDeConta_ContaPaiCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Conta Pai", AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDeConta_ContaPaiCod_Internalname, "Title", edtTipoDeConta_ContaPaiCod_Title);
         AV57GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57GridCurrentPage), 10, 0)));
         AV58GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58GridPageCount), 10, 0)));
         AV61WWTipodeContaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWTipodeContaDS_2_Tipodeconta_codigo1 = AV31TipodeConta_Codigo1;
         AV63WWTipodeContaDS_3_Tipodeconta_nome1 = AV17TipodeConta_Nome1;
         AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 = AV34TipoDeConta_ContaPaiCod1;
         AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV66WWTipodeContaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV67WWTipodeContaDS_7_Tipodeconta_codigo2 = AV32TipodeConta_Codigo2;
         AV68WWTipodeContaDS_8_Tipodeconta_nome2 = AV21TipodeConta_Nome2;
         AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 = AV35TipoDeConta_ContaPaiCod2;
         AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV71WWTipodeContaDS_11_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV72WWTipodeContaDS_12_Tipodeconta_codigo3 = AV33TipodeConta_Codigo3;
         AV73WWTipodeContaDS_13_Tipodeconta_nome3 = AV25TipodeConta_Nome3;
         AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 = AV36TipoDeConta_ContaPaiCod3;
         AV75WWTipodeContaDS_15_Tftipodeconta_codigo = AV40TFTipodeConta_Codigo;
         AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel = AV41TFTipodeConta_Codigo_Sel;
         AV77WWTipodeContaDS_17_Tftipodeconta_nome = AV44TFTipodeConta_Nome;
         AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel = AV45TFTipodeConta_Nome_Sel;
         AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels = AV49TFTipodeConta_Tipo_Sels;
         AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod = AV52TFTipoDeConta_ContaPaiCod;
         AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = AV53TFTipoDeConta_ContaPaiCod_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV39TipodeConta_CodigoTitleFilterData", AV39TipodeConta_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43TipodeConta_NomeTitleFilterData", AV43TipodeConta_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47TipodeConta_TipoTitleFilterData", AV47TipodeConta_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51TipoDeConta_ContaPaiCodTitleFilterData", AV51TipoDeConta_ContaPaiCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11FH2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV56PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV56PageToGo) ;
         }
      }

      protected void E12FH2( )
      {
         /* Ddo_tipodeconta_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tipodeconta_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodeconta_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_codigo_Internalname, "SortedStatus", Ddo_tipodeconta_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodeconta_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodeconta_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_codigo_Internalname, "SortedStatus", Ddo_tipodeconta_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodeconta_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFTipodeConta_Codigo = Ddo_tipodeconta_codigo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipodeConta_Codigo", AV40TFTipodeConta_Codigo);
            AV41TFTipodeConta_Codigo_Sel = Ddo_tipodeconta_codigo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipodeConta_Codigo_Sel", AV41TFTipodeConta_Codigo_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13FH2( )
      {
         /* Ddo_tipodeconta_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tipodeconta_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodeconta_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_nome_Internalname, "SortedStatus", Ddo_tipodeconta_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodeconta_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodeconta_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_nome_Internalname, "SortedStatus", Ddo_tipodeconta_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodeconta_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFTipodeConta_Nome = Ddo_tipodeconta_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFTipodeConta_Nome", AV44TFTipodeConta_Nome);
            AV45TFTipodeConta_Nome_Sel = Ddo_tipodeconta_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFTipodeConta_Nome_Sel", AV45TFTipodeConta_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14FH2( )
      {
         /* Ddo_tipodeconta_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tipodeconta_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodeconta_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_tipo_Internalname, "SortedStatus", Ddo_tipodeconta_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodeconta_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodeconta_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_tipo_Internalname, "SortedStatus", Ddo_tipodeconta_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodeconta_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFTipodeConta_Tipo_SelsJson = Ddo_tipodeconta_tipo_Selectedvalue_get;
            AV49TFTipodeConta_Tipo_Sels.FromJSonString(AV48TFTipodeConta_Tipo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49TFTipodeConta_Tipo_Sels", AV49TFTipodeConta_Tipo_Sels);
      }

      protected void E15FH2( )
      {
         /* Ddo_tipodeconta_contapaicod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tipodeconta_contapaicod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodeconta_contapaicod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_contapaicod_Internalname, "SortedStatus", Ddo_tipodeconta_contapaicod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodeconta_contapaicod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodeconta_contapaicod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_contapaicod_Internalname, "SortedStatus", Ddo_tipodeconta_contapaicod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodeconta_contapaicod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFTipoDeConta_ContaPaiCod = Ddo_tipodeconta_contapaicod_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFTipoDeConta_ContaPaiCod", AV52TFTipoDeConta_ContaPaiCod);
            AV53TFTipoDeConta_ContaPaiCod_Sel = Ddo_tipodeconta_contapaicod_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFTipoDeConta_ContaPaiCod_Sel", AV53TFTipoDeConta_ContaPaiCod_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29FH2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV82Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("tipodeconta.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode(StringUtil.RTrim(A870TipodeConta_Codigo));
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV83Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("tipodeconta.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode(StringUtil.RTrim(A870TipodeConta_Codigo));
         edtTipodeConta_Nome_Link = formatLink("viewtipodeconta.aspx") + "?" + UrlEncode(StringUtil.RTrim(A870TipodeConta_Codigo)) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 79;
         }
         sendrow_792( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_79_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(79, GridRow);
         }
      }

      protected void E16FH2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22FH2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E17FH2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31TipodeConta_Codigo1, AV17TipodeConta_Nome1, AV34TipoDeConta_ContaPaiCod1, AV19DynamicFiltersSelector2, AV32TipodeConta_Codigo2, AV21TipodeConta_Nome2, AV35TipoDeConta_ContaPaiCod2, AV23DynamicFiltersSelector3, AV33TipodeConta_Codigo3, AV25TipodeConta_Nome3, AV36TipoDeConta_ContaPaiCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV40TFTipodeConta_Codigo, AV41TFTipodeConta_Codigo_Sel, AV44TFTipodeConta_Nome, AV45TFTipodeConta_Nome_Sel, AV52TFTipoDeConta_ContaPaiCod, AV53TFTipoDeConta_ContaPaiCod_Sel, AV42ddo_TipodeConta_CodigoTitleControlIdToReplace, AV46ddo_TipodeConta_NomeTitleControlIdToReplace, AV50ddo_TipodeConta_TipoTitleControlIdToReplace, AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace, AV49TFTipodeConta_Tipo_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A870TipodeConta_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavTipodeconta_codigo2.CurrentValue = StringUtil.RTrim( AV32TipodeConta_Codigo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo2_Internalname, "Values", dynavTipodeconta_codigo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavTipodeconta_codigo3.CurrentValue = StringUtil.RTrim( AV33TipodeConta_Codigo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo3_Internalname, "Values", dynavTipodeconta_codigo3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavTipodeconta_codigo1.CurrentValue = StringUtil.RTrim( AV31TipodeConta_Codigo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo1_Internalname, "Values", dynavTipodeconta_codigo1.ToJavascriptSource());
         dynavTipodeconta_contapaicod1.CurrentValue = StringUtil.RTrim( AV34TipoDeConta_ContaPaiCod1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod1_Internalname, "Values", dynavTipodeconta_contapaicod1.ToJavascriptSource());
         dynavTipodeconta_contapaicod2.CurrentValue = StringUtil.RTrim( AV35TipoDeConta_ContaPaiCod2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod2_Internalname, "Values", dynavTipodeconta_contapaicod2.ToJavascriptSource());
         dynavTipodeconta_contapaicod3.CurrentValue = StringUtil.RTrim( AV36TipoDeConta_ContaPaiCod3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod3_Internalname, "Values", dynavTipodeconta_contapaicod3.ToJavascriptSource());
      }

      protected void E23FH2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24FH2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E18FH2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31TipodeConta_Codigo1, AV17TipodeConta_Nome1, AV34TipoDeConta_ContaPaiCod1, AV19DynamicFiltersSelector2, AV32TipodeConta_Codigo2, AV21TipodeConta_Nome2, AV35TipoDeConta_ContaPaiCod2, AV23DynamicFiltersSelector3, AV33TipodeConta_Codigo3, AV25TipodeConta_Nome3, AV36TipoDeConta_ContaPaiCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV40TFTipodeConta_Codigo, AV41TFTipodeConta_Codigo_Sel, AV44TFTipodeConta_Nome, AV45TFTipodeConta_Nome_Sel, AV52TFTipoDeConta_ContaPaiCod, AV53TFTipoDeConta_ContaPaiCod_Sel, AV42ddo_TipodeConta_CodigoTitleControlIdToReplace, AV46ddo_TipodeConta_NomeTitleControlIdToReplace, AV50ddo_TipodeConta_TipoTitleControlIdToReplace, AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace, AV49TFTipodeConta_Tipo_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A870TipodeConta_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavTipodeconta_codigo2.CurrentValue = StringUtil.RTrim( AV32TipodeConta_Codigo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo2_Internalname, "Values", dynavTipodeconta_codigo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavTipodeconta_codigo3.CurrentValue = StringUtil.RTrim( AV33TipodeConta_Codigo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo3_Internalname, "Values", dynavTipodeconta_codigo3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavTipodeconta_codigo1.CurrentValue = StringUtil.RTrim( AV31TipodeConta_Codigo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo1_Internalname, "Values", dynavTipodeconta_codigo1.ToJavascriptSource());
         dynavTipodeconta_contapaicod1.CurrentValue = StringUtil.RTrim( AV34TipoDeConta_ContaPaiCod1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod1_Internalname, "Values", dynavTipodeconta_contapaicod1.ToJavascriptSource());
         dynavTipodeconta_contapaicod2.CurrentValue = StringUtil.RTrim( AV35TipoDeConta_ContaPaiCod2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod2_Internalname, "Values", dynavTipodeconta_contapaicod2.ToJavascriptSource());
         dynavTipodeconta_contapaicod3.CurrentValue = StringUtil.RTrim( AV36TipoDeConta_ContaPaiCod3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod3_Internalname, "Values", dynavTipodeconta_contapaicod3.ToJavascriptSource());
      }

      protected void E25FH2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19FH2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31TipodeConta_Codigo1, AV17TipodeConta_Nome1, AV34TipoDeConta_ContaPaiCod1, AV19DynamicFiltersSelector2, AV32TipodeConta_Codigo2, AV21TipodeConta_Nome2, AV35TipoDeConta_ContaPaiCod2, AV23DynamicFiltersSelector3, AV33TipodeConta_Codigo3, AV25TipodeConta_Nome3, AV36TipoDeConta_ContaPaiCod3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV40TFTipodeConta_Codigo, AV41TFTipodeConta_Codigo_Sel, AV44TFTipodeConta_Nome, AV45TFTipodeConta_Nome_Sel, AV52TFTipoDeConta_ContaPaiCod, AV53TFTipoDeConta_ContaPaiCod_Sel, AV42ddo_TipodeConta_CodigoTitleControlIdToReplace, AV46ddo_TipodeConta_NomeTitleControlIdToReplace, AV50ddo_TipodeConta_TipoTitleControlIdToReplace, AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace, AV49TFTipodeConta_Tipo_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A870TipodeConta_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavTipodeconta_codigo2.CurrentValue = StringUtil.RTrim( AV32TipodeConta_Codigo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo2_Internalname, "Values", dynavTipodeconta_codigo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavTipodeconta_codigo3.CurrentValue = StringUtil.RTrim( AV33TipodeConta_Codigo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo3_Internalname, "Values", dynavTipodeconta_codigo3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavTipodeconta_codigo1.CurrentValue = StringUtil.RTrim( AV31TipodeConta_Codigo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo1_Internalname, "Values", dynavTipodeconta_codigo1.ToJavascriptSource());
         dynavTipodeconta_contapaicod1.CurrentValue = StringUtil.RTrim( AV34TipoDeConta_ContaPaiCod1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod1_Internalname, "Values", dynavTipodeconta_contapaicod1.ToJavascriptSource());
         dynavTipodeconta_contapaicod2.CurrentValue = StringUtil.RTrim( AV35TipoDeConta_ContaPaiCod2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod2_Internalname, "Values", dynavTipodeconta_contapaicod2.ToJavascriptSource());
         dynavTipodeconta_contapaicod3.CurrentValue = StringUtil.RTrim( AV36TipoDeConta_ContaPaiCod3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod3_Internalname, "Values", dynavTipodeconta_contapaicod3.ToJavascriptSource());
      }

      protected void E26FH2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20FH2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49TFTipodeConta_Tipo_Sels", AV49TFTipodeConta_Tipo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavTipodeconta_codigo1.CurrentValue = StringUtil.RTrim( AV31TipodeConta_Codigo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo1_Internalname, "Values", dynavTipodeconta_codigo1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavTipodeconta_codigo2.CurrentValue = StringUtil.RTrim( AV32TipodeConta_Codigo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo2_Internalname, "Values", dynavTipodeconta_codigo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavTipodeconta_codigo3.CurrentValue = StringUtil.RTrim( AV33TipodeConta_Codigo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo3_Internalname, "Values", dynavTipodeconta_codigo3.ToJavascriptSource());
         dynavTipodeconta_contapaicod1.CurrentValue = StringUtil.RTrim( AV34TipoDeConta_ContaPaiCod1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod1_Internalname, "Values", dynavTipodeconta_contapaicod1.ToJavascriptSource());
         dynavTipodeconta_contapaicod2.CurrentValue = StringUtil.RTrim( AV35TipoDeConta_ContaPaiCod2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod2_Internalname, "Values", dynavTipodeconta_contapaicod2.ToJavascriptSource());
         dynavTipodeconta_contapaicod3.CurrentValue = StringUtil.RTrim( AV36TipoDeConta_ContaPaiCod3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod3_Internalname, "Values", dynavTipodeconta_contapaicod3.ToJavascriptSource());
      }

      protected void E21FH2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("tipodeconta.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_tipodeconta_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_codigo_Internalname, "SortedStatus", Ddo_tipodeconta_codigo_Sortedstatus);
         Ddo_tipodeconta_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_nome_Internalname, "SortedStatus", Ddo_tipodeconta_nome_Sortedstatus);
         Ddo_tipodeconta_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_tipo_Internalname, "SortedStatus", Ddo_tipodeconta_tipo_Sortedstatus);
         Ddo_tipodeconta_contapaicod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_contapaicod_Internalname, "SortedStatus", Ddo_tipodeconta_contapaicod_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_tipodeconta_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_codigo_Internalname, "SortedStatus", Ddo_tipodeconta_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_tipodeconta_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_nome_Internalname, "SortedStatus", Ddo_tipodeconta_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_tipodeconta_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_tipo_Internalname, "SortedStatus", Ddo_tipodeconta_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_tipodeconta_contapaicod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_contapaicod_Internalname, "SortedStatus", Ddo_tipodeconta_contapaicod_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         dynavTipodeconta_codigo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_codigo1.Visible), 5, 0)));
         edtavTipodeconta_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodeconta_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodeconta_nome1_Visible), 5, 0)));
         dynavTipodeconta_contapaicod1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_contapaicod1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODECONTA_CODIGO") == 0 )
         {
            dynavTipodeconta_codigo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_codigo1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODECONTA_NOME") == 0 )
         {
            edtavTipodeconta_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodeconta_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodeconta_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODECONTA_CONTAPAICOD") == 0 )
         {
            dynavTipodeconta_contapaicod1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_contapaicod1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         dynavTipodeconta_codigo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_codigo2.Visible), 5, 0)));
         edtavTipodeconta_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodeconta_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodeconta_nome2_Visible), 5, 0)));
         dynavTipodeconta_contapaicod2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_contapaicod2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODECONTA_CODIGO") == 0 )
         {
            dynavTipodeconta_codigo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_codigo2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODECONTA_NOME") == 0 )
         {
            edtavTipodeconta_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodeconta_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodeconta_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODECONTA_CONTAPAICOD") == 0 )
         {
            dynavTipodeconta_contapaicod2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_contapaicod2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         dynavTipodeconta_codigo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_codigo3.Visible), 5, 0)));
         edtavTipodeconta_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodeconta_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodeconta_nome3_Visible), 5, 0)));
         dynavTipodeconta_contapaicod3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_contapaicod3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODECONTA_CODIGO") == 0 )
         {
            dynavTipodeconta_codigo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_codigo3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODECONTA_NOME") == 0 )
         {
            edtavTipodeconta_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodeconta_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodeconta_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODECONTA_CONTAPAICOD") == 0 )
         {
            dynavTipodeconta_contapaicod3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavTipodeconta_contapaicod3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "TIPODECONTA_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV32TipodeConta_Codigo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TipodeConta_Codigo2", AV32TipodeConta_Codigo2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "TIPODECONTA_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV33TipodeConta_Codigo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TipodeConta_Codigo3", AV33TipodeConta_Codigo3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV40TFTipodeConta_Codigo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipodeConta_Codigo", AV40TFTipodeConta_Codigo);
         Ddo_tipodeconta_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_codigo_Internalname, "FilteredText_set", Ddo_tipodeconta_codigo_Filteredtext_set);
         AV41TFTipodeConta_Codigo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipodeConta_Codigo_Sel", AV41TFTipodeConta_Codigo_Sel);
         Ddo_tipodeconta_codigo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_codigo_Internalname, "SelectedValue_set", Ddo_tipodeconta_codigo_Selectedvalue_set);
         AV44TFTipodeConta_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFTipodeConta_Nome", AV44TFTipodeConta_Nome);
         Ddo_tipodeconta_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_nome_Internalname, "FilteredText_set", Ddo_tipodeconta_nome_Filteredtext_set);
         AV45TFTipodeConta_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFTipodeConta_Nome_Sel", AV45TFTipodeConta_Nome_Sel);
         Ddo_tipodeconta_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_nome_Internalname, "SelectedValue_set", Ddo_tipodeconta_nome_Selectedvalue_set);
         AV49TFTipodeConta_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_tipodeconta_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_tipo_Internalname, "SelectedValue_set", Ddo_tipodeconta_tipo_Selectedvalue_set);
         AV52TFTipoDeConta_ContaPaiCod = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFTipoDeConta_ContaPaiCod", AV52TFTipoDeConta_ContaPaiCod);
         Ddo_tipodeconta_contapaicod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_contapaicod_Internalname, "FilteredText_set", Ddo_tipodeconta_contapaicod_Filteredtext_set);
         AV53TFTipoDeConta_ContaPaiCod_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFTipoDeConta_ContaPaiCod_Sel", AV53TFTipoDeConta_ContaPaiCod_Sel);
         Ddo_tipodeconta_contapaicod_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_contapaicod_Internalname, "SelectedValue_set", Ddo_tipodeconta_contapaicod_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "TIPODECONTA_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV31TipodeConta_Codigo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TipodeConta_Codigo1", AV31TipodeConta_Codigo1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV84Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV84Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV84Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV85GXV1 = 1;
         while ( AV85GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV85GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_CODIGO") == 0 )
            {
               AV40TFTipodeConta_Codigo = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipodeConta_Codigo", AV40TFTipodeConta_Codigo);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFTipodeConta_Codigo)) )
               {
                  Ddo_tipodeconta_codigo_Filteredtext_set = AV40TFTipodeConta_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_codigo_Internalname, "FilteredText_set", Ddo_tipodeconta_codigo_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_CODIGO_SEL") == 0 )
            {
               AV41TFTipodeConta_Codigo_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipodeConta_Codigo_Sel", AV41TFTipodeConta_Codigo_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFTipodeConta_Codigo_Sel)) )
               {
                  Ddo_tipodeconta_codigo_Selectedvalue_set = AV41TFTipodeConta_Codigo_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_codigo_Internalname, "SelectedValue_set", Ddo_tipodeconta_codigo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_NOME") == 0 )
            {
               AV44TFTipodeConta_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFTipodeConta_Nome", AV44TFTipodeConta_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFTipodeConta_Nome)) )
               {
                  Ddo_tipodeconta_nome_Filteredtext_set = AV44TFTipodeConta_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_nome_Internalname, "FilteredText_set", Ddo_tipodeconta_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_NOME_SEL") == 0 )
            {
               AV45TFTipodeConta_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFTipodeConta_Nome_Sel", AV45TFTipodeConta_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFTipodeConta_Nome_Sel)) )
               {
                  Ddo_tipodeconta_nome_Selectedvalue_set = AV45TFTipodeConta_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_nome_Internalname, "SelectedValue_set", Ddo_tipodeconta_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_TIPO_SEL") == 0 )
            {
               AV48TFTipodeConta_Tipo_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV49TFTipodeConta_Tipo_Sels.FromJSonString(AV48TFTipodeConta_Tipo_SelsJson);
               if ( ! ( AV49TFTipodeConta_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_tipodeconta_tipo_Selectedvalue_set = AV48TFTipodeConta_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_tipo_Internalname, "SelectedValue_set", Ddo_tipodeconta_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_CONTAPAICOD") == 0 )
            {
               AV52TFTipoDeConta_ContaPaiCod = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFTipoDeConta_ContaPaiCod", AV52TFTipoDeConta_ContaPaiCod);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFTipoDeConta_ContaPaiCod)) )
               {
                  Ddo_tipodeconta_contapaicod_Filteredtext_set = AV52TFTipoDeConta_ContaPaiCod;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_contapaicod_Internalname, "FilteredText_set", Ddo_tipodeconta_contapaicod_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_CONTAPAICOD_SEL") == 0 )
            {
               AV53TFTipoDeConta_ContaPaiCod_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFTipoDeConta_ContaPaiCod_Sel", AV53TFTipoDeConta_ContaPaiCod_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFTipoDeConta_ContaPaiCod_Sel)) )
               {
                  Ddo_tipodeconta_contapaicod_Selectedvalue_set = AV53TFTipoDeConta_ContaPaiCod_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodeconta_contapaicod_Internalname, "SelectedValue_set", Ddo_tipodeconta_contapaicod_Selectedvalue_set);
               }
            }
            AV85GXV1 = (int)(AV85GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODECONTA_CODIGO") == 0 )
            {
               AV31TipodeConta_Codigo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TipodeConta_Codigo1", AV31TipodeConta_Codigo1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODECONTA_NOME") == 0 )
            {
               AV17TipodeConta_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipodeConta_Nome1", AV17TipodeConta_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODECONTA_CONTAPAICOD") == 0 )
            {
               AV34TipoDeConta_ContaPaiCod1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDeConta_ContaPaiCod1", AV34TipoDeConta_ContaPaiCod1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODECONTA_CODIGO") == 0 )
               {
                  AV32TipodeConta_Codigo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TipodeConta_Codigo2", AV32TipodeConta_Codigo2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODECONTA_NOME") == 0 )
               {
                  AV21TipodeConta_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipodeConta_Nome2", AV21TipodeConta_Nome2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODECONTA_CONTAPAICOD") == 0 )
               {
                  AV35TipoDeConta_ContaPaiCod2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TipoDeConta_ContaPaiCod2", AV35TipoDeConta_ContaPaiCod2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODECONTA_CODIGO") == 0 )
                  {
                     AV33TipodeConta_Codigo3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TipodeConta_Codigo3", AV33TipodeConta_Codigo3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODECONTA_NOME") == 0 )
                  {
                     AV25TipodeConta_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipodeConta_Nome3", AV25TipodeConta_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODECONTA_CONTAPAICOD") == 0 )
                  {
                     AV36TipoDeConta_ContaPaiCod3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TipoDeConta_ContaPaiCod3", AV36TipoDeConta_ContaPaiCod3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV84Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFTipodeConta_Codigo)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODECONTA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFTipodeConta_Codigo;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFTipodeConta_Codigo_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODECONTA_CODIGO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFTipodeConta_Codigo_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFTipodeConta_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODECONTA_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFTipodeConta_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFTipodeConta_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODECONTA_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFTipodeConta_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV49TFTipodeConta_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODECONTA_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV49TFTipodeConta_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFTipoDeConta_ContaPaiCod)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODECONTA_CONTAPAICOD";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFTipoDeConta_ContaPaiCod;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFTipoDeConta_ContaPaiCod_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODECONTA_CONTAPAICOD_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV53TFTipoDeConta_ContaPaiCod_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV84Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODECONTA_CODIGO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TipodeConta_Codigo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31TipodeConta_Codigo1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODECONTA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TipodeConta_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17TipodeConta_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODECONTA_CONTAPAICOD") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TipoDeConta_ContaPaiCod1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV34TipoDeConta_ContaPaiCod1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODECONTA_CODIGO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TipodeConta_Codigo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV32TipodeConta_Codigo2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODECONTA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TipodeConta_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21TipodeConta_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODECONTA_CONTAPAICOD") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TipoDeConta_ContaPaiCod2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV35TipoDeConta_ContaPaiCod2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODECONTA_CODIGO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TipodeConta_Codigo3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV33TipodeConta_Codigo3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODECONTA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TipodeConta_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25TipodeConta_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODECONTA_CONTAPAICOD") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TipoDeConta_ContaPaiCod3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV36TipoDeConta_ContaPaiCod3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV84Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "TipodeConta";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_FH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_FH2( true) ;
         }
         else
         {
            wb_table2_8_FH2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_FH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_73_FH2( true) ;
         }
         else
         {
            wb_table3_73_FH2( false) ;
         }
         return  ;
      }

      protected void wb_table3_73_FH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_FH2e( true) ;
         }
         else
         {
            wb_table1_2_FH2e( false) ;
         }
      }

      protected void wb_table3_73_FH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_76_FH2( true) ;
         }
         else
         {
            wb_table4_76_FH2( false) ;
         }
         return  ;
      }

      protected void wb_table4_76_FH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_73_FH2e( true) ;
         }
         else
         {
            wb_table3_73_FH2e( false) ;
         }
      }

      protected void wb_table4_76_FH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"79\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipodeConta_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipodeConta_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipodeConta_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipodeConta_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipodeConta_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipodeConta_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbTipodeConta_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbTipodeConta_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbTipodeConta_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipoDeConta_ContaPaiCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipoDeConta_ContaPaiCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipoDeConta_ContaPaiCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A870TipodeConta_Codigo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipodeConta_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipodeConta_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A871TipodeConta_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipodeConta_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipodeConta_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTipodeConta_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A873TipodeConta_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbTipodeConta_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTipodeConta_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A885TipoDeConta_ContaPaiCod));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipoDeConta_ContaPaiCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoDeConta_ContaPaiCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 79 )
         {
            wbEnd = 0;
            nRC_GXsfl_79 = (short)(nGXsfl_79_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_76_FH2e( true) ;
         }
         else
         {
            wb_table4_76_FH2e( false) ;
         }
      }

      protected void wb_table2_8_FH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTipodecontatitle_Internalname, "Plano de Contas", "", "", lblTipodecontatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_FH2( true) ;
         }
         else
         {
            wb_table5_13_FH2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_FH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWTipodeConta.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_FH2( true) ;
         }
         else
         {
            wb_table6_23_FH2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_FH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_FH2e( true) ;
         }
         else
         {
            wb_table2_8_FH2e( false) ;
         }
      }

      protected void wb_table6_23_FH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_FH2( true) ;
         }
         else
         {
            wb_table7_28_FH2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_FH2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_FH2e( true) ;
         }
         else
         {
            wb_table6_23_FH2e( false) ;
         }
      }

      protected void wb_table7_28_FH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWTipodeConta.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipodeconta_codigo1, dynavTipodeconta_codigo1_Internalname, StringUtil.RTrim( AV31TipodeConta_Codigo1), 1, dynavTipodeconta_codigo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", dynavTipodeconta_codigo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WWTipodeConta.htm");
            dynavTipodeconta_codigo1.CurrentValue = StringUtil.RTrim( AV31TipodeConta_Codigo1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo1_Internalname, "Values", (String)(dynavTipodeconta_codigo1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodeconta_nome1_Internalname, StringUtil.RTrim( AV17TipodeConta_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17TipodeConta_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodeconta_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodeconta_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipodeConta.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipodeconta_contapaicod1, dynavTipodeconta_contapaicod1_Internalname, StringUtil.RTrim( AV34TipoDeConta_ContaPaiCod1), 1, dynavTipodeconta_contapaicod1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", dynavTipodeconta_contapaicod1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_WWTipodeConta.htm");
            dynavTipodeconta_contapaicod1.CurrentValue = StringUtil.RTrim( AV34TipoDeConta_ContaPaiCod1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod1_Internalname, "Values", (String)(dynavTipodeconta_contapaicod1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipodeConta.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_WWTipodeConta.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipodeconta_codigo2, dynavTipodeconta_codigo2_Internalname, StringUtil.RTrim( AV32TipodeConta_Codigo2), 1, dynavTipodeconta_codigo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", dynavTipodeconta_codigo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_WWTipodeConta.htm");
            dynavTipodeconta_codigo2.CurrentValue = StringUtil.RTrim( AV32TipodeConta_Codigo2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo2_Internalname, "Values", (String)(dynavTipodeconta_codigo2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodeconta_nome2_Internalname, StringUtil.RTrim( AV21TipodeConta_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21TipodeConta_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodeconta_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodeconta_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipodeConta.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipodeconta_contapaicod2, dynavTipodeconta_contapaicod2_Internalname, StringUtil.RTrim( AV35TipoDeConta_ContaPaiCod2), 1, dynavTipodeconta_contapaicod2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", dynavTipodeconta_contapaicod2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_WWTipodeConta.htm");
            dynavTipodeconta_contapaicod2.CurrentValue = StringUtil.RTrim( AV35TipoDeConta_ContaPaiCod2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod2_Internalname, "Values", (String)(dynavTipodeconta_contapaicod2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipodeConta.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_WWTipodeConta.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipodeconta_codigo3, dynavTipodeconta_codigo3_Internalname, StringUtil.RTrim( AV33TipodeConta_Codigo3), 1, dynavTipodeconta_codigo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", dynavTipodeconta_codigo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_WWTipodeConta.htm");
            dynavTipodeconta_codigo3.CurrentValue = StringUtil.RTrim( AV33TipodeConta_Codigo3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_codigo3_Internalname, "Values", (String)(dynavTipodeconta_codigo3.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodeconta_nome3_Internalname, StringUtil.RTrim( AV25TipodeConta_Nome3), StringUtil.RTrim( context.localUtil.Format( AV25TipodeConta_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodeconta_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodeconta_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipodeConta.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipodeconta_contapaicod3, dynavTipodeconta_contapaicod3_Internalname, StringUtil.RTrim( AV36TipoDeConta_ContaPaiCod3), 1, dynavTipodeconta_contapaicod3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", dynavTipodeconta_contapaicod3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWTipodeConta.htm");
            dynavTipodeconta_contapaicod3.CurrentValue = StringUtil.RTrim( AV36TipoDeConta_ContaPaiCod3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodeconta_contapaicod3_Internalname, "Values", (String)(dynavTipodeconta_contapaicod3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_FH2e( true) ;
         }
         else
         {
            wb_table7_28_FH2e( false) ;
         }
      }

      protected void wb_table5_13_FH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipodeConta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_FH2e( true) ;
         }
         else
         {
            wb_table5_13_FH2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAFH2( ) ;
         WSFH2( ) ;
         WEFH2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051813255326");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwtipodeconta.js", "?202051813255326");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_792( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_79_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_79_idx;
         edtTipodeConta_Codigo_Internalname = "TIPODECONTA_CODIGO_"+sGXsfl_79_idx;
         edtTipodeConta_Nome_Internalname = "TIPODECONTA_NOME_"+sGXsfl_79_idx;
         cmbTipodeConta_Tipo_Internalname = "TIPODECONTA_TIPO_"+sGXsfl_79_idx;
         edtTipoDeConta_ContaPaiCod_Internalname = "TIPODECONTA_CONTAPAICOD_"+sGXsfl_79_idx;
      }

      protected void SubsflControlProps_fel_792( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_79_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_79_fel_idx;
         edtTipodeConta_Codigo_Internalname = "TIPODECONTA_CODIGO_"+sGXsfl_79_fel_idx;
         edtTipodeConta_Nome_Internalname = "TIPODECONTA_NOME_"+sGXsfl_79_fel_idx;
         cmbTipodeConta_Tipo_Internalname = "TIPODECONTA_TIPO_"+sGXsfl_79_fel_idx;
         edtTipoDeConta_ContaPaiCod_Internalname = "TIPODECONTA_CONTAPAICOD_"+sGXsfl_79_fel_idx;
      }

      protected void sendrow_792( )
      {
         SubsflControlProps_792( ) ;
         WBFH0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_79_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_79_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_79_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV82Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV82Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV83Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV83Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipodeConta_Codigo_Internalname,StringUtil.RTrim( A870TipodeConta_Codigo),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipodeConta_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipodeConta_Nome_Internalname,StringUtil.RTrim( A871TipodeConta_Nome),StringUtil.RTrim( context.localUtil.Format( A871TipodeConta_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtTipodeConta_Nome_Link,(String)"",(String)"",(String)"",(String)edtTipodeConta_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_79_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "TIPODECONTA_TIPO_" + sGXsfl_79_idx;
               cmbTipodeConta_Tipo.Name = GXCCtl;
               cmbTipodeConta_Tipo.WebTags = "";
               cmbTipodeConta_Tipo.addItem("", "(Nenhum)", 0);
               cmbTipodeConta_Tipo.addItem("D", "D�bito", 0);
               cmbTipodeConta_Tipo.addItem("C", "Cr�dito", 0);
               if ( cmbTipodeConta_Tipo.ItemCount > 0 )
               {
                  A873TipodeConta_Tipo = cmbTipodeConta_Tipo.getValidValue(A873TipodeConta_Tipo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbTipodeConta_Tipo,(String)cmbTipodeConta_Tipo_Internalname,StringUtil.RTrim( A873TipodeConta_Tipo),(short)1,(String)cmbTipodeConta_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbTipodeConta_Tipo.CurrentValue = StringUtil.RTrim( A873TipodeConta_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipodeConta_Tipo_Internalname, "Values", (String)(cmbTipodeConta_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoDeConta_ContaPaiCod_Internalname,StringUtil.RTrim( A885TipoDeConta_ContaPaiCod),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoDeConta_ContaPaiCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_TIPODECONTA_CODIGO"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A870TipodeConta_Codigo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPODECONTA_NOME"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A871TipodeConta_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPODECONTA_TIPO"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A873TipodeConta_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPODECONTA_CONTAPAICOD"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A885TipoDeConta_ContaPaiCod, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_79_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_79_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_79_idx+1));
            sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
            SubsflControlProps_792( ) ;
         }
         /* End function sendrow_792 */
      }

      protected void init_default_properties( )
      {
         lblTipodecontatitle_Internalname = "TIPODECONTATITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         dynavTipodeconta_codigo1_Internalname = "vTIPODECONTA_CODIGO1";
         edtavTipodeconta_nome1_Internalname = "vTIPODECONTA_NOME1";
         dynavTipodeconta_contapaicod1_Internalname = "vTIPODECONTA_CONTAPAICOD1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         dynavTipodeconta_codigo2_Internalname = "vTIPODECONTA_CODIGO2";
         edtavTipodeconta_nome2_Internalname = "vTIPODECONTA_NOME2";
         dynavTipodeconta_contapaicod2_Internalname = "vTIPODECONTA_CONTAPAICOD2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         dynavTipodeconta_codigo3_Internalname = "vTIPODECONTA_CODIGO3";
         edtavTipodeconta_nome3_Internalname = "vTIPODECONTA_NOME3";
         dynavTipodeconta_contapaicod3_Internalname = "vTIPODECONTA_CONTAPAICOD3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtTipodeConta_Codigo_Internalname = "TIPODECONTA_CODIGO";
         edtTipodeConta_Nome_Internalname = "TIPODECONTA_NOME";
         cmbTipodeConta_Tipo_Internalname = "TIPODECONTA_TIPO";
         edtTipoDeConta_ContaPaiCod_Internalname = "TIPODECONTA_CONTAPAICOD";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTftipodeconta_codigo_Internalname = "vTFTIPODECONTA_CODIGO";
         edtavTftipodeconta_codigo_sel_Internalname = "vTFTIPODECONTA_CODIGO_SEL";
         edtavTftipodeconta_nome_Internalname = "vTFTIPODECONTA_NOME";
         edtavTftipodeconta_nome_sel_Internalname = "vTFTIPODECONTA_NOME_SEL";
         edtavTftipodeconta_contapaicod_Internalname = "vTFTIPODECONTA_CONTAPAICOD";
         edtavTftipodeconta_contapaicod_sel_Internalname = "vTFTIPODECONTA_CONTAPAICOD_SEL";
         Ddo_tipodeconta_codigo_Internalname = "DDO_TIPODECONTA_CODIGO";
         edtavDdo_tipodeconta_codigotitlecontrolidtoreplace_Internalname = "vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_tipodeconta_nome_Internalname = "DDO_TIPODECONTA_NOME";
         edtavDdo_tipodeconta_nometitlecontrolidtoreplace_Internalname = "vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE";
         Ddo_tipodeconta_tipo_Internalname = "DDO_TIPODECONTA_TIPO";
         edtavDdo_tipodeconta_tipotitlecontrolidtoreplace_Internalname = "vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_tipodeconta_contapaicod_Internalname = "DDO_TIPODECONTA_CONTAPAICOD";
         edtavDdo_tipodeconta_contapaicodtitlecontrolidtoreplace_Internalname = "vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtTipoDeConta_ContaPaiCod_Jsonclick = "";
         cmbTipodeConta_Tipo_Jsonclick = "";
         edtTipodeConta_Nome_Jsonclick = "";
         edtTipodeConta_Codigo_Jsonclick = "";
         dynavTipodeconta_contapaicod3_Jsonclick = "";
         edtavTipodeconta_nome3_Jsonclick = "";
         dynavTipodeconta_codigo3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         dynavTipodeconta_contapaicod2_Jsonclick = "";
         edtavTipodeconta_nome2_Jsonclick = "";
         dynavTipodeconta_codigo2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         dynavTipodeconta_contapaicod1_Jsonclick = "";
         edtavTipodeconta_nome1_Jsonclick = "";
         dynavTipodeconta_codigo1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtTipodeConta_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtTipoDeConta_ContaPaiCod_Titleformat = 0;
         cmbTipodeConta_Tipo_Titleformat = 0;
         edtTipodeConta_Nome_Titleformat = 0;
         edtTipodeConta_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         dynavTipodeconta_contapaicod3.Visible = 1;
         edtavTipodeconta_nome3_Visible = 1;
         dynavTipodeconta_codigo3.Visible = 1;
         dynavTipodeconta_contapaicod2.Visible = 1;
         edtavTipodeconta_nome2_Visible = 1;
         dynavTipodeconta_codigo2.Visible = 1;
         dynavTipodeconta_contapaicod1.Visible = 1;
         edtavTipodeconta_nome1_Visible = 1;
         dynavTipodeconta_codigo1.Visible = 1;
         edtTipoDeConta_ContaPaiCod_Title = "Conta Pai";
         cmbTipodeConta_Tipo.Title.Text = "Tipo";
         edtTipodeConta_Nome_Title = "Nome";
         edtTipodeConta_Codigo_Title = "Conta";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_tipodeconta_contapaicodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tipodeconta_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tipodeconta_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tipodeconta_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTftipodeconta_contapaicod_sel_Jsonclick = "";
         edtavTftipodeconta_contapaicod_sel_Visible = 1;
         edtavTftipodeconta_contapaicod_Jsonclick = "";
         edtavTftipodeconta_contapaicod_Visible = 1;
         edtavTftipodeconta_nome_sel_Jsonclick = "";
         edtavTftipodeconta_nome_sel_Visible = 1;
         edtavTftipodeconta_nome_Jsonclick = "";
         edtavTftipodeconta_nome_Visible = 1;
         edtavTftipodeconta_codigo_sel_Jsonclick = "";
         edtavTftipodeconta_codigo_sel_Visible = 1;
         edtavTftipodeconta_codigo_Jsonclick = "";
         edtavTftipodeconta_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_tipodeconta_contapaicod_Searchbuttontext = "Pesquisar";
         Ddo_tipodeconta_contapaicod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tipodeconta_contapaicod_Cleanfilter = "Limpar pesquisa";
         Ddo_tipodeconta_contapaicod_Loadingdata = "Carregando dados...";
         Ddo_tipodeconta_contapaicod_Sortdsc = "Ordenar de Z � A";
         Ddo_tipodeconta_contapaicod_Sortasc = "Ordenar de A � Z";
         Ddo_tipodeconta_contapaicod_Datalistupdateminimumcharacters = 0;
         Ddo_tipodeconta_contapaicod_Datalistproc = "GetWWTipodeContaFilterData";
         Ddo_tipodeconta_contapaicod_Datalisttype = "Dynamic";
         Ddo_tipodeconta_contapaicod_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tipodeconta_contapaicod_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tipodeconta_contapaicod_Filtertype = "Character";
         Ddo_tipodeconta_contapaicod_Includefilter = Convert.ToBoolean( -1);
         Ddo_tipodeconta_contapaicod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tipodeconta_contapaicod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tipodeconta_contapaicod_Titlecontrolidtoreplace = "";
         Ddo_tipodeconta_contapaicod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tipodeconta_contapaicod_Cls = "ColumnSettings";
         Ddo_tipodeconta_contapaicod_Tooltip = "Op��es";
         Ddo_tipodeconta_contapaicod_Caption = "";
         Ddo_tipodeconta_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_tipodeconta_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_tipodeconta_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_tipodeconta_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_tipodeconta_tipo_Datalistfixedvalues = "D:D�bito,C:Cr�dito";
         Ddo_tipodeconta_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_tipodeconta_tipo_Datalisttype = "FixedValues";
         Ddo_tipodeconta_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tipodeconta_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_tipodeconta_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tipodeconta_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tipodeconta_tipo_Titlecontrolidtoreplace = "";
         Ddo_tipodeconta_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tipodeconta_tipo_Cls = "ColumnSettings";
         Ddo_tipodeconta_tipo_Tooltip = "Op��es";
         Ddo_tipodeconta_tipo_Caption = "";
         Ddo_tipodeconta_nome_Searchbuttontext = "Pesquisar";
         Ddo_tipodeconta_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tipodeconta_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_tipodeconta_nome_Loadingdata = "Carregando dados...";
         Ddo_tipodeconta_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_tipodeconta_nome_Sortasc = "Ordenar de A � Z";
         Ddo_tipodeconta_nome_Datalistupdateminimumcharacters = 0;
         Ddo_tipodeconta_nome_Datalistproc = "GetWWTipodeContaFilterData";
         Ddo_tipodeconta_nome_Datalisttype = "Dynamic";
         Ddo_tipodeconta_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tipodeconta_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tipodeconta_nome_Filtertype = "Character";
         Ddo_tipodeconta_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_tipodeconta_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tipodeconta_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tipodeconta_nome_Titlecontrolidtoreplace = "";
         Ddo_tipodeconta_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tipodeconta_nome_Cls = "ColumnSettings";
         Ddo_tipodeconta_nome_Tooltip = "Op��es";
         Ddo_tipodeconta_nome_Caption = "";
         Ddo_tipodeconta_codigo_Searchbuttontext = "Pesquisar";
         Ddo_tipodeconta_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tipodeconta_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_tipodeconta_codigo_Loadingdata = "Carregando dados...";
         Ddo_tipodeconta_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_tipodeconta_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_tipodeconta_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_tipodeconta_codigo_Datalistproc = "GetWWTipodeContaFilterData";
         Ddo_tipodeconta_codigo_Datalisttype = "Dynamic";
         Ddo_tipodeconta_codigo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tipodeconta_codigo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tipodeconta_codigo_Filtertype = "Character";
         Ddo_tipodeconta_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_tipodeconta_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tipodeconta_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tipodeconta_codigo_Titlecontrolidtoreplace = "";
         Ddo_tipodeconta_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tipodeconta_codigo_Cls = "ColumnSettings";
         Ddo_tipodeconta_codigo_Tooltip = "Op��es";
         Ddo_tipodeconta_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Tipo de Contas";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''},{av:'AV42ddo_TipodeConta_CodigoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipodeConta_NomeTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipodeConta_TipoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV39TipodeConta_CodigoTitleFilterData',fld:'vTIPODECONTA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV43TipodeConta_NomeTitleFilterData',fld:'vTIPODECONTA_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV47TipodeConta_TipoTitleFilterData',fld:'vTIPODECONTA_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV51TipoDeConta_ContaPaiCodTitleFilterData',fld:'vTIPODECONTA_CONTAPAICODTITLEFILTERDATA',pic:'',nv:null},{av:'edtTipodeConta_Codigo_Titleformat',ctrl:'TIPODECONTA_CODIGO',prop:'Titleformat'},{av:'edtTipodeConta_Codigo_Title',ctrl:'TIPODECONTA_CODIGO',prop:'Title'},{av:'edtTipodeConta_Nome_Titleformat',ctrl:'TIPODECONTA_NOME',prop:'Titleformat'},{av:'edtTipodeConta_Nome_Title',ctrl:'TIPODECONTA_NOME',prop:'Title'},{av:'cmbTipodeConta_Tipo'},{av:'edtTipoDeConta_ContaPaiCod_Titleformat',ctrl:'TIPODECONTA_CONTAPAICOD',prop:'Titleformat'},{av:'edtTipoDeConta_ContaPaiCod_Title',ctrl:'TIPODECONTA_CONTAPAICOD',prop:'Title'},{av:'AV57GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV58GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11FH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'AV42ddo_TipodeConta_CodigoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipodeConta_NomeTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipodeConta_TipoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_TIPODECONTA_CODIGO.ONOPTIONCLICKED","{handler:'E12FH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'AV42ddo_TipodeConta_CodigoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipodeConta_NomeTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipodeConta_TipoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''},{av:'Ddo_tipodeconta_codigo_Activeeventkey',ctrl:'DDO_TIPODECONTA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_tipodeconta_codigo_Filteredtext_get',ctrl:'DDO_TIPODECONTA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_tipodeconta_codigo_Selectedvalue_get',ctrl:'DDO_TIPODECONTA_CODIGO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tipodeconta_codigo_Sortedstatus',ctrl:'DDO_TIPODECONTA_CODIGO',prop:'SortedStatus'},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'Ddo_tipodeconta_nome_Sortedstatus',ctrl:'DDO_TIPODECONTA_NOME',prop:'SortedStatus'},{av:'Ddo_tipodeconta_tipo_Sortedstatus',ctrl:'DDO_TIPODECONTA_TIPO',prop:'SortedStatus'},{av:'Ddo_tipodeconta_contapaicod_Sortedstatus',ctrl:'DDO_TIPODECONTA_CONTAPAICOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TIPODECONTA_NOME.ONOPTIONCLICKED","{handler:'E13FH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'AV42ddo_TipodeConta_CodigoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipodeConta_NomeTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipodeConta_TipoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''},{av:'Ddo_tipodeconta_nome_Activeeventkey',ctrl:'DDO_TIPODECONTA_NOME',prop:'ActiveEventKey'},{av:'Ddo_tipodeconta_nome_Filteredtext_get',ctrl:'DDO_TIPODECONTA_NOME',prop:'FilteredText_get'},{av:'Ddo_tipodeconta_nome_Selectedvalue_get',ctrl:'DDO_TIPODECONTA_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tipodeconta_nome_Sortedstatus',ctrl:'DDO_TIPODECONTA_NOME',prop:'SortedStatus'},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tipodeconta_codigo_Sortedstatus',ctrl:'DDO_TIPODECONTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_tipodeconta_tipo_Sortedstatus',ctrl:'DDO_TIPODECONTA_TIPO',prop:'SortedStatus'},{av:'Ddo_tipodeconta_contapaicod_Sortedstatus',ctrl:'DDO_TIPODECONTA_CONTAPAICOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TIPODECONTA_TIPO.ONOPTIONCLICKED","{handler:'E14FH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'AV42ddo_TipodeConta_CodigoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipodeConta_NomeTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipodeConta_TipoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''},{av:'Ddo_tipodeconta_tipo_Activeeventkey',ctrl:'DDO_TIPODECONTA_TIPO',prop:'ActiveEventKey'},{av:'Ddo_tipodeconta_tipo_Selectedvalue_get',ctrl:'DDO_TIPODECONTA_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tipodeconta_tipo_Sortedstatus',ctrl:'DDO_TIPODECONTA_TIPO',prop:'SortedStatus'},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'Ddo_tipodeconta_codigo_Sortedstatus',ctrl:'DDO_TIPODECONTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_tipodeconta_nome_Sortedstatus',ctrl:'DDO_TIPODECONTA_NOME',prop:'SortedStatus'},{av:'Ddo_tipodeconta_contapaicod_Sortedstatus',ctrl:'DDO_TIPODECONTA_CONTAPAICOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TIPODECONTA_CONTAPAICOD.ONOPTIONCLICKED","{handler:'E15FH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'AV42ddo_TipodeConta_CodigoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipodeConta_NomeTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipodeConta_TipoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''},{av:'Ddo_tipodeconta_contapaicod_Activeeventkey',ctrl:'DDO_TIPODECONTA_CONTAPAICOD',prop:'ActiveEventKey'},{av:'Ddo_tipodeconta_contapaicod_Filteredtext_get',ctrl:'DDO_TIPODECONTA_CONTAPAICOD',prop:'FilteredText_get'},{av:'Ddo_tipodeconta_contapaicod_Selectedvalue_get',ctrl:'DDO_TIPODECONTA_CONTAPAICOD',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tipodeconta_contapaicod_Sortedstatus',ctrl:'DDO_TIPODECONTA_CONTAPAICOD',prop:'SortedStatus'},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'Ddo_tipodeconta_codigo_Sortedstatus',ctrl:'DDO_TIPODECONTA_CODIGO',prop:'SortedStatus'},{av:'Ddo_tipodeconta_nome_Sortedstatus',ctrl:'DDO_TIPODECONTA_NOME',prop:'SortedStatus'},{av:'Ddo_tipodeconta_tipo_Sortedstatus',ctrl:'DDO_TIPODECONTA_TIPO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29FH2',iparms:[{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtTipodeConta_Nome_Link',ctrl:'TIPODECONTA_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16FH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'AV42ddo_TipodeConta_CodigoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipodeConta_NomeTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipodeConta_TipoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22FH2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E17FH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'AV42ddo_TipodeConta_CodigoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipodeConta_NomeTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipodeConta_TipoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'dynavTipodeconta_codigo2'},{av:'edtavTipodeconta_nome2_Visible',ctrl:'vTIPODECONTA_NOME2',prop:'Visible'},{av:'dynavTipodeconta_contapaicod2'},{av:'dynavTipodeconta_codigo3'},{av:'edtavTipodeconta_nome3_Visible',ctrl:'vTIPODECONTA_NOME3',prop:'Visible'},{av:'dynavTipodeconta_contapaicod3'},{av:'dynavTipodeconta_codigo1'},{av:'edtavTipodeconta_nome1_Visible',ctrl:'vTIPODECONTA_NOME1',prop:'Visible'},{av:'dynavTipodeconta_contapaicod1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23FH2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'dynavTipodeconta_codigo1'},{av:'edtavTipodeconta_nome1_Visible',ctrl:'vTIPODECONTA_NOME1',prop:'Visible'},{av:'dynavTipodeconta_contapaicod1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24FH2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E18FH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'AV42ddo_TipodeConta_CodigoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipodeConta_NomeTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipodeConta_TipoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'dynavTipodeconta_codigo2'},{av:'edtavTipodeconta_nome2_Visible',ctrl:'vTIPODECONTA_NOME2',prop:'Visible'},{av:'dynavTipodeconta_contapaicod2'},{av:'dynavTipodeconta_codigo3'},{av:'edtavTipodeconta_nome3_Visible',ctrl:'vTIPODECONTA_NOME3',prop:'Visible'},{av:'dynavTipodeconta_contapaicod3'},{av:'dynavTipodeconta_codigo1'},{av:'edtavTipodeconta_nome1_Visible',ctrl:'vTIPODECONTA_NOME1',prop:'Visible'},{av:'dynavTipodeconta_contapaicod1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25FH2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'dynavTipodeconta_codigo2'},{av:'edtavTipodeconta_nome2_Visible',ctrl:'vTIPODECONTA_NOME2',prop:'Visible'},{av:'dynavTipodeconta_contapaicod2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E19FH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'AV42ddo_TipodeConta_CodigoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipodeConta_NomeTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipodeConta_TipoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'dynavTipodeconta_codigo2'},{av:'edtavTipodeconta_nome2_Visible',ctrl:'vTIPODECONTA_NOME2',prop:'Visible'},{av:'dynavTipodeconta_contapaicod2'},{av:'dynavTipodeconta_codigo3'},{av:'edtavTipodeconta_nome3_Visible',ctrl:'vTIPODECONTA_NOME3',prop:'Visible'},{av:'dynavTipodeconta_contapaicod3'},{av:'dynavTipodeconta_codigo1'},{av:'edtavTipodeconta_nome1_Visible',ctrl:'vTIPODECONTA_NOME1',prop:'Visible'},{av:'dynavTipodeconta_contapaicod1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26FH2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'dynavTipodeconta_codigo3'},{av:'edtavTipodeconta_nome3_Visible',ctrl:'vTIPODECONTA_NOME3',prop:'Visible'},{av:'dynavTipodeconta_contapaicod3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E20FH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'AV42ddo_TipodeConta_CodigoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipodeConta_NomeTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipodeConta_TipoTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace',fld:'vDDO_TIPODECONTA_CONTAPAICODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''}],oparms:[{av:'AV40TFTipodeConta_Codigo',fld:'vTFTIPODECONTA_CODIGO',pic:'',nv:''},{av:'Ddo_tipodeconta_codigo_Filteredtext_set',ctrl:'DDO_TIPODECONTA_CODIGO',prop:'FilteredText_set'},{av:'AV41TFTipodeConta_Codigo_Sel',fld:'vTFTIPODECONTA_CODIGO_SEL',pic:'',nv:''},{av:'Ddo_tipodeconta_codigo_Selectedvalue_set',ctrl:'DDO_TIPODECONTA_CODIGO',prop:'SelectedValue_set'},{av:'AV44TFTipodeConta_Nome',fld:'vTFTIPODECONTA_NOME',pic:'@!',nv:''},{av:'Ddo_tipodeconta_nome_Filteredtext_set',ctrl:'DDO_TIPODECONTA_NOME',prop:'FilteredText_set'},{av:'AV45TFTipodeConta_Nome_Sel',fld:'vTFTIPODECONTA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tipodeconta_nome_Selectedvalue_set',ctrl:'DDO_TIPODECONTA_NOME',prop:'SelectedValue_set'},{av:'AV49TFTipodeConta_Tipo_Sels',fld:'vTFTIPODECONTA_TIPO_SELS',pic:'',nv:null},{av:'Ddo_tipodeconta_tipo_Selectedvalue_set',ctrl:'DDO_TIPODECONTA_TIPO',prop:'SelectedValue_set'},{av:'AV52TFTipoDeConta_ContaPaiCod',fld:'vTFTIPODECONTA_CONTAPAICOD',pic:'',nv:''},{av:'Ddo_tipodeconta_contapaicod_Filteredtext_set',ctrl:'DDO_TIPODECONTA_CONTAPAICOD',prop:'FilteredText_set'},{av:'AV53TFTipoDeConta_ContaPaiCod_Sel',fld:'vTFTIPODECONTA_CONTAPAICOD_SEL',pic:'',nv:''},{av:'Ddo_tipodeconta_contapaicod_Selectedvalue_set',ctrl:'DDO_TIPODECONTA_CONTAPAICOD',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31TipodeConta_Codigo1',fld:'vTIPODECONTA_CODIGO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'dynavTipodeconta_codigo1'},{av:'edtavTipodeconta_nome1_Visible',ctrl:'vTIPODECONTA_NOME1',prop:'Visible'},{av:'dynavTipodeconta_contapaicod1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32TipodeConta_Codigo2',fld:'vTIPODECONTA_CODIGO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV33TipodeConta_Codigo3',fld:'vTIPODECONTA_CODIGO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17TipodeConta_Nome1',fld:'vTIPODECONTA_NOME1',pic:'@!',nv:''},{av:'AV34TipoDeConta_ContaPaiCod1',fld:'vTIPODECONTA_CONTAPAICOD1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21TipodeConta_Nome2',fld:'vTIPODECONTA_NOME2',pic:'@!',nv:''},{av:'AV35TipoDeConta_ContaPaiCod2',fld:'vTIPODECONTA_CONTAPAICOD2',pic:'',nv:''},{av:'AV25TipodeConta_Nome3',fld:'vTIPODECONTA_NOME3',pic:'@!',nv:''},{av:'AV36TipoDeConta_ContaPaiCod3',fld:'vTIPODECONTA_CONTAPAICOD3',pic:'',nv:''},{av:'dynavTipodeconta_codigo2'},{av:'edtavTipodeconta_nome2_Visible',ctrl:'vTIPODECONTA_NOME2',prop:'Visible'},{av:'dynavTipodeconta_contapaicod2'},{av:'dynavTipodeconta_codigo3'},{av:'edtavTipodeconta_nome3_Visible',ctrl:'vTIPODECONTA_NOME3',prop:'Visible'},{av:'dynavTipodeconta_contapaicod3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E21FH2',iparms:[{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',hsh:true,nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_tipodeconta_codigo_Activeeventkey = "";
         Ddo_tipodeconta_codigo_Filteredtext_get = "";
         Ddo_tipodeconta_codigo_Selectedvalue_get = "";
         Ddo_tipodeconta_nome_Activeeventkey = "";
         Ddo_tipodeconta_nome_Filteredtext_get = "";
         Ddo_tipodeconta_nome_Selectedvalue_get = "";
         Ddo_tipodeconta_tipo_Activeeventkey = "";
         Ddo_tipodeconta_tipo_Selectedvalue_get = "";
         Ddo_tipodeconta_contapaicod_Activeeventkey = "";
         Ddo_tipodeconta_contapaicod_Filteredtext_get = "";
         Ddo_tipodeconta_contapaicod_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV31TipodeConta_Codigo1 = "";
         AV17TipodeConta_Nome1 = "";
         AV34TipoDeConta_ContaPaiCod1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV32TipodeConta_Codigo2 = "";
         AV21TipodeConta_Nome2 = "";
         AV35TipoDeConta_ContaPaiCod2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV33TipodeConta_Codigo3 = "";
         AV25TipodeConta_Nome3 = "";
         AV36TipoDeConta_ContaPaiCod3 = "";
         AV40TFTipodeConta_Codigo = "";
         AV41TFTipodeConta_Codigo_Sel = "";
         AV44TFTipodeConta_Nome = "";
         AV45TFTipodeConta_Nome_Sel = "";
         AV52TFTipoDeConta_ContaPaiCod = "";
         AV53TFTipoDeConta_ContaPaiCod_Sel = "";
         AV42ddo_TipodeConta_CodigoTitleControlIdToReplace = "";
         AV46ddo_TipodeConta_NomeTitleControlIdToReplace = "";
         AV50ddo_TipodeConta_TipoTitleControlIdToReplace = "";
         AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace = "";
         AV49TFTipodeConta_Tipo_Sels = new GxSimpleCollection();
         AV84Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A870TipodeConta_Codigo = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV55DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV39TipodeConta_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43TipodeConta_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47TipodeConta_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51TipoDeConta_ContaPaiCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_tipodeconta_codigo_Filteredtext_set = "";
         Ddo_tipodeconta_codigo_Selectedvalue_set = "";
         Ddo_tipodeconta_codigo_Sortedstatus = "";
         Ddo_tipodeconta_nome_Filteredtext_set = "";
         Ddo_tipodeconta_nome_Selectedvalue_set = "";
         Ddo_tipodeconta_nome_Sortedstatus = "";
         Ddo_tipodeconta_tipo_Selectedvalue_set = "";
         Ddo_tipodeconta_tipo_Sortedstatus = "";
         Ddo_tipodeconta_contapaicod_Filteredtext_set = "";
         Ddo_tipodeconta_contapaicod_Selectedvalue_set = "";
         Ddo_tipodeconta_contapaicod_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV82Update_GXI = "";
         AV29Delete = "";
         AV83Delete_GXI = "";
         A871TipodeConta_Nome = "";
         A873TipodeConta_Tipo = "";
         A885TipoDeConta_ContaPaiCod = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00FH2_A870TipodeConta_Codigo = new String[] {""} ;
         H00FH2_A871TipodeConta_Nome = new String[] {""} ;
         H00FH3_A870TipodeConta_Codigo = new String[] {""} ;
         H00FH3_A871TipodeConta_Nome = new String[] {""} ;
         H00FH4_A870TipodeConta_Codigo = new String[] {""} ;
         H00FH4_A871TipodeConta_Nome = new String[] {""} ;
         H00FH5_A870TipodeConta_Codigo = new String[] {""} ;
         H00FH5_A871TipodeConta_Nome = new String[] {""} ;
         H00FH6_A870TipodeConta_Codigo = new String[] {""} ;
         H00FH6_A871TipodeConta_Nome = new String[] {""} ;
         H00FH7_A870TipodeConta_Codigo = new String[] {""} ;
         H00FH7_A871TipodeConta_Nome = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels = new GxSimpleCollection();
         lV62WWTipodeContaDS_2_Tipodeconta_codigo1 = "";
         lV63WWTipodeContaDS_3_Tipodeconta_nome1 = "";
         lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 = "";
         lV67WWTipodeContaDS_7_Tipodeconta_codigo2 = "";
         lV68WWTipodeContaDS_8_Tipodeconta_nome2 = "";
         lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 = "";
         lV72WWTipodeContaDS_12_Tipodeconta_codigo3 = "";
         lV73WWTipodeContaDS_13_Tipodeconta_nome3 = "";
         lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 = "";
         lV75WWTipodeContaDS_15_Tftipodeconta_codigo = "";
         lV77WWTipodeContaDS_17_Tftipodeconta_nome = "";
         lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod = "";
         AV61WWTipodeContaDS_1_Dynamicfiltersselector1 = "";
         AV62WWTipodeContaDS_2_Tipodeconta_codigo1 = "";
         AV63WWTipodeContaDS_3_Tipodeconta_nome1 = "";
         AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 = "";
         AV66WWTipodeContaDS_6_Dynamicfiltersselector2 = "";
         AV67WWTipodeContaDS_7_Tipodeconta_codigo2 = "";
         AV68WWTipodeContaDS_8_Tipodeconta_nome2 = "";
         AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 = "";
         AV71WWTipodeContaDS_11_Dynamicfiltersselector3 = "";
         AV72WWTipodeContaDS_12_Tipodeconta_codigo3 = "";
         AV73WWTipodeContaDS_13_Tipodeconta_nome3 = "";
         AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 = "";
         AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel = "";
         AV75WWTipodeContaDS_15_Tftipodeconta_codigo = "";
         AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel = "";
         AV77WWTipodeContaDS_17_Tftipodeconta_nome = "";
         AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = "";
         AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod = "";
         H00FH8_A885TipoDeConta_ContaPaiCod = new String[] {""} ;
         H00FH8_n885TipoDeConta_ContaPaiCod = new bool[] {false} ;
         H00FH8_A873TipodeConta_Tipo = new String[] {""} ;
         H00FH8_A871TipodeConta_Nome = new String[] {""} ;
         H00FH8_A870TipodeConta_Codigo = new String[] {""} ;
         H00FH9_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV48TFTipodeConta_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTipodecontatitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwtipodeconta__default(),
            new Object[][] {
                new Object[] {
               H00FH2_A870TipodeConta_Codigo, H00FH2_A871TipodeConta_Nome
               }
               , new Object[] {
               H00FH3_A870TipodeConta_Codigo, H00FH3_A871TipodeConta_Nome
               }
               , new Object[] {
               H00FH4_A870TipodeConta_Codigo, H00FH4_A871TipodeConta_Nome
               }
               , new Object[] {
               H00FH5_A870TipodeConta_Codigo, H00FH5_A871TipodeConta_Nome
               }
               , new Object[] {
               H00FH6_A870TipodeConta_Codigo, H00FH6_A871TipodeConta_Nome
               }
               , new Object[] {
               H00FH7_A870TipodeConta_Codigo, H00FH7_A871TipodeConta_Nome
               }
               , new Object[] {
               H00FH8_A885TipoDeConta_ContaPaiCod, H00FH8_n885TipoDeConta_ContaPaiCod, H00FH8_A873TipodeConta_Tipo, H00FH8_A871TipodeConta_Nome, H00FH8_A870TipodeConta_Codigo
               }
               , new Object[] {
               H00FH9_AGRID_nRecordCount
               }
            }
         );
         AV84Pgmname = "WWTipodeConta";
         /* GeneXus formulas. */
         AV84Pgmname = "WWTipodeConta";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_79 ;
      private short nGXsfl_79_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_79_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtTipodeConta_Codigo_Titleformat ;
      private short edtTipodeConta_Nome_Titleformat ;
      private short cmbTipodeConta_Tipo_Titleformat ;
      private short edtTipoDeConta_ContaPaiCod_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_tipodeconta_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_tipodeconta_nome_Datalistupdateminimumcharacters ;
      private int Ddo_tipodeconta_contapaicod_Datalistupdateminimumcharacters ;
      private int edtavTftipodeconta_codigo_Visible ;
      private int edtavTftipodeconta_codigo_sel_Visible ;
      private int edtavTftipodeconta_nome_Visible ;
      private int edtavTftipodeconta_nome_sel_Visible ;
      private int edtavTftipodeconta_contapaicod_Visible ;
      private int edtavTftipodeconta_contapaicod_sel_Visible ;
      private int edtavDdo_tipodeconta_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tipodeconta_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tipodeconta_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tipodeconta_contapaicodtitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int AV56PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavTipodeconta_nome1_Visible ;
      private int edtavTipodeconta_nome2_Visible ;
      private int edtavTipodeconta_nome3_Visible ;
      private int AV85GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV57GridCurrentPage ;
      private long AV58GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_tipodeconta_codigo_Activeeventkey ;
      private String Ddo_tipodeconta_codigo_Filteredtext_get ;
      private String Ddo_tipodeconta_codigo_Selectedvalue_get ;
      private String Ddo_tipodeconta_nome_Activeeventkey ;
      private String Ddo_tipodeconta_nome_Filteredtext_get ;
      private String Ddo_tipodeconta_nome_Selectedvalue_get ;
      private String Ddo_tipodeconta_tipo_Activeeventkey ;
      private String Ddo_tipodeconta_tipo_Selectedvalue_get ;
      private String Ddo_tipodeconta_contapaicod_Activeeventkey ;
      private String Ddo_tipodeconta_contapaicod_Filteredtext_get ;
      private String Ddo_tipodeconta_contapaicod_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_79_idx="0001" ;
      private String AV31TipodeConta_Codigo1 ;
      private String AV17TipodeConta_Nome1 ;
      private String AV34TipoDeConta_ContaPaiCod1 ;
      private String AV32TipodeConta_Codigo2 ;
      private String AV21TipodeConta_Nome2 ;
      private String AV35TipoDeConta_ContaPaiCod2 ;
      private String AV33TipodeConta_Codigo3 ;
      private String AV25TipodeConta_Nome3 ;
      private String AV36TipoDeConta_ContaPaiCod3 ;
      private String AV40TFTipodeConta_Codigo ;
      private String AV41TFTipodeConta_Codigo_Sel ;
      private String AV44TFTipodeConta_Nome ;
      private String AV45TFTipodeConta_Nome_Sel ;
      private String AV52TFTipoDeConta_ContaPaiCod ;
      private String AV53TFTipoDeConta_ContaPaiCod_Sel ;
      private String AV84Pgmname ;
      private String A870TipodeConta_Codigo ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_tipodeconta_codigo_Caption ;
      private String Ddo_tipodeconta_codigo_Tooltip ;
      private String Ddo_tipodeconta_codigo_Cls ;
      private String Ddo_tipodeconta_codigo_Filteredtext_set ;
      private String Ddo_tipodeconta_codigo_Selectedvalue_set ;
      private String Ddo_tipodeconta_codigo_Dropdownoptionstype ;
      private String Ddo_tipodeconta_codigo_Titlecontrolidtoreplace ;
      private String Ddo_tipodeconta_codigo_Sortedstatus ;
      private String Ddo_tipodeconta_codigo_Filtertype ;
      private String Ddo_tipodeconta_codigo_Datalisttype ;
      private String Ddo_tipodeconta_codigo_Datalistproc ;
      private String Ddo_tipodeconta_codigo_Sortasc ;
      private String Ddo_tipodeconta_codigo_Sortdsc ;
      private String Ddo_tipodeconta_codigo_Loadingdata ;
      private String Ddo_tipodeconta_codigo_Cleanfilter ;
      private String Ddo_tipodeconta_codigo_Noresultsfound ;
      private String Ddo_tipodeconta_codigo_Searchbuttontext ;
      private String Ddo_tipodeconta_nome_Caption ;
      private String Ddo_tipodeconta_nome_Tooltip ;
      private String Ddo_tipodeconta_nome_Cls ;
      private String Ddo_tipodeconta_nome_Filteredtext_set ;
      private String Ddo_tipodeconta_nome_Selectedvalue_set ;
      private String Ddo_tipodeconta_nome_Dropdownoptionstype ;
      private String Ddo_tipodeconta_nome_Titlecontrolidtoreplace ;
      private String Ddo_tipodeconta_nome_Sortedstatus ;
      private String Ddo_tipodeconta_nome_Filtertype ;
      private String Ddo_tipodeconta_nome_Datalisttype ;
      private String Ddo_tipodeconta_nome_Datalistproc ;
      private String Ddo_tipodeconta_nome_Sortasc ;
      private String Ddo_tipodeconta_nome_Sortdsc ;
      private String Ddo_tipodeconta_nome_Loadingdata ;
      private String Ddo_tipodeconta_nome_Cleanfilter ;
      private String Ddo_tipodeconta_nome_Noresultsfound ;
      private String Ddo_tipodeconta_nome_Searchbuttontext ;
      private String Ddo_tipodeconta_tipo_Caption ;
      private String Ddo_tipodeconta_tipo_Tooltip ;
      private String Ddo_tipodeconta_tipo_Cls ;
      private String Ddo_tipodeconta_tipo_Selectedvalue_set ;
      private String Ddo_tipodeconta_tipo_Dropdownoptionstype ;
      private String Ddo_tipodeconta_tipo_Titlecontrolidtoreplace ;
      private String Ddo_tipodeconta_tipo_Sortedstatus ;
      private String Ddo_tipodeconta_tipo_Datalisttype ;
      private String Ddo_tipodeconta_tipo_Datalistfixedvalues ;
      private String Ddo_tipodeconta_tipo_Sortasc ;
      private String Ddo_tipodeconta_tipo_Sortdsc ;
      private String Ddo_tipodeconta_tipo_Cleanfilter ;
      private String Ddo_tipodeconta_tipo_Searchbuttontext ;
      private String Ddo_tipodeconta_contapaicod_Caption ;
      private String Ddo_tipodeconta_contapaicod_Tooltip ;
      private String Ddo_tipodeconta_contapaicod_Cls ;
      private String Ddo_tipodeconta_contapaicod_Filteredtext_set ;
      private String Ddo_tipodeconta_contapaicod_Selectedvalue_set ;
      private String Ddo_tipodeconta_contapaicod_Dropdownoptionstype ;
      private String Ddo_tipodeconta_contapaicod_Titlecontrolidtoreplace ;
      private String Ddo_tipodeconta_contapaicod_Sortedstatus ;
      private String Ddo_tipodeconta_contapaicod_Filtertype ;
      private String Ddo_tipodeconta_contapaicod_Datalisttype ;
      private String Ddo_tipodeconta_contapaicod_Datalistproc ;
      private String Ddo_tipodeconta_contapaicod_Sortasc ;
      private String Ddo_tipodeconta_contapaicod_Sortdsc ;
      private String Ddo_tipodeconta_contapaicod_Loadingdata ;
      private String Ddo_tipodeconta_contapaicod_Cleanfilter ;
      private String Ddo_tipodeconta_contapaicod_Noresultsfound ;
      private String Ddo_tipodeconta_contapaicod_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTftipodeconta_codigo_Internalname ;
      private String edtavTftipodeconta_codigo_Jsonclick ;
      private String edtavTftipodeconta_codigo_sel_Internalname ;
      private String edtavTftipodeconta_codigo_sel_Jsonclick ;
      private String edtavTftipodeconta_nome_Internalname ;
      private String edtavTftipodeconta_nome_Jsonclick ;
      private String edtavTftipodeconta_nome_sel_Internalname ;
      private String edtavTftipodeconta_nome_sel_Jsonclick ;
      private String edtavTftipodeconta_contapaicod_Internalname ;
      private String edtavTftipodeconta_contapaicod_Jsonclick ;
      private String edtavTftipodeconta_contapaicod_sel_Internalname ;
      private String edtavTftipodeconta_contapaicod_sel_Jsonclick ;
      private String edtavDdo_tipodeconta_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tipodeconta_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tipodeconta_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tipodeconta_contapaicodtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtTipodeConta_Codigo_Internalname ;
      private String A871TipodeConta_Nome ;
      private String edtTipodeConta_Nome_Internalname ;
      private String cmbTipodeConta_Tipo_Internalname ;
      private String A873TipodeConta_Tipo ;
      private String A885TipoDeConta_ContaPaiCod ;
      private String edtTipoDeConta_ContaPaiCod_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV62WWTipodeContaDS_2_Tipodeconta_codigo1 ;
      private String lV63WWTipodeContaDS_3_Tipodeconta_nome1 ;
      private String lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 ;
      private String lV67WWTipodeContaDS_7_Tipodeconta_codigo2 ;
      private String lV68WWTipodeContaDS_8_Tipodeconta_nome2 ;
      private String lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 ;
      private String lV72WWTipodeContaDS_12_Tipodeconta_codigo3 ;
      private String lV73WWTipodeContaDS_13_Tipodeconta_nome3 ;
      private String lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 ;
      private String lV75WWTipodeContaDS_15_Tftipodeconta_codigo ;
      private String lV77WWTipodeContaDS_17_Tftipodeconta_nome ;
      private String lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod ;
      private String AV62WWTipodeContaDS_2_Tipodeconta_codigo1 ;
      private String AV63WWTipodeContaDS_3_Tipodeconta_nome1 ;
      private String AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 ;
      private String AV67WWTipodeContaDS_7_Tipodeconta_codigo2 ;
      private String AV68WWTipodeContaDS_8_Tipodeconta_nome2 ;
      private String AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 ;
      private String AV72WWTipodeContaDS_12_Tipodeconta_codigo3 ;
      private String AV73WWTipodeContaDS_13_Tipodeconta_nome3 ;
      private String AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 ;
      private String AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel ;
      private String AV75WWTipodeContaDS_15_Tftipodeconta_codigo ;
      private String AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel ;
      private String AV77WWTipodeContaDS_17_Tftipodeconta_nome ;
      private String AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ;
      private String AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String dynavTipodeconta_codigo1_Internalname ;
      private String edtavTipodeconta_nome1_Internalname ;
      private String dynavTipodeconta_contapaicod1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String dynavTipodeconta_codigo2_Internalname ;
      private String edtavTipodeconta_nome2_Internalname ;
      private String dynavTipodeconta_contapaicod2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String dynavTipodeconta_codigo3_Internalname ;
      private String edtavTipodeconta_nome3_Internalname ;
      private String dynavTipodeconta_contapaicod3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_tipodeconta_codigo_Internalname ;
      private String Ddo_tipodeconta_nome_Internalname ;
      private String Ddo_tipodeconta_tipo_Internalname ;
      private String Ddo_tipodeconta_contapaicod_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtTipodeConta_Codigo_Title ;
      private String edtTipodeConta_Nome_Title ;
      private String edtTipoDeConta_ContaPaiCod_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtTipodeConta_Nome_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblTipodecontatitle_Internalname ;
      private String lblTipodecontatitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String dynavTipodeconta_codigo1_Jsonclick ;
      private String edtavTipodeconta_nome1_Jsonclick ;
      private String dynavTipodeconta_contapaicod1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String dynavTipodeconta_codigo2_Jsonclick ;
      private String edtavTipodeconta_nome2_Jsonclick ;
      private String dynavTipodeconta_contapaicod2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String dynavTipodeconta_codigo3_Jsonclick ;
      private String edtavTipodeconta_nome3_Jsonclick ;
      private String dynavTipodeconta_contapaicod3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_79_fel_idx="0001" ;
      private String ROClassString ;
      private String edtTipodeConta_Codigo_Jsonclick ;
      private String edtTipodeConta_Nome_Jsonclick ;
      private String cmbTipodeConta_Tipo_Jsonclick ;
      private String edtTipoDeConta_ContaPaiCod_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_tipodeconta_codigo_Includesortasc ;
      private bool Ddo_tipodeconta_codigo_Includesortdsc ;
      private bool Ddo_tipodeconta_codigo_Includefilter ;
      private bool Ddo_tipodeconta_codigo_Filterisrange ;
      private bool Ddo_tipodeconta_codigo_Includedatalist ;
      private bool Ddo_tipodeconta_nome_Includesortasc ;
      private bool Ddo_tipodeconta_nome_Includesortdsc ;
      private bool Ddo_tipodeconta_nome_Includefilter ;
      private bool Ddo_tipodeconta_nome_Filterisrange ;
      private bool Ddo_tipodeconta_nome_Includedatalist ;
      private bool Ddo_tipodeconta_tipo_Includesortasc ;
      private bool Ddo_tipodeconta_tipo_Includesortdsc ;
      private bool Ddo_tipodeconta_tipo_Includefilter ;
      private bool Ddo_tipodeconta_tipo_Includedatalist ;
      private bool Ddo_tipodeconta_tipo_Allowmultipleselection ;
      private bool Ddo_tipodeconta_contapaicod_Includesortasc ;
      private bool Ddo_tipodeconta_contapaicod_Includesortdsc ;
      private bool Ddo_tipodeconta_contapaicod_Includefilter ;
      private bool Ddo_tipodeconta_contapaicod_Filterisrange ;
      private bool Ddo_tipodeconta_contapaicod_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n885TipoDeConta_ContaPaiCod ;
      private bool AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 ;
      private bool AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV48TFTipodeConta_Tipo_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV42ddo_TipodeConta_CodigoTitleControlIdToReplace ;
      private String AV46ddo_TipodeConta_NomeTitleControlIdToReplace ;
      private String AV50ddo_TipodeConta_TipoTitleControlIdToReplace ;
      private String AV54ddo_TipoDeConta_ContaPaiCodTitleControlIdToReplace ;
      private String AV82Update_GXI ;
      private String AV83Delete_GXI ;
      private String AV61WWTipodeContaDS_1_Dynamicfiltersselector1 ;
      private String AV66WWTipodeContaDS_6_Dynamicfiltersselector2 ;
      private String AV71WWTipodeContaDS_11_Dynamicfiltersselector3 ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox dynavTipodeconta_codigo1 ;
      private GXCombobox dynavTipodeconta_contapaicod1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox dynavTipodeconta_codigo2 ;
      private GXCombobox dynavTipodeconta_contapaicod2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox dynavTipodeconta_codigo3 ;
      private GXCombobox dynavTipodeconta_contapaicod3 ;
      private GXCombobox cmbTipodeConta_Tipo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00FH2_A870TipodeConta_Codigo ;
      private String[] H00FH2_A871TipodeConta_Nome ;
      private String[] H00FH3_A870TipodeConta_Codigo ;
      private String[] H00FH3_A871TipodeConta_Nome ;
      private String[] H00FH4_A870TipodeConta_Codigo ;
      private String[] H00FH4_A871TipodeConta_Nome ;
      private String[] H00FH5_A870TipodeConta_Codigo ;
      private String[] H00FH5_A871TipodeConta_Nome ;
      private String[] H00FH6_A870TipodeConta_Codigo ;
      private String[] H00FH6_A871TipodeConta_Nome ;
      private String[] H00FH7_A870TipodeConta_Codigo ;
      private String[] H00FH7_A871TipodeConta_Nome ;
      private String[] H00FH8_A885TipoDeConta_ContaPaiCod ;
      private bool[] H00FH8_n885TipoDeConta_ContaPaiCod ;
      private String[] H00FH8_A873TipodeConta_Tipo ;
      private String[] H00FH8_A871TipodeConta_Nome ;
      private String[] H00FH8_A870TipodeConta_Codigo ;
      private long[] H00FH9_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV49TFTipodeConta_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39TipodeConta_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43TipodeConta_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47TipodeConta_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51TipoDeConta_ContaPaiCodTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV55DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwtipodeconta__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00FH8( IGxContext context ,
                                             String A873TipodeConta_Tipo ,
                                             IGxCollection AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels ,
                                             String AV61WWTipodeContaDS_1_Dynamicfiltersselector1 ,
                                             String AV62WWTipodeContaDS_2_Tipodeconta_codigo1 ,
                                             String AV63WWTipodeContaDS_3_Tipodeconta_nome1 ,
                                             String AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 ,
                                             bool AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 ,
                                             String AV66WWTipodeContaDS_6_Dynamicfiltersselector2 ,
                                             String AV67WWTipodeContaDS_7_Tipodeconta_codigo2 ,
                                             String AV68WWTipodeContaDS_8_Tipodeconta_nome2 ,
                                             String AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 ,
                                             bool AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 ,
                                             String AV71WWTipodeContaDS_11_Dynamicfiltersselector3 ,
                                             String AV72WWTipodeContaDS_12_Tipodeconta_codigo3 ,
                                             String AV73WWTipodeContaDS_13_Tipodeconta_nome3 ,
                                             String AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 ,
                                             String AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel ,
                                             String AV75WWTipodeContaDS_15_Tftipodeconta_codigo ,
                                             String AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel ,
                                             String AV77WWTipodeContaDS_17_Tftipodeconta_nome ,
                                             int AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count ,
                                             String AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ,
                                             String AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod ,
                                             String A870TipodeConta_Codigo ,
                                             String A871TipodeConta_Nome ,
                                             String A885TipoDeConta_ContaPaiCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [20] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [TipoDeConta_ContaPaiCod], [TipodeConta_Tipo], [TipodeConta_Nome], [TipodeConta_Codigo]";
         sFromString = " FROM [TipodeConta] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV61WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTipodeContaDS_2_Tipodeconta_codigo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV62WWTipodeContaDS_2_Tipodeconta_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV62WWTipodeContaDS_2_Tipodeconta_codigo1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV61WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWTipodeContaDS_3_Tipodeconta_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV63WWTipodeContaDS_3_Tipodeconta_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV63WWTipodeContaDS_3_Tipodeconta_nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV61WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV66WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWTipodeContaDS_7_Tipodeconta_codigo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV67WWTipodeContaDS_7_Tipodeconta_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV67WWTipodeContaDS_7_Tipodeconta_codigo2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV66WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWTipodeContaDS_8_Tipodeconta_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV68WWTipodeContaDS_8_Tipodeconta_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV68WWTipodeContaDS_8_Tipodeconta_nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV66WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWTipodeContaDS_12_Tipodeconta_codigo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV72WWTipodeContaDS_12_Tipodeconta_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV72WWTipodeContaDS_12_Tipodeconta_codigo3)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWTipodeContaDS_13_Tipodeconta_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV73WWTipodeContaDS_13_Tipodeconta_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV73WWTipodeContaDS_13_Tipodeconta_nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWTipodeContaDS_15_Tftipodeconta_codigo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like @lV75WWTipodeContaDS_15_Tftipodeconta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like @lV75WWTipodeContaDS_15_Tftipodeconta_codigo)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] = @AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] = @AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWTipodeContaDS_17_Tftipodeconta_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like @lV77WWTipodeContaDS_17_Tftipodeconta_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like @lV77WWTipodeContaDS_17_Tftipodeconta_nome)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] = @AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] = @AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like @lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like @lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] = @AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] = @AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipodeConta_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipodeConta_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipodeConta_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipodeConta_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipodeConta_Tipo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipodeConta_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipoDeConta_ContaPaiCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipoDeConta_ContaPaiCod] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [TipodeConta_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00FH9( IGxContext context ,
                                             String A873TipodeConta_Tipo ,
                                             IGxCollection AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels ,
                                             String AV61WWTipodeContaDS_1_Dynamicfiltersselector1 ,
                                             String AV62WWTipodeContaDS_2_Tipodeconta_codigo1 ,
                                             String AV63WWTipodeContaDS_3_Tipodeconta_nome1 ,
                                             String AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1 ,
                                             bool AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 ,
                                             String AV66WWTipodeContaDS_6_Dynamicfiltersselector2 ,
                                             String AV67WWTipodeContaDS_7_Tipodeconta_codigo2 ,
                                             String AV68WWTipodeContaDS_8_Tipodeconta_nome2 ,
                                             String AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2 ,
                                             bool AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 ,
                                             String AV71WWTipodeContaDS_11_Dynamicfiltersselector3 ,
                                             String AV72WWTipodeContaDS_12_Tipodeconta_codigo3 ,
                                             String AV73WWTipodeContaDS_13_Tipodeconta_nome3 ,
                                             String AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3 ,
                                             String AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel ,
                                             String AV75WWTipodeContaDS_15_Tftipodeconta_codigo ,
                                             String AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel ,
                                             String AV77WWTipodeContaDS_17_Tftipodeconta_nome ,
                                             int AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count ,
                                             String AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ,
                                             String AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod ,
                                             String A870TipodeConta_Codigo ,
                                             String A871TipodeConta_Nome ,
                                             String A885TipoDeConta_ContaPaiCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [15] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [TipodeConta] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV61WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTipodeContaDS_2_Tipodeconta_codigo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV62WWTipodeContaDS_2_Tipodeconta_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV62WWTipodeContaDS_2_Tipodeconta_codigo1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV61WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWTipodeContaDS_3_Tipodeconta_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV63WWTipodeContaDS_3_Tipodeconta_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV63WWTipodeContaDS_3_Tipodeconta_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV61WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTipodeContaDS_4_Tipodeconta_contapaicod1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV66WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWTipodeContaDS_7_Tipodeconta_codigo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV67WWTipodeContaDS_7_Tipodeconta_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV67WWTipodeContaDS_7_Tipodeconta_codigo2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV66WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWTipodeContaDS_8_Tipodeconta_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV68WWTipodeContaDS_8_Tipodeconta_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV68WWTipodeContaDS_8_Tipodeconta_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV65WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV66WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWTipodeContaDS_9_Tipodeconta_contapaicod2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWTipodeContaDS_12_Tipodeconta_codigo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV72WWTipodeContaDS_12_Tipodeconta_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV72WWTipodeContaDS_12_Tipodeconta_codigo3)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWTipodeContaDS_13_Tipodeconta_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV73WWTipodeContaDS_13_Tipodeconta_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV73WWTipodeContaDS_13_Tipodeconta_nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV70WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWTipodeContaDS_14_Tipodeconta_contapaicod3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWTipodeContaDS_15_Tftipodeconta_codigo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like @lV75WWTipodeContaDS_15_Tftipodeconta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like @lV75WWTipodeContaDS_15_Tftipodeconta_codigo)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] = @AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] = @AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWTipodeContaDS_17_Tftipodeconta_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like @lV77WWTipodeContaDS_17_Tftipodeconta_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like @lV77WWTipodeContaDS_17_Tftipodeconta_nome)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] = @AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] = @AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV79WWTipodeContaDS_19_Tftipodeconta_tipo_sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWTipodeContaDS_20_Tftipodeconta_contapaicod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like @lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like @lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] = @AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] = @AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 6 :
                     return conditional_H00FH8(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
               case 7 :
                     return conditional_H00FH9(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00FH2 ;
          prmH00FH2 = new Object[] {
          } ;
          Object[] prmH00FH3 ;
          prmH00FH3 = new Object[] {
          } ;
          Object[] prmH00FH4 ;
          prmH00FH4 = new Object[] {
          } ;
          Object[] prmH00FH5 ;
          prmH00FH5 = new Object[] {
          } ;
          Object[] prmH00FH6 ;
          prmH00FH6 = new Object[] {
          } ;
          Object[] prmH00FH7 ;
          prmH00FH7 = new Object[] {
          } ;
          Object[] prmH00FH8 ;
          prmH00FH8 = new Object[] {
          new Object[] {"@lV62WWTipodeContaDS_2_Tipodeconta_codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV63WWTipodeContaDS_3_Tipodeconta_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV67WWTipodeContaDS_7_Tipodeconta_codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV68WWTipodeContaDS_8_Tipodeconta_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV72WWTipodeContaDS_12_Tipodeconta_codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV73WWTipodeContaDS_13_Tipodeconta_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV75WWTipodeContaDS_15_Tftipodeconta_codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV77WWTipodeContaDS_17_Tftipodeconta_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00FH9 ;
          prmH00FH9 = new Object[] {
          new Object[] {"@lV62WWTipodeContaDS_2_Tipodeconta_codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV63WWTipodeContaDS_3_Tipodeconta_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWTipodeContaDS_4_Tipodeconta_contapaicod1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV67WWTipodeContaDS_7_Tipodeconta_codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV68WWTipodeContaDS_8_Tipodeconta_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWTipodeContaDS_9_Tipodeconta_contapaicod2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV72WWTipodeContaDS_12_Tipodeconta_codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV73WWTipodeContaDS_13_Tipodeconta_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWTipodeContaDS_14_Tipodeconta_contapaicod3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV75WWTipodeContaDS_15_Tftipodeconta_codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV76WWTipodeContaDS_16_Tftipodeconta_codigo_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV77WWTipodeContaDS_17_Tftipodeconta_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV78WWTipodeContaDS_18_Tftipodeconta_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV80WWTipodeContaDS_20_Tftipodeconta_contapaicod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV81WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00FH2", "SELECT [TipodeConta_Codigo], [TipodeConta_Nome] FROM [TipodeConta] WITH (NOLOCK) ORDER BY [TipodeConta_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FH2,0,0,true,false )
             ,new CursorDef("H00FH3", "SELECT [TipodeConta_Codigo], [TipodeConta_Nome] FROM [TipodeConta] WITH (NOLOCK) ORDER BY [TipodeConta_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FH3,0,0,true,false )
             ,new CursorDef("H00FH4", "SELECT [TipodeConta_Codigo], [TipodeConta_Nome] FROM [TipodeConta] WITH (NOLOCK) ORDER BY [TipodeConta_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FH4,0,0,true,false )
             ,new CursorDef("H00FH5", "SELECT [TipodeConta_Codigo], [TipodeConta_Nome] FROM [TipodeConta] WITH (NOLOCK) ORDER BY [TipodeConta_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FH5,0,0,true,false )
             ,new CursorDef("H00FH6", "SELECT [TipodeConta_Codigo], [TipodeConta_Nome] FROM [TipodeConta] WITH (NOLOCK) ORDER BY [TipodeConta_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FH6,0,0,true,false )
             ,new CursorDef("H00FH7", "SELECT [TipodeConta_Codigo], [TipodeConta_Nome] FROM [TipodeConta] WITH (NOLOCK) ORDER BY [TipodeConta_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FH7,0,0,true,false )
             ,new CursorDef("H00FH8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FH8,11,0,true,false )
             ,new CursorDef("H00FH9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FH9,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                return;
             case 7 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                return;
             case 7 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
       }
    }

 }

}
