/*
               File: PRC_GetHorasEntrega
        Description: Get Horas De Entrega
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:26:39.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_gethorasentrega : GXProcedure
   {
      public prc_gethorasentrega( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_gethorasentrega( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           int aP1_ContagemResultado_Codigo ,
                           int aP2_ContratoServicos_Codigo ,
                           out short aP3_Horas ,
                           out short aP4_Minutos )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV13ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         this.AV19ContratoServicos_Codigo = aP2_ContratoServicos_Codigo;
         this.AV12Horas = 0 ;
         this.AV17Minutos = 0 ;
         initialize();
         executePrivate();
         aP3_Horas=this.AV12Horas;
         aP4_Minutos=this.AV17Minutos;
      }

      public short executeUdp( int aP0_AreaTrabalho_Codigo ,
                               int aP1_ContagemResultado_Codigo ,
                               int aP2_ContratoServicos_Codigo ,
                               out short aP3_Horas )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV13ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         this.AV19ContratoServicos_Codigo = aP2_ContratoServicos_Codigo;
         this.AV12Horas = 0 ;
         this.AV17Minutos = 0 ;
         initialize();
         executePrivate();
         aP3_Horas=this.AV12Horas;
         aP4_Minutos=this.AV17Minutos;
         return AV17Minutos ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 int aP1_ContagemResultado_Codigo ,
                                 int aP2_ContratoServicos_Codigo ,
                                 out short aP3_Horas ,
                                 out short aP4_Minutos )
      {
         prc_gethorasentrega objprc_gethorasentrega;
         objprc_gethorasentrega = new prc_gethorasentrega();
         objprc_gethorasentrega.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_gethorasentrega.AV13ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         objprc_gethorasentrega.AV19ContratoServicos_Codigo = aP2_ContratoServicos_Codigo;
         objprc_gethorasentrega.AV12Horas = 0 ;
         objprc_gethorasentrega.AV17Minutos = 0 ;
         objprc_gethorasentrega.context.SetSubmitInitialConfig(context);
         objprc_gethorasentrega.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_gethorasentrega);
         aP3_Horas=this.AV12Horas;
         aP4_Minutos=this.AV17Minutos;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_gethorasentrega)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P007N2 */
         pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = P007N2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P007N2_n29Contratante_Codigo[0];
            A1192Contratante_FimDoExpediente = P007N2_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P007N2_n1192Contratante_FimDoExpediente[0];
            A1192Contratante_FimDoExpediente = P007N2_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P007N2_n1192Contratante_FimDoExpediente[0];
            /* Using cursor P007N3 */
            pr_default.execute(1, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               AV8Contratante_FimDoExpediente = A1192Contratante_FimDoExpediente;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( (0==AV13ContagemResultado_Codigo) )
         {
            /* Using cursor P007N4 */
            pr_default.execute(2, new Object[] {AV19ContratoServicos_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A160ContratoServicos_Codigo = P007N4_A160ContratoServicos_Codigo[0];
               A1649ContratoServicos_PrazoInicio = P007N4_A1649ContratoServicos_PrazoInicio[0];
               n1649ContratoServicos_PrazoInicio = P007N4_n1649ContratoServicos_PrazoInicio[0];
               OV21ContratoServicos_PrazoInicio = AV21ContratoServicos_PrazoInicio;
               AV21ContratoServicos_PrazoInicio = A1649ContratoServicos_PrazoInicio;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
         }
         else
         {
            /* Using cursor P007N5 */
            pr_default.execute(3, new Object[] {AV13ContagemResultado_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P007N5_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P007N5_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = P007N5_A456ContagemResultado_Codigo[0];
               A490ContagemResultado_ContratadaCod = P007N5_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P007N5_n490ContagemResultado_ContratadaCod[0];
               A601ContagemResultado_Servico = P007N5_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P007N5_n601ContagemResultado_Servico[0];
               A1650ContagemResultado_PrzInc = P007N5_A1650ContagemResultado_PrzInc[0];
               n1650ContagemResultado_PrzInc = P007N5_n1650ContagemResultado_PrzInc[0];
               A601ContagemResultado_Servico = P007N5_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P007N5_n601ContagemResultado_Servico[0];
               A1650ContagemResultado_PrzInc = P007N5_A1650ContagemResultado_PrzInc[0];
               n1650ContagemResultado_PrzInc = P007N5_n1650ContagemResultado_PrzInc[0];
               OV21ContratoServicos_PrazoInicio = AV21ContratoServicos_PrazoInicio;
               AV18Contratada = A490ContagemResultado_ContratadaCod;
               AV20Servico = A601ContagemResultado_Servico;
               AV21ContratoServicos_PrazoInicio = A1650ContagemResultado_PrzInc;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
         }
         if ( AV21ContratoServicos_PrazoInicio == 3 )
         {
            AV10HorasSolicitacao = DateTimeUtil.ResetDate(DateTimeUtil.ServerNow( context, "DEFAULT"));
            AV12Horas = (short)(DateTimeUtil.Hour( AV10HorasSolicitacao));
            AV17Minutos = (short)(DateTimeUtil.Minute( AV10HorasSolicitacao));
         }
         else
         {
            if ( DateTimeUtil.Hour( AV8Contratante_FimDoExpediente) == 0 )
            {
               AV12Horas = 18;
            }
            else
            {
               AV12Horas = (short)(DateTimeUtil.Hour( AV8Contratante_FimDoExpediente));
               AV17Minutos = (short)(DateTimeUtil.Minute( AV8Contratante_FimDoExpediente));
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P007N2_A5AreaTrabalho_Codigo = new int[1] ;
         P007N2_A29Contratante_Codigo = new int[1] ;
         P007N2_n29Contratante_Codigo = new bool[] {false} ;
         P007N2_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P007N2_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         P007N3_A29Contratante_Codigo = new int[1] ;
         P007N3_n29Contratante_Codigo = new bool[] {false} ;
         AV8Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         P007N4_A160ContratoServicos_Codigo = new int[1] ;
         P007N4_A1649ContratoServicos_PrazoInicio = new short[1] ;
         P007N4_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         AV21ContratoServicos_PrazoInicio = 1;
         P007N5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P007N5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P007N5_A456ContagemResultado_Codigo = new int[1] ;
         P007N5_A490ContagemResultado_ContratadaCod = new int[1] ;
         P007N5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P007N5_A601ContagemResultado_Servico = new int[1] ;
         P007N5_n601ContagemResultado_Servico = new bool[] {false} ;
         P007N5_A1650ContagemResultado_PrzInc = new short[1] ;
         P007N5_n1650ContagemResultado_PrzInc = new bool[] {false} ;
         AV10HorasSolicitacao = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_gethorasentrega__default(),
            new Object[][] {
                new Object[] {
               P007N2_A5AreaTrabalho_Codigo, P007N2_A29Contratante_Codigo, P007N2_n29Contratante_Codigo, P007N2_A1192Contratante_FimDoExpediente, P007N2_n1192Contratante_FimDoExpediente
               }
               , new Object[] {
               P007N3_A29Contratante_Codigo
               }
               , new Object[] {
               P007N4_A160ContratoServicos_Codigo, P007N4_A1649ContratoServicos_PrazoInicio, P007N4_n1649ContratoServicos_PrazoInicio
               }
               , new Object[] {
               P007N5_A1553ContagemResultado_CntSrvCod, P007N5_n1553ContagemResultado_CntSrvCod, P007N5_A456ContagemResultado_Codigo, P007N5_A490ContagemResultado_ContratadaCod, P007N5_n490ContagemResultado_ContratadaCod, P007N5_A601ContagemResultado_Servico, P007N5_n601ContagemResultado_Servico, P007N5_A1650ContagemResultado_PrzInc, P007N5_n1650ContagemResultado_PrzInc
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV17Minutos ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short OV21ContratoServicos_PrazoInicio ;
      private short AV21ContratoServicos_PrazoInicio ;
      private short A1650ContagemResultado_PrzInc ;
      private short AV12Horas ;
      private int A5AreaTrabalho_Codigo ;
      private int AV13ContagemResultado_Codigo ;
      private int AV19ContratoServicos_Codigo ;
      private int A29Contratante_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int AV18Contratada ;
      private int AV20Servico ;
      private String scmdbuf ;
      private DateTime A1192Contratante_FimDoExpediente ;
      private DateTime AV8Contratante_FimDoExpediente ;
      private DateTime AV10HorasSolicitacao ;
      private bool n29Contratante_Codigo ;
      private bool n1192Contratante_FimDoExpediente ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1650ContagemResultado_PrzInc ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P007N2_A5AreaTrabalho_Codigo ;
      private int[] P007N2_A29Contratante_Codigo ;
      private bool[] P007N2_n29Contratante_Codigo ;
      private DateTime[] P007N2_A1192Contratante_FimDoExpediente ;
      private bool[] P007N2_n1192Contratante_FimDoExpediente ;
      private int[] P007N3_A29Contratante_Codigo ;
      private bool[] P007N3_n29Contratante_Codigo ;
      private int[] P007N4_A160ContratoServicos_Codigo ;
      private short[] P007N4_A1649ContratoServicos_PrazoInicio ;
      private bool[] P007N4_n1649ContratoServicos_PrazoInicio ;
      private int[] P007N5_A1553ContagemResultado_CntSrvCod ;
      private bool[] P007N5_n1553ContagemResultado_CntSrvCod ;
      private int[] P007N5_A456ContagemResultado_Codigo ;
      private int[] P007N5_A490ContagemResultado_ContratadaCod ;
      private bool[] P007N5_n490ContagemResultado_ContratadaCod ;
      private int[] P007N5_A601ContagemResultado_Servico ;
      private bool[] P007N5_n601ContagemResultado_Servico ;
      private short[] P007N5_A1650ContagemResultado_PrzInc ;
      private bool[] P007N5_n1650ContagemResultado_PrzInc ;
      private short aP3_Horas ;
      private short aP4_Minutos ;
   }

   public class prc_gethorasentrega__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007N2 ;
          prmP007N2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007N3 ;
          prmP007N3 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007N4 ;
          prmP007N4 = new Object[] {
          new Object[] {"@AV19ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007N5 ;
          prmP007N5 = new Object[] {
          new Object[] {"@AV13ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007N2", "SELECT T1.[AreaTrabalho_Codigo], T1.[Contratante_Codigo], T2.[Contratante_FimDoExpediente] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007N2,1,0,true,true )
             ,new CursorDef("P007N3", "SELECT TOP 1 [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007N3,1,0,false,true )
             ,new CursorDef("P007N4", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicos_PrazoInicio] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV19ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007N4,1,0,false,true )
             ,new CursorDef("P007N5", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod], T2.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContratoServicos_PrazoInicio] AS ContagemResultado_PrzInc FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV13ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007N5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
