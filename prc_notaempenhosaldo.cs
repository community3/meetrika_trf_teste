/*
               File: PRC_NotaEmpenhoSaldo
        Description: PRC_Nota Empenho Saldo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:36.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_notaempenhosaldo : GXProcedure
   {
      public prc_notaempenhosaldo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_notaempenhosaldo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_NotaEmpenho_Codigo ,
                           int aP1_SaldoContrato_Codigo ,
                           out decimal aP2_SaldoContrato_Saldo )
      {
         this.AV10NotaEmpenho_Codigo = aP0_NotaEmpenho_Codigo;
         this.AV8SaldoContrato_Codigo = aP1_SaldoContrato_Codigo;
         this.AV9SaldoContrato_Saldo = 0 ;
         initialize();
         executePrivate();
         aP2_SaldoContrato_Saldo=this.AV9SaldoContrato_Saldo;
      }

      public decimal executeUdp( int aP0_NotaEmpenho_Codigo ,
                                 int aP1_SaldoContrato_Codigo )
      {
         this.AV10NotaEmpenho_Codigo = aP0_NotaEmpenho_Codigo;
         this.AV8SaldoContrato_Codigo = aP1_SaldoContrato_Codigo;
         this.AV9SaldoContrato_Saldo = 0 ;
         initialize();
         executePrivate();
         aP2_SaldoContrato_Saldo=this.AV9SaldoContrato_Saldo;
         return AV9SaldoContrato_Saldo ;
      }

      public void executeSubmit( int aP0_NotaEmpenho_Codigo ,
                                 int aP1_SaldoContrato_Codigo ,
                                 out decimal aP2_SaldoContrato_Saldo )
      {
         prc_notaempenhosaldo objprc_notaempenhosaldo;
         objprc_notaempenhosaldo = new prc_notaempenhosaldo();
         objprc_notaempenhosaldo.AV10NotaEmpenho_Codigo = aP0_NotaEmpenho_Codigo;
         objprc_notaempenhosaldo.AV8SaldoContrato_Codigo = aP1_SaldoContrato_Codigo;
         objprc_notaempenhosaldo.AV9SaldoContrato_Saldo = 0 ;
         objprc_notaempenhosaldo.context.SetSubmitInitialConfig(context);
         objprc_notaempenhosaldo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_notaempenhosaldo);
         aP2_SaldoContrato_Saldo=this.AV9SaldoContrato_Saldo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_notaempenhosaldo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00BM2 */
         pr_default.execute(0, new Object[] {AV8SaldoContrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1561SaldoContrato_Codigo = P00BM2_A1561SaldoContrato_Codigo[0];
            A1576SaldoContrato_Saldo = P00BM2_A1576SaldoContrato_Saldo[0];
            AV9SaldoContrato_Saldo = A1576SaldoContrato_Saldo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Optimized group. */
         /* Using cursor P00BM3 */
         pr_default.execute(1, new Object[] {AV10NotaEmpenho_Codigo});
         c1566NotaEmpenho_Valor = P00BM3_A1566NotaEmpenho_Valor[0];
         n1566NotaEmpenho_Valor = P00BM3_n1566NotaEmpenho_Valor[0];
         pr_default.close(1);
         AV9SaldoContrato_Saldo = (decimal)(AV9SaldoContrato_Saldo-c1566NotaEmpenho_Valor);
         /* End optimized group. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BM2_A1561SaldoContrato_Codigo = new int[1] ;
         P00BM2_A1576SaldoContrato_Saldo = new decimal[1] ;
         P00BM3_A1566NotaEmpenho_Valor = new decimal[1] ;
         P00BM3_n1566NotaEmpenho_Valor = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_notaempenhosaldo__default(),
            new Object[][] {
                new Object[] {
               P00BM2_A1561SaldoContrato_Codigo, P00BM2_A1576SaldoContrato_Saldo
               }
               , new Object[] {
               P00BM3_A1566NotaEmpenho_Valor, P00BM3_n1566NotaEmpenho_Valor
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV10NotaEmpenho_Codigo ;
      private int AV8SaldoContrato_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private decimal AV9SaldoContrato_Saldo ;
      private decimal A1576SaldoContrato_Saldo ;
      private decimal c1566NotaEmpenho_Valor ;
      private String scmdbuf ;
      private bool n1566NotaEmpenho_Valor ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BM2_A1561SaldoContrato_Codigo ;
      private decimal[] P00BM2_A1576SaldoContrato_Saldo ;
      private decimal[] P00BM3_A1566NotaEmpenho_Valor ;
      private bool[] P00BM3_n1566NotaEmpenho_Valor ;
      private decimal aP2_SaldoContrato_Saldo ;
   }

   public class prc_notaempenhosaldo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BM2 ;
          prmP00BM2 = new Object[] {
          new Object[] {"@AV8SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BM3 ;
          prmP00BM3 = new Object[] {
          new Object[] {"@AV10NotaEmpenho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BM2", "SELECT [SaldoContrato_Codigo], [SaldoContrato_Saldo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [SaldoContrato_Codigo] = @AV8SaldoContrato_Codigo ORDER BY [SaldoContrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BM2,1,0,false,true )
             ,new CursorDef("P00BM3", "SELECT SUM([NotaEmpenho_Valor]) FROM [NotaEmpenho] WITH (NOLOCK) WHERE [NotaEmpenho_Codigo] = @AV10NotaEmpenho_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BM3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                return;
             case 1 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
