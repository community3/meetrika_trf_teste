/*
               File: type_SdtQueryViewerItemClickData_Filter
        Description: QueryViewerItemClickData
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:58.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerItemClickData.Filter" )]
   [XmlType(TypeName =  "QueryViewerItemClickData.Filter" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtQueryViewerItemClickData_Filter : GxUserType
   {
      public SdtQueryViewerItemClickData_Filter( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerItemClickData_Filter_Name = "";
      }

      public SdtQueryViewerItemClickData_Filter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerItemClickData_Filter deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerItemClickData_Filter)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerItemClickData_Filter obj ;
         obj = this;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Values = deserialized.gxTpr_Values;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Name") )
               {
                  gxTv_SdtQueryViewerItemClickData_Filter_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Values") )
               {
                  if ( gxTv_SdtQueryViewerItemClickData_Filter_Values == null )
                  {
                     gxTv_SdtQueryViewerItemClickData_Filter_Values = new GxSimpleCollection();
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtQueryViewerItemClickData_Filter_Values.readxmlcollection(oReader, "Values", "Item");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerItemClickData.Filter";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Name", StringUtil.RTrim( gxTv_SdtQueryViewerItemClickData_Filter_Name));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( gxTv_SdtQueryViewerItemClickData_Filter_Values != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtQueryViewerItemClickData_Filter_Values.writexmlcollection(oWriter, "Values", sNameSpace1, "Item", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Name", gxTv_SdtQueryViewerItemClickData_Filter_Name, false);
         if ( gxTv_SdtQueryViewerItemClickData_Filter_Values != null )
         {
            AddObjectProperty("Values", gxTv_SdtQueryViewerItemClickData_Filter_Values, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Name" )]
      [  XmlElement( ElementName = "Name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtQueryViewerItemClickData_Filter_Name ;
         }

         set {
            gxTv_SdtQueryViewerItemClickData_Filter_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Values" )]
      [  XmlArray( ElementName = "Values"  )]
      [  XmlArrayItemAttribute( Type= typeof( String ), ElementName= "Item"  , IsNullable=false)]
      public GxSimpleCollection gxTpr_Values_GxSimpleCollection
      {
         get {
            if ( gxTv_SdtQueryViewerItemClickData_Filter_Values == null )
            {
               gxTv_SdtQueryViewerItemClickData_Filter_Values = new GxSimpleCollection();
            }
            return (GxSimpleCollection)gxTv_SdtQueryViewerItemClickData_Filter_Values ;
         }

         set {
            if ( gxTv_SdtQueryViewerItemClickData_Filter_Values == null )
            {
               gxTv_SdtQueryViewerItemClickData_Filter_Values = new GxSimpleCollection();
            }
            gxTv_SdtQueryViewerItemClickData_Filter_Values = (GxSimpleCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Values
      {
         get {
            if ( gxTv_SdtQueryViewerItemClickData_Filter_Values == null )
            {
               gxTv_SdtQueryViewerItemClickData_Filter_Values = new GxSimpleCollection();
            }
            return gxTv_SdtQueryViewerItemClickData_Filter_Values ;
         }

         set {
            gxTv_SdtQueryViewerItemClickData_Filter_Values = value;
         }

      }

      public void gxTv_SdtQueryViewerItemClickData_Filter_Values_SetNull( )
      {
         gxTv_SdtQueryViewerItemClickData_Filter_Values = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerItemClickData_Filter_Values_IsNull( )
      {
         if ( gxTv_SdtQueryViewerItemClickData_Filter_Values == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerItemClickData_Filter_Name = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerItemClickData_Filter_Name ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( String ))]
      protected IGxCollection gxTv_SdtQueryViewerItemClickData_Filter_Values=null ;
   }

   [DataContract(Name = @"QueryViewerItemClickData.Filter", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtQueryViewerItemClickData_Filter_RESTInterface : GxGenericCollectionItem<SdtQueryViewerItemClickData_Filter>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerItemClickData_Filter_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerItemClickData_Filter_RESTInterface( SdtQueryViewerItemClickData_Filter psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Name" , Order = 0 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "Values" , Order = 1 )]
      public GxSimpleCollection gxTpr_Values
      {
         get {
            return (GxSimpleCollection)(sdt.gxTpr_Values) ;
         }

         set {
            sdt.gxTpr_Values = value;
         }

      }

      public SdtQueryViewerItemClickData_Filter sdt
      {
         get {
            return (SdtQueryViewerItemClickData_Filter)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerItemClickData_Filter() ;
         }
      }

   }

}
