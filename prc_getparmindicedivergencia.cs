/*
               File: PRC_GetParmIndiceDivergencia
        Description: Get Parm Indice Divergencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:51.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getparmindicedivergencia : GXProcedure
   {
      public prc_getparmindicedivergencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getparmindicedivergencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           out decimal aP1_Contrato_IndiceDivergencia ,
                           out String aP2_Contrato_CalculoDivergencia ,
                           out decimal aP3_ContagemResultado_ValorPF )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV10Contrato_IndiceDivergencia = 0 ;
         this.AV9Contrato_CalculoDivergencia = "" ;
         this.AV11ContagemResultado_ValorPF = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_Contrato_IndiceDivergencia=this.AV10Contrato_IndiceDivergencia;
         aP2_Contrato_CalculoDivergencia=this.AV9Contrato_CalculoDivergencia;
         aP3_ContagemResultado_ValorPF=this.AV11ContagemResultado_ValorPF;
      }

      public decimal executeUdp( ref int aP0_ContratoServicos_Codigo ,
                                 out decimal aP1_Contrato_IndiceDivergencia ,
                                 out String aP2_Contrato_CalculoDivergencia )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV10Contrato_IndiceDivergencia = 0 ;
         this.AV9Contrato_CalculoDivergencia = "" ;
         this.AV11ContagemResultado_ValorPF = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_Contrato_IndiceDivergencia=this.AV10Contrato_IndiceDivergencia;
         aP2_Contrato_CalculoDivergencia=this.AV9Contrato_CalculoDivergencia;
         aP3_ContagemResultado_ValorPF=this.AV11ContagemResultado_ValorPF;
         return AV11ContagemResultado_ValorPF ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 out decimal aP1_Contrato_IndiceDivergencia ,
                                 out String aP2_Contrato_CalculoDivergencia ,
                                 out decimal aP3_ContagemResultado_ValorPF )
      {
         prc_getparmindicedivergencia objprc_getparmindicedivergencia;
         objprc_getparmindicedivergencia = new prc_getparmindicedivergencia();
         objprc_getparmindicedivergencia.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_getparmindicedivergencia.AV10Contrato_IndiceDivergencia = 0 ;
         objprc_getparmindicedivergencia.AV9Contrato_CalculoDivergencia = "" ;
         objprc_getparmindicedivergencia.AV11ContagemResultado_ValorPF = 0 ;
         objprc_getparmindicedivergencia.context.SetSubmitInitialConfig(context);
         objprc_getparmindicedivergencia.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getparmindicedivergencia);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_Contrato_IndiceDivergencia=this.AV10Contrato_IndiceDivergencia;
         aP2_Contrato_CalculoDivergencia=this.AV9Contrato_CalculoDivergencia;
         aP3_ContagemResultado_ValorPF=this.AV11ContagemResultado_ValorPF;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getparmindicedivergencia)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P002J2 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P002J2_A74Contrato_Codigo[0];
            A452Contrato_CalculoDivergencia = P002J2_A452Contrato_CalculoDivergencia[0];
            A557Servico_VlrUnidadeContratada = P002J2_A557Servico_VlrUnidadeContratada[0];
            A116Contrato_ValorUnidadeContratacao = P002J2_A116Contrato_ValorUnidadeContratacao[0];
            A1455ContratoServicos_IndiceDivergencia = P002J2_A1455ContratoServicos_IndiceDivergencia[0];
            n1455ContratoServicos_IndiceDivergencia = P002J2_n1455ContratoServicos_IndiceDivergencia[0];
            A453Contrato_IndiceDivergencia = P002J2_A453Contrato_IndiceDivergencia[0];
            A452Contrato_CalculoDivergencia = P002J2_A452Contrato_CalculoDivergencia[0];
            A116Contrato_ValorUnidadeContratacao = P002J2_A116Contrato_ValorUnidadeContratacao[0];
            A453Contrato_IndiceDivergencia = P002J2_A453Contrato_IndiceDivergencia[0];
            OV9Contrato_CalculoDivergencia = AV9Contrato_CalculoDivergencia;
            OV10Contrato_IndiceDivergencia = AV10Contrato_IndiceDivergencia;
            AV9Contrato_CalculoDivergencia = A452Contrato_CalculoDivergencia;
            if ( ( A557Servico_VlrUnidadeContratada > Convert.ToDecimal( 0 )) )
            {
               AV11ContagemResultado_ValorPF = A557Servico_VlrUnidadeContratada;
            }
            else
            {
               AV11ContagemResultado_ValorPF = A116Contrato_ValorUnidadeContratacao;
            }
            if ( ( A1455ContratoServicos_IndiceDivergencia > Convert.ToDecimal( 0 )) )
            {
               AV10Contrato_IndiceDivergencia = A1455ContratoServicos_IndiceDivergencia;
            }
            else
            {
               AV10Contrato_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P002J2_A74Contrato_Codigo = new int[1] ;
         P002J2_A160ContratoServicos_Codigo = new int[1] ;
         P002J2_A452Contrato_CalculoDivergencia = new String[] {""} ;
         P002J2_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         P002J2_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P002J2_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         P002J2_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         P002J2_A453Contrato_IndiceDivergencia = new decimal[1] ;
         A452Contrato_CalculoDivergencia = "";
         OV9Contrato_CalculoDivergencia = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getparmindicedivergencia__default(),
            new Object[][] {
                new Object[] {
               P002J2_A74Contrato_Codigo, P002J2_A160ContratoServicos_Codigo, P002J2_A452Contrato_CalculoDivergencia, P002J2_A557Servico_VlrUnidadeContratada, P002J2_A116Contrato_ValorUnidadeContratacao, P002J2_A1455ContratoServicos_IndiceDivergencia, P002J2_n1455ContratoServicos_IndiceDivergencia, P002J2_A453Contrato_IndiceDivergencia
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A160ContratoServicos_Codigo ;
      private int A74Contrato_Codigo ;
      private decimal AV11ContagemResultado_ValorPF ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal A1455ContratoServicos_IndiceDivergencia ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal OV10Contrato_IndiceDivergencia ;
      private decimal AV10Contrato_IndiceDivergencia ;
      private String scmdbuf ;
      private String A452Contrato_CalculoDivergencia ;
      private String OV9Contrato_CalculoDivergencia ;
      private String AV9Contrato_CalculoDivergencia ;
      private bool n1455ContratoServicos_IndiceDivergencia ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P002J2_A74Contrato_Codigo ;
      private int[] P002J2_A160ContratoServicos_Codigo ;
      private String[] P002J2_A452Contrato_CalculoDivergencia ;
      private decimal[] P002J2_A557Servico_VlrUnidadeContratada ;
      private decimal[] P002J2_A116Contrato_ValorUnidadeContratacao ;
      private decimal[] P002J2_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] P002J2_n1455ContratoServicos_IndiceDivergencia ;
      private decimal[] P002J2_A453Contrato_IndiceDivergencia ;
      private decimal aP1_Contrato_IndiceDivergencia ;
      private String aP2_Contrato_CalculoDivergencia ;
      private decimal aP3_ContagemResultado_ValorPF ;
   }

   public class prc_getparmindicedivergencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002J2 ;
          prmP002J2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002J2", "SELECT TOP 1 T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T2.[Contrato_CalculoDivergencia], T1.[Servico_VlrUnidadeContratada], T2.[Contrato_ValorUnidadeContratacao], T1.[ContratoServicos_IndiceDivergencia], T2.[Contrato_IndiceDivergencia] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002J2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
