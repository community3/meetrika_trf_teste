/*
               File: PRC_InsereGlosa
        Description: PRC_Insere Glosa
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:23.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_insereglosa : GXProcedure
   {
      public prc_insereglosa( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_insereglosa( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           ref DateTime aP1_ContagemResultado_GlsData ,
                           ref String aP2_ContagemResultado_GlsDescricao ,
                           ref decimal aP3_ContagemResultado_GlsValor ,
                           ref int aP4_ContagemResultado_GlsUser )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10ContagemResultado_GlsData = aP1_ContagemResultado_GlsData;
         this.AV11ContagemResultado_GlsDescricao = aP2_ContagemResultado_GlsDescricao;
         this.AV8ContagemResultado_GlsValor = aP3_ContagemResultado_GlsValor;
         this.AV9ContagemResultado_GlsUser = aP4_ContagemResultado_GlsUser;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContagemResultado_GlsData=this.AV10ContagemResultado_GlsData;
         aP2_ContagemResultado_GlsDescricao=this.AV11ContagemResultado_GlsDescricao;
         aP3_ContagemResultado_GlsValor=this.AV8ContagemResultado_GlsValor;
         aP4_ContagemResultado_GlsUser=this.AV9ContagemResultado_GlsUser;
      }

      public int executeUdp( ref int aP0_ContagemResultado_Codigo ,
                             ref DateTime aP1_ContagemResultado_GlsData ,
                             ref String aP2_ContagemResultado_GlsDescricao ,
                             ref decimal aP3_ContagemResultado_GlsValor )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10ContagemResultado_GlsData = aP1_ContagemResultado_GlsData;
         this.AV11ContagemResultado_GlsDescricao = aP2_ContagemResultado_GlsDescricao;
         this.AV8ContagemResultado_GlsValor = aP3_ContagemResultado_GlsValor;
         this.AV9ContagemResultado_GlsUser = aP4_ContagemResultado_GlsUser;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContagemResultado_GlsData=this.AV10ContagemResultado_GlsData;
         aP2_ContagemResultado_GlsDescricao=this.AV11ContagemResultado_GlsDescricao;
         aP3_ContagemResultado_GlsValor=this.AV8ContagemResultado_GlsValor;
         aP4_ContagemResultado_GlsUser=this.AV9ContagemResultado_GlsUser;
         return AV9ContagemResultado_GlsUser ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 ref DateTime aP1_ContagemResultado_GlsData ,
                                 ref String aP2_ContagemResultado_GlsDescricao ,
                                 ref decimal aP3_ContagemResultado_GlsValor ,
                                 ref int aP4_ContagemResultado_GlsUser )
      {
         prc_insereglosa objprc_insereglosa;
         objprc_insereglosa = new prc_insereglosa();
         objprc_insereglosa.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_insereglosa.AV10ContagemResultado_GlsData = aP1_ContagemResultado_GlsData;
         objprc_insereglosa.AV11ContagemResultado_GlsDescricao = aP2_ContagemResultado_GlsDescricao;
         objprc_insereglosa.AV8ContagemResultado_GlsValor = aP3_ContagemResultado_GlsValor;
         objprc_insereglosa.AV9ContagemResultado_GlsUser = aP4_ContagemResultado_GlsUser;
         objprc_insereglosa.context.SetSubmitInitialConfig(context);
         objprc_insereglosa.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_insereglosa);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContagemResultado_GlsData=this.AV10ContagemResultado_GlsData;
         aP2_ContagemResultado_GlsDescricao=this.AV11ContagemResultado_GlsDescricao;
         aP3_ContagemResultado_GlsValor=this.AV8ContagemResultado_GlsValor;
         aP4_ContagemResultado_GlsUser=this.AV9ContagemResultado_GlsUser;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_insereglosa)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P00722 */
         pr_default.execute(0, new Object[] {n1049ContagemResultado_GlsData, AV10ContagemResultado_GlsData, n1050ContagemResultado_GlsDescricao, AV11ContagemResultado_GlsDescricao, n1051ContagemResultado_GlsValor, AV8ContagemResultado_GlsValor, n1052ContagemResultado_GlsUser, AV9ContagemResultado_GlsUser, A456ContagemResultado_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         /* End optimized UPDATE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_InsereGlosa");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         A1049ContagemResultado_GlsData = DateTime.MinValue;
         A1050ContagemResultado_GlsDescricao = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_insereglosa__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int AV9ContagemResultado_GlsUser ;
      private int A1052ContagemResultado_GlsUser ;
      private decimal AV8ContagemResultado_GlsValor ;
      private decimal A1051ContagemResultado_GlsValor ;
      private DateTime AV10ContagemResultado_GlsData ;
      private DateTime A1049ContagemResultado_GlsData ;
      private bool n1049ContagemResultado_GlsData ;
      private bool n1050ContagemResultado_GlsDescricao ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n1052ContagemResultado_GlsUser ;
      private String AV11ContagemResultado_GlsDescricao ;
      private String A1050ContagemResultado_GlsDescricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private DateTime aP1_ContagemResultado_GlsData ;
      private String aP2_ContagemResultado_GlsDescricao ;
      private decimal aP3_ContagemResultado_GlsValor ;
      private int aP4_ContagemResultado_GlsUser ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_insereglosa__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00722 ;
          prmP00722 = new Object[] {
          new Object[] {"@ContagemResultado_GlsData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_GlsDescricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_GlsValor",SqlDbType.Decimal,12,2} ,
          new Object[] {"@ContagemResultado_GlsUser",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00722", "UPDATE [ContagemResultado] SET [ContagemResultado_GlsData]=@ContagemResultado_GlsData, [ContagemResultado_GlsDescricao]=@ContagemResultado_GlsDescricao, [ContagemResultado_GlsValor]=@ContagemResultado_GlsValor, [ContagemResultado_GlsUser]=@ContagemResultado_GlsUser  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00722)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                return;
       }
    }

 }

}
