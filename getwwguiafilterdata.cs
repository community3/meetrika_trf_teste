/*
               File: GetWWGuiaFilterData
        Description: Get WWGuia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:20.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwguiafilterdata : GXProcedure
   {
      public getwwguiafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwguiafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwguiafilterdata objgetwwguiafilterdata;
         objgetwwguiafilterdata = new getwwguiafilterdata();
         objgetwwguiafilterdata.AV20DDOName = aP0_DDOName;
         objgetwwguiafilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetwwguiafilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetwwguiafilterdata.AV24OptionsJson = "" ;
         objgetwwguiafilterdata.AV27OptionsDescJson = "" ;
         objgetwwguiafilterdata.AV29OptionIndexesJson = "" ;
         objgetwwguiafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwguiafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwguiafilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwguiafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_GUIA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADGUIA_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_GUIA_VERSAO") == 0 )
         {
            /* Execute user subroutine: 'LOADGUIA_VERSAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("WWGuiaGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWGuiaGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("WWGuiaGridState"), "");
         }
         AV52GXV1 = 1;
         while ( AV52GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV52GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFGUIA_CODIGO") == 0 )
            {
               AV10TFGuia_Codigo = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
               AV11TFGuia_Codigo_To = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFGUIA_NOME") == 0 )
            {
               AV12TFGuia_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFGUIA_NOME_SEL") == 0 )
            {
               AV13TFGuia_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFGUIA_VERSAO") == 0 )
            {
               AV14TFGuia_Versao = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFGUIA_VERSAO_SEL") == 0 )
            {
               AV15TFGuia_Versao_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFGUIA_ANO") == 0 )
            {
               AV16TFGuia_Ano = (short)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
               AV17TFGuia_Ano_To = (short)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV52GXV1 = (int)(AV52GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "GUIA_NOME") == 0 )
            {
               AV37DynamicFiltersOperator1 = AV35GridStateDynamicFilter.gxTpr_Operator;
               AV38Guia_Nome1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "GUIA_CODIGO") == 0 )
            {
               AV37DynamicFiltersOperator1 = AV35GridStateDynamicFilter.gxTpr_Operator;
               AV39Guia_Codigo1 = (int)(NumberUtil.Val( AV35GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV40DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV41DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "GUIA_NOME") == 0 )
               {
                  AV42DynamicFiltersOperator2 = AV35GridStateDynamicFilter.gxTpr_Operator;
                  AV43Guia_Nome2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "GUIA_CODIGO") == 0 )
               {
                  AV42DynamicFiltersOperator2 = AV35GridStateDynamicFilter.gxTpr_Operator;
                  AV44Guia_Codigo2 = (int)(NumberUtil.Val( AV35GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV45DynamicFiltersEnabled3 = true;
                  AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(3));
                  AV46DynamicFiltersSelector3 = AV35GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "GUIA_NOME") == 0 )
                  {
                     AV47DynamicFiltersOperator3 = AV35GridStateDynamicFilter.gxTpr_Operator;
                     AV48Guia_Nome3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "GUIA_CODIGO") == 0 )
                  {
                     AV47DynamicFiltersOperator3 = AV35GridStateDynamicFilter.gxTpr_Operator;
                     AV49Guia_Codigo3 = (int)(NumberUtil.Val( AV35GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADGUIA_NOMEOPTIONS' Routine */
         AV12TFGuia_Nome = AV18SearchTxt;
         AV13TFGuia_Nome_Sel = "";
         AV54WWGuiaDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV55WWGuiaDS_2_Dynamicfiltersoperator1 = AV37DynamicFiltersOperator1;
         AV56WWGuiaDS_3_Guia_nome1 = AV38Guia_Nome1;
         AV57WWGuiaDS_4_Guia_codigo1 = AV39Guia_Codigo1;
         AV58WWGuiaDS_5_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV59WWGuiaDS_6_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV60WWGuiaDS_7_Dynamicfiltersoperator2 = AV42DynamicFiltersOperator2;
         AV61WWGuiaDS_8_Guia_nome2 = AV43Guia_Nome2;
         AV62WWGuiaDS_9_Guia_codigo2 = AV44Guia_Codigo2;
         AV63WWGuiaDS_10_Dynamicfiltersenabled3 = AV45DynamicFiltersEnabled3;
         AV64WWGuiaDS_11_Dynamicfiltersselector3 = AV46DynamicFiltersSelector3;
         AV65WWGuiaDS_12_Dynamicfiltersoperator3 = AV47DynamicFiltersOperator3;
         AV66WWGuiaDS_13_Guia_nome3 = AV48Guia_Nome3;
         AV67WWGuiaDS_14_Guia_codigo3 = AV49Guia_Codigo3;
         AV68WWGuiaDS_15_Tfguia_codigo = AV10TFGuia_Codigo;
         AV69WWGuiaDS_16_Tfguia_codigo_to = AV11TFGuia_Codigo_To;
         AV70WWGuiaDS_17_Tfguia_nome = AV12TFGuia_Nome;
         AV71WWGuiaDS_18_Tfguia_nome_sel = AV13TFGuia_Nome_Sel;
         AV72WWGuiaDS_19_Tfguia_versao = AV14TFGuia_Versao;
         AV73WWGuiaDS_20_Tfguia_versao_sel = AV15TFGuia_Versao_Sel;
         AV74WWGuiaDS_21_Tfguia_ano = AV16TFGuia_Ano;
         AV75WWGuiaDS_22_Tfguia_ano_to = AV17TFGuia_Ano_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV54WWGuiaDS_1_Dynamicfiltersselector1 ,
                                              AV55WWGuiaDS_2_Dynamicfiltersoperator1 ,
                                              AV56WWGuiaDS_3_Guia_nome1 ,
                                              AV57WWGuiaDS_4_Guia_codigo1 ,
                                              AV58WWGuiaDS_5_Dynamicfiltersenabled2 ,
                                              AV59WWGuiaDS_6_Dynamicfiltersselector2 ,
                                              AV60WWGuiaDS_7_Dynamicfiltersoperator2 ,
                                              AV61WWGuiaDS_8_Guia_nome2 ,
                                              AV62WWGuiaDS_9_Guia_codigo2 ,
                                              AV63WWGuiaDS_10_Dynamicfiltersenabled3 ,
                                              AV64WWGuiaDS_11_Dynamicfiltersselector3 ,
                                              AV65WWGuiaDS_12_Dynamicfiltersoperator3 ,
                                              AV66WWGuiaDS_13_Guia_nome3 ,
                                              AV67WWGuiaDS_14_Guia_codigo3 ,
                                              AV68WWGuiaDS_15_Tfguia_codigo ,
                                              AV69WWGuiaDS_16_Tfguia_codigo_to ,
                                              AV71WWGuiaDS_18_Tfguia_nome_sel ,
                                              AV70WWGuiaDS_17_Tfguia_nome ,
                                              AV73WWGuiaDS_20_Tfguia_versao_sel ,
                                              AV72WWGuiaDS_19_Tfguia_versao ,
                                              AV74WWGuiaDS_21_Tfguia_ano ,
                                              AV75WWGuiaDS_22_Tfguia_ano_to ,
                                              A94Guia_Nome ,
                                              A93Guia_Codigo ,
                                              A95Guia_Versao ,
                                              A96Guia_Ano },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT
                                              }
         });
         lV56WWGuiaDS_3_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV56WWGuiaDS_3_Guia_nome1), 50, "%");
         lV56WWGuiaDS_3_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV56WWGuiaDS_3_Guia_nome1), 50, "%");
         lV61WWGuiaDS_8_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV61WWGuiaDS_8_Guia_nome2), 50, "%");
         lV61WWGuiaDS_8_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV61WWGuiaDS_8_Guia_nome2), 50, "%");
         lV66WWGuiaDS_13_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV66WWGuiaDS_13_Guia_nome3), 50, "%");
         lV66WWGuiaDS_13_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV66WWGuiaDS_13_Guia_nome3), 50, "%");
         lV70WWGuiaDS_17_Tfguia_nome = StringUtil.PadR( StringUtil.RTrim( AV70WWGuiaDS_17_Tfguia_nome), 50, "%");
         lV72WWGuiaDS_19_Tfguia_versao = StringUtil.PadR( StringUtil.RTrim( AV72WWGuiaDS_19_Tfguia_versao), 15, "%");
         /* Using cursor P00I92 */
         pr_default.execute(0, new Object[] {lV56WWGuiaDS_3_Guia_nome1, lV56WWGuiaDS_3_Guia_nome1, AV57WWGuiaDS_4_Guia_codigo1, lV61WWGuiaDS_8_Guia_nome2, lV61WWGuiaDS_8_Guia_nome2, AV62WWGuiaDS_9_Guia_codigo2, lV66WWGuiaDS_13_Guia_nome3, lV66WWGuiaDS_13_Guia_nome3, AV67WWGuiaDS_14_Guia_codigo3, AV68WWGuiaDS_15_Tfguia_codigo, AV69WWGuiaDS_16_Tfguia_codigo_to, lV70WWGuiaDS_17_Tfguia_nome, AV71WWGuiaDS_18_Tfguia_nome_sel, lV72WWGuiaDS_19_Tfguia_versao, AV73WWGuiaDS_20_Tfguia_versao_sel, AV74WWGuiaDS_21_Tfguia_ano, AV75WWGuiaDS_22_Tfguia_ano_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKI92 = false;
            A94Guia_Nome = P00I92_A94Guia_Nome[0];
            A96Guia_Ano = P00I92_A96Guia_Ano[0];
            A95Guia_Versao = P00I92_A95Guia_Versao[0];
            A93Guia_Codigo = P00I92_A93Guia_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00I92_A94Guia_Nome[0], A94Guia_Nome) == 0 ) )
            {
               BRKI92 = false;
               A93Guia_Codigo = P00I92_A93Guia_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKI92 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A94Guia_Nome)) )
            {
               AV22Option = A94Guia_Nome;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKI92 )
            {
               BRKI92 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADGUIA_VERSAOOPTIONS' Routine */
         AV14TFGuia_Versao = AV18SearchTxt;
         AV15TFGuia_Versao_Sel = "";
         AV54WWGuiaDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV55WWGuiaDS_2_Dynamicfiltersoperator1 = AV37DynamicFiltersOperator1;
         AV56WWGuiaDS_3_Guia_nome1 = AV38Guia_Nome1;
         AV57WWGuiaDS_4_Guia_codigo1 = AV39Guia_Codigo1;
         AV58WWGuiaDS_5_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV59WWGuiaDS_6_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV60WWGuiaDS_7_Dynamicfiltersoperator2 = AV42DynamicFiltersOperator2;
         AV61WWGuiaDS_8_Guia_nome2 = AV43Guia_Nome2;
         AV62WWGuiaDS_9_Guia_codigo2 = AV44Guia_Codigo2;
         AV63WWGuiaDS_10_Dynamicfiltersenabled3 = AV45DynamicFiltersEnabled3;
         AV64WWGuiaDS_11_Dynamicfiltersselector3 = AV46DynamicFiltersSelector3;
         AV65WWGuiaDS_12_Dynamicfiltersoperator3 = AV47DynamicFiltersOperator3;
         AV66WWGuiaDS_13_Guia_nome3 = AV48Guia_Nome3;
         AV67WWGuiaDS_14_Guia_codigo3 = AV49Guia_Codigo3;
         AV68WWGuiaDS_15_Tfguia_codigo = AV10TFGuia_Codigo;
         AV69WWGuiaDS_16_Tfguia_codigo_to = AV11TFGuia_Codigo_To;
         AV70WWGuiaDS_17_Tfguia_nome = AV12TFGuia_Nome;
         AV71WWGuiaDS_18_Tfguia_nome_sel = AV13TFGuia_Nome_Sel;
         AV72WWGuiaDS_19_Tfguia_versao = AV14TFGuia_Versao;
         AV73WWGuiaDS_20_Tfguia_versao_sel = AV15TFGuia_Versao_Sel;
         AV74WWGuiaDS_21_Tfguia_ano = AV16TFGuia_Ano;
         AV75WWGuiaDS_22_Tfguia_ano_to = AV17TFGuia_Ano_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV54WWGuiaDS_1_Dynamicfiltersselector1 ,
                                              AV55WWGuiaDS_2_Dynamicfiltersoperator1 ,
                                              AV56WWGuiaDS_3_Guia_nome1 ,
                                              AV57WWGuiaDS_4_Guia_codigo1 ,
                                              AV58WWGuiaDS_5_Dynamicfiltersenabled2 ,
                                              AV59WWGuiaDS_6_Dynamicfiltersselector2 ,
                                              AV60WWGuiaDS_7_Dynamicfiltersoperator2 ,
                                              AV61WWGuiaDS_8_Guia_nome2 ,
                                              AV62WWGuiaDS_9_Guia_codigo2 ,
                                              AV63WWGuiaDS_10_Dynamicfiltersenabled3 ,
                                              AV64WWGuiaDS_11_Dynamicfiltersselector3 ,
                                              AV65WWGuiaDS_12_Dynamicfiltersoperator3 ,
                                              AV66WWGuiaDS_13_Guia_nome3 ,
                                              AV67WWGuiaDS_14_Guia_codigo3 ,
                                              AV68WWGuiaDS_15_Tfguia_codigo ,
                                              AV69WWGuiaDS_16_Tfguia_codigo_to ,
                                              AV71WWGuiaDS_18_Tfguia_nome_sel ,
                                              AV70WWGuiaDS_17_Tfguia_nome ,
                                              AV73WWGuiaDS_20_Tfguia_versao_sel ,
                                              AV72WWGuiaDS_19_Tfguia_versao ,
                                              AV74WWGuiaDS_21_Tfguia_ano ,
                                              AV75WWGuiaDS_22_Tfguia_ano_to ,
                                              A94Guia_Nome ,
                                              A93Guia_Codigo ,
                                              A95Guia_Versao ,
                                              A96Guia_Ano },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT
                                              }
         });
         lV56WWGuiaDS_3_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV56WWGuiaDS_3_Guia_nome1), 50, "%");
         lV56WWGuiaDS_3_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV56WWGuiaDS_3_Guia_nome1), 50, "%");
         lV61WWGuiaDS_8_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV61WWGuiaDS_8_Guia_nome2), 50, "%");
         lV61WWGuiaDS_8_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV61WWGuiaDS_8_Guia_nome2), 50, "%");
         lV66WWGuiaDS_13_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV66WWGuiaDS_13_Guia_nome3), 50, "%");
         lV66WWGuiaDS_13_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV66WWGuiaDS_13_Guia_nome3), 50, "%");
         lV70WWGuiaDS_17_Tfguia_nome = StringUtil.PadR( StringUtil.RTrim( AV70WWGuiaDS_17_Tfguia_nome), 50, "%");
         lV72WWGuiaDS_19_Tfguia_versao = StringUtil.PadR( StringUtil.RTrim( AV72WWGuiaDS_19_Tfguia_versao), 15, "%");
         /* Using cursor P00I93 */
         pr_default.execute(1, new Object[] {lV56WWGuiaDS_3_Guia_nome1, lV56WWGuiaDS_3_Guia_nome1, AV57WWGuiaDS_4_Guia_codigo1, lV61WWGuiaDS_8_Guia_nome2, lV61WWGuiaDS_8_Guia_nome2, AV62WWGuiaDS_9_Guia_codigo2, lV66WWGuiaDS_13_Guia_nome3, lV66WWGuiaDS_13_Guia_nome3, AV67WWGuiaDS_14_Guia_codigo3, AV68WWGuiaDS_15_Tfguia_codigo, AV69WWGuiaDS_16_Tfguia_codigo_to, lV70WWGuiaDS_17_Tfguia_nome, AV71WWGuiaDS_18_Tfguia_nome_sel, lV72WWGuiaDS_19_Tfguia_versao, AV73WWGuiaDS_20_Tfguia_versao_sel, AV74WWGuiaDS_21_Tfguia_ano, AV75WWGuiaDS_22_Tfguia_ano_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKI94 = false;
            A95Guia_Versao = P00I93_A95Guia_Versao[0];
            A96Guia_Ano = P00I93_A96Guia_Ano[0];
            A93Guia_Codigo = P00I93_A93Guia_Codigo[0];
            A94Guia_Nome = P00I93_A94Guia_Nome[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00I93_A95Guia_Versao[0], A95Guia_Versao) == 0 ) )
            {
               BRKI94 = false;
               A93Guia_Codigo = P00I93_A93Guia_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKI94 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A95Guia_Versao)) )
            {
               AV22Option = A95Guia_Versao;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKI94 )
            {
               BRKI94 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFGuia_Nome = "";
         AV13TFGuia_Nome_Sel = "";
         AV14TFGuia_Versao = "";
         AV15TFGuia_Versao_Sel = "";
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV38Guia_Nome1 = "";
         AV41DynamicFiltersSelector2 = "";
         AV43Guia_Nome2 = "";
         AV46DynamicFiltersSelector3 = "";
         AV48Guia_Nome3 = "";
         AV54WWGuiaDS_1_Dynamicfiltersselector1 = "";
         AV56WWGuiaDS_3_Guia_nome1 = "";
         AV59WWGuiaDS_6_Dynamicfiltersselector2 = "";
         AV61WWGuiaDS_8_Guia_nome2 = "";
         AV64WWGuiaDS_11_Dynamicfiltersselector3 = "";
         AV66WWGuiaDS_13_Guia_nome3 = "";
         AV70WWGuiaDS_17_Tfguia_nome = "";
         AV71WWGuiaDS_18_Tfguia_nome_sel = "";
         AV72WWGuiaDS_19_Tfguia_versao = "";
         AV73WWGuiaDS_20_Tfguia_versao_sel = "";
         scmdbuf = "";
         lV56WWGuiaDS_3_Guia_nome1 = "";
         lV61WWGuiaDS_8_Guia_nome2 = "";
         lV66WWGuiaDS_13_Guia_nome3 = "";
         lV70WWGuiaDS_17_Tfguia_nome = "";
         lV72WWGuiaDS_19_Tfguia_versao = "";
         A94Guia_Nome = "";
         A95Guia_Versao = "";
         P00I92_A94Guia_Nome = new String[] {""} ;
         P00I92_A96Guia_Ano = new short[1] ;
         P00I92_A95Guia_Versao = new String[] {""} ;
         P00I92_A93Guia_Codigo = new int[1] ;
         AV22Option = "";
         P00I93_A95Guia_Versao = new String[] {""} ;
         P00I93_A96Guia_Ano = new short[1] ;
         P00I93_A93Guia_Codigo = new int[1] ;
         P00I93_A94Guia_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwguiafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00I92_A94Guia_Nome, P00I92_A96Guia_Ano, P00I92_A95Guia_Versao, P00I92_A93Guia_Codigo
               }
               , new Object[] {
               P00I93_A95Guia_Versao, P00I93_A96Guia_Ano, P00I93_A93Guia_Codigo, P00I93_A94Guia_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFGuia_Ano ;
      private short AV17TFGuia_Ano_To ;
      private short AV37DynamicFiltersOperator1 ;
      private short AV42DynamicFiltersOperator2 ;
      private short AV47DynamicFiltersOperator3 ;
      private short AV55WWGuiaDS_2_Dynamicfiltersoperator1 ;
      private short AV60WWGuiaDS_7_Dynamicfiltersoperator2 ;
      private short AV65WWGuiaDS_12_Dynamicfiltersoperator3 ;
      private short AV74WWGuiaDS_21_Tfguia_ano ;
      private short AV75WWGuiaDS_22_Tfguia_ano_to ;
      private short A96Guia_Ano ;
      private int AV52GXV1 ;
      private int AV10TFGuia_Codigo ;
      private int AV11TFGuia_Codigo_To ;
      private int AV39Guia_Codigo1 ;
      private int AV44Guia_Codigo2 ;
      private int AV49Guia_Codigo3 ;
      private int AV57WWGuiaDS_4_Guia_codigo1 ;
      private int AV62WWGuiaDS_9_Guia_codigo2 ;
      private int AV67WWGuiaDS_14_Guia_codigo3 ;
      private int AV68WWGuiaDS_15_Tfguia_codigo ;
      private int AV69WWGuiaDS_16_Tfguia_codigo_to ;
      private int A93Guia_Codigo ;
      private long AV30count ;
      private String AV12TFGuia_Nome ;
      private String AV13TFGuia_Nome_Sel ;
      private String AV14TFGuia_Versao ;
      private String AV15TFGuia_Versao_Sel ;
      private String AV38Guia_Nome1 ;
      private String AV43Guia_Nome2 ;
      private String AV48Guia_Nome3 ;
      private String AV56WWGuiaDS_3_Guia_nome1 ;
      private String AV61WWGuiaDS_8_Guia_nome2 ;
      private String AV66WWGuiaDS_13_Guia_nome3 ;
      private String AV70WWGuiaDS_17_Tfguia_nome ;
      private String AV71WWGuiaDS_18_Tfguia_nome_sel ;
      private String AV72WWGuiaDS_19_Tfguia_versao ;
      private String AV73WWGuiaDS_20_Tfguia_versao_sel ;
      private String scmdbuf ;
      private String lV56WWGuiaDS_3_Guia_nome1 ;
      private String lV61WWGuiaDS_8_Guia_nome2 ;
      private String lV66WWGuiaDS_13_Guia_nome3 ;
      private String lV70WWGuiaDS_17_Tfguia_nome ;
      private String lV72WWGuiaDS_19_Tfguia_versao ;
      private String A94Guia_Nome ;
      private String A95Guia_Versao ;
      private bool returnInSub ;
      private bool AV40DynamicFiltersEnabled2 ;
      private bool AV45DynamicFiltersEnabled3 ;
      private bool AV58WWGuiaDS_5_Dynamicfiltersenabled2 ;
      private bool AV63WWGuiaDS_10_Dynamicfiltersenabled3 ;
      private bool BRKI92 ;
      private bool BRKI94 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV36DynamicFiltersSelector1 ;
      private String AV41DynamicFiltersSelector2 ;
      private String AV46DynamicFiltersSelector3 ;
      private String AV54WWGuiaDS_1_Dynamicfiltersselector1 ;
      private String AV59WWGuiaDS_6_Dynamicfiltersselector2 ;
      private String AV64WWGuiaDS_11_Dynamicfiltersselector3 ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00I92_A94Guia_Nome ;
      private short[] P00I92_A96Guia_Ano ;
      private String[] P00I92_A95Guia_Versao ;
      private int[] P00I92_A93Guia_Codigo ;
      private String[] P00I93_A95Guia_Versao ;
      private short[] P00I93_A96Guia_Ano ;
      private int[] P00I93_A93Guia_Codigo ;
      private String[] P00I93_A94Guia_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getwwguiafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00I92( IGxContext context ,
                                             String AV54WWGuiaDS_1_Dynamicfiltersselector1 ,
                                             short AV55WWGuiaDS_2_Dynamicfiltersoperator1 ,
                                             String AV56WWGuiaDS_3_Guia_nome1 ,
                                             int AV57WWGuiaDS_4_Guia_codigo1 ,
                                             bool AV58WWGuiaDS_5_Dynamicfiltersenabled2 ,
                                             String AV59WWGuiaDS_6_Dynamicfiltersselector2 ,
                                             short AV60WWGuiaDS_7_Dynamicfiltersoperator2 ,
                                             String AV61WWGuiaDS_8_Guia_nome2 ,
                                             int AV62WWGuiaDS_9_Guia_codigo2 ,
                                             bool AV63WWGuiaDS_10_Dynamicfiltersenabled3 ,
                                             String AV64WWGuiaDS_11_Dynamicfiltersselector3 ,
                                             short AV65WWGuiaDS_12_Dynamicfiltersoperator3 ,
                                             String AV66WWGuiaDS_13_Guia_nome3 ,
                                             int AV67WWGuiaDS_14_Guia_codigo3 ,
                                             int AV68WWGuiaDS_15_Tfguia_codigo ,
                                             int AV69WWGuiaDS_16_Tfguia_codigo_to ,
                                             String AV71WWGuiaDS_18_Tfguia_nome_sel ,
                                             String AV70WWGuiaDS_17_Tfguia_nome ,
                                             String AV73WWGuiaDS_20_Tfguia_versao_sel ,
                                             String AV72WWGuiaDS_19_Tfguia_versao ,
                                             short AV74WWGuiaDS_21_Tfguia_ano ,
                                             short AV75WWGuiaDS_22_Tfguia_ano_to ,
                                             String A94Guia_Nome ,
                                             int A93Guia_Codigo ,
                                             String A95Guia_Versao ,
                                             short A96Guia_Ano )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [17] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Guia_Nome], [Guia_Ano], [Guia_Versao], [Guia_Codigo] FROM [Guia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV54WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV55WWGuiaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWGuiaDS_3_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like @lV56WWGuiaDS_3_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like @lV56WWGuiaDS_3_Guia_nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV55WWGuiaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWGuiaDS_3_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like '%' + @lV56WWGuiaDS_3_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like '%' + @lV56WWGuiaDS_3_Guia_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_CODIGO") == 0 ) && ( AV55WWGuiaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV57WWGuiaDS_4_Guia_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Codigo] = @AV57WWGuiaDS_4_Guia_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Codigo] = @AV57WWGuiaDS_4_Guia_codigo1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV58WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV60WWGuiaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGuiaDS_8_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like @lV61WWGuiaDS_8_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like @lV61WWGuiaDS_8_Guia_nome2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV58WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV60WWGuiaDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGuiaDS_8_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like '%' + @lV61WWGuiaDS_8_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like '%' + @lV61WWGuiaDS_8_Guia_nome2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV58WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_CODIGO") == 0 ) && ( AV60WWGuiaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV62WWGuiaDS_9_Guia_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Codigo] = @AV62WWGuiaDS_9_Guia_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Codigo] = @AV62WWGuiaDS_9_Guia_codigo2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV63WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV65WWGuiaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGuiaDS_13_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like @lV66WWGuiaDS_13_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like @lV66WWGuiaDS_13_Guia_nome3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV63WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV65WWGuiaDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGuiaDS_13_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like '%' + @lV66WWGuiaDS_13_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like '%' + @lV66WWGuiaDS_13_Guia_nome3)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV63WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_CODIGO") == 0 ) && ( AV65WWGuiaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV67WWGuiaDS_14_Guia_codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Codigo] = @AV67WWGuiaDS_14_Guia_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Codigo] = @AV67WWGuiaDS_14_Guia_codigo3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV68WWGuiaDS_15_Tfguia_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Codigo] >= @AV68WWGuiaDS_15_Tfguia_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Codigo] >= @AV68WWGuiaDS_15_Tfguia_codigo)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (0==AV69WWGuiaDS_16_Tfguia_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Codigo] <= @AV69WWGuiaDS_16_Tfguia_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Codigo] <= @AV69WWGuiaDS_16_Tfguia_codigo_to)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWGuiaDS_18_Tfguia_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWGuiaDS_17_Tfguia_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like @lV70WWGuiaDS_17_Tfguia_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like @lV70WWGuiaDS_17_Tfguia_nome)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWGuiaDS_18_Tfguia_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] = @AV71WWGuiaDS_18_Tfguia_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] = @AV71WWGuiaDS_18_Tfguia_nome_sel)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWGuiaDS_20_Tfguia_versao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWGuiaDS_19_Tfguia_versao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Versao] like @lV72WWGuiaDS_19_Tfguia_versao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Versao] like @lV72WWGuiaDS_19_Tfguia_versao)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWGuiaDS_20_Tfguia_versao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Versao] = @AV73WWGuiaDS_20_Tfguia_versao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Versao] = @AV73WWGuiaDS_20_Tfguia_versao_sel)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV74WWGuiaDS_21_Tfguia_ano) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Ano] >= @AV74WWGuiaDS_21_Tfguia_ano)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Ano] >= @AV74WWGuiaDS_21_Tfguia_ano)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (0==AV75WWGuiaDS_22_Tfguia_ano_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Ano] <= @AV75WWGuiaDS_22_Tfguia_ano_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Ano] <= @AV75WWGuiaDS_22_Tfguia_ano_to)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Guia_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00I93( IGxContext context ,
                                             String AV54WWGuiaDS_1_Dynamicfiltersselector1 ,
                                             short AV55WWGuiaDS_2_Dynamicfiltersoperator1 ,
                                             String AV56WWGuiaDS_3_Guia_nome1 ,
                                             int AV57WWGuiaDS_4_Guia_codigo1 ,
                                             bool AV58WWGuiaDS_5_Dynamicfiltersenabled2 ,
                                             String AV59WWGuiaDS_6_Dynamicfiltersselector2 ,
                                             short AV60WWGuiaDS_7_Dynamicfiltersoperator2 ,
                                             String AV61WWGuiaDS_8_Guia_nome2 ,
                                             int AV62WWGuiaDS_9_Guia_codigo2 ,
                                             bool AV63WWGuiaDS_10_Dynamicfiltersenabled3 ,
                                             String AV64WWGuiaDS_11_Dynamicfiltersselector3 ,
                                             short AV65WWGuiaDS_12_Dynamicfiltersoperator3 ,
                                             String AV66WWGuiaDS_13_Guia_nome3 ,
                                             int AV67WWGuiaDS_14_Guia_codigo3 ,
                                             int AV68WWGuiaDS_15_Tfguia_codigo ,
                                             int AV69WWGuiaDS_16_Tfguia_codigo_to ,
                                             String AV71WWGuiaDS_18_Tfguia_nome_sel ,
                                             String AV70WWGuiaDS_17_Tfguia_nome ,
                                             String AV73WWGuiaDS_20_Tfguia_versao_sel ,
                                             String AV72WWGuiaDS_19_Tfguia_versao ,
                                             short AV74WWGuiaDS_21_Tfguia_ano ,
                                             short AV75WWGuiaDS_22_Tfguia_ano_to ,
                                             String A94Guia_Nome ,
                                             int A93Guia_Codigo ,
                                             String A95Guia_Versao ,
                                             short A96Guia_Ano )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [17] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Guia_Versao], [Guia_Ano], [Guia_Codigo], [Guia_Nome] FROM [Guia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV54WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV55WWGuiaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWGuiaDS_3_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like @lV56WWGuiaDS_3_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like @lV56WWGuiaDS_3_Guia_nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV55WWGuiaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWGuiaDS_3_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like '%' + @lV56WWGuiaDS_3_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like '%' + @lV56WWGuiaDS_3_Guia_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWGuiaDS_1_Dynamicfiltersselector1, "GUIA_CODIGO") == 0 ) && ( AV55WWGuiaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV57WWGuiaDS_4_Guia_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Codigo] = @AV57WWGuiaDS_4_Guia_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Codigo] = @AV57WWGuiaDS_4_Guia_codigo1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV58WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV60WWGuiaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGuiaDS_8_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like @lV61WWGuiaDS_8_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like @lV61WWGuiaDS_8_Guia_nome2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV58WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV60WWGuiaDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGuiaDS_8_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like '%' + @lV61WWGuiaDS_8_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like '%' + @lV61WWGuiaDS_8_Guia_nome2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV58WWGuiaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWGuiaDS_6_Dynamicfiltersselector2, "GUIA_CODIGO") == 0 ) && ( AV60WWGuiaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV62WWGuiaDS_9_Guia_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Codigo] = @AV62WWGuiaDS_9_Guia_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Codigo] = @AV62WWGuiaDS_9_Guia_codigo2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV63WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV65WWGuiaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGuiaDS_13_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like @lV66WWGuiaDS_13_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like @lV66WWGuiaDS_13_Guia_nome3)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV63WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV65WWGuiaDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGuiaDS_13_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like '%' + @lV66WWGuiaDS_13_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like '%' + @lV66WWGuiaDS_13_Guia_nome3)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV63WWGuiaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWGuiaDS_11_Dynamicfiltersselector3, "GUIA_CODIGO") == 0 ) && ( AV65WWGuiaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV67WWGuiaDS_14_Guia_codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Codigo] = @AV67WWGuiaDS_14_Guia_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Codigo] = @AV67WWGuiaDS_14_Guia_codigo3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV68WWGuiaDS_15_Tfguia_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Codigo] >= @AV68WWGuiaDS_15_Tfguia_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Codigo] >= @AV68WWGuiaDS_15_Tfguia_codigo)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (0==AV69WWGuiaDS_16_Tfguia_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Codigo] <= @AV69WWGuiaDS_16_Tfguia_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Codigo] <= @AV69WWGuiaDS_16_Tfguia_codigo_to)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWGuiaDS_18_Tfguia_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWGuiaDS_17_Tfguia_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] like @lV70WWGuiaDS_17_Tfguia_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] like @lV70WWGuiaDS_17_Tfguia_nome)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWGuiaDS_18_Tfguia_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Nome] = @AV71WWGuiaDS_18_Tfguia_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Nome] = @AV71WWGuiaDS_18_Tfguia_nome_sel)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWGuiaDS_20_Tfguia_versao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWGuiaDS_19_Tfguia_versao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Versao] like @lV72WWGuiaDS_19_Tfguia_versao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Versao] like @lV72WWGuiaDS_19_Tfguia_versao)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWGuiaDS_20_Tfguia_versao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Versao] = @AV73WWGuiaDS_20_Tfguia_versao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Versao] = @AV73WWGuiaDS_20_Tfguia_versao_sel)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV74WWGuiaDS_21_Tfguia_ano) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Ano] >= @AV74WWGuiaDS_21_Tfguia_ano)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Ano] >= @AV74WWGuiaDS_21_Tfguia_ano)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (0==AV75WWGuiaDS_22_Tfguia_ano_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Guia_Ano] <= @AV75WWGuiaDS_22_Tfguia_ano_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Guia_Ano] <= @AV75WWGuiaDS_22_Tfguia_ano_to)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Guia_Versao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00I92(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (short)dynConstraints[25] );
               case 1 :
                     return conditional_P00I93(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (short)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00I92 ;
          prmP00I92 = new Object[] {
          new Object[] {"@lV56WWGuiaDS_3_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56WWGuiaDS_3_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWGuiaDS_4_Guia_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWGuiaDS_8_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWGuiaDS_8_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV62WWGuiaDS_9_Guia_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV66WWGuiaDS_13_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWGuiaDS_13_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWGuiaDS_14_Guia_codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWGuiaDS_15_Tfguia_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWGuiaDS_16_Tfguia_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV70WWGuiaDS_17_Tfguia_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWGuiaDS_18_Tfguia_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWGuiaDS_19_Tfguia_versao",SqlDbType.Char,15,0} ,
          new Object[] {"@AV73WWGuiaDS_20_Tfguia_versao_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV74WWGuiaDS_21_Tfguia_ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV75WWGuiaDS_22_Tfguia_ano_to",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00I93 ;
          prmP00I93 = new Object[] {
          new Object[] {"@lV56WWGuiaDS_3_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56WWGuiaDS_3_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWGuiaDS_4_Guia_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWGuiaDS_8_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWGuiaDS_8_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV62WWGuiaDS_9_Guia_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV66WWGuiaDS_13_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWGuiaDS_13_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWGuiaDS_14_Guia_codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWGuiaDS_15_Tfguia_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWGuiaDS_16_Tfguia_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV70WWGuiaDS_17_Tfguia_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWGuiaDS_18_Tfguia_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWGuiaDS_19_Tfguia_versao",SqlDbType.Char,15,0} ,
          new Object[] {"@AV73WWGuiaDS_20_Tfguia_versao_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV74WWGuiaDS_21_Tfguia_ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV75WWGuiaDS_22_Tfguia_ano_to",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00I92", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00I92,100,0,true,false )
             ,new CursorDef("P00I93", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00I93,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwguiafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwguiafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwguiafilterdata") )
          {
             return  ;
          }
          getwwguiafilterdata worker = new getwwguiafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
