/*
               File: ServicoServicoFluxo
        Description: Servico Servico Fluxo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:55.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoservicofluxo : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public servicoservicofluxo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public servicoservicofluxo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ServicoFluxo_ServicoCod )
      {
         this.AV7ServicoFluxo_ServicoCod = aP0_ServicoFluxo_ServicoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ServicoFluxo_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ServicoFluxo_ServicoCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_16 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_16_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_16_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV21TFServicoFluxo_Ordem = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFServicoFluxo_Ordem), 3, 0)));
                  AV22TFServicoFluxo_Ordem_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFServicoFluxo_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFServicoFluxo_Ordem_To), 3, 0)));
                  AV25TFServicoFluxo_SrvPosSigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFServicoFluxo_SrvPosSigla", AV25TFServicoFluxo_SrvPosSigla);
                  AV26TFServicoFluxo_SrvPosSigla_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFServicoFluxo_SrvPosSigla_Sel", AV26TFServicoFluxo_SrvPosSigla_Sel);
                  AV29TFServicoFluxo_SrvPosPrcTmp = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFServicoFluxo_SrvPosPrcTmp), 4, 0)));
                  AV30TFServicoFluxo_SrvPosPrcTmp_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFServicoFluxo_SrvPosPrcTmp_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFServicoFluxo_SrvPosPrcTmp_To), 4, 0)));
                  AV33TFServicoFluxo_SrvPosPrcPgm = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFServicoFluxo_SrvPosPrcPgm), 4, 0)));
                  AV34TFServicoFluxo_SrvPosPrcPgm_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFServicoFluxo_SrvPosPrcPgm_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFServicoFluxo_SrvPosPrcPgm_To), 4, 0)));
                  AV37TFServicoFluxo_SrvPosPrcCnc = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFServicoFluxo_SrvPosPrcCnc), 4, 0)));
                  AV38TFServicoFluxo_SrvPosPrcCnc_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFServicoFluxo_SrvPosPrcCnc_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFServicoFluxo_SrvPosPrcCnc_To), 4, 0)));
                  AV7ServicoFluxo_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0)));
                  AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace", AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace);
                  AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace", AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace);
                  AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace", AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace);
                  AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace", AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace);
                  AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace", AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace);
                  A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                  A605Servico_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A605Servico_Sigla", A605Servico_Sigla);
                  AV46Pgmname = GetNextPar( );
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV21TFServicoFluxo_Ordem, AV22TFServicoFluxo_Ordem_To, AV25TFServicoFluxo_SrvPosSigla, AV26TFServicoFluxo_SrvPosSigla_Sel, AV29TFServicoFluxo_SrvPosPrcTmp, AV30TFServicoFluxo_SrvPosPrcTmp_To, AV33TFServicoFluxo_SrvPosPrcPgm, AV34TFServicoFluxo_SrvPosPrcPgm_To, AV37TFServicoFluxo_SrvPosPrcCnc, AV38TFServicoFluxo_SrvPosPrcCnc_To, AV7ServicoFluxo_ServicoCod, AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace, AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace, AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace, A155Servico_Codigo, A605Servico_Sigla, AV46Pgmname, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAL72( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV46Pgmname = "ServicoServicoFluxo";
               context.Gx_err = 0;
               WSL72( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Servico Servico Fluxo") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299315582");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicoservicofluxo.aspx") + "?" + UrlEncode("" +AV7ServicoFluxo_ServicoCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOFLUXO_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21TFServicoFluxo_Ordem), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOFLUXO_ORDEM_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22TFServicoFluxo_Ordem_To), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSSIGLA", StringUtil.RTrim( AV25TFServicoFluxo_SrvPosSigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSSIGLA_SEL", StringUtil.RTrim( AV26TFServicoFluxo_SrvPosSigla_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCTMP", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29TFServicoFluxo_SrvPosPrcTmp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCTMP_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30TFServicoFluxo_SrvPosPrcTmp_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCPGM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33TFServicoFluxo_SrvPosPrcPgm), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCPGM_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34TFServicoFluxo_SrvPosPrcPgm_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCCNC", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFServicoFluxo_SrvPosPrcCnc), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCCNC_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFServicoFluxo_SrvPosPrcCnc_To), 4, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_16", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_16), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV40DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV40DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSERVICOFLUXO_ORDEMTITLEFILTERDATA", AV20ServicoFluxo_OrdemTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSERVICOFLUXO_ORDEMTITLEFILTERDATA", AV20ServicoFluxo_OrdemTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA", AV24ServicoFluxo_SrvPosSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA", AV24ServicoFluxo_SrvPosSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSERVICOFLUXO_SRVPOSPRCTMPTITLEFILTERDATA", AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSERVICOFLUXO_SRVPOSPRCTMPTITLEFILTERDATA", AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSERVICOFLUXO_SRVPOSPRCPGMTITLEFILTERDATA", AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSERVICOFLUXO_SRVPOSPRCPGMTITLEFILTERDATA", AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSERVICOFLUXO_SRVPOSPRCCNCTITLEFILTERDATA", AV36ServicoFluxo_SrvPosPrcCncTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSERVICOFLUXO_SRVPOSPRCCNCTITLEFILTERDATA", AV36ServicoFluxo_SrvPosPrcCncTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ServicoFluxo_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSERVICOFLUXO_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV46Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSERVICOFLUXO_SERVICOSIGLA", StringUtil.RTrim( AV18ServicoFluxo_ServicoSigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Caption", StringUtil.RTrim( Ddo_servicofluxo_ordem_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_ordem_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Cls", StringUtil.RTrim( Ddo_servicofluxo_ordem_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filteredtext_set", StringUtil.RTrim( Ddo_servicofluxo_ordem_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filteredtextto_set", StringUtil.RTrim( Ddo_servicofluxo_ordem_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_ordem_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_ordem_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_ordem_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_ordem_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_ordem_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filtertype", StringUtil.RTrim( Ddo_servicofluxo_ordem_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filterisrange", StringUtil.BoolToStr( Ddo_servicofluxo_ordem_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_ordem_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_ordem_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Rangefilterfrom", StringUtil.RTrim( Ddo_servicofluxo_ordem_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Rangefilterto", StringUtil.RTrim( Ddo_servicofluxo_ordem_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_ordem_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Caption", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Cls", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_srvpossigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_srvpossigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_srvpossigla_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Filtertype", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_servicofluxo_srvpossigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_srvpossigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalisttype", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalistproc", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicofluxo_srvpossigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Loadingdata", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Caption", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Cls", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filteredtext_set", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filteredtextto_set", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprctmp_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprctmp_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprctmp_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filtertype", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filterisrange", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprctmp_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprctmp_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Rangefilterfrom", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Rangefilterto", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Caption", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Cls", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filteredtext_set", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filteredtextto_set", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprcpgm_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprcpgm_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprcpgm_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filtertype", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filterisrange", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprcpgm_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprcpgm_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Rangefilterfrom", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Rangefilterto", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Caption", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Tooltip", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Cls", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filteredtext_set", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filteredtextto_set", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Includesortasc", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprccnc_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Includesortdsc", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprccnc_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Includefilter", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprccnc_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filtertype", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filterisrange", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprccnc_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Includedatalist", StringUtil.BoolToStr( Ddo_servicofluxo_srvposprccnc_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Cleanfilter", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Rangefilterfrom", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Rangefilterto", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Searchbuttontext", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_ordem_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filteredtext_get", StringUtil.RTrim( Ddo_servicofluxo_ordem_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filteredtextto_get", StringUtil.RTrim( Ddo_servicofluxo_ordem_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_servicofluxo_srvpossigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filteredtext_get", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filteredtextto_get", StringUtil.RTrim( Ddo_servicofluxo_srvposprctmp_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filteredtext_get", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filteredtextto_get", StringUtil.RTrim( Ddo_servicofluxo_srvposprcpgm_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Activeeventkey", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filteredtext_get", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filteredtextto_get", StringUtil.RTrim( Ddo_servicofluxo_srvposprccnc_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormL72( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("servicoservicofluxo.js", "?20205299315736");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ServicoServicoFluxo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servico Servico Fluxo" ;
      }

      protected void WBL70( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "servicoservicofluxo.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_L72( true) ;
         }
         else
         {
            wb_table1_2_L72( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_L72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_ServicoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_ServicoCod_Jsonclick, 0, "Attribute", "", "", "", edtServicoFluxo_ServicoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21TFServicoFluxo_Ordem), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV21TFServicoFluxo_Ordem), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,29);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_ordem_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_ordem_Visible, 1, 0, "text", "", 50, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ServicoServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_ordem_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22TFServicoFluxo_Ordem_To), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22TFServicoFluxo_Ordem_To), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_ordem_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_ordem_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ServicoServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_srvpossigla_Internalname, StringUtil.RTrim( AV25TFServicoFluxo_SrvPosSigla), StringUtil.RTrim( context.localUtil.Format( AV25TFServicoFluxo_SrvPosSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,31);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_srvpossigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_srvpossigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_srvpossigla_sel_Internalname, StringUtil.RTrim( AV26TFServicoFluxo_SrvPosSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV26TFServicoFluxo_SrvPosSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,32);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_srvpossigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_srvpossigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_srvposprctmp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29TFServicoFluxo_SrvPosPrcTmp), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29TFServicoFluxo_SrvPosPrcTmp), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,33);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_srvposprctmp_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_srvposprctmp_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_srvposprctmp_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30TFServicoFluxo_SrvPosPrcTmp_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV30TFServicoFluxo_SrvPosPrcTmp_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,34);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_srvposprctmp_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_srvposprctmp_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_srvposprcpgm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33TFServicoFluxo_SrvPosPrcPgm), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33TFServicoFluxo_SrvPosPrcPgm), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,35);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_srvposprcpgm_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_srvposprcpgm_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_srvposprcpgm_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34TFServicoFluxo_SrvPosPrcPgm_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34TFServicoFluxo_SrvPosPrcPgm_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,36);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_srvposprcpgm_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_srvposprcpgm_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_srvposprccnc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFServicoFluxo_SrvPosPrcCnc), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37TFServicoFluxo_SrvPosPrcCnc), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_srvposprccnc_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_srvposprccnc_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoServicoFluxo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicofluxo_srvposprccnc_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFServicoFluxo_SrvPosPrcCnc_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38TFServicoFluxo_SrvPosPrcCnc_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicofluxo_srvposprccnc_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicofluxo_srvposprccnc_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SERVICOFLUXO_ORDEMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Internalname, AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", 0, edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ServicoServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Internalname, AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ServicoServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMPContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_srvposprctmptitlecontrolidtoreplace_Internalname, AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", 0, edtavDdo_servicofluxo_srvposprctmptitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ServicoServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_srvposprcpgmtitlecontrolidtoreplace_Internalname, AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", 0, edtavDdo_servicofluxo_srvposprcpgmtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ServicoServicoFluxo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNCContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_16_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicofluxo_srvposprccnctitlecontrolidtoreplace_Internalname, AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", 0, edtavDdo_servicofluxo_srvposprccnctitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ServicoServicoFluxo.htm");
         }
         wbLoad = true;
      }

      protected void STARTL72( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Servico Servico Fluxo", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPL70( ) ;
            }
         }
      }

      protected void WSL72( )
      {
         STARTL72( ) ;
         EVTL72( ) ;
      }

      protected void EVTL72( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_ORDEM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11L72 */
                                    E11L72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_SRVPOSSIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12L72 */
                                    E12L72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_SRVPOSPRCTMP.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13L72 */
                                    E13L72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_SRVPOSPRCPGM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14L72 */
                                    E14L72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOFLUXO_SRVPOSPRCCNC.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15L72 */
                                    E15L72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16L72 */
                                    E16L72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavTfservicofluxo_ordem_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL70( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 12), "'DOMODIFICA'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DODELETE'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 12), "'DOMODIFICA'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DODELETE'") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL70( ) ;
                              }
                              nGXsfl_16_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_16_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_16_idx), 4, 0)), 4, "0");
                              SubsflControlProps_162( ) ;
                              AV19Modifica = cgiGet( edtavModifica_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavModifica_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV19Modifica)) ? AV44Modifica_GXI : context.convertURL( context.PathToRelativeUrl( AV19Modifica))));
                              AV17Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)) ? AV45Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV17Delete))));
                              A1528ServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_Codigo_Internalname), ",", "."));
                              A1526ServicoFluxo_ServicoPos = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_ServicoPos_Internalname), ",", "."));
                              n1526ServicoFluxo_ServicoPos = false;
                              A1532ServicoFluxo_Ordem = (short)(context.localUtil.CToN( cgiGet( edtServicoFluxo_Ordem_Internalname), ",", "."));
                              n1532ServicoFluxo_Ordem = false;
                              A1527ServicoFluxo_SrvPosSigla = StringUtil.Upper( cgiGet( edtServicoFluxo_SrvPosSigla_Internalname));
                              n1527ServicoFluxo_SrvPosSigla = false;
                              A1554ServicoFluxo_SrvPosPrcTmp = (short)(context.localUtil.CToN( cgiGet( edtServicoFluxo_SrvPosPrcTmp_Internalname), ",", "."));
                              n1554ServicoFluxo_SrvPosPrcTmp = false;
                              A1555ServicoFluxo_SrvPosPrcPgm = (short)(context.localUtil.CToN( cgiGet( edtServicoFluxo_SrvPosPrcPgm_Internalname), ",", "."));
                              n1555ServicoFluxo_SrvPosPrcPgm = false;
                              A1556ServicoFluxo_SrvPosPrcCnc = (short)(context.localUtil.CToN( cgiGet( edtServicoFluxo_SrvPosPrcCnc_Internalname), ",", "."));
                              n1556ServicoFluxo_SrvPosPrcCnc = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTfservicofluxo_ordem_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17L72 */
                                          E17L72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTfservicofluxo_ordem_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18L72 */
                                          E18L72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTfservicofluxo_ordem_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19L72 */
                                          E19L72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOMODIFICA'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTfservicofluxo_ordem_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E20L72 */
                                          E20L72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTfservicofluxo_ordem_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E21L72 */
                                          E21L72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Tfservicofluxo_ordem Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_ORDEM"), ",", ".") != Convert.ToDecimal( AV21TFServicoFluxo_Ordem )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicofluxo_ordem_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_ORDEM_TO"), ",", ".") != Convert.ToDecimal( AV22TFServicoFluxo_Ordem_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicofluxo_srvpossigla Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSSIGLA"), AV25TFServicoFluxo_SrvPosSigla) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicofluxo_srvpossigla_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSSIGLA_SEL"), AV26TFServicoFluxo_SrvPosSigla_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicofluxo_srvposprctmp Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCTMP"), ",", ".") != Convert.ToDecimal( AV29TFServicoFluxo_SrvPosPrcTmp )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicofluxo_srvposprctmp_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCTMP_TO"), ",", ".") != Convert.ToDecimal( AV30TFServicoFluxo_SrvPosPrcTmp_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicofluxo_srvposprcpgm Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCPGM"), ",", ".") != Convert.ToDecimal( AV33TFServicoFluxo_SrvPosPrcPgm )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicofluxo_srvposprcpgm_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCPGM_TO"), ",", ".") != Convert.ToDecimal( AV34TFServicoFluxo_SrvPosPrcPgm_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicofluxo_srvposprccnc Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCCNC"), ",", ".") != Convert.ToDecimal( AV37TFServicoFluxo_SrvPosPrcCnc )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicofluxo_srvposprccnc_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCCNC_TO"), ",", ".") != Convert.ToDecimal( AV38TFServicoFluxo_SrvPosPrcCnc_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTfservicofluxo_ordem_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPL70( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTfservicofluxo_ordem_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEL72( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormL72( ) ;
            }
         }
      }

      protected void PAL72( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavTfservicofluxo_ordem_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_162( ) ;
         while ( nGXsfl_16_idx <= nRC_GXsfl_16 )
         {
            sendrow_162( ) ;
            nGXsfl_16_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_16_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_16_idx+1));
            sGXsfl_16_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_16_idx), 4, 0)), 4, "0");
            SubsflControlProps_162( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV21TFServicoFluxo_Ordem ,
                                       short AV22TFServicoFluxo_Ordem_To ,
                                       String AV25TFServicoFluxo_SrvPosSigla ,
                                       String AV26TFServicoFluxo_SrvPosSigla_Sel ,
                                       short AV29TFServicoFluxo_SrvPosPrcTmp ,
                                       short AV30TFServicoFluxo_SrvPosPrcTmp_To ,
                                       short AV33TFServicoFluxo_SrvPosPrcPgm ,
                                       short AV34TFServicoFluxo_SrvPosPrcPgm_To ,
                                       short AV37TFServicoFluxo_SrvPosPrcCnc ,
                                       short AV38TFServicoFluxo_SrvPosPrcCnc_To ,
                                       int AV7ServicoFluxo_ServicoCod ,
                                       String AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace ,
                                       String AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace ,
                                       String AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace ,
                                       String AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace ,
                                       String AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace ,
                                       int A155Servico_Codigo ,
                                       String A605Servico_Sigla ,
                                       String AV46Pgmname ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFL72( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOFLUXO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_SERVICOPOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOFLUXO_SERVICOPOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFL72( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV46Pgmname = "ServicoServicoFluxo";
         context.Gx_err = 0;
      }

      protected void RFL72( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 16;
         /* Execute user event: E18L72 */
         E18L72 ();
         nGXsfl_16_idx = 1;
         sGXsfl_16_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_16_idx), 4, 0)), 4, "0");
         SubsflControlProps_162( ) ;
         nGXsfl_16_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_162( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV21TFServicoFluxo_Ordem ,
                                                 AV22TFServicoFluxo_Ordem_To ,
                                                 AV26TFServicoFluxo_SrvPosSigla_Sel ,
                                                 AV25TFServicoFluxo_SrvPosSigla ,
                                                 AV29TFServicoFluxo_SrvPosPrcTmp ,
                                                 AV30TFServicoFluxo_SrvPosPrcTmp_To ,
                                                 AV33TFServicoFluxo_SrvPosPrcPgm ,
                                                 AV34TFServicoFluxo_SrvPosPrcPgm_To ,
                                                 AV37TFServicoFluxo_SrvPosPrcCnc ,
                                                 AV38TFServicoFluxo_SrvPosPrcCnc_To ,
                                                 A1532ServicoFluxo_Ordem ,
                                                 A1527ServicoFluxo_SrvPosSigla ,
                                                 A1554ServicoFluxo_SrvPosPrcTmp ,
                                                 A1555ServicoFluxo_SrvPosPrcPgm ,
                                                 A1556ServicoFluxo_SrvPosPrcCnc ,
                                                 AV7ServicoFluxo_ServicoCod ,
                                                 A1522ServicoFluxo_ServicoCod },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV25TFServicoFluxo_SrvPosSigla = StringUtil.PadR( StringUtil.RTrim( AV25TFServicoFluxo_SrvPosSigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFServicoFluxo_SrvPosSigla", AV25TFServicoFluxo_SrvPosSigla);
            /* Using cursor H00L72 */
            pr_default.execute(0, new Object[] {AV7ServicoFluxo_ServicoCod, AV21TFServicoFluxo_Ordem, AV22TFServicoFluxo_Ordem_To, lV25TFServicoFluxo_SrvPosSigla, AV26TFServicoFluxo_SrvPosSigla_Sel, AV29TFServicoFluxo_SrvPosPrcTmp, AV30TFServicoFluxo_SrvPosPrcTmp_To, AV33TFServicoFluxo_SrvPosPrcPgm, AV34TFServicoFluxo_SrvPosPrcPgm_To, AV37TFServicoFluxo_SrvPosPrcCnc, AV38TFServicoFluxo_SrvPosPrcCnc_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_16_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1522ServicoFluxo_ServicoCod = H00L72_A1522ServicoFluxo_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
               A1556ServicoFluxo_SrvPosPrcCnc = H00L72_A1556ServicoFluxo_SrvPosPrcCnc[0];
               n1556ServicoFluxo_SrvPosPrcCnc = H00L72_n1556ServicoFluxo_SrvPosPrcCnc[0];
               A1555ServicoFluxo_SrvPosPrcPgm = H00L72_A1555ServicoFluxo_SrvPosPrcPgm[0];
               n1555ServicoFluxo_SrvPosPrcPgm = H00L72_n1555ServicoFluxo_SrvPosPrcPgm[0];
               A1554ServicoFluxo_SrvPosPrcTmp = H00L72_A1554ServicoFluxo_SrvPosPrcTmp[0];
               n1554ServicoFluxo_SrvPosPrcTmp = H00L72_n1554ServicoFluxo_SrvPosPrcTmp[0];
               A1527ServicoFluxo_SrvPosSigla = H00L72_A1527ServicoFluxo_SrvPosSigla[0];
               n1527ServicoFluxo_SrvPosSigla = H00L72_n1527ServicoFluxo_SrvPosSigla[0];
               A1532ServicoFluxo_Ordem = H00L72_A1532ServicoFluxo_Ordem[0];
               n1532ServicoFluxo_Ordem = H00L72_n1532ServicoFluxo_Ordem[0];
               A1526ServicoFluxo_ServicoPos = H00L72_A1526ServicoFluxo_ServicoPos[0];
               n1526ServicoFluxo_ServicoPos = H00L72_n1526ServicoFluxo_ServicoPos[0];
               A1528ServicoFluxo_Codigo = H00L72_A1528ServicoFluxo_Codigo[0];
               A1556ServicoFluxo_SrvPosPrcCnc = H00L72_A1556ServicoFluxo_SrvPosPrcCnc[0];
               n1556ServicoFluxo_SrvPosPrcCnc = H00L72_n1556ServicoFluxo_SrvPosPrcCnc[0];
               A1555ServicoFluxo_SrvPosPrcPgm = H00L72_A1555ServicoFluxo_SrvPosPrcPgm[0];
               n1555ServicoFluxo_SrvPosPrcPgm = H00L72_n1555ServicoFluxo_SrvPosPrcPgm[0];
               A1554ServicoFluxo_SrvPosPrcTmp = H00L72_A1554ServicoFluxo_SrvPosPrcTmp[0];
               n1554ServicoFluxo_SrvPosPrcTmp = H00L72_n1554ServicoFluxo_SrvPosPrcTmp[0];
               A1527ServicoFluxo_SrvPosSigla = H00L72_A1527ServicoFluxo_SrvPosSigla[0];
               n1527ServicoFluxo_SrvPosSigla = H00L72_n1527ServicoFluxo_SrvPosSigla[0];
               /* Execute user event: E19L72 */
               E19L72 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 16;
            WBL70( ) ;
         }
         nGXsfl_16_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV21TFServicoFluxo_Ordem ,
                                              AV22TFServicoFluxo_Ordem_To ,
                                              AV26TFServicoFluxo_SrvPosSigla_Sel ,
                                              AV25TFServicoFluxo_SrvPosSigla ,
                                              AV29TFServicoFluxo_SrvPosPrcTmp ,
                                              AV30TFServicoFluxo_SrvPosPrcTmp_To ,
                                              AV33TFServicoFluxo_SrvPosPrcPgm ,
                                              AV34TFServicoFluxo_SrvPosPrcPgm_To ,
                                              AV37TFServicoFluxo_SrvPosPrcCnc ,
                                              AV38TFServicoFluxo_SrvPosPrcCnc_To ,
                                              A1532ServicoFluxo_Ordem ,
                                              A1527ServicoFluxo_SrvPosSigla ,
                                              A1554ServicoFluxo_SrvPosPrcTmp ,
                                              A1555ServicoFluxo_SrvPosPrcPgm ,
                                              A1556ServicoFluxo_SrvPosPrcCnc ,
                                              AV7ServicoFluxo_ServicoCod ,
                                              A1522ServicoFluxo_ServicoCod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV25TFServicoFluxo_SrvPosSigla = StringUtil.PadR( StringUtil.RTrim( AV25TFServicoFluxo_SrvPosSigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFServicoFluxo_SrvPosSigla", AV25TFServicoFluxo_SrvPosSigla);
         /* Using cursor H00L73 */
         pr_default.execute(1, new Object[] {AV7ServicoFluxo_ServicoCod, AV21TFServicoFluxo_Ordem, AV22TFServicoFluxo_Ordem_To, lV25TFServicoFluxo_SrvPosSigla, AV26TFServicoFluxo_SrvPosSigla_Sel, AV29TFServicoFluxo_SrvPosPrcTmp, AV30TFServicoFluxo_SrvPosPrcTmp_To, AV33TFServicoFluxo_SrvPosPrcPgm, AV34TFServicoFluxo_SrvPosPrcPgm_To, AV37TFServicoFluxo_SrvPosPrcCnc, AV38TFServicoFluxo_SrvPosPrcCnc_To});
         GRID_nRecordCount = H00L73_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV21TFServicoFluxo_Ordem, AV22TFServicoFluxo_Ordem_To, AV25TFServicoFluxo_SrvPosSigla, AV26TFServicoFluxo_SrvPosSigla_Sel, AV29TFServicoFluxo_SrvPosPrcTmp, AV30TFServicoFluxo_SrvPosPrcTmp_To, AV33TFServicoFluxo_SrvPosPrcPgm, AV34TFServicoFluxo_SrvPosPrcPgm_To, AV37TFServicoFluxo_SrvPosPrcCnc, AV38TFServicoFluxo_SrvPosPrcCnc_To, AV7ServicoFluxo_ServicoCod, AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace, AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace, AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace, A155Servico_Codigo, A605Servico_Sigla, AV46Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV21TFServicoFluxo_Ordem, AV22TFServicoFluxo_Ordem_To, AV25TFServicoFluxo_SrvPosSigla, AV26TFServicoFluxo_SrvPosSigla_Sel, AV29TFServicoFluxo_SrvPosPrcTmp, AV30TFServicoFluxo_SrvPosPrcTmp_To, AV33TFServicoFluxo_SrvPosPrcPgm, AV34TFServicoFluxo_SrvPosPrcPgm_To, AV37TFServicoFluxo_SrvPosPrcCnc, AV38TFServicoFluxo_SrvPosPrcCnc_To, AV7ServicoFluxo_ServicoCod, AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace, AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace, AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace, A155Servico_Codigo, A605Servico_Sigla, AV46Pgmname, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV21TFServicoFluxo_Ordem, AV22TFServicoFluxo_Ordem_To, AV25TFServicoFluxo_SrvPosSigla, AV26TFServicoFluxo_SrvPosSigla_Sel, AV29TFServicoFluxo_SrvPosPrcTmp, AV30TFServicoFluxo_SrvPosPrcTmp_To, AV33TFServicoFluxo_SrvPosPrcPgm, AV34TFServicoFluxo_SrvPosPrcPgm_To, AV37TFServicoFluxo_SrvPosPrcCnc, AV38TFServicoFluxo_SrvPosPrcCnc_To, AV7ServicoFluxo_ServicoCod, AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace, AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace, AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace, A155Servico_Codigo, A605Servico_Sigla, AV46Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV21TFServicoFluxo_Ordem, AV22TFServicoFluxo_Ordem_To, AV25TFServicoFluxo_SrvPosSigla, AV26TFServicoFluxo_SrvPosSigla_Sel, AV29TFServicoFluxo_SrvPosPrcTmp, AV30TFServicoFluxo_SrvPosPrcTmp_To, AV33TFServicoFluxo_SrvPosPrcPgm, AV34TFServicoFluxo_SrvPosPrcPgm_To, AV37TFServicoFluxo_SrvPosPrcCnc, AV38TFServicoFluxo_SrvPosPrcCnc_To, AV7ServicoFluxo_ServicoCod, AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace, AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace, AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace, A155Servico_Codigo, A605Servico_Sigla, AV46Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV21TFServicoFluxo_Ordem, AV22TFServicoFluxo_Ordem_To, AV25TFServicoFluxo_SrvPosSigla, AV26TFServicoFluxo_SrvPosSigla_Sel, AV29TFServicoFluxo_SrvPosPrcTmp, AV30TFServicoFluxo_SrvPosPrcTmp_To, AV33TFServicoFluxo_SrvPosPrcPgm, AV34TFServicoFluxo_SrvPosPrcPgm_To, AV37TFServicoFluxo_SrvPosPrcCnc, AV38TFServicoFluxo_SrvPosPrcCnc_To, AV7ServicoFluxo_ServicoCod, AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace, AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace, AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace, AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace, A155Servico_Codigo, A605Servico_Sigla, AV46Pgmname, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPL70( )
      {
         /* Before Start, stand alone formulas. */
         AV46Pgmname = "ServicoServicoFluxo";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E17L72 */
         E17L72 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV40DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSERVICOFLUXO_ORDEMTITLEFILTERDATA"), AV20ServicoFluxo_OrdemTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA"), AV24ServicoFluxo_SrvPosSiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSERVICOFLUXO_SRVPOSPRCTMPTITLEFILTERDATA"), AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSERVICOFLUXO_SRVPOSPRCPGMTITLEFILTERDATA"), AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSERVICOFLUXO_SRVPOSPRCCNCTITLEFILTERDATA"), AV36ServicoFluxo_SrvPosPrcCncTitleFilterData);
            /* Read variables values. */
            A1522ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_ServicoCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
            A1522ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_ServicoCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_ORDEM");
               GX_FocusControl = edtavTfservicofluxo_ordem_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21TFServicoFluxo_Ordem = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFServicoFluxo_Ordem), 3, 0)));
            }
            else
            {
               AV21TFServicoFluxo_Ordem = (short)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFServicoFluxo_Ordem), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_to_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_ORDEM_TO");
               GX_FocusControl = edtavTfservicofluxo_ordem_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22TFServicoFluxo_Ordem_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFServicoFluxo_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFServicoFluxo_Ordem_To), 3, 0)));
            }
            else
            {
               AV22TFServicoFluxo_Ordem_To = (short)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_ordem_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFServicoFluxo_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFServicoFluxo_Ordem_To), 3, 0)));
            }
            AV25TFServicoFluxo_SrvPosSigla = StringUtil.Upper( cgiGet( edtavTfservicofluxo_srvpossigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFServicoFluxo_SrvPosSigla", AV25TFServicoFluxo_SrvPosSigla);
            AV26TFServicoFluxo_SrvPosSigla_Sel = StringUtil.Upper( cgiGet( edtavTfservicofluxo_srvpossigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFServicoFluxo_SrvPosSigla_Sel", AV26TFServicoFluxo_SrvPosSigla_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprctmp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprctmp_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_SRVPOSPRCTMP");
               GX_FocusControl = edtavTfservicofluxo_srvposprctmp_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29TFServicoFluxo_SrvPosPrcTmp = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFServicoFluxo_SrvPosPrcTmp), 4, 0)));
            }
            else
            {
               AV29TFServicoFluxo_SrvPosPrcTmp = (short)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprctmp_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFServicoFluxo_SrvPosPrcTmp), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprctmp_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprctmp_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_SRVPOSPRCTMP_TO");
               GX_FocusControl = edtavTfservicofluxo_srvposprctmp_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30TFServicoFluxo_SrvPosPrcTmp_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFServicoFluxo_SrvPosPrcTmp_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFServicoFluxo_SrvPosPrcTmp_To), 4, 0)));
            }
            else
            {
               AV30TFServicoFluxo_SrvPosPrcTmp_To = (short)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprctmp_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFServicoFluxo_SrvPosPrcTmp_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFServicoFluxo_SrvPosPrcTmp_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprcpgm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprcpgm_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_SRVPOSPRCPGM");
               GX_FocusControl = edtavTfservicofluxo_srvposprcpgm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33TFServicoFluxo_SrvPosPrcPgm = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFServicoFluxo_SrvPosPrcPgm), 4, 0)));
            }
            else
            {
               AV33TFServicoFluxo_SrvPosPrcPgm = (short)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprcpgm_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFServicoFluxo_SrvPosPrcPgm), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprcpgm_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprcpgm_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_SRVPOSPRCPGM_TO");
               GX_FocusControl = edtavTfservicofluxo_srvposprcpgm_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34TFServicoFluxo_SrvPosPrcPgm_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFServicoFluxo_SrvPosPrcPgm_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFServicoFluxo_SrvPosPrcPgm_To), 4, 0)));
            }
            else
            {
               AV34TFServicoFluxo_SrvPosPrcPgm_To = (short)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprcpgm_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFServicoFluxo_SrvPosPrcPgm_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFServicoFluxo_SrvPosPrcPgm_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprccnc_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprccnc_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_SRVPOSPRCCNC");
               GX_FocusControl = edtavTfservicofluxo_srvposprccnc_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFServicoFluxo_SrvPosPrcCnc = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFServicoFluxo_SrvPosPrcCnc), 4, 0)));
            }
            else
            {
               AV37TFServicoFluxo_SrvPosPrcCnc = (short)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprccnc_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFServicoFluxo_SrvPosPrcCnc), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprccnc_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprccnc_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICOFLUXO_SRVPOSPRCCNC_TO");
               GX_FocusControl = edtavTfservicofluxo_srvposprccnc_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFServicoFluxo_SrvPosPrcCnc_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFServicoFluxo_SrvPosPrcCnc_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFServicoFluxo_SrvPosPrcCnc_To), 4, 0)));
            }
            else
            {
               AV38TFServicoFluxo_SrvPosPrcCnc_To = (short)(context.localUtil.CToN( cgiGet( edtavTfservicofluxo_srvposprccnc_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFServicoFluxo_SrvPosPrcCnc_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFServicoFluxo_SrvPosPrcCnc_To), 4, 0)));
            }
            AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace", AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace);
            AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace", AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace);
            AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_srvposprctmptitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace", AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace);
            AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_srvposprcpgmtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace", AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace);
            AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace = cgiGet( edtavDdo_servicofluxo_srvposprccnctitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace", AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_16 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_16"), ",", "."));
            wcpOAV7ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ServicoFluxo_ServicoCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_servicofluxo_ordem_Caption = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Caption");
            Ddo_servicofluxo_ordem_Tooltip = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Tooltip");
            Ddo_servicofluxo_ordem_Cls = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Cls");
            Ddo_servicofluxo_ordem_Filteredtext_set = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filteredtext_set");
            Ddo_servicofluxo_ordem_Filteredtextto_set = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filteredtextto_set");
            Ddo_servicofluxo_ordem_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Dropdownoptionstype");
            Ddo_servicofluxo_ordem_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Titlecontrolidtoreplace");
            Ddo_servicofluxo_ordem_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Includesortasc"));
            Ddo_servicofluxo_ordem_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Includesortdsc"));
            Ddo_servicofluxo_ordem_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Includefilter"));
            Ddo_servicofluxo_ordem_Filtertype = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filtertype");
            Ddo_servicofluxo_ordem_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filterisrange"));
            Ddo_servicofluxo_ordem_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Includedatalist"));
            Ddo_servicofluxo_ordem_Cleanfilter = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Cleanfilter");
            Ddo_servicofluxo_ordem_Rangefilterfrom = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Rangefilterfrom");
            Ddo_servicofluxo_ordem_Rangefilterto = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Rangefilterto");
            Ddo_servicofluxo_ordem_Searchbuttontext = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Searchbuttontext");
            Ddo_servicofluxo_srvpossigla_Caption = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Caption");
            Ddo_servicofluxo_srvpossigla_Tooltip = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Tooltip");
            Ddo_servicofluxo_srvpossigla_Cls = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Cls");
            Ddo_servicofluxo_srvpossigla_Filteredtext_set = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Filteredtext_set");
            Ddo_servicofluxo_srvpossigla_Selectedvalue_set = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Selectedvalue_set");
            Ddo_servicofluxo_srvpossigla_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Dropdownoptionstype");
            Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Titlecontrolidtoreplace");
            Ddo_servicofluxo_srvpossigla_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Includesortasc"));
            Ddo_servicofluxo_srvpossigla_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Includesortdsc"));
            Ddo_servicofluxo_srvpossigla_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Includefilter"));
            Ddo_servicofluxo_srvpossigla_Filtertype = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Filtertype");
            Ddo_servicofluxo_srvpossigla_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Filterisrange"));
            Ddo_servicofluxo_srvpossigla_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Includedatalist"));
            Ddo_servicofluxo_srvpossigla_Datalisttype = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalisttype");
            Ddo_servicofluxo_srvpossigla_Datalistproc = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalistproc");
            Ddo_servicofluxo_srvpossigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicofluxo_srvpossigla_Loadingdata = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Loadingdata");
            Ddo_servicofluxo_srvpossigla_Cleanfilter = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Cleanfilter");
            Ddo_servicofluxo_srvpossigla_Noresultsfound = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Noresultsfound");
            Ddo_servicofluxo_srvpossigla_Searchbuttontext = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Searchbuttontext");
            Ddo_servicofluxo_srvposprctmp_Caption = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Caption");
            Ddo_servicofluxo_srvposprctmp_Tooltip = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Tooltip");
            Ddo_servicofluxo_srvposprctmp_Cls = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Cls");
            Ddo_servicofluxo_srvposprctmp_Filteredtext_set = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filteredtext_set");
            Ddo_servicofluxo_srvposprctmp_Filteredtextto_set = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filteredtextto_set");
            Ddo_servicofluxo_srvposprctmp_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Dropdownoptionstype");
            Ddo_servicofluxo_srvposprctmp_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Titlecontrolidtoreplace");
            Ddo_servicofluxo_srvposprctmp_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Includesortasc"));
            Ddo_servicofluxo_srvposprctmp_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Includesortdsc"));
            Ddo_servicofluxo_srvposprctmp_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Includefilter"));
            Ddo_servicofluxo_srvposprctmp_Filtertype = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filtertype");
            Ddo_servicofluxo_srvposprctmp_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filterisrange"));
            Ddo_servicofluxo_srvposprctmp_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Includedatalist"));
            Ddo_servicofluxo_srvposprctmp_Cleanfilter = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Cleanfilter");
            Ddo_servicofluxo_srvposprctmp_Rangefilterfrom = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Rangefilterfrom");
            Ddo_servicofluxo_srvposprctmp_Rangefilterto = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Rangefilterto");
            Ddo_servicofluxo_srvposprctmp_Searchbuttontext = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Searchbuttontext");
            Ddo_servicofluxo_srvposprcpgm_Caption = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Caption");
            Ddo_servicofluxo_srvposprcpgm_Tooltip = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Tooltip");
            Ddo_servicofluxo_srvposprcpgm_Cls = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Cls");
            Ddo_servicofluxo_srvposprcpgm_Filteredtext_set = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filteredtext_set");
            Ddo_servicofluxo_srvposprcpgm_Filteredtextto_set = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filteredtextto_set");
            Ddo_servicofluxo_srvposprcpgm_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Dropdownoptionstype");
            Ddo_servicofluxo_srvposprcpgm_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Titlecontrolidtoreplace");
            Ddo_servicofluxo_srvposprcpgm_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Includesortasc"));
            Ddo_servicofluxo_srvposprcpgm_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Includesortdsc"));
            Ddo_servicofluxo_srvposprcpgm_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Includefilter"));
            Ddo_servicofluxo_srvposprcpgm_Filtertype = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filtertype");
            Ddo_servicofluxo_srvposprcpgm_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filterisrange"));
            Ddo_servicofluxo_srvposprcpgm_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Includedatalist"));
            Ddo_servicofluxo_srvposprcpgm_Cleanfilter = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Cleanfilter");
            Ddo_servicofluxo_srvposprcpgm_Rangefilterfrom = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Rangefilterfrom");
            Ddo_servicofluxo_srvposprcpgm_Rangefilterto = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Rangefilterto");
            Ddo_servicofluxo_srvposprcpgm_Searchbuttontext = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Searchbuttontext");
            Ddo_servicofluxo_srvposprccnc_Caption = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Caption");
            Ddo_servicofluxo_srvposprccnc_Tooltip = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Tooltip");
            Ddo_servicofluxo_srvposprccnc_Cls = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Cls");
            Ddo_servicofluxo_srvposprccnc_Filteredtext_set = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filteredtext_set");
            Ddo_servicofluxo_srvposprccnc_Filteredtextto_set = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filteredtextto_set");
            Ddo_servicofluxo_srvposprccnc_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Dropdownoptionstype");
            Ddo_servicofluxo_srvposprccnc_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Titlecontrolidtoreplace");
            Ddo_servicofluxo_srvposprccnc_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Includesortasc"));
            Ddo_servicofluxo_srvposprccnc_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Includesortdsc"));
            Ddo_servicofluxo_srvposprccnc_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Includefilter"));
            Ddo_servicofluxo_srvposprccnc_Filtertype = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filtertype");
            Ddo_servicofluxo_srvposprccnc_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filterisrange"));
            Ddo_servicofluxo_srvposprccnc_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Includedatalist"));
            Ddo_servicofluxo_srvposprccnc_Cleanfilter = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Cleanfilter");
            Ddo_servicofluxo_srvposprccnc_Rangefilterfrom = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Rangefilterfrom");
            Ddo_servicofluxo_srvposprccnc_Rangefilterto = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Rangefilterto");
            Ddo_servicofluxo_srvposprccnc_Searchbuttontext = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Searchbuttontext");
            Ddo_servicofluxo_ordem_Activeeventkey = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Activeeventkey");
            Ddo_servicofluxo_ordem_Filteredtext_get = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filteredtext_get");
            Ddo_servicofluxo_ordem_Filteredtextto_get = cgiGet( sPrefix+"DDO_SERVICOFLUXO_ORDEM_Filteredtextto_get");
            Ddo_servicofluxo_srvpossigla_Activeeventkey = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Activeeventkey");
            Ddo_servicofluxo_srvpossigla_Filteredtext_get = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Filteredtext_get");
            Ddo_servicofluxo_srvpossigla_Selectedvalue_get = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA_Selectedvalue_get");
            Ddo_servicofluxo_srvposprctmp_Activeeventkey = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Activeeventkey");
            Ddo_servicofluxo_srvposprctmp_Filteredtext_get = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filteredtext_get");
            Ddo_servicofluxo_srvposprctmp_Filteredtextto_get = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP_Filteredtextto_get");
            Ddo_servicofluxo_srvposprcpgm_Activeeventkey = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Activeeventkey");
            Ddo_servicofluxo_srvposprcpgm_Filteredtext_get = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filteredtext_get");
            Ddo_servicofluxo_srvposprcpgm_Filteredtextto_get = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM_Filteredtextto_get");
            Ddo_servicofluxo_srvposprccnc_Activeeventkey = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Activeeventkey");
            Ddo_servicofluxo_srvposprccnc_Filteredtext_get = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filteredtext_get");
            Ddo_servicofluxo_srvposprccnc_Filteredtextto_get = cgiGet( sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_ORDEM"), ",", ".") != Convert.ToDecimal( AV21TFServicoFluxo_Ordem )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_ORDEM_TO"), ",", ".") != Convert.ToDecimal( AV22TFServicoFluxo_Ordem_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSSIGLA"), AV25TFServicoFluxo_SrvPosSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSSIGLA_SEL"), AV26TFServicoFluxo_SrvPosSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCTMP"), ",", ".") != Convert.ToDecimal( AV29TFServicoFluxo_SrvPosPrcTmp )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCTMP_TO"), ",", ".") != Convert.ToDecimal( AV30TFServicoFluxo_SrvPosPrcTmp_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCPGM"), ",", ".") != Convert.ToDecimal( AV33TFServicoFluxo_SrvPosPrcPgm )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCPGM_TO"), ",", ".") != Convert.ToDecimal( AV34TFServicoFluxo_SrvPosPrcPgm_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCCNC"), ",", ".") != Convert.ToDecimal( AV37TFServicoFluxo_SrvPosPrcCnc )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICOFLUXO_SRVPOSPRCCNC_TO"), ",", ".") != Convert.ToDecimal( AV38TFServicoFluxo_SrvPosPrcCnc_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E17L72 */
         E17L72 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17L72( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfservicofluxo_ordem_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicofluxo_ordem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_ordem_Visible), 5, 0)));
         edtavTfservicofluxo_ordem_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicofluxo_ordem_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_ordem_to_Visible), 5, 0)));
         edtavTfservicofluxo_srvpossigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicofluxo_srvpossigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_srvpossigla_Visible), 5, 0)));
         edtavTfservicofluxo_srvpossigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicofluxo_srvpossigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_srvpossigla_sel_Visible), 5, 0)));
         edtavTfservicofluxo_srvposprctmp_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicofluxo_srvposprctmp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_srvposprctmp_Visible), 5, 0)));
         edtavTfservicofluxo_srvposprctmp_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicofluxo_srvposprctmp_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_srvposprctmp_to_Visible), 5, 0)));
         edtavTfservicofluxo_srvposprcpgm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicofluxo_srvposprcpgm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_srvposprcpgm_Visible), 5, 0)));
         edtavTfservicofluxo_srvposprcpgm_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicofluxo_srvposprcpgm_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_srvposprcpgm_to_Visible), 5, 0)));
         edtavTfservicofluxo_srvposprccnc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicofluxo_srvposprccnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_srvposprccnc_Visible), 5, 0)));
         edtavTfservicofluxo_srvposprccnc_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicofluxo_srvposprccnc_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicofluxo_srvposprccnc_to_Visible), 5, 0)));
         Ddo_servicofluxo_ordem_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_Ordem";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_ordem_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_ordem_Titlecontrolidtoreplace);
         AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace = Ddo_servicofluxo_ordem_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace", AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace);
         edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_SrvPosSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvpossigla_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace);
         AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace = Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace", AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace);
         edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicofluxo_srvposprctmp_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_SrvPosPrcTmp";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvposprctmp_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_srvposprctmp_Titlecontrolidtoreplace);
         AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace = Ddo_servicofluxo_srvposprctmp_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace", AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace);
         edtavDdo_servicofluxo_srvposprctmptitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_servicofluxo_srvposprctmptitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_srvposprctmptitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicofluxo_srvposprcpgm_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_SrvPosPrcPgm";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvposprcpgm_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_srvposprcpgm_Titlecontrolidtoreplace);
         AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace = Ddo_servicofluxo_srvposprcpgm_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace", AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace);
         edtavDdo_servicofluxo_srvposprcpgmtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_servicofluxo_srvposprcpgmtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_srvposprcpgmtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicofluxo_srvposprccnc_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoFluxo_SrvPosPrcCnc";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvposprccnc_Internalname, "TitleControlIdToReplace", Ddo_servicofluxo_srvposprccnc_Titlecontrolidtoreplace);
         AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace = Ddo_servicofluxo_srvposprccnc_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace", AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace);
         edtavDdo_servicofluxo_srvposprccnctitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_servicofluxo_srvposprccnctitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicofluxo_srvposprccnctitlecontrolidtoreplace_Visible), 5, 0)));
         edtServicoFluxo_ServicoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_ServicoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_ServicoCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV40DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV40DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E18L72( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV20ServicoFluxo_OrdemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ServicoFluxo_SrvPosSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36ServicoFluxo_SrvPosPrcCncTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServicoFluxo_Ordem_Titleformat = 2;
         edtServicoFluxo_Ordem_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ordem", AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_Ordem_Internalname, "Title", edtServicoFluxo_Ordem_Title);
         edtServicoFluxo_SrvPosSigla_Titleformat = 2;
         edtServicoFluxo_SrvPosSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "A��o", AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_SrvPosSigla_Internalname, "Title", edtServicoFluxo_SrvPosSigla_Title);
         edtServicoFluxo_SrvPosPrcTmp_Titleformat = 2;
         edtServicoFluxo_SrvPosPrcTmp_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "% Tempo", AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_SrvPosPrcTmp_Internalname, "Title", edtServicoFluxo_SrvPosPrcTmp_Title);
         edtServicoFluxo_SrvPosPrcPgm_Titleformat = 2;
         edtServicoFluxo_SrvPosPrcPgm_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "% Pagamento", AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_SrvPosPrcPgm_Internalname, "Title", edtServicoFluxo_SrvPosPrcPgm_Title);
         edtServicoFluxo_SrvPosPrcCnc_Titleformat = 2;
         edtServicoFluxo_SrvPosPrcCnc_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "% Cancelamento", AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_SrvPosPrcCnc_Internalname, "Title", edtServicoFluxo_SrvPosPrcCnc_Title);
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavModifica_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavModifica_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavModifica_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         /* Using cursor H00L74 */
         pr_default.execute(2, new Object[] {AV7ServicoFluxo_ServicoCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A155Servico_Codigo = H00L74_A155Servico_Codigo[0];
            A605Servico_Sigla = H00L74_A605Servico_Sigla[0];
            AV18ServicoFluxo_ServicoSigla = A605Servico_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ServicoFluxo_ServicoSigla", AV18ServicoFluxo_ServicoSigla);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV20ServicoFluxo_OrdemTitleFilterData", AV20ServicoFluxo_OrdemTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV24ServicoFluxo_SrvPosSiglaTitleFilterData", AV24ServicoFluxo_SrvPosSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData", AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData", AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV36ServicoFluxo_SrvPosPrcCncTitleFilterData", AV36ServicoFluxo_SrvPosPrcCncTitleFilterData);
      }

      protected void E11L72( )
      {
         /* Ddo_servicofluxo_ordem_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_ordem_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV21TFServicoFluxo_Ordem = (short)(NumberUtil.Val( Ddo_servicofluxo_ordem_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFServicoFluxo_Ordem), 3, 0)));
            AV22TFServicoFluxo_Ordem_To = (short)(NumberUtil.Val( Ddo_servicofluxo_ordem_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFServicoFluxo_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFServicoFluxo_Ordem_To), 3, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E12L72( )
      {
         /* Ddo_servicofluxo_srvpossigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_srvpossigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV25TFServicoFluxo_SrvPosSigla = Ddo_servicofluxo_srvpossigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFServicoFluxo_SrvPosSigla", AV25TFServicoFluxo_SrvPosSigla);
            AV26TFServicoFluxo_SrvPosSigla_Sel = Ddo_servicofluxo_srvpossigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFServicoFluxo_SrvPosSigla_Sel", AV26TFServicoFluxo_SrvPosSigla_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E13L72( )
      {
         /* Ddo_servicofluxo_srvposprctmp_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_srvposprctmp_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV29TFServicoFluxo_SrvPosPrcTmp = (short)(NumberUtil.Val( Ddo_servicofluxo_srvposprctmp_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFServicoFluxo_SrvPosPrcTmp), 4, 0)));
            AV30TFServicoFluxo_SrvPosPrcTmp_To = (short)(NumberUtil.Val( Ddo_servicofluxo_srvposprctmp_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFServicoFluxo_SrvPosPrcTmp_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFServicoFluxo_SrvPosPrcTmp_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E14L72( )
      {
         /* Ddo_servicofluxo_srvposprcpgm_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_srvposprcpgm_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV33TFServicoFluxo_SrvPosPrcPgm = (short)(NumberUtil.Val( Ddo_servicofluxo_srvposprcpgm_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFServicoFluxo_SrvPosPrcPgm), 4, 0)));
            AV34TFServicoFluxo_SrvPosPrcPgm_To = (short)(NumberUtil.Val( Ddo_servicofluxo_srvposprcpgm_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFServicoFluxo_SrvPosPrcPgm_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFServicoFluxo_SrvPosPrcPgm_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E15L72( )
      {
         /* Ddo_servicofluxo_srvposprccnc_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicofluxo_srvposprccnc_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFServicoFluxo_SrvPosPrcCnc = (short)(NumberUtil.Val( Ddo_servicofluxo_srvposprccnc_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFServicoFluxo_SrvPosPrcCnc), 4, 0)));
            AV38TFServicoFluxo_SrvPosPrcCnc_To = (short)(NumberUtil.Val( Ddo_servicofluxo_srvposprccnc_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFServicoFluxo_SrvPosPrcCnc_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFServicoFluxo_SrvPosPrcCnc_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      private void E19L72( )
      {
         /* Grid_Load Routine */
         AV19Modifica = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavModifica_Internalname, AV19Modifica);
         AV44Modifica_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavModifica_Tooltiptext = "Modifica";
         AV17Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV17Delete);
         AV45Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 16;
         }
         sendrow_162( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_16_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(16, GridRow);
         }
      }

      protected void E16L72( )
      {
         /* 'DoInsert' Routine */
         context.PopUp(formatLink("wp_processo.aspx") + "?" + UrlEncode("" +AV7ServicoFluxo_ServicoCod) + "," + UrlEncode(StringUtil.RTrim(AV18ServicoFluxo_ServicoSigla)), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E20L72( )
      {
         /* 'DoModifica' Routine */
         context.PopUp(formatLink("servicoprocesso.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1526ServicoFluxo_ServicoPos) + "," + UrlEncode("" +A1532ServicoFluxo_Ordem), new Object[] {"A1532ServicoFluxo_Ordem"});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E21L72( )
      {
         /* 'DoDelete' Routine */
         context.PopUp(formatLink("servicofluxo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1528ServicoFluxo_Codigo), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV46Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV46Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV46Pgmname+"GridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_ORDEM") == 0 )
            {
               AV21TFServicoFluxo_Ordem = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFServicoFluxo_Ordem), 3, 0)));
               AV22TFServicoFluxo_Ordem_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFServicoFluxo_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFServicoFluxo_Ordem_To), 3, 0)));
               if ( ! (0==AV21TFServicoFluxo_Ordem) )
               {
                  Ddo_servicofluxo_ordem_Filteredtext_set = StringUtil.Str( (decimal)(AV21TFServicoFluxo_Ordem), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_ordem_Internalname, "FilteredText_set", Ddo_servicofluxo_ordem_Filteredtext_set);
               }
               if ( ! (0==AV22TFServicoFluxo_Ordem_To) )
               {
                  Ddo_servicofluxo_ordem_Filteredtextto_set = StringUtil.Str( (decimal)(AV22TFServicoFluxo_Ordem_To), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_ordem_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_ordem_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSSIGLA") == 0 )
            {
               AV25TFServicoFluxo_SrvPosSigla = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFServicoFluxo_SrvPosSigla", AV25TFServicoFluxo_SrvPosSigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFServicoFluxo_SrvPosSigla)) )
               {
                  Ddo_servicofluxo_srvpossigla_Filteredtext_set = AV25TFServicoFluxo_SrvPosSigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvpossigla_Internalname, "FilteredText_set", Ddo_servicofluxo_srvpossigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSSIGLA_SEL") == 0 )
            {
               AV26TFServicoFluxo_SrvPosSigla_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFServicoFluxo_SrvPosSigla_Sel", AV26TFServicoFluxo_SrvPosSigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFServicoFluxo_SrvPosSigla_Sel)) )
               {
                  Ddo_servicofluxo_srvpossigla_Selectedvalue_set = AV26TFServicoFluxo_SrvPosSigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvpossigla_Internalname, "SelectedValue_set", Ddo_servicofluxo_srvpossigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSPRCTMP") == 0 )
            {
               AV29TFServicoFluxo_SrvPosPrcTmp = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFServicoFluxo_SrvPosPrcTmp), 4, 0)));
               AV30TFServicoFluxo_SrvPosPrcTmp_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFServicoFluxo_SrvPosPrcTmp_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFServicoFluxo_SrvPosPrcTmp_To), 4, 0)));
               if ( ! (0==AV29TFServicoFluxo_SrvPosPrcTmp) )
               {
                  Ddo_servicofluxo_srvposprctmp_Filteredtext_set = StringUtil.Str( (decimal)(AV29TFServicoFluxo_SrvPosPrcTmp), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvposprctmp_Internalname, "FilteredText_set", Ddo_servicofluxo_srvposprctmp_Filteredtext_set);
               }
               if ( ! (0==AV30TFServicoFluxo_SrvPosPrcTmp_To) )
               {
                  Ddo_servicofluxo_srvposprctmp_Filteredtextto_set = StringUtil.Str( (decimal)(AV30TFServicoFluxo_SrvPosPrcTmp_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvposprctmp_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_srvposprctmp_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSPRCPGM") == 0 )
            {
               AV33TFServicoFluxo_SrvPosPrcPgm = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFServicoFluxo_SrvPosPrcPgm), 4, 0)));
               AV34TFServicoFluxo_SrvPosPrcPgm_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFServicoFluxo_SrvPosPrcPgm_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFServicoFluxo_SrvPosPrcPgm_To), 4, 0)));
               if ( ! (0==AV33TFServicoFluxo_SrvPosPrcPgm) )
               {
                  Ddo_servicofluxo_srvposprcpgm_Filteredtext_set = StringUtil.Str( (decimal)(AV33TFServicoFluxo_SrvPosPrcPgm), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvposprcpgm_Internalname, "FilteredText_set", Ddo_servicofluxo_srvposprcpgm_Filteredtext_set);
               }
               if ( ! (0==AV34TFServicoFluxo_SrvPosPrcPgm_To) )
               {
                  Ddo_servicofluxo_srvposprcpgm_Filteredtextto_set = StringUtil.Str( (decimal)(AV34TFServicoFluxo_SrvPosPrcPgm_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvposprcpgm_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_srvposprcpgm_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSPRCCNC") == 0 )
            {
               AV37TFServicoFluxo_SrvPosPrcCnc = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFServicoFluxo_SrvPosPrcCnc), 4, 0)));
               AV38TFServicoFluxo_SrvPosPrcCnc_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFServicoFluxo_SrvPosPrcCnc_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFServicoFluxo_SrvPosPrcCnc_To), 4, 0)));
               if ( ! (0==AV37TFServicoFluxo_SrvPosPrcCnc) )
               {
                  Ddo_servicofluxo_srvposprccnc_Filteredtext_set = StringUtil.Str( (decimal)(AV37TFServicoFluxo_SrvPosPrcCnc), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvposprccnc_Internalname, "FilteredText_set", Ddo_servicofluxo_srvposprccnc_Filteredtext_set);
               }
               if ( ! (0==AV38TFServicoFluxo_SrvPosPrcCnc_To) )
               {
                  Ddo_servicofluxo_srvposprccnc_Filteredtextto_set = StringUtil.Str( (decimal)(AV38TFServicoFluxo_SrvPosPrcCnc_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicofluxo_srvposprccnc_Internalname, "FilteredTextTo_set", Ddo_servicofluxo_srvposprccnc_Filteredtextto_set);
               }
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV46Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV21TFServicoFluxo_Ordem) && (0==AV22TFServicoFluxo_Ordem_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_ORDEM";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV21TFServicoFluxo_Ordem), 3, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV22TFServicoFluxo_Ordem_To), 3, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFServicoFluxo_SrvPosSigla)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SRVPOSSIGLA";
            AV12GridStateFilterValue.gxTpr_Value = AV25TFServicoFluxo_SrvPosSigla;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFServicoFluxo_SrvPosSigla_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SRVPOSSIGLA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV26TFServicoFluxo_SrvPosSigla_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV29TFServicoFluxo_SrvPosPrcTmp) && (0==AV30TFServicoFluxo_SrvPosPrcTmp_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SRVPOSPRCTMP";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV29TFServicoFluxo_SrvPosPrcTmp), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV30TFServicoFluxo_SrvPosPrcTmp_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV33TFServicoFluxo_SrvPosPrcPgm) && (0==AV34TFServicoFluxo_SrvPosPrcPgm_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SRVPOSPRCPGM";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV33TFServicoFluxo_SrvPosPrcPgm), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV34TFServicoFluxo_SrvPosPrcPgm_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV37TFServicoFluxo_SrvPosPrcCnc) && (0==AV38TFServicoFluxo_SrvPosPrcCnc_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOFLUXO_SRVPOSPRCCNC";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV37TFServicoFluxo_SrvPosPrcCnc), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV38TFServicoFluxo_SrvPosPrcCnc_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7ServicoFluxo_ServicoCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&SERVICOFLUXO_SERVICOCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV46Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV46Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ServicoFluxo";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ServicoFluxo_ServicoCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_L72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_L72( true) ;
         }
         else
         {
            wb_table2_5_L72( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_L72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_13_L72( true) ;
         }
         else
         {
            wb_table3_13_L72( false) ;
         }
         return  ;
      }

      protected void wb_table3_13_L72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_L72e( true) ;
         }
         else
         {
            wb_table1_2_L72e( false) ;
         }
      }

      protected void wb_table3_13_L72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"16\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavModifica_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Processo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(43), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_Ordem_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_Ordem_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_Ordem_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_SrvPosSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_SrvPosSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_SrvPosSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(56), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_SrvPosPrcTmp_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_SrvPosPrcTmp_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_SrvPosPrcTmp_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(84), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_SrvPosPrcPgm_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_SrvPosPrcPgm_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_SrvPosPrcPgm_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(103), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_SrvPosPrcCnc_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_SrvPosPrcCnc_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_SrvPosPrcCnc_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV19Modifica));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavModifica_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavModifica_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV17Delete));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1532ServicoFluxo_Ordem), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_Ordem_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_Ordem_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1527ServicoFluxo_SrvPosSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_SrvPosSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_SrvPosSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_SrvPosPrcTmp_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_SrvPosPrcTmp_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_SrvPosPrcPgm_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_SrvPosPrcPgm_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_SrvPosPrcCnc_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_SrvPosPrcCnc_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 16 )
         {
            wbEnd = 0;
            nRC_GXsfl_16 = (short)(nGXsfl_16_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Nova sequ�ncia", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_13_L72e( true) ;
         }
         else
         {
            wb_table3_13_L72e( false) ;
         }
      }

      protected void wb_table2_5_L72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_servicocod_Internalname, "Servi�o", "", "", lblTextblockservicofluxo_servicocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ServicoServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_ServicoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_ServicoCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtServicoFluxo_ServicoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_L72e( true) ;
         }
         else
         {
            wb_table2_5_L72e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ServicoFluxo_ServicoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAL72( ) ;
         WSL72( ) ;
         WEL72( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ServicoFluxo_ServicoCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAL72( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "servicoservicofluxo");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAL72( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ServicoFluxo_ServicoCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0)));
         }
         wcpOAV7ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ServicoFluxo_ServicoCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ServicoFluxo_ServicoCod != wcpOAV7ServicoFluxo_ServicoCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ServicoFluxo_ServicoCod = AV7ServicoFluxo_ServicoCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ServicoFluxo_ServicoCod = cgiGet( sPrefix+"AV7ServicoFluxo_ServicoCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7ServicoFluxo_ServicoCod) > 0 )
         {
            AV7ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ServicoFluxo_ServicoCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0)));
         }
         else
         {
            AV7ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ServicoFluxo_ServicoCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAL72( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSL72( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSL72( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ServicoFluxo_ServicoCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ServicoFluxo_ServicoCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ServicoFluxo_ServicoCod_CTRL", StringUtil.RTrim( sCtrlAV7ServicoFluxo_ServicoCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEL72( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020529932268");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("servicoservicofluxo.js", "?2020529932268");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_162( )
      {
         edtavModifica_Internalname = sPrefix+"vMODIFICA_"+sGXsfl_16_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_16_idx;
         edtServicoFluxo_Codigo_Internalname = sPrefix+"SERVICOFLUXO_CODIGO_"+sGXsfl_16_idx;
         edtServicoFluxo_ServicoPos_Internalname = sPrefix+"SERVICOFLUXO_SERVICOPOS_"+sGXsfl_16_idx;
         edtServicoFluxo_Ordem_Internalname = sPrefix+"SERVICOFLUXO_ORDEM_"+sGXsfl_16_idx;
         edtServicoFluxo_SrvPosSigla_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSSIGLA_"+sGXsfl_16_idx;
         edtServicoFluxo_SrvPosPrcTmp_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSPRCTMP_"+sGXsfl_16_idx;
         edtServicoFluxo_SrvPosPrcPgm_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSPRCPGM_"+sGXsfl_16_idx;
         edtServicoFluxo_SrvPosPrcCnc_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSPRCCNC_"+sGXsfl_16_idx;
      }

      protected void SubsflControlProps_fel_162( )
      {
         edtavModifica_Internalname = sPrefix+"vMODIFICA_"+sGXsfl_16_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_16_fel_idx;
         edtServicoFluxo_Codigo_Internalname = sPrefix+"SERVICOFLUXO_CODIGO_"+sGXsfl_16_fel_idx;
         edtServicoFluxo_ServicoPos_Internalname = sPrefix+"SERVICOFLUXO_SERVICOPOS_"+sGXsfl_16_fel_idx;
         edtServicoFluxo_Ordem_Internalname = sPrefix+"SERVICOFLUXO_ORDEM_"+sGXsfl_16_fel_idx;
         edtServicoFluxo_SrvPosSigla_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSSIGLA_"+sGXsfl_16_fel_idx;
         edtServicoFluxo_SrvPosPrcTmp_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSPRCTMP_"+sGXsfl_16_fel_idx;
         edtServicoFluxo_SrvPosPrcPgm_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSPRCPGM_"+sGXsfl_16_fel_idx;
         edtServicoFluxo_SrvPosPrcCnc_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSPRCCNC_"+sGXsfl_16_fel_idx;
      }

      protected void sendrow_162( )
      {
         SubsflControlProps_162( ) ;
         WBL70( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_16_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_16_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_16_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavModifica_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavModifica_Enabled!=0)&&(edtavModifica_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 17,'"+sPrefix+"',false,'',16)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV19Modifica_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV19Modifica))&&String.IsNullOrEmpty(StringUtil.RTrim( AV44Modifica_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV19Modifica)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavModifica_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV19Modifica)) ? AV44Modifica_GXI : context.PathToRelativeUrl( AV19Modifica)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavModifica_Visible,(short)1,(String)"",(String)edtavModifica_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavModifica_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOMODIFICA\\'."+sGXsfl_16_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV19Modifica_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavDelete_Enabled!=0)&&(edtavDelete_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 18,'"+sPrefix+"',false,'',16)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV17Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV45Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)) ? AV45Delete_GXI : context.PathToRelativeUrl( AV17Delete)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavDelete_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+sGXsfl_16_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV17Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_ServicoPos_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_ServicoPos_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_Ordem_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1532ServicoFluxo_Ordem), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_Ordem_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)43,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)0,(bool)true,(String)"Ordem",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_SrvPosSigla_Internalname,StringUtil.RTrim( A1527ServicoFluxo_SrvPosSigla),StringUtil.RTrim( context.localUtil.Format( A1527ServicoFluxo_SrvPosSigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_SrvPosSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_SrvPosPrcTmp_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_SrvPosPrcTmp_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)56,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_SrvPosPrcPgm_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_SrvPosPrcPgm_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)84,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_SrvPosPrcCnc_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_SrvPosPrcCnc_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)103,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)16,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_CODIGO"+"_"+sGXsfl_16_idx, GetSecureSignedToken( sPrefix+sGXsfl_16_idx, context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_SERVICOPOS"+"_"+sGXsfl_16_idx, GetSecureSignedToken( sPrefix+sGXsfl_16_idx, context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_16_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_16_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_16_idx+1));
            sGXsfl_16_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_16_idx), 4, 0)), 4, "0");
            SubsflControlProps_162( ) ;
         }
         /* End function sendrow_162 */
      }

      protected void init_default_properties( )
      {
         lblTextblockservicofluxo_servicocod_Internalname = sPrefix+"TEXTBLOCKSERVICOFLUXO_SERVICOCOD";
         edtServicoFluxo_ServicoCod_Internalname = sPrefix+"SERVICOFLUXO_SERVICOCOD";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         edtavModifica_Internalname = sPrefix+"vMODIFICA";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtServicoFluxo_Codigo_Internalname = sPrefix+"SERVICOFLUXO_CODIGO";
         edtServicoFluxo_ServicoPos_Internalname = sPrefix+"SERVICOFLUXO_SERVICOPOS";
         edtServicoFluxo_Ordem_Internalname = sPrefix+"SERVICOFLUXO_ORDEM";
         edtServicoFluxo_SrvPosSigla_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSSIGLA";
         edtServicoFluxo_SrvPosPrcTmp_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSPRCTMP";
         edtServicoFluxo_SrvPosPrcPgm_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSPRCPGM";
         edtServicoFluxo_SrvPosPrcCnc_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSPRCCNC";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblUnnamedtable3_Internalname = sPrefix+"UNNAMEDTABLE3";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtServicoFluxo_ServicoCod_Internalname = sPrefix+"SERVICOFLUXO_SERVICOCOD";
         edtavTfservicofluxo_ordem_Internalname = sPrefix+"vTFSERVICOFLUXO_ORDEM";
         edtavTfservicofluxo_ordem_to_Internalname = sPrefix+"vTFSERVICOFLUXO_ORDEM_TO";
         edtavTfservicofluxo_srvpossigla_Internalname = sPrefix+"vTFSERVICOFLUXO_SRVPOSSIGLA";
         edtavTfservicofluxo_srvpossigla_sel_Internalname = sPrefix+"vTFSERVICOFLUXO_SRVPOSSIGLA_SEL";
         edtavTfservicofluxo_srvposprctmp_Internalname = sPrefix+"vTFSERVICOFLUXO_SRVPOSPRCTMP";
         edtavTfservicofluxo_srvposprctmp_to_Internalname = sPrefix+"vTFSERVICOFLUXO_SRVPOSPRCTMP_TO";
         edtavTfservicofluxo_srvposprcpgm_Internalname = sPrefix+"vTFSERVICOFLUXO_SRVPOSPRCPGM";
         edtavTfservicofluxo_srvposprcpgm_to_Internalname = sPrefix+"vTFSERVICOFLUXO_SRVPOSPRCPGM_TO";
         edtavTfservicofluxo_srvposprccnc_Internalname = sPrefix+"vTFSERVICOFLUXO_SRVPOSPRCCNC";
         edtavTfservicofluxo_srvposprccnc_to_Internalname = sPrefix+"vTFSERVICOFLUXO_SRVPOSPRCCNC_TO";
         Ddo_servicofluxo_ordem_Internalname = sPrefix+"DDO_SERVICOFLUXO_ORDEM";
         edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE";
         Ddo_servicofluxo_srvpossigla_Internalname = sPrefix+"DDO_SERVICOFLUXO_SRVPOSSIGLA";
         edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE";
         Ddo_servicofluxo_srvposprctmp_Internalname = sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCTMP";
         edtavDdo_servicofluxo_srvposprctmptitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE";
         Ddo_servicofluxo_srvposprcpgm_Internalname = sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCPGM";
         edtavDdo_servicofluxo_srvposprcpgmtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE";
         Ddo_servicofluxo_srvposprccnc_Internalname = sPrefix+"DDO_SERVICOFLUXO_SRVPOSPRCCNC";
         edtavDdo_servicofluxo_srvposprccnctitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtServicoFluxo_SrvPosPrcCnc_Jsonclick = "";
         edtServicoFluxo_SrvPosPrcPgm_Jsonclick = "";
         edtServicoFluxo_SrvPosPrcTmp_Jsonclick = "";
         edtServicoFluxo_SrvPosSigla_Jsonclick = "";
         edtServicoFluxo_Ordem_Jsonclick = "";
         edtServicoFluxo_ServicoPos_Jsonclick = "";
         edtServicoFluxo_Codigo_Jsonclick = "";
         edtavDelete_Jsonclick = "";
         edtavDelete_Enabled = 1;
         edtavModifica_Jsonclick = "";
         edtavModifica_Enabled = 1;
         imgInsert_Visible = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavModifica_Tooltiptext = "Modifica";
         edtServicoFluxo_SrvPosPrcCnc_Titleformat = 0;
         edtServicoFluxo_SrvPosPrcPgm_Titleformat = 0;
         edtServicoFluxo_SrvPosPrcTmp_Titleformat = 0;
         edtServicoFluxo_SrvPosSigla_Titleformat = 0;
         edtServicoFluxo_Ordem_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavDelete_Visible = -1;
         edtavModifica_Visible = -1;
         edtServicoFluxo_SrvPosPrcCnc_Title = "% Cancelamento";
         edtServicoFluxo_SrvPosPrcPgm_Title = "% Pagamento";
         edtServicoFluxo_SrvPosPrcTmp_Title = "% Tempo";
         edtServicoFluxo_SrvPosSigla_Title = "A��o";
         edtServicoFluxo_Ordem_Title = "Ordem";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_servicofluxo_srvposprccnctitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicofluxo_srvposprcpgmtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicofluxo_srvposprctmptitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Visible = 1;
         edtavTfservicofluxo_srvposprccnc_to_Jsonclick = "";
         edtavTfservicofluxo_srvposprccnc_to_Visible = 1;
         edtavTfservicofluxo_srvposprccnc_Jsonclick = "";
         edtavTfservicofluxo_srvposprccnc_Visible = 1;
         edtavTfservicofluxo_srvposprcpgm_to_Jsonclick = "";
         edtavTfservicofluxo_srvposprcpgm_to_Visible = 1;
         edtavTfservicofluxo_srvposprcpgm_Jsonclick = "";
         edtavTfservicofluxo_srvposprcpgm_Visible = 1;
         edtavTfservicofluxo_srvposprctmp_to_Jsonclick = "";
         edtavTfservicofluxo_srvposprctmp_to_Visible = 1;
         edtavTfservicofluxo_srvposprctmp_Jsonclick = "";
         edtavTfservicofluxo_srvposprctmp_Visible = 1;
         edtavTfservicofluxo_srvpossigla_sel_Jsonclick = "";
         edtavTfservicofluxo_srvpossigla_sel_Visible = 1;
         edtavTfservicofluxo_srvpossigla_Jsonclick = "";
         edtavTfservicofluxo_srvpossigla_Visible = 1;
         edtavTfservicofluxo_ordem_to_Jsonclick = "";
         edtavTfservicofluxo_ordem_to_Visible = 1;
         edtavTfservicofluxo_ordem_Jsonclick = "";
         edtavTfservicofluxo_ordem_Visible = 1;
         edtServicoFluxo_ServicoCod_Jsonclick = "";
         edtServicoFluxo_ServicoCod_Visible = 1;
         Ddo_servicofluxo_srvposprccnc_Searchbuttontext = "Pesquisar";
         Ddo_servicofluxo_srvposprccnc_Rangefilterto = "At�";
         Ddo_servicofluxo_srvposprccnc_Rangefilterfrom = "Desde";
         Ddo_servicofluxo_srvposprccnc_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_srvposprccnc_Includedatalist = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvposprccnc_Filterisrange = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvposprccnc_Filtertype = "Numeric";
         Ddo_servicofluxo_srvposprccnc_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvposprccnc_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvposprccnc_Includesortasc = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvposprccnc_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_srvposprccnc_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_srvposprccnc_Cls = "ColumnSettings";
         Ddo_servicofluxo_srvposprccnc_Tooltip = "Op��es";
         Ddo_servicofluxo_srvposprccnc_Caption = "";
         Ddo_servicofluxo_srvposprcpgm_Searchbuttontext = "Pesquisar";
         Ddo_servicofluxo_srvposprcpgm_Rangefilterto = "At�";
         Ddo_servicofluxo_srvposprcpgm_Rangefilterfrom = "Desde";
         Ddo_servicofluxo_srvposprcpgm_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_srvposprcpgm_Includedatalist = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvposprcpgm_Filterisrange = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvposprcpgm_Filtertype = "Numeric";
         Ddo_servicofluxo_srvposprcpgm_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvposprcpgm_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvposprcpgm_Includesortasc = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvposprcpgm_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_srvposprcpgm_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_srvposprcpgm_Cls = "ColumnSettings";
         Ddo_servicofluxo_srvposprcpgm_Tooltip = "Op��es";
         Ddo_servicofluxo_srvposprcpgm_Caption = "";
         Ddo_servicofluxo_srvposprctmp_Searchbuttontext = "Pesquisar";
         Ddo_servicofluxo_srvposprctmp_Rangefilterto = "At�";
         Ddo_servicofluxo_srvposprctmp_Rangefilterfrom = "Desde";
         Ddo_servicofluxo_srvposprctmp_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_srvposprctmp_Includedatalist = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvposprctmp_Filterisrange = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvposprctmp_Filtertype = "Numeric";
         Ddo_servicofluxo_srvposprctmp_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvposprctmp_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvposprctmp_Includesortasc = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvposprctmp_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_srvposprctmp_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_srvposprctmp_Cls = "ColumnSettings";
         Ddo_servicofluxo_srvposprctmp_Tooltip = "Op��es";
         Ddo_servicofluxo_srvposprctmp_Caption = "";
         Ddo_servicofluxo_srvpossigla_Searchbuttontext = "Pesquisar";
         Ddo_servicofluxo_srvpossigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicofluxo_srvpossigla_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_srvpossigla_Loadingdata = "Carregando dados...";
         Ddo_servicofluxo_srvpossigla_Datalistupdateminimumcharacters = 0;
         Ddo_servicofluxo_srvpossigla_Datalistproc = "GetServicoServicoFluxoFilterData";
         Ddo_servicofluxo_srvpossigla_Datalisttype = "Dynamic";
         Ddo_servicofluxo_srvpossigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvpossigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvpossigla_Filtertype = "Character";
         Ddo_servicofluxo_srvpossigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicofluxo_srvpossigla_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvpossigla_Includesortasc = Convert.ToBoolean( 0);
         Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_srvpossigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_srvpossigla_Cls = "ColumnSettings";
         Ddo_servicofluxo_srvpossigla_Tooltip = "Op��es";
         Ddo_servicofluxo_srvpossigla_Caption = "";
         Ddo_servicofluxo_ordem_Searchbuttontext = "Pesquisar";
         Ddo_servicofluxo_ordem_Rangefilterto = "At�";
         Ddo_servicofluxo_ordem_Rangefilterfrom = "Desde";
         Ddo_servicofluxo_ordem_Cleanfilter = "Limpar pesquisa";
         Ddo_servicofluxo_ordem_Includedatalist = Convert.ToBoolean( 0);
         Ddo_servicofluxo_ordem_Filterisrange = Convert.ToBoolean( -1);
         Ddo_servicofluxo_ordem_Filtertype = "Numeric";
         Ddo_servicofluxo_ordem_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicofluxo_ordem_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_servicofluxo_ordem_Includesortasc = Convert.ToBoolean( 0);
         Ddo_servicofluxo_ordem_Titlecontrolidtoreplace = "";
         Ddo_servicofluxo_ordem_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicofluxo_ordem_Cls = "ColumnSettings";
         Ddo_servicofluxo_ordem_Tooltip = "Op��es";
         Ddo_servicofluxo_ordem_Caption = "";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV20ServicoFluxo_OrdemTitleFilterData',fld:'vSERVICOFLUXO_ORDEMTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ServicoFluxo_SrvPosSiglaTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCTMPTITLEFILTERDATA',pic:'',nv:null},{av:'AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCPGMTITLEFILTERDATA',pic:'',nv:null},{av:'AV36ServicoFluxo_SrvPosPrcCncTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCCNCTITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoFluxo_Ordem_Titleformat',ctrl:'SERVICOFLUXO_ORDEM',prop:'Titleformat'},{av:'edtServicoFluxo_Ordem_Title',ctrl:'SERVICOFLUXO_ORDEM',prop:'Title'},{av:'edtServicoFluxo_SrvPosSigla_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosSigla_Title',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcTmp_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCTMP',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcTmp_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCTMP',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcPgm_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCPGM',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcPgm_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCPGM',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcCnc_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCCNC',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcCnc_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCCNC',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavModifica_Visible',ctrl:'vMODIFICA',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV18ServicoFluxo_ServicoSigla',fld:'vSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''}]}");
         setEventMetadata("DDO_SERVICOFLUXO_ORDEM.ONOPTIONCLICKED","{handler:'E11L72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_servicofluxo_ordem_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_ordem_Filteredtext_get',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'FilteredText_get'},{av:'Ddo_servicofluxo_ordem_Filteredtextto_get',ctrl:'DDO_SERVICOFLUXO_ORDEM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0}]}");
         setEventMetadata("DDO_SERVICOFLUXO_SRVPOSSIGLA.ONOPTIONCLICKED","{handler:'E12L72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_servicofluxo_srvpossigla_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_srvpossigla_Filteredtext_get',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'FilteredText_get'},{av:'Ddo_servicofluxo_srvpossigla_Selectedvalue_get',ctrl:'DDO_SERVICOFLUXO_SRVPOSSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''}]}");
         setEventMetadata("DDO_SERVICOFLUXO_SRVPOSPRCTMP.ONOPTIONCLICKED","{handler:'E13L72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_servicofluxo_srvposprctmp_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_SRVPOSPRCTMP',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_srvposprctmp_Filteredtext_get',ctrl:'DDO_SERVICOFLUXO_SRVPOSPRCTMP',prop:'FilteredText_get'},{av:'Ddo_servicofluxo_srvposprctmp_Filteredtextto_get',ctrl:'DDO_SERVICOFLUXO_SRVPOSPRCTMP',prop:'FilteredTextTo_get'}],oparms:[{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_SERVICOFLUXO_SRVPOSPRCPGM.ONOPTIONCLICKED","{handler:'E14L72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_servicofluxo_srvposprcpgm_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_SRVPOSPRCPGM',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_srvposprcpgm_Filteredtext_get',ctrl:'DDO_SERVICOFLUXO_SRVPOSPRCPGM',prop:'FilteredText_get'},{av:'Ddo_servicofluxo_srvposprcpgm_Filteredtextto_get',ctrl:'DDO_SERVICOFLUXO_SRVPOSPRCPGM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_SERVICOFLUXO_SRVPOSPRCCNC.ONOPTIONCLICKED","{handler:'E15L72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_servicofluxo_srvposprccnc_Activeeventkey',ctrl:'DDO_SERVICOFLUXO_SRVPOSPRCCNC',prop:'ActiveEventKey'},{av:'Ddo_servicofluxo_srvposprccnc_Filteredtext_get',ctrl:'DDO_SERVICOFLUXO_SRVPOSPRCCNC',prop:'FilteredText_get'},{av:'Ddo_servicofluxo_srvposprccnc_Filteredtextto_get',ctrl:'DDO_SERVICOFLUXO_SRVPOSPRCCNC',prop:'FilteredTextTo_get'}],oparms:[{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E19L72',iparms:[],oparms:[{av:'AV19Modifica',fld:'vMODIFICA',pic:'',nv:''},{av:'edtavModifica_Tooltiptext',ctrl:'vMODIFICA',prop:'Tooltiptext'},{av:'AV17Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16L72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV18ServicoFluxo_ServicoSigla',fld:'vSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("'DOMODIFICA'","{handler:'E20L72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'A1526ServicoFluxo_ServicoPos',fld:'SERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1532ServicoFluxo_Ordem',fld:'SERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0}],oparms:[{av:'A1532ServicoFluxo_Ordem',fld:'SERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E21L72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV20ServicoFluxo_OrdemTitleFilterData',fld:'vSERVICOFLUXO_ORDEMTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ServicoFluxo_SrvPosSiglaTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCTMPTITLEFILTERDATA',pic:'',nv:null},{av:'AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCPGMTITLEFILTERDATA',pic:'',nv:null},{av:'AV36ServicoFluxo_SrvPosPrcCncTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCCNCTITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoFluxo_Ordem_Titleformat',ctrl:'SERVICOFLUXO_ORDEM',prop:'Titleformat'},{av:'edtServicoFluxo_Ordem_Title',ctrl:'SERVICOFLUXO_ORDEM',prop:'Title'},{av:'edtServicoFluxo_SrvPosSigla_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosSigla_Title',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcTmp_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCTMP',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcTmp_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCTMP',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcPgm_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCPGM',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcPgm_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCPGM',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcCnc_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCCNC',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcCnc_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCCNC',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavModifica_Visible',ctrl:'vMODIFICA',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV18ServicoFluxo_ServicoSigla',fld:'vSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV20ServicoFluxo_OrdemTitleFilterData',fld:'vSERVICOFLUXO_ORDEMTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ServicoFluxo_SrvPosSiglaTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCTMPTITLEFILTERDATA',pic:'',nv:null},{av:'AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCPGMTITLEFILTERDATA',pic:'',nv:null},{av:'AV36ServicoFluxo_SrvPosPrcCncTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCCNCTITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoFluxo_Ordem_Titleformat',ctrl:'SERVICOFLUXO_ORDEM',prop:'Titleformat'},{av:'edtServicoFluxo_Ordem_Title',ctrl:'SERVICOFLUXO_ORDEM',prop:'Title'},{av:'edtServicoFluxo_SrvPosSigla_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosSigla_Title',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcTmp_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCTMP',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcTmp_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCTMP',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcPgm_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCPGM',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcPgm_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCPGM',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcCnc_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCCNC',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcCnc_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCCNC',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavModifica_Visible',ctrl:'vMODIFICA',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV18ServicoFluxo_ServicoSigla',fld:'vSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV20ServicoFluxo_OrdemTitleFilterData',fld:'vSERVICOFLUXO_ORDEMTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ServicoFluxo_SrvPosSiglaTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCTMPTITLEFILTERDATA',pic:'',nv:null},{av:'AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCPGMTITLEFILTERDATA',pic:'',nv:null},{av:'AV36ServicoFluxo_SrvPosPrcCncTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCCNCTITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoFluxo_Ordem_Titleformat',ctrl:'SERVICOFLUXO_ORDEM',prop:'Titleformat'},{av:'edtServicoFluxo_Ordem_Title',ctrl:'SERVICOFLUXO_ORDEM',prop:'Title'},{av:'edtServicoFluxo_SrvPosSigla_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosSigla_Title',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcTmp_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCTMP',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcTmp_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCTMP',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcPgm_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCPGM',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcPgm_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCPGM',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcCnc_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCCNC',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcCnc_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCCNC',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavModifica_Visible',ctrl:'vMODIFICA',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV18ServicoFluxo_ServicoSigla',fld:'vSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCTMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCPGMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace',fld:'vDDO_SERVICOFLUXO_SRVPOSPRCCNCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV21TFServicoFluxo_Ordem',fld:'vTFSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV22TFServicoFluxo_Ordem_To',fld:'vTFSERVICOFLUXO_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV25TFServicoFluxo_SrvPosSigla',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA',pic:'@!',nv:''},{av:'AV26TFServicoFluxo_SrvPosSigla_Sel',fld:'vTFSERVICOFLUXO_SRVPOSSIGLA_SEL',pic:'@!',nv:''},{av:'AV29TFServicoFluxo_SrvPosPrcTmp',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP',pic:'ZZZ9',nv:0},{av:'AV30TFServicoFluxo_SrvPosPrcTmp_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCTMP_TO',pic:'ZZZ9',nv:0},{av:'AV33TFServicoFluxo_SrvPosPrcPgm',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM',pic:'ZZZ9',nv:0},{av:'AV34TFServicoFluxo_SrvPosPrcPgm_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCPGM_TO',pic:'ZZZ9',nv:0},{av:'AV37TFServicoFluxo_SrvPosPrcCnc',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC',pic:'ZZZ9',nv:0},{av:'AV38TFServicoFluxo_SrvPosPrcCnc_To',fld:'vTFSERVICOFLUXO_SRVPOSPRCCNC_TO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV20ServicoFluxo_OrdemTitleFilterData',fld:'vSERVICOFLUXO_ORDEMTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ServicoFluxo_SrvPosSiglaTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCTMPTITLEFILTERDATA',pic:'',nv:null},{av:'AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCPGMTITLEFILTERDATA',pic:'',nv:null},{av:'AV36ServicoFluxo_SrvPosPrcCncTitleFilterData',fld:'vSERVICOFLUXO_SRVPOSPRCCNCTITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoFluxo_Ordem_Titleformat',ctrl:'SERVICOFLUXO_ORDEM',prop:'Titleformat'},{av:'edtServicoFluxo_Ordem_Title',ctrl:'SERVICOFLUXO_ORDEM',prop:'Title'},{av:'edtServicoFluxo_SrvPosSigla_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosSigla_Title',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcTmp_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCTMP',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcTmp_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCTMP',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcPgm_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCPGM',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcPgm_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCPGM',prop:'Title'},{av:'edtServicoFluxo_SrvPosPrcCnc_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSPRCCNC',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosPrcCnc_Title',ctrl:'SERVICOFLUXO_SRVPOSPRCCNC',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavModifica_Visible',ctrl:'vMODIFICA',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV18ServicoFluxo_ServicoSigla',fld:'vSERVICOFLUXO_SERVICOSIGLA',pic:'@!',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_servicofluxo_ordem_Activeeventkey = "";
         Ddo_servicofluxo_ordem_Filteredtext_get = "";
         Ddo_servicofluxo_ordem_Filteredtextto_get = "";
         Ddo_servicofluxo_srvpossigla_Activeeventkey = "";
         Ddo_servicofluxo_srvpossigla_Filteredtext_get = "";
         Ddo_servicofluxo_srvpossigla_Selectedvalue_get = "";
         Ddo_servicofluxo_srvposprctmp_Activeeventkey = "";
         Ddo_servicofluxo_srvposprctmp_Filteredtext_get = "";
         Ddo_servicofluxo_srvposprctmp_Filteredtextto_get = "";
         Ddo_servicofluxo_srvposprcpgm_Activeeventkey = "";
         Ddo_servicofluxo_srvposprcpgm_Filteredtext_get = "";
         Ddo_servicofluxo_srvposprcpgm_Filteredtextto_get = "";
         Ddo_servicofluxo_srvposprccnc_Activeeventkey = "";
         Ddo_servicofluxo_srvposprccnc_Filteredtext_get = "";
         Ddo_servicofluxo_srvposprccnc_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV25TFServicoFluxo_SrvPosSigla = "";
         AV26TFServicoFluxo_SrvPosSigla_Sel = "";
         AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace = "";
         AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace = "";
         AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace = "";
         AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace = "";
         AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace = "";
         A605Servico_Sigla = "";
         AV46Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV40DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV20ServicoFluxo_OrdemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ServicoFluxo_SrvPosSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36ServicoFluxo_SrvPosPrcCncTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV18ServicoFluxo_ServicoSigla = "";
         Ddo_servicofluxo_ordem_Filteredtext_set = "";
         Ddo_servicofluxo_ordem_Filteredtextto_set = "";
         Ddo_servicofluxo_srvpossigla_Filteredtext_set = "";
         Ddo_servicofluxo_srvpossigla_Selectedvalue_set = "";
         Ddo_servicofluxo_srvposprctmp_Filteredtext_set = "";
         Ddo_servicofluxo_srvposprctmp_Filteredtextto_set = "";
         Ddo_servicofluxo_srvposprcpgm_Filteredtext_set = "";
         Ddo_servicofluxo_srvposprcpgm_Filteredtextto_set = "";
         Ddo_servicofluxo_srvposprccnc_Filteredtext_set = "";
         Ddo_servicofluxo_srvposprccnc_Filteredtextto_set = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV19Modifica = "";
         AV44Modifica_GXI = "";
         AV17Delete = "";
         AV45Delete_GXI = "";
         A1527ServicoFluxo_SrvPosSigla = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV25TFServicoFluxo_SrvPosSigla = "";
         H00L72_A1522ServicoFluxo_ServicoCod = new int[1] ;
         H00L72_A1556ServicoFluxo_SrvPosPrcCnc = new short[1] ;
         H00L72_n1556ServicoFluxo_SrvPosPrcCnc = new bool[] {false} ;
         H00L72_A1555ServicoFluxo_SrvPosPrcPgm = new short[1] ;
         H00L72_n1555ServicoFluxo_SrvPosPrcPgm = new bool[] {false} ;
         H00L72_A1554ServicoFluxo_SrvPosPrcTmp = new short[1] ;
         H00L72_n1554ServicoFluxo_SrvPosPrcTmp = new bool[] {false} ;
         H00L72_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         H00L72_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         H00L72_A1532ServicoFluxo_Ordem = new short[1] ;
         H00L72_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         H00L72_A1526ServicoFluxo_ServicoPos = new int[1] ;
         H00L72_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         H00L72_A1528ServicoFluxo_Codigo = new int[1] ;
         H00L73_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00L74_A155Servico_Codigo = new int[1] ;
         H00L74_A605Servico_Sigla = new String[] {""} ;
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgInsert_Jsonclick = "";
         lblTextblockservicofluxo_servicocod_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ServicoFluxo_ServicoCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoservicofluxo__default(),
            new Object[][] {
                new Object[] {
               H00L72_A1522ServicoFluxo_ServicoCod, H00L72_A1556ServicoFluxo_SrvPosPrcCnc, H00L72_n1556ServicoFluxo_SrvPosPrcCnc, H00L72_A1555ServicoFluxo_SrvPosPrcPgm, H00L72_n1555ServicoFluxo_SrvPosPrcPgm, H00L72_A1554ServicoFluxo_SrvPosPrcTmp, H00L72_n1554ServicoFluxo_SrvPosPrcTmp, H00L72_A1527ServicoFluxo_SrvPosSigla, H00L72_n1527ServicoFluxo_SrvPosSigla, H00L72_A1532ServicoFluxo_Ordem,
               H00L72_n1532ServicoFluxo_Ordem, H00L72_A1526ServicoFluxo_ServicoPos, H00L72_n1526ServicoFluxo_ServicoPos, H00L72_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               H00L73_AGRID_nRecordCount
               }
               , new Object[] {
               H00L74_A155Servico_Codigo, H00L74_A605Servico_Sigla
               }
            }
         );
         AV46Pgmname = "ServicoServicoFluxo";
         /* GeneXus formulas. */
         AV46Pgmname = "ServicoServicoFluxo";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_16 ;
      private short nGXsfl_16_idx=1 ;
      private short AV21TFServicoFluxo_Ordem ;
      private short AV22TFServicoFluxo_Ordem_To ;
      private short AV29TFServicoFluxo_SrvPosPrcTmp ;
      private short AV30TFServicoFluxo_SrvPosPrcTmp_To ;
      private short AV33TFServicoFluxo_SrvPosPrcPgm ;
      private short AV34TFServicoFluxo_SrvPosPrcPgm_To ;
      private short AV37TFServicoFluxo_SrvPosPrcCnc ;
      private short AV38TFServicoFluxo_SrvPosPrcCnc_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A1532ServicoFluxo_Ordem ;
      private short A1554ServicoFluxo_SrvPosPrcTmp ;
      private short A1555ServicoFluxo_SrvPosPrcPgm ;
      private short A1556ServicoFluxo_SrvPosPrcCnc ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_16_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtServicoFluxo_Ordem_Titleformat ;
      private short edtServicoFluxo_SrvPosSigla_Titleformat ;
      private short edtServicoFluxo_SrvPosPrcTmp_Titleformat ;
      private short edtServicoFluxo_SrvPosPrcPgm_Titleformat ;
      private short edtServicoFluxo_SrvPosPrcCnc_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ServicoFluxo_ServicoCod ;
      private int wcpOAV7ServicoFluxo_ServicoCod ;
      private int subGrid_Rows ;
      private int A155Servico_Codigo ;
      private int Ddo_servicofluxo_srvpossigla_Datalistupdateminimumcharacters ;
      private int A1522ServicoFluxo_ServicoCod ;
      private int edtServicoFluxo_ServicoCod_Visible ;
      private int edtavTfservicofluxo_ordem_Visible ;
      private int edtavTfservicofluxo_ordem_to_Visible ;
      private int edtavTfservicofluxo_srvpossigla_Visible ;
      private int edtavTfservicofluxo_srvpossigla_sel_Visible ;
      private int edtavTfservicofluxo_srvposprctmp_Visible ;
      private int edtavTfservicofluxo_srvposprctmp_to_Visible ;
      private int edtavTfservicofluxo_srvposprcpgm_Visible ;
      private int edtavTfservicofluxo_srvposprcpgm_to_Visible ;
      private int edtavTfservicofluxo_srvposprccnc_Visible ;
      private int edtavTfservicofluxo_srvposprccnc_to_Visible ;
      private int edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicofluxo_srvposprctmptitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicofluxo_srvposprcpgmtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicofluxo_srvposprccnctitlecontrolidtoreplace_Visible ;
      private int A1528ServicoFluxo_Codigo ;
      private int A1526ServicoFluxo_ServicoPos ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int imgInsert_Visible ;
      private int edtavModifica_Visible ;
      private int edtavDelete_Visible ;
      private int AV47GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavModifica_Enabled ;
      private int edtavDelete_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Ddo_servicofluxo_ordem_Activeeventkey ;
      private String Ddo_servicofluxo_ordem_Filteredtext_get ;
      private String Ddo_servicofluxo_ordem_Filteredtextto_get ;
      private String Ddo_servicofluxo_srvpossigla_Activeeventkey ;
      private String Ddo_servicofluxo_srvpossigla_Filteredtext_get ;
      private String Ddo_servicofluxo_srvpossigla_Selectedvalue_get ;
      private String Ddo_servicofluxo_srvposprctmp_Activeeventkey ;
      private String Ddo_servicofluxo_srvposprctmp_Filteredtext_get ;
      private String Ddo_servicofluxo_srvposprctmp_Filteredtextto_get ;
      private String Ddo_servicofluxo_srvposprcpgm_Activeeventkey ;
      private String Ddo_servicofluxo_srvposprcpgm_Filteredtext_get ;
      private String Ddo_servicofluxo_srvposprcpgm_Filteredtextto_get ;
      private String Ddo_servicofluxo_srvposprccnc_Activeeventkey ;
      private String Ddo_servicofluxo_srvposprccnc_Filteredtext_get ;
      private String Ddo_servicofluxo_srvposprccnc_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_16_idx="0001" ;
      private String AV25TFServicoFluxo_SrvPosSigla ;
      private String AV26TFServicoFluxo_SrvPosSigla_Sel ;
      private String A605Servico_Sigla ;
      private String AV46Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV18ServicoFluxo_ServicoSigla ;
      private String Ddo_servicofluxo_ordem_Caption ;
      private String Ddo_servicofluxo_ordem_Tooltip ;
      private String Ddo_servicofluxo_ordem_Cls ;
      private String Ddo_servicofluxo_ordem_Filteredtext_set ;
      private String Ddo_servicofluxo_ordem_Filteredtextto_set ;
      private String Ddo_servicofluxo_ordem_Dropdownoptionstype ;
      private String Ddo_servicofluxo_ordem_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_ordem_Filtertype ;
      private String Ddo_servicofluxo_ordem_Cleanfilter ;
      private String Ddo_servicofluxo_ordem_Rangefilterfrom ;
      private String Ddo_servicofluxo_ordem_Rangefilterto ;
      private String Ddo_servicofluxo_ordem_Searchbuttontext ;
      private String Ddo_servicofluxo_srvpossigla_Caption ;
      private String Ddo_servicofluxo_srvpossigla_Tooltip ;
      private String Ddo_servicofluxo_srvpossigla_Cls ;
      private String Ddo_servicofluxo_srvpossigla_Filteredtext_set ;
      private String Ddo_servicofluxo_srvpossigla_Selectedvalue_set ;
      private String Ddo_servicofluxo_srvpossigla_Dropdownoptionstype ;
      private String Ddo_servicofluxo_srvpossigla_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_srvpossigla_Filtertype ;
      private String Ddo_servicofluxo_srvpossigla_Datalisttype ;
      private String Ddo_servicofluxo_srvpossigla_Datalistproc ;
      private String Ddo_servicofluxo_srvpossigla_Loadingdata ;
      private String Ddo_servicofluxo_srvpossigla_Cleanfilter ;
      private String Ddo_servicofluxo_srvpossigla_Noresultsfound ;
      private String Ddo_servicofluxo_srvpossigla_Searchbuttontext ;
      private String Ddo_servicofluxo_srvposprctmp_Caption ;
      private String Ddo_servicofluxo_srvposprctmp_Tooltip ;
      private String Ddo_servicofluxo_srvposprctmp_Cls ;
      private String Ddo_servicofluxo_srvposprctmp_Filteredtext_set ;
      private String Ddo_servicofluxo_srvposprctmp_Filteredtextto_set ;
      private String Ddo_servicofluxo_srvposprctmp_Dropdownoptionstype ;
      private String Ddo_servicofluxo_srvposprctmp_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_srvposprctmp_Filtertype ;
      private String Ddo_servicofluxo_srvposprctmp_Cleanfilter ;
      private String Ddo_servicofluxo_srvposprctmp_Rangefilterfrom ;
      private String Ddo_servicofluxo_srvposprctmp_Rangefilterto ;
      private String Ddo_servicofluxo_srvposprctmp_Searchbuttontext ;
      private String Ddo_servicofluxo_srvposprcpgm_Caption ;
      private String Ddo_servicofluxo_srvposprcpgm_Tooltip ;
      private String Ddo_servicofluxo_srvposprcpgm_Cls ;
      private String Ddo_servicofluxo_srvposprcpgm_Filteredtext_set ;
      private String Ddo_servicofluxo_srvposprcpgm_Filteredtextto_set ;
      private String Ddo_servicofluxo_srvposprcpgm_Dropdownoptionstype ;
      private String Ddo_servicofluxo_srvposprcpgm_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_srvposprcpgm_Filtertype ;
      private String Ddo_servicofluxo_srvposprcpgm_Cleanfilter ;
      private String Ddo_servicofluxo_srvposprcpgm_Rangefilterfrom ;
      private String Ddo_servicofluxo_srvposprcpgm_Rangefilterto ;
      private String Ddo_servicofluxo_srvposprcpgm_Searchbuttontext ;
      private String Ddo_servicofluxo_srvposprccnc_Caption ;
      private String Ddo_servicofluxo_srvposprccnc_Tooltip ;
      private String Ddo_servicofluxo_srvposprccnc_Cls ;
      private String Ddo_servicofluxo_srvposprccnc_Filteredtext_set ;
      private String Ddo_servicofluxo_srvposprccnc_Filteredtextto_set ;
      private String Ddo_servicofluxo_srvposprccnc_Dropdownoptionstype ;
      private String Ddo_servicofluxo_srvposprccnc_Titlecontrolidtoreplace ;
      private String Ddo_servicofluxo_srvposprccnc_Filtertype ;
      private String Ddo_servicofluxo_srvposprccnc_Cleanfilter ;
      private String Ddo_servicofluxo_srvposprccnc_Rangefilterfrom ;
      private String Ddo_servicofluxo_srvposprccnc_Rangefilterto ;
      private String Ddo_servicofluxo_srvposprccnc_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtServicoFluxo_ServicoCod_Internalname ;
      private String edtServicoFluxo_ServicoCod_Jsonclick ;
      private String TempTags ;
      private String edtavTfservicofluxo_ordem_Internalname ;
      private String edtavTfservicofluxo_ordem_Jsonclick ;
      private String edtavTfservicofluxo_ordem_to_Internalname ;
      private String edtavTfservicofluxo_ordem_to_Jsonclick ;
      private String edtavTfservicofluxo_srvpossigla_Internalname ;
      private String edtavTfservicofluxo_srvpossigla_Jsonclick ;
      private String edtavTfservicofluxo_srvpossigla_sel_Internalname ;
      private String edtavTfservicofluxo_srvpossigla_sel_Jsonclick ;
      private String edtavTfservicofluxo_srvposprctmp_Internalname ;
      private String edtavTfservicofluxo_srvposprctmp_Jsonclick ;
      private String edtavTfservicofluxo_srvposprctmp_to_Internalname ;
      private String edtavTfservicofluxo_srvposprctmp_to_Jsonclick ;
      private String edtavTfservicofluxo_srvposprcpgm_Internalname ;
      private String edtavTfservicofluxo_srvposprcpgm_Jsonclick ;
      private String edtavTfservicofluxo_srvposprcpgm_to_Internalname ;
      private String edtavTfservicofluxo_srvposprcpgm_to_Jsonclick ;
      private String edtavTfservicofluxo_srvposprccnc_Internalname ;
      private String edtavTfservicofluxo_srvposprccnc_Jsonclick ;
      private String edtavTfservicofluxo_srvposprccnc_to_Internalname ;
      private String edtavTfservicofluxo_srvposprccnc_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_servicofluxo_ordemtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicofluxo_srvpossiglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicofluxo_srvposprctmptitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicofluxo_srvposprcpgmtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicofluxo_srvposprccnctitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavModifica_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtServicoFluxo_Codigo_Internalname ;
      private String edtServicoFluxo_ServicoPos_Internalname ;
      private String edtServicoFluxo_Ordem_Internalname ;
      private String A1527ServicoFluxo_SrvPosSigla ;
      private String edtServicoFluxo_SrvPosSigla_Internalname ;
      private String edtServicoFluxo_SrvPosPrcTmp_Internalname ;
      private String edtServicoFluxo_SrvPosPrcPgm_Internalname ;
      private String edtServicoFluxo_SrvPosPrcCnc_Internalname ;
      private String scmdbuf ;
      private String lV25TFServicoFluxo_SrvPosSigla ;
      private String subGrid_Internalname ;
      private String Ddo_servicofluxo_ordem_Internalname ;
      private String Ddo_servicofluxo_srvpossigla_Internalname ;
      private String Ddo_servicofluxo_srvposprctmp_Internalname ;
      private String Ddo_servicofluxo_srvposprcpgm_Internalname ;
      private String Ddo_servicofluxo_srvposprccnc_Internalname ;
      private String edtServicoFluxo_Ordem_Title ;
      private String edtServicoFluxo_SrvPosSigla_Title ;
      private String edtServicoFluxo_SrvPosPrcTmp_Title ;
      private String edtServicoFluxo_SrvPosPrcPgm_Title ;
      private String edtServicoFluxo_SrvPosPrcCnc_Title ;
      private String imgInsert_Internalname ;
      private String edtavModifica_Tooltiptext ;
      private String edtavDelete_Tooltiptext ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String imgInsert_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblockservicofluxo_servicocod_Internalname ;
      private String lblTextblockservicofluxo_servicocod_Jsonclick ;
      private String sCtrlAV7ServicoFluxo_ServicoCod ;
      private String sGXsfl_16_fel_idx="0001" ;
      private String edtavModifica_Jsonclick ;
      private String edtavDelete_Jsonclick ;
      private String ROClassString ;
      private String edtServicoFluxo_Codigo_Jsonclick ;
      private String edtServicoFluxo_ServicoPos_Jsonclick ;
      private String edtServicoFluxo_Ordem_Jsonclick ;
      private String edtServicoFluxo_SrvPosSigla_Jsonclick ;
      private String edtServicoFluxo_SrvPosPrcTmp_Jsonclick ;
      private String edtServicoFluxo_SrvPosPrcPgm_Jsonclick ;
      private String edtServicoFluxo_SrvPosPrcCnc_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Ddo_servicofluxo_ordem_Includesortasc ;
      private bool Ddo_servicofluxo_ordem_Includesortdsc ;
      private bool Ddo_servicofluxo_ordem_Includefilter ;
      private bool Ddo_servicofluxo_ordem_Filterisrange ;
      private bool Ddo_servicofluxo_ordem_Includedatalist ;
      private bool Ddo_servicofluxo_srvpossigla_Includesortasc ;
      private bool Ddo_servicofluxo_srvpossigla_Includesortdsc ;
      private bool Ddo_servicofluxo_srvpossigla_Includefilter ;
      private bool Ddo_servicofluxo_srvpossigla_Filterisrange ;
      private bool Ddo_servicofluxo_srvpossigla_Includedatalist ;
      private bool Ddo_servicofluxo_srvposprctmp_Includesortasc ;
      private bool Ddo_servicofluxo_srvposprctmp_Includesortdsc ;
      private bool Ddo_servicofluxo_srvposprctmp_Includefilter ;
      private bool Ddo_servicofluxo_srvposprctmp_Filterisrange ;
      private bool Ddo_servicofluxo_srvposprctmp_Includedatalist ;
      private bool Ddo_servicofluxo_srvposprcpgm_Includesortasc ;
      private bool Ddo_servicofluxo_srvposprcpgm_Includesortdsc ;
      private bool Ddo_servicofluxo_srvposprcpgm_Includefilter ;
      private bool Ddo_servicofluxo_srvposprcpgm_Filterisrange ;
      private bool Ddo_servicofluxo_srvposprcpgm_Includedatalist ;
      private bool Ddo_servicofluxo_srvposprccnc_Includesortasc ;
      private bool Ddo_servicofluxo_srvposprccnc_Includesortdsc ;
      private bool Ddo_servicofluxo_srvposprccnc_Includefilter ;
      private bool Ddo_servicofluxo_srvposprccnc_Filterisrange ;
      private bool Ddo_servicofluxo_srvposprccnc_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1526ServicoFluxo_ServicoPos ;
      private bool n1532ServicoFluxo_Ordem ;
      private bool n1527ServicoFluxo_SrvPosSigla ;
      private bool n1554ServicoFluxo_SrvPosPrcTmp ;
      private bool n1555ServicoFluxo_SrvPosPrcPgm ;
      private bool n1556ServicoFluxo_SrvPosPrcCnc ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV19Modifica_IsBlob ;
      private bool AV17Delete_IsBlob ;
      private String AV23ddo_ServicoFluxo_OrdemTitleControlIdToReplace ;
      private String AV27ddo_ServicoFluxo_SrvPosSiglaTitleControlIdToReplace ;
      private String AV31ddo_ServicoFluxo_SrvPosPrcTmpTitleControlIdToReplace ;
      private String AV35ddo_ServicoFluxo_SrvPosPrcPgmTitleControlIdToReplace ;
      private String AV39ddo_ServicoFluxo_SrvPosPrcCncTitleControlIdToReplace ;
      private String AV44Modifica_GXI ;
      private String AV45Delete_GXI ;
      private String AV19Modifica ;
      private String AV17Delete ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00L72_A1522ServicoFluxo_ServicoCod ;
      private short[] H00L72_A1556ServicoFluxo_SrvPosPrcCnc ;
      private bool[] H00L72_n1556ServicoFluxo_SrvPosPrcCnc ;
      private short[] H00L72_A1555ServicoFluxo_SrvPosPrcPgm ;
      private bool[] H00L72_n1555ServicoFluxo_SrvPosPrcPgm ;
      private short[] H00L72_A1554ServicoFluxo_SrvPosPrcTmp ;
      private bool[] H00L72_n1554ServicoFluxo_SrvPosPrcTmp ;
      private String[] H00L72_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] H00L72_n1527ServicoFluxo_SrvPosSigla ;
      private short[] H00L72_A1532ServicoFluxo_Ordem ;
      private bool[] H00L72_n1532ServicoFluxo_Ordem ;
      private int[] H00L72_A1526ServicoFluxo_ServicoPos ;
      private bool[] H00L72_n1526ServicoFluxo_ServicoPos ;
      private int[] H00L72_A1528ServicoFluxo_Codigo ;
      private long[] H00L73_AGRID_nRecordCount ;
      private int[] H00L74_A155Servico_Codigo ;
      private String[] H00L74_A605Servico_Sigla ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV20ServicoFluxo_OrdemTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV24ServicoFluxo_SrvPosSiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV28ServicoFluxo_SrvPosPrcTmpTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV32ServicoFluxo_SrvPosPrcPgmTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36ServicoFluxo_SrvPosPrcCncTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV40DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class servicoservicofluxo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00L72( IGxContext context ,
                                             short AV21TFServicoFluxo_Ordem ,
                                             short AV22TFServicoFluxo_Ordem_To ,
                                             String AV26TFServicoFluxo_SrvPosSigla_Sel ,
                                             String AV25TFServicoFluxo_SrvPosSigla ,
                                             short AV29TFServicoFluxo_SrvPosPrcTmp ,
                                             short AV30TFServicoFluxo_SrvPosPrcTmp_To ,
                                             short AV33TFServicoFluxo_SrvPosPrcPgm ,
                                             short AV34TFServicoFluxo_SrvPosPrcPgm_To ,
                                             short AV37TFServicoFluxo_SrvPosPrcCnc ,
                                             short AV38TFServicoFluxo_SrvPosPrcCnc_To ,
                                             short A1532ServicoFluxo_Ordem ,
                                             String A1527ServicoFluxo_SrvPosSigla ,
                                             short A1554ServicoFluxo_SrvPosPrcTmp ,
                                             short A1555ServicoFluxo_SrvPosPrcPgm ,
                                             short A1556ServicoFluxo_SrvPosPrcCnc ,
                                             int AV7ServicoFluxo_ServicoCod ,
                                             int A1522ServicoFluxo_ServicoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [16] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ServicoFluxo_ServicoCod], T2.[Servico_PercCnc] AS ServicoFluxo_SrvPosPrcCnc, T2.[Servico_PercPgm] AS ServicoFluxo_SrvPosPrcPgm, T2.[Servico_PercTmp] AS ServicoFluxo_SrvPosPrcTmp, T2.[Servico_Sigla] AS ServicoFluxo_SrvPosSigla, T1.[ServicoFluxo_Ordem], T1.[ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos, T1.[ServicoFluxo_Codigo]";
         sFromString = " FROM ([ServicoFluxo] T1 WITH (NOLOCK) LEFT JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[ServicoFluxo_ServicoPos])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ServicoFluxo_ServicoCod] = @AV7ServicoFluxo_ServicoCod)";
         if ( ! (0==AV21TFServicoFluxo_Ordem) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] >= @AV21TFServicoFluxo_Ordem)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV22TFServicoFluxo_Ordem_To) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] <= @AV22TFServicoFluxo_Ordem_To)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV26TFServicoFluxo_SrvPosSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFServicoFluxo_SrvPosSigla)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV25TFServicoFluxo_SrvPosSigla)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFServicoFluxo_SrvPosSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] = @AV26TFServicoFluxo_SrvPosSigla_Sel)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (0==AV29TFServicoFluxo_SrvPosPrcTmp) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercTmp] >= @AV29TFServicoFluxo_SrvPosPrcTmp)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV30TFServicoFluxo_SrvPosPrcTmp_To) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercTmp] <= @AV30TFServicoFluxo_SrvPosPrcTmp_To)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV33TFServicoFluxo_SrvPosPrcPgm) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercPgm] >= @AV33TFServicoFluxo_SrvPosPrcPgm)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV34TFServicoFluxo_SrvPosPrcPgm_To) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercPgm] <= @AV34TFServicoFluxo_SrvPosPrcPgm_To)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV37TFServicoFluxo_SrvPosPrcCnc) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercCnc] >= @AV37TFServicoFluxo_SrvPosPrcCnc)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV38TFServicoFluxo_SrvPosPrcCnc_To) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercCnc] <= @AV38TFServicoFluxo_SrvPosPrcCnc_To)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod], T1.[ServicoFluxo_Ordem]";
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00L73( IGxContext context ,
                                             short AV21TFServicoFluxo_Ordem ,
                                             short AV22TFServicoFluxo_Ordem_To ,
                                             String AV26TFServicoFluxo_SrvPosSigla_Sel ,
                                             String AV25TFServicoFluxo_SrvPosSigla ,
                                             short AV29TFServicoFluxo_SrvPosPrcTmp ,
                                             short AV30TFServicoFluxo_SrvPosPrcTmp_To ,
                                             short AV33TFServicoFluxo_SrvPosPrcPgm ,
                                             short AV34TFServicoFluxo_SrvPosPrcPgm_To ,
                                             short AV37TFServicoFluxo_SrvPosPrcCnc ,
                                             short AV38TFServicoFluxo_SrvPosPrcCnc_To ,
                                             short A1532ServicoFluxo_Ordem ,
                                             String A1527ServicoFluxo_SrvPosSigla ,
                                             short A1554ServicoFluxo_SrvPosPrcTmp ,
                                             short A1555ServicoFluxo_SrvPosPrcPgm ,
                                             short A1556ServicoFluxo_SrvPosPrcCnc ,
                                             int AV7ServicoFluxo_ServicoCod ,
                                             int A1522ServicoFluxo_ServicoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [11] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ServicoFluxo] T1 WITH (NOLOCK) LEFT JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[ServicoFluxo_ServicoPos])";
         scmdbuf = scmdbuf + " WHERE (T1.[ServicoFluxo_ServicoCod] = @AV7ServicoFluxo_ServicoCod)";
         if ( ! (0==AV21TFServicoFluxo_Ordem) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] >= @AV21TFServicoFluxo_Ordem)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV22TFServicoFluxo_Ordem_To) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] <= @AV22TFServicoFluxo_Ordem_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV26TFServicoFluxo_SrvPosSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFServicoFluxo_SrvPosSigla)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV25TFServicoFluxo_SrvPosSigla)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFServicoFluxo_SrvPosSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] = @AV26TFServicoFluxo_SrvPosSigla_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (0==AV29TFServicoFluxo_SrvPosPrcTmp) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercTmp] >= @AV29TFServicoFluxo_SrvPosPrcTmp)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV30TFServicoFluxo_SrvPosPrcTmp_To) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercTmp] <= @AV30TFServicoFluxo_SrvPosPrcTmp_To)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV33TFServicoFluxo_SrvPosPrcPgm) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercPgm] >= @AV33TFServicoFluxo_SrvPosPrcPgm)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV34TFServicoFluxo_SrvPosPrcPgm_To) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercPgm] <= @AV34TFServicoFluxo_SrvPosPrcPgm_To)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV37TFServicoFluxo_SrvPosPrcCnc) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercCnc] >= @AV37TFServicoFluxo_SrvPosPrcCnc)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV38TFServicoFluxo_SrvPosPrcCnc_To) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_PercCnc] <= @AV38TFServicoFluxo_SrvPosPrcCnc_To)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00L72(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (short)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (short)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
               case 1 :
                     return conditional_H00L73(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (short)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (short)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00L74 ;
          prmH00L74 = new Object[] {
          new Object[] {"@AV7ServicoFluxo_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00L72 ;
          prmH00L72 = new Object[] {
          new Object[] {"@AV7ServicoFluxo_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV22TFServicoFluxo_Ordem_To",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@lV25TFServicoFluxo_SrvPosSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV26TFServicoFluxo_SrvPosSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV29TFServicoFluxo_SrvPosPrcTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV30TFServicoFluxo_SrvPosPrcTmp_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV33TFServicoFluxo_SrvPosPrcPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV34TFServicoFluxo_SrvPosPrcPgm_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV37TFServicoFluxo_SrvPosPrcCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV38TFServicoFluxo_SrvPosPrcCnc_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00L73 ;
          prmH00L73 = new Object[] {
          new Object[] {"@AV7ServicoFluxo_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV22TFServicoFluxo_Ordem_To",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@lV25TFServicoFluxo_SrvPosSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV26TFServicoFluxo_SrvPosSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV29TFServicoFluxo_SrvPosPrcTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV30TFServicoFluxo_SrvPosPrcTmp_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV33TFServicoFluxo_SrvPosPrcPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV34TFServicoFluxo_SrvPosPrcPgm_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV37TFServicoFluxo_SrvPosPrcCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV38TFServicoFluxo_SrvPosPrcCnc_To",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00L72", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L72,11,0,true,false )
             ,new CursorDef("H00L73", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L73,1,0,true,false )
             ,new CursorDef("H00L74", "SELECT TOP 1 [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV7ServicoFluxo_ServicoCod ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L74,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
