/*
               File: type_SdtGAMDescription
        Description: GAMDescription
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 1:32:7.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMDescription : GxUserType, IGxExternalObject
   {
      public SdtGAMDescription( )
      {
         initialize();
      }

      public SdtGAMDescription( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMDescription_externalReference == null )
         {
            GAMDescription_externalReference = new Artech.Security.GAMDescription(context);
         }
         returntostring = "";
         returntostring = (String)(GAMDescription_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Language
      {
         get {
            if ( GAMDescription_externalReference == null )
            {
               GAMDescription_externalReference = new Artech.Security.GAMDescription(context);
            }
            return GAMDescription_externalReference.Language ;
         }

         set {
            if ( GAMDescription_externalReference == null )
            {
               GAMDescription_externalReference = new Artech.Security.GAMDescription(context);
            }
            GAMDescription_externalReference.Language = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMDescription_externalReference == null )
            {
               GAMDescription_externalReference = new Artech.Security.GAMDescription(context);
            }
            return GAMDescription_externalReference.Name ;
         }

         set {
            if ( GAMDescription_externalReference == null )
            {
               GAMDescription_externalReference = new Artech.Security.GAMDescription(context);
            }
            GAMDescription_externalReference.Name = value;
         }

      }

      public String gxTpr_Text
      {
         get {
            if ( GAMDescription_externalReference == null )
            {
               GAMDescription_externalReference = new Artech.Security.GAMDescription(context);
            }
            return GAMDescription_externalReference.Text ;
         }

         set {
            if ( GAMDescription_externalReference == null )
            {
               GAMDescription_externalReference = new Artech.Security.GAMDescription(context);
            }
            GAMDescription_externalReference.Text = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMDescription_externalReference == null )
            {
               GAMDescription_externalReference = new Artech.Security.GAMDescription(context);
            }
            return GAMDescription_externalReference ;
         }

         set {
            GAMDescription_externalReference = (Artech.Security.GAMDescription)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMDescription GAMDescription_externalReference=null ;
   }

}
