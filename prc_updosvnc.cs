/*
               File: PRC_UpdOSVnc
        Description: PRC_Upd OSVnc
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:16.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updosvnc : GXProcedure
   {
      public prc_updosvnc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updosvnc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo )
      {
         prc_updosvnc objprc_updosvnc;
         objprc_updosvnc = new prc_updosvnc();
         objprc_updosvnc.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_updosvnc.context.SetSubmitInitialConfig(context);
         objprc_updosvnc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updosvnc);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updosvnc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P005O2 */
         pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P005O2_A456ContagemResultado_Codigo[0];
            A489ContagemResultado_SistemaCod = P005O2_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P005O2_n489ContagemResultado_SistemaCod[0];
            A146Modulo_Codigo = P005O2_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P005O2_n146Modulo_Codigo[0];
            A465ContagemResultado_Link = P005O2_A465ContagemResultado_Link[0];
            n465ContagemResultado_Link = P005O2_n465ContagemResultado_Link[0];
            AV9ContagemResultado_SistemaCod = A489ContagemResultado_SistemaCod;
            AV10Modulo_Codigo = A146Modulo_Codigo;
            AV11ContagemResultado_Link = A465ContagemResultado_Link;
            /* Execute user subroutine: 'UPDOSVNC' */
            S111 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               this.cleanup();
               if (true) return;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'UPDOSVNC' Routine */
         /* Optimized UPDATE. */
         /* Using cursor P005O3 */
         pr_default.execute(1, new Object[] {n489ContagemResultado_SistemaCod, AV9ContagemResultado_SistemaCod, n146Modulo_Codigo, AV10Modulo_Codigo, n465ContagemResultado_Link, AV11ContagemResultado_Link, AV8ContagemResultado_Codigo});
         pr_default.close(1);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         /* End optimized UPDATE. */
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005O2_A456ContagemResultado_Codigo = new int[1] ;
         P005O2_A489ContagemResultado_SistemaCod = new int[1] ;
         P005O2_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P005O2_A146Modulo_Codigo = new int[1] ;
         P005O2_n146Modulo_Codigo = new bool[] {false} ;
         P005O2_A465ContagemResultado_Link = new String[] {""} ;
         P005O2_n465ContagemResultado_Link = new bool[] {false} ;
         A465ContagemResultado_Link = "";
         AV11ContagemResultado_Link = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updosvnc__default(),
            new Object[][] {
                new Object[] {
               P005O2_A456ContagemResultado_Codigo, P005O2_A489ContagemResultado_SistemaCod, P005O2_n489ContagemResultado_SistemaCod, P005O2_A146Modulo_Codigo, P005O2_n146Modulo_Codigo, P005O2_A465ContagemResultado_Link, P005O2_n465ContagemResultado_Link
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int A146Modulo_Codigo ;
      private int AV9ContagemResultado_SistemaCod ;
      private int AV10Modulo_Codigo ;
      private String scmdbuf ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n146Modulo_Codigo ;
      private bool n465ContagemResultado_Link ;
      private bool returnInSub ;
      private String A465ContagemResultado_Link ;
      private String AV11ContagemResultado_Link ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P005O2_A456ContagemResultado_Codigo ;
      private int[] P005O2_A489ContagemResultado_SistemaCod ;
      private bool[] P005O2_n489ContagemResultado_SistemaCod ;
      private int[] P005O2_A146Modulo_Codigo ;
      private bool[] P005O2_n146Modulo_Codigo ;
      private String[] P005O2_A465ContagemResultado_Link ;
      private bool[] P005O2_n465ContagemResultado_Link ;
   }

   public class prc_updosvnc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005O2 ;
          prmP005O2 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005O3 ;
          prmP005O3 = new Object[] {
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005O2", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_SistemaCod], [Modulo_Codigo], [ContagemResultado_Link] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005O2,1,0,true,true )
             ,new CursorDef("P005O3", "UPDATE [ContagemResultado] SET [ContagemResultado_SistemaCod]=@ContagemResultado_SistemaCod, [Modulo_Codigo]=@Modulo_Codigo, [ContagemResultado_Link]=@ContagemResultado_Link  WHERE [ContagemResultado_OSVinculada] = @AV8ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005O3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
       }
    }

 }

}
