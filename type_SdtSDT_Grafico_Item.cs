/*
               File: type_SdtSDT_Grafico_Item
        Description: SDT_Grafico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:0.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Grafico.Item" )]
   [XmlType(TypeName =  "SDT_Grafico.Item" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_Grafico_Item : GxUserType
   {
      public SdtSDT_Grafico_Item( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Grafico_Item_Nome = "";
      }

      public SdtSDT_Grafico_Item( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Grafico_Item deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Grafico_Item)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Grafico_Item obj ;
         obj = this;
         obj.gxTpr_Nome = deserialized.gxTpr_Nome;
         obj.gxTpr_Quantidade = deserialized.gxTpr_Quantidade;
         obj.gxTpr_Mes = deserialized.gxTpr_Mes;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Nome") )
               {
                  gxTv_SdtSDT_Grafico_Item_Nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Quantidade") )
               {
                  gxTv_SdtSDT_Grafico_Item_Quantidade = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mes") )
               {
                  if ( gxTv_SdtSDT_Grafico_Item_Mes == null )
                  {
                     gxTv_SdtSDT_Grafico_Item_Mes = new GxSimpleCollection();
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtSDT_Grafico_Item_Mes.readxmlcollection(oReader, "Mes", "Qtd");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Grafico.Item";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Nome", StringUtil.RTrim( gxTv_SdtSDT_Grafico_Item_Nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Quantidade", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Grafico_Item_Quantidade), 12, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( gxTv_SdtSDT_Grafico_Item_Mes != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtSDT_Grafico_Item_Mes.writexmlcollection(oWriter, "Mes", sNameSpace1, "Qtd", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Nome", gxTv_SdtSDT_Grafico_Item_Nome, false);
         AddObjectProperty("Quantidade", gxTv_SdtSDT_Grafico_Item_Quantidade, false);
         if ( gxTv_SdtSDT_Grafico_Item_Mes != null )
         {
            AddObjectProperty("Mes", gxTv_SdtSDT_Grafico_Item_Mes, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Nome" )]
      [  XmlElement( ElementName = "Nome"   )]
      public String gxTpr_Nome
      {
         get {
            return gxTv_SdtSDT_Grafico_Item_Nome ;
         }

         set {
            gxTv_SdtSDT_Grafico_Item_Nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Quantidade" )]
      [  XmlElement( ElementName = "Quantidade"   )]
      public long gxTpr_Quantidade
      {
         get {
            return gxTv_SdtSDT_Grafico_Item_Quantidade ;
         }

         set {
            gxTv_SdtSDT_Grafico_Item_Quantidade = (long)(value);
         }

      }

      [  SoapElement( ElementName = "Mes" )]
      [  XmlArray( ElementName = "Mes"  )]
      [  XmlArrayItemAttribute( Type= typeof( long ), ElementName= "Qtd"  , IsNullable=false)]
      public GxSimpleCollection gxTpr_Mes_GxSimpleCollection
      {
         get {
            if ( gxTv_SdtSDT_Grafico_Item_Mes == null )
            {
               gxTv_SdtSDT_Grafico_Item_Mes = new GxSimpleCollection();
            }
            return (GxSimpleCollection)gxTv_SdtSDT_Grafico_Item_Mes ;
         }

         set {
            if ( gxTv_SdtSDT_Grafico_Item_Mes == null )
            {
               gxTv_SdtSDT_Grafico_Item_Mes = new GxSimpleCollection();
            }
            gxTv_SdtSDT_Grafico_Item_Mes = (GxSimpleCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Mes
      {
         get {
            if ( gxTv_SdtSDT_Grafico_Item_Mes == null )
            {
               gxTv_SdtSDT_Grafico_Item_Mes = new GxSimpleCollection();
            }
            return gxTv_SdtSDT_Grafico_Item_Mes ;
         }

         set {
            gxTv_SdtSDT_Grafico_Item_Mes = value;
         }

      }

      public void gxTv_SdtSDT_Grafico_Item_Mes_SetNull( )
      {
         gxTv_SdtSDT_Grafico_Item_Mes = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Grafico_Item_Mes_IsNull( )
      {
         if ( gxTv_SdtSDT_Grafico_Item_Mes == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_Grafico_Item_Nome = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected long gxTv_SdtSDT_Grafico_Item_Quantidade ;
      protected String gxTv_SdtSDT_Grafico_Item_Nome ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( double ))]
      protected IGxCollection gxTv_SdtSDT_Grafico_Item_Mes=null ;
   }

   [DataContract(Name = @"SDT_Grafico.Item", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_Grafico_Item_RESTInterface : GxGenericCollectionItem<SdtSDT_Grafico_Item>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Grafico_Item_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Grafico_Item_RESTInterface( SdtSDT_Grafico_Item psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Nome" , Order = 0 )]
      public String gxTpr_Nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Nome) ;
         }

         set {
            sdt.gxTpr_Nome = (String)(value);
         }

      }

      [DataMember( Name = "Quantidade" , Order = 1 )]
      public String gxTpr_Quantidade
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Quantidade), 12, 0)) ;
         }

         set {
            sdt.gxTpr_Quantidade = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Mes" , Order = 2 )]
      public GxSimpleCollection gxTpr_Mes
      {
         get {
            return (GxSimpleCollection)(sdt.gxTpr_Mes) ;
         }

         set {
            sdt.gxTpr_Mes = value;
         }

      }

      public SdtSDT_Grafico_Item sdt
      {
         get {
            return (SdtSDT_Grafico_Item)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Grafico_Item() ;
         }
      }

   }

}
