/*
               File: type_SdtQueryViewerAxes_Axis_AxisOrder
        Description: QueryViewerAxes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:57.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerAxes.Axis.AxisOrder" )]
   [XmlType(TypeName =  "QueryViewerAxes.Axis.AxisOrder" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtQueryViewerAxes_Axis_AxisOrder : GxUserType
   {
      public SdtQueryViewerAxes_Axis_AxisOrder( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Type = "";
      }

      public SdtQueryViewerAxes_Axis_AxisOrder( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerAxes_Axis_AxisOrder deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerAxes_Axis_AxisOrder)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerAxes_Axis_AxisOrder obj ;
         obj = this;
         obj.gxTpr_Type = deserialized.gxTpr_Type;
         obj.gxTpr_Values = deserialized.gxTpr_Values;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Type") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Type = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Values") )
               {
                  if ( gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values == null )
                  {
                     gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values = new GxSimpleCollection();
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values.readxmlcollection(oReader, "Values", "Value");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerAxes.Axis.AxisOrder";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Type", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Type));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values.writexmlcollection(oWriter, "Values", sNameSpace1, "Value", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Type", gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Type, false);
         if ( gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values != null )
         {
            AddObjectProperty("Values", gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Type" )]
      [  XmlElement( ElementName = "Type"   )]
      public String gxTpr_Type
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Type ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Type = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Values" )]
      [  XmlArray( ElementName = "Values"  )]
      [  XmlArrayItemAttribute( Type= typeof( String ), ElementName= "Value"  , IsNullable=false)]
      public GxSimpleCollection gxTpr_Values_GxSimpleCollection
      {
         get {
            if ( gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values = new GxSimpleCollection();
            }
            return (GxSimpleCollection)gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values ;
         }

         set {
            if ( gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values = new GxSimpleCollection();
            }
            gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values = (GxSimpleCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Values
      {
         get {
            if ( gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values = new GxSimpleCollection();
            }
            return gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values = value;
         }

      }

      public void gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values_SetNull( )
      {
         gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values_IsNull( )
      {
         if ( gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Type = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Type ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( String ))]
      protected IGxCollection gxTv_SdtQueryViewerAxes_Axis_AxisOrder_Values=null ;
   }

   [DataContract(Name = @"QueryViewerAxes.Axis.AxisOrder", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtQueryViewerAxes_Axis_AxisOrder_RESTInterface : GxGenericCollectionItem<SdtQueryViewerAxes_Axis_AxisOrder>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerAxes_Axis_AxisOrder_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerAxes_Axis_AxisOrder_RESTInterface( SdtQueryViewerAxes_Axis_AxisOrder psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Type" , Order = 0 )]
      public String gxTpr_Type
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Type) ;
         }

         set {
            sdt.gxTpr_Type = (String)(value);
         }

      }

      [DataMember( Name = "Values" , Order = 1 )]
      public GxSimpleCollection gxTpr_Values
      {
         get {
            return (GxSimpleCollection)(sdt.gxTpr_Values) ;
         }

         set {
            sdt.gxTpr_Values = value;
         }

      }

      public SdtQueryViewerAxes_Axis_AxisOrder sdt
      {
         get {
            return (SdtQueryViewerAxes_Axis_AxisOrder)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerAxes_Axis_AxisOrder() ;
         }
      }

   }

}
