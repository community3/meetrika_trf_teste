/*
               File: TecnologiaAmbienteTecnologicoWC
        Description: Tecnologia Ambiente Tecnologico WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:15.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tecnologiaambientetecnologicowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public tecnologiaambientetecnologicowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public tecnologiaambientetecnologicowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Tecnologia_Codigo )
      {
         this.AV7Tecnologia_Codigo = aP0_Tecnologia_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         chkAmbienteTecnologicoTecnologias_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Tecnologia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tecnologia_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Tecnologia_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_59 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_59_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_59_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV19AmbienteTecnologico_Descricao1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AmbienteTecnologico_Descricao1", AV19AmbienteTecnologico_Descricao1);
                  AV21DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
                  AV24AmbienteTecnologico_Descricao2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24AmbienteTecnologico_Descricao2", AV24AmbienteTecnologico_Descricao2);
                  AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
                  AV39TFAmbienteTecnologico_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAmbienteTecnologico_Descricao", AV39TFAmbienteTecnologico_Descricao);
                  AV40TFAmbienteTecnologico_Descricao_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFAmbienteTecnologico_Descricao_Sel", AV40TFAmbienteTecnologico_Descricao_Sel);
                  AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel", StringUtil.Str( (decimal)(AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel), 1, 0));
                  AV7Tecnologia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tecnologia_Codigo), 6, 0)));
                  AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
                  AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace", AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace);
                  AV51Pgmname = GetNextPar( );
                  AV35AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35AreaTrabalho_Codigo), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV31DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
                  AV30DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
                  A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV24AmbienteTecnologico_Descricao2, AV20DynamicFiltersEnabled2, AV39TFAmbienteTecnologico_Descricao, AV40TFAmbienteTecnologico_Descricao_Sel, AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel, AV7Tecnologia_Codigo, AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace, AV51Pgmname, AV35AreaTrabalho_Codigo, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAAH2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV51Pgmname = "TecnologiaAmbienteTecnologicoWC";
               context.Gx_err = 0;
               WSAH2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Tecnologia Ambiente Tecnologico WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822501542");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tecnologiaambientetecnologicowc.aspx") + "?" + UrlEncode("" +AV7Tecnologia_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vAMBIENTETECNOLOGICO_DESCRICAO1", AV19AmbienteTecnologico_Descricao1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vAMBIENTETECNOLOGICO_DESCRICAO2", AV24AmbienteTecnologico_Descricao2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO", AV39TFAmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL", AV40TFAmbienteTecnologico_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_59", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_59), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV45DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV45DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA", AV38AmbienteTecnologico_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA", AV38AmbienteTecnologico_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLEFILTERDATA", AV42AmbienteTecnologicoTecnologias_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLEFILTERDATA", AV42AmbienteTecnologicoTecnologias_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Tecnologia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Tecnologia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTECNOLOGIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Tecnologia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV51Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Rangefilterfrom", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Rangefilterto", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Caption", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Cls", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologicotecnologias_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologicotecnologias_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologicotecnologias_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologicotecnologias_ativo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologicotecnologias_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_ambientetecnologicotecnologias_ativo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Loadingdata", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Rangefilterfrom", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Rangefilterto", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Noresultsfound", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormAH2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("tecnologiaambientetecnologicowc.js", "?202042822501655");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "TecnologiaAmbienteTecnologicoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tecnologia Ambiente Tecnologico WC" ;
      }

      protected void WBAH0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "tecnologiaambientetecnologicowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_AH2( true) ;
         }
         else
         {
            wb_table1_2_AH2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AH2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTecnologia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A131Tecnologia_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTecnologia_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtTecnologia_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(68, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_descricao_Internalname, AV39TFAmbienteTecnologico_Descricao, StringUtil.RTrim( context.localUtil.Format( AV39TFAmbienteTecnologico_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,69);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_descricao_sel_Internalname, AV40TFAmbienteTecnologico_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV40TFAmbienteTecnologico_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologicotecnologias_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,71);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologicotecnologias_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologicotecnologias_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname, AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", 0, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologicotecnologias_ativotitlecontrolidtoreplace_Internalname, AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", 0, edtavDdo_ambientetecnologicotecnologias_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_TecnologiaAmbienteTecnologicoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTAH2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Tecnologia Ambiente Tecnologico WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPAH0( ) ;
            }
         }
      }

      protected void WSAH2( )
      {
         STARTAH2( ) ;
         EVTAH2( ) ;
      }

      protected void EVTAH2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11AH2 */
                                    E11AH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12AH2 */
                                    E12AH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13AH2 */
                                    E13AH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14AH2 */
                                    E14AH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15AH2 */
                                    E15AH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16AH2 */
                                    E16AH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17AH2 */
                                    E17AH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18AH2 */
                                    E18AH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19AH2 */
                                    E19AH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20AH2 */
                                    E20AH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAH0( ) ;
                              }
                              nGXsfl_59_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_59_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_59_idx), 4, 0)), 4, "0");
                              SubsflControlProps_592( ) ;
                              A351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_Codigo_Internalname), ",", "."));
                              A352AmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtAmbienteTecnologico_Descricao_Internalname));
                              A354AmbienteTecnologicoTecnologias_Ativo = StringUtil.StrToBool( cgiGet( chkAmbienteTecnologicoTecnologias_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E21AH2 */
                                          E21AH2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E22AH2 */
                                          E22AH2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23AH2 */
                                          E23AH2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ambientetecnologico_descricao1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vAMBIENTETECNOLOGICO_DESCRICAO1"), AV19AmbienteTecnologico_Descricao1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ambientetecnologico_descricao2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vAMBIENTETECNOLOGICO_DESCRICAO2"), AV24AmbienteTecnologico_Descricao2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfambientetecnologico_descricao Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO"), AV39TFAmbienteTecnologico_Descricao) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfambientetecnologico_descricao_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL"), AV40TFAmbienteTecnologico_Descricao_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfambientetecnologicotecnologias_ativo_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPAH0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEAH2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormAH2( ) ;
            }
         }
      }

      protected void PAAH2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("AMBIENTETECNOLOGICO_DESCRICAO", "Ambiente Tecnol�gico", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("AMBIENTETECNOLOGICO_DESCRICAO", "Ambiente Tecnol�gico", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            GXCCtl = "AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_" + sGXsfl_59_idx;
            chkAmbienteTecnologicoTecnologias_Ativo.Name = GXCCtl;
            chkAmbienteTecnologicoTecnologias_Ativo.WebTags = "";
            chkAmbienteTecnologicoTecnologias_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAmbienteTecnologicoTecnologias_Ativo_Internalname, "TitleCaption", chkAmbienteTecnologicoTecnologias_Ativo.Caption);
            chkAmbienteTecnologicoTecnologias_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_592( ) ;
         while ( nGXsfl_59_idx <= nRC_GXsfl_59 )
         {
            sendrow_592( ) ;
            nGXsfl_59_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_59_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_59_idx+1));
            sGXsfl_59_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_59_idx), 4, 0)), 4, "0");
            SubsflControlProps_592( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV19AmbienteTecnologico_Descricao1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       String AV24AmbienteTecnologico_Descricao2 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       String AV39TFAmbienteTecnologico_Descricao ,
                                       String AV40TFAmbienteTecnologico_Descricao_Sel ,
                                       short AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel ,
                                       int AV7Tecnologia_Codigo ,
                                       String AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace ,
                                       String AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace ,
                                       String AV51Pgmname ,
                                       int AV35AreaTrabalho_Codigo ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV31DynamicFiltersIgnoreFirst ,
                                       bool AV30DynamicFiltersRemoving ,
                                       int A351AmbienteTecnologico_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFAH2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AMBIENTETECNOLOGICO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"AMBIENTETECNOLOGICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO", GetSecureSignedToken( sPrefix, A354AmbienteTecnologicoTecnologias_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO", StringUtil.BoolToStr( A354AmbienteTecnologicoTecnologias_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAH2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV51Pgmname = "TecnologiaAmbienteTecnologicoWC";
         context.Gx_err = 0;
      }

      protected void RFAH2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 59;
         /* Execute user event: E22AH2 */
         E22AH2 ();
         nGXsfl_59_idx = 1;
         sGXsfl_59_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_59_idx), 4, 0)), 4, "0");
         SubsflControlProps_592( ) ;
         nGXsfl_59_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_592( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV19AmbienteTecnologico_Descricao1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV24AmbienteTecnologico_Descricao2 ,
                                                 AV40TFAmbienteTecnologico_Descricao_Sel ,
                                                 AV39TFAmbienteTecnologico_Descricao ,
                                                 AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel ,
                                                 A352AmbienteTecnologico_Descricao ,
                                                 A354AmbienteTecnologicoTecnologias_Ativo ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A131Tecnologia_Codigo ,
                                                 AV7Tecnologia_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV19AmbienteTecnologico_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV19AmbienteTecnologico_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AmbienteTecnologico_Descricao1", AV19AmbienteTecnologico_Descricao1);
            lV24AmbienteTecnologico_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV24AmbienteTecnologico_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24AmbienteTecnologico_Descricao2", AV24AmbienteTecnologico_Descricao2);
            lV39TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV39TFAmbienteTecnologico_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAmbienteTecnologico_Descricao", AV39TFAmbienteTecnologico_Descricao);
            /* Using cursor H00AH2 */
            pr_default.execute(0, new Object[] {AV7Tecnologia_Codigo, lV19AmbienteTecnologico_Descricao1, lV24AmbienteTecnologico_Descricao2, lV39TFAmbienteTecnologico_Descricao, AV40TFAmbienteTecnologico_Descricao_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_59_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A131Tecnologia_Codigo = H00AH2_A131Tecnologia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
               A354AmbienteTecnologicoTecnologias_Ativo = H00AH2_A354AmbienteTecnologicoTecnologias_Ativo[0];
               A352AmbienteTecnologico_Descricao = H00AH2_A352AmbienteTecnologico_Descricao[0];
               A351AmbienteTecnologico_Codigo = H00AH2_A351AmbienteTecnologico_Codigo[0];
               A352AmbienteTecnologico_Descricao = H00AH2_A352AmbienteTecnologico_Descricao[0];
               /* Execute user event: E23AH2 */
               E23AH2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 59;
            WBAH0( ) ;
         }
         nGXsfl_59_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV19AmbienteTecnologico_Descricao1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV24AmbienteTecnologico_Descricao2 ,
                                              AV40TFAmbienteTecnologico_Descricao_Sel ,
                                              AV39TFAmbienteTecnologico_Descricao ,
                                              AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A354AmbienteTecnologicoTecnologias_Ativo ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A131Tecnologia_Codigo ,
                                              AV7Tecnologia_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV19AmbienteTecnologico_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV19AmbienteTecnologico_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AmbienteTecnologico_Descricao1", AV19AmbienteTecnologico_Descricao1);
         lV24AmbienteTecnologico_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV24AmbienteTecnologico_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24AmbienteTecnologico_Descricao2", AV24AmbienteTecnologico_Descricao2);
         lV39TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV39TFAmbienteTecnologico_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAmbienteTecnologico_Descricao", AV39TFAmbienteTecnologico_Descricao);
         /* Using cursor H00AH3 */
         pr_default.execute(1, new Object[] {AV7Tecnologia_Codigo, lV19AmbienteTecnologico_Descricao1, lV24AmbienteTecnologico_Descricao2, lV39TFAmbienteTecnologico_Descricao, AV40TFAmbienteTecnologico_Descricao_Sel});
         GRID_nRecordCount = H00AH3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV24AmbienteTecnologico_Descricao2, AV20DynamicFiltersEnabled2, AV39TFAmbienteTecnologico_Descricao, AV40TFAmbienteTecnologico_Descricao_Sel, AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel, AV7Tecnologia_Codigo, AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace, AV51Pgmname, AV35AreaTrabalho_Codigo, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV24AmbienteTecnologico_Descricao2, AV20DynamicFiltersEnabled2, AV39TFAmbienteTecnologico_Descricao, AV40TFAmbienteTecnologico_Descricao_Sel, AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel, AV7Tecnologia_Codigo, AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace, AV51Pgmname, AV35AreaTrabalho_Codigo, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV24AmbienteTecnologico_Descricao2, AV20DynamicFiltersEnabled2, AV39TFAmbienteTecnologico_Descricao, AV40TFAmbienteTecnologico_Descricao_Sel, AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel, AV7Tecnologia_Codigo, AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace, AV51Pgmname, AV35AreaTrabalho_Codigo, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV24AmbienteTecnologico_Descricao2, AV20DynamicFiltersEnabled2, AV39TFAmbienteTecnologico_Descricao, AV40TFAmbienteTecnologico_Descricao_Sel, AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel, AV7Tecnologia_Codigo, AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace, AV51Pgmname, AV35AreaTrabalho_Codigo, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV24AmbienteTecnologico_Descricao2, AV20DynamicFiltersEnabled2, AV39TFAmbienteTecnologico_Descricao, AV40TFAmbienteTecnologico_Descricao_Sel, AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel, AV7Tecnologia_Codigo, AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace, AV51Pgmname, AV35AreaTrabalho_Codigo, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPAH0( )
      {
         /* Before Start, stand alone formulas. */
         AV51Pgmname = "TecnologiaAmbienteTecnologicoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E21AH2 */
         E21AH2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV45DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA"), AV38AmbienteTecnologico_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLEFILTERDATA"), AV42AmbienteTecnologicoTecnologias_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAREATRABALHO_CODIGO");
               GX_FocusControl = edtavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35AreaTrabalho_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35AreaTrabalho_Codigo), 6, 0)));
            }
            else
            {
               AV35AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35AreaTrabalho_Codigo), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV19AmbienteTecnologico_Descricao1 = StringUtil.Upper( cgiGet( edtavAmbientetecnologico_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AmbienteTecnologico_Descricao1", AV19AmbienteTecnologico_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            AV24AmbienteTecnologico_Descricao2 = StringUtil.Upper( cgiGet( edtavAmbientetecnologico_descricao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24AmbienteTecnologico_Descricao2", AV24AmbienteTecnologico_Descricao2);
            A131Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTecnologia_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV39TFAmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAmbienteTecnologico_Descricao", AV39TFAmbienteTecnologico_Descricao);
            AV40TFAmbienteTecnologico_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFAmbienteTecnologico_Descricao_Sel", AV40TFAmbienteTecnologico_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologicotecnologias_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologicotecnologias_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL");
               GX_FocusControl = edtavTfambientetecnologicotecnologias_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel", StringUtil.Str( (decimal)(AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel), 1, 0));
            }
            else
            {
               AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfambientetecnologicotecnologias_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel", StringUtil.Str( (decimal)(AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel), 1, 0));
            }
            AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
            AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologicotecnologias_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace", AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_59 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_59"), ",", "."));
            AV47GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV48GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Tecnologia_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_ambientetecnologico_descricao_Caption = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Caption");
            Ddo_ambientetecnologico_descricao_Tooltip = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Tooltip");
            Ddo_ambientetecnologico_descricao_Cls = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cls");
            Ddo_ambientetecnologico_descricao_Filteredtext_set = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_set");
            Ddo_ambientetecnologico_descricao_Selectedvalue_set = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_set");
            Ddo_ambientetecnologico_descricao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Dropdownoptionstype");
            Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortasc"));
            Ddo_ambientetecnologico_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortdsc"));
            Ddo_ambientetecnologico_descricao_Sortedstatus = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortedstatus");
            Ddo_ambientetecnologico_descricao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includefilter"));
            Ddo_ambientetecnologico_descricao_Filtertype = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filtertype");
            Ddo_ambientetecnologico_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filterisrange"));
            Ddo_ambientetecnologico_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includedatalist"));
            Ddo_ambientetecnologico_descricao_Datalisttype = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalisttype");
            Ddo_ambientetecnologico_descricao_Datalistfixedvalues = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistfixedvalues");
            Ddo_ambientetecnologico_descricao_Datalistproc = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistproc");
            Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_ambientetecnologico_descricao_Sortasc = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortasc");
            Ddo_ambientetecnologico_descricao_Sortdsc = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortdsc");
            Ddo_ambientetecnologico_descricao_Loadingdata = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Loadingdata");
            Ddo_ambientetecnologico_descricao_Cleanfilter = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cleanfilter");
            Ddo_ambientetecnologico_descricao_Rangefilterfrom = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Rangefilterfrom");
            Ddo_ambientetecnologico_descricao_Rangefilterto = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Rangefilterto");
            Ddo_ambientetecnologico_descricao_Noresultsfound = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Noresultsfound");
            Ddo_ambientetecnologico_descricao_Searchbuttontext = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Searchbuttontext");
            Ddo_ambientetecnologicotecnologias_ativo_Caption = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Caption");
            Ddo_ambientetecnologicotecnologias_ativo_Tooltip = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Tooltip");
            Ddo_ambientetecnologicotecnologias_ativo_Cls = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Cls");
            Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_set = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Selectedvalue_set");
            Ddo_ambientetecnologicotecnologias_ativo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Dropdownoptionstype");
            Ddo_ambientetecnologicotecnologias_ativo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Titlecontrolidtoreplace");
            Ddo_ambientetecnologicotecnologias_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Includesortasc"));
            Ddo_ambientetecnologicotecnologias_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Includesortdsc"));
            Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Sortedstatus");
            Ddo_ambientetecnologicotecnologias_ativo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Includefilter"));
            Ddo_ambientetecnologicotecnologias_ativo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Filterisrange"));
            Ddo_ambientetecnologicotecnologias_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Includedatalist"));
            Ddo_ambientetecnologicotecnologias_ativo_Datalisttype = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Datalisttype");
            Ddo_ambientetecnologicotecnologias_ativo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Datalistfixedvalues");
            Ddo_ambientetecnologicotecnologias_ativo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_ambientetecnologicotecnologias_ativo_Sortasc = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Sortasc");
            Ddo_ambientetecnologicotecnologias_ativo_Sortdsc = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Sortdsc");
            Ddo_ambientetecnologicotecnologias_ativo_Loadingdata = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Loadingdata");
            Ddo_ambientetecnologicotecnologias_ativo_Cleanfilter = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Cleanfilter");
            Ddo_ambientetecnologicotecnologias_ativo_Rangefilterfrom = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Rangefilterfrom");
            Ddo_ambientetecnologicotecnologias_ativo_Rangefilterto = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Rangefilterto");
            Ddo_ambientetecnologicotecnologias_ativo_Noresultsfound = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Noresultsfound");
            Ddo_ambientetecnologicotecnologias_ativo_Searchbuttontext = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_ambientetecnologico_descricao_Activeeventkey = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Activeeventkey");
            Ddo_ambientetecnologico_descricao_Filteredtext_get = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_get");
            Ddo_ambientetecnologico_descricao_Selectedvalue_get = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_get");
            Ddo_ambientetecnologicotecnologias_ativo_Activeeventkey = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Activeeventkey");
            Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_get = cgiGet( sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vAMBIENTETECNOLOGICO_DESCRICAO1"), AV19AmbienteTecnologico_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vAMBIENTETECNOLOGICO_DESCRICAO2"), AV24AmbienteTecnologico_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO"), AV39TFAmbienteTecnologico_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL"), AV40TFAmbienteTecnologico_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E21AH2 */
         E21AH2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21AH2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfambientetecnologico_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfambientetecnologico_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_descricao_Visible), 5, 0)));
         edtavTfambientetecnologico_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfambientetecnologico_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_descricao_sel_Visible), 5, 0)));
         edtavTfambientetecnologicotecnologias_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfambientetecnologicotecnologias_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologicotecnologias_ativo_sel_Visible), 5, 0)));
         Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologico_descricao_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace);
         AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_ambientetecnologicotecnologias_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologicoTecnologias_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologicotecnologias_ativo_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologicotecnologias_ativo_Titlecontrolidtoreplace);
         AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace = Ddo_ambientetecnologicotecnologias_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace", AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace);
         edtavDdo_ambientetecnologicotecnologias_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_ambientetecnologicotecnologias_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologicotecnologias_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         edtTecnologia_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTecnologia_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTecnologia_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Tecnologico Tecnologias_Ativo", 0);
         cmbavOrderedby.addItem("2", "Ambiente Tecnol�gico", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV45DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV45DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E22AH2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV38AmbienteTecnologico_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42AmbienteTecnologicoTecnologias_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtAmbienteTecnologico_Descricao_Titleformat = 2;
         edtAmbienteTecnologico_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ambiente Tecnol�gico", AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAmbienteTecnologico_Descricao_Internalname, "Title", edtAmbienteTecnologico_Descricao_Title);
         chkAmbienteTecnologicoTecnologias_Ativo_Titleformat = 2;
         chkAmbienteTecnologicoTecnologias_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Activo", AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAmbienteTecnologicoTecnologias_Ativo_Internalname, "Title", chkAmbienteTecnologicoTecnologias_Ativo.Title.Text);
         AV47GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47GridCurrentPage), 10, 0)));
         AV48GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48GridPageCount), 10, 0)));
         AV35AreaTrabalho_Codigo = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV38AmbienteTecnologico_DescricaoTitleFilterData", AV38AmbienteTecnologico_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV42AmbienteTecnologicoTecnologias_AtivoTitleFilterData", AV42AmbienteTecnologicoTecnologias_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11AH2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV46PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV46PageToGo) ;
         }
      }

      protected void E12AH2( )
      {
         /* Ddo_ambientetecnologico_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_ambientetecnologico_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_ambientetecnologico_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFAmbienteTecnologico_Descricao = Ddo_ambientetecnologico_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAmbienteTecnologico_Descricao", AV39TFAmbienteTecnologico_Descricao);
            AV40TFAmbienteTecnologico_Descricao_Sel = Ddo_ambientetecnologico_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFAmbienteTecnologico_Descricao_Sel", AV40TFAmbienteTecnologico_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13AH2( )
      {
         /* Ddo_ambientetecnologicotecnologias_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologicotecnologias_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologicotecnologias_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologicotecnologias_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologicotecnologias_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologicotecnologias_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel = (short)(NumberUtil.Val( Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel", StringUtil.Str( (decimal)(AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E23AH2( )
      {
         /* Grid_Load Routine */
         edtAmbienteTecnologico_Descricao_Link = formatLink("viewambientetecnologico.aspx") + "?" + UrlEncode("" +A351AmbienteTecnologico_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 59;
         }
         sendrow_592( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_59_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(59, GridRow);
         }
      }

      protected void E14AH2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E18AH2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15AH2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV24AmbienteTecnologico_Descricao2, AV20DynamicFiltersEnabled2, AV39TFAmbienteTecnologico_Descricao, AV40TFAmbienteTecnologico_Descricao_Sel, AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel, AV7Tecnologia_Codigo, AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace, AV51Pgmname, AV35AreaTrabalho_Codigo, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E19AH2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16AH2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV24AmbienteTecnologico_Descricao2, AV20DynamicFiltersEnabled2, AV39TFAmbienteTecnologico_Descricao, AV40TFAmbienteTecnologico_Descricao_Sel, AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel, AV7Tecnologia_Codigo, AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace, AV51Pgmname, AV35AreaTrabalho_Codigo, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E20AH2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17AH2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_ambientetecnologico_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
         Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologicotecnologias_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 2 )
         {
            Ddo_ambientetecnologico_descricao_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
         }
         else if ( AV14OrderedBy == 1 )
         {
            Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologicotecnologias_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavAmbientetecnologico_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAmbientetecnologico_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            edtavAmbientetecnologico_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAmbientetecnologico_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavAmbientetecnologico_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAmbientetecnologico_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            edtavAmbientetecnologico_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAmbientetecnologico_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao2_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV24AmbienteTecnologico_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24AmbienteTecnologico_Descricao2", AV24AmbienteTecnologico_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV35AreaTrabalho_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35AreaTrabalho_Codigo), 6, 0)));
         AV39TFAmbienteTecnologico_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAmbienteTecnologico_Descricao", AV39TFAmbienteTecnologico_Descricao);
         Ddo_ambientetecnologico_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologico_descricao_Internalname, "FilteredText_set", Ddo_ambientetecnologico_descricao_Filteredtext_set);
         AV40TFAmbienteTecnologico_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFAmbienteTecnologico_Descricao_Sel", AV40TFAmbienteTecnologico_Descricao_Sel);
         Ddo_ambientetecnologico_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologico_descricao_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_descricao_Selectedvalue_set);
         AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel", StringUtil.Str( (decimal)(AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel), 1, 0));
         Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologicotecnologias_ativo_Internalname, "SelectedValue_set", Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV19AmbienteTecnologico_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AmbienteTecnologico_Descricao1", AV19AmbienteTecnologico_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get(AV51Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV51Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV34Session.Get(AV51Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV52GXV1 = 1;
         while ( AV52GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV52GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "AREATRABALHO_CODIGO") == 0 )
            {
               AV35AreaTrabalho_Codigo = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35AreaTrabalho_Codigo), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV39TFAmbienteTecnologico_Descricao = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAmbienteTecnologico_Descricao", AV39TFAmbienteTecnologico_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFAmbienteTecnologico_Descricao)) )
               {
                  Ddo_ambientetecnologico_descricao_Filteredtext_set = AV39TFAmbienteTecnologico_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologico_descricao_Internalname, "FilteredText_set", Ddo_ambientetecnologico_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL") == 0 )
            {
               AV40TFAmbienteTecnologico_Descricao_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFAmbienteTecnologico_Descricao_Sel", AV40TFAmbienteTecnologico_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAmbienteTecnologico_Descricao_Sel)) )
               {
                  Ddo_ambientetecnologico_descricao_Selectedvalue_set = AV40TFAmbienteTecnologico_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologico_descricao_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL") == 0 )
            {
               AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel", StringUtil.Str( (decimal)(AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel), 1, 0));
               if ( ! (0==AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel) )
               {
                  Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_ambientetecnologicotecnologias_ativo_Internalname, "SelectedValue_set", Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_set);
               }
            }
            AV52GXV1 = (int)(AV52GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV19AmbienteTecnologico_Descricao1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19AmbienteTecnologico_Descricao1", AV19AmbienteTecnologico_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
               {
                  AV24AmbienteTecnologico_Descricao2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24AmbienteTecnologico_Descricao2", AV24AmbienteTecnologico_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV34Session.Get(AV51Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV35AreaTrabalho_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "AREATRABALHO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35AreaTrabalho_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFAmbienteTecnologico_Descricao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_DESCRICAO";
            AV12GridStateFilterValue.gxTpr_Value = AV39TFAmbienteTecnologico_Descricao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAmbienteTecnologico_Descricao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV40TFAmbienteTecnologico_Descricao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Tecnologia_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&TECNOLOGIA_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Tecnologia_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV51Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19AmbienteTecnologico_Descricao1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19AmbienteTecnologico_Descricao1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24AmbienteTecnologico_Descricao2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24AmbienteTecnologico_Descricao2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV51Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "AmbienteTecnologicoTecnologias";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Tecnologia_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Tecnologia_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV34Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_AH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_AH2( true) ;
         }
         else
         {
            wb_table2_8_AH2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_AH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_56_AH2( true) ;
         }
         else
         {
            wb_table3_56_AH2( false) ;
         }
         return  ;
      }

      protected void wb_table3_56_AH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AH2e( true) ;
         }
         else
         {
            wb_table1_2_AH2e( false) ;
         }
      }

      protected void wb_table3_56_AH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"59\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Ambiente Tecnol�gico") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAmbienteTecnologico_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtAmbienteTecnologico_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAmbienteTecnologico_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkAmbienteTecnologicoTecnologias_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkAmbienteTecnologicoTecnologias_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkAmbienteTecnologicoTecnologias_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A352AmbienteTecnologico_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAmbienteTecnologico_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtAmbienteTecnologico_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A354AmbienteTecnologicoTecnologias_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkAmbienteTecnologicoTecnologias_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkAmbienteTecnologicoTecnologias_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 59 )
         {
            wbEnd = 0;
            nRC_GXsfl_59 = (short)(nGXsfl_59_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_56_AH2e( true) ;
         }
         else
         {
            wb_table3_56_AH2e( false) ;
         }
      }

      protected void wb_table2_8_AH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_AH2( true) ;
         }
         else
         {
            wb_table4_11_AH2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_AH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "", true, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_20_AH2( true) ;
         }
         else
         {
            wb_table5_20_AH2( false) ;
         }
         return  ;
      }

      protected void wb_table5_20_AH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_AH2e( true) ;
         }
         else
         {
            wb_table2_8_AH2e( false) ;
         }
      }

      protected void wb_table5_20_AH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextareatrabalho_codigo_Internalname, "C�digo Area de Trabalho", "", "", lblFiltertextareatrabalho_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFixedFilterDescription", 0, "", 1, 1, 0, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35AreaTrabalho_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_29_AH2( true) ;
         }
         else
         {
            wb_table6_29_AH2( false) ;
         }
         return  ;
      }

      protected void wb_table6_29_AH2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_20_AH2e( true) ;
         }
         else
         {
            wb_table5_20_AH2e( false) ;
         }
      }

      protected void wb_table6_29_AH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAmbientetecnologico_descricao1_Internalname, AV19AmbienteTecnologico_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV19AmbienteTecnologico_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAmbientetecnologico_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAmbientetecnologico_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'" + sGXsfl_59_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAmbientetecnologico_descricao2_Internalname, AV24AmbienteTecnologico_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV24AmbienteTecnologico_Descricao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAmbientetecnologico_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAmbientetecnologico_descricao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TecnologiaAmbienteTecnologicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_29_AH2e( true) ;
         }
         else
         {
            wb_table6_29_AH2e( false) ;
         }
      }

      protected void wb_table4_11_AH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_AH2e( true) ;
         }
         else
         {
            wb_table4_11_AH2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Tecnologia_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tecnologia_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAH2( ) ;
         WSAH2( ) ;
         WEAH2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Tecnologia_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAAH2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "tecnologiaambientetecnologicowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAAH2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Tecnologia_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tecnologia_Codigo), 6, 0)));
         }
         wcpOAV7Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Tecnologia_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Tecnologia_Codigo != wcpOAV7Tecnologia_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Tecnologia_Codigo = AV7Tecnologia_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Tecnologia_Codigo = cgiGet( sPrefix+"AV7Tecnologia_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Tecnologia_Codigo) > 0 )
         {
            AV7Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Tecnologia_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tecnologia_Codigo), 6, 0)));
         }
         else
         {
            AV7Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Tecnologia_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAAH2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSAH2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSAH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Tecnologia_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Tecnologia_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Tecnologia_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Tecnologia_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Tecnologia_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEAH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822502098");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("tecnologiaambientetecnologicowc.js", "?202042822502098");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_592( )
      {
         edtAmbienteTecnologico_Codigo_Internalname = sPrefix+"AMBIENTETECNOLOGICO_CODIGO_"+sGXsfl_59_idx;
         edtAmbienteTecnologico_Descricao_Internalname = sPrefix+"AMBIENTETECNOLOGICO_DESCRICAO_"+sGXsfl_59_idx;
         chkAmbienteTecnologicoTecnologias_Ativo_Internalname = sPrefix+"AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_"+sGXsfl_59_idx;
      }

      protected void SubsflControlProps_fel_592( )
      {
         edtAmbienteTecnologico_Codigo_Internalname = sPrefix+"AMBIENTETECNOLOGICO_CODIGO_"+sGXsfl_59_fel_idx;
         edtAmbienteTecnologico_Descricao_Internalname = sPrefix+"AMBIENTETECNOLOGICO_DESCRICAO_"+sGXsfl_59_fel_idx;
         chkAmbienteTecnologicoTecnologias_Ativo_Internalname = sPrefix+"AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_"+sGXsfl_59_fel_idx;
      }

      protected void sendrow_592( )
      {
         SubsflControlProps_592( ) ;
         WBAH0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_59_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_59_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_59_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)59,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_Descricao_Internalname,(String)A352AmbienteTecnologico_Descricao,StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtAmbienteTecnologico_Descricao_Link,(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)59,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAmbienteTecnologicoTecnologias_Ativo_Internalname,StringUtil.BoolToStr( A354AmbienteTecnologicoTecnologias_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AMBIENTETECNOLOGICO_CODIGO"+"_"+sGXsfl_59_idx, GetSecureSignedToken( sPrefix+sGXsfl_59_idx, context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO"+"_"+sGXsfl_59_idx, GetSecureSignedToken( sPrefix+sGXsfl_59_idx, A354AmbienteTecnologicoTecnologias_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_59_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_59_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_59_idx+1));
            sGXsfl_59_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_59_idx), 4, 0)), 4, "0");
            SubsflControlProps_592( ) ;
         }
         /* End function sendrow_592 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblFiltertextareatrabalho_codigo_Internalname = sPrefix+"FILTERTEXTAREATRABALHO_CODIGO";
         edtavAreatrabalho_codigo_Internalname = sPrefix+"vAREATRABALHO_CODIGO";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavAmbientetecnologico_descricao1_Internalname = sPrefix+"vAMBIENTETECNOLOGICO_DESCRICAO1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         edtavAmbientetecnologico_descricao2_Internalname = sPrefix+"vAMBIENTETECNOLOGICO_DESCRICAO2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtAmbienteTecnologico_Codigo_Internalname = sPrefix+"AMBIENTETECNOLOGICO_CODIGO";
         edtAmbienteTecnologico_Descricao_Internalname = sPrefix+"AMBIENTETECNOLOGICO_DESCRICAO";
         chkAmbienteTecnologicoTecnologias_Ativo_Internalname = sPrefix+"AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtTecnologia_Codigo_Internalname = sPrefix+"TECNOLOGIA_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         edtavTfambientetecnologico_descricao_Internalname = sPrefix+"vTFAMBIENTETECNOLOGICO_DESCRICAO";
         edtavTfambientetecnologico_descricao_sel_Internalname = sPrefix+"vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL";
         edtavTfambientetecnologicotecnologias_ativo_sel_Internalname = sPrefix+"vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL";
         Ddo_ambientetecnologico_descricao_Internalname = sPrefix+"DDO_AMBIENTETECNOLOGICO_DESCRICAO";
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_ambientetecnologicotecnologias_ativo_Internalname = sPrefix+"DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO";
         edtavDdo_ambientetecnologicotecnologias_ativotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtAmbienteTecnologico_Descricao_Jsonclick = "";
         edtAmbienteTecnologico_Codigo_Jsonclick = "";
         edtavAmbientetecnologico_descricao2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavAmbientetecnologico_descricao1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavAreatrabalho_codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtAmbienteTecnologico_Descricao_Link = "";
         chkAmbienteTecnologicoTecnologias_Ativo_Titleformat = 0;
         edtAmbienteTecnologico_Descricao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavAmbientetecnologico_descricao2_Visible = 1;
         edtavAmbientetecnologico_descricao1_Visible = 1;
         chkAmbienteTecnologicoTecnologias_Ativo.Title.Text = "Activo";
         edtAmbienteTecnologico_Descricao_Title = "Ambiente Tecnol�gico";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         chkAmbienteTecnologicoTecnologias_Ativo.Caption = "";
         edtavDdo_ambientetecnologicotecnologias_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavTfambientetecnologicotecnologias_ativo_sel_Jsonclick = "";
         edtavTfambientetecnologicotecnologias_ativo_sel_Visible = 1;
         edtavTfambientetecnologico_descricao_sel_Jsonclick = "";
         edtavTfambientetecnologico_descricao_sel_Visible = 1;
         edtavTfambientetecnologico_descricao_Jsonclick = "";
         edtavTfambientetecnologico_descricao_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtTecnologia_Codigo_Jsonclick = "";
         edtTecnologia_Codigo_Visible = 1;
         Ddo_ambientetecnologicotecnologias_ativo_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologicotecnologias_ativo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_ambientetecnologicotecnologias_ativo_Rangefilterto = "At�";
         Ddo_ambientetecnologicotecnologias_ativo_Rangefilterfrom = "Desde";
         Ddo_ambientetecnologicotecnologias_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologicotecnologias_ativo_Loadingdata = "Carregando dados...";
         Ddo_ambientetecnologicotecnologias_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologicotecnologias_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologicotecnologias_ativo_Datalistupdateminimumcharacters = 0;
         Ddo_ambientetecnologicotecnologias_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_ambientetecnologicotecnologias_ativo_Datalisttype = "FixedValues";
         Ddo_ambientetecnologicotecnologias_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_ambientetecnologicotecnologias_ativo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_ambientetecnologicotecnologias_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_ambientetecnologicotecnologias_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologicotecnologias_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologicotecnologias_ativo_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologicotecnologias_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologicotecnologias_ativo_Cls = "ColumnSettings";
         Ddo_ambientetecnologicotecnologias_ativo_Tooltip = "Op��es";
         Ddo_ambientetecnologicotecnologias_ativo_Caption = "";
         Ddo_ambientetecnologico_descricao_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_ambientetecnologico_descricao_Rangefilterto = "At�";
         Ddo_ambientetecnologico_descricao_Rangefilterfrom = "Desde";
         Ddo_ambientetecnologico_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_descricao_Loadingdata = "Carregando dados...";
         Ddo_ambientetecnologico_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_ambientetecnologico_descricao_Datalistproc = "GetTecnologiaAmbienteTecnologicoWCFilterData";
         Ddo_ambientetecnologico_descricao_Datalistfixedvalues = "";
         Ddo_ambientetecnologico_descricao_Datalisttype = "Dynamic";
         Ddo_ambientetecnologico_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_descricao_Filtertype = "Character";
         Ddo_ambientetecnologico_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_descricao_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_descricao_Tooltip = "Op��es";
         Ddo_ambientetecnologico_descricao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tecnologia_Codigo',fld:'vTECNOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''}],oparms:[{av:'AV38AmbienteTecnologico_DescricaoTitleFilterData',fld:'vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV42AmbienteTecnologicoTecnologias_AtivoTitleFilterData',fld:'vAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtAmbienteTecnologico_Descricao_Titleformat',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Titleformat'},{av:'edtAmbienteTecnologico_Descricao_Title',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Title'},{av:'chkAmbienteTecnologicoTecnologias_Ativo_Titleformat',ctrl:'AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO',prop:'Titleformat'},{av:'chkAmbienteTecnologicoTecnologias_Ativo.Title.Text',ctrl:'AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO',prop:'Title'},{av:'AV47GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV48GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV35AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11AH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tecnologia_Codigo',fld:'vTECNOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV35AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_DESCRICAO.ONOPTIONCLICKED","{handler:'E12AH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tecnologia_Codigo',fld:'vTECNOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV35AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_ambientetecnologico_descricao_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_descricao_Filteredtext_get',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_ambientetecnologico_descricao_Selectedvalue_get',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'AV39TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO.ONOPTIONCLICKED","{handler:'E13AH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tecnologia_Codigo',fld:'vTECNOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV35AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_ambientetecnologicotecnologias_ativo_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_get',ctrl:'DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO',prop:'SortedStatus'},{av:'AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E23AH2',iparms:[{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtAmbienteTecnologico_Descricao_Link',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14AH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tecnologia_Codigo',fld:'vTECNOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV35AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E18AH2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15AH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tecnologia_Codigo',fld:'vTECNOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV35AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E19AH2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16AH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tecnologia_Codigo',fld:'vTECNOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV35AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20AH2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E17AH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tecnologia_Codigo',fld:'vTECNOLOGIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV35AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV35AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_descricao_Filteredtext_set',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'FilteredText_set'},{av:'AV40TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_descricao_Selectedvalue_set',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_set',ctrl:'DDO_AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_ambientetecnologico_descricao_Activeeventkey = "";
         Ddo_ambientetecnologico_descricao_Filteredtext_get = "";
         Ddo_ambientetecnologico_descricao_Selectedvalue_get = "";
         Ddo_ambientetecnologicotecnologias_ativo_Activeeventkey = "";
         Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV19AmbienteTecnologico_Descricao1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV24AmbienteTecnologico_Descricao2 = "";
         AV39TFAmbienteTecnologico_Descricao = "";
         AV40TFAmbienteTecnologico_Descricao_Sel = "";
         AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = "";
         AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace = "";
         AV51Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV45DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV38AmbienteTecnologico_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42AmbienteTecnologicoTecnologias_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_ambientetecnologico_descricao_Filteredtext_set = "";
         Ddo_ambientetecnologico_descricao_Selectedvalue_set = "";
         Ddo_ambientetecnologico_descricao_Sortedstatus = "";
         Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_set = "";
         Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A352AmbienteTecnologico_Descricao = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV19AmbienteTecnologico_Descricao1 = "";
         lV24AmbienteTecnologico_Descricao2 = "";
         lV39TFAmbienteTecnologico_Descricao = "";
         H00AH2_A131Tecnologia_Codigo = new int[1] ;
         H00AH2_A354AmbienteTecnologicoTecnologias_Ativo = new bool[] {false} ;
         H00AH2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         H00AH2_A351AmbienteTecnologico_Codigo = new int[1] ;
         H00AH3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV34Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextareatrabalho_codigo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Tecnologia_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tecnologiaambientetecnologicowc__default(),
            new Object[][] {
                new Object[] {
               H00AH2_A131Tecnologia_Codigo, H00AH2_A354AmbienteTecnologicoTecnologias_Ativo, H00AH2_A352AmbienteTecnologico_Descricao, H00AH2_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               H00AH3_AGRID_nRecordCount
               }
            }
         );
         AV51Pgmname = "TecnologiaAmbienteTecnologicoWC";
         /* GeneXus formulas. */
         AV51Pgmname = "TecnologiaAmbienteTecnologicoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_59 ;
      private short nGXsfl_59_idx=1 ;
      private short AV14OrderedBy ;
      private short AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_59_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtAmbienteTecnologico_Descricao_Titleformat ;
      private short chkAmbienteTecnologicoTecnologias_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Tecnologia_Codigo ;
      private int wcpOAV7Tecnologia_Codigo ;
      private int subGrid_Rows ;
      private int AV35AreaTrabalho_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_ambientetecnologicotecnologias_ativo_Datalistupdateminimumcharacters ;
      private int A131Tecnologia_Codigo ;
      private int edtTecnologia_Codigo_Visible ;
      private int edtavTfambientetecnologico_descricao_Visible ;
      private int edtavTfambientetecnologico_descricao_sel_Visible ;
      private int edtavTfambientetecnologicotecnologias_ativo_sel_Visible ;
      private int edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_ambientetecnologicotecnologias_ativotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV46PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavAmbientetecnologico_descricao1_Visible ;
      private int edtavAmbientetecnologico_descricao2_Visible ;
      private int AV52GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV47GridCurrentPage ;
      private long AV48GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_ambientetecnologico_descricao_Activeeventkey ;
      private String Ddo_ambientetecnologico_descricao_Filteredtext_get ;
      private String Ddo_ambientetecnologico_descricao_Selectedvalue_get ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Activeeventkey ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_59_idx="0001" ;
      private String AV51Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_ambientetecnologico_descricao_Caption ;
      private String Ddo_ambientetecnologico_descricao_Tooltip ;
      private String Ddo_ambientetecnologico_descricao_Cls ;
      private String Ddo_ambientetecnologico_descricao_Filteredtext_set ;
      private String Ddo_ambientetecnologico_descricao_Selectedvalue_set ;
      private String Ddo_ambientetecnologico_descricao_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_descricao_Sortedstatus ;
      private String Ddo_ambientetecnologico_descricao_Filtertype ;
      private String Ddo_ambientetecnologico_descricao_Datalisttype ;
      private String Ddo_ambientetecnologico_descricao_Datalistfixedvalues ;
      private String Ddo_ambientetecnologico_descricao_Datalistproc ;
      private String Ddo_ambientetecnologico_descricao_Sortasc ;
      private String Ddo_ambientetecnologico_descricao_Sortdsc ;
      private String Ddo_ambientetecnologico_descricao_Loadingdata ;
      private String Ddo_ambientetecnologico_descricao_Cleanfilter ;
      private String Ddo_ambientetecnologico_descricao_Rangefilterfrom ;
      private String Ddo_ambientetecnologico_descricao_Rangefilterto ;
      private String Ddo_ambientetecnologico_descricao_Noresultsfound ;
      private String Ddo_ambientetecnologico_descricao_Searchbuttontext ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Caption ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Tooltip ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Cls ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Selectedvalue_set ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Dropdownoptionstype ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Sortedstatus ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Datalisttype ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Datalistfixedvalues ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Sortasc ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Sortdsc ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Loadingdata ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Cleanfilter ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Rangefilterfrom ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Rangefilterto ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Noresultsfound ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtTecnologia_Codigo_Internalname ;
      private String edtTecnologia_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfambientetecnologico_descricao_Internalname ;
      private String edtavTfambientetecnologico_descricao_Jsonclick ;
      private String edtavTfambientetecnologico_descricao_sel_Internalname ;
      private String edtavTfambientetecnologico_descricao_sel_Jsonclick ;
      private String edtavTfambientetecnologicotecnologias_ativo_sel_Internalname ;
      private String edtavTfambientetecnologicotecnologias_ativo_sel_Jsonclick ;
      private String edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_ambientetecnologicotecnologias_ativotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtAmbienteTecnologico_Codigo_Internalname ;
      private String edtAmbienteTecnologico_Descricao_Internalname ;
      private String chkAmbienteTecnologicoTecnologias_Ativo_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavAreatrabalho_codigo_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavAmbientetecnologico_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavAmbientetecnologico_descricao2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_ambientetecnologico_descricao_Internalname ;
      private String Ddo_ambientetecnologicotecnologias_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtAmbienteTecnologico_Descricao_Title ;
      private String edtAmbienteTecnologico_Descricao_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextareatrabalho_codigo_Internalname ;
      private String lblFiltertextareatrabalho_codigo_Jsonclick ;
      private String edtavAreatrabalho_codigo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavAmbientetecnologico_descricao1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavAmbientetecnologico_descricao2_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sCtrlAV7Tecnologia_Codigo ;
      private String sGXsfl_59_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAmbienteTecnologico_Codigo_Jsonclick ;
      private String edtAmbienteTecnologico_Descricao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_ambientetecnologico_descricao_Includesortasc ;
      private bool Ddo_ambientetecnologico_descricao_Includesortdsc ;
      private bool Ddo_ambientetecnologico_descricao_Includefilter ;
      private bool Ddo_ambientetecnologico_descricao_Filterisrange ;
      private bool Ddo_ambientetecnologico_descricao_Includedatalist ;
      private bool Ddo_ambientetecnologicotecnologias_ativo_Includesortasc ;
      private bool Ddo_ambientetecnologicotecnologias_ativo_Includesortdsc ;
      private bool Ddo_ambientetecnologicotecnologias_ativo_Includefilter ;
      private bool Ddo_ambientetecnologicotecnologias_ativo_Filterisrange ;
      private bool Ddo_ambientetecnologicotecnologias_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A354AmbienteTecnologicoTecnologias_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV19AmbienteTecnologico_Descricao1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV24AmbienteTecnologico_Descricao2 ;
      private String AV39TFAmbienteTecnologico_Descricao ;
      private String AV40TFAmbienteTecnologico_Descricao_Sel ;
      private String AV41ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace ;
      private String AV44ddo_AmbienteTecnologicoTecnologias_AtivoTitleControlIdToReplace ;
      private String A352AmbienteTecnologico_Descricao ;
      private String lV19AmbienteTecnologico_Descricao1 ;
      private String lV24AmbienteTecnologico_Descricao2 ;
      private String lV39TFAmbienteTecnologico_Descricao ;
      private IGxSession AV34Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCheckbox chkAmbienteTecnologicoTecnologias_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H00AH2_A131Tecnologia_Codigo ;
      private bool[] H00AH2_A354AmbienteTecnologicoTecnologias_Ativo ;
      private String[] H00AH2_A352AmbienteTecnologico_Descricao ;
      private int[] H00AH2_A351AmbienteTecnologico_Codigo ;
      private long[] H00AH3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38AmbienteTecnologico_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42AmbienteTecnologicoTecnologias_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV45DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class tecnologiaambientetecnologicowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00AH2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV19AmbienteTecnologico_Descricao1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             String AV24AmbienteTecnologico_Descricao2 ,
                                             String AV40TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV39TFAmbienteTecnologico_Descricao ,
                                             short AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             bool A354AmbienteTecnologicoTecnologias_Ativo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A131Tecnologia_Codigo ,
                                             int AV7Tecnologia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [10] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Tecnologia_Codigo], T1.[AmbienteTecnologicoTecnologias_Ativo], T2.[AmbienteTecnologico_Descricao], T1.[AmbienteTecnologico_Codigo]";
         sFromString = " FROM ([AmbienteTecnologicoTecnologias] T1 WITH (NOLOCK) INNER JOIN [AmbienteTecnologico] T2 WITH (NOLOCK) ON T2.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Tecnologia_Codigo] = @AV7Tecnologia_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19AmbienteTecnologico_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV19AmbienteTecnologico_Descricao1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24AmbienteTecnologico_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV24AmbienteTecnologico_Descricao2 + '%')";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFAmbienteTecnologico_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV39TFAmbienteTecnologico_Descricao)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAmbienteTecnologico_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] = @AV40TFAmbienteTecnologico_Descricao_Sel)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[AmbienteTecnologicoTecnologias_Ativo] = 1)";
         }
         if ( AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[AmbienteTecnologicoTecnologias_Ativo] = 0)";
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tecnologia_Codigo], T1.[AmbienteTecnologicoTecnologias_Ativo]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tecnologia_Codigo] DESC, T1.[AmbienteTecnologicoTecnologias_Ativo] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tecnologia_Codigo], T2.[AmbienteTecnologico_Descricao]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tecnologia_Codigo] DESC, T2.[AmbienteTecnologico_Descricao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AmbienteTecnologico_Codigo], T1.[Tecnologia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00AH3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV19AmbienteTecnologico_Descricao1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             String AV24AmbienteTecnologico_Descricao2 ,
                                             String AV40TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV39TFAmbienteTecnologico_Descricao ,
                                             short AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             bool A354AmbienteTecnologicoTecnologias_Ativo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A131Tecnologia_Codigo ,
                                             int AV7Tecnologia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [5] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([AmbienteTecnologicoTecnologias] T1 WITH (NOLOCK) INNER JOIN [AmbienteTecnologico] T2 WITH (NOLOCK) ON T2.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Tecnologia_Codigo] = @AV7Tecnologia_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19AmbienteTecnologico_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV19AmbienteTecnologico_Descricao1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24AmbienteTecnologico_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV24AmbienteTecnologico_Descricao2 + '%')";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFAmbienteTecnologico_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV39TFAmbienteTecnologico_Descricao)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAmbienteTecnologico_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] = @AV40TFAmbienteTecnologico_Descricao_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[AmbienteTecnologicoTecnologias_Ativo] = 1)";
         }
         if ( AV43TFAmbienteTecnologicoTecnologias_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[AmbienteTecnologicoTecnologias_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00AH2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (short)dynConstraints[10] , (bool)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] );
               case 1 :
                     return conditional_H00AH3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (short)dynConstraints[10] , (bool)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AH2 ;
          prmH00AH2 = new Object[] {
          new Object[] {"@AV7Tecnologia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19AmbienteTecnologico_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV24AmbienteTecnologico_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV39TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV40TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00AH3 ;
          prmH00AH3 = new Object[] {
          new Object[] {"@AV7Tecnologia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19AmbienteTecnologico_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV24AmbienteTecnologico_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV39TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV40TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AH2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AH2,11,0,true,false )
             ,new CursorDef("H00AH3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AH3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

}
