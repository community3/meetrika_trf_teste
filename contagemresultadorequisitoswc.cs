/*
               File: ContagemResultadoRequisitosWC
        Description: Contagem Resultado Requisitos WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 5:19:11.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadorequisitoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultadorequisitoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultadorequisitoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultadoRequisito_OSCod )
      {
         this.AV67ContagemResultadoRequisito_OSCod = aP0_ContagemResultadoRequisito_OSCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV67ContagemResultadoRequisito_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultadoRequisito_OSCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV67ContagemResultadoRequisito_OSCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_10 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_10_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_10_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  edtContagemResultadoRequisito_ReqCod_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoRequisito_ReqCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoRequisito_ReqCod_Visible), 5, 0)));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV67ContagemResultadoRequisito_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultadoRequisito_OSCod), 6, 0)));
                  edtContagemResultadoRequisito_ReqCod_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoRequisito_ReqCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoRequisito_ReqCod_Visible), 5, 0)));
                  AV70Pgmname = GetNextPar( );
                  A1636ContagemResultado_ServicoSS = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1636ContagemResultado_ServicoSS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1636ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0)));
                  A2004ContagemResultadoRequisito_ReqCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A1932Requisito_Pontuacao = NumberUtil.Val( GetNextPar( ), ".");
                  n1932Requisito_Pontuacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1932Requisito_Pontuacao", StringUtil.LTrim( StringUtil.Str( A1932Requisito_Pontuacao, 14, 5)));
                  AV65PontosFilhos = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65PontosFilhos", StringUtil.LTrim( StringUtil.Str( AV65PontosFilhos, 14, 5)));
                  AV60Total = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60Total", StringUtil.LTrim( StringUtil.Str( AV60Total, 14, 5)));
                  A1926Requisito_Agrupador = GetNextPar( );
                  n1926Requisito_Agrupador = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1926Requisito_Agrupador", A1926Requisito_Agrupador);
                  A2001Requisito_Identificador = GetNextPar( );
                  n2001Requisito_Identificador = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2001Requisito_Identificador", A2001Requisito_Identificador);
                  A1927Requisito_Titulo = GetNextPar( );
                  n1927Requisito_Titulo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1927Requisito_Titulo", A1927Requisito_Titulo);
                  A1934Requisito_Status = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  n1934Requisito_Status = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
                  A1923Requisito_Descricao = GetNextPar( );
                  n1923Requisito_Descricao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1923Requisito_Descricao", A1923Requisito_Descricao);
                  A508ContagemResultado_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n508ContagemResultado_Owner = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A2006ContagemResultadoRequisito_Owner = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2006ContagemResultadoRequisito_Owner", A2006ContagemResultadoRequisito_Owner);
                  A1999Requisito_ReqCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1999Requisito_ReqCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1999Requisito_ReqCod), 6, 0)));
                  AV63Requisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63Requisito_Codigo), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultadoRequisito_OSCod, AV70Pgmname, A1636ContagemResultado_ServicoSS, A2004ContagemResultadoRequisito_ReqCod, A1932Requisito_Pontuacao, AV65PontosFilhos, AV60Total, A1926Requisito_Agrupador, A2001Requisito_Identificador, A1927Requisito_Titulo, A1934Requisito_Status, A1923Requisito_Descricao, A508ContagemResultado_Owner, AV6WWPContext, A2006ContagemResultadoRequisito_Owner, A1999Requisito_ReqCod, AV63Requisito_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAPG2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV70Pgmname = "ContagemResultadoRequisitosWC";
               context.Gx_err = 0;
               edtavDescricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
               WSPG2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado Requisitos WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020625191152");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadorequisitoswc.aspx") + "?" + UrlEncode("" +AV67ContagemResultadoRequisito_OSCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_10", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_10), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV67ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV67ContagemResultadoRequisito_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADOREQUISITO_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67ContagemResultadoRequisito_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV70Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_SERVICOSS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_PONTUACAO", StringUtil.LTrim( StringUtil.NToC( A1932Requisito_Pontuacao, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPONTOSFILHOS", StringUtil.LTrim( StringUtil.NToC( AV65PontosFilhos, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTOTAL", StringUtil.LTrim( StringUtil.NToC( AV60Total, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_AGRUPADOR", A1926Requisito_Agrupador);
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_IDENTIFICADOR", A2001Requisito_Identificador);
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_TITULO", A1927Requisito_Titulo);
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_STATUS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1934Requisito_Status), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_DESCRICAO", A1923Requisito_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A508ContagemResultado_Owner), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"CONTAGEMRESULTADOREQUISITO_OWNER", A2006ContagemResultadoRequisito_Owner);
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_REQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1999Requisito_ReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vREQUISITO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63Requisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Width", StringUtil.RTrim( Dvpanel_unnamedtable2_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Cls", StringUtil.RTrim( Dvpanel_unnamedtable2_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Title", StringUtil.RTrim( Dvpanel_unnamedtable2_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Collapsible", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Collapsible));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Collapsed", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Collapsed));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Autowidth", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Autowidth));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Autoheight", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Autoheight));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Iconposition", StringUtil.RTrim( Dvpanel_unnamedtable2_Iconposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Autoscroll", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Autoscroll));
         GxWebStd.gx_hidden_field( context, sPrefix+"INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Confirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Nobuttoncaption", StringUtil.RTrim( Confirmpanel_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Cancelbuttoncaption", StringUtil.RTrim( Confirmpanel_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Yesbuttonposition", StringUtil.RTrim( Confirmpanel_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOREQUISITO_REQCOD_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoRequisito_ReqCod_Visible), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Result", StringUtil.RTrim( Confirmpanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormPG2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultadorequisitoswc.js", "?2020625191175");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            if ( ! ( WebComp_Wcwc_requisitostecnicos == null ) )
            {
               WebComp_Wcwc_requisitostecnicos.componentjscripts();
            }
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoRequisitosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Requisitos WC" ;
      }

      protected void WBPG0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultadorequisitoswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
               context.AddJavascriptSource("Window/InNewWindowRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_PG2( true) ;
         }
         else
         {
            wb_table1_2_PG2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_PG2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoRequisito_OSCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A2003ContagemResultadoRequisito_OSCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoRequisito_OSCod_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultadoRequisito_OSCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoRequisitosWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTPG2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado Requisitos WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPPG0( ) ;
            }
         }
      }

      protected void WSPG2( )
      {
         STARTPG2( ) ;
         EVTPG2( ) ;
      }

      protected void EVTPG2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPG0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPG0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11PG2 */
                                    E11PG2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DONEWRN'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPG0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12PG2 */
                                    E12PG2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPG0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavDescricao_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPG0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DOUPDATE'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "'DONEWREQTEC'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DOUPDATE'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "'DONEWREQTEC'") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPPG0( ) ;
                              }
                              nGXsfl_10_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_10_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_10_idx), 4, 0)), 4, "0");
                              SubsflControlProps_102( ) ;
                              GXCCtl = "DVPANEL_TBLREQNEG_Width_" + sGXsfl_10_idx;
                              Dvpanel_tblreqneg_Width = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "Width", Dvpanel_tblreqneg_Width);
                              GXCCtl = "DVPANEL_TBLREQNEG_Cls_" + sGXsfl_10_idx;
                              Dvpanel_tblreqneg_Cls = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "Cls", Dvpanel_tblreqneg_Cls);
                              GXCCtl = "DVPANEL_TBLREQNEG_Title_" + sGXsfl_10_idx;
                              Dvpanel_tblreqneg_Title = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "Title", Dvpanel_tblreqneg_Title);
                              GXCCtl = "DVPANEL_TBLREQNEG_Collapsible_" + sGXsfl_10_idx;
                              Dvpanel_tblreqneg_Collapsible = StringUtil.StrToBool( cgiGet( sPrefix+GXCCtl));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "Collapsible", StringUtil.BoolToStr( Dvpanel_tblreqneg_Collapsible));
                              GXCCtl = "DVPANEL_TBLREQNEG_Collapsed_" + sGXsfl_10_idx;
                              Dvpanel_tblreqneg_Collapsed = StringUtil.StrToBool( cgiGet( sPrefix+GXCCtl));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "Collapsed", StringUtil.BoolToStr( Dvpanel_tblreqneg_Collapsed));
                              GXCCtl = "DVPANEL_TBLREQNEG_Autowidth_" + sGXsfl_10_idx;
                              Dvpanel_tblreqneg_Autowidth = StringUtil.StrToBool( cgiGet( sPrefix+GXCCtl));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "AutoWidth", StringUtil.BoolToStr( Dvpanel_tblreqneg_Autowidth));
                              GXCCtl = "DVPANEL_TBLREQNEG_Autoheight_" + sGXsfl_10_idx;
                              Dvpanel_tblreqneg_Autoheight = StringUtil.StrToBool( cgiGet( sPrefix+GXCCtl));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "AutoHeight", StringUtil.BoolToStr( Dvpanel_tblreqneg_Autoheight));
                              GXCCtl = "DVPANEL_TBLREQNEG_Showcollapseicon_" + sGXsfl_10_idx;
                              Dvpanel_tblreqneg_Showcollapseicon = StringUtil.StrToBool( cgiGet( sPrefix+GXCCtl));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "ShowCollapseIcon", StringUtil.BoolToStr( Dvpanel_tblreqneg_Showcollapseicon));
                              GXCCtl = "DVPANEL_TBLREQNEG_Iconposition_" + sGXsfl_10_idx;
                              Dvpanel_tblreqneg_Iconposition = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "IconPosition", Dvpanel_tblreqneg_Iconposition);
                              GXCCtl = "DVPANEL_TBLREQNEG_Autoscroll_" + sGXsfl_10_idx;
                              Dvpanel_tblreqneg_Autoscroll = StringUtil.StrToBool( cgiGet( sPrefix+GXCCtl));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "AutoScroll", StringUtil.BoolToStr( Dvpanel_tblreqneg_Autoscroll));
                              AV54Descricao = cgiGet( edtavDescricao_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDescricao_Internalname, AV54Descricao);
                              A2004ContagemResultadoRequisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoRequisito_ReqCod_Internalname), ",", "."));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13PG2 */
                                          E13PG2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14PG2 */
                                          E14PG2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15PG2 */
                                          E15PG2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16PG2 */
                                          E16PG2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DONEWREQTEC'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17PG2 */
                                          E17PG2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPPG0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavDescricao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 24 )
                        {
                           sEvtType = StringUtil.Left( sEvt, 4);
                           sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           sCmpCtrl = "W0024" + sEvtType;
                           OldWcwc_requisitostecnicos = cgiGet( sPrefix+sCmpCtrl);
                           if ( ( StringUtil.Len( OldWcwc_requisitostecnicos) == 0 ) || ( StringUtil.StrCmp(OldWcwc_requisitostecnicos, WebComp_GX_Process_Component) != 0 ) )
                           {
                              WebComp_GX_Process = getWebComponent(GetType(), "GeneXus.Programs", OldWcwc_requisitostecnicos, new Object[] {context} );
                              WebComp_GX_Process.ComponentInit();
                              WebComp_GX_Process.Name = "OldWcwc_requisitostecnicos";
                              WebComp_GX_Process_Component = OldWcwc_requisitostecnicos;
                           }
                           if ( StringUtil.Len( WebComp_GX_Process_Component) != 0 )
                           {
                              WebComp_GX_Process.componentprocess(sPrefix+"W0024", sEvtType, sEvt);
                           }
                           nGXsfl_10_webc_idx = (int)(NumberUtil.Val( sEvtType, "."));
                           WebCompHandler = "Wcwc_requisitostecnicos";
                           WebComp_GX_Process_Component = OldWcwc_requisitostecnicos;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEPG2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormPG2( ) ;
            }
         }
      }

      protected void PAPG2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_102( ) ;
         while ( nGXsfl_10_idx <= nRC_GXsfl_10 )
         {
            sendrow_102( ) ;
            nGXsfl_10_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_10_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_10_idx+1));
            sGXsfl_10_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_10_idx), 4, 0)), 4, "0");
            SubsflControlProps_102( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int AV67ContagemResultadoRequisito_OSCod ,
                                       String AV70Pgmname ,
                                       int A1636ContagemResultado_ServicoSS ,
                                       int A2004ContagemResultadoRequisito_ReqCod ,
                                       decimal A1932Requisito_Pontuacao ,
                                       decimal AV65PontosFilhos ,
                                       decimal AV60Total ,
                                       String A1926Requisito_Agrupador ,
                                       String A2001Requisito_Identificador ,
                                       String A1927Requisito_Titulo ,
                                       short A1934Requisito_Status ,
                                       String A1923Requisito_Descricao ,
                                       int A508ContagemResultado_Owner ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       bool A2006ContagemResultadoRequisito_Owner ,
                                       int A1999Requisito_ReqCod ,
                                       int AV63Requisito_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFPG2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFPG2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV70Pgmname = "ContagemResultadoRequisitosWC";
         context.Gx_err = 0;
         edtavDescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
      }

      protected void RFPG2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 10;
         /* Execute user event: E14PG2 */
         E14PG2 ();
         nGXsfl_10_idx = 1;
         sGXsfl_10_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_10_idx), 4, 0)), 4, "0");
         SubsflControlProps_102( ) ;
         nGXsfl_10_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", StringUtil.RTrim( "FreeStyleGrid"));
         GridContainer.AddObjectProperty("Class", "FreeStyleGrid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_GX_Process_Component) != 0 )
               {
                  WebComp_GX_Process.componentstart();
               }
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( WebComp_Wcwc_requisitostecnicos_Visible != 0 )
            {
               if ( StringUtil.Len( WebComp_Wcwc_requisitostecnicos_Component) != 0 )
               {
                  WebComp_Wcwc_requisitostecnicos.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_102( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            /* Using cursor H00PG2 */
            pr_default.execute(0, new Object[] {AV67ContagemResultadoRequisito_OSCod, GXPagingFrom2, GXPagingTo2});
            nGXsfl_10_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1999Requisito_ReqCod = H00PG2_A1999Requisito_ReqCod[0];
               n1999Requisito_ReqCod = H00PG2_n1999Requisito_ReqCod[0];
               A1636ContagemResultado_ServicoSS = H00PG2_A1636ContagemResultado_ServicoSS[0];
               n1636ContagemResultado_ServicoSS = H00PG2_n1636ContagemResultado_ServicoSS[0];
               A1932Requisito_Pontuacao = H00PG2_A1932Requisito_Pontuacao[0];
               n1932Requisito_Pontuacao = H00PG2_n1932Requisito_Pontuacao[0];
               A1934Requisito_Status = H00PG2_A1934Requisito_Status[0];
               n1934Requisito_Status = H00PG2_n1934Requisito_Status[0];
               A1927Requisito_Titulo = H00PG2_A1927Requisito_Titulo[0];
               n1927Requisito_Titulo = H00PG2_n1927Requisito_Titulo[0];
               A2001Requisito_Identificador = H00PG2_A2001Requisito_Identificador[0];
               n2001Requisito_Identificador = H00PG2_n2001Requisito_Identificador[0];
               A1926Requisito_Agrupador = H00PG2_A1926Requisito_Agrupador[0];
               n1926Requisito_Agrupador = H00PG2_n1926Requisito_Agrupador[0];
               A1923Requisito_Descricao = H00PG2_A1923Requisito_Descricao[0];
               n1923Requisito_Descricao = H00PG2_n1923Requisito_Descricao[0];
               A508ContagemResultado_Owner = H00PG2_A508ContagemResultado_Owner[0];
               n508ContagemResultado_Owner = H00PG2_n508ContagemResultado_Owner[0];
               A2006ContagemResultadoRequisito_Owner = H00PG2_A2006ContagemResultadoRequisito_Owner[0];
               A2003ContagemResultadoRequisito_OSCod = H00PG2_A2003ContagemResultadoRequisito_OSCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2003ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0)));
               A2004ContagemResultadoRequisito_ReqCod = H00PG2_A2004ContagemResultadoRequisito_ReqCod[0];
               A1636ContagemResultado_ServicoSS = H00PG2_A1636ContagemResultado_ServicoSS[0];
               n1636ContagemResultado_ServicoSS = H00PG2_n1636ContagemResultado_ServicoSS[0];
               A508ContagemResultado_Owner = H00PG2_A508ContagemResultado_Owner[0];
               n508ContagemResultado_Owner = H00PG2_n508ContagemResultado_Owner[0];
               A1999Requisito_ReqCod = H00PG2_A1999Requisito_ReqCod[0];
               n1999Requisito_ReqCod = H00PG2_n1999Requisito_ReqCod[0];
               A1932Requisito_Pontuacao = H00PG2_A1932Requisito_Pontuacao[0];
               n1932Requisito_Pontuacao = H00PG2_n1932Requisito_Pontuacao[0];
               A1934Requisito_Status = H00PG2_A1934Requisito_Status[0];
               n1934Requisito_Status = H00PG2_n1934Requisito_Status[0];
               A1927Requisito_Titulo = H00PG2_A1927Requisito_Titulo[0];
               n1927Requisito_Titulo = H00PG2_n1927Requisito_Titulo[0];
               A2001Requisito_Identificador = H00PG2_A2001Requisito_Identificador[0];
               n2001Requisito_Identificador = H00PG2_n2001Requisito_Identificador[0];
               A1926Requisito_Agrupador = H00PG2_A1926Requisito_Agrupador[0];
               n1926Requisito_Agrupador = H00PG2_n1926Requisito_Agrupador[0];
               A1923Requisito_Descricao = H00PG2_A1923Requisito_Descricao[0];
               n1923Requisito_Descricao = H00PG2_n1923Requisito_Descricao[0];
               /* Execute user event: E15PG2 */
               E15PG2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 10;
            WBPG0( ) ;
         }
         nGXsfl_10_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         /* Using cursor H00PG3 */
         pr_default.execute(1, new Object[] {AV67ContagemResultadoRequisito_OSCod});
         GRID_nRecordCount = H00PG3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultadoRequisito_OSCod, AV70Pgmname, A1636ContagemResultado_ServicoSS, A2004ContagemResultadoRequisito_ReqCod, A1932Requisito_Pontuacao, AV65PontosFilhos, AV60Total, A1926Requisito_Agrupador, A2001Requisito_Identificador, A1927Requisito_Titulo, A1934Requisito_Status, A1923Requisito_Descricao, A508ContagemResultado_Owner, AV6WWPContext, A2006ContagemResultadoRequisito_Owner, A1999Requisito_ReqCod, AV63Requisito_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultadoRequisito_OSCod, AV70Pgmname, A1636ContagemResultado_ServicoSS, A2004ContagemResultadoRequisito_ReqCod, A1932Requisito_Pontuacao, AV65PontosFilhos, AV60Total, A1926Requisito_Agrupador, A2001Requisito_Identificador, A1927Requisito_Titulo, A1934Requisito_Status, A1923Requisito_Descricao, A508ContagemResultado_Owner, AV6WWPContext, A2006ContagemResultadoRequisito_Owner, A1999Requisito_ReqCod, AV63Requisito_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultadoRequisito_OSCod, AV70Pgmname, A1636ContagemResultado_ServicoSS, A2004ContagemResultadoRequisito_ReqCod, A1932Requisito_Pontuacao, AV65PontosFilhos, AV60Total, A1926Requisito_Agrupador, A2001Requisito_Identificador, A1927Requisito_Titulo, A1934Requisito_Status, A1923Requisito_Descricao, A508ContagemResultado_Owner, AV6WWPContext, A2006ContagemResultadoRequisito_Owner, A1999Requisito_ReqCod, AV63Requisito_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultadoRequisito_OSCod, AV70Pgmname, A1636ContagemResultado_ServicoSS, A2004ContagemResultadoRequisito_ReqCod, A1932Requisito_Pontuacao, AV65PontosFilhos, AV60Total, A1926Requisito_Agrupador, A2001Requisito_Identificador, A1927Requisito_Titulo, A1934Requisito_Status, A1923Requisito_Descricao, A508ContagemResultado_Owner, AV6WWPContext, A2006ContagemResultadoRequisito_Owner, A1999Requisito_ReqCod, AV63Requisito_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultadoRequisito_OSCod, AV70Pgmname, A1636ContagemResultado_ServicoSS, A2004ContagemResultadoRequisito_ReqCod, A1932Requisito_Pontuacao, AV65PontosFilhos, AV60Total, A1926Requisito_Agrupador, A2001Requisito_Identificador, A1927Requisito_Titulo, A1934Requisito_Status, A1923Requisito_Descricao, A508ContagemResultado_Owner, AV6WWPContext, A2006ContagemResultadoRequisito_Owner, A1999Requisito_ReqCod, AV63Requisito_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPPG0( )
      {
         /* Before Start, stand alone formulas. */
         AV70Pgmname = "ContagemResultadoRequisitosWC";
         context.Gx_err = 0;
         edtavDescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13PG2 */
         E13PG2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A2003ContagemResultadoRequisito_OSCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoRequisito_OSCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2003ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0)));
            /* Read saved values. */
            nRC_GXsfl_10 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_10"), ",", "."));
            wcpOAV67ContagemResultadoRequisito_OSCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV67ContagemResultadoRequisito_OSCod"), ",", "."));
            AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"vCONTAGEMRESULTADO_CODIGO"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Dvpanel_unnamedtable2_Width = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Width");
            Dvpanel_unnamedtable2_Cls = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Cls");
            Dvpanel_unnamedtable2_Title = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Title");
            Dvpanel_unnamedtable2_Collapsible = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Collapsible"));
            Dvpanel_unnamedtable2_Collapsed = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Collapsed"));
            Dvpanel_unnamedtable2_Autowidth = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Autowidth"));
            Dvpanel_unnamedtable2_Autoheight = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Autoheight"));
            Dvpanel_unnamedtable2_Showcollapseicon = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Showcollapseicon"));
            Dvpanel_unnamedtable2_Iconposition = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Iconposition");
            Dvpanel_unnamedtable2_Autoscroll = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Autoscroll"));
            Innewwindow_Target = cgiGet( sPrefix+"INNEWWINDOW_Target");
            Confirmpanel_Title = cgiGet( sPrefix+"CONFIRMPANEL_Title");
            Confirmpanel_Confirmationtext = cgiGet( sPrefix+"CONFIRMPANEL_Confirmationtext");
            Confirmpanel_Yesbuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL_Yesbuttoncaption");
            Confirmpanel_Nobuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL_Nobuttoncaption");
            Confirmpanel_Cancelbuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL_Cancelbuttoncaption");
            Confirmpanel_Yesbuttonposition = cgiGet( sPrefix+"CONFIRMPANEL_Yesbuttonposition");
            Confirmpanel_Result = cgiGet( sPrefix+"CONFIRMPANEL_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13PG2 */
         E13PG2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13PG2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         edtContagemResultadoRequisito_ReqCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoRequisito_ReqCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoRequisito_ReqCod_Visible), 5, 0)));
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtContagemResultadoRequisito_OSCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoRequisito_OSCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoRequisito_OSCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         Dvpanel_tblreqneg_Collapsible = false;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "Collapsible", StringUtil.BoolToStr( Dvpanel_tblreqneg_Collapsible));
         Dvpanel_unnamedtable2_Collapsible = false;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_unnamedtable2_Internalname, "Collapsible", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Collapsible));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
      }

      protected void E14PG2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV60Total = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60Total", StringUtil.LTrim( StringUtil.Str( AV60Total, 14, 5)));
         AV45Agrupador = "";
         imgNewreqtec_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgNewreqtec_Internalname, "Linktarget", imgNewreqtec_Linktarget);
         imgPdf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgPdf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgPdf_Visible), 5, 0)));
         imgPdf_Tooltiptext = "Gerar documento para imprimir";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgPdf_Internalname, "Tooltiptext", imgPdf_Tooltiptext);
         imgNewrn_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgNewrn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgNewrn_Visible), 5, 0)));
         imgNewreqtec_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgNewreqtec_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgNewreqtec_Visible), 5, 0)));
         imgUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgUpdate_Visible), 5, 0)));
         imgActiondelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgActiondelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgActiondelete_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
      }

      private void E15PG2( )
      {
         /* Grid_Load Routine */
         if ( (0==A1636ContagemResultado_ServicoSS) )
         {
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wcwc_requisitostecnicos_Component), StringUtil.Lower( "WC_RequisitosTecnicos")) != 0 )
            {
               WebComp_Wcwc_requisitostecnicos = getWebComponent(GetType(), "GeneXus.Programs", "wc_requisitostecnicos", new Object[] {context} );
               WebComp_Wcwc_requisitostecnicos.ComponentInit();
               WebComp_Wcwc_requisitostecnicos.Name = "WC_RequisitosTecnicos";
               WebComp_Wcwc_requisitostecnicos_Component = "WC_RequisitosTecnicos";
            }
            if ( StringUtil.Len( WebComp_Wcwc_requisitostecnicos_Component) != 0 )
            {
               WebComp_Wcwc_requisitostecnicos.setjustcreated();
               WebComp_Wcwc_requisitostecnicos.componentprepare(new Object[] {(String)sPrefix+"W0024",(String)sGXsfl_10_idx,(int)A2004ContagemResultadoRequisito_ReqCod});
               WebComp_Wcwc_requisitostecnicos.componentbind(new Object[] {(String)""});
            }
            imgNewreqtec_Visible = 1;
         }
         else
         {
            imgNewreqtec_Visible = 0;
            WebComp_Wcwc_requisitostecnicos_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, sPrefix+"gxHTMLWrpW0024"+sGXsfl_10_idx, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(WebComp_Wcwc_requisitostecnicos_Visible), 5, 0)));
         }
         AV59Pontos = A1932Requisito_Pontuacao;
         AV63Requisito_Codigo = A2004ContagemResultadoRequisito_ReqCod;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63Requisito_Codigo), 6, 0)));
         /* Execute user subroutine: 'PONTOSFILHOS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ( AV65PontosFilhos > Convert.ToDecimal( 0 )) )
         {
            AV60Total = (decimal)(AV60Total+AV65PontosFilhos);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60Total", StringUtil.LTrim( StringUtil.Str( AV60Total, 14, 5)));
            AV59Pontos = AV65PontosFilhos;
         }
         else
         {
            AV60Total = (decimal)(AV60Total+AV59Pontos);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60Total", StringUtil.LTrim( StringUtil.Str( AV60Total, 14, 5)));
         }
         Dvpanel_tblreqneg_Title = A1926Requisito_Agrupador+": "+A2001Requisito_Identificador+", "+A1927Requisito_Titulo+" - "+gxdomainstatusrequisito.getDescription(context,A1934Requisito_Status)+" - ["+StringUtil.Str( AV59Pontos, 14, 5)+"]";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_tblreqneg_Internalname, "Title", Dvpanel_tblreqneg_Title);
         AV54Descricao = A1923Requisito_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDescricao_Internalname, AV54Descricao);
         AV63Requisito_Codigo = A2004ContagemResultadoRequisito_ReqCod;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63Requisito_Codigo), 6, 0)));
         imgNewrn_Visible = (((A508ContagemResultado_Owner==AV6WWPContext.gxTpr_Userid)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgNewrn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgNewrn_Visible), 5, 0)));
         imgActiondelete_Visible = ((A2006ContagemResultadoRequisito_Owner&&(A508ContagemResultado_Owner==AV6WWPContext.gxTpr_Userid)) ? 1 : 0);
         imgUpdate_Visible = ((A2006ContagemResultadoRequisito_Owner&&(A508ContagemResultado_Owner==AV6WWPContext.gxTpr_Userid)) ? 1 : 0);
         Dvpanel_unnamedtable2_Title = "Pontua��o total - ["+StringUtil.Str( AV60Total, 14, 5)+"]";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvpanel_unnamedtable2_Internalname, "Title", Dvpanel_unnamedtable2_Title);
         imgPdf_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgPdf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgPdf_Visible), 5, 0)));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 10;
         }
         sendrow_102( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_10_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(10, GridRow);
         }
      }

      protected void E12PG2( )
      {
         /* 'DoNewRN' Routine */
         AV61Websession.Set("Caller", "OS");
         AV61Websession.Set("Codigo", StringUtil.Str( (decimal)(AV67ContagemResultadoRequisito_OSCod), 6, 0));
         AV58height = (short)(AV6WWPContext.gxTpr_Screen_height*0.90m);
         AV57width = (short)(AV6WWPContext.gxTpr_Screen_width*0.90m);
         AV56Window.Autoresize = 0;
         AV56Window.Height = AV58height;
         AV56Window.Width = AV57width;
         AV56Window.Url = formatLink("wp_ss.aspx") ;
         AV56Window.SetReturnParms(new Object[] {});
         context.NewWindow(AV56Window);
      }

      protected void E16PG2( )
      {
         /* 'DoUpdate' Routine */
         context.PopUp(formatLink("requisito.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A2004ContagemResultadoRequisito_ReqCod), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E17PG2( )
      {
         /* 'DoNewReqTec' Routine */
         AV58height = (short)(AV6WWPContext.gxTpr_Screen_height*0.68m);
         AV57width = (short)(AV6WWPContext.gxTpr_Screen_width*0.50m);
         AV56Window.Autoresize = 0;
         AV56Window.Height = ((AV58height<620) ? 620 : AV58height);
         AV56Window.Width = ((AV57width<890) ? 890 : AV57width);
         AV56Window.Url = formatLink("wp_novorequisito.aspx") + "?" + UrlEncode("" +0) + "," + UrlEncode("" +A2004ContagemResultadoRequisito_ReqCod) + "," + UrlEncode("" +AV67ContagemResultadoRequisito_OSCod);
         AV56Window.SetReturnParms(new Object[] {});
         context.NewWindow(AV56Window);
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV70Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV70Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV70Pgmname+"GridState"), "");
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV70Pgmname+"GridState"), "");
         new wwpbaseobjects.savegridstate(context ).execute(  AV70Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV70Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Requisito";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultadoRequisito_OSCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV67ContagemResultadoRequisito_OSCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E11PG2( )
      {
         /* Confirmpanel_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel_Result, "Yes") == 0 )
         {
            new prc_dltrequisito(context ).execute( ref  AV67ContagemResultadoRequisito_OSCod, ref  A2004ContagemResultadoRequisito_ReqCod) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultadoRequisito_OSCod), 6, 0)));
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultadoRequisito_OSCod, AV70Pgmname, A1636ContagemResultado_ServicoSS, A2004ContagemResultadoRequisito_ReqCod, A1932Requisito_Pontuacao, AV65PontosFilhos, AV60Total, A1926Requisito_Agrupador, A2001Requisito_Identificador, A1927Requisito_Titulo, A1934Requisito_Status, A1923Requisito_Descricao, A508ContagemResultado_Owner, AV6WWPContext, A2006ContagemResultadoRequisito_Owner, A1999Requisito_ReqCod, AV63Requisito_Codigo, sPrefix) ;
         }
      }

      protected void S142( )
      {
         /* 'PONTOSFILHOS' Routine */
         AV65PontosFilhos = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65PontosFilhos", StringUtil.LTrim( StringUtil.Str( AV65PontosFilhos, 14, 5)));
         /* Optimized group. */
         /* Using cursor H00PG4 */
         pr_default.execute(2, new Object[] {AV63Requisito_Codigo});
         c1932Requisito_Pontuacao = H00PG4_A1932Requisito_Pontuacao[0];
         n1932Requisito_Pontuacao = H00PG4_n1932Requisito_Pontuacao[0];
         pr_default.close(2);
         AV65PontosFilhos = (decimal)(AV65PontosFilhos+c1932Requisito_Pontuacao);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65PontosFilhos", StringUtil.LTrim( StringUtil.Str( AV65PontosFilhos, 14, 5)));
         /* End optimized group. */
      }

      protected void wb_table1_2_PG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 5, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVPANEL_UNNAMEDTABLE2Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVPANEL_UNNAMEDTABLE2Container"+"Body"+"\" style=\"display:none;\">") ;
            wb_table2_7_PG2( true) ;
         }
         else
         {
            wb_table2_7_PG2( false) ;
         }
         return  ;
      }

      protected void wb_table2_7_PG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgNewrn_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgNewrn_Visible, 1, "", "Criar um Requisito", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgNewrn_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DONEWRN\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisitosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgPdf_Internalname, context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgPdf_Visible, 1, "", imgPdf_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 7, imgPdf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+"e18pg1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisitosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_38_PG2( true) ;
         }
         else
         {
            wb_table3_38_PG2( false) ;
         }
         return  ;
      }

      protected void wb_table3_38_PG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, "script", "", "", lblTbjava_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_ContagemResultadoRequisitosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_PG2e( true) ;
         }
         else
         {
            wb_table1_2_PG2e( false) ;
         }
      }

      protected void wb_table3_38_PG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"INNEWWINDOWContainer"+"\"></div>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"CONFIRMPANELContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_38_PG2e( true) ;
         }
         else
         {
            wb_table3_38_PG2e( false) ;
         }
      }

      protected void wb_table2_7_PG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetIsFreestyle(true);
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"10\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "FreeStyleGrid", 0, "", "", 1, 2, sStyleString, "", 0);
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetIsFreestyle(true);
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", StringUtil.RTrim( "FreeStyleGrid"));
               GridContainer.AddObjectProperty("Class", "FreeStyleGrid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoRequisito_ReqCod_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ))));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ))));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( ))));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 10 )
         {
            wbEnd = 0;
            nRC_GXsfl_10 = (short)(nGXsfl_10_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_7_PG2e( true) ;
         }
         else
         {
            wb_table2_7_PG2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV67ContagemResultadoRequisito_OSCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultadoRequisito_OSCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAPG2( ) ;
         WSPG2( ) ;
         WEPG2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV67ContagemResultadoRequisito_OSCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAPG2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultadorequisitoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAPG2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV67ContagemResultadoRequisito_OSCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultadoRequisito_OSCod), 6, 0)));
         }
         wcpOAV67ContagemResultadoRequisito_OSCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV67ContagemResultadoRequisito_OSCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV67ContagemResultadoRequisito_OSCod != wcpOAV67ContagemResultadoRequisito_OSCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV67ContagemResultadoRequisito_OSCod = AV67ContagemResultadoRequisito_OSCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV67ContagemResultadoRequisito_OSCod = cgiGet( sPrefix+"AV67ContagemResultadoRequisito_OSCod_CTRL");
         if ( StringUtil.Len( sCtrlAV67ContagemResultadoRequisito_OSCod) > 0 )
         {
            AV67ContagemResultadoRequisito_OSCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV67ContagemResultadoRequisito_OSCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultadoRequisito_OSCod), 6, 0)));
         }
         else
         {
            AV67ContagemResultadoRequisito_OSCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV67ContagemResultadoRequisito_OSCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAPG2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSPG2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSPG2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV67ContagemResultadoRequisito_OSCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67ContagemResultadoRequisito_OSCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV67ContagemResultadoRequisito_OSCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV67ContagemResultadoRequisito_OSCod_CTRL", StringUtil.RTrim( sCtrlAV67ContagemResultadoRequisito_OSCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEPG2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
         if ( ! ( WebComp_GX_Process == null ) )
         {
            WebComp_GX_Process.componentjscripts();
         }
         if ( ! ( WebComp_Wcwc_requisitostecnicos == null ) )
         {
            WebComp_Wcwc_requisitostecnicos.componentjscripts();
         }
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         if ( ! ( WebComp_Wcwc_requisitostecnicos == null ) )
         {
            if ( StringUtil.Len( WebComp_Wcwc_requisitostecnicos_Component) != 0 )
            {
               WebComp_Wcwc_requisitostecnicos.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020625191290");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemresultadorequisitoswc.js", "?2020625191290");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_102( )
      {
         lblTextblockdescricao_Internalname = sPrefix+"TEXTBLOCKDESCRICAO_"+sGXsfl_10_idx;
         edtavDescricao_Internalname = sPrefix+"vDESCRICAO_"+sGXsfl_10_idx;
         Dvpanel_tblreqneg_Internalname = sPrefix+"DVPANEL_TBLREQNEG_"+sGXsfl_10_idx;
         edtContagemResultadoRequisito_ReqCod_Internalname = sPrefix+"CONTAGEMRESULTADOREQUISITO_REQCOD_"+sGXsfl_10_idx;
         imgUpdate_Internalname = sPrefix+"UPDATE_"+sGXsfl_10_idx;
         imgActiondelete_Internalname = sPrefix+"ACTIONDELETE_"+sGXsfl_10_idx;
         imgNewreqtec_Internalname = sPrefix+"NEWREQTEC_"+sGXsfl_10_idx;
      }

      protected void SubsflControlProps_fel_102( )
      {
         lblTextblockdescricao_Internalname = sPrefix+"TEXTBLOCKDESCRICAO_"+sGXsfl_10_fel_idx;
         edtavDescricao_Internalname = sPrefix+"vDESCRICAO_"+sGXsfl_10_fel_idx;
         Dvpanel_tblreqneg_Internalname = sPrefix+"DVPANEL_TBLREQNEG_"+sGXsfl_10_fel_idx;
         edtContagemResultadoRequisito_ReqCod_Internalname = sPrefix+"CONTAGEMRESULTADOREQUISITO_REQCOD_"+sGXsfl_10_fel_idx;
         imgUpdate_Internalname = sPrefix+"UPDATE_"+sGXsfl_10_fel_idx;
         imgActiondelete_Internalname = sPrefix+"ACTIONDELETE_"+sGXsfl_10_fel_idx;
         imgNewreqtec_Internalname = sPrefix+"NEWREQTEC_"+sGXsfl_10_fel_idx;
      }

      protected void sendrow_102( )
      {
         SubsflControlProps_102( ) ;
         WBPG0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_10_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)(((nGXsfl_10_idx-1)/ (decimal)(1)) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
            }
            /* Start of Columns property logic. */
            if ( GridContainer.GetWrapped() == 1 )
            {
               if ( ( 1 == 0 ) && ( nGXsfl_10_idx == 1 ) )
               {
                  context.WriteHtmlText( "<tr"+" class=\""+subGrid_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_10_idx+"\">") ;
               }
               if ( 1 > 0 )
               {
                  if ( ( 1 == 1 ) || ( ((int)((nGXsfl_10_idx) % (1))) - 1 == 0 ) )
                  {
                     context.WriteHtmlText( "<tr"+" class=\""+subGrid_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_10_idx+"\">") ;
                  }
               }
            }
            GridRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid_Linesclass,(String)""});
            GridRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "vertical-align:top")+"\""});
            /* User Defined Control */
            GridRow.AddColumnProperties("usercontrol", -1, isAjaxCallMode( ), new Object[] {(String)sPrefix+"DVPANEL_TBLREQNEGContainer"+"_"+sGXsfl_10_idx,(short)-1});
            GridRow.AddColumnProperties("usercontrolcontainer", -1, isAjaxCallMode( ), new Object[] {(String)sPrefix+"DVPANEL_TBLREQNEGContainer",(String)"Body"});
            /* Table start */
            GridRow.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTblreqneg_Internalname+"_"+sGXsfl_10_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
            GridRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
            GridRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
            /* Text block */
            GridRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockdescricao_Internalname,(String)"",(String)"",(String)"",(String)lblTextblockdescricao_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("cell");
            }
            GridRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
            /* Multiple line edit */
            TempTags = " " + ((edtavDescricao_Enabled!=0)&&(edtavDescricao_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 20,'"+sPrefix+"',false,'"+sGXsfl_10_idx+"',10)\"" : " ");
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GridRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavDescricao_Internalname,(String)AV54Descricao,(String)"",TempTags+((edtavDescricao_Enabled!=0)&&(edtavDescricao_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavDescricao_Enabled!=0)&&(edtavDescricao_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,20);\"" : " "),(short)2,(short)1,(int)edtavDescricao_Enabled,(short)0,(short)80,(String)"chr",(short)4,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"500",(short)1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("cell");
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("row");
            }
            GridRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
            GridRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("cell");
            }
            GridRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
            /* WebComponent */
            if ( ( StringUtil.StrCmp(WebCompHandler, "Wcwc_requisitostecnicos") == 0 ) && ( NumberUtil.Val( sGXsfl_10_idx, ".") == Convert.ToDecimal( nGXsfl_10_webc_idx )) )
            {
               if ( ( StringUtil.Len( WebComp_GX_Process_Component) != 0 ) && ( StringUtil.StrCmp(WebComp_GX_Process_Component, "WC_RequisitosTecnicos") == 0 ) )
               {
                  WebComp_GX_Process.setjustcreated();
                  WebComp_GX_Process.componentprepare(new Object[] {(String)sPrefix+"W0024",(String)sGXsfl_10_idx,(int)A2004ContagemResultadoRequisito_ReqCod});
                  WebComp_GX_Process.componentbind(new Object[] {(String)""});
               }
               if ( isFullAjaxMode( ) )
               {
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix+"gxHTMLWrpW00-1"+"");
                  WebComp_GX_Process.componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
               }
               GxWebStd.gx_hidden_field( context, sPrefix+"W0024"+sGXsfl_10_idx, StringUtil.RTrim( WebComp_GX_Process_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+sPrefix+"gxHTMLWrpW0024"+sGXsfl_10_idx+"\""+((WebComp_Wcwc_requisitostecnicos_Visible==1) ? "" : " style=\"display:none;\"")) ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_GX_Process_Component) != 0 )
               {
                  if ( ! context.isAjaxRequest( ) || ( StringUtil.StringSearch( sPrefix+"W0024"+sGXsfl_10_idx, cgiGet( "_EventName"), 1) != 0 ) )
                  {
                     if ( 1 != 0 )
                     {
                        if ( StringUtil.Len( WebComp_GX_Process_Component) != 0 )
                        {
                           WebComp_GX_Process.componentstart();
                        }
                     }
                  }
                  if ( ! context.isAjaxRequest( ) || ( StringUtil.StrCmp(StringUtil.Lower( OldWcwc_requisitostecnicos), StringUtil.Lower( WebComp_GX_Process_Component)) != 0 ) )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp(sPrefix+"gxHTMLWrpW0024"+sGXsfl_10_idx);
                  }
                  WebComp_GX_Process.componentdraw();
                  if ( ! context.isAjaxRequest( ) || ( StringUtil.StrCmp(StringUtil.Lower( OldWcwc_requisitostecnicos), StringUtil.Lower( WebComp_GX_Process_Component)) != 0 ) )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"W0024"+sGXsfl_10_idx, StringUtil.RTrim( WebComp_Wcwc_requisitostecnicos_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+sPrefix+"gxHTMLWrpW0024"+sGXsfl_10_idx+"\""+((WebComp_Wcwc_requisitostecnicos_Visible==1) ? "" : " style=\"display:none;\"")) ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wcwc_requisitostecnicos_Component) != 0 )
               {
                  if ( ! context.isAjaxRequest( ) || ( StringUtil.StringSearch( sPrefix+"W0024"+sGXsfl_10_idx, cgiGet( "_EventName"), 1) != 0 ) )
                  {
                     if ( WebComp_Wcwc_requisitostecnicos_Visible != 0 )
                     {
                        if ( StringUtil.Len( WebComp_Wcwc_requisitostecnicos_Component) != 0 )
                        {
                           WebComp_Wcwc_requisitostecnicos.componentstart();
                        }
                     }
                  }
                  if ( ! context.isAjaxRequest( ) || ( StringUtil.StrCmp(StringUtil.Lower( OldWcwc_requisitostecnicos), StringUtil.Lower( WebComp_Wcwc_requisitostecnicos_Component)) != 0 ) )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp(sPrefix+"gxHTMLWrpW0024"+sGXsfl_10_idx);
                  }
                  WebComp_Wcwc_requisitostecnicos.componentdraw();
                  if ( ! context.isAjaxRequest( ) || ( StringUtil.StrCmp(StringUtil.Lower( OldWcwc_requisitostecnicos), StringUtil.Lower( WebComp_Wcwc_requisitostecnicos_Component)) != 0 ) )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            WebComp_Wcwc_requisitostecnicos_Component = "";
            WebComp_Wcwc_requisitostecnicos.componentjscripts();
            GridRow.AddColumnProperties("webcomp", -1, isAjaxCallMode( ), new Object[] {(String)"Wcwc_requisitostecnicos"});
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("cell");
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("row");
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("table");
            }
            /* End of table */
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoRequisito_ReqCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2004ContagemResultadoRequisito_ReqCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoRequisito_ReqCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(int)edtContagemResultadoRequisito_ReqCod_Visible,(short)0,(short)0,(String)"text",(String)"",(short)6,(String)"chr",(short)1,(String)"row",(short)6,(short)0,(short)0,(short)10,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("cell");
            }
            GridRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "vertical-align:top")+"\""});
            /* Active images/pictures */
            TempTags = " " + ((imgUpdate_Enabled!=0)&&(imgUpdate_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 27,'"+sPrefix+"',false,'',0)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgUpdate_Internalname,context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )),(String)"",(String)"",(String)"",context.GetTheme( ),(int)imgUpdate_Visible,(short)1,(String)"",(String)"Alterar Requisito",(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)imgUpdate_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)false,(bool)false});
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("cell");
            }
            GridRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "vertical-align:top")+"\""});
            /* Active images/pictures */
            TempTags = " " + ((imgActiondelete_Enabled!=0)&&(imgActiondelete_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 29,'"+sPrefix+"',false,'',0)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgActiondelete_Internalname,context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )),(String)"",(String)"",(String)"",context.GetTheme( ),(int)imgActiondelete_Visible,(short)1,(String)"",(String)"",(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)imgActiondelete_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e19pg2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)false,(bool)false});
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("cell");
            }
            GridRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "vertical-align:top")+"\""});
            /* Active images/pictures */
            TempTags = " " + ((imgNewreqtec_Enabled!=0)&&(imgNewreqtec_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 31,'"+sPrefix+"',false,'',0)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgNewreqtec_Internalname,context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )),(String)"",(String)"",(String)"",context.GetTheme( ),(int)imgNewreqtec_Visible,(short)1,(String)"",(String)"Criar um novo Requisito associado",(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)imgNewreqtec_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DONEWREQTEC\\'."+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)false,(bool)false});
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("cell");
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               GridContainer.CloseTag("row");
            }
            GXCCtl = "DVPANEL_TBLREQNEG_Width_" + sGXsfl_10_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Dvpanel_tblreqneg_Width));
            GXCCtl = "DVPANEL_TBLREQNEG_Cls_" + sGXsfl_10_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Dvpanel_tblreqneg_Cls));
            GXCCtl = "DVPANEL_TBLREQNEG_Title_" + sGXsfl_10_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Dvpanel_tblreqneg_Title));
            GXCCtl = "DVPANEL_TBLREQNEG_Collapsible_" + sGXsfl_10_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.BoolToStr( Dvpanel_tblreqneg_Collapsible));
            GXCCtl = "DVPANEL_TBLREQNEG_Collapsed_" + sGXsfl_10_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.BoolToStr( Dvpanel_tblreqneg_Collapsed));
            GXCCtl = "DVPANEL_TBLREQNEG_Autowidth_" + sGXsfl_10_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.BoolToStr( Dvpanel_tblreqneg_Autowidth));
            GXCCtl = "DVPANEL_TBLREQNEG_Autoheight_" + sGXsfl_10_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.BoolToStr( Dvpanel_tblreqneg_Autoheight));
            GXCCtl = "DVPANEL_TBLREQNEG_Showcollapseicon_" + sGXsfl_10_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.BoolToStr( Dvpanel_tblreqneg_Showcollapseicon));
            GXCCtl = "DVPANEL_TBLREQNEG_Iconposition_" + sGXsfl_10_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Dvpanel_tblreqneg_Iconposition));
            GXCCtl = "DVPANEL_TBLREQNEG_Autoscroll_" + sGXsfl_10_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.BoolToStr( Dvpanel_tblreqneg_Autoscroll));
            /* End of Columns property logic. */
            if ( GridContainer.GetWrapped() == 1 )
            {
               if ( 1 > 0 )
               {
                  if ( ((int)((nGXsfl_10_idx) % (1))) == 0 )
                  {
                     context.WriteHtmlTextNl( "</tr>") ;
                  }
               }
            }
            GridContainer.AddRow(GridRow);
            nGXsfl_10_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_10_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_10_idx+1));
            sGXsfl_10_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_10_idx), 4, 0)), 4, "0");
            SubsflControlProps_102( ) ;
         }
         /* End function sendrow_102 */
      }

      protected void init_default_properties( )
      {
         lblTextblockdescricao_Internalname = sPrefix+"TEXTBLOCKDESCRICAO";
         edtavDescricao_Internalname = sPrefix+"vDESCRICAO";
         tblTblreqneg_Internalname = sPrefix+"TBLREQNEG";
         Dvpanel_tblreqneg_Internalname = sPrefix+"DVPANEL_TBLREQNEG";
         edtContagemResultadoRequisito_ReqCod_Internalname = sPrefix+"CONTAGEMRESULTADOREQUISITO_REQCOD";
         imgUpdate_Internalname = sPrefix+"UPDATE";
         imgActiondelete_Internalname = sPrefix+"ACTIONDELETE";
         imgNewreqtec_Internalname = sPrefix+"NEWREQTEC";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         Dvpanel_unnamedtable2_Internalname = sPrefix+"DVPANEL_UNNAMEDTABLE2";
         imgNewrn_Internalname = sPrefix+"NEWRN";
         imgPdf_Internalname = sPrefix+"PDF";
         Innewwindow_Internalname = sPrefix+"INNEWWINDOW";
         Confirmpanel_Internalname = sPrefix+"CONFIRMPANEL";
         tblUsertable_Internalname = sPrefix+"USERTABLE";
         lblTbjava_Internalname = sPrefix+"TBJAVA";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtContagemResultadoRequisito_OSCod_Internalname = sPrefix+"CONTAGEMRESULTADOREQUISITO_OSCOD";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         imgNewreqtec_Visible = 1;
         imgActiondelete_Visible = 1;
         imgUpdate_Visible = 1;
         edtContagemResultadoRequisito_ReqCod_Jsonclick = "";
         edtavDescricao_Visible = 1;
         edtavDescricao_Enabled = 1;
         subGrid_Class = "FreeStyleGrid";
         subGrid_Allowcollapsing = 0;
         lblTbjava_Visible = 1;
         imgPdf_Visible = 1;
         imgNewrn_Visible = 1;
         imgPdf_Tooltiptext = "Gerar em PDF";
         WebComp_Wcwc_requisitostecnicos_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, sPrefix+"gxHTMLWrpW0024"+sGXsfl_10_idx, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(WebComp_Wcwc_requisitostecnicos_Visible), 5, 0)));
         subGrid_Backcolorstyle = 0;
         Dvpanel_tblreqneg_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tblreqneg_Iconposition = "left";
         Dvpanel_tblreqneg_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tblreqneg_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tblreqneg_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tblreqneg_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tblreqneg_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_tblreqneg_Title = "Requisito";
         Dvpanel_tblreqneg_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tblreqneg_Width = "100%";
         edtContagemResultadoRequisito_OSCod_Jsonclick = "";
         edtContagemResultadoRequisito_OSCod_Visible = 1;
         Confirmpanel_Yesbuttonposition = "left";
         Confirmpanel_Cancelbuttoncaption = "Cancel";
         Confirmpanel_Nobuttoncaption = "N�o";
         Confirmpanel_Yesbuttoncaption = "Sim";
         Confirmpanel_Confirmationtext = "Confirma eliminar este requisito?";
         Confirmpanel_Title = "Confirma��o";
         Innewwindow_Target = "";
         Dvpanel_unnamedtable2_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable2_Iconposition = "left";
         Dvpanel_unnamedtable2_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable2_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable2_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable2_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable2_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable2_Title = "Pontua��o total:";
         Dvpanel_unnamedtable2_Cls = "GXUI-DVelop-Panel";
         Dvpanel_unnamedtable2_Width = "100%";
         subGrid_Rows = 0;
         edtContagemResultadoRequisito_ReqCod_Visible = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'edtContagemResultadoRequisito_ReqCod_Visible',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'imgNewreqtec_Linktarget',ctrl:'NEWREQTEC',prop:'Linktarget'},{av:'imgPdf_Visible',ctrl:'PDF',prop:'Visible'},{av:'imgPdf_Tooltiptext',ctrl:'PDF',prop:'Tooltiptext'},{av:'imgNewrn_Visible',ctrl:'NEWRN',prop:'Visible'},{av:'imgNewreqtec_Visible',ctrl:'NEWREQTEC',prop:'Visible'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'imgActiondelete_Visible',ctrl:'ACTIONDELETE',prop:'Visible'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E15PG2',iparms:[{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'WCWC_REQUISITOSTECNICOS'},{ctrl:'WCWC_REQUISITOSTECNICOS',prop:'Visible'},{av:'imgNewreqtec_Visible',ctrl:'NEWREQTEC',prop:'Visible'},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Dvpanel_tblreqneg_Title',ctrl:'DVPANEL_TBLREQNEG',prop:'Title'},{av:'AV54Descricao',fld:'vDESCRICAO',pic:'',nv:''},{av:'imgNewrn_Visible',ctrl:'NEWRN',prop:'Visible'},{av:'imgActiondelete_Visible',ctrl:'ACTIONDELETE',prop:'Visible'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'Dvpanel_unnamedtable2_Title',ctrl:'DVPANEL_UNNAMEDTABLE2',prop:'Title'},{av:'imgPdf_Visible',ctrl:'PDF',prop:'Visible'},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("'DONEWRN'","{handler:'E12PG2',iparms:[{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOPDF'","{handler:'E18PG1',iparms:[{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         setEventMetadata("'DOUPDATE'","{handler:'E16PG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'edtContagemResultadoRequisito_ReqCod_Visible',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'DOACTIONDELETE'","{handler:'E19PG2',iparms:[],oparms:[]}");
         setEventMetadata("'DONEWREQTEC'","{handler:'E17PG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'edtContagemResultadoRequisito_ReqCod_Visible',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E11PG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'edtContagemResultadoRequisito_ReqCod_Visible',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Confirmpanel_Result',ctrl:'CONFIRMPANEL',prop:'Result'}],oparms:[{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'edtContagemResultadoRequisito_ReqCod_Visible',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'imgNewreqtec_Linktarget',ctrl:'NEWREQTEC',prop:'Linktarget'},{av:'imgPdf_Visible',ctrl:'PDF',prop:'Visible'},{av:'imgPdf_Tooltiptext',ctrl:'PDF',prop:'Tooltiptext'},{av:'imgNewrn_Visible',ctrl:'NEWRN',prop:'Visible'},{av:'imgNewreqtec_Visible',ctrl:'NEWREQTEC',prop:'Visible'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'imgActiondelete_Visible',ctrl:'ACTIONDELETE',prop:'Visible'}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'edtContagemResultadoRequisito_ReqCod_Visible',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'imgNewreqtec_Linktarget',ctrl:'NEWREQTEC',prop:'Linktarget'},{av:'imgPdf_Visible',ctrl:'PDF',prop:'Visible'},{av:'imgPdf_Tooltiptext',ctrl:'PDF',prop:'Tooltiptext'},{av:'imgNewrn_Visible',ctrl:'NEWRN',prop:'Visible'},{av:'imgNewreqtec_Visible',ctrl:'NEWREQTEC',prop:'Visible'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'imgActiondelete_Visible',ctrl:'ACTIONDELETE',prop:'Visible'}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'edtContagemResultadoRequisito_ReqCod_Visible',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'imgNewreqtec_Linktarget',ctrl:'NEWREQTEC',prop:'Linktarget'},{av:'imgPdf_Visible',ctrl:'PDF',prop:'Visible'},{av:'imgPdf_Tooltiptext',ctrl:'PDF',prop:'Tooltiptext'},{av:'imgNewrn_Visible',ctrl:'NEWRN',prop:'Visible'},{av:'imgNewreqtec_Visible',ctrl:'NEWREQTEC',prop:'Visible'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'imgActiondelete_Visible',ctrl:'ACTIONDELETE',prop:'Visible'}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'edtContagemResultadoRequisito_ReqCod_Visible',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'imgNewreqtec_Linktarget',ctrl:'NEWREQTEC',prop:'Linktarget'},{av:'imgPdf_Visible',ctrl:'PDF',prop:'Visible'},{av:'imgPdf_Tooltiptext',ctrl:'PDF',prop:'Tooltiptext'},{av:'imgNewrn_Visible',ctrl:'NEWRN',prop:'Visible'},{av:'imgNewreqtec_Visible',ctrl:'NEWREQTEC',prop:'Visible'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'imgActiondelete_Visible',ctrl:'ACTIONDELETE',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Confirmpanel_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV70Pgmname = "";
         A1926Requisito_Agrupador = "";
         A2001Requisito_Identificador = "";
         A1927Requisito_Titulo = "";
         A1923Requisito_Descricao = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         AV54Descricao = "";
         OldWcwc_requisitostecnicos = "";
         sCmpCtrl = "";
         WebComp_GX_Process_Component = "";
         GridContainer = new GXWebGrid( context);
         WebComp_Wcwc_requisitostecnicos_Component = "";
         scmdbuf = "";
         H00PG2_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         H00PG2_A1999Requisito_ReqCod = new int[1] ;
         H00PG2_n1999Requisito_ReqCod = new bool[] {false} ;
         H00PG2_A1636ContagemResultado_ServicoSS = new int[1] ;
         H00PG2_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         H00PG2_A1932Requisito_Pontuacao = new decimal[1] ;
         H00PG2_n1932Requisito_Pontuacao = new bool[] {false} ;
         H00PG2_A1934Requisito_Status = new short[1] ;
         H00PG2_n1934Requisito_Status = new bool[] {false} ;
         H00PG2_A1927Requisito_Titulo = new String[] {""} ;
         H00PG2_n1927Requisito_Titulo = new bool[] {false} ;
         H00PG2_A2001Requisito_Identificador = new String[] {""} ;
         H00PG2_n2001Requisito_Identificador = new bool[] {false} ;
         H00PG2_A1926Requisito_Agrupador = new String[] {""} ;
         H00PG2_n1926Requisito_Agrupador = new bool[] {false} ;
         H00PG2_A1923Requisito_Descricao = new String[] {""} ;
         H00PG2_n1923Requisito_Descricao = new bool[] {false} ;
         H00PG2_A508ContagemResultado_Owner = new int[1] ;
         H00PG2_n508ContagemResultado_Owner = new bool[] {false} ;
         H00PG2_A2006ContagemResultadoRequisito_Owner = new bool[] {false} ;
         H00PG2_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         H00PG2_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         H00PG3_AGRID_nRecordCount = new long[1] ;
         AV45Agrupador = "";
         imgNewreqtec_Linktarget = "";
         GridRow = new GXWebRow();
         AV61Websession = context.GetSession();
         AV56Window = new GXWindow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         H00PG4_A1932Requisito_Pontuacao = new decimal[1] ;
         H00PG4_n1932Requisito_Pontuacao = new bool[] {false} ;
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         imgNewrn_Jsonclick = "";
         imgPdf_Jsonclick = "";
         lblTbjava_Jsonclick = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV67ContagemResultadoRequisito_OSCod = "";
         subGrid_Linesclass = "";
         lblTextblockdescricao_Jsonclick = "";
         ROClassString = "";
         imgUpdate_Jsonclick = "";
         imgActiondelete_Jsonclick = "";
         imgNewreqtec_Jsonclick = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadorequisitoswc__default(),
            new Object[][] {
                new Object[] {
               H00PG2_A2005ContagemResultadoRequisito_Codigo, H00PG2_A1999Requisito_ReqCod, H00PG2_n1999Requisito_ReqCod, H00PG2_A1636ContagemResultado_ServicoSS, H00PG2_n1636ContagemResultado_ServicoSS, H00PG2_A1932Requisito_Pontuacao, H00PG2_n1932Requisito_Pontuacao, H00PG2_A1934Requisito_Status, H00PG2_n1934Requisito_Status, H00PG2_A1927Requisito_Titulo,
               H00PG2_n1927Requisito_Titulo, H00PG2_A2001Requisito_Identificador, H00PG2_n2001Requisito_Identificador, H00PG2_A1926Requisito_Agrupador, H00PG2_n1926Requisito_Agrupador, H00PG2_A1923Requisito_Descricao, H00PG2_n1923Requisito_Descricao, H00PG2_A508ContagemResultado_Owner, H00PG2_n508ContagemResultado_Owner, H00PG2_A2006ContagemResultadoRequisito_Owner,
               H00PG2_A2003ContagemResultadoRequisito_OSCod, H00PG2_A2004ContagemResultadoRequisito_ReqCod
               }
               , new Object[] {
               H00PG3_AGRID_nRecordCount
               }
               , new Object[] {
               H00PG4_A1932Requisito_Pontuacao, H00PG4_n1932Requisito_Pontuacao
               }
            }
         );
         WebComp_GX_Process = new GeneXus.Http.GXNullWebComponent();
         WebComp_Wcwc_requisitostecnicos = new GeneXus.Http.GXNullWebComponent();
         AV70Pgmname = "ContagemResultadoRequisitosWC";
         /* GeneXus formulas. */
         AV70Pgmname = "ContagemResultadoRequisitosWC";
         context.Gx_err = 0;
         edtavDescricao_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_10 ;
      private short nGXsfl_10_idx=1 ;
      private short A1934Requisito_Status ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_10_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short AV58height ;
      private short AV57width ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV67ContagemResultadoRequisito_OSCod ;
      private int wcpOAV67ContagemResultadoRequisito_OSCod ;
      private int edtContagemResultadoRequisito_ReqCod_Visible ;
      private int subGrid_Rows ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int A508ContagemResultado_Owner ;
      private int A1999Requisito_ReqCod ;
      private int AV63Requisito_Codigo ;
      private int edtavDescricao_Enabled ;
      private int AV7ContagemResultado_Codigo ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int edtContagemResultadoRequisito_OSCod_Visible ;
      private int nGXsfl_10_webc_idx=0 ;
      private int subGrid_Islastpage ;
      private int WebComp_Wcwc_requisitostecnicos_Visible ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int lblTbjava_Visible ;
      private int imgPdf_Visible ;
      private int imgNewrn_Visible ;
      private int imgNewreqtec_Visible ;
      private int imgUpdate_Visible ;
      private int imgActiondelete_Visible ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int subGrid_Allbackcolor ;
      private int edtavDescricao_Visible ;
      private int imgUpdate_Enabled ;
      private int imgActiondelete_Enabled ;
      private int imgNewreqtec_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A1932Requisito_Pontuacao ;
      private decimal AV65PontosFilhos ;
      private decimal AV60Total ;
      private decimal AV59Pontos ;
      private decimal c1932Requisito_Pontuacao ;
      private String Confirmpanel_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_10_idx="0001" ;
      private String edtContagemResultadoRequisito_ReqCod_Internalname ;
      private String AV70Pgmname ;
      private String GXKey ;
      private String edtavDescricao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_unnamedtable2_Width ;
      private String Dvpanel_unnamedtable2_Cls ;
      private String Dvpanel_unnamedtable2_Title ;
      private String Dvpanel_unnamedtable2_Iconposition ;
      private String Innewwindow_Target ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Confirmationtext ;
      private String Confirmpanel_Yesbuttoncaption ;
      private String Confirmpanel_Nobuttoncaption ;
      private String Confirmpanel_Cancelbuttoncaption ;
      private String Confirmpanel_Yesbuttonposition ;
      private String GX_FocusControl ;
      private String edtContagemResultadoRequisito_OSCod_Internalname ;
      private String edtContagemResultadoRequisito_OSCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Dvpanel_tblreqneg_Width ;
      private String Dvpanel_tblreqneg_Internalname ;
      private String Dvpanel_tblreqneg_Cls ;
      private String Dvpanel_tblreqneg_Title ;
      private String Dvpanel_tblreqneg_Iconposition ;
      private String OldWcwc_requisitostecnicos ;
      private String sCmpCtrl ;
      private String WebComp_GX_Process_Component ;
      private String WebCompHandler="" ;
      private String WebComp_Wcwc_requisitostecnicos_Component ;
      private String scmdbuf ;
      private String Dvpanel_unnamedtable2_Internalname ;
      private String lblTbjava_Internalname ;
      private String imgNewreqtec_Linktarget ;
      private String imgNewreqtec_Internalname ;
      private String imgPdf_Internalname ;
      private String imgPdf_Tooltiptext ;
      private String imgNewrn_Internalname ;
      private String imgUpdate_Internalname ;
      private String imgActiondelete_Internalname ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String imgNewrn_Jsonclick ;
      private String imgPdf_Jsonclick ;
      private String lblTbjava_Jsonclick ;
      private String tblUsertable_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String subGrid_Internalname ;
      private String sCtrlAV67ContagemResultadoRequisito_OSCod ;
      private String lblTextblockdescricao_Internalname ;
      private String sGXsfl_10_fel_idx="0001" ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTblreqneg_Internalname ;
      private String lblTextblockdescricao_Jsonclick ;
      private String ROClassString ;
      private String edtContagemResultadoRequisito_ReqCod_Jsonclick ;
      private String imgUpdate_Jsonclick ;
      private String imgActiondelete_Jsonclick ;
      private String imgNewreqtec_Jsonclick ;
      private String Innewwindow_Internalname ;
      private String Confirmpanel_Internalname ;
      private bool entryPointCalled ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n1932Requisito_Pontuacao ;
      private bool n1926Requisito_Agrupador ;
      private bool n2001Requisito_Identificador ;
      private bool n1927Requisito_Titulo ;
      private bool n1934Requisito_Status ;
      private bool n1923Requisito_Descricao ;
      private bool n508ContagemResultado_Owner ;
      private bool A2006ContagemResultadoRequisito_Owner ;
      private bool n1999Requisito_ReqCod ;
      private bool toggleJsOutput ;
      private bool Dvpanel_unnamedtable2_Collapsible ;
      private bool Dvpanel_unnamedtable2_Collapsed ;
      private bool Dvpanel_unnamedtable2_Autowidth ;
      private bool Dvpanel_unnamedtable2_Autoheight ;
      private bool Dvpanel_unnamedtable2_Showcollapseicon ;
      private bool Dvpanel_unnamedtable2_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool Dvpanel_tblreqneg_Collapsible ;
      private bool Dvpanel_tblreqneg_Collapsed ;
      private bool Dvpanel_tblreqneg_Autowidth ;
      private bool Dvpanel_tblreqneg_Autoheight ;
      private bool Dvpanel_tblreqneg_Showcollapseicon ;
      private bool Dvpanel_tblreqneg_Autoscroll ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String A1923Requisito_Descricao ;
      private String AV54Descricao ;
      private String A1926Requisito_Agrupador ;
      private String A2001Requisito_Identificador ;
      private String A1927Requisito_Titulo ;
      private String AV45Agrupador ;
      private IGxSession AV15Session ;
      private GXWebComponent WebComp_Wcwc_requisitostecnicos ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXWebComponent WebComp_GX_Process ;
      private IDataStoreProvider pr_default ;
      private int[] H00PG2_A2005ContagemResultadoRequisito_Codigo ;
      private int[] H00PG2_A1999Requisito_ReqCod ;
      private bool[] H00PG2_n1999Requisito_ReqCod ;
      private int[] H00PG2_A1636ContagemResultado_ServicoSS ;
      private bool[] H00PG2_n1636ContagemResultado_ServicoSS ;
      private decimal[] H00PG2_A1932Requisito_Pontuacao ;
      private bool[] H00PG2_n1932Requisito_Pontuacao ;
      private short[] H00PG2_A1934Requisito_Status ;
      private bool[] H00PG2_n1934Requisito_Status ;
      private String[] H00PG2_A1927Requisito_Titulo ;
      private bool[] H00PG2_n1927Requisito_Titulo ;
      private String[] H00PG2_A2001Requisito_Identificador ;
      private bool[] H00PG2_n2001Requisito_Identificador ;
      private String[] H00PG2_A1926Requisito_Agrupador ;
      private bool[] H00PG2_n1926Requisito_Agrupador ;
      private String[] H00PG2_A1923Requisito_Descricao ;
      private bool[] H00PG2_n1923Requisito_Descricao ;
      private int[] H00PG2_A508ContagemResultado_Owner ;
      private bool[] H00PG2_n508ContagemResultado_Owner ;
      private bool[] H00PG2_A2006ContagemResultadoRequisito_Owner ;
      private int[] H00PG2_A2003ContagemResultadoRequisito_OSCod ;
      private int[] H00PG2_A2004ContagemResultadoRequisito_ReqCod ;
      private long[] H00PG3_AGRID_nRecordCount ;
      private decimal[] H00PG4_A1932Requisito_Pontuacao ;
      private bool[] H00PG4_n1932Requisito_Pontuacao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private IGxSession AV61Websession ;
      private GXWindow AV56Window ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contagemresultadorequisitoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00PG2 ;
          prmH00PG2 = new Object[] {
          new Object[] {"@AV67ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00PG3 ;
          prmH00PG3 = new Object[] {
          new Object[] {"@AV67ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00PG4 ;
          prmH00PG4 = new Object[] {
          new Object[] {"@AV63Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00PG2", "SELECT * FROM (SELECT  T1.[ContagemResultadoRequisito_Codigo], T3.[Requisito_ReqCod], T2.[ContagemResultado_ServicoSS], T3.[Requisito_Pontuacao], T3.[Requisito_Status], T3.[Requisito_Titulo], T3.[Requisito_Identificador], T3.[Requisito_Agrupador], T3.[Requisito_Descricao], T2.[ContagemResultado_Owner], T1.[ContagemResultadoRequisito_Owner], T1.[ContagemResultadoRequisito_OSCod] AS ContagemResultadoRequisito_OSCod, T1.[ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod, ROW_NUMBER() OVER ( ORDER BY T1.[ContagemResultadoRequisito_OSCod] ) AS GX_ROW_NUMBER FROM (([ContagemResultadoRequisito] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoRequisito_OSCod]) INNER JOIN [Requisito] T3 WITH (NOLOCK) ON T3.[Requisito_Codigo] = T1.[ContagemResultadoRequisito_ReqCod]) WHERE (T1.[ContagemResultadoRequisito_OSCod] = @AV67ContagemResultadoRequisito_OSCod) AND (T3.[Requisito_ReqCod] IS NULL)) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom2 AND @GXPagingTo2 OR @GXPagingTo2 < @GXPagingFrom2 AND GX_ROW_NUMBER >= @GXPagingFrom2",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00PG2,11,0,true,false )
             ,new CursorDef("H00PG3", "SELECT COUNT(*) FROM (([ContagemResultadoRequisito] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoRequisito_OSCod]) INNER JOIN [Requisito] T3 WITH (NOLOCK) ON T3.[Requisito_Codigo] = T1.[ContagemResultadoRequisito_ReqCod]) WHERE (T1.[ContagemResultadoRequisito_OSCod] = @AV67ContagemResultadoRequisito_OSCod) AND (T3.[Requisito_ReqCod] IS NULL) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00PG3,1,0,true,false )
             ,new CursorDef("H00PG4", "SELECT SUM([Requisito_Pontuacao]) FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_ReqCod] = @AV63Requisito_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00PG4,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
