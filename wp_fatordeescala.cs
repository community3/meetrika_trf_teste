/*
               File: WP_FatorDeEscala
        Description: Fator de Escala
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:30:58.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_fatordeescala : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wp_fatordeescala( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wp_fatordeescala( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           ref int aP1_Projeto_Codigo ,
                           ref String aP2_Tipo ,
                           out decimal aP3_Fator )
      {
         this.AV17AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.A648Projeto_Codigo = aP1_Projeto_Codigo;
         this.AV32Tipo = aP2_Tipo;
         this.AV27Fator = 0 ;
         executePrivate();
         aP1_Projeto_Codigo=this.A648Projeto_Codigo;
         aP2_Tipo=this.AV32Tipo;
         aP3_Fator=this.AV27Fator;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavBase = new GXCombobox();
         cmbavIncremento = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV17AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17AreaTrabalho_Codigo), 6, 0)));
                  A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
                  AV32Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tipo", AV32Tipo);
                  AV27Fator = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Fator", StringUtil.LTrim( StringUtil.Str( AV27Fator, 6, 2)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV17AreaTrabalho_Codigo,(int)A648Projeto_Codigo,(String)AV32Tipo,(decimal)AV27Fator});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_5 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_5_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_5_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
                  AV32Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tipo", AV32Tipo);
                  A961VariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
                  AV17AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17AreaTrabalho_Codigo), 6, 0)));
                  A965VariavelCocomo_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n965VariavelCocomo_ProjetoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0)));
                  A964VariavelCocomo_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
                  A963VariavelCocomo_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A963VariavelCocomo_Nome", A963VariavelCocomo_Nome);
                  A962VariavelCocomo_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
                  A969VariavelCocomo_Nominal = NumberUtil.Val( GetNextPar( ), ".");
                  n969VariavelCocomo_Nominal = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A969VariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( A969VariavelCocomo_Nominal, 12, 2)));
                  A968VariavelCocomo_Baixo = NumberUtil.Val( GetNextPar( ), ".");
                  n968VariavelCocomo_Baixo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A968VariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( A968VariavelCocomo_Baixo, 12, 2)));
                  A967VariavelCocomo_MuitoBaixo = NumberUtil.Val( GetNextPar( ), ".");
                  n967VariavelCocomo_MuitoBaixo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A967VariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( A967VariavelCocomo_MuitoBaixo, 12, 2)));
                  A986VariavelCocomo_ExtraBaixo = NumberUtil.Val( GetNextPar( ), ".");
                  n986VariavelCocomo_ExtraBaixo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A986VariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( A986VariavelCocomo_ExtraBaixo, 12, 2)));
                  A970VariavelCocomo_Alto = NumberUtil.Val( GetNextPar( ), ".");
                  n970VariavelCocomo_Alto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A970VariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( A970VariavelCocomo_Alto, 12, 2)));
                  A971VariavelCocomo_MuitoAlto = NumberUtil.Val( GetNextPar( ), ".");
                  n971VariavelCocomo_MuitoAlto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A971VariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( A971VariavelCocomo_MuitoAlto, 12, 2)));
                  A972VariavelCocomo_ExtraAlto = NumberUtil.Val( GetNextPar( ), ".");
                  n972VariavelCocomo_ExtraAlto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A972VariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( A972VariavelCocomo_ExtraAlto, 12, 2)));
                  A973VariavelCocomo_Usado = NumberUtil.Val( GetNextPar( ), ".");
                  n973VariavelCocomo_Usado = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A973VariavelCocomo_Usado", StringUtil.LTrim( StringUtil.Str( A973VariavelCocomo_Usado, 12, 2)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV48sdt_variavel);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV47sdt_variaveiscocomo);
                  AV5Projeto_Fator = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
                  AV26i = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavI_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV26i), 4, 0)));
                  AV39QtdLinhas = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39QtdLinhas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39QtdLinhas), 4, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( A648Projeto_Codigo, AV32Tipo, A961VariavelCocomo_AreaTrabalhoCod, AV17AreaTrabalho_Codigo, A965VariavelCocomo_ProjetoCod, A964VariavelCocomo_Tipo, A963VariavelCocomo_Nome, A962VariavelCocomo_Sigla, A969VariavelCocomo_Nominal, A968VariavelCocomo_Baixo, A967VariavelCocomo_MuitoBaixo, A986VariavelCocomo_ExtraBaixo, A970VariavelCocomo_Alto, A971VariavelCocomo_MuitoAlto, A972VariavelCocomo_ExtraAlto, A973VariavelCocomo_Usado, AV48sdt_variavel, AV47sdt_variaveiscocomo, AV5Projeto_Fator, AV26i, AV39QtdLinhas, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAGS2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavI_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavI_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavI_Enabled), 5, 0)));
               edtavOldbase_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOldbase_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOldbase_Enabled), 5, 0)));
               edtavNome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
               edtavValor_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavValor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValor_Enabled), 5, 0)));
               edtavProjeto_fator_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavProjeto_fator_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_fator_Enabled), 5, 0)));
               WSGS2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Fator de Escala") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020548305896");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_fatordeescala.aspx") + "?" + UrlEncode("" +AV17AreaTrabalho_Codigo) + "," + UrlEncode("" +A648Projeto_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV32Tipo)) + "," + UrlEncode(StringUtil.Str(AV27Fator,6,2))+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_5", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_5), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV17AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV17AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA648Projeto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV32Tipo", StringUtil.RTrim( wcpOAV32Tipo));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTIPO", StringUtil.RTrim( AV32Tipo));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_TIPO", StringUtil.RTrim( A964VariavelCocomo_Tipo));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_NOME", StringUtil.RTrim( A963VariavelCocomo_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_SIGLA", StringUtil.RTrim( A962VariavelCocomo_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_NOMINAL", StringUtil.LTrim( StringUtil.NToC( A969VariavelCocomo_Nominal, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_BAIXO", StringUtil.LTrim( StringUtil.NToC( A968VariavelCocomo_Baixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_MUITOBAIXO", StringUtil.LTrim( StringUtil.NToC( A967VariavelCocomo_MuitoBaixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_EXTRABAIXO", StringUtil.LTrim( StringUtil.NToC( A986VariavelCocomo_ExtraBaixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_ALTO", StringUtil.LTrim( StringUtil.NToC( A970VariavelCocomo_Alto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_MUITOALTO", StringUtil.LTrim( StringUtil.NToC( A971VariavelCocomo_MuitoAlto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_EXTRAALTO", StringUtil.LTrim( StringUtil.NToC( A972VariavelCocomo_ExtraAlto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_USADO", StringUtil.LTrim( StringUtil.NToC( A973VariavelCocomo_Usado, 12, 2, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSDT_VARIAVEL", AV48sdt_variavel);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSDT_VARIAVEL", AV48sdt_variavel);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSDT_VARIAVEISCOCOMO", AV47sdt_variaveiscocomo);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSDT_VARIAVEISCOCOMO", AV47sdt_variaveiscocomo);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vQTDLINHAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39QtdLinhas), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vVALORES", AV15Valores);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vVALORES", AV15Valores);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vOLDINCREMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20OldIncremento), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFATORINICIAL", StringUtil.LTrim( StringUtil.NToC( AV38FatorInicial, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFATOR", StringUtil.LTrim( StringUtil.NToC( AV27Fator, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vP", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36p), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPERCENTUAL", StringUtil.LTrim( StringUtil.NToC( AV22Percentual, 6, 2, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormGS2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wp_fatordeescala.js", "?202054830590");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WP_FatorDeEscala" ;
      }

      public override String GetPgmdesc( )
      {
         return "Fator de Escala" ;
      }

      protected void WBGS0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wp_fatordeescala.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_GS2( true) ;
         }
         else
         {
            wb_table1_2_GS2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GS2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTGS2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Fator de Escala", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPGS0( ) ;
            }
         }
      }

      protected void WSGS2( )
      {
         STARTGS2( ) ;
         EVTGS2( ) ;
      }

      protected void EVTGS2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGS0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENTER'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGS0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11GS2 */
                                    E11GS2 ();
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGS0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavI_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGS0( ) ;
                              }
                              nGXsfl_5_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
                              SubsflControlProps_52( ) ;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavI_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavI_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vI");
                                 GX_FocusControl = edtavI_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV26i = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavI_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV26i), 4, 0)));
                              }
                              else
                              {
                                 AV26i = (short)(context.localUtil.CToN( cgiGet( edtavI_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavI_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV26i), 4, 0)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavOldbase_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOldbase_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vOLDBASE");
                                 GX_FocusControl = edtavOldbase_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV19OldBase = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavOldbase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV19OldBase), 4, 0)));
                              }
                              else
                              {
                                 AV19OldBase = (short)(context.localUtil.CToN( cgiGet( edtavOldbase_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavOldbase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV19OldBase), 4, 0)));
                              }
                              AV16Nome = StringUtil.Upper( cgiGet( edtavNome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNome_Internalname, AV16Nome);
                              cmbavBase.Name = cmbavBase_Internalname;
                              cmbavBase.CurrentValue = cgiGet( cmbavBase_Internalname);
                              AV13Base = (short)(NumberUtil.Val( cgiGet( cmbavBase_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
                              cmbavIncremento.Name = cmbavIncremento_Internalname;
                              cmbavIncremento.CurrentValue = cgiGet( cmbavIncremento_Internalname);
                              AV12Incremento = (short)(NumberUtil.Val( cgiGet( cmbavIncremento_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavValor_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavValor_Internalname), ",", ".") > 999999999.99m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vVALOR");
                                 GX_FocusControl = edtavValor_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV21Valor = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavValor_Internalname, StringUtil.LTrim( StringUtil.Str( AV21Valor, 12, 2)));
                              }
                              else
                              {
                                 AV21Valor = context.localUtil.CToN( cgiGet( edtavValor_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavValor_Internalname, StringUtil.LTrim( StringUtil.Str( AV21Valor, 12, 2)));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavI_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12GS2 */
                                          E12GS2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavI_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13GS2 */
                                          E13GS2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavI_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14GS2 */
                                          E14GS2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                                /* Execute user event: E11GS2 */
                                                E11GS2 ();
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPGS0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavI_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGS2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGS2( ) ;
            }
         }
      }

      protected void PAGS2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "vBASE_" + sGXsfl_5_idx;
            cmbavBase.Name = GXCCtl;
            cmbavBase.WebTags = "";
            cmbavBase.addItem("1", "Nominal", 0);
            cmbavBase.addItem("2", "Baixo", 0);
            cmbavBase.addItem("3", "Muito Baixo", 0);
            cmbavBase.addItem("4", "Extra Baixo", 0);
            cmbavBase.addItem("5", "Alto", 0);
            cmbavBase.addItem("6", "Muito Alto", 0);
            cmbavBase.addItem("7", "Extra Alto", 0);
            if ( cmbavBase.ItemCount > 0 )
            {
               AV13Base = (short)(NumberUtil.Val( cmbavBase.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13Base), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
            }
            GXCCtl = "vINCREMENTO_" + sGXsfl_5_idx;
            cmbavIncremento.Name = GXCCtl;
            cmbavIncremento.WebTags = "";
            cmbavIncremento.addItem("0", "0%", 0);
            cmbavIncremento.addItem("25", "25%", 0);
            cmbavIncremento.addItem("50", "50%", 0);
            cmbavIncremento.addItem("100", "100%", 0);
            if ( cmbavIncremento.ItemCount > 0 )
            {
               AV12Incremento = (short)(NumberUtil.Val( cmbavIncremento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavProjeto_fator_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_52( ) ;
         while ( nGXsfl_5_idx <= nRC_GXsfl_5 )
         {
            sendrow_52( ) ;
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int A648Projeto_Codigo ,
                                       String AV32Tipo ,
                                       int A961VariavelCocomo_AreaTrabalhoCod ,
                                       int AV17AreaTrabalho_Codigo ,
                                       int A965VariavelCocomo_ProjetoCod ,
                                       String A964VariavelCocomo_Tipo ,
                                       String A963VariavelCocomo_Nome ,
                                       String A962VariavelCocomo_Sigla ,
                                       decimal A969VariavelCocomo_Nominal ,
                                       decimal A968VariavelCocomo_Baixo ,
                                       decimal A967VariavelCocomo_MuitoBaixo ,
                                       decimal A986VariavelCocomo_ExtraBaixo ,
                                       decimal A970VariavelCocomo_Alto ,
                                       decimal A971VariavelCocomo_MuitoAlto ,
                                       decimal A972VariavelCocomo_ExtraAlto ,
                                       decimal A973VariavelCocomo_Usado ,
                                       SdtSDT_VariaveisCocomo_Item AV48sdt_variavel ,
                                       IGxCollection AV47sdt_variaveiscocomo ,
                                       decimal AV5Projeto_Fator ,
                                       short AV26i ,
                                       short AV39QtdLinhas ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFGS2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGS2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavI_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavI_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavI_Enabled), 5, 0)));
         edtavOldbase_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOldbase_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOldbase_Enabled), 5, 0)));
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
         edtavValor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavValor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValor_Enabled), 5, 0)));
         edtavProjeto_fator_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavProjeto_fator_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_fator_Enabled), 5, 0)));
      }

      protected void RFGS2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 5;
         /* Execute user event: E13GS2 */
         E13GS2 ();
         nGXsfl_5_idx = 1;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         nGXsfl_5_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_52( ) ;
            /* Using cursor H00GS2 */
            pr_default.execute(0, new Object[] {A648Projeto_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               /* Execute user event: E14GS2 */
               E14GS2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            wbEnd = 5;
            WBGS0( ) ;
         }
         nGXsfl_5_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPGS0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavI_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavI_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavI_Enabled), 5, 0)));
         edtavOldbase_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOldbase_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOldbase_Enabled), 5, 0)));
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
         edtavValor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavValor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValor_Enabled), 5, 0)));
         edtavProjeto_fator_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavProjeto_fator_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_fator_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12GS2 */
         E12GS2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSDT_VARIAVEISCOCOMO"), AV47sdt_variaveiscocomo);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSDT_VARIAVEL"), AV48sdt_variavel);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavProjeto_fator_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavProjeto_fator_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPROJETO_FATOR");
               GX_FocusControl = edtavProjeto_fator_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV5Projeto_Fator = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
            }
            else
            {
               AV5Projeto_Fator = context.localUtil.CToN( cgiGet( edtavProjeto_fator_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
            }
            /* Read saved values. */
            nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
            wcpOAV17AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV17AreaTrabalho_Codigo"), ",", "."));
            wcpOA648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA648Projeto_Codigo"), ",", "."));
            wcpOAV32Tipo = cgiGet( sPrefix+"wcpOAV32Tipo");
            AV32Tipo = cgiGet( sPrefix+"vTIPO");
            AV36p = (short)(context.localUtil.CToN( cgiGet( sPrefix+"vP"), ",", "."));
            AV39QtdLinhas = (short)(context.localUtil.CToN( cgiGet( sPrefix+"vQTDLINHAS"), ",", "."));
            AV27Fator = context.localUtil.CToN( cgiGet( sPrefix+"vFATOR"), ",", ".");
            AV20OldIncremento = (short)(context.localUtil.CToN( cgiGet( sPrefix+"vOLDINCREMENTO"), ",", "."));
            AV22Percentual = context.localUtil.CToN( cgiGet( sPrefix+"vPERCENTUAL"), ",", ".");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12GS2 */
         E12GS2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12GS2( )
      {
         /* Start Routine */
      }

      protected void E13GS2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV26i = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavI_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV26i), 4, 0)));
         AV19OldBase = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavOldbase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV19OldBase), 4, 0)));
         GX_I = 1;
         while ( GX_I <= 50 )
         {
            GX_J = 1;
            while ( GX_J <= 8 )
            {
               AV15Valores[GX_I-1][GX_J-1] = 0;
               GX_J = (int)(GX_J+1);
            }
            GX_I = (int)(GX_I+1);
         }
         AV38FatorInicial = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FatorInicial", StringUtil.LTrim( StringUtil.Str( AV38FatorInicial, 6, 2)));
         AV47sdt_variaveiscocomo.Clear();
         if ( StringUtil.StrCmp(AV32Tipo, "E") == 0 )
         {
            lblTbfator_Caption = "Fator de Escala";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbfator_Internalname, "Caption", lblTbfator_Caption);
            AV5Projeto_Fator = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
         }
         else
         {
            lblTbfator_Caption = "Fator Multiplicador";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbfator_Internalname, "Caption", lblTbfator_Caption);
            AV5Projeto_Fator = (decimal)(1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
         }
         AV39QtdLinhas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39QtdLinhas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39QtdLinhas), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV47sdt_variaveiscocomo", AV47sdt_variaveiscocomo);
      }

      private void E14GS2( )
      {
         /* Grid_Load Routine */
         AV52GXLvl22 = 0;
         /* Using cursor H00GS3 */
         pr_default.execute(1, new Object[] {AV17AreaTrabalho_Codigo, A648Projeto_Codigo, AV32Tipo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A965VariavelCocomo_ProjetoCod = H00GS3_A965VariavelCocomo_ProjetoCod[0];
            n965VariavelCocomo_ProjetoCod = H00GS3_n965VariavelCocomo_ProjetoCod[0];
            A964VariavelCocomo_Tipo = H00GS3_A964VariavelCocomo_Tipo[0];
            A961VariavelCocomo_AreaTrabalhoCod = H00GS3_A961VariavelCocomo_AreaTrabalhoCod[0];
            A963VariavelCocomo_Nome = H00GS3_A963VariavelCocomo_Nome[0];
            A962VariavelCocomo_Sigla = H00GS3_A962VariavelCocomo_Sigla[0];
            A969VariavelCocomo_Nominal = H00GS3_A969VariavelCocomo_Nominal[0];
            n969VariavelCocomo_Nominal = H00GS3_n969VariavelCocomo_Nominal[0];
            A968VariavelCocomo_Baixo = H00GS3_A968VariavelCocomo_Baixo[0];
            n968VariavelCocomo_Baixo = H00GS3_n968VariavelCocomo_Baixo[0];
            A967VariavelCocomo_MuitoBaixo = H00GS3_A967VariavelCocomo_MuitoBaixo[0];
            n967VariavelCocomo_MuitoBaixo = H00GS3_n967VariavelCocomo_MuitoBaixo[0];
            A986VariavelCocomo_ExtraBaixo = H00GS3_A986VariavelCocomo_ExtraBaixo[0];
            n986VariavelCocomo_ExtraBaixo = H00GS3_n986VariavelCocomo_ExtraBaixo[0];
            A970VariavelCocomo_Alto = H00GS3_A970VariavelCocomo_Alto[0];
            n970VariavelCocomo_Alto = H00GS3_n970VariavelCocomo_Alto[0];
            A971VariavelCocomo_MuitoAlto = H00GS3_A971VariavelCocomo_MuitoAlto[0];
            n971VariavelCocomo_MuitoAlto = H00GS3_n971VariavelCocomo_MuitoAlto[0];
            A972VariavelCocomo_ExtraAlto = H00GS3_A972VariavelCocomo_ExtraAlto[0];
            n972VariavelCocomo_ExtraAlto = H00GS3_n972VariavelCocomo_ExtraAlto[0];
            A973VariavelCocomo_Usado = H00GS3_A973VariavelCocomo_Usado[0];
            n973VariavelCocomo_Usado = H00GS3_n973VariavelCocomo_Usado[0];
            AV52GXLvl22 = 1;
            AV13Base = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
            AV19OldBase = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavOldbase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV19OldBase), 4, 0)));
            AV12Incremento = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            AV20OldIncremento = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20OldIncremento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20OldIncremento), 4, 0)));
            AV16Nome = StringUtil.StringReplace( A963VariavelCocomo_Nome, " ", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNome_Internalname, AV16Nome);
            AV42Sigla[AV26i-1] = A962VariavelCocomo_Sigla;
            AV15Valores[AV26i-1][1-1] = A969VariavelCocomo_Nominal;
            AV15Valores[AV26i-1][2-1] = A968VariavelCocomo_Baixo;
            AV15Valores[AV26i-1][3-1] = A967VariavelCocomo_MuitoBaixo;
            AV15Valores[AV26i-1][4-1] = A986VariavelCocomo_ExtraBaixo;
            AV15Valores[AV26i-1][5-1] = A970VariavelCocomo_Alto;
            AV15Valores[AV26i-1][6-1] = A971VariavelCocomo_MuitoAlto;
            AV15Valores[AV26i-1][7-1] = A972VariavelCocomo_ExtraAlto;
            AV15Valores[AV26i-1][8-1] = A973VariavelCocomo_Usado;
            AV48sdt_variavel.gxTpr_Variavelcocomo_nominal = A969VariavelCocomo_Nominal;
            AV48sdt_variavel.gxTpr_Variavelcocomo_baixo = A968VariavelCocomo_Baixo;
            AV48sdt_variavel.gxTpr_Variavelcocomo_muitobaixo = A967VariavelCocomo_MuitoBaixo;
            AV48sdt_variavel.gxTpr_Variavelcocomo_extrabaixo = A986VariavelCocomo_ExtraBaixo;
            AV48sdt_variavel.gxTpr_Variavelcocomo_alto = A970VariavelCocomo_Alto;
            AV48sdt_variavel.gxTpr_Variavelcocomo_muitoalto = A971VariavelCocomo_MuitoAlto;
            AV48sdt_variavel.gxTpr_Variavelcocomo_extraalto = A972VariavelCocomo_ExtraAlto;
            AV48sdt_variavel.gxTpr_Variavelcocomo_usado = A973VariavelCocomo_Usado;
            AV47sdt_variaveiscocomo.Add(AV48sdt_variavel, 0);
            AV48sdt_variavel = new SdtSDT_VariaveisCocomo_Item(context);
            AV21Valor = A973VariavelCocomo_Usado;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavValor_Internalname, StringUtil.LTrim( StringUtil.Str( AV21Valor, 12, 2)));
            if ( StringUtil.StrCmp(AV32Tipo, "E") == 0 )
            {
               AV5Projeto_Fator = (decimal)(AV5Projeto_Fator+A973VariavelCocomo_Usado);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
            }
            else
            {
               if ( ( A973VariavelCocomo_Usado > Convert.ToDecimal( 0 )) )
               {
                  AV5Projeto_Fator = (decimal)(AV5Projeto_Fator*A973VariavelCocomo_Usado);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
               }
            }
            if ( A969VariavelCocomo_Nominal == A973VariavelCocomo_Usado )
            {
               AV13Base = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
            }
            else if ( A969VariavelCocomo_Nominal * 1.25m == A973VariavelCocomo_Usado )
            {
               AV13Base = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 25;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A969VariavelCocomo_Nominal * 1.5m == A973VariavelCocomo_Usado )
            {
               AV13Base = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 50;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A969VariavelCocomo_Nominal * 2 == A973VariavelCocomo_Usado )
            {
               AV13Base = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 100;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A968VariavelCocomo_Baixo == A973VariavelCocomo_Usado )
            {
               AV13Base = 2;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
            }
            else if ( A968VariavelCocomo_Baixo * 1.25m == A973VariavelCocomo_Usado )
            {
               AV13Base = 2;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 25;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A968VariavelCocomo_Baixo * 1.5m == A973VariavelCocomo_Usado )
            {
               AV13Base = 2;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 50;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A968VariavelCocomo_Baixo * 2 == A973VariavelCocomo_Usado )
            {
               AV13Base = 2;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 100;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A967VariavelCocomo_MuitoBaixo == A973VariavelCocomo_Usado )
            {
               AV13Base = 3;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
            }
            else if ( A967VariavelCocomo_MuitoBaixo * 1.25m == A973VariavelCocomo_Usado )
            {
               AV13Base = 3;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 25;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A967VariavelCocomo_MuitoBaixo * 1.5m == A973VariavelCocomo_Usado )
            {
               AV13Base = 3;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 50;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A967VariavelCocomo_MuitoBaixo * 2 == A973VariavelCocomo_Usado )
            {
               AV13Base = 3;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 100;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A986VariavelCocomo_ExtraBaixo == A973VariavelCocomo_Usado )
            {
               AV13Base = 4;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
            }
            else if ( A986VariavelCocomo_ExtraBaixo * 1.25m == A973VariavelCocomo_Usado )
            {
               AV13Base = 4;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 25;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A986VariavelCocomo_ExtraBaixo * 1.5m == A973VariavelCocomo_Usado )
            {
               AV13Base = 4;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 50;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A986VariavelCocomo_ExtraBaixo * 2 == A973VariavelCocomo_Usado )
            {
               AV13Base = 4;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 100;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A970VariavelCocomo_Alto == A973VariavelCocomo_Usado )
            {
               AV13Base = 5;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
            }
            else if ( A970VariavelCocomo_Alto * 1.25m == A973VariavelCocomo_Usado )
            {
               AV13Base = 5;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 25;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A970VariavelCocomo_Alto * 1.5m == A973VariavelCocomo_Usado )
            {
               AV13Base = 5;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 50;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A970VariavelCocomo_Alto * 2 == A973VariavelCocomo_Usado )
            {
               AV13Base = 5;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 100;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A971VariavelCocomo_MuitoAlto == A973VariavelCocomo_Usado )
            {
               AV13Base = 6;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
            }
            else if ( A971VariavelCocomo_MuitoAlto * 1.25m == A973VariavelCocomo_Usado )
            {
               AV13Base = 6;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 25;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A971VariavelCocomo_MuitoAlto * 1.5m == A973VariavelCocomo_Usado )
            {
               AV13Base = 6;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 50;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A971VariavelCocomo_MuitoAlto * 2 == A973VariavelCocomo_Usado )
            {
               AV13Base = 6;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 100;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A972VariavelCocomo_ExtraAlto == A973VariavelCocomo_Usado )
            {
               AV13Base = 7;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
            }
            else if ( A972VariavelCocomo_ExtraAlto * 1.25m == A973VariavelCocomo_Usado )
            {
               AV13Base = 7;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 25;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A972VariavelCocomo_ExtraAlto * 1.5m == A973VariavelCocomo_Usado )
            {
               AV13Base = 7;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 50;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            else if ( A972VariavelCocomo_ExtraAlto * 2 == A973VariavelCocomo_Usado )
            {
               AV13Base = 7;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 100;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 5;
            }
            sendrow_52( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(5, GridRow);
            }
            AV26i = (short)(AV26i+1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavI_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV26i), 4, 0)));
            AV39QtdLinhas = (short)(AV39QtdLinhas+1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39QtdLinhas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39QtdLinhas), 4, 0)));
            AV38FatorInicial = AV5Projeto_Fator;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FatorInicial", StringUtil.LTrim( StringUtil.Str( AV38FatorInicial, 6, 2)));
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV52GXLvl22 == 0 )
         {
            /* Using cursor H00GS4 */
            pr_default.execute(2, new Object[] {AV17AreaTrabalho_Codigo, AV32Tipo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A964VariavelCocomo_Tipo = H00GS4_A964VariavelCocomo_Tipo[0];
               A965VariavelCocomo_ProjetoCod = H00GS4_A965VariavelCocomo_ProjetoCod[0];
               n965VariavelCocomo_ProjetoCod = H00GS4_n965VariavelCocomo_ProjetoCod[0];
               A961VariavelCocomo_AreaTrabalhoCod = H00GS4_A961VariavelCocomo_AreaTrabalhoCod[0];
               A963VariavelCocomo_Nome = H00GS4_A963VariavelCocomo_Nome[0];
               A962VariavelCocomo_Sigla = H00GS4_A962VariavelCocomo_Sigla[0];
               A969VariavelCocomo_Nominal = H00GS4_A969VariavelCocomo_Nominal[0];
               n969VariavelCocomo_Nominal = H00GS4_n969VariavelCocomo_Nominal[0];
               A968VariavelCocomo_Baixo = H00GS4_A968VariavelCocomo_Baixo[0];
               n968VariavelCocomo_Baixo = H00GS4_n968VariavelCocomo_Baixo[0];
               A967VariavelCocomo_MuitoBaixo = H00GS4_A967VariavelCocomo_MuitoBaixo[0];
               n967VariavelCocomo_MuitoBaixo = H00GS4_n967VariavelCocomo_MuitoBaixo[0];
               A986VariavelCocomo_ExtraBaixo = H00GS4_A986VariavelCocomo_ExtraBaixo[0];
               n986VariavelCocomo_ExtraBaixo = H00GS4_n986VariavelCocomo_ExtraBaixo[0];
               A970VariavelCocomo_Alto = H00GS4_A970VariavelCocomo_Alto[0];
               n970VariavelCocomo_Alto = H00GS4_n970VariavelCocomo_Alto[0];
               A971VariavelCocomo_MuitoAlto = H00GS4_A971VariavelCocomo_MuitoAlto[0];
               n971VariavelCocomo_MuitoAlto = H00GS4_n971VariavelCocomo_MuitoAlto[0];
               A972VariavelCocomo_ExtraAlto = H00GS4_A972VariavelCocomo_ExtraAlto[0];
               n972VariavelCocomo_ExtraAlto = H00GS4_n972VariavelCocomo_ExtraAlto[0];
               AV13Base = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
               AV12Incremento = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
               AV16Nome = StringUtil.StringReplace( A963VariavelCocomo_Nome, " ", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNome_Internalname, AV16Nome);
               AV42Sigla[AV26i-1] = A962VariavelCocomo_Sigla;
               AV15Valores[AV26i-1][1-1] = A969VariavelCocomo_Nominal;
               AV15Valores[AV26i-1][2-1] = A968VariavelCocomo_Baixo;
               AV15Valores[AV26i-1][3-1] = A967VariavelCocomo_MuitoBaixo;
               AV15Valores[AV26i-1][4-1] = A986VariavelCocomo_ExtraBaixo;
               AV15Valores[AV26i-1][5-1] = A970VariavelCocomo_Alto;
               AV15Valores[AV26i-1][6-1] = A971VariavelCocomo_MuitoAlto;
               AV15Valores[AV26i-1][7-1] = A972VariavelCocomo_ExtraAlto;
               AV15Valores[AV26i-1][8-1] = A969VariavelCocomo_Nominal;
               AV48sdt_variavel.gxTpr_Variavelcocomo_nominal = A969VariavelCocomo_Nominal;
               AV48sdt_variavel.gxTpr_Variavelcocomo_baixo = A968VariavelCocomo_Baixo;
               AV48sdt_variavel.gxTpr_Variavelcocomo_muitobaixo = A967VariavelCocomo_MuitoBaixo;
               AV48sdt_variavel.gxTpr_Variavelcocomo_extrabaixo = A986VariavelCocomo_ExtraBaixo;
               AV48sdt_variavel.gxTpr_Variavelcocomo_alto = A970VariavelCocomo_Alto;
               AV48sdt_variavel.gxTpr_Variavelcocomo_muitoalto = A971VariavelCocomo_MuitoAlto;
               AV48sdt_variavel.gxTpr_Variavelcocomo_extraalto = A972VariavelCocomo_ExtraAlto;
               AV48sdt_variavel.gxTpr_Variavelcocomo_usado = A969VariavelCocomo_Nominal;
               AV47sdt_variaveiscocomo.Add(AV48sdt_variavel, 0);
               AV48sdt_variavel = new SdtSDT_VariaveisCocomo_Item(context);
               AV21Valor = A969VariavelCocomo_Nominal;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavValor_Internalname, StringUtil.LTrim( StringUtil.Str( AV21Valor, 12, 2)));
               if ( StringUtil.StrCmp(AV32Tipo, "E") == 0 )
               {
                  AV5Projeto_Fator = (decimal)(AV5Projeto_Fator+A969VariavelCocomo_Nominal);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
               }
               else
               {
                  if ( ( A969VariavelCocomo_Nominal > Convert.ToDecimal( 0 )) )
                  {
                     AV5Projeto_Fator = (decimal)(AV5Projeto_Fator*A969VariavelCocomo_Nominal);
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
                  }
               }
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 5;
               }
               sendrow_52( ) ;
               if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(5, GridRow);
               }
               AV26i = (short)(AV26i+1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavI_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV26i), 4, 0)));
               AV39QtdLinhas = (short)(AV39QtdLinhas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39QtdLinhas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39QtdLinhas), 4, 0)));
               AV38FatorInicial = AV5Projeto_Fator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FatorInicial", StringUtil.LTrim( StringUtil.Str( AV38FatorInicial, 6, 2)));
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         AV27Fator = AV38FatorInicial;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Fator", StringUtil.LTrim( StringUtil.Str( AV27Fator, 6, 2)));
         cmbavBase.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Base), 4, 0));
         cmbavIncremento.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV48sdt_variavel", AV48sdt_variavel);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV47sdt_variaveiscocomo", AV47sdt_variaveiscocomo);
      }

      protected void E11GS2( )
      {
         /* 'Enter' Routine */
         if ( AV38FatorInicial != AV5Projeto_Fator )
         {
            /* Execute user subroutine: 'GRAVAVARIAVEIS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         context.setWebReturnParms(new Object[] {(int)A648Projeto_Codigo,(String)AV32Tipo,(decimal)AV27Fator});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'CALCULAFATOR' Routine */
         if ( StringUtil.StrCmp(AV32Tipo, "E") == 0 )
         {
            AV5Projeto_Fator = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
            AV36p = 1;
            while ( AV36p <= AV39QtdLinhas )
            {
               AV5Projeto_Fator = (decimal)(AV5Projeto_Fator+(AV15Valores[AV36p-1][8-1]));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
               AV36p = (short)(AV36p+1);
            }
         }
         else
         {
            AV5Projeto_Fator = (decimal)(1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
            AV36p = 1;
            while ( AV36p <= AV39QtdLinhas )
            {
               if ( ( AV15Valores[AV36p-1][8-1] > Convert.ToDecimal( 0 )) )
               {
                  AV5Projeto_Fator = (decimal)(AV5Projeto_Fator*AV15Valores[AV36p-1][8-1]);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Projeto_Fator", StringUtil.LTrim( StringUtil.Str( AV5Projeto_Fator, 6, 2)));
               }
               AV36p = (short)(AV36p+1);
            }
         }
         AV27Fator = AV5Projeto_Fator;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Fator", StringUtil.LTrim( StringUtil.Str( AV27Fator, 6, 2)));
      }

      protected void S122( )
      {
         /* 'GRAVAVARIAVEIS' Routine */
         AV49WebSession.Set("VariaveisCocomo", AV47sdt_variaveiscocomo.ToXml(false, true, "SDT_VariaveisCocomo", "GxEv3Up14_MeetrikaVs3"));
         new prc_updprojetofatorescala(context ).execute( ref  AV17AreaTrabalho_Codigo, ref  A648Projeto_Codigo, ref  AV32Tipo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tipo", AV32Tipo);
      }

      protected void wb_table1_2_GS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"5\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "i") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Indice") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Base") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Incremento") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Valor") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26i), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavI_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19OldBase), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavOldbase_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV16Nome));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNome_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Base), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Incremento), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV21Valor, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavValor_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 5 )
         {
            wbEnd = 0;
            nRC_GXsfl_5 = (short)(nGXsfl_5_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:40px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbfator_Internalname, lblTbfator_Caption, "", "", lblTbfator_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_FatorDeEscala.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_fator_Internalname, StringUtil.LTrim( StringUtil.NToC( AV5Projeto_Fator, 6, 2, ",", "")), ((edtavProjeto_fator_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV5Projeto_Fator, "ZZ9.99")) : context.localUtil.Format( AV5Projeto_Fator, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,15);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_fator_Jsonclick, 0, "Attribute", "", "", "", 1, edtavProjeto_fator_Enabled, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_FatorDeEscala.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirmar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(5), 1, 0)+","+"null"+");", "Confirmar", bttBtnconfirmar_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ENTER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_FatorDeEscala.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtncancel_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(5), 1, 0)+","+"null"+");", "Fechar", bttBtncancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_FatorDeEscala.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GS2e( true) ;
         }
         else
         {
            wb_table1_2_GS2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV17AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17AreaTrabalho_Codigo), 6, 0)));
         A648Projeto_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         AV32Tipo = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tipo", AV32Tipo);
         AV27Fator = (decimal)(Convert.ToDecimal((decimal)getParm(obj,3)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Fator", StringUtil.LTrim( StringUtil.Str( AV27Fator, 6, 2)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGS2( ) ;
         WSGS2( ) ;
         WEGS2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV17AreaTrabalho_Codigo = (String)((String)getParm(obj,0));
         sCtrlA648Projeto_Codigo = (String)((String)getParm(obj,1));
         sCtrlAV32Tipo = (String)((String)getParm(obj,2));
         sCtrlAV27Fator = (String)((String)getParm(obj,3));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAGS2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wp_fatordeescala");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAGS2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV17AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17AreaTrabalho_Codigo), 6, 0)));
            A648Projeto_Codigo = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            AV32Tipo = (String)getParm(obj,4);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tipo", AV32Tipo);
            AV27Fator = (decimal)(Convert.ToDecimal((decimal)getParm(obj,5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Fator", StringUtil.LTrim( StringUtil.Str( AV27Fator, 6, 2)));
         }
         wcpOAV17AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV17AreaTrabalho_Codigo"), ",", "."));
         wcpOA648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA648Projeto_Codigo"), ",", "."));
         wcpOAV32Tipo = cgiGet( sPrefix+"wcpOAV32Tipo");
         if ( ! GetJustCreated( ) && ( ( AV17AreaTrabalho_Codigo != wcpOAV17AreaTrabalho_Codigo ) || ( A648Projeto_Codigo != wcpOA648Projeto_Codigo ) || ( StringUtil.StrCmp(AV32Tipo, wcpOAV32Tipo) != 0 ) ) )
         {
            setjustcreated();
         }
         wcpOAV17AreaTrabalho_Codigo = AV17AreaTrabalho_Codigo;
         wcpOA648Projeto_Codigo = A648Projeto_Codigo;
         wcpOAV32Tipo = AV32Tipo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV17AreaTrabalho_Codigo = cgiGet( sPrefix+"AV17AreaTrabalho_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV17AreaTrabalho_Codigo) > 0 )
         {
            AV17AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV17AreaTrabalho_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17AreaTrabalho_Codigo), 6, 0)));
         }
         else
         {
            AV17AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV17AreaTrabalho_Codigo_PARM"), ",", "."));
         }
         sCtrlA648Projeto_Codigo = cgiGet( sPrefix+"A648Projeto_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA648Projeto_Codigo) > 0 )
         {
            A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA648Projeto_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         else
         {
            A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A648Projeto_Codigo_PARM"), ",", "."));
         }
         sCtrlAV32Tipo = cgiGet( sPrefix+"AV32Tipo_CTRL");
         if ( StringUtil.Len( sCtrlAV32Tipo) > 0 )
         {
            AV32Tipo = cgiGet( sCtrlAV32Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Tipo", AV32Tipo);
         }
         else
         {
            AV32Tipo = cgiGet( sPrefix+"AV32Tipo_PARM");
         }
         sCtrlAV27Fator = cgiGet( sPrefix+"AV27Fator_CTRL");
         if ( StringUtil.Len( sCtrlAV27Fator) > 0 )
         {
            AV27Fator = context.localUtil.CToN( cgiGet( sCtrlAV27Fator), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Fator", StringUtil.LTrim( StringUtil.Str( AV27Fator, 6, 2)));
         }
         else
         {
            AV27Fator = context.localUtil.CToN( cgiGet( sPrefix+"AV27Fator_PARM"), ",", ".");
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAGS2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSGS2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSGS2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV17AreaTrabalho_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17AreaTrabalho_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV17AreaTrabalho_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV17AreaTrabalho_Codigo_CTRL", StringUtil.RTrim( sCtrlAV17AreaTrabalho_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A648Projeto_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA648Projeto_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A648Projeto_Codigo_CTRL", StringUtil.RTrim( sCtrlA648Projeto_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV32Tipo_PARM", StringUtil.RTrim( AV32Tipo));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV32Tipo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV32Tipo_CTRL", StringUtil.RTrim( sCtrlAV32Tipo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV27Fator_PARM", StringUtil.LTrim( StringUtil.NToC( AV27Fator, 6, 2, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV27Fator)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV27Fator_CTRL", StringUtil.RTrim( sCtrlAV27Fator));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEGS2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020548305975");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("wp_fatordeescala.js", "?2020548305975");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_52( )
      {
         edtavI_Internalname = sPrefix+"vI_"+sGXsfl_5_idx;
         edtavOldbase_Internalname = sPrefix+"vOLDBASE_"+sGXsfl_5_idx;
         edtavNome_Internalname = sPrefix+"vNOME_"+sGXsfl_5_idx;
         cmbavBase_Internalname = sPrefix+"vBASE_"+sGXsfl_5_idx;
         cmbavIncremento_Internalname = sPrefix+"vINCREMENTO_"+sGXsfl_5_idx;
         edtavValor_Internalname = sPrefix+"vVALOR_"+sGXsfl_5_idx;
      }

      protected void SubsflControlProps_fel_52( )
      {
         edtavI_Internalname = sPrefix+"vI_"+sGXsfl_5_fel_idx;
         edtavOldbase_Internalname = sPrefix+"vOLDBASE_"+sGXsfl_5_fel_idx;
         edtavNome_Internalname = sPrefix+"vNOME_"+sGXsfl_5_fel_idx;
         cmbavBase_Internalname = sPrefix+"vBASE_"+sGXsfl_5_fel_idx;
         cmbavIncremento_Internalname = sPrefix+"vINCREMENTO_"+sGXsfl_5_fel_idx;
         edtavValor_Internalname = sPrefix+"vVALOR_"+sGXsfl_5_fel_idx;
      }

      protected void sendrow_52( )
      {
         SubsflControlProps_52( ) ;
         WBGS0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0x0);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_5_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_5_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavI_Enabled!=0)&&(edtavI_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 6,'"+sPrefix+"',false,'"+sGXsfl_5_idx+"',5)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavI_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26i), 4, 0, ",", "")),((edtavI_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV26i), "ZZZ9")) : context.localUtil.Format( (decimal)(AV26i), "ZZZ9")),TempTags+((edtavI_Enabled!=0)&&(edtavI_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavI_Enabled!=0)&&(edtavI_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,6);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavI_Jsonclick,(short)0,(String)"Attribute",(String)"color:#00FFFFFF;",(String)ROClassString,(String)"",(short)0,(int)edtavI_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavOldbase_Enabled!=0)&&(edtavOldbase_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 7,'"+sPrefix+"',false,'"+sGXsfl_5_idx+"',5)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavOldbase_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19OldBase), 4, 0, ",", "")),((edtavOldbase_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV19OldBase), "ZZZ9")) : context.localUtil.Format( (decimal)(AV19OldBase), "ZZZ9")),TempTags+((edtavOldbase_Enabled!=0)&&(edtavOldbase_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavOldbase_Enabled!=0)&&(edtavOldbase_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,7);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavOldbase_Jsonclick,(short)0,(String)"Attribute",(String)"color:#00FFFFFF;",(String)ROClassString,(String)"",(short)0,(int)edtavOldbase_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavNome_Enabled!=0)&&(edtavNome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 8,'"+sPrefix+"',false,'"+sGXsfl_5_idx+"',5)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavNome_Internalname,StringUtil.RTrim( AV16Nome),StringUtil.RTrim( context.localUtil.Format( AV16Nome, "@!")),TempTags+((edtavNome_Enabled!=0)&&(edtavNome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavNome_Enabled!=0)&&(edtavNome_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,8);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavNome_Jsonclick,(short)0,(String)"Attribute",(String)"color:#00FFFFFF;",(String)ROClassString,(String)"",(short)-1,(int)edtavNome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavBase.Enabled!=0)&&(cmbavBase.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 9,'"+sPrefix+"',false,'"+sGXsfl_5_idx+"',5)\"" : " ");
         if ( ( nGXsfl_5_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vBASE_" + sGXsfl_5_idx;
            cmbavBase.Name = GXCCtl;
            cmbavBase.WebTags = "";
            cmbavBase.addItem("1", "Nominal", 0);
            cmbavBase.addItem("2", "Baixo", 0);
            cmbavBase.addItem("3", "Muito Baixo", 0);
            cmbavBase.addItem("4", "Extra Baixo", 0);
            cmbavBase.addItem("5", "Alto", 0);
            cmbavBase.addItem("6", "Muito Alto", 0);
            cmbavBase.addItem("7", "Extra Alto", 0);
            if ( cmbavBase.ItemCount > 0 )
            {
               AV13Base = (short)(NumberUtil.Val( cmbavBase.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13Base), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavBase_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Base), 4, 0)));
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavBase,(String)cmbavBase_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV13Base), 4, 0)),(short)1,(String)cmbavBase_Jsonclick,(short)7,(String)"'"+sPrefix+"'"+",false,"+"'"+"e15gs2_client"+"'",(String)"int",(String)"",(short)-1,(short)1,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"color:#00FFFFFF;",(String)"Attribute",(String)"",TempTags+((cmbavBase.Enabled!=0)&&(cmbavBase.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavBase.Enabled!=0)&&(cmbavBase.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,9);\"" : " "),(String)"",(bool)true});
         cmbavBase.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Base), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavBase_Internalname, "Values", (String)(cmbavBase.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavIncremento.Enabled!=0)&&(cmbavIncremento.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 10,'"+sPrefix+"',false,'"+sGXsfl_5_idx+"',5)\"" : " ");
         if ( ( nGXsfl_5_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vINCREMENTO_" + sGXsfl_5_idx;
            cmbavIncremento.Name = GXCCtl;
            cmbavIncremento.WebTags = "";
            cmbavIncremento.addItem("0", "0%", 0);
            cmbavIncremento.addItem("25", "25%", 0);
            cmbavIncremento.addItem("50", "50%", 0);
            cmbavIncremento.addItem("100", "100%", 0);
            if ( cmbavIncremento.ItemCount > 0 )
            {
               AV12Incremento = (short)(NumberUtil.Val( cmbavIncremento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavIncremento_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)));
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavIncremento,(String)cmbavIncremento_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0)),(short)1,(String)cmbavIncremento_Jsonclick,(short)7,(String)"'"+sPrefix+"'"+",false,"+"'"+"e16gs2_client"+"'",(String)"int",(String)"",(short)-1,(short)1,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"color:#00FFFFFF;",(String)"Attribute",(String)"",TempTags+((cmbavIncremento.Enabled!=0)&&(cmbavIncremento.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavIncremento.Enabled!=0)&&(cmbavIncremento.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,10);\"" : " "),(String)"",(bool)true});
         cmbavIncremento.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12Incremento), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavIncremento_Internalname, "Values", (String)(cmbavIncremento.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavValor_Enabled!=0)&&(edtavValor_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 11,'"+sPrefix+"',false,'"+sGXsfl_5_idx+"',5)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavValor_Internalname,StringUtil.LTrim( StringUtil.NToC( AV21Valor, 18, 2, ",", "")),((edtavValor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV21Valor, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV21Valor, "ZZ,ZZZ,ZZZ,ZZ9.999")),TempTags+((edtavValor_Enabled!=0)&&(edtavValor_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavValor_Enabled!=0)&&(edtavValor_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,11);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavValor_Jsonclick,(short)0,(String)"Attribute",(String)"color:#00FFFFFF;",(String)ROClassString,(String)"",(short)-1,(int)edtavValor_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
         GridContainer.AddRow(GridRow);
         nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         /* End function sendrow_52 */
      }

      protected void init_default_properties( )
      {
         edtavI_Internalname = sPrefix+"vI";
         edtavOldbase_Internalname = sPrefix+"vOLDBASE";
         edtavNome_Internalname = sPrefix+"vNOME";
         cmbavBase_Internalname = sPrefix+"vBASE";
         cmbavIncremento_Internalname = sPrefix+"vINCREMENTO";
         edtavValor_Internalname = sPrefix+"vVALOR";
         lblTbfator_Internalname = sPrefix+"TBFATOR";
         edtavProjeto_fator_Internalname = sPrefix+"vPROJETO_FATOR";
         bttBtnconfirmar_Internalname = sPrefix+"BTNCONFIRMAR";
         bttBtncancel_Internalname = sPrefix+"BTNCANCEL";
         tblTable1_Internalname = sPrefix+"TABLE1";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavValor_Jsonclick = "";
         edtavValor_Visible = -1;
         cmbavIncremento_Jsonclick = "";
         cmbavIncremento.Visible = -1;
         cmbavIncremento.Enabled = 1;
         cmbavBase_Jsonclick = "";
         cmbavBase.Visible = -1;
         cmbavBase.Enabled = 1;
         edtavNome_Jsonclick = "";
         edtavNome_Visible = -1;
         edtavOldbase_Jsonclick = "";
         edtavOldbase_Visible = 0;
         edtavI_Jsonclick = "";
         edtavI_Visible = 0;
         edtavProjeto_fator_Jsonclick = "";
         edtavProjeto_fator_Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavValor_Enabled = 1;
         edtavNome_Enabled = 1;
         edtavOldbase_Enabled = 1;
         edtavI_Enabled = 1;
         subGrid_Class = "Grid";
         lblTbfator_Caption = "Fator de Escala:";
         subGrid_Sortable = 0;
         subGrid_Titleforecolor = (int)(0x000000);
         subGrid_Backcolorstyle = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A965VariavelCocomo_ProjetoCod',fld:'VARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',nv:''},{av:'A963VariavelCocomo_Nome',fld:'VARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'A969VariavelCocomo_Nominal',fld:'VARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A968VariavelCocomo_Baixo',fld:'VARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A967VariavelCocomo_MuitoBaixo',fld:'VARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A986VariavelCocomo_ExtraBaixo',fld:'VARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A970VariavelCocomo_Alto',fld:'VARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A971VariavelCocomo_MuitoAlto',fld:'VARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A972VariavelCocomo_ExtraAlto',fld:'VARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A973VariavelCocomo_Usado',fld:'VARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48sdt_variavel',fld:'vSDT_VARIAVEL',pic:'',nv:null},{av:'AV47sdt_variaveiscocomo',fld:'vSDT_VARIAVEISCOCOMO',pic:'',nv:null},{av:'AV5Projeto_Fator',fld:'vPROJETO_FATOR',pic:'ZZ9.99',nv:0.0},{av:'AV26i',fld:'vI',pic:'ZZZ9',nv:0},{av:'AV39QtdLinhas',fld:'vQTDLINHAS',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV32Tipo',fld:'vTIPO',pic:'',nv:''}],oparms:[{av:'AV26i',fld:'vI',pic:'ZZZ9',nv:0},{av:'AV19OldBase',fld:'vOLDBASE',pic:'ZZZ9',nv:0},{av:'AV15Valores',fld:'vVALORES',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FatorInicial',fld:'vFATORINICIAL',pic:'ZZ9.99',nv:0.0},{av:'AV47sdt_variaveiscocomo',fld:'vSDT_VARIAVEISCOCOMO',pic:'',nv:null},{av:'lblTbfator_Caption',ctrl:'TBFATOR',prop:'Caption'},{av:'AV5Projeto_Fator',fld:'vPROJETO_FATOR',pic:'ZZ9.99',nv:0.0},{av:'AV39QtdLinhas',fld:'vQTDLINHAS',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E14GS2',iparms:[{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A965VariavelCocomo_ProjetoCod',fld:'VARIAVELCOCOMO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',nv:''},{av:'AV32Tipo',fld:'vTIPO',pic:'',nv:''},{av:'A963VariavelCocomo_Nome',fld:'VARIAVELCOCOMO_NOME',pic:'@!',nv:''},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'A969VariavelCocomo_Nominal',fld:'VARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A968VariavelCocomo_Baixo',fld:'VARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A967VariavelCocomo_MuitoBaixo',fld:'VARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A986VariavelCocomo_ExtraBaixo',fld:'VARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A970VariavelCocomo_Alto',fld:'VARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A971VariavelCocomo_MuitoAlto',fld:'VARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A972VariavelCocomo_ExtraAlto',fld:'VARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A973VariavelCocomo_Usado',fld:'VARIAVELCOCOMO_USADO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48sdt_variavel',fld:'vSDT_VARIAVEL',pic:'',nv:null},{av:'AV47sdt_variaveiscocomo',fld:'vSDT_VARIAVEISCOCOMO',pic:'',nv:null},{av:'AV5Projeto_Fator',fld:'vPROJETO_FATOR',pic:'ZZ9.99',nv:0.0},{av:'AV26i',fld:'vI',pic:'ZZZ9',nv:0},{av:'AV39QtdLinhas',fld:'vQTDLINHAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV13Base',fld:'vBASE',pic:'ZZZ9',nv:0},{av:'AV19OldBase',fld:'vOLDBASE',pic:'ZZZ9',nv:0},{av:'AV12Incremento',fld:'vINCREMENTO',pic:'ZZZ9',nv:0},{av:'AV20OldIncremento',fld:'vOLDINCREMENTO',pic:'ZZZ9',nv:0},{av:'AV16Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV15Valores',fld:'vVALORES',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48sdt_variavel',fld:'vSDT_VARIAVEL',pic:'',nv:null},{av:'AV47sdt_variaveiscocomo',fld:'vSDT_VARIAVEISCOCOMO',pic:'',nv:null},{av:'AV21Valor',fld:'vVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV5Projeto_Fator',fld:'vPROJETO_FATOR',pic:'ZZ9.99',nv:0.0},{av:'AV26i',fld:'vI',pic:'ZZZ9',nv:0},{av:'AV39QtdLinhas',fld:'vQTDLINHAS',pic:'ZZZ9',nv:0},{av:'AV38FatorInicial',fld:'vFATORINICIAL',pic:'ZZ9.99',nv:0.0},{av:'AV27Fator',fld:'vFATOR',pic:'ZZ9.99',nv:0.0}]}");
         setEventMetadata("VBASE.CLICK","{handler:'E15GS2',iparms:[{av:'AV15Valores',fld:'vVALORES',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26i',fld:'vI',pic:'ZZZ9',nv:0},{av:'AV13Base',fld:'vBASE',pic:'ZZZ9',nv:0},{av:'AV47sdt_variaveiscocomo',fld:'vSDT_VARIAVEISCOCOMO',pic:'',nv:null},{av:'AV32Tipo',fld:'vTIPO',pic:'',nv:''},{av:'AV39QtdLinhas',fld:'vQTDLINHAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV21Valor',fld:'vVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV15Valores',fld:'vVALORES',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48sdt_variavel',fld:'vSDT_VARIAVEL',pic:'',nv:null},{av:'AV5Projeto_Fator',fld:'vPROJETO_FATOR',pic:'ZZ9.99',nv:0.0},{av:'AV27Fator',fld:'vFATOR',pic:'ZZ9.99',nv:0.0}]}");
         setEventMetadata("VINCREMENTO.CLICK","{handler:'E16GS2',iparms:[{av:'AV12Incremento',fld:'vINCREMENTO',pic:'ZZZ9',nv:0},{av:'AV15Valores',fld:'vVALORES',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26i',fld:'vI',pic:'ZZZ9',nv:0},{av:'AV13Base',fld:'vBASE',pic:'ZZZ9',nv:0},{av:'AV47sdt_variaveiscocomo',fld:'vSDT_VARIAVEISCOCOMO',pic:'',nv:null},{av:'AV20OldIncremento',fld:'vOLDINCREMENTO',pic:'ZZZ9',nv:0},{av:'AV32Tipo',fld:'vTIPO',pic:'',nv:''},{av:'AV39QtdLinhas',fld:'vQTDLINHAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV21Valor',fld:'vVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV15Valores',fld:'vVALORES',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV48sdt_variavel',fld:'vSDT_VARIAVEL',pic:'',nv:null},{av:'AV5Projeto_Fator',fld:'vPROJETO_FATOR',pic:'ZZ9.99',nv:0.0},{av:'AV27Fator',fld:'vFATOR',pic:'ZZ9.99',nv:0.0}]}");
         setEventMetadata("'ENTER'","{handler:'E11GS2',iparms:[{av:'AV38FatorInicial',fld:'vFATORINICIAL',pic:'ZZ9.99',nv:0.0},{av:'AV5Projeto_Fator',fld:'vPROJETO_FATOR',pic:'ZZ9.99',nv:0.0},{av:'AV27Fator',fld:'vFATOR',pic:'ZZ9.99',nv:0.0},{av:'AV32Tipo',fld:'vTIPO',pic:'',nv:''},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV47sdt_variaveiscocomo',fld:'vSDT_VARIAVEISCOCOMO',pic:'',nv:null},{av:'AV17AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV32Tipo',fld:'vTIPO',pic:'',nv:''},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV17AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV32Tipo = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         A964VariavelCocomo_Tipo = "";
         A963VariavelCocomo_Nome = "";
         A962VariavelCocomo_Sigla = "";
         AV48sdt_variavel = new SdtSDT_VariaveisCocomo_Item(context);
         AV47sdt_variaveiscocomo = new GxObjectCollection( context, "SDT_VariaveisCocomo.Item", "GxEv3Up14_MeetrikaVs3", "SdtSDT_VariaveisCocomo_Item", "GeneXus.Programs");
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV15Valores = new decimal [50][] ;
         for( GX_I=0;GX_I<50;GX_I++) AV15Valores[GX_I] = new decimal[8];
         GX_I = 1;
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV16Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00GS2_A648Projeto_Codigo = new int[1] ;
         H00GS3_A992VariavelCocomo_Sequencial = new short[1] ;
         H00GS3_A965VariavelCocomo_ProjetoCod = new int[1] ;
         H00GS3_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         H00GS3_A964VariavelCocomo_Tipo = new String[] {""} ;
         H00GS3_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         H00GS3_A963VariavelCocomo_Nome = new String[] {""} ;
         H00GS3_A962VariavelCocomo_Sigla = new String[] {""} ;
         H00GS3_A969VariavelCocomo_Nominal = new decimal[1] ;
         H00GS3_n969VariavelCocomo_Nominal = new bool[] {false} ;
         H00GS3_A968VariavelCocomo_Baixo = new decimal[1] ;
         H00GS3_n968VariavelCocomo_Baixo = new bool[] {false} ;
         H00GS3_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         H00GS3_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         H00GS3_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         H00GS3_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         H00GS3_A970VariavelCocomo_Alto = new decimal[1] ;
         H00GS3_n970VariavelCocomo_Alto = new bool[] {false} ;
         H00GS3_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         H00GS3_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         H00GS3_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         H00GS3_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         H00GS3_A973VariavelCocomo_Usado = new decimal[1] ;
         H00GS3_n973VariavelCocomo_Usado = new bool[] {false} ;
         AV42Sigla = new String [50] ;
         GX_I = 1;
         while ( GX_I <= 50 )
         {
            AV42Sigla[GX_I-1] = "";
            GX_I = (int)(GX_I+1);
         }
         GridRow = new GXWebRow();
         H00GS4_A992VariavelCocomo_Sequencial = new short[1] ;
         H00GS4_A964VariavelCocomo_Tipo = new String[] {""} ;
         H00GS4_A965VariavelCocomo_ProjetoCod = new int[1] ;
         H00GS4_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         H00GS4_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         H00GS4_A963VariavelCocomo_Nome = new String[] {""} ;
         H00GS4_A962VariavelCocomo_Sigla = new String[] {""} ;
         H00GS4_A969VariavelCocomo_Nominal = new decimal[1] ;
         H00GS4_n969VariavelCocomo_Nominal = new bool[] {false} ;
         H00GS4_A968VariavelCocomo_Baixo = new decimal[1] ;
         H00GS4_n968VariavelCocomo_Baixo = new bool[] {false} ;
         H00GS4_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         H00GS4_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         H00GS4_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         H00GS4_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         H00GS4_A970VariavelCocomo_Alto = new decimal[1] ;
         H00GS4_n970VariavelCocomo_Alto = new bool[] {false} ;
         H00GS4_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         H00GS4_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         H00GS4_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         H00GS4_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         AV49WebSession = context.GetSession();
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTbfator_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnconfirmar_Jsonclick = "";
         bttBtncancel_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV17AreaTrabalho_Codigo = "";
         sCtrlA648Projeto_Codigo = "";
         sCtrlAV32Tipo = "";
         sCtrlAV27Fator = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_fatordeescala__default(),
            new Object[][] {
                new Object[] {
               H00GS2_A648Projeto_Codigo
               }
               , new Object[] {
               H00GS3_A992VariavelCocomo_Sequencial, H00GS3_A965VariavelCocomo_ProjetoCod, H00GS3_n965VariavelCocomo_ProjetoCod, H00GS3_A964VariavelCocomo_Tipo, H00GS3_A961VariavelCocomo_AreaTrabalhoCod, H00GS3_A963VariavelCocomo_Nome, H00GS3_A962VariavelCocomo_Sigla, H00GS3_A969VariavelCocomo_Nominal, H00GS3_n969VariavelCocomo_Nominal, H00GS3_A968VariavelCocomo_Baixo,
               H00GS3_n968VariavelCocomo_Baixo, H00GS3_A967VariavelCocomo_MuitoBaixo, H00GS3_n967VariavelCocomo_MuitoBaixo, H00GS3_A986VariavelCocomo_ExtraBaixo, H00GS3_n986VariavelCocomo_ExtraBaixo, H00GS3_A970VariavelCocomo_Alto, H00GS3_n970VariavelCocomo_Alto, H00GS3_A971VariavelCocomo_MuitoAlto, H00GS3_n971VariavelCocomo_MuitoAlto, H00GS3_A972VariavelCocomo_ExtraAlto,
               H00GS3_n972VariavelCocomo_ExtraAlto, H00GS3_A973VariavelCocomo_Usado, H00GS3_n973VariavelCocomo_Usado
               }
               , new Object[] {
               H00GS4_A992VariavelCocomo_Sequencial, H00GS4_A964VariavelCocomo_Tipo, H00GS4_A965VariavelCocomo_ProjetoCod, H00GS4_n965VariavelCocomo_ProjetoCod, H00GS4_A961VariavelCocomo_AreaTrabalhoCod, H00GS4_A963VariavelCocomo_Nome, H00GS4_A962VariavelCocomo_Sigla, H00GS4_A969VariavelCocomo_Nominal, H00GS4_n969VariavelCocomo_Nominal, H00GS4_A968VariavelCocomo_Baixo,
               H00GS4_n968VariavelCocomo_Baixo, H00GS4_A967VariavelCocomo_MuitoBaixo, H00GS4_n967VariavelCocomo_MuitoBaixo, H00GS4_A986VariavelCocomo_ExtraBaixo, H00GS4_n986VariavelCocomo_ExtraBaixo, H00GS4_A970VariavelCocomo_Alto, H00GS4_n970VariavelCocomo_Alto, H00GS4_A971VariavelCocomo_MuitoAlto, H00GS4_n971VariavelCocomo_MuitoAlto, H00GS4_A972VariavelCocomo_ExtraAlto,
               H00GS4_n972VariavelCocomo_ExtraAlto
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavI_Enabled = 0;
         edtavOldbase_Enabled = 0;
         edtavNome_Enabled = 0;
         edtavValor_Enabled = 0;
         edtavProjeto_fator_Enabled = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_5 ;
      private short nGXsfl_5_idx=1 ;
      private short AV26i ;
      private short AV39QtdLinhas ;
      private short initialized ;
      private short nGXWrapped ;
      private short AV20OldIncremento ;
      private short AV36p ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short AV19OldBase ;
      private short AV13Base ;
      private short AV12Incremento ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_5_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short GRID_nEOF ;
      private short AV52GXLvl22 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int AV17AreaTrabalho_Codigo ;
      private int A648Projeto_Codigo ;
      private int wcpOAV17AreaTrabalho_Codigo ;
      private int wcpOA648Projeto_Codigo ;
      private int A961VariavelCocomo_AreaTrabalhoCod ;
      private int A965VariavelCocomo_ProjetoCod ;
      private int edtavI_Enabled ;
      private int edtavOldbase_Enabled ;
      private int edtavNome_Enabled ;
      private int edtavValor_Enabled ;
      private int edtavProjeto_fator_Enabled ;
      private int subGrid_Islastpage ;
      private int subGrid_Titleforecolor ;
      private int GX_I ;
      private int GX_J ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavI_Visible ;
      private int edtavOldbase_Visible ;
      private int edtavNome_Visible ;
      private int edtavValor_Visible ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private decimal AV27Fator ;
      private decimal A969VariavelCocomo_Nominal ;
      private decimal A968VariavelCocomo_Baixo ;
      private decimal A967VariavelCocomo_MuitoBaixo ;
      private decimal A986VariavelCocomo_ExtraBaixo ;
      private decimal A970VariavelCocomo_Alto ;
      private decimal A971VariavelCocomo_MuitoAlto ;
      private decimal A972VariavelCocomo_ExtraAlto ;
      private decimal A973VariavelCocomo_Usado ;
      private decimal AV5Projeto_Fator ;
      private decimal [][] AV15Valores ;
      private decimal AV38FatorInicial ;
      private decimal AV22Percentual ;
      private decimal AV21Valor ;
      private String AV32Tipo ;
      private String wcpOAV32Tipo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_5_idx="0001" ;
      private String A964VariavelCocomo_Tipo ;
      private String A963VariavelCocomo_Nome ;
      private String A962VariavelCocomo_Sigla ;
      private String edtavI_Internalname ;
      private String GXKey ;
      private String edtavOldbase_Internalname ;
      private String edtavNome_Internalname ;
      private String edtavValor_Internalname ;
      private String edtavProjeto_fator_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV16Nome ;
      private String cmbavBase_Internalname ;
      private String cmbavIncremento_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lblTbfator_Caption ;
      private String lblTbfator_Internalname ;
      private String [] AV42Sigla ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String lblTbfator_Jsonclick ;
      private String TempTags ;
      private String edtavProjeto_fator_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnconfirmar_Internalname ;
      private String bttBtnconfirmar_Jsonclick ;
      private String bttBtncancel_Internalname ;
      private String bttBtncancel_Jsonclick ;
      private String sCtrlAV17AreaTrabalho_Codigo ;
      private String sCtrlA648Projeto_Codigo ;
      private String sCtrlAV32Tipo ;
      private String sCtrlAV27Fator ;
      private String sGXsfl_5_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavI_Jsonclick ;
      private String edtavOldbase_Jsonclick ;
      private String edtavNome_Jsonclick ;
      private String cmbavBase_Jsonclick ;
      private String cmbavIncremento_Jsonclick ;
      private String edtavValor_Jsonclick ;
      private bool entryPointCalled ;
      private bool n965VariavelCocomo_ProjetoCod ;
      private bool n969VariavelCocomo_Nominal ;
      private bool n968VariavelCocomo_Baixo ;
      private bool n967VariavelCocomo_MuitoBaixo ;
      private bool n986VariavelCocomo_ExtraBaixo ;
      private bool n970VariavelCocomo_Alto ;
      private bool n971VariavelCocomo_MuitoAlto ;
      private bool n972VariavelCocomo_ExtraAlto ;
      private bool n973VariavelCocomo_Usado ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP1_Projeto_Codigo ;
      private String aP2_Tipo ;
      private GXCombobox cmbavBase ;
      private GXCombobox cmbavIncremento ;
      private IDataStoreProvider pr_default ;
      private int[] H00GS2_A648Projeto_Codigo ;
      private short[] H00GS3_A992VariavelCocomo_Sequencial ;
      private int[] H00GS3_A965VariavelCocomo_ProjetoCod ;
      private bool[] H00GS3_n965VariavelCocomo_ProjetoCod ;
      private String[] H00GS3_A964VariavelCocomo_Tipo ;
      private int[] H00GS3_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] H00GS3_A963VariavelCocomo_Nome ;
      private String[] H00GS3_A962VariavelCocomo_Sigla ;
      private decimal[] H00GS3_A969VariavelCocomo_Nominal ;
      private bool[] H00GS3_n969VariavelCocomo_Nominal ;
      private decimal[] H00GS3_A968VariavelCocomo_Baixo ;
      private bool[] H00GS3_n968VariavelCocomo_Baixo ;
      private decimal[] H00GS3_A967VariavelCocomo_MuitoBaixo ;
      private bool[] H00GS3_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] H00GS3_A986VariavelCocomo_ExtraBaixo ;
      private bool[] H00GS3_n986VariavelCocomo_ExtraBaixo ;
      private decimal[] H00GS3_A970VariavelCocomo_Alto ;
      private bool[] H00GS3_n970VariavelCocomo_Alto ;
      private decimal[] H00GS3_A971VariavelCocomo_MuitoAlto ;
      private bool[] H00GS3_n971VariavelCocomo_MuitoAlto ;
      private decimal[] H00GS3_A972VariavelCocomo_ExtraAlto ;
      private bool[] H00GS3_n972VariavelCocomo_ExtraAlto ;
      private decimal[] H00GS3_A973VariavelCocomo_Usado ;
      private bool[] H00GS3_n973VariavelCocomo_Usado ;
      private short[] H00GS4_A992VariavelCocomo_Sequencial ;
      private String[] H00GS4_A964VariavelCocomo_Tipo ;
      private int[] H00GS4_A965VariavelCocomo_ProjetoCod ;
      private bool[] H00GS4_n965VariavelCocomo_ProjetoCod ;
      private int[] H00GS4_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] H00GS4_A963VariavelCocomo_Nome ;
      private String[] H00GS4_A962VariavelCocomo_Sigla ;
      private decimal[] H00GS4_A969VariavelCocomo_Nominal ;
      private bool[] H00GS4_n969VariavelCocomo_Nominal ;
      private decimal[] H00GS4_A968VariavelCocomo_Baixo ;
      private bool[] H00GS4_n968VariavelCocomo_Baixo ;
      private decimal[] H00GS4_A967VariavelCocomo_MuitoBaixo ;
      private bool[] H00GS4_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] H00GS4_A986VariavelCocomo_ExtraBaixo ;
      private bool[] H00GS4_n986VariavelCocomo_ExtraBaixo ;
      private decimal[] H00GS4_A970VariavelCocomo_Alto ;
      private bool[] H00GS4_n970VariavelCocomo_Alto ;
      private decimal[] H00GS4_A971VariavelCocomo_MuitoAlto ;
      private bool[] H00GS4_n971VariavelCocomo_MuitoAlto ;
      private decimal[] H00GS4_A972VariavelCocomo_ExtraAlto ;
      private bool[] H00GS4_n972VariavelCocomo_ExtraAlto ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private decimal aP3_Fator ;
      private IGxSession AV49WebSession ;
      [ObjectCollection(ItemType=typeof( SdtSDT_VariaveisCocomo_Item ))]
      private IGxCollection AV47sdt_variaveiscocomo ;
      private SdtSDT_VariaveisCocomo_Item AV48sdt_variavel ;
   }

   public class wp_fatordeescala__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GS2 ;
          prmH00GS2 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00GS3 ;
          prmH00GS3 = new Object[] {
          new Object[] {"@AV17AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32Tipo",SqlDbType.Char,1,0}
          } ;
          Object[] prmH00GS4 ;
          prmH00GS4 = new Object[] {
          new Object[] {"@AV17AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32Tipo",SqlDbType.Char,1,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GS2", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ORDER BY [Projeto_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GS2,1,0,true,true )
             ,new CursorDef("H00GS3", "SELECT [VariavelCocomo_Sequencial], [VariavelCocomo_ProjetoCod], [VariavelCocomo_Tipo], [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Nome], [VariavelCocomo_Sigla], [VariavelCocomo_Nominal], [VariavelCocomo_Baixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_Alto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_ExtraAlto], [VariavelCocomo_Usado] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE ([VariavelCocomo_AreaTrabalhoCod] = @AV17AreaTrabalho_Codigo) AND ([VariavelCocomo_ProjetoCod] = @Projeto_Codigo) AND ([VariavelCocomo_Tipo] = @AV32Tipo) ORDER BY [VariavelCocomo_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GS3,100,0,false,false )
             ,new CursorDef("H00GS4", "SELECT [VariavelCocomo_Sequencial], [VariavelCocomo_Tipo], [VariavelCocomo_ProjetoCod], [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Nome], [VariavelCocomo_Sigla], [VariavelCocomo_Nominal], [VariavelCocomo_Baixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_Alto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_ExtraAlto] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE ([VariavelCocomo_AreaTrabalhoCod] = @AV17AreaTrabalho_Codigo) AND ([VariavelCocomo_ProjetoCod] IS NULL) AND ([VariavelCocomo_Tipo] = @AV32Tipo) ORDER BY [VariavelCocomo_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GS4,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 15) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                return;
             case 2 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 15) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
       }
    }

 }

}
