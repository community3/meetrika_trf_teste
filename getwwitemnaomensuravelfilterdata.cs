/*
               File: GetWWItemNaoMensuravelFilterData
        Description: Get WWItem Nao Mensuravel Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:52:22.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwitemnaomensuravelfilterdata : GXProcedure
   {
      public getwwitemnaomensuravelfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwitemnaomensuravelfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
         return AV32OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwitemnaomensuravelfilterdata objgetwwitemnaomensuravelfilterdata;
         objgetwwitemnaomensuravelfilterdata = new getwwitemnaomensuravelfilterdata();
         objgetwwitemnaomensuravelfilterdata.AV23DDOName = aP0_DDOName;
         objgetwwitemnaomensuravelfilterdata.AV21SearchTxt = aP1_SearchTxt;
         objgetwwitemnaomensuravelfilterdata.AV22SearchTxtTo = aP2_SearchTxtTo;
         objgetwwitemnaomensuravelfilterdata.AV27OptionsJson = "" ;
         objgetwwitemnaomensuravelfilterdata.AV30OptionsDescJson = "" ;
         objgetwwitemnaomensuravelfilterdata.AV32OptionIndexesJson = "" ;
         objgetwwitemnaomensuravelfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwitemnaomensuravelfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwitemnaomensuravelfilterdata);
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwitemnaomensuravelfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV26Options = (IGxCollection)(new GxSimpleCollection());
         AV29OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV31OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_ITEMNAOMENSURAVEL_CODIGO") == 0 )
         {
            /* Execute user subroutine: 'LOADITEMNAOMENSURAVEL_CODIGOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADITEMNAOMENSURAVEL_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_REFERENCIAINM_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADREFERENCIAINM_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV27OptionsJson = AV26Options.ToJSonString(false);
         AV30OptionsDescJson = AV29OptionsDesc.ToJSonString(false);
         AV32OptionIndexesJson = AV31OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get("WWItemNaoMensuravelGridState"), "") == 0 )
         {
            AV36GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWItemNaoMensuravelGridState"), "");
         }
         else
         {
            AV36GridState.FromXml(AV34Session.Get("WWItemNaoMensuravelGridState"), "");
         }
         AV55GXV1 = 1;
         while ( AV55GXV1 <= AV36GridState.gxTpr_Filtervalues.Count )
         {
            AV37GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV36GridState.gxTpr_Filtervalues.Item(AV55GXV1));
            if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_CODIGO") == 0 )
            {
               AV10TFItemNaoMensuravel_Codigo = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_CODIGO_SEL") == 0 )
            {
               AV11TFItemNaoMensuravel_Codigo_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_DESCRICAO") == 0 )
            {
               AV12TFItemNaoMensuravel_Descricao = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_DESCRICAO_SEL") == 0 )
            {
               AV13TFItemNaoMensuravel_Descricao_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_VALOR") == 0 )
            {
               AV14TFItemNaoMensuravel_Valor = NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, ".");
               AV15TFItemNaoMensuravel_Valor_To = NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_DESCRICAO") == 0 )
            {
               AV16TFReferenciaINM_Descricao = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_DESCRICAO_SEL") == 0 )
            {
               AV17TFReferenciaINM_Descricao_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_TIPO_SEL") == 0 )
            {
               AV18TFItemNaoMensuravel_Tipo_SelsJson = AV37GridStateFilterValue.gxTpr_Value;
               AV19TFItemNaoMensuravel_Tipo_Sels.FromJSonString(AV18TFItemNaoMensuravel_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_ATIVO_SEL") == 0 )
            {
               AV20TFItemNaoMensuravel_Ativo_Sel = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            AV55GXV1 = (int)(AV55GXV1+1);
         }
         if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(1));
            AV39DynamicFiltersSelector1 = AV38GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
            {
               AV40DynamicFiltersOperator1 = AV38GridStateDynamicFilter.gxTpr_Operator;
               AV41ItemNaoMensuravel_AreaTrabalhoDes1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 )
            {
               AV40DynamicFiltersOperator1 = AV38GridStateDynamicFilter.gxTpr_Operator;
               AV42ReferenciaINM_Descricao1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV43DynamicFiltersEnabled2 = true;
               AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(2));
               AV44DynamicFiltersSelector2 = AV38GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
               {
                  AV45DynamicFiltersOperator2 = AV38GridStateDynamicFilter.gxTpr_Operator;
                  AV46ItemNaoMensuravel_AreaTrabalhoDes2 = AV38GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 )
               {
                  AV45DynamicFiltersOperator2 = AV38GridStateDynamicFilter.gxTpr_Operator;
                  AV47ReferenciaINM_Descricao2 = AV38GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV48DynamicFiltersEnabled3 = true;
                  AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(3));
                  AV49DynamicFiltersSelector3 = AV38GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV49DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
                  {
                     AV50DynamicFiltersOperator3 = AV38GridStateDynamicFilter.gxTpr_Operator;
                     AV51ItemNaoMensuravel_AreaTrabalhoDes3 = AV38GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 )
                  {
                     AV50DynamicFiltersOperator3 = AV38GridStateDynamicFilter.gxTpr_Operator;
                     AV52ReferenciaINM_Descricao3 = AV38GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADITEMNAOMENSURAVEL_CODIGOOPTIONS' Routine */
         AV10TFItemNaoMensuravel_Codigo = AV21SearchTxt;
         AV11TFItemNaoMensuravel_Codigo_Sel = "";
         AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = AV39DynamicFiltersSelector1;
         AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 = AV40DynamicFiltersOperator1;
         AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = AV41ItemNaoMensuravel_AreaTrabalhoDes1;
         AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = AV42ReferenciaINM_Descricao1;
         AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = AV46ItemNaoMensuravel_AreaTrabalhoDes2;
         AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = AV47ReferenciaINM_Descricao2;
         AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 = AV48DynamicFiltersEnabled3;
         AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = AV49DynamicFiltersSelector3;
         AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 = AV50DynamicFiltersOperator3;
         AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = AV51ItemNaoMensuravel_AreaTrabalhoDes3;
         AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = AV52ReferenciaINM_Descricao3;
         AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = AV10TFItemNaoMensuravel_Codigo;
         AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = AV11TFItemNaoMensuravel_Codigo_Sel;
         AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = AV12TFItemNaoMensuravel_Descricao;
         AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = AV13TFItemNaoMensuravel_Descricao_Sel;
         AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor = AV14TFItemNaoMensuravel_Valor;
         AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to = AV15TFItemNaoMensuravel_Valor_To;
         AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = AV16TFReferenciaINM_Descricao;
         AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = AV17TFReferenciaINM_Descricao_Sel;
         AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = AV19TFItemNaoMensuravel_Tipo_Sels;
         AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel = AV20TFItemNaoMensuravel_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ,
                                              AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ,
                                              AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ,
                                              AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ,
                                              AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ,
                                              AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ,
                                              AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ,
                                              AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ,
                                              AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ,
                                              AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ,
                                              AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ,
                                              AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ,
                                              AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ,
                                              AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ,
                                              AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ,
                                              AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ,
                                              AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ,
                                              AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ,
                                              AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ,
                                              AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ,
                                              AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ,
                                              AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ,
                                              AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels.Count ,
                                              AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ,
                                              A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                              A710ReferenciaINM_Descricao ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A716ItemNaoMensuravel_Ativo ,
                                              AV81Itemnaomensuravel_areatrabalhocod ,
                                              A718ItemNaoMensuravel_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1), "%", "");
         lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1), "%", "");
         lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1), "%", "");
         lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1), "%", "");
         lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2), "%", "");
         lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2), "%", "");
         lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2), "%", "");
         lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2), "%", "");
         lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3), "%", "");
         lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3), "%", "");
         lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3), "%", "");
         lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3), "%", "");
         lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = StringUtil.PadR( StringUtil.RTrim( AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo), 20, "%");
         lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = StringUtil.Concat( StringUtil.RTrim( AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao), "%", "");
         lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = StringUtil.Concat( StringUtil.RTrim( AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao), "%", "");
         /* Using cursor P00OF2 */
         pr_default.execute(0, new Object[] {AV81Itemnaomensuravel_areatrabalhocod, lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1, lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1, lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1, lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1, lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2, lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2, lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2, lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2, lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3, lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3, lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3, lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3, lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo, AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel, lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao, AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel, AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor, AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to, lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao, AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKOF2 = false;
            A709ReferenciaINM_Codigo = P00OF2_A709ReferenciaINM_Codigo[0];
            A718ItemNaoMensuravel_AreaTrabalhoCod = P00OF2_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            A715ItemNaoMensuravel_Codigo = P00OF2_A715ItemNaoMensuravel_Codigo[0];
            A716ItemNaoMensuravel_Ativo = P00OF2_A716ItemNaoMensuravel_Ativo[0];
            A717ItemNaoMensuravel_Tipo = P00OF2_A717ItemNaoMensuravel_Tipo[0];
            A719ItemNaoMensuravel_Valor = P00OF2_A719ItemNaoMensuravel_Valor[0];
            A714ItemNaoMensuravel_Descricao = P00OF2_A714ItemNaoMensuravel_Descricao[0];
            A710ReferenciaINM_Descricao = P00OF2_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OF2_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OF2_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            A710ReferenciaINM_Descricao = P00OF2_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OF2_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OF2_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            AV33count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00OF2_A718ItemNaoMensuravel_AreaTrabalhoCod[0] == A718ItemNaoMensuravel_AreaTrabalhoCod ) && ( StringUtil.StrCmp(P00OF2_A715ItemNaoMensuravel_Codigo[0], A715ItemNaoMensuravel_Codigo) == 0 ) )
            {
               BRKOF2 = false;
               AV33count = (long)(AV33count+1);
               BRKOF2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A715ItemNaoMensuravel_Codigo)) )
            {
               AV25Option = A715ItemNaoMensuravel_Codigo;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOF2 )
            {
               BRKOF2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADITEMNAOMENSURAVEL_DESCRICAOOPTIONS' Routine */
         AV12TFItemNaoMensuravel_Descricao = AV21SearchTxt;
         AV13TFItemNaoMensuravel_Descricao_Sel = "";
         AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = AV39DynamicFiltersSelector1;
         AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 = AV40DynamicFiltersOperator1;
         AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = AV41ItemNaoMensuravel_AreaTrabalhoDes1;
         AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = AV42ReferenciaINM_Descricao1;
         AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = AV46ItemNaoMensuravel_AreaTrabalhoDes2;
         AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = AV47ReferenciaINM_Descricao2;
         AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 = AV48DynamicFiltersEnabled3;
         AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = AV49DynamicFiltersSelector3;
         AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 = AV50DynamicFiltersOperator3;
         AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = AV51ItemNaoMensuravel_AreaTrabalhoDes3;
         AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = AV52ReferenciaINM_Descricao3;
         AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = AV10TFItemNaoMensuravel_Codigo;
         AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = AV11TFItemNaoMensuravel_Codigo_Sel;
         AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = AV12TFItemNaoMensuravel_Descricao;
         AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = AV13TFItemNaoMensuravel_Descricao_Sel;
         AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor = AV14TFItemNaoMensuravel_Valor;
         AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to = AV15TFItemNaoMensuravel_Valor_To;
         AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = AV16TFReferenciaINM_Descricao;
         AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = AV17TFReferenciaINM_Descricao_Sel;
         AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = AV19TFItemNaoMensuravel_Tipo_Sels;
         AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel = AV20TFItemNaoMensuravel_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ,
                                              AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ,
                                              AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ,
                                              AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ,
                                              AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ,
                                              AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ,
                                              AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ,
                                              AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ,
                                              AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ,
                                              AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ,
                                              AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ,
                                              AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ,
                                              AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ,
                                              AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ,
                                              AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ,
                                              AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ,
                                              AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ,
                                              AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ,
                                              AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ,
                                              AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ,
                                              AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ,
                                              AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ,
                                              AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels.Count ,
                                              AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ,
                                              A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                              A710ReferenciaINM_Descricao ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A716ItemNaoMensuravel_Ativo ,
                                              A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                              AV81Itemnaomensuravel_areatrabalhocod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1), "%", "");
         lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1), "%", "");
         lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1), "%", "");
         lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1), "%", "");
         lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2), "%", "");
         lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2), "%", "");
         lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2), "%", "");
         lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2), "%", "");
         lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3), "%", "");
         lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3), "%", "");
         lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3), "%", "");
         lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3), "%", "");
         lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = StringUtil.PadR( StringUtil.RTrim( AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo), 20, "%");
         lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = StringUtil.Concat( StringUtil.RTrim( AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao), "%", "");
         lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = StringUtil.Concat( StringUtil.RTrim( AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao), "%", "");
         /* Using cursor P00OF3 */
         pr_default.execute(1, new Object[] {AV81Itemnaomensuravel_areatrabalhocod, lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1, lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1, lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1, lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1, lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2, lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2, lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2, lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2, lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3, lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3, lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3, lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3, lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo, AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel, lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao, AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel, AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor, AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to, lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao, AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKOF4 = false;
            A709ReferenciaINM_Codigo = P00OF3_A709ReferenciaINM_Codigo[0];
            A718ItemNaoMensuravel_AreaTrabalhoCod = P00OF3_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            A714ItemNaoMensuravel_Descricao = P00OF3_A714ItemNaoMensuravel_Descricao[0];
            A716ItemNaoMensuravel_Ativo = P00OF3_A716ItemNaoMensuravel_Ativo[0];
            A717ItemNaoMensuravel_Tipo = P00OF3_A717ItemNaoMensuravel_Tipo[0];
            A719ItemNaoMensuravel_Valor = P00OF3_A719ItemNaoMensuravel_Valor[0];
            A715ItemNaoMensuravel_Codigo = P00OF3_A715ItemNaoMensuravel_Codigo[0];
            A710ReferenciaINM_Descricao = P00OF3_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OF3_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OF3_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            A710ReferenciaINM_Descricao = P00OF3_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OF3_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OF3_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            AV33count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00OF3_A714ItemNaoMensuravel_Descricao[0], A714ItemNaoMensuravel_Descricao) == 0 ) )
            {
               BRKOF4 = false;
               A718ItemNaoMensuravel_AreaTrabalhoCod = P00OF3_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               A715ItemNaoMensuravel_Codigo = P00OF3_A715ItemNaoMensuravel_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKOF4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A714ItemNaoMensuravel_Descricao)) )
            {
               AV25Option = A714ItemNaoMensuravel_Descricao;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOF4 )
            {
               BRKOF4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADREFERENCIAINM_DESCRICAOOPTIONS' Routine */
         AV16TFReferenciaINM_Descricao = AV21SearchTxt;
         AV17TFReferenciaINM_Descricao_Sel = "";
         AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = AV39DynamicFiltersSelector1;
         AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 = AV40DynamicFiltersOperator1;
         AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = AV41ItemNaoMensuravel_AreaTrabalhoDes1;
         AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = AV42ReferenciaINM_Descricao1;
         AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = AV46ItemNaoMensuravel_AreaTrabalhoDes2;
         AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = AV47ReferenciaINM_Descricao2;
         AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 = AV48DynamicFiltersEnabled3;
         AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = AV49DynamicFiltersSelector3;
         AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 = AV50DynamicFiltersOperator3;
         AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = AV51ItemNaoMensuravel_AreaTrabalhoDes3;
         AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = AV52ReferenciaINM_Descricao3;
         AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = AV10TFItemNaoMensuravel_Codigo;
         AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = AV11TFItemNaoMensuravel_Codigo_Sel;
         AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = AV12TFItemNaoMensuravel_Descricao;
         AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = AV13TFItemNaoMensuravel_Descricao_Sel;
         AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor = AV14TFItemNaoMensuravel_Valor;
         AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to = AV15TFItemNaoMensuravel_Valor_To;
         AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = AV16TFReferenciaINM_Descricao;
         AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = AV17TFReferenciaINM_Descricao_Sel;
         AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = AV19TFItemNaoMensuravel_Tipo_Sels;
         AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel = AV20TFItemNaoMensuravel_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ,
                                              AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ,
                                              AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ,
                                              AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ,
                                              AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ,
                                              AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ,
                                              AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ,
                                              AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ,
                                              AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ,
                                              AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ,
                                              AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ,
                                              AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ,
                                              AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ,
                                              AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ,
                                              AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ,
                                              AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ,
                                              AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ,
                                              AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ,
                                              AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ,
                                              AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ,
                                              AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ,
                                              AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ,
                                              AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels.Count ,
                                              AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ,
                                              A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                              A710ReferenciaINM_Descricao ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A716ItemNaoMensuravel_Ativo ,
                                              A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                              AV81Itemnaomensuravel_areatrabalhocod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1), "%", "");
         lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1), "%", "");
         lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1), "%", "");
         lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1), "%", "");
         lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2), "%", "");
         lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2), "%", "");
         lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2), "%", "");
         lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2), "%", "");
         lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3), "%", "");
         lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3), "%", "");
         lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3), "%", "");
         lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3), "%", "");
         lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = StringUtil.PadR( StringUtil.RTrim( AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo), 20, "%");
         lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = StringUtil.Concat( StringUtil.RTrim( AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao), "%", "");
         lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = StringUtil.Concat( StringUtil.RTrim( AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao), "%", "");
         /* Using cursor P00OF4 */
         pr_default.execute(2, new Object[] {AV81Itemnaomensuravel_areatrabalhocod, lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1, lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1, lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1, lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1, lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2, lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2, lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2, lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2, lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3, lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3, lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3, lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3, lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo, AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel, lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao, AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel, AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor, AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to, lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao, AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKOF6 = false;
            A709ReferenciaINM_Codigo = P00OF4_A709ReferenciaINM_Codigo[0];
            A718ItemNaoMensuravel_AreaTrabalhoCod = P00OF4_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            A716ItemNaoMensuravel_Ativo = P00OF4_A716ItemNaoMensuravel_Ativo[0];
            A717ItemNaoMensuravel_Tipo = P00OF4_A717ItemNaoMensuravel_Tipo[0];
            A719ItemNaoMensuravel_Valor = P00OF4_A719ItemNaoMensuravel_Valor[0];
            A714ItemNaoMensuravel_Descricao = P00OF4_A714ItemNaoMensuravel_Descricao[0];
            A715ItemNaoMensuravel_Codigo = P00OF4_A715ItemNaoMensuravel_Codigo[0];
            A710ReferenciaINM_Descricao = P00OF4_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OF4_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OF4_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            A710ReferenciaINM_Descricao = P00OF4_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OF4_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OF4_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            AV33count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00OF4_A709ReferenciaINM_Codigo[0] == A709ReferenciaINM_Codigo ) )
            {
               BRKOF6 = false;
               A718ItemNaoMensuravel_AreaTrabalhoCod = P00OF4_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               A715ItemNaoMensuravel_Codigo = P00OF4_A715ItemNaoMensuravel_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKOF6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A710ReferenciaINM_Descricao)) )
            {
               AV25Option = A710ReferenciaINM_Descricao;
               AV24InsertIndex = 1;
               while ( ( AV24InsertIndex <= AV26Options.Count ) && ( StringUtil.StrCmp(((String)AV26Options.Item(AV24InsertIndex)), AV25Option) < 0 ) )
               {
                  AV24InsertIndex = (int)(AV24InsertIndex+1);
               }
               AV26Options.Add(AV25Option, AV24InsertIndex);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), AV24InsertIndex);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOF6 )
            {
               BRKOF6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV26Options = new GxSimpleCollection();
         AV29OptionsDesc = new GxSimpleCollection();
         AV31OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV34Session = context.GetSession();
         AV36GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV37GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFItemNaoMensuravel_Codigo = "";
         AV11TFItemNaoMensuravel_Codigo_Sel = "";
         AV12TFItemNaoMensuravel_Descricao = "";
         AV13TFItemNaoMensuravel_Descricao_Sel = "";
         AV16TFReferenciaINM_Descricao = "";
         AV17TFReferenciaINM_Descricao_Sel = "";
         AV18TFItemNaoMensuravel_Tipo_SelsJson = "";
         AV19TFItemNaoMensuravel_Tipo_Sels = new GxSimpleCollection();
         AV38GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV39DynamicFiltersSelector1 = "";
         AV41ItemNaoMensuravel_AreaTrabalhoDes1 = "";
         AV42ReferenciaINM_Descricao1 = "";
         AV44DynamicFiltersSelector2 = "";
         AV46ItemNaoMensuravel_AreaTrabalhoDes2 = "";
         AV47ReferenciaINM_Descricao2 = "";
         AV49DynamicFiltersSelector3 = "";
         AV51ItemNaoMensuravel_AreaTrabalhoDes3 = "";
         AV52ReferenciaINM_Descricao3 = "";
         AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = "";
         AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = "";
         AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = "";
         AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = "";
         AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = "";
         AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = "";
         AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = "";
         AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = "";
         AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = "";
         AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = "";
         AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = "";
         AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = "";
         AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = "";
         AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = "";
         AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = "";
         AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = "";
         lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = "";
         lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = "";
         lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = "";
         lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = "";
         lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = "";
         lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = "";
         lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = "";
         lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = "";
         A720ItemNaoMensuravel_AreaTrabalhoDes = "";
         A710ReferenciaINM_Descricao = "";
         A715ItemNaoMensuravel_Codigo = "";
         A714ItemNaoMensuravel_Descricao = "";
         P00OF2_A709ReferenciaINM_Codigo = new int[1] ;
         P00OF2_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         P00OF2_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         P00OF2_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         P00OF2_A717ItemNaoMensuravel_Tipo = new short[1] ;
         P00OF2_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         P00OF2_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         P00OF2_A710ReferenciaINM_Descricao = new String[] {""} ;
         P00OF2_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         P00OF2_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         AV25Option = "";
         P00OF3_A709ReferenciaINM_Codigo = new int[1] ;
         P00OF3_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         P00OF3_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         P00OF3_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         P00OF3_A717ItemNaoMensuravel_Tipo = new short[1] ;
         P00OF3_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         P00OF3_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         P00OF3_A710ReferenciaINM_Descricao = new String[] {""} ;
         P00OF3_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         P00OF3_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         P00OF4_A709ReferenciaINM_Codigo = new int[1] ;
         P00OF4_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         P00OF4_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         P00OF4_A717ItemNaoMensuravel_Tipo = new short[1] ;
         P00OF4_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         P00OF4_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         P00OF4_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         P00OF4_A710ReferenciaINM_Descricao = new String[] {""} ;
         P00OF4_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         P00OF4_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwitemnaomensuravelfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00OF2_A709ReferenciaINM_Codigo, P00OF2_A718ItemNaoMensuravel_AreaTrabalhoCod, P00OF2_A715ItemNaoMensuravel_Codigo, P00OF2_A716ItemNaoMensuravel_Ativo, P00OF2_A717ItemNaoMensuravel_Tipo, P00OF2_A719ItemNaoMensuravel_Valor, P00OF2_A714ItemNaoMensuravel_Descricao, P00OF2_A710ReferenciaINM_Descricao, P00OF2_A720ItemNaoMensuravel_AreaTrabalhoDes, P00OF2_n720ItemNaoMensuravel_AreaTrabalhoDes
               }
               , new Object[] {
               P00OF3_A709ReferenciaINM_Codigo, P00OF3_A718ItemNaoMensuravel_AreaTrabalhoCod, P00OF3_A714ItemNaoMensuravel_Descricao, P00OF3_A716ItemNaoMensuravel_Ativo, P00OF3_A717ItemNaoMensuravel_Tipo, P00OF3_A719ItemNaoMensuravel_Valor, P00OF3_A715ItemNaoMensuravel_Codigo, P00OF3_A710ReferenciaINM_Descricao, P00OF3_A720ItemNaoMensuravel_AreaTrabalhoDes, P00OF3_n720ItemNaoMensuravel_AreaTrabalhoDes
               }
               , new Object[] {
               P00OF4_A709ReferenciaINM_Codigo, P00OF4_A718ItemNaoMensuravel_AreaTrabalhoCod, P00OF4_A716ItemNaoMensuravel_Ativo, P00OF4_A717ItemNaoMensuravel_Tipo, P00OF4_A719ItemNaoMensuravel_Valor, P00OF4_A714ItemNaoMensuravel_Descricao, P00OF4_A715ItemNaoMensuravel_Codigo, P00OF4_A710ReferenciaINM_Descricao, P00OF4_A720ItemNaoMensuravel_AreaTrabalhoDes, P00OF4_n720ItemNaoMensuravel_AreaTrabalhoDes
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV20TFItemNaoMensuravel_Ativo_Sel ;
      private short AV40DynamicFiltersOperator1 ;
      private short AV45DynamicFiltersOperator2 ;
      private short AV50DynamicFiltersOperator3 ;
      private short AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ;
      private short AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ;
      private short AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ;
      private short AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ;
      private short A717ItemNaoMensuravel_Tipo ;
      private int AV55GXV1 ;
      private int AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count ;
      private int AV81Itemnaomensuravel_areatrabalhocod ;
      private int A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int A709ReferenciaINM_Codigo ;
      private int AV24InsertIndex ;
      private long AV33count ;
      private decimal AV14TFItemNaoMensuravel_Valor ;
      private decimal AV15TFItemNaoMensuravel_Valor_To ;
      private decimal AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ;
      private decimal AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ;
      private decimal A719ItemNaoMensuravel_Valor ;
      private String AV10TFItemNaoMensuravel_Codigo ;
      private String AV11TFItemNaoMensuravel_Codigo_Sel ;
      private String AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ;
      private String AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ;
      private String scmdbuf ;
      private String lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ;
      private String A715ItemNaoMensuravel_Codigo ;
      private bool returnInSub ;
      private bool AV43DynamicFiltersEnabled2 ;
      private bool AV48DynamicFiltersEnabled3 ;
      private bool AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ;
      private bool AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ;
      private bool A716ItemNaoMensuravel_Ativo ;
      private bool BRKOF2 ;
      private bool n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool BRKOF4 ;
      private bool BRKOF6 ;
      private String AV32OptionIndexesJson ;
      private String AV27OptionsJson ;
      private String AV30OptionsDescJson ;
      private String AV18TFItemNaoMensuravel_Tipo_SelsJson ;
      private String A714ItemNaoMensuravel_Descricao ;
      private String AV23DDOName ;
      private String AV21SearchTxt ;
      private String AV22SearchTxtTo ;
      private String AV12TFItemNaoMensuravel_Descricao ;
      private String AV13TFItemNaoMensuravel_Descricao_Sel ;
      private String AV16TFReferenciaINM_Descricao ;
      private String AV17TFReferenciaINM_Descricao_Sel ;
      private String AV39DynamicFiltersSelector1 ;
      private String AV41ItemNaoMensuravel_AreaTrabalhoDes1 ;
      private String AV42ReferenciaINM_Descricao1 ;
      private String AV44DynamicFiltersSelector2 ;
      private String AV46ItemNaoMensuravel_AreaTrabalhoDes2 ;
      private String AV47ReferenciaINM_Descricao2 ;
      private String AV49DynamicFiltersSelector3 ;
      private String AV51ItemNaoMensuravel_AreaTrabalhoDes3 ;
      private String AV52ReferenciaINM_Descricao3 ;
      private String AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ;
      private String AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ;
      private String AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ;
      private String AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ;
      private String AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ;
      private String AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ;
      private String AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ;
      private String AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ;
      private String AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ;
      private String AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ;
      private String AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ;
      private String AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ;
      private String AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ;
      private String lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ;
      private String lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ;
      private String lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ;
      private String lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ;
      private String lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ;
      private String lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ;
      private String lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ;
      private String lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ;
      private String A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String A710ReferenciaINM_Descricao ;
      private String AV25Option ;
      private IGxSession AV34Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00OF2_A709ReferenciaINM_Codigo ;
      private int[] P00OF2_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] P00OF2_A715ItemNaoMensuravel_Codigo ;
      private bool[] P00OF2_A716ItemNaoMensuravel_Ativo ;
      private short[] P00OF2_A717ItemNaoMensuravel_Tipo ;
      private decimal[] P00OF2_A719ItemNaoMensuravel_Valor ;
      private String[] P00OF2_A714ItemNaoMensuravel_Descricao ;
      private String[] P00OF2_A710ReferenciaINM_Descricao ;
      private String[] P00OF2_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] P00OF2_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private int[] P00OF3_A709ReferenciaINM_Codigo ;
      private int[] P00OF3_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] P00OF3_A714ItemNaoMensuravel_Descricao ;
      private bool[] P00OF3_A716ItemNaoMensuravel_Ativo ;
      private short[] P00OF3_A717ItemNaoMensuravel_Tipo ;
      private decimal[] P00OF3_A719ItemNaoMensuravel_Valor ;
      private String[] P00OF3_A715ItemNaoMensuravel_Codigo ;
      private String[] P00OF3_A710ReferenciaINM_Descricao ;
      private String[] P00OF3_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] P00OF3_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private int[] P00OF4_A709ReferenciaINM_Codigo ;
      private int[] P00OF4_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private bool[] P00OF4_A716ItemNaoMensuravel_Ativo ;
      private short[] P00OF4_A717ItemNaoMensuravel_Tipo ;
      private decimal[] P00OF4_A719ItemNaoMensuravel_Valor ;
      private String[] P00OF4_A714ItemNaoMensuravel_Descricao ;
      private String[] P00OF4_A715ItemNaoMensuravel_Codigo ;
      private String[] P00OF4_A710ReferenciaINM_Descricao ;
      private String[] P00OF4_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] P00OF4_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV19TFItemNaoMensuravel_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV36GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV37GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV38GridStateDynamicFilter ;
   }

   public class getwwitemnaomensuravelfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00OF2( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ,
                                             String AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ,
                                             String AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ,
                                             bool AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ,
                                             String AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ,
                                             short AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ,
                                             String AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ,
                                             String AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ,
                                             bool AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ,
                                             String AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ,
                                             short AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ,
                                             String AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ,
                                             String AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ,
                                             String AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ,
                                             String AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ,
                                             String AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ,
                                             String AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ,
                                             decimal AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ,
                                             decimal AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ,
                                             String AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ,
                                             String AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ,
                                             int AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count ,
                                             short AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ,
                                             String A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                             String A710ReferenciaINM_Descricao ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             bool A716ItemNaoMensuravel_Ativo ,
                                             int AV81Itemnaomensuravel_areatrabalhocod ,
                                             int A718ItemNaoMensuravel_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [21] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ReferenciaINM_Codigo], T1.[ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod, T1.[ItemNaoMensuravel_Codigo], T1.[ItemNaoMensuravel_Ativo], T1.[ItemNaoMensuravel_Tipo], T1.[ItemNaoMensuravel_Valor], T1.[ItemNaoMensuravel_Descricao], T2.[ReferenciaINM_Descricao], T3.[AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes FROM (([ItemNaoMensuravel] T1 WITH (NOLOCK) INNER JOIN [ReferenciaINM] T2 WITH (NOLOCK) ON T2.[ReferenciaINM_Codigo] = T1.[ReferenciaINM_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[ItemNaoMensuravel_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ItemNaoMensuravel_AreaTrabalhoCod] = @AV81Itemnaomensuravel_areatrabalhocod)";
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] like @lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] = @AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] = @AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] >= @AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] <= @AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] = @AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
         }
         if ( AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 1)";
         }
         if ( AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ItemNaoMensuravel_AreaTrabalhoCod], T1.[ItemNaoMensuravel_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00OF3( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ,
                                             String AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ,
                                             String AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ,
                                             bool AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ,
                                             String AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ,
                                             short AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ,
                                             String AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ,
                                             String AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ,
                                             bool AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ,
                                             String AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ,
                                             short AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ,
                                             String AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ,
                                             String AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ,
                                             String AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ,
                                             String AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ,
                                             String AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ,
                                             String AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ,
                                             decimal AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ,
                                             decimal AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ,
                                             String AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ,
                                             String AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ,
                                             int AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count ,
                                             short AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ,
                                             String A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                             String A710ReferenciaINM_Descricao ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             bool A716ItemNaoMensuravel_Ativo ,
                                             int A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                             int AV81Itemnaomensuravel_areatrabalhocod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [21] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ReferenciaINM_Codigo], T1.[ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod, T1.[ItemNaoMensuravel_Descricao], T1.[ItemNaoMensuravel_Ativo], T1.[ItemNaoMensuravel_Tipo], T1.[ItemNaoMensuravel_Valor], T1.[ItemNaoMensuravel_Codigo], T2.[ReferenciaINM_Descricao], T3.[AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes FROM (([ItemNaoMensuravel] T1 WITH (NOLOCK) INNER JOIN [ReferenciaINM] T2 WITH (NOLOCK) ON T2.[ReferenciaINM_Codigo] = T1.[ReferenciaINM_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[ItemNaoMensuravel_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ItemNaoMensuravel_AreaTrabalhoCod] = @AV81Itemnaomensuravel_areatrabalhocod)";
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] like @lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] = @AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] = @AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] >= @AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] <= @AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] = @AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
         }
         if ( AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 1)";
         }
         if ( AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ItemNaoMensuravel_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00OF4( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ,
                                             String AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ,
                                             String AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ,
                                             bool AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ,
                                             String AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ,
                                             short AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ,
                                             String AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ,
                                             String AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ,
                                             bool AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ,
                                             String AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ,
                                             short AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ,
                                             String AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ,
                                             String AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ,
                                             String AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ,
                                             String AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ,
                                             String AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ,
                                             String AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ,
                                             decimal AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ,
                                             decimal AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ,
                                             String AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ,
                                             String AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ,
                                             int AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count ,
                                             short AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ,
                                             String A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                             String A710ReferenciaINM_Descricao ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             bool A716ItemNaoMensuravel_Ativo ,
                                             int A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                             int AV81Itemnaomensuravel_areatrabalhocod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [21] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ReferenciaINM_Codigo], T1.[ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod, T1.[ItemNaoMensuravel_Ativo], T1.[ItemNaoMensuravel_Tipo], T1.[ItemNaoMensuravel_Valor], T1.[ItemNaoMensuravel_Descricao], T1.[ItemNaoMensuravel_Codigo], T2.[ReferenciaINM_Descricao], T3.[AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes FROM (([ItemNaoMensuravel] T1 WITH (NOLOCK) INNER JOIN [ReferenciaINM] T2 WITH (NOLOCK) ON T2.[ReferenciaINM_Codigo] = T1.[ReferenciaINM_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[ItemNaoMensuravel_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ItemNaoMensuravel_AreaTrabalhoCod] = @AV81Itemnaomensuravel_areatrabalhocod)";
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV61WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV63WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV66WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV68WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] like @lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] = @AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] = @AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] >= @AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] <= @AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] = @AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV79WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
         }
         if ( AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 1)";
         }
         if ( AV80WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ReferenciaINM_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00OF2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (short)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (decimal)dynConstraints[30] , (bool)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
               case 1 :
                     return conditional_P00OF3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (short)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (decimal)dynConstraints[30] , (bool)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
               case 2 :
                     return conditional_P00OF4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (short)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (decimal)dynConstraints[30] , (bool)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00OF2 ;
          prmP00OF2 = new Object[] {
          new Object[] {"@AV81Itemnaomensuravel_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00OF3 ;
          prmP00OF3 = new Object[] {
          new Object[] {"@AV81Itemnaomensuravel_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00OF4 ;
          prmP00OF4 = new Object[] {
          new Object[] {"@AV81Itemnaomensuravel_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV60WWItemNaoMensuravelDS_4_Referenciainm_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWItemNaoMensuravelDS_9_Referenciainm_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV69WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV70WWItemNaoMensuravelDS_14_Referenciainm_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV71WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV72WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV73WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV74WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV75WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV76WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV77WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV78WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00OF2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OF2,100,0,true,false )
             ,new CursorDef("P00OF3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OF3,100,0,true,false )
             ,new CursorDef("P00OF4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OF4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(9);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(9);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwitemnaomensuravelfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwitemnaomensuravelfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwitemnaomensuravelfilterdata") )
          {
             return  ;
          }
          getwwitemnaomensuravelfilterdata worker = new getwwitemnaomensuravelfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
