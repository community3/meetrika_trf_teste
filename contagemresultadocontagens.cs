/*
               File: ContagemResultadoContagens
        Description: Contagem Resultado Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:16:27.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadocontagens : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action39") == 0 )
         {
            A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_39_1V72( A456ContagemResultado_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTAGEMRESULTADO_CONTADORFMCOD") == 0 )
         {
            A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n490ContagemResultado_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTAGEMRESULTADO_CONTADORFMCOD1V72( A490ContagemResultado_ContratadaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTAGEMRESULTADO_NAOCNFCNTCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTAGEMRESULTADO_NAOCNFCNTCOD1V72( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel27"+"_"+"vCONTAGEMRESULTADO_DIVERGENCIA") == 0 )
         {
            A462ContagemResultado_Divergencia = NumberUtil.Val( GetNextPar( ), ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A462ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( A462ContagemResultado_Divergencia, 6, 2)));
            AV27ContagemResultado_Divergencia = NumberUtil.Val( GetNextPar( ), ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV27ContagemResultado_Divergencia, 6, 2)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX27ASACONTAGEMRESULTADO_DIVERGENCIA1V72( A462ContagemResultado_Divergencia, AV27ContagemResultado_Divergencia) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel28"+"_"+"vCONTAGEMRESULTADO_DIVERGENCIA") == 0 )
         {
            Gx_mode = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            AV20CalculoDivergencia = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CalculoDivergencia", AV20CalculoDivergencia);
            A458ContagemResultado_PFBFS = NumberUtil.Val( GetNextPar( ), ".");
            n458ContagemResultado_PFBFS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
            A460ContagemResultado_PFBFM = NumberUtil.Val( GetNextPar( ), ".");
            n460ContagemResultado_PFBFM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
            A459ContagemResultado_PFLFS = NumberUtil.Val( GetNextPar( ), ".");
            n459ContagemResultado_PFLFS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)));
            A461ContagemResultado_PFLFM = NumberUtil.Val( GetNextPar( ), ".");
            n461ContagemResultado_PFLFM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX28ASACONTAGEMRESULTADO_DIVERGENCIA1V72( Gx_mode, AV20CalculoDivergencia, A458ContagemResultado_PFBFS, A460ContagemResultado_PFBFM, A459ContagemResultado_PFLFS, A461ContagemResultado_PFLFM) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel31"+"_"+"vCALCULOPFINAL") == 0 )
         {
            A1553ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1553ContagemResultado_CntSrvCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
            A470ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
            A833ContagemResultado_CstUntPrd = NumberUtil.Val( GetNextPar( ), ".");
            n833ContagemResultado_CstUntPrd = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A833ContagemResultado_CstUntPrd", StringUtil.LTrim( StringUtil.Str( A833ContagemResultado_CstUntPrd, 18, 5)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX31ASACALCULOPFINAL1V72( A1553ContagemResultado_CntSrvCod, A470ContagemResultado_ContadorFMCod, A833ContagemResultado_CstUntPrd) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_41") == 0 )
         {
            A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_41( A456ContagemResultado_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_44") == 0 )
         {
            A146Modulo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n146Modulo_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_44( A146Modulo_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_45") == 0 )
         {
            A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_45( A127Sistema_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_46") == 0 )
         {
            A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n490ContagemResultado_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_46( A490ContagemResultado_ContratadaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_47") == 0 )
         {
            A1553ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1553ContagemResultado_CntSrvCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_47( A1553ContagemResultado_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_42") == 0 )
         {
            A470ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_42( A470ContagemResultado_ContadorFMCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_48") == 0 )
         {
            A479ContagemResultado_CrFMPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n479ContagemResultado_CrFMPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A479ContagemResultado_CrFMPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A479ContagemResultado_CrFMPessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_48( A479ContagemResultado_CrFMPessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_43") == 0 )
         {
            A469ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n469ContagemResultado_NaoCnfCntCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A469ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_43( A469ContagemResultado_NaoCnfCntCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
               AV8ContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_DataCnt", context.localUtil.Format(AV8ContagemResultado_DataCnt, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV8ContagemResultado_DataCnt));
               AV18ContagemResultado_HoraCnt = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_HoraCnt", AV18ContagemResultado_HoraCnt);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18ContagemResultado_HoraCnt, ""))));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynContagemResultado_ContadorFMCod.Name = "CONTAGEMRESULTADO_CONTADORFMCOD";
         dynContagemResultado_ContadorFMCod.WebTags = "";
         dynContagemResultado_NaoCnfCntCod.Name = "CONTAGEMRESULTADO_NAOCNFCNTCOD";
         dynContagemResultado_NaoCnfCntCod.WebTags = "";
         cmbContagemResultado_StatusCnt.Name = "CONTAGEMRESULTADO_STATUSCNT";
         cmbContagemResultado_StatusCnt.WebTags = "";
         cmbContagemResultado_StatusCnt.addItem("1", "Inicial", 0);
         cmbContagemResultado_StatusCnt.addItem("2", "Auditoria", 0);
         cmbContagemResultado_StatusCnt.addItem("3", "Negociac�o", 0);
         cmbContagemResultado_StatusCnt.addItem("4", "N�o Aprovada", 0);
         cmbContagemResultado_StatusCnt.addItem("5", "Entregue", 0);
         cmbContagemResultado_StatusCnt.addItem("6", "Pend�ncias", 0);
         cmbContagemResultado_StatusCnt.addItem("7", "Diverg�ncia", 0);
         cmbContagemResultado_StatusCnt.addItem("8", "Aprovada", 0);
         if ( cmbContagemResultado_StatusCnt.ItemCount > 0 )
         {
            A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cmbContagemResultado_StatusCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado Contagens", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultado_DataCnt_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadocontagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadocontagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContagemResultado_Codigo ,
                           DateTime aP2_ContagemResultado_DataCnt ,
                           String aP3_ContagemResultado_HoraCnt )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         this.AV8ContagemResultado_DataCnt = aP2_ContagemResultado_DataCnt;
         this.AV18ContagemResultado_HoraCnt = aP3_ContagemResultado_HoraCnt;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContagemResultado_ContadorFMCod = new GXCombobox();
         dynContagemResultado_NaoCnfCntCod = new GXCombobox();
         cmbContagemResultado_StatusCnt = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContagemResultado_ContadorFMCod.ItemCount > 0 )
         {
            A470ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynContagemResultado_ContadorFMCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
         }
         if ( dynContagemResultado_NaoCnfCntCod.ItemCount > 0 )
         {
            A469ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( dynContagemResultado_NaoCnfCntCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0))), "."));
            n469ContagemResultado_NaoCnfCntCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A469ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0)));
         }
         if ( cmbContagemResultado_StatusCnt.ItemCount > 0 )
         {
            A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cmbContagemResultado_StatusCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1V72( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1V72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultado_Codigo_Visible, edtContagemResultado_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoContagens.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1V72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1V72( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1V72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_106_1V72( true) ;
         }
         return  ;
      }

      protected void wb_table3_106_1V72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1V72e( true) ;
         }
         else
         {
            wb_table1_2_1V72e( false) ;
         }
      }

      protected void wb_table3_106_1V72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_106_1V72e( true) ;
         }
         else
         {
            wb_table3_106_1V72e( false) ;
         }
      }

      protected void wb_table2_5_1V72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1V72( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1V72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1V72e( true) ;
         }
         else
         {
            wb_table2_5_1V72e( false) ;
         }
      }

      protected void wb_table4_13_1V72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_osfsosfm_Internalname, "OS Ref|OS", "", "", lblTextblockcontagemresultado_osfsosfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_OsFsOsFm_Internalname, A501ContagemResultado_OsFsOsFm, StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_OsFsOsFm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_OsFsOsFm_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_datacnt_Internalname, "Data", "", "", lblTextblockcontagemresultado_datacnt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_25_1V72( true) ;
         }
         return  ;
      }

      protected void wb_table5_25_1V72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contadorfmcod_Internalname, "Contador FM", "", "", lblTextblockcontagemresultado_contadorfmcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontagemresultado_contadorfmcod_Visible, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagemResultado_ContadorFMCod, dynContagemResultado_ContadorFMCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)), 1, dynContagemResultado_ContadorFMCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynContagemResultado_ContadorFMCod.Visible, dynContagemResultado_ContadorFMCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_ContagemResultadoContagens.htm");
            dynContagemResultado_ContadorFMCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ContadorFMCod_Internalname, "Values", (String)(dynContagemResultado_ContadorFMCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfs_Internalname, "PF Bruto FS", "", "", lblTextblockcontagemresultado_pfbfs_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PFBFS_Internalname, StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PFBFS_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_PFBFS_Enabled, 1, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfs_Internalname, "PF L�quido FS", "", "", lblTextblockcontagemresultado_pflfs_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PFLFS_Internalname, StringUtil.LTrim( StringUtil.NToC( A459ContagemResultado_PFLFS, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PFLFS_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_PFLFS_Enabled, 1, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfm_Internalname, "PF Bruto FM", "", "", lblTextblockcontagemresultado_pfbfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PFBFM_Internalname, StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PFBFM_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_PFBFM_Enabled, 1, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfm_Internalname, "PF L�quido FM", "", "", lblTextblockcontagemresultado_pflfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PFLFM_Internalname, StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PFLFM_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_PFLFM_Enabled, 1, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_deflator_Internalname, "Deflator", "", "", lblTextblockcontagemresultado_deflator_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Deflator_Internalname, StringUtil.LTrim( StringUtil.NToC( A800ContagemResultado_Deflator, 6, 3, ",", "")), ((edtContagemResultado_Deflator_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A800ContagemResultado_Deflator, "Z9.999")) : context.localUtil.Format( A800ContagemResultado_Deflator, "Z9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','3');"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Deflator_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtContagemResultado_Deflator_Visible, edtContagemResultado_Deflator_Enabled, 0, "text", "", 80, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Deflator", "right", false, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_parecertcn_Internalname, "Parecer t�cnico", "", "", lblTextblockcontagemresultado_parecertcn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultado_ParecerTcn_Internalname, A463ContagemResultado_ParecerTcn, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", 0, 1, edtContagemResultado_ParecerTcn_Enabled, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_naocnfcntcod_Internalname, "N�o conformidade", "", "", lblTextblockcontagemresultado_naocnfcntcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagemResultado_NaoCnfCntCod, dynContagemResultado_NaoCnfCntCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0)), 1, dynContagemResultado_NaoCnfCntCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContagemResultado_NaoCnfCntCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_ContagemResultadoContagens.htm");
            dynContagemResultado_NaoCnfCntCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_NaoCnfCntCod_Internalname, "Values", (String)(dynContagemResultado_NaoCnfCntCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_statuscnt_Internalname, "Status", "", "", lblTextblockcontagemresultado_statuscnt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultado_StatusCnt, cmbContagemResultado_StatusCnt_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)), 1, cmbContagemResultado_StatusCnt_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContagemResultado_StatusCnt.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "", true, "HLP_ContagemResultadoContagens.htm");
            cmbContagemResultado_StatusCnt.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_StatusCnt_Internalname, "Values", (String)(cmbContagemResultado_StatusCnt.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_divergencia_Internalname, "Diverg�ncia", "", "", lblTextblockcontagemresultado_divergencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_78_1V72( true) ;
         }
         return  ;
      }

      protected void wb_table6_78_1V72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_planilha_Internalname, "Planilha", "", "", lblTextblockcontagemresultado_planilha_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataContentCell'>") ;
            ClassString = "BootstrapAttribute";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'',0)\"";
            edtContagemResultado_Planilha_Filename = A853ContagemResultado_NomePla;
            edtContagemResultado_Planilha_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Planilha_Internalname, "Filetype", edtContagemResultado_Planilha_Filetype);
            edtContagemResultado_Planilha_Filetype = A854ContagemResultado_TipoPla;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Planilha_Internalname, "Filetype", edtContagemResultado_Planilha_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A852ContagemResultado_Planilha)) )
            {
               gxblobfileaux.Source = A852ContagemResultado_Planilha;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtContagemResultado_Planilha_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtContagemResultado_Planilha_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A852ContagemResultado_Planilha = gxblobfileaux.GetAbsoluteName();
                  n852ContagemResultado_Planilha = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A852ContagemResultado_Planilha", A852ContagemResultado_Planilha);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Planilha_Internalname, "URL", context.PathToRelativeUrl( A852ContagemResultado_Planilha));
                  edtContagemResultado_Planilha_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Planilha_Internalname, "Filetype", edtContagemResultado_Planilha_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Planilha_Internalname, "URL", context.PathToRelativeUrl( A852ContagemResultado_Planilha));
            }
            GxWebStd.gx_blob_field( context, edtContagemResultado_Planilha_Internalname, StringUtil.RTrim( A852ContagemResultado_Planilha), context.PathToRelativeUrl( A852ContagemResultado_Planilha), (String.IsNullOrEmpty(StringUtil.RTrim( edtContagemResultado_Planilha_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtContagemResultado_Planilha_Filetype)) ? A852ContagemResultado_Planilha : edtContagemResultado_Planilha_Filetype)) : edtContagemResultado_Planilha_Contenttype), true, "", edtContagemResultado_Planilha_Parameters, edtContagemResultado_Planilha_Display, edtContagemResultado_Planilha_Enabled, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtContagemResultado_Planilha_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"", "", "", "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_nomepla_Internalname, "Nome da Planilha", "", "", lblTextblockcontagemresultado_nomepla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_NomePla_Internalname, StringUtil.RTrim( A853ContagemResultado_NomePla), StringUtil.RTrim( context.localUtil.Format( A853ContagemResultado_NomePla, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_NomePla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_NomePla_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_tipopla_Internalname, "Tipo de Arquivo", "", "", lblTextblockcontagemresultado_tipopla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_TipoPla_Internalname, StringUtil.RTrim( A854ContagemResultado_TipoPla), StringUtil.RTrim( context.localUtil.Format( A854ContagemResultado_TipoPla, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_TipoPla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_TipoPla_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "TipoArq", "left", true, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1V72e( true) ;
         }
         else
         {
            wb_table4_13_1V72e( false) ;
         }
      }

      protected void wb_table6_78_1V72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_divergencia_Internalname, tblTablemergedcontagemresultado_divergencia_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_divergencia_Internalname, StringUtil.LTrim( StringUtil.NToC( AV27ContagemResultado_Divergencia, 6, 2, ",", "")), ((edtavContagemresultado_divergencia_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV27ContagemResultado_Divergencia, "ZZ9.99")) : context.localUtil.Format( AV27ContagemResultado_Divergencia, "ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_divergencia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContagemresultado_divergencia_Enabled, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_divergencia_righttext_Internalname, "%", "", "", lblContagemresultado_divergencia_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadocontagens_esforco_Internalname, "Esfor�o", "", "", lblTextblockcontagemresultadocontagens_esforco_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoContagens_Esforco_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0, ",", "")), ((edtContagemResultadoContagens_Esforco_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A482ContagemResultadoContagens_Esforco), "ZZZ9")) : context.localUtil.Format( (decimal)(A482ContagemResultadoContagens_Esforco), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoContagens_Esforco_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultadoContagens_Esforco_Enabled, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadocontagens_esforco_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadocontagens_esforco_righttext_Internalname, "min.", "", "", lblContagemresultadocontagens_esforco_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_78_1V72e( true) ;
         }
         else
         {
            wb_table6_78_1V72e( false) ;
         }
      }

      protected void wb_table5_25_1V72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_datacnt_Internalname, tblTablemergedcontagemresultado_datacnt_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_DataCnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DataCnt_Internalname, context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"), context.localUtil.Format( A473ContagemResultado_DataCnt, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DataCnt_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContagemResultado_DataCnt_Enabled, 1, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoContagens.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_DataCnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultado_DataCnt_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_HoraCnt_Internalname, StringUtil.RTrim( A511ContagemResultado_HoraCnt), StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_HoraCnt_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_HoraCnt_Enabled, 1, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_25_1V72e( true) ;
         }
         else
         {
            wb_table5_25_1V72e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111V2 */
         E111V2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A501ContagemResultado_OsFsOsFm = cgiGet( edtContagemResultado_OsFsOsFm_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
               if ( context.localUtil.VCDate( cgiGet( edtContagemResultado_DataCnt_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data"}), 1, "CONTAGEMRESULTADO_DATACNT");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_DataCnt_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A473ContagemResultado_DataCnt = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
               }
               else
               {
                  A473ContagemResultado_DataCnt = context.localUtil.CToD( cgiGet( edtContagemResultado_DataCnt_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
               }
               A511ContagemResultado_HoraCnt = cgiGet( edtContagemResultado_HoraCnt_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
               dynContagemResultado_ContadorFMCod.CurrentValue = cgiGet( dynContagemResultado_ContadorFMCod_Internalname);
               A470ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( cgiGet( dynContagemResultado_ContadorFMCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFS_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFS_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_PFBFS");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_PFBFS_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A458ContagemResultado_PFBFS = 0;
                  n458ContagemResultado_PFBFS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
               }
               else
               {
                  A458ContagemResultado_PFBFS = context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFS_Internalname), ",", ".");
                  n458ContagemResultado_PFBFS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
               }
               n458ContagemResultado_PFBFS = ((Convert.ToDecimal(0)==A458ContagemResultado_PFBFS) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFS_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFS_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_PFLFS");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_PFLFS_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A459ContagemResultado_PFLFS = 0;
                  n459ContagemResultado_PFLFS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)));
               }
               else
               {
                  A459ContagemResultado_PFLFS = context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFS_Internalname), ",", ".");
                  n459ContagemResultado_PFLFS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)));
               }
               n459ContagemResultado_PFLFS = ((Convert.ToDecimal(0)==A459ContagemResultado_PFLFS) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFM_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFM_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_PFBFM");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_PFBFM_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A460ContagemResultado_PFBFM = 0;
                  n460ContagemResultado_PFBFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
               }
               else
               {
                  A460ContagemResultado_PFBFM = context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFM_Internalname), ",", ".");
                  n460ContagemResultado_PFBFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
               }
               n460ContagemResultado_PFBFM = ((Convert.ToDecimal(0)==A460ContagemResultado_PFBFM) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFM_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFM_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_PFLFM");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_PFLFM_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A461ContagemResultado_PFLFM = 0;
                  n461ContagemResultado_PFLFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
               }
               else
               {
                  A461ContagemResultado_PFLFM = context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFM_Internalname), ",", ".");
                  n461ContagemResultado_PFLFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
               }
               n461ContagemResultado_PFLFM = ((Convert.ToDecimal(0)==A461ContagemResultado_PFLFM) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Deflator_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Deflator_Internalname), ",", ".") > 99.999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_DEFLATOR");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_Deflator_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A800ContagemResultado_Deflator = 0;
                  n800ContagemResultado_Deflator = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A800ContagemResultado_Deflator", StringUtil.LTrim( StringUtil.Str( A800ContagemResultado_Deflator, 6, 3)));
               }
               else
               {
                  A800ContagemResultado_Deflator = context.localUtil.CToN( cgiGet( edtContagemResultado_Deflator_Internalname), ",", ".");
                  n800ContagemResultado_Deflator = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A800ContagemResultado_Deflator", StringUtil.LTrim( StringUtil.Str( A800ContagemResultado_Deflator, 6, 3)));
               }
               n800ContagemResultado_Deflator = ((Convert.ToDecimal(0)==A800ContagemResultado_Deflator) ? true : false);
               A463ContagemResultado_ParecerTcn = cgiGet( edtContagemResultado_ParecerTcn_Internalname);
               n463ContagemResultado_ParecerTcn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A463ContagemResultado_ParecerTcn", A463ContagemResultado_ParecerTcn);
               n463ContagemResultado_ParecerTcn = (String.IsNullOrEmpty(StringUtil.RTrim( A463ContagemResultado_ParecerTcn)) ? true : false);
               dynContagemResultado_NaoCnfCntCod.CurrentValue = cgiGet( dynContagemResultado_NaoCnfCntCod_Internalname);
               A469ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( cgiGet( dynContagemResultado_NaoCnfCntCod_Internalname), "."));
               n469ContagemResultado_NaoCnfCntCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A469ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0)));
               n469ContagemResultado_NaoCnfCntCod = ((0==A469ContagemResultado_NaoCnfCntCod) ? true : false);
               cmbContagemResultado_StatusCnt.CurrentValue = cgiGet( cmbContagemResultado_StatusCnt_Internalname);
               A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cgiGet( cmbContagemResultado_StatusCnt_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
               AV27ContagemResultado_Divergencia = context.localUtil.CToN( cgiGet( edtavContagemresultado_divergencia_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV27ContagemResultado_Divergencia, 6, 2)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoContagens_Esforco_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoContagens_Esforco_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOCONTAGENS_ESFORCO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoContagens_Esforco_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A482ContagemResultadoContagens_Esforco = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A482ContagemResultadoContagens_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0)));
               }
               else
               {
                  A482ContagemResultadoContagens_Esforco = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoContagens_Esforco_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A482ContagemResultadoContagens_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0)));
               }
               A852ContagemResultado_Planilha = cgiGet( edtContagemResultado_Planilha_Internalname);
               n852ContagemResultado_Planilha = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A852ContagemResultado_Planilha", A852ContagemResultado_Planilha);
               n852ContagemResultado_Planilha = (String.IsNullOrEmpty(StringUtil.RTrim( A852ContagemResultado_Planilha)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A456ContagemResultado_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               }
               else
               {
                  A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               }
               /* Read saved values. */
               Z456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z456ContagemResultado_Codigo"), ",", "."));
               Z473ContagemResultado_DataCnt = context.localUtil.CToD( cgiGet( "Z473ContagemResultado_DataCnt"), 0);
               Z511ContagemResultado_HoraCnt = cgiGet( "Z511ContagemResultado_HoraCnt");
               Z482ContagemResultadoContagens_Esforco = (short)(context.localUtil.CToN( cgiGet( "Z482ContagemResultadoContagens_Esforco"), ",", "."));
               Z462ContagemResultado_Divergencia = context.localUtil.CToN( cgiGet( "Z462ContagemResultado_Divergencia"), ",", ".");
               Z483ContagemResultado_StatusCnt = (short)(context.localUtil.CToN( cgiGet( "Z483ContagemResultado_StatusCnt"), ",", "."));
               Z481ContagemResultado_TimeCnt = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( "Z481ContagemResultado_TimeCnt"), 0));
               n481ContagemResultado_TimeCnt = ((DateTime.MinValue==A481ContagemResultado_TimeCnt) ? true : false);
               Z901ContagemResultadoContagens_Prazo = context.localUtil.CToT( cgiGet( "Z901ContagemResultadoContagens_Prazo"), 0);
               n901ContagemResultadoContagens_Prazo = ((DateTime.MinValue==A901ContagemResultadoContagens_Prazo) ? true : false);
               Z458ContagemResultado_PFBFS = context.localUtil.CToN( cgiGet( "Z458ContagemResultado_PFBFS"), ",", ".");
               n458ContagemResultado_PFBFS = ((Convert.ToDecimal(0)==A458ContagemResultado_PFBFS) ? true : false);
               Z459ContagemResultado_PFLFS = context.localUtil.CToN( cgiGet( "Z459ContagemResultado_PFLFS"), ",", ".");
               n459ContagemResultado_PFLFS = ((Convert.ToDecimal(0)==A459ContagemResultado_PFLFS) ? true : false);
               Z460ContagemResultado_PFBFM = context.localUtil.CToN( cgiGet( "Z460ContagemResultado_PFBFM"), ",", ".");
               n460ContagemResultado_PFBFM = ((Convert.ToDecimal(0)==A460ContagemResultado_PFBFM) ? true : false);
               Z461ContagemResultado_PFLFM = context.localUtil.CToN( cgiGet( "Z461ContagemResultado_PFLFM"), ",", ".");
               n461ContagemResultado_PFLFM = ((Convert.ToDecimal(0)==A461ContagemResultado_PFLFM) ? true : false);
               Z517ContagemResultado_Ultima = StringUtil.StrToBool( cgiGet( "Z517ContagemResultado_Ultima"));
               Z800ContagemResultado_Deflator = context.localUtil.CToN( cgiGet( "Z800ContagemResultado_Deflator"), ",", ".");
               n800ContagemResultado_Deflator = ((Convert.ToDecimal(0)==A800ContagemResultado_Deflator) ? true : false);
               Z833ContagemResultado_CstUntPrd = context.localUtil.CToN( cgiGet( "Z833ContagemResultado_CstUntPrd"), ",", ".");
               n833ContagemResultado_CstUntPrd = ((Convert.ToDecimal(0)==A833ContagemResultado_CstUntPrd) ? true : false);
               Z1756ContagemResultado_NvlCnt = (short)(context.localUtil.CToN( cgiGet( "Z1756ContagemResultado_NvlCnt"), ",", "."));
               n1756ContagemResultado_NvlCnt = ((0==A1756ContagemResultado_NvlCnt) ? true : false);
               Z470ContagemResultado_ContadorFMCod = (int)(context.localUtil.CToN( cgiGet( "Z470ContagemResultado_ContadorFMCod"), ",", "."));
               Z469ContagemResultado_NaoCnfCntCod = (int)(context.localUtil.CToN( cgiGet( "Z469ContagemResultado_NaoCnfCntCod"), ",", "."));
               n469ContagemResultado_NaoCnfCntCod = ((0==A469ContagemResultado_NaoCnfCntCod) ? true : false);
               A462ContagemResultado_Divergencia = context.localUtil.CToN( cgiGet( "Z462ContagemResultado_Divergencia"), ",", ".");
               A481ContagemResultado_TimeCnt = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( "Z481ContagemResultado_TimeCnt"), 0));
               n481ContagemResultado_TimeCnt = false;
               n481ContagemResultado_TimeCnt = ((DateTime.MinValue==A481ContagemResultado_TimeCnt) ? true : false);
               A901ContagemResultadoContagens_Prazo = context.localUtil.CToT( cgiGet( "Z901ContagemResultadoContagens_Prazo"), 0);
               n901ContagemResultadoContagens_Prazo = false;
               n901ContagemResultadoContagens_Prazo = ((DateTime.MinValue==A901ContagemResultadoContagens_Prazo) ? true : false);
               A517ContagemResultado_Ultima = StringUtil.StrToBool( cgiGet( "Z517ContagemResultado_Ultima"));
               A833ContagemResultado_CstUntPrd = context.localUtil.CToN( cgiGet( "Z833ContagemResultado_CstUntPrd"), ",", ".");
               n833ContagemResultado_CstUntPrd = false;
               n833ContagemResultado_CstUntPrd = ((Convert.ToDecimal(0)==A833ContagemResultado_CstUntPrd) ? true : false);
               A1756ContagemResultado_NvlCnt = (short)(context.localUtil.CToN( cgiGet( "Z1756ContagemResultado_NvlCnt"), ",", "."));
               n1756ContagemResultado_NvlCnt = false;
               n1756ContagemResultado_NvlCnt = ((0==A1756ContagemResultado_NvlCnt) ? true : false);
               O483ContagemResultado_StatusCnt = (short)(context.localUtil.CToN( cgiGet( "O483ContagemResultado_StatusCnt"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N469ContagemResultado_NaoCnfCntCod = (int)(context.localUtil.CToN( cgiGet( "N469ContagemResultado_NaoCnfCntCod"), ",", "."));
               n469ContagemResultado_NaoCnfCntCod = ((0==A469ContagemResultado_NaoCnfCntCod) ? true : false);
               A457ContagemResultado_Demanda = cgiGet( "CONTAGEMRESULTADO_DEMANDA");
               n457ContagemResultado_Demanda = false;
               A493ContagemResultado_DemandaFM = cgiGet( "CONTAGEMRESULTADO_DEMANDAFM");
               n493ContagemResultado_DemandaFM = false;
               AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTAGEMRESULTADO_CODIGO"), ",", "."));
               AV8ContagemResultado_DataCnt = context.localUtil.CToD( cgiGet( "vCONTAGEMRESULTADO_DATACNT"), 0);
               AV18ContagemResultado_HoraCnt = cgiGet( "vCONTAGEMRESULTADO_HORACNT");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               AV10Insert_ContagemResultado_ContadorFMCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEMRESULTADO_CONTADORFMCOD"), ",", "."));
               AV11Insert_ContagemResultado_NaoCnfCntCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEMRESULTADO_NAOCNFCNTCOD"), ",", "."));
               A462ContagemResultado_Divergencia = context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_DIVERGENCIA"), ",", ".");
               AV20CalculoDivergencia = cgiGet( "vCALCULODIVERGENCIA");
               AV19IndiceDivergencia = context.localUtil.CToN( cgiGet( "vINDICEDIVERGENCIA"), ",", ".");
               A1553ContagemResultado_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CNTSRVCOD"), ",", "."));
               A833ContagemResultado_CstUntPrd = context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CSTUNTPRD"), ",", ".");
               n833ContagemResultado_CstUntPrd = ((Convert.ToDecimal(0)==A833ContagemResultado_CstUntPrd) ? true : false);
               AV35CalculoPFinal = cgiGet( "vCALCULOPFINAL");
               A517ContagemResultado_Ultima = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_ULTIMA"));
               A490ContagemResultado_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CONTRATADACOD"), ",", "."));
               A481ContagemResultado_TimeCnt = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( "CONTAGEMRESULTADO_TIMECNT"), 0));
               n481ContagemResultado_TimeCnt = ((DateTime.MinValue==A481ContagemResultado_TimeCnt) ? true : false);
               A901ContagemResultadoContagens_Prazo = context.localUtil.CToT( cgiGet( "CONTAGEMRESULTADOCONTAGENS_PRAZO"), 0);
               n901ContagemResultadoContagens_Prazo = ((DateTime.MinValue==A901ContagemResultadoContagens_Prazo) ? true : false);
               A1756ContagemResultado_NvlCnt = (short)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_NVLCNT"), ",", "."));
               n1756ContagemResultado_NvlCnt = ((0==A1756ContagemResultado_NvlCnt) ? true : false);
               A146Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( "MODULO_CODIGO"), ",", "."));
               A485ContagemResultado_EhValidacao = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_EHVALIDACAO"));
               n485ContagemResultado_EhValidacao = false;
               A484ContagemResultado_StatusDmn = cgiGet( "CONTAGEMRESULTADO_STATUSDMN");
               n484ContagemResultado_StatusDmn = false;
               A1389ContagemResultado_RdmnIssueId = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_RDMNISSUEID"), ",", "."));
               n1389ContagemResultado_RdmnIssueId = false;
               A890ContagemResultado_Responsavel = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_RESPONSAVEL"), ",", "."));
               n890ContagemResultado_Responsavel = false;
               A468ContagemResultado_NaoCnfDmnCod = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_NAOCNFDMNCOD"), ",", "."));
               n468ContagemResultado_NaoCnfDmnCod = false;
               A805ContagemResultado_ContratadaOrigemCod = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD"), ",", "."));
               n805ContagemResultado_ContratadaOrigemCod = false;
               A602ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_OSVINCULADA"), ",", "."));
               n602ContagemResultado_OSVinculada = false;
               A999ContagemResultado_CrFMEhContratada = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_CRFMEHCONTRATADA"));
               n999ContagemResultado_CrFMEhContratada = false;
               A1000ContagemResultado_CrFMEhContratante = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_CRFMEHCONTRATANTE"));
               n1000ContagemResultado_CrFMEhContratante = false;
               A479ContagemResultado_CrFMPessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CRFMPESSOACOD"), ",", "."));
               A478ContagemResultado_NaoCnfCntNom = cgiGet( "CONTAGEMRESULTADO_NAOCNFCNTNOM");
               n478ContagemResultado_NaoCnfCntNom = false;
               A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "SISTEMA_CODIGO"), ",", "."));
               A513Sistema_Coordenacao = cgiGet( "SISTEMA_COORDENACAO");
               n513Sistema_Coordenacao = false;
               A52Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_AREATRABALHOCOD"), ",", "."));
               n52Contratada_AreaTrabalhoCod = false;
               A601ContagemResultado_Servico = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_SERVICO"), ",", "."));
               n601ContagemResultado_Servico = false;
               A474ContagemResultado_ContadorFMNom = cgiGet( "CONTAGEMRESULTADO_CONTADORFMNOM");
               n474ContagemResultado_ContadorFMNom = false;
               AV37Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               edtContagemResultado_Planilha_Filetype = cgiGet( "CONTAGEMRESULTADO_PLANILHA_Filetype");
               edtContagemResultado_Planilha_Filename = cgiGet( "CONTAGEMRESULTADO_PLANILHA_Filename");
               edtContagemResultado_Planilha_Filename = cgiGet( "CONTAGEMRESULTADO_PLANILHA_Filename");
               edtContagemResultado_Planilha_Filetype = cgiGet( "CONTAGEMRESULTADO_PLANILHA_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A852ContagemResultado_Planilha)) )
               {
                  edtContagemResultado_Planilha_Filename = (String)(CGIGetFileName(edtContagemResultado_Planilha_Internalname));
                  edtContagemResultado_Planilha_Filetype = (String)(CGIGetFileType(edtContagemResultado_Planilha_Internalname));
               }
               A854ContagemResultado_TipoPla = edtContagemResultado_Planilha_Filetype;
               n854ContagemResultado_TipoPla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A854ContagemResultado_TipoPla", A854ContagemResultado_TipoPla);
               A853ContagemResultado_NomePla = edtContagemResultado_Planilha_Filename;
               n853ContagemResultado_NomePla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A853ContagemResultado_NomePla", A853ContagemResultado_NomePla);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A852ContagemResultado_Planilha)) )
               {
                  GXCCtlgxBlob = "CONTAGEMRESULTADO_PLANILHA" + "_gxBlob";
                  A852ContagemResultado_Planilha = cgiGet( GXCCtlgxBlob);
                  n852ContagemResultado_Planilha = false;
                  n852ContagemResultado_Planilha = (String.IsNullOrEmpty(StringUtil.RTrim( A852ContagemResultado_Planilha)) ? true : false);
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContagemResultadoContagens";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV20CalculoDivergencia, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A481ContagemResultado_TimeCnt, "99:99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A901ContagemResultadoContagens_Prazo, "99/99/99 99:99");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A517ContagemResultado_Ultima);
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A833ContagemResultado_CstUntPrd, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1756ContagemResultado_NvlCnt), "ZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) || ( A473ContagemResultado_DataCnt != Z473ContagemResultado_DataCnt ) || ( StringUtil.StrCmp(A511ContagemResultado_HoraCnt, Z511ContagemResultado_HoraCnt) != 0 ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[SecurityCheckFailed value for]"+"CalculoDivergencia:"+StringUtil.RTrim( context.localUtil.Format( AV20CalculoDivergencia, "")));
                  GXUtil.WriteLog("contagemresultadocontagens:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contagemresultadocontagens:[SecurityCheckFailed value for]"+"ContagemResultado_TimeCnt:"+context.localUtil.Format( A481ContagemResultado_TimeCnt, "99:99"));
                  GXUtil.WriteLog("contagemresultadocontagens:[SecurityCheckFailed value for]"+"ContagemResultadoContagens_Prazo:"+context.localUtil.Format( A901ContagemResultadoContagens_Prazo, "99/99/99 99:99"));
                  GXUtil.WriteLog("contagemresultadocontagens:[SecurityCheckFailed value for]"+"ContagemResultado_Ultima:"+StringUtil.BoolToStr( A517ContagemResultado_Ultima));
                  GXUtil.WriteLog("contagemresultadocontagens:[SecurityCheckFailed value for]"+"ContagemResultado_CstUntPrd:"+context.localUtil.Format( A833ContagemResultado_CstUntPrd, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
                  GXUtil.WriteLog("contagemresultadocontagens:[SecurityCheckFailed value for]"+"ContagemResultado_NvlCnt:"+context.localUtil.Format( (decimal)(A1756ContagemResultado_NvlCnt), "ZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A473ContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
                  A511ContagemResultado_HoraCnt = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
                  getEqualNoModal( ) ;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContagemResultado_HoraCnt)) )
                  {
                     A511ContagemResultado_HoraCnt = AV18ContagemResultado_HoraCnt;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
                  }
                  else
                  {
                     if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A511ContagemResultado_HoraCnt)) && ( Gx_BScreen == 0 ) )
                     {
                        A511ContagemResultado_HoraCnt = context.localUtil.Time( );
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
                     }
                  }
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode72 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContagemResultado_HoraCnt)) )
                     {
                        A511ContagemResultado_HoraCnt = AV18ContagemResultado_HoraCnt;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
                     }
                     else
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A511ContagemResultado_HoraCnt)) && ( Gx_BScreen == 0 ) )
                        {
                           A511ContagemResultado_HoraCnt = context.localUtil.Time( );
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
                        }
                     }
                     Gx_mode = sMode72;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound72 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1V0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTAGEMRESULTADO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111V2 */
                           E111V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121V2 */
                           E121V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "CONTAGEMRESULTADO_PFBFS.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E131V2 */
                           E131V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "CONTAGEMRESULTADO_PFLFS.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E141V2 */
                           E141V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "CONTAGEMRESULTADO_PFBFM.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E151V2 */
                           E151V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "CONTAGEMRESULTADO_PFLFM.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E161V2 */
                           E161V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121V2 */
            E121V2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1V72( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1V72( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_divergencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_divergencia_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1V0( )
      {
         BeforeValidate1V72( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1V72( ) ;
            }
            else
            {
               CheckExtendedTable1V72( ) ;
               CloseExtendedTableCursors1V72( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1V0( )
      {
      }

      protected void E111V2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV15WWPContext) ;
         AV12TrnContext.FromXml(AV14WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV12TrnContext.gxTpr_Transactionname, AV37Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV38GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38GXV1), 8, 0)));
            while ( AV38GXV1 <= AV12TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV12TrnContext.gxTpr_Attributes.Item(AV38GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContagemResultado_ContadorFMCod") == 0 )
               {
                  AV10Insert_ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Insert_ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Insert_ContagemResultado_ContadorFMCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContagemResultado_NaoCnfCntCod") == 0 )
               {
                  AV11Insert_ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContagemResultado_NaoCnfCntCod), 6, 0)));
               }
               AV38GXV1 = (int)(AV38GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38GXV1), 8, 0)));
            }
         }
         edtContagemResultado_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Visible), 5, 0)));
         dynContagemResultado_ContadorFMCod.Enabled = (AV15WWPContext.gxTpr_Userehadministradorgam||AV15WWPContext.gxTpr_Userehfinanceiro ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ContadorFMCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_ContadorFMCod.Enabled), 5, 0)));
         edtContagemResultado_Deflator_Visible = (AV15WWPContext.gxTpr_Userehadministradorgam||AV15WWPContext.gxTpr_Userehfinanceiro ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Deflator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Deflator_Visible), 5, 0)));
         edtContagemResultado_Planilha_Display = 1;
         AV35CalculoPFinal = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35CalculoPFinal", AV35CalculoPFinal);
         AV19IndiceDivergencia = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( AV19IndiceDivergencia, 6, 2)));
         AV27ContagemResultado_Divergencia = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV27ContagemResultado_Divergencia, 6, 2)));
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            new prc_getparmindicedivergenciadaos(context ).execute(  AV7ContagemResultado_Codigo, out  AV19IndiceDivergencia, out  AV20CalculoDivergencia, out  AV29ContagemResultado_ValorPF) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( AV19IndiceDivergencia, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CalculoDivergencia", AV20CalculoDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_ValorPF", StringUtil.LTrim( StringUtil.Str( AV29ContagemResultado_ValorPF, 18, 5)));
            /* Execute user subroutine: 'CALCULADIVERGENCIA' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E121V2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            context.wjLoc = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim("Contagens"));
            context.wjLocDisableFrm = 1;
         }
         else
         {
            if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
            {
               if ( A483ContagemResultado_StatusCnt != O483ContagemResultado_StatusCnt )
               {
                  new prc_contagemresultadoalterastatus(context ).execute( ref  A456ContagemResultado_Codigo,  ((A483ContagemResultado_StatusCnt==5) ? "R" : "A"),  0,  "", ref  AV34Confirmado) ;
               }
            }
            if ( ( A483ContagemResultado_StatusCnt == 7 ) && ( A890ContagemResultado_Responsavel > 0 ) )
            {
               new prc_nadivergenciaficacom(context ).execute(  A456ContagemResultado_Codigo,  A602ContagemResultado_OSVinculada,  A457ContagemResultado_Demanda,  A805ContagemResultado_ContratadaOrigemCod,  A601ContagemResultado_Servico,  A52Contratada_AreaTrabalhoCod,  AV15WWPContext.gxTpr_Userid,  A463ContagemResultado_ParecerTcn) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A805ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A601ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A463ContagemResultado_ParecerTcn", A463ContagemResultado_ParecerTcn);
            }
            new prc_disparoservicovinculado(context ).execute(  A456ContagemResultado_Codigo,  AV15WWPContext.gxTpr_Userid) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            if ( StringUtil.StringSearch( AV15WWPContext.gxTpr_Parametrossistema_nomesistema, "HOMOL", 1) > 0 )
            {
               new prc_putredminedata(context ).execute(  A52Contratada_AreaTrabalhoCod,  A1389ContagemResultado_RdmnIssueId,  3) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1389ContagemResultado_RdmnIssueId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1389ContagemResultado_RdmnIssueId), 6, 0)));
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV12TrnContext.gxTpr_Callerondelete )
            {
               context.wjLoc = formatLink("wwcontagemresultadocontagens.aspx") ;
               context.wjLocDisableFrm = 1;
            }
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E131V2( )
      {
         /* ContagemResultado_PFBFS_Isvalid Routine */
         /* Execute user subroutine: 'CALCULADIVERGENCIA' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E141V2( )
      {
         /* ContagemResultado_PFLFS_Isvalid Routine */
         /* Execute user subroutine: 'CALCULADIVERGENCIA' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E151V2( )
      {
         /* ContagemResultado_PFBFM_Isvalid Routine */
         /* Execute user subroutine: 'CALCULADIVERGENCIA' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E161V2( )
      {
         /* ContagemResultado_PFLFM_Isvalid Routine */
         /* Execute user subroutine: 'CALCULADIVERGENCIA' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'CALCULADIVERGENCIA' Routine */
         GXt_decimal1 = AV27ContagemResultado_Divergencia;
         new prc_calculardivergencia(context ).execute(  AV20CalculoDivergencia,  A458ContagemResultado_PFBFS,  A460ContagemResultado_PFBFM,  A459ContagemResultado_PFLFS,  A461ContagemResultado_PFLFM, out  GXt_decimal1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CalculoDivergencia", AV20CalculoDivergencia);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
         AV27ContagemResultado_Divergencia = GXt_decimal1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV27ContagemResultado_Divergencia, 6, 2)));
      }

      protected void ZM1V72( short GX_JID )
      {
         if ( ( GX_JID == 40 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z482ContagemResultadoContagens_Esforco = T001V3_A482ContagemResultadoContagens_Esforco[0];
               Z462ContagemResultado_Divergencia = T001V3_A462ContagemResultado_Divergencia[0];
               Z483ContagemResultado_StatusCnt = T001V3_A483ContagemResultado_StatusCnt[0];
               Z481ContagemResultado_TimeCnt = T001V3_A481ContagemResultado_TimeCnt[0];
               Z901ContagemResultadoContagens_Prazo = T001V3_A901ContagemResultadoContagens_Prazo[0];
               Z458ContagemResultado_PFBFS = T001V3_A458ContagemResultado_PFBFS[0];
               Z459ContagemResultado_PFLFS = T001V3_A459ContagemResultado_PFLFS[0];
               Z460ContagemResultado_PFBFM = T001V3_A460ContagemResultado_PFBFM[0];
               Z461ContagemResultado_PFLFM = T001V3_A461ContagemResultado_PFLFM[0];
               Z517ContagemResultado_Ultima = T001V3_A517ContagemResultado_Ultima[0];
               Z800ContagemResultado_Deflator = T001V3_A800ContagemResultado_Deflator[0];
               Z833ContagemResultado_CstUntPrd = T001V3_A833ContagemResultado_CstUntPrd[0];
               Z1756ContagemResultado_NvlCnt = T001V3_A1756ContagemResultado_NvlCnt[0];
               Z470ContagemResultado_ContadorFMCod = T001V3_A470ContagemResultado_ContadorFMCod[0];
               Z469ContagemResultado_NaoCnfCntCod = T001V3_A469ContagemResultado_NaoCnfCntCod[0];
            }
            else
            {
               Z482ContagemResultadoContagens_Esforco = A482ContagemResultadoContagens_Esforco;
               Z462ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
               Z483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
               Z481ContagemResultado_TimeCnt = A481ContagemResultado_TimeCnt;
               Z901ContagemResultadoContagens_Prazo = A901ContagemResultadoContagens_Prazo;
               Z458ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
               Z459ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
               Z460ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
               Z461ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
               Z517ContagemResultado_Ultima = A517ContagemResultado_Ultima;
               Z800ContagemResultado_Deflator = A800ContagemResultado_Deflator;
               Z833ContagemResultado_CstUntPrd = A833ContagemResultado_CstUntPrd;
               Z1756ContagemResultado_NvlCnt = A1756ContagemResultado_NvlCnt;
               Z470ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
               Z469ContagemResultado_NaoCnfCntCod = A469ContagemResultado_NaoCnfCntCod;
            }
         }
         if ( GX_JID == -40 )
         {
            Z473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            Z511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            Z482ContagemResultadoContagens_Esforco = A482ContagemResultadoContagens_Esforco;
            Z462ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
            Z483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
            Z481ContagemResultado_TimeCnt = A481ContagemResultado_TimeCnt;
            Z901ContagemResultadoContagens_Prazo = A901ContagemResultadoContagens_Prazo;
            Z458ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
            Z459ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
            Z460ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
            Z461ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
            Z463ContagemResultado_ParecerTcn = A463ContagemResultado_ParecerTcn;
            Z517ContagemResultado_Ultima = A517ContagemResultado_Ultima;
            Z800ContagemResultado_Deflator = A800ContagemResultado_Deflator;
            Z833ContagemResultado_CstUntPrd = A833ContagemResultado_CstUntPrd;
            Z852ContagemResultado_Planilha = A852ContagemResultado_Planilha;
            Z1756ContagemResultado_NvlCnt = A1756ContagemResultado_NvlCnt;
            Z854ContagemResultado_TipoPla = A854ContagemResultado_TipoPla;
            Z853ContagemResultado_NomePla = A853ContagemResultado_NomePla;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z470ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
            Z469ContagemResultado_NaoCnfCntCod = A469ContagemResultado_NaoCnfCntCod;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z485ContagemResultado_EhValidacao = A485ContagemResultado_EhValidacao;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z484ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
            Z1389ContagemResultado_RdmnIssueId = A1389ContagemResultado_RdmnIssueId;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z890ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
            Z468ContagemResultado_NaoCnfDmnCod = A468ContagemResultado_NaoCnfDmnCod;
            Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            Z805ContagemResultado_ContratadaOrigemCod = A805ContagemResultado_ContratadaOrigemCod;
            Z1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
            Z602ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z513Sistema_Coordenacao = A513Sistema_Coordenacao;
            Z52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z999ContagemResultado_CrFMEhContratada = A999ContagemResultado_CrFMEhContratada;
            Z1000ContagemResultado_CrFMEhContratante = A1000ContagemResultado_CrFMEhContratante;
            Z479ContagemResultado_CrFMPessoaCod = A479ContagemResultado_CrFMPessoaCod;
            Z474ContagemResultado_ContadorFMNom = A474ContagemResultado_ContadorFMNom;
            Z478ContagemResultado_NaoCnfCntNom = A478ContagemResultado_NaoCnfCntNom;
         }
      }

      protected void standaloneNotModal( )
      {
         GXACONTAGEMRESULTADO_NAOCNFCNTCOD_html1V72( ) ;
         edtContagemResultado_NomePla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_NomePla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_NomePla_Enabled), 5, 0)));
         edtContagemResultado_TipoPla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_TipoPla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_TipoPla_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV37Pgmname = "ContagemResultadoContagens";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Pgmname", AV37Pgmname);
         edtContagemResultado_NomePla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_NomePla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_NomePla_Enabled), 5, 0)));
         edtContagemResultado_TipoPla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_TipoPla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_TipoPla_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContagemResultado_Codigo) )
         {
            A456ContagemResultado_Codigo = AV7ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         }
         if ( ! (0==AV7ContagemResultado_Codigo) )
         {
            edtContagemResultado_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtContagemResultado_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV7ContagemResultado_Codigo) )
         {
            edtContagemResultado_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
         }
         if ( ! (DateTime.MinValue==AV8ContagemResultado_DataCnt) )
         {
            A473ContagemResultado_DataCnt = AV8ContagemResultado_DataCnt;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
         }
         if ( ! (DateTime.MinValue==AV8ContagemResultado_DataCnt) )
         {
            edtContagemResultado_DataCnt_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DataCnt_Enabled), 5, 0)));
         }
         else
         {
            edtContagemResultado_DataCnt_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DataCnt_Enabled), 5, 0)));
         }
         if ( ! (DateTime.MinValue==AV8ContagemResultado_DataCnt) )
         {
            edtContagemResultado_DataCnt_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DataCnt_Enabled), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContagemResultado_HoraCnt)) )
         {
            edtContagemResultado_HoraCnt_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_HoraCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_HoraCnt_Enabled), 5, 0)));
         }
         else
         {
            edtContagemResultado_HoraCnt_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_HoraCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_HoraCnt_Enabled), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContagemResultado_HoraCnt)) )
         {
            edtContagemResultado_HoraCnt_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_HoraCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_HoraCnt_Enabled), 5, 0)));
         }
         bttBtn_trn_delete_Visible = (AV15WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         dynContagemResultado_ContadorFMCod.Visible = (!AV15WWPContext.gxTpr_Userehcontratante ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ContadorFMCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_ContadorFMCod.Visible), 5, 0)));
         lblTextblockcontagemresultado_contadorfmcod_Visible = (!AV15WWPContext.gxTpr_Userehcontratante ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontagemresultado_contadorfmcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontagemresultado_contadorfmcod_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContagemResultado_NaoCnfCntCod) )
         {
            dynContagemResultado_NaoCnfCntCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_NaoCnfCntCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_NaoCnfCntCod.Enabled), 5, 0)));
         }
         else
         {
            dynContagemResultado_NaoCnfCntCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_NaoCnfCntCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_NaoCnfCntCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContagemResultado_NaoCnfCntCod) )
         {
            A469ContagemResultado_NaoCnfCntCod = AV11Insert_ContagemResultado_NaoCnfCntCod;
            n469ContagemResultado_NaoCnfCntCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A469ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV10Insert_ContagemResultado_ContadorFMCod) )
         {
            A470ContagemResultado_ContadorFMCod = AV10Insert_ContagemResultado_ContadorFMCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContagemResultado_HoraCnt)) )
         {
            A511ContagemResultado_HoraCnt = AV18ContagemResultado_HoraCnt;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A511ContagemResultado_HoraCnt)) && ( Gx_BScreen == 0 ) )
            {
               A511ContagemResultado_HoraCnt = context.localUtil.Time( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A482ContagemResultadoContagens_Esforco) && ( Gx_BScreen == 0 ) )
         {
            A482ContagemResultadoContagens_Esforco = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A482ContagemResultadoContagens_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A517ContagemResultado_Ultima) && ( Gx_BScreen == 0 ) )
         {
            A517ContagemResultado_Ultima = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A517ContagemResultado_Ultima", A517ContagemResultado_Ultima);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T001V6 */
            pr_default.execute(4, new Object[] {n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod});
            A478ContagemResultado_NaoCnfCntNom = T001V6_A478ContagemResultado_NaoCnfCntNom[0];
            n478ContagemResultado_NaoCnfCntNom = T001V6_n478ContagemResultado_NaoCnfCntNom[0];
            pr_default.close(4);
            /* Using cursor T001V4 */
            pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
            A146Modulo_Codigo = T001V4_A146Modulo_Codigo[0];
            n146Modulo_Codigo = T001V4_n146Modulo_Codigo[0];
            A485ContagemResultado_EhValidacao = T001V4_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = T001V4_n485ContagemResultado_EhValidacao[0];
            A457ContagemResultado_Demanda = T001V4_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T001V4_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = T001V4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = T001V4_n484ContagemResultado_StatusDmn[0];
            A1389ContagemResultado_RdmnIssueId = T001V4_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = T001V4_n1389ContagemResultado_RdmnIssueId[0];
            A493ContagemResultado_DemandaFM = T001V4_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T001V4_n493ContagemResultado_DemandaFM[0];
            A890ContagemResultado_Responsavel = T001V4_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = T001V4_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = T001V4_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = T001V4_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = T001V4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = T001V4_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = T001V4_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = T001V4_n805ContagemResultado_ContratadaOrigemCod[0];
            A1553ContagemResultado_CntSrvCod = T001V4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = T001V4_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = T001V4_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = T001V4_n602ContagemResultado_OSVinculada[0];
            pr_default.close(2);
            /* Using cursor T001V7 */
            pr_default.execute(5, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            A127Sistema_Codigo = T001V7_A127Sistema_Codigo[0];
            pr_default.close(5);
            /* Using cursor T001V8 */
            pr_default.execute(6, new Object[] {A127Sistema_Codigo});
            A513Sistema_Coordenacao = T001V8_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = T001V8_n513Sistema_Coordenacao[0];
            pr_default.close(6);
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
            /* Using cursor T001V9 */
            pr_default.execute(7, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            A52Contratada_AreaTrabalhoCod = T001V9_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = T001V9_n52Contratada_AreaTrabalhoCod[0];
            pr_default.close(7);
            GXACONTAGEMRESULTADO_CONTADORFMCOD_html1V72( A490ContagemResultado_ContratadaCod) ;
            /* Using cursor T001V10 */
            pr_default.execute(8, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
            A601ContagemResultado_Servico = T001V10_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = T001V10_n601ContagemResultado_Servico[0];
            pr_default.close(8);
            /* Using cursor T001V5 */
            pr_default.execute(3, new Object[] {A470ContagemResultado_ContadorFMCod});
            A999ContagemResultado_CrFMEhContratada = T001V5_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = T001V5_n999ContagemResultado_CrFMEhContratada[0];
            A1000ContagemResultado_CrFMEhContratante = T001V5_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = T001V5_n1000ContagemResultado_CrFMEhContratante[0];
            A479ContagemResultado_CrFMPessoaCod = T001V5_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = T001V5_n479ContagemResultado_CrFMPessoaCod[0];
            pr_default.close(3);
            /* Using cursor T001V11 */
            pr_default.execute(9, new Object[] {n479ContagemResultado_CrFMPessoaCod, A479ContagemResultado_CrFMPessoaCod});
            A474ContagemResultado_ContadorFMNom = T001V11_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = T001V11_n474ContagemResultado_ContadorFMNom[0];
            pr_default.close(9);
         }
      }

      protected void Load1V72( )
      {
         /* Using cursor T001V12 */
         pr_default.execute(10, new Object[] {A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound72 = 1;
            A146Modulo_Codigo = T001V12_A146Modulo_Codigo[0];
            n146Modulo_Codigo = T001V12_n146Modulo_Codigo[0];
            A127Sistema_Codigo = T001V12_A127Sistema_Codigo[0];
            A482ContagemResultadoContagens_Esforco = T001V12_A482ContagemResultadoContagens_Esforco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A482ContagemResultadoContagens_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0)));
            A462ContagemResultado_Divergencia = T001V12_A462ContagemResultado_Divergencia[0];
            A483ContagemResultado_StatusCnt = T001V12_A483ContagemResultado_StatusCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
            A485ContagemResultado_EhValidacao = T001V12_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = T001V12_n485ContagemResultado_EhValidacao[0];
            A513Sistema_Coordenacao = T001V12_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = T001V12_n513Sistema_Coordenacao[0];
            A457ContagemResultado_Demanda = T001V12_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T001V12_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = T001V12_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = T001V12_n484ContagemResultado_StatusDmn[0];
            A481ContagemResultado_TimeCnt = T001V12_A481ContagemResultado_TimeCnt[0];
            n481ContagemResultado_TimeCnt = T001V12_n481ContagemResultado_TimeCnt[0];
            A901ContagemResultadoContagens_Prazo = T001V12_A901ContagemResultadoContagens_Prazo[0];
            n901ContagemResultadoContagens_Prazo = T001V12_n901ContagemResultadoContagens_Prazo[0];
            A458ContagemResultado_PFBFS = T001V12_A458ContagemResultado_PFBFS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
            n458ContagemResultado_PFBFS = T001V12_n458ContagemResultado_PFBFS[0];
            A459ContagemResultado_PFLFS = T001V12_A459ContagemResultado_PFLFS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)));
            n459ContagemResultado_PFLFS = T001V12_n459ContagemResultado_PFLFS[0];
            A460ContagemResultado_PFBFM = T001V12_A460ContagemResultado_PFBFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
            n460ContagemResultado_PFBFM = T001V12_n460ContagemResultado_PFBFM[0];
            A461ContagemResultado_PFLFM = T001V12_A461ContagemResultado_PFLFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
            n461ContagemResultado_PFLFM = T001V12_n461ContagemResultado_PFLFM[0];
            A463ContagemResultado_ParecerTcn = T001V12_A463ContagemResultado_ParecerTcn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A463ContagemResultado_ParecerTcn", A463ContagemResultado_ParecerTcn);
            n463ContagemResultado_ParecerTcn = T001V12_n463ContagemResultado_ParecerTcn[0];
            A474ContagemResultado_ContadorFMNom = T001V12_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = T001V12_n474ContagemResultado_ContadorFMNom[0];
            A999ContagemResultado_CrFMEhContratada = T001V12_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = T001V12_n999ContagemResultado_CrFMEhContratada[0];
            A1000ContagemResultado_CrFMEhContratante = T001V12_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = T001V12_n1000ContagemResultado_CrFMEhContratante[0];
            A478ContagemResultado_NaoCnfCntNom = T001V12_A478ContagemResultado_NaoCnfCntNom[0];
            n478ContagemResultado_NaoCnfCntNom = T001V12_n478ContagemResultado_NaoCnfCntNom[0];
            A517ContagemResultado_Ultima = T001V12_A517ContagemResultado_Ultima[0];
            A800ContagemResultado_Deflator = T001V12_A800ContagemResultado_Deflator[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A800ContagemResultado_Deflator", StringUtil.LTrim( StringUtil.Str( A800ContagemResultado_Deflator, 6, 3)));
            n800ContagemResultado_Deflator = T001V12_n800ContagemResultado_Deflator[0];
            A833ContagemResultado_CstUntPrd = T001V12_A833ContagemResultado_CstUntPrd[0];
            n833ContagemResultado_CstUntPrd = T001V12_n833ContagemResultado_CstUntPrd[0];
            A1389ContagemResultado_RdmnIssueId = T001V12_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = T001V12_n1389ContagemResultado_RdmnIssueId[0];
            A1756ContagemResultado_NvlCnt = T001V12_A1756ContagemResultado_NvlCnt[0];
            n1756ContagemResultado_NvlCnt = T001V12_n1756ContagemResultado_NvlCnt[0];
            A854ContagemResultado_TipoPla = T001V12_A854ContagemResultado_TipoPla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A854ContagemResultado_TipoPla", A854ContagemResultado_TipoPla);
            n854ContagemResultado_TipoPla = T001V12_n854ContagemResultado_TipoPla[0];
            edtContagemResultado_Planilha_Filetype = A854ContagemResultado_TipoPla;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Planilha_Internalname, "Filetype", edtContagemResultado_Planilha_Filetype);
            A853ContagemResultado_NomePla = T001V12_A853ContagemResultado_NomePla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A853ContagemResultado_NomePla", A853ContagemResultado_NomePla);
            n853ContagemResultado_NomePla = T001V12_n853ContagemResultado_NomePla[0];
            edtContagemResultado_Planilha_Filename = A853ContagemResultado_NomePla;
            A493ContagemResultado_DemandaFM = T001V12_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T001V12_n493ContagemResultado_DemandaFM[0];
            A470ContagemResultado_ContadorFMCod = T001V12_A470ContagemResultado_ContadorFMCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
            A469ContagemResultado_NaoCnfCntCod = T001V12_A469ContagemResultado_NaoCnfCntCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A469ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0)));
            n469ContagemResultado_NaoCnfCntCod = T001V12_n469ContagemResultado_NaoCnfCntCod[0];
            A890ContagemResultado_Responsavel = T001V12_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = T001V12_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = T001V12_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = T001V12_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = T001V12_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = T001V12_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = T001V12_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = T001V12_n52Contratada_AreaTrabalhoCod[0];
            A805ContagemResultado_ContratadaOrigemCod = T001V12_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = T001V12_n805ContagemResultado_ContratadaOrigemCod[0];
            A1553ContagemResultado_CntSrvCod = T001V12_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = T001V12_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = T001V12_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = T001V12_n601ContagemResultado_Servico[0];
            A602ContagemResultado_OSVinculada = T001V12_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = T001V12_n602ContagemResultado_OSVinculada[0];
            A479ContagemResultado_CrFMPessoaCod = T001V12_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = T001V12_n479ContagemResultado_CrFMPessoaCod[0];
            A852ContagemResultado_Planilha = T001V12_A852ContagemResultado_Planilha[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A852ContagemResultado_Planilha", A852ContagemResultado_Planilha);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Planilha_Internalname, "URL", context.PathToRelativeUrl( A852ContagemResultado_Planilha));
            n852ContagemResultado_Planilha = T001V12_n852ContagemResultado_Planilha[0];
            ZM1V72( -40) ;
         }
         pr_default.close(10);
         OnLoadActions1V72( ) ;
      }

      protected void OnLoadActions1V72( )
      {
         if ( (Convert.ToDecimal(0)==AV27ContagemResultado_Divergencia) )
         {
            AV27ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV27ContagemResultado_Divergencia, 6, 2)));
         }
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         GXACONTAGEMRESULTADO_CONTADORFMCOD_html1V72( A490ContagemResultado_ContratadaCod) ;
         GXt_char2 = AV35CalculoPFinal;
         new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  A470ContagemResultado_ContadorFMCod, ref  A833ContagemResultado_CstUntPrd, out  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A833ContagemResultado_CstUntPrd", StringUtil.LTrim( StringUtil.Str( A833ContagemResultado_CstUntPrd, 18, 5)));
         AV35CalculoPFinal = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35CalculoPFinal", AV35CalculoPFinal);
         edtContagemResultado_PFBFM_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFBFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFBFM_Enabled), 5, 0)));
         edtContagemResultado_PFBFS_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFBFS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFBFS_Enabled), 5, 0)));
         edtContagemResultado_PFLFM_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFLFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFLFM_Enabled), 5, 0)));
         edtContagemResultado_PFLFS_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFLFS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFLFS_Enabled), 5, 0)));
      }

      protected void CheckExtendedTable1V72( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( (Convert.ToDecimal(0)==AV27ContagemResultado_Divergencia) )
         {
            AV27ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV27ContagemResultado_Divergencia, 6, 2)));
         }
         /* Using cursor T001V4 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A146Modulo_Codigo = T001V4_A146Modulo_Codigo[0];
         n146Modulo_Codigo = T001V4_n146Modulo_Codigo[0];
         A485ContagemResultado_EhValidacao = T001V4_A485ContagemResultado_EhValidacao[0];
         n485ContagemResultado_EhValidacao = T001V4_n485ContagemResultado_EhValidacao[0];
         A457ContagemResultado_Demanda = T001V4_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T001V4_n457ContagemResultado_Demanda[0];
         A484ContagemResultado_StatusDmn = T001V4_A484ContagemResultado_StatusDmn[0];
         n484ContagemResultado_StatusDmn = T001V4_n484ContagemResultado_StatusDmn[0];
         A1389ContagemResultado_RdmnIssueId = T001V4_A1389ContagemResultado_RdmnIssueId[0];
         n1389ContagemResultado_RdmnIssueId = T001V4_n1389ContagemResultado_RdmnIssueId[0];
         A493ContagemResultado_DemandaFM = T001V4_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T001V4_n493ContagemResultado_DemandaFM[0];
         A890ContagemResultado_Responsavel = T001V4_A890ContagemResultado_Responsavel[0];
         n890ContagemResultado_Responsavel = T001V4_n890ContagemResultado_Responsavel[0];
         A468ContagemResultado_NaoCnfDmnCod = T001V4_A468ContagemResultado_NaoCnfDmnCod[0];
         n468ContagemResultado_NaoCnfDmnCod = T001V4_n468ContagemResultado_NaoCnfDmnCod[0];
         A490ContagemResultado_ContratadaCod = T001V4_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T001V4_n490ContagemResultado_ContratadaCod[0];
         A805ContagemResultado_ContratadaOrigemCod = T001V4_A805ContagemResultado_ContratadaOrigemCod[0];
         n805ContagemResultado_ContratadaOrigemCod = T001V4_n805ContagemResultado_ContratadaOrigemCod[0];
         A1553ContagemResultado_CntSrvCod = T001V4_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = T001V4_n1553ContagemResultado_CntSrvCod[0];
         A602ContagemResultado_OSVinculada = T001V4_A602ContagemResultado_OSVinculada[0];
         n602ContagemResultado_OSVinculada = T001V4_n602ContagemResultado_OSVinculada[0];
         pr_default.close(2);
         /* Using cursor T001V7 */
         pr_default.execute(5, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A146Modulo_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A127Sistema_Codigo = T001V7_A127Sistema_Codigo[0];
         pr_default.close(5);
         /* Using cursor T001V8 */
         pr_default.execute(6, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A127Sistema_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A513Sistema_Coordenacao = T001V8_A513Sistema_Coordenacao[0];
         n513Sistema_Coordenacao = T001V8_n513Sistema_Coordenacao[0];
         pr_default.close(6);
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         /* Using cursor T001V9 */
         pr_default.execute(7, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A52Contratada_AreaTrabalhoCod = T001V9_A52Contratada_AreaTrabalhoCod[0];
         n52Contratada_AreaTrabalhoCod = T001V9_n52Contratada_AreaTrabalhoCod[0];
         pr_default.close(7);
         GXACONTAGEMRESULTADO_CONTADORFMCOD_html1V72( A490ContagemResultado_ContratadaCod) ;
         /* Using cursor T001V10 */
         pr_default.execute(8, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A601ContagemResultado_Servico = T001V10_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = T001V10_n601ContagemResultado_Servico[0];
         pr_default.close(8);
         GXt_char2 = AV35CalculoPFinal;
         new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  A470ContagemResultado_ContadorFMCod, ref  A833ContagemResultado_CstUntPrd, out  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A833ContagemResultado_CstUntPrd", StringUtil.LTrim( StringUtil.Str( A833ContagemResultado_CstUntPrd, 18, 5)));
         AV35CalculoPFinal = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35CalculoPFinal", AV35CalculoPFinal);
         if ( ! ( (DateTime.MinValue==A473ContagemResultado_DataCnt) || ( A473ContagemResultado_DataCnt >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADO_DATACNT");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_DataCnt_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T001V5 */
         pr_default.execute(3, new Object[] {A470ContagemResultado_ContadorFMCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Contador Fab. de M�t.'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTADORFMCOD");
            AnyError = 1;
            GX_FocusControl = dynContagemResultado_ContadorFMCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A999ContagemResultado_CrFMEhContratada = T001V5_A999ContagemResultado_CrFMEhContratada[0];
         n999ContagemResultado_CrFMEhContratada = T001V5_n999ContagemResultado_CrFMEhContratada[0];
         A1000ContagemResultado_CrFMEhContratante = T001V5_A1000ContagemResultado_CrFMEhContratante[0];
         n1000ContagemResultado_CrFMEhContratante = T001V5_n1000ContagemResultado_CrFMEhContratante[0];
         A479ContagemResultado_CrFMPessoaCod = T001V5_A479ContagemResultado_CrFMPessoaCod[0];
         n479ContagemResultado_CrFMPessoaCod = T001V5_n479ContagemResultado_CrFMPessoaCod[0];
         pr_default.close(3);
         /* Using cursor T001V11 */
         pr_default.execute(9, new Object[] {n479ContagemResultado_CrFMPessoaCod, A479ContagemResultado_CrFMPessoaCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A474ContagemResultado_ContadorFMNom = T001V11_A474ContagemResultado_ContadorFMNom[0];
         n474ContagemResultado_ContadorFMNom = T001V11_n474ContagemResultado_ContadorFMNom[0];
         pr_default.close(9);
         /* Using cursor T001V6 */
         pr_default.execute(4, new Object[] {n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A469ContagemResultado_NaoCnfCntCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Nao Conformidade da Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_NAOCNFCNTCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_NaoCnfCntCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A478ContagemResultado_NaoCnfCntNom = T001V6_A478ContagemResultado_NaoCnfCntNom[0];
         n478ContagemResultado_NaoCnfCntNom = T001V6_n478ContagemResultado_NaoCnfCntNom[0];
         pr_default.close(4);
         if ( ! ( ( A483ContagemResultado_StatusCnt == 1 ) || ( A483ContagemResultado_StatusCnt == 2 ) || ( A483ContagemResultado_StatusCnt == 3 ) || ( A483ContagemResultado_StatusCnt == 4 ) || ( A483ContagemResultado_StatusCnt == 5 ) || ( A483ContagemResultado_StatusCnt == 6 ) || ( A483ContagemResultado_StatusCnt == 7 ) || ( A483ContagemResultado_StatusCnt == 8 ) ) )
         {
            GX_msglist.addItem("Campo Status fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADO_STATUSCNT");
            AnyError = 1;
            GX_FocusControl = cmbContagemResultado_StatusCnt_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         edtContagemResultado_PFBFM_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFBFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFBFM_Enabled), 5, 0)));
         edtContagemResultado_PFBFS_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFBFS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFBFS_Enabled), 5, 0)));
         edtContagemResultado_PFLFM_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFLFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFLFM_Enabled), 5, 0)));
         edtContagemResultado_PFLFS_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFLFS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFLFS_Enabled), 5, 0)));
      }

      protected void CloseExtendedTableCursors1V72( )
      {
         pr_default.close(2);
         pr_default.close(5);
         pr_default.close(6);
         pr_default.close(7);
         pr_default.close(8);
         pr_default.close(3);
         pr_default.close(9);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_41( int A456ContagemResultado_Codigo )
      {
         /* Using cursor T001V13 */
         pr_default.execute(11, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A146Modulo_Codigo = T001V13_A146Modulo_Codigo[0];
         n146Modulo_Codigo = T001V13_n146Modulo_Codigo[0];
         A485ContagemResultado_EhValidacao = T001V13_A485ContagemResultado_EhValidacao[0];
         n485ContagemResultado_EhValidacao = T001V13_n485ContagemResultado_EhValidacao[0];
         A457ContagemResultado_Demanda = T001V13_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T001V13_n457ContagemResultado_Demanda[0];
         A484ContagemResultado_StatusDmn = T001V13_A484ContagemResultado_StatusDmn[0];
         n484ContagemResultado_StatusDmn = T001V13_n484ContagemResultado_StatusDmn[0];
         A1389ContagemResultado_RdmnIssueId = T001V13_A1389ContagemResultado_RdmnIssueId[0];
         n1389ContagemResultado_RdmnIssueId = T001V13_n1389ContagemResultado_RdmnIssueId[0];
         A493ContagemResultado_DemandaFM = T001V13_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T001V13_n493ContagemResultado_DemandaFM[0];
         A890ContagemResultado_Responsavel = T001V13_A890ContagemResultado_Responsavel[0];
         n890ContagemResultado_Responsavel = T001V13_n890ContagemResultado_Responsavel[0];
         A468ContagemResultado_NaoCnfDmnCod = T001V13_A468ContagemResultado_NaoCnfDmnCod[0];
         n468ContagemResultado_NaoCnfDmnCod = T001V13_n468ContagemResultado_NaoCnfDmnCod[0];
         A490ContagemResultado_ContratadaCod = T001V13_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T001V13_n490ContagemResultado_ContratadaCod[0];
         A805ContagemResultado_ContratadaOrigemCod = T001V13_A805ContagemResultado_ContratadaOrigemCod[0];
         n805ContagemResultado_ContratadaOrigemCod = T001V13_n805ContagemResultado_ContratadaOrigemCod[0];
         A1553ContagemResultado_CntSrvCod = T001V13_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = T001V13_n1553ContagemResultado_CntSrvCod[0];
         A602ContagemResultado_OSVinculada = T001V13_A602ContagemResultado_OSVinculada[0];
         n602ContagemResultado_OSVinculada = T001V13_n602ContagemResultado_OSVinculada[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A485ContagemResultado_EhValidacao))+"\""+","+"\""+GXUtil.EncodeJSConstant( A457ContagemResultado_Demanda)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A484ContagemResultado_StatusDmn))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1389ContagemResultado_RdmnIssueId), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( A493ContagemResultado_DemandaFM)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A890ContagemResultado_Responsavel), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_44( int A146Modulo_Codigo )
      {
         /* Using cursor T001V14 */
         pr_default.execute(12, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            if ( ! ( (0==A146Modulo_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A127Sistema_Codigo = T001V14_A127Sistema_Codigo[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void gxLoad_45( int A127Sistema_Codigo )
      {
         /* Using cursor T001V15 */
         pr_default.execute(13, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( (0==A127Sistema_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A513Sistema_Coordenacao = T001V15_A513Sistema_Coordenacao[0];
         n513Sistema_Coordenacao = T001V15_n513Sistema_Coordenacao[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A513Sistema_Coordenacao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(13) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(13);
      }

      protected void gxLoad_46( int A490ContagemResultado_ContratadaCod )
      {
         /* Using cursor T001V16 */
         pr_default.execute(14, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A52Contratada_AreaTrabalhoCod = T001V16_A52Contratada_AreaTrabalhoCod[0];
         n52Contratada_AreaTrabalhoCod = T001V16_n52Contratada_AreaTrabalhoCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(14);
      }

      protected void gxLoad_47( int A1553ContagemResultado_CntSrvCod )
      {
         /* Using cursor T001V17 */
         pr_default.execute(15, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A601ContagemResultado_Servico = T001V17_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = T001V17_n601ContagemResultado_Servico[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void gxLoad_42( int A470ContagemResultado_ContadorFMCod )
      {
         /* Using cursor T001V18 */
         pr_default.execute(16, new Object[] {A470ContagemResultado_ContadorFMCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Contador Fab. de M�t.'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTADORFMCOD");
            AnyError = 1;
            GX_FocusControl = dynContagemResultado_ContadorFMCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A999ContagemResultado_CrFMEhContratada = T001V18_A999ContagemResultado_CrFMEhContratada[0];
         n999ContagemResultado_CrFMEhContratada = T001V18_n999ContagemResultado_CrFMEhContratada[0];
         A1000ContagemResultado_CrFMEhContratante = T001V18_A1000ContagemResultado_CrFMEhContratante[0];
         n1000ContagemResultado_CrFMEhContratante = T001V18_n1000ContagemResultado_CrFMEhContratante[0];
         A479ContagemResultado_CrFMPessoaCod = T001V18_A479ContagemResultado_CrFMPessoaCod[0];
         n479ContagemResultado_CrFMPessoaCod = T001V18_n479ContagemResultado_CrFMPessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A999ContagemResultado_CrFMEhContratada))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A1000ContagemResultado_CrFMEhContratante))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A479ContagemResultado_CrFMPessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(16);
      }

      protected void gxLoad_48( int A479ContagemResultado_CrFMPessoaCod )
      {
         /* Using cursor T001V19 */
         pr_default.execute(17, new Object[] {n479ContagemResultado_CrFMPessoaCod, A479ContagemResultado_CrFMPessoaCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A474ContagemResultado_ContadorFMNom = T001V19_A474ContagemResultado_ContadorFMNom[0];
         n474ContagemResultado_ContadorFMNom = T001V19_n474ContagemResultado_ContadorFMNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A474ContagemResultado_ContadorFMNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(17) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(17);
      }

      protected void gxLoad_43( int A469ContagemResultado_NaoCnfCntCod )
      {
         /* Using cursor T001V20 */
         pr_default.execute(18, new Object[] {n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod});
         if ( (pr_default.getStatus(18) == 101) )
         {
            if ( ! ( (0==A469ContagemResultado_NaoCnfCntCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Nao Conformidade da Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_NAOCNFCNTCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_NaoCnfCntCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A478ContagemResultado_NaoCnfCntNom = T001V20_A478ContagemResultado_NaoCnfCntNom[0];
         n478ContagemResultado_NaoCnfCntNom = T001V20_n478ContagemResultado_NaoCnfCntNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A478ContagemResultado_NaoCnfCntNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(18) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(18);
      }

      protected void GetKey1V72( )
      {
         /* Using cursor T001V21 */
         pr_default.execute(19, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound72 = 1;
         }
         else
         {
            RcdFound72 = 0;
         }
         pr_default.close(19);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001V3 */
         pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1V72( 40) ;
            RcdFound72 = 1;
            A473ContagemResultado_DataCnt = T001V3_A473ContagemResultado_DataCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
            A511ContagemResultado_HoraCnt = T001V3_A511ContagemResultado_HoraCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
            A482ContagemResultadoContagens_Esforco = T001V3_A482ContagemResultadoContagens_Esforco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A482ContagemResultadoContagens_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0)));
            A462ContagemResultado_Divergencia = T001V3_A462ContagemResultado_Divergencia[0];
            A483ContagemResultado_StatusCnt = T001V3_A483ContagemResultado_StatusCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
            A481ContagemResultado_TimeCnt = T001V3_A481ContagemResultado_TimeCnt[0];
            n481ContagemResultado_TimeCnt = T001V3_n481ContagemResultado_TimeCnt[0];
            A901ContagemResultadoContagens_Prazo = T001V3_A901ContagemResultadoContagens_Prazo[0];
            n901ContagemResultadoContagens_Prazo = T001V3_n901ContagemResultadoContagens_Prazo[0];
            A458ContagemResultado_PFBFS = T001V3_A458ContagemResultado_PFBFS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
            n458ContagemResultado_PFBFS = T001V3_n458ContagemResultado_PFBFS[0];
            A459ContagemResultado_PFLFS = T001V3_A459ContagemResultado_PFLFS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)));
            n459ContagemResultado_PFLFS = T001V3_n459ContagemResultado_PFLFS[0];
            A460ContagemResultado_PFBFM = T001V3_A460ContagemResultado_PFBFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
            n460ContagemResultado_PFBFM = T001V3_n460ContagemResultado_PFBFM[0];
            A461ContagemResultado_PFLFM = T001V3_A461ContagemResultado_PFLFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
            n461ContagemResultado_PFLFM = T001V3_n461ContagemResultado_PFLFM[0];
            A463ContagemResultado_ParecerTcn = T001V3_A463ContagemResultado_ParecerTcn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A463ContagemResultado_ParecerTcn", A463ContagemResultado_ParecerTcn);
            n463ContagemResultado_ParecerTcn = T001V3_n463ContagemResultado_ParecerTcn[0];
            A517ContagemResultado_Ultima = T001V3_A517ContagemResultado_Ultima[0];
            A800ContagemResultado_Deflator = T001V3_A800ContagemResultado_Deflator[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A800ContagemResultado_Deflator", StringUtil.LTrim( StringUtil.Str( A800ContagemResultado_Deflator, 6, 3)));
            n800ContagemResultado_Deflator = T001V3_n800ContagemResultado_Deflator[0];
            A833ContagemResultado_CstUntPrd = T001V3_A833ContagemResultado_CstUntPrd[0];
            n833ContagemResultado_CstUntPrd = T001V3_n833ContagemResultado_CstUntPrd[0];
            A1756ContagemResultado_NvlCnt = T001V3_A1756ContagemResultado_NvlCnt[0];
            n1756ContagemResultado_NvlCnt = T001V3_n1756ContagemResultado_NvlCnt[0];
            A854ContagemResultado_TipoPla = T001V3_A854ContagemResultado_TipoPla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A854ContagemResultado_TipoPla", A854ContagemResultado_TipoPla);
            n854ContagemResultado_TipoPla = T001V3_n854ContagemResultado_TipoPla[0];
            edtContagemResultado_Planilha_Filetype = A854ContagemResultado_TipoPla;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Planilha_Internalname, "Filetype", edtContagemResultado_Planilha_Filetype);
            A853ContagemResultado_NomePla = T001V3_A853ContagemResultado_NomePla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A853ContagemResultado_NomePla", A853ContagemResultado_NomePla);
            n853ContagemResultado_NomePla = T001V3_n853ContagemResultado_NomePla[0];
            edtContagemResultado_Planilha_Filename = A853ContagemResultado_NomePla;
            A456ContagemResultado_Codigo = T001V3_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A470ContagemResultado_ContadorFMCod = T001V3_A470ContagemResultado_ContadorFMCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
            A469ContagemResultado_NaoCnfCntCod = T001V3_A469ContagemResultado_NaoCnfCntCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A469ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0)));
            n469ContagemResultado_NaoCnfCntCod = T001V3_n469ContagemResultado_NaoCnfCntCod[0];
            A852ContagemResultado_Planilha = T001V3_A852ContagemResultado_Planilha[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A852ContagemResultado_Planilha", A852ContagemResultado_Planilha);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Planilha_Internalname, "URL", context.PathToRelativeUrl( A852ContagemResultado_Planilha));
            n852ContagemResultado_Planilha = T001V3_n852ContagemResultado_Planilha[0];
            O483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            Z511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            sMode72 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1V72( ) ;
            if ( AnyError == 1 )
            {
               RcdFound72 = 0;
               InitializeNonKey1V72( ) ;
            }
            Gx_mode = sMode72;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound72 = 0;
            InitializeNonKey1V72( ) ;
            sMode72 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode72;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1V72( ) ;
         if ( RcdFound72 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound72 = 0;
         /* Using cursor T001V22 */
         pr_default.execute(20, new Object[] {A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(20) != 101) )
         {
            while ( (pr_default.getStatus(20) != 101) && ( ( T001V22_A473ContagemResultado_DataCnt[0] < A473ContagemResultado_DataCnt ) || ( T001V22_A473ContagemResultado_DataCnt[0] == A473ContagemResultado_DataCnt ) && ( StringUtil.StrCmp(T001V22_A511ContagemResultado_HoraCnt[0], A511ContagemResultado_HoraCnt) < 0 ) || ( StringUtil.StrCmp(T001V22_A511ContagemResultado_HoraCnt[0], A511ContagemResultado_HoraCnt) == 0 ) && ( T001V22_A473ContagemResultado_DataCnt[0] == A473ContagemResultado_DataCnt ) && ( T001V22_A456ContagemResultado_Codigo[0] < A456ContagemResultado_Codigo ) ) )
            {
               pr_default.readNext(20);
            }
            if ( (pr_default.getStatus(20) != 101) && ( ( T001V22_A473ContagemResultado_DataCnt[0] > A473ContagemResultado_DataCnt ) || ( T001V22_A473ContagemResultado_DataCnt[0] == A473ContagemResultado_DataCnt ) && ( StringUtil.StrCmp(T001V22_A511ContagemResultado_HoraCnt[0], A511ContagemResultado_HoraCnt) > 0 ) || ( StringUtil.StrCmp(T001V22_A511ContagemResultado_HoraCnt[0], A511ContagemResultado_HoraCnt) == 0 ) && ( T001V22_A473ContagemResultado_DataCnt[0] == A473ContagemResultado_DataCnt ) && ( T001V22_A456ContagemResultado_Codigo[0] > A456ContagemResultado_Codigo ) ) )
            {
               A473ContagemResultado_DataCnt = T001V22_A473ContagemResultado_DataCnt[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
               A511ContagemResultado_HoraCnt = T001V22_A511ContagemResultado_HoraCnt[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
               A456ContagemResultado_Codigo = T001V22_A456ContagemResultado_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               RcdFound72 = 1;
            }
         }
         pr_default.close(20);
      }

      protected void move_previous( )
      {
         RcdFound72 = 0;
         /* Using cursor T001V23 */
         pr_default.execute(21, new Object[] {A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(21) != 101) )
         {
            while ( (pr_default.getStatus(21) != 101) && ( ( T001V23_A473ContagemResultado_DataCnt[0] > A473ContagemResultado_DataCnt ) || ( T001V23_A473ContagemResultado_DataCnt[0] == A473ContagemResultado_DataCnt ) && ( StringUtil.StrCmp(T001V23_A511ContagemResultado_HoraCnt[0], A511ContagemResultado_HoraCnt) > 0 ) || ( StringUtil.StrCmp(T001V23_A511ContagemResultado_HoraCnt[0], A511ContagemResultado_HoraCnt) == 0 ) && ( T001V23_A473ContagemResultado_DataCnt[0] == A473ContagemResultado_DataCnt ) && ( T001V23_A456ContagemResultado_Codigo[0] > A456ContagemResultado_Codigo ) ) )
            {
               pr_default.readNext(21);
            }
            if ( (pr_default.getStatus(21) != 101) && ( ( T001V23_A473ContagemResultado_DataCnt[0] < A473ContagemResultado_DataCnt ) || ( T001V23_A473ContagemResultado_DataCnt[0] == A473ContagemResultado_DataCnt ) && ( StringUtil.StrCmp(T001V23_A511ContagemResultado_HoraCnt[0], A511ContagemResultado_HoraCnt) < 0 ) || ( StringUtil.StrCmp(T001V23_A511ContagemResultado_HoraCnt[0], A511ContagemResultado_HoraCnt) == 0 ) && ( T001V23_A473ContagemResultado_DataCnt[0] == A473ContagemResultado_DataCnt ) && ( T001V23_A456ContagemResultado_Codigo[0] < A456ContagemResultado_Codigo ) ) )
            {
               A473ContagemResultado_DataCnt = T001V23_A473ContagemResultado_DataCnt[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
               A511ContagemResultado_HoraCnt = T001V23_A511ContagemResultado_HoraCnt[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
               A456ContagemResultado_Codigo = T001V23_A456ContagemResultado_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               RcdFound72 = 1;
            }
         }
         pr_default.close(21);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1V72( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultado_DataCnt_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1V72( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound72 == 1 )
            {
               if ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) || ( A473ContagemResultado_DataCnt != Z473ContagemResultado_DataCnt ) || ( StringUtil.StrCmp(A511ContagemResultado_HoraCnt, Z511ContagemResultado_HoraCnt) != 0 ) )
               {
                  A456ContagemResultado_Codigo = Z456ContagemResultado_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A473ContagemResultado_DataCnt = Z473ContagemResultado_DataCnt;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
                  A511ContagemResultado_HoraCnt = Z511ContagemResultado_HoraCnt;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultado_DataCnt_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1V72( ) ;
                  GX_FocusControl = edtContagemResultado_DataCnt_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) || ( A473ContagemResultado_DataCnt != Z473ContagemResultado_DataCnt ) || ( StringUtil.StrCmp(A511ContagemResultado_HoraCnt, Z511ContagemResultado_HoraCnt) != 0 ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtContagemResultado_DataCnt_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1V72( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContagemResultado_DataCnt_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1V72( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) || ( A473ContagemResultado_DataCnt != Z473ContagemResultado_DataCnt ) || ( StringUtil.StrCmp(A511ContagemResultado_HoraCnt, Z511ContagemResultado_HoraCnt) != 0 ) )
         {
            A456ContagemResultado_Codigo = Z456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A473ContagemResultado_DataCnt = Z473ContagemResultado_DataCnt;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
            A511ContagemResultado_HoraCnt = Z511ContagemResultado_HoraCnt;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultado_DataCnt_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1V72( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001V2 */
            pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoContagens"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z482ContagemResultadoContagens_Esforco != T001V2_A482ContagemResultadoContagens_Esforco[0] ) || ( Z462ContagemResultado_Divergencia != T001V2_A462ContagemResultado_Divergencia[0] ) || ( Z483ContagemResultado_StatusCnt != T001V2_A483ContagemResultado_StatusCnt[0] ) || ( Z481ContagemResultado_TimeCnt != T001V2_A481ContagemResultado_TimeCnt[0] ) || ( Z901ContagemResultadoContagens_Prazo != T001V2_A901ContagemResultadoContagens_Prazo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z458ContagemResultado_PFBFS != T001V2_A458ContagemResultado_PFBFS[0] ) || ( Z459ContagemResultado_PFLFS != T001V2_A459ContagemResultado_PFLFS[0] ) || ( Z460ContagemResultado_PFBFM != T001V2_A460ContagemResultado_PFBFM[0] ) || ( Z461ContagemResultado_PFLFM != T001V2_A461ContagemResultado_PFLFM[0] ) || ( Z517ContagemResultado_Ultima != T001V2_A517ContagemResultado_Ultima[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z800ContagemResultado_Deflator != T001V2_A800ContagemResultado_Deflator[0] ) || ( Z833ContagemResultado_CstUntPrd != T001V2_A833ContagemResultado_CstUntPrd[0] ) || ( Z1756ContagemResultado_NvlCnt != T001V2_A1756ContagemResultado_NvlCnt[0] ) || ( Z470ContagemResultado_ContadorFMCod != T001V2_A470ContagemResultado_ContadorFMCod[0] ) || ( Z469ContagemResultado_NaoCnfCntCod != T001V2_A469ContagemResultado_NaoCnfCntCod[0] ) )
            {
               if ( Z482ContagemResultadoContagens_Esforco != T001V2_A482ContagemResultadoContagens_Esforco[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultadoContagens_Esforco");
                  GXUtil.WriteLogRaw("Old: ",Z482ContagemResultadoContagens_Esforco);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A482ContagemResultadoContagens_Esforco[0]);
               }
               if ( Z462ContagemResultado_Divergencia != T001V2_A462ContagemResultado_Divergencia[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_Divergencia");
                  GXUtil.WriteLogRaw("Old: ",Z462ContagemResultado_Divergencia);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A462ContagemResultado_Divergencia[0]);
               }
               if ( Z483ContagemResultado_StatusCnt != T001V2_A483ContagemResultado_StatusCnt[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_StatusCnt");
                  GXUtil.WriteLogRaw("Old: ",Z483ContagemResultado_StatusCnt);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A483ContagemResultado_StatusCnt[0]);
               }
               if ( Z481ContagemResultado_TimeCnt != T001V2_A481ContagemResultado_TimeCnt[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_TimeCnt");
                  GXUtil.WriteLogRaw("Old: ",Z481ContagemResultado_TimeCnt);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A481ContagemResultado_TimeCnt[0]);
               }
               if ( Z901ContagemResultadoContagens_Prazo != T001V2_A901ContagemResultadoContagens_Prazo[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultadoContagens_Prazo");
                  GXUtil.WriteLogRaw("Old: ",Z901ContagemResultadoContagens_Prazo);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A901ContagemResultadoContagens_Prazo[0]);
               }
               if ( Z458ContagemResultado_PFBFS != T001V2_A458ContagemResultado_PFBFS[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_PFBFS");
                  GXUtil.WriteLogRaw("Old: ",Z458ContagemResultado_PFBFS);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A458ContagemResultado_PFBFS[0]);
               }
               if ( Z459ContagemResultado_PFLFS != T001V2_A459ContagemResultado_PFLFS[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_PFLFS");
                  GXUtil.WriteLogRaw("Old: ",Z459ContagemResultado_PFLFS);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A459ContagemResultado_PFLFS[0]);
               }
               if ( Z460ContagemResultado_PFBFM != T001V2_A460ContagemResultado_PFBFM[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_PFBFM");
                  GXUtil.WriteLogRaw("Old: ",Z460ContagemResultado_PFBFM);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A460ContagemResultado_PFBFM[0]);
               }
               if ( Z461ContagemResultado_PFLFM != T001V2_A461ContagemResultado_PFLFM[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_PFLFM");
                  GXUtil.WriteLogRaw("Old: ",Z461ContagemResultado_PFLFM);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A461ContagemResultado_PFLFM[0]);
               }
               if ( Z517ContagemResultado_Ultima != T001V2_A517ContagemResultado_Ultima[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_Ultima");
                  GXUtil.WriteLogRaw("Old: ",Z517ContagemResultado_Ultima);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A517ContagemResultado_Ultima[0]);
               }
               if ( Z800ContagemResultado_Deflator != T001V2_A800ContagemResultado_Deflator[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_Deflator");
                  GXUtil.WriteLogRaw("Old: ",Z800ContagemResultado_Deflator);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A800ContagemResultado_Deflator[0]);
               }
               if ( Z833ContagemResultado_CstUntPrd != T001V2_A833ContagemResultado_CstUntPrd[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_CstUntPrd");
                  GXUtil.WriteLogRaw("Old: ",Z833ContagemResultado_CstUntPrd);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A833ContagemResultado_CstUntPrd[0]);
               }
               if ( Z1756ContagemResultado_NvlCnt != T001V2_A1756ContagemResultado_NvlCnt[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_NvlCnt");
                  GXUtil.WriteLogRaw("Old: ",Z1756ContagemResultado_NvlCnt);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A1756ContagemResultado_NvlCnt[0]);
               }
               if ( Z470ContagemResultado_ContadorFMCod != T001V2_A470ContagemResultado_ContadorFMCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_ContadorFMCod");
                  GXUtil.WriteLogRaw("Old: ",Z470ContagemResultado_ContadorFMCod);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A470ContagemResultado_ContadorFMCod[0]);
               }
               if ( Z469ContagemResultado_NaoCnfCntCod != T001V2_A469ContagemResultado_NaoCnfCntCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadocontagens:[seudo value changed for attri]"+"ContagemResultado_NaoCnfCntCod");
                  GXUtil.WriteLogRaw("Old: ",Z469ContagemResultado_NaoCnfCntCod);
                  GXUtil.WriteLogRaw("Current: ",T001V2_A469ContagemResultado_NaoCnfCntCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoContagens"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1V72( )
      {
         BeforeValidate1V72( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1V72( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1V72( 0) ;
            CheckOptimisticConcurrency1V72( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1V72( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1V72( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001V24 */
                     A854ContagemResultado_TipoPla = edtContagemResultado_Planilha_Filetype;
                     n854ContagemResultado_TipoPla = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A854ContagemResultado_TipoPla", A854ContagemResultado_TipoPla);
                     A853ContagemResultado_NomePla = edtContagemResultado_Planilha_Filename;
                     n853ContagemResultado_NomePla = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A853ContagemResultado_NomePla", A853ContagemResultado_NomePla);
                     pr_default.execute(22, new Object[] {A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A482ContagemResultadoContagens_Esforco, A462ContagemResultado_Divergencia, A483ContagemResultado_StatusCnt, n481ContagemResultado_TimeCnt, A481ContagemResultado_TimeCnt, n901ContagemResultadoContagens_Prazo, A901ContagemResultadoContagens_Prazo, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, n1756ContagemResultado_NvlCnt, A1756ContagemResultado_NvlCnt, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, A456ContagemResultado_Codigo, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod});
                     pr_default.close(22);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                     if ( (pr_default.getStatus(22) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        new prc_contagemresultado_ultima(context ).execute( ref  A456ContagemResultado_Codigo) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1V0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1V72( ) ;
            }
            EndLevel1V72( ) ;
         }
         CloseExtendedTableCursors1V72( ) ;
      }

      protected void Update1V72( )
      {
         BeforeValidate1V72( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1V72( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1V72( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1V72( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1V72( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001V25 */
                     A854ContagemResultado_TipoPla = edtContagemResultado_Planilha_Filetype;
                     n854ContagemResultado_TipoPla = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A854ContagemResultado_TipoPla", A854ContagemResultado_TipoPla);
                     A853ContagemResultado_NomePla = edtContagemResultado_Planilha_Filename;
                     n853ContagemResultado_NomePla = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A853ContagemResultado_NomePla", A853ContagemResultado_NomePla);
                     pr_default.execute(23, new Object[] {A482ContagemResultadoContagens_Esforco, A462ContagemResultado_Divergencia, A483ContagemResultado_StatusCnt, n481ContagemResultado_TimeCnt, A481ContagemResultado_TimeCnt, n901ContagemResultadoContagens_Prazo, A901ContagemResultadoContagens_Prazo, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n1756ContagemResultado_NvlCnt, A1756ContagemResultado_NvlCnt, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                     pr_default.close(23);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                     if ( (pr_default.getStatus(23) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoContagens"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1V72( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        new prc_contagemresultado_ultima(context ).execute( ref  A456ContagemResultado_Codigo) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1V72( ) ;
         }
         CloseExtendedTableCursors1V72( ) ;
      }

      protected void DeferredUpdate1V72( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T001V26 */
            pr_default.execute(24, new Object[] {n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(24);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate1V72( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1V72( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1V72( ) ;
            AfterConfirm1V72( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1V72( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001V27 */
                  pr_default.execute(25, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                  pr_default.close(25);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     new prc_contagemresultado_ultima(context ).execute( ref  A456ContagemResultado_Codigo) ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode72 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1V72( ) ;
         Gx_mode = sMode72;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1V72( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001V28 */
            pr_default.execute(26, new Object[] {A456ContagemResultado_Codigo});
            A146Modulo_Codigo = T001V28_A146Modulo_Codigo[0];
            n146Modulo_Codigo = T001V28_n146Modulo_Codigo[0];
            A485ContagemResultado_EhValidacao = T001V28_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = T001V28_n485ContagemResultado_EhValidacao[0];
            A457ContagemResultado_Demanda = T001V28_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T001V28_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = T001V28_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = T001V28_n484ContagemResultado_StatusDmn[0];
            A1389ContagemResultado_RdmnIssueId = T001V28_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = T001V28_n1389ContagemResultado_RdmnIssueId[0];
            A493ContagemResultado_DemandaFM = T001V28_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T001V28_n493ContagemResultado_DemandaFM[0];
            A890ContagemResultado_Responsavel = T001V28_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = T001V28_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = T001V28_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = T001V28_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = T001V28_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = T001V28_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = T001V28_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = T001V28_n805ContagemResultado_ContratadaOrigemCod[0];
            A1553ContagemResultado_CntSrvCod = T001V28_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = T001V28_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = T001V28_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = T001V28_n602ContagemResultado_OSVinculada[0];
            pr_default.close(26);
            /* Using cursor T001V29 */
            pr_default.execute(27, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            A127Sistema_Codigo = T001V29_A127Sistema_Codigo[0];
            pr_default.close(27);
            /* Using cursor T001V30 */
            pr_default.execute(28, new Object[] {A127Sistema_Codigo});
            A513Sistema_Coordenacao = T001V30_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = T001V30_n513Sistema_Coordenacao[0];
            pr_default.close(28);
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
            /* Using cursor T001V31 */
            pr_default.execute(29, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            A52Contratada_AreaTrabalhoCod = T001V31_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = T001V31_n52Contratada_AreaTrabalhoCod[0];
            pr_default.close(29);
            GXACONTAGEMRESULTADO_CONTADORFMCOD_html1V72( A490ContagemResultado_ContratadaCod) ;
            /* Using cursor T001V32 */
            pr_default.execute(30, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
            A601ContagemResultado_Servico = T001V32_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = T001V32_n601ContagemResultado_Servico[0];
            pr_default.close(30);
            /* Using cursor T001V33 */
            pr_default.execute(31, new Object[] {A470ContagemResultado_ContadorFMCod});
            A999ContagemResultado_CrFMEhContratada = T001V33_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = T001V33_n999ContagemResultado_CrFMEhContratada[0];
            A1000ContagemResultado_CrFMEhContratante = T001V33_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = T001V33_n1000ContagemResultado_CrFMEhContratante[0];
            A479ContagemResultado_CrFMPessoaCod = T001V33_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = T001V33_n479ContagemResultado_CrFMPessoaCod[0];
            pr_default.close(31);
            /* Using cursor T001V34 */
            pr_default.execute(32, new Object[] {n479ContagemResultado_CrFMPessoaCod, A479ContagemResultado_CrFMPessoaCod});
            A474ContagemResultado_ContadorFMNom = T001V34_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = T001V34_n474ContagemResultado_ContadorFMNom[0];
            pr_default.close(32);
            /* Using cursor T001V35 */
            pr_default.execute(33, new Object[] {n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod});
            A478ContagemResultado_NaoCnfCntNom = T001V35_A478ContagemResultado_NaoCnfCntNom[0];
            n478ContagemResultado_NaoCnfCntNom = T001V35_n478ContagemResultado_NaoCnfCntNom[0];
            pr_default.close(33);
            edtContagemResultado_PFBFM_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFBFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFBFM_Enabled), 5, 0)));
            edtContagemResultado_PFBFS_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFBFS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFBFS_Enabled), 5, 0)));
            edtContagemResultado_PFLFM_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFLFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFLFM_Enabled), 5, 0)));
            edtContagemResultado_PFLFS_Enabled = (((A483ContagemResultado_StatusCnt!=7)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFLFS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFLFS_Enabled), 5, 0)));
            if ( (Convert.ToDecimal(0)==AV27ContagemResultado_Divergencia) )
            {
               AV27ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV27ContagemResultado_Divergencia, 6, 2)));
            }
            GXt_char2 = AV35CalculoPFinal;
            new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  A470ContagemResultado_ContadorFMCod, ref  A833ContagemResultado_CstUntPrd, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A833ContagemResultado_CstUntPrd", StringUtil.LTrim( StringUtil.Str( A833ContagemResultado_CstUntPrd, 18, 5)));
            AV35CalculoPFinal = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35CalculoPFinal", AV35CalculoPFinal);
         }
      }

      protected void EndLevel1V72( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1V72( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(26);
            pr_default.close(31);
            pr_default.close(33);
            pr_default.close(27);
            pr_default.close(28);
            pr_default.close(29);
            pr_default.close(30);
            pr_default.close(32);
            context.CommitDataStores( "ContagemResultadoContagens");
            if ( AnyError == 0 )
            {
               ConfirmValues1V0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(26);
            pr_default.close(31);
            pr_default.close(33);
            pr_default.close(27);
            pr_default.close(28);
            pr_default.close(29);
            pr_default.close(30);
            pr_default.close(32);
            context.RollbackDataStores( "ContagemResultadoContagens");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1V72( )
      {
         /* Scan By routine */
         /* Using cursor T001V36 */
         pr_default.execute(34);
         RcdFound72 = 0;
         if ( (pr_default.getStatus(34) != 101) )
         {
            RcdFound72 = 1;
            A456ContagemResultado_Codigo = T001V36_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A473ContagemResultado_DataCnt = T001V36_A473ContagemResultado_DataCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
            A511ContagemResultado_HoraCnt = T001V36_A511ContagemResultado_HoraCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1V72( )
      {
         /* Scan next routine */
         pr_default.readNext(34);
         RcdFound72 = 0;
         if ( (pr_default.getStatus(34) != 101) )
         {
            RcdFound72 = 1;
            A456ContagemResultado_Codigo = T001V36_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A473ContagemResultado_DataCnt = T001V36_A473ContagemResultado_DataCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
            A511ContagemResultado_HoraCnt = T001V36_A511ContagemResultado_HoraCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
         }
      }

      protected void ScanEnd1V72( )
      {
         pr_default.close(34);
      }

      protected void AfterConfirm1V72( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1V72( )
      {
         /* Before Insert Rules */
         if ( (0==A469ContagemResultado_NaoCnfCntCod) )
         {
            A469ContagemResultado_NaoCnfCntCod = 0;
            n469ContagemResultado_NaoCnfCntCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A469ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0)));
            n469ContagemResultado_NaoCnfCntCod = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A469ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0)));
         }
      }

      protected void BeforeUpdate1V72( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1V72( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1V72( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1V72( )
      {
         /* Before Validate Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            GXt_decimal1 = AV27ContagemResultado_Divergencia;
            new prc_calculardivergencia(context ).execute(  AV20CalculoDivergencia,  A458ContagemResultado_PFBFS,  A460ContagemResultado_PFBFM,  A459ContagemResultado_PFLFS,  A461ContagemResultado_PFLFM, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CalculoDivergencia", AV20CalculoDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
            AV27ContagemResultado_Divergencia = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV27ContagemResultado_Divergencia, 6, 2)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            A462ContagemResultado_Divergencia = AV27ContagemResultado_Divergencia;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A462ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( A462ContagemResultado_Divergencia, 6, 2)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && (0==A482ContagemResultadoContagens_Esforco) )
         {
            A482ContagemResultadoContagens_Esforco = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A482ContagemResultadoContagens_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( A483ContagemResultado_StatusCnt == O483ContagemResultado_StatusCnt ) && ! (Convert.ToDecimal(0)==A458ContagemResultado_PFBFS) && ( A462ContagemResultado_Divergencia > AV19IndiceDivergencia ) )
         {
            A483ContagemResultado_StatusCnt = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( A483ContagemResultado_StatusCnt == O483ContagemResultado_StatusCnt ) && ! (0==A469ContagemResultado_NaoCnfCntCod) )
            {
               A483ContagemResultado_StatusCnt = 6;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( A483ContagemResultado_StatusCnt == O483ContagemResultado_StatusCnt ) && ! (Convert.ToDecimal(0)==A458ContagemResultado_PFBFS) && ( A462ContagemResultado_Divergencia <= AV19IndiceDivergencia ) )
               {
                  A483ContagemResultado_StatusCnt = 5;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
               }
            }
         }
      }

      protected void DisableAttributes1V72( )
      {
         edtContagemResultado_OsFsOsFm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_OsFsOsFm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_OsFsOsFm_Enabled), 5, 0)));
         edtContagemResultado_DataCnt_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DataCnt_Enabled), 5, 0)));
         edtContagemResultado_HoraCnt_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_HoraCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_HoraCnt_Enabled), 5, 0)));
         dynContagemResultado_ContadorFMCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ContadorFMCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_ContadorFMCod.Enabled), 5, 0)));
         edtContagemResultado_PFBFS_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFBFS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFBFS_Enabled), 5, 0)));
         edtContagemResultado_PFLFS_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFLFS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFLFS_Enabled), 5, 0)));
         edtContagemResultado_PFBFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFBFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFBFM_Enabled), 5, 0)));
         edtContagemResultado_PFLFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PFLFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PFLFM_Enabled), 5, 0)));
         edtContagemResultado_Deflator_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Deflator_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Deflator_Enabled), 5, 0)));
         edtContagemResultado_ParecerTcn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_ParecerTcn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_ParecerTcn_Enabled), 5, 0)));
         dynContagemResultado_NaoCnfCntCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_NaoCnfCntCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_NaoCnfCntCod.Enabled), 5, 0)));
         cmbContagemResultado_StatusCnt.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_StatusCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultado_StatusCnt.Enabled), 5, 0)));
         edtavContagemresultado_divergencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_divergencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_divergencia_Enabled), 5, 0)));
         edtContagemResultadoContagens_Esforco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoContagens_Esforco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoContagens_Esforco_Enabled), 5, 0)));
         edtContagemResultado_Planilha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Planilha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Planilha_Enabled), 5, 0)));
         edtContagemResultado_NomePla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_NomePla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_NomePla_Enabled), 5, 0)));
         edtContagemResultado_TipoPla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_TipoPla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_TipoPla_Enabled), 5, 0)));
         edtContagemResultado_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1V0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216163183");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadocontagens.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(AV18ContagemResultado_HoraCnt))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z473ContagemResultado_DataCnt", context.localUtil.DToC( Z473ContagemResultado_DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z511ContagemResultado_HoraCnt", StringUtil.RTrim( Z511ContagemResultado_HoraCnt));
         GxWebStd.gx_hidden_field( context, "Z482ContagemResultadoContagens_Esforco", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z482ContagemResultadoContagens_Esforco), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z462ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.NToC( Z462ContagemResultado_Divergencia, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z483ContagemResultado_StatusCnt), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z481ContagemResultado_TimeCnt", context.localUtil.TToC( Z481ContagemResultado_TimeCnt, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z901ContagemResultadoContagens_Prazo", context.localUtil.TToC( Z901ContagemResultadoContagens_Prazo, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.NToC( Z458ContagemResultado_PFBFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.NToC( Z459ContagemResultado_PFLFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.NToC( Z460ContagemResultado_PFBFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.NToC( Z461ContagemResultado_PFLFM, 14, 5, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z517ContagemResultado_Ultima", Z517ContagemResultado_Ultima);
         GxWebStd.gx_hidden_field( context, "Z800ContagemResultado_Deflator", StringUtil.LTrim( StringUtil.NToC( Z800ContagemResultado_Deflator, 6, 3, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z833ContagemResultado_CstUntPrd", StringUtil.LTrim( StringUtil.NToC( Z833ContagemResultado_CstUntPrd, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1756ContagemResultado_NvlCnt", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1756ContagemResultado_NvlCnt), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z470ContagemResultado_ContadorFMCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z469ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z469ContagemResultado_NaoCnfCntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.NToC( (decimal)(O483ContagemResultado_StatusCnt), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N469ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vCONFIRMADO", AV34Confirmado);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV15WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV15WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV12TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV12TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_DATACNT", context.localUtil.DToC( AV8ContagemResultado_DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_HORACNT", StringUtil.RTrim( AV18ContagemResultado_HoraCnt));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEMRESULTADO_CONTADORFMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10Insert_ContagemResultado_ContadorFMCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEMRESULTADO_NAOCNFCNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ContagemResultado_NaoCnfCntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( A462ContagemResultado_Divergencia, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCALCULODIVERGENCIA", StringUtil.RTrim( AV20CalculoDivergencia));
         GxWebStd.gx_hidden_field( context, "vINDICEDIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( AV19IndiceDivergencia, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CSTUNTPRD", StringUtil.LTrim( StringUtil.NToC( A833ContagemResultado_CstUntPrd, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCALCULOPFINAL", StringUtil.RTrim( AV35CalculoPFinal));
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_ULTIMA", A517ContagemResultado_Ultima);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_TIMECNT", context.localUtil.TToC( A481ContagemResultado_TimeCnt, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOCONTAGENS_PRAZO", context.localUtil.TToC( A901ContagemResultadoContagens_Prazo, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_NVLCNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1756ContagemResultado_NvlCnt), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "MODULO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_EHVALIDACAO", A485ContagemResultado_EhValidacao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_RDMNISSUEID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1389ContagemResultado_RdmnIssueId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A890ContagemResultado_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_NAOCNFDMNCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_CRFMEHCONTRATADA", A999ContagemResultado_CrFMEhContratada);
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_CRFMEHCONTRATANTE", A1000ContagemResultado_CrFMEhContratante);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CRFMPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A479ContagemResultado_CrFMPessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_NAOCNFCNTNOM", StringUtil.RTrim( A478ContagemResultado_NaoCnfCntNom));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_COORDENACAO", A513Sistema_Coordenacao);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTADORFMNOM", StringUtil.RTrim( A474ContagemResultado_ContadorFMNom));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV37Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV8ContagemResultado_DataCnt));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18ContagemResultado_HoraCnt, ""))));
         GXCCtlgxBlob = "CONTAGEMRESULTADO_PLANILHA" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A852ContagemResultado_Planilha);
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PLANILHA_Filetype", StringUtil.RTrim( edtContagemResultado_Planilha_Filetype));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PLANILHA_Filename", StringUtil.RTrim( edtContagemResultado_Planilha_Filename));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PLANILHA_Filename", StringUtil.RTrim( edtContagemResultado_Planilha_Filename));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PLANILHA_Filetype", StringUtil.RTrim( edtContagemResultado_Planilha_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContagemResultadoContagens";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV20CalculoDivergencia, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A481ContagemResultado_TimeCnt, "99:99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A901ContagemResultadoContagens_Prazo, "99/99/99 99:99");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A517ContagemResultado_Ultima);
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A833ContagemResultado_CstUntPrd, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1756ContagemResultado_NvlCnt), "ZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemresultadocontagens:[SendSecurityCheck value for]"+"CalculoDivergencia:"+StringUtil.RTrim( context.localUtil.Format( AV20CalculoDivergencia, "")));
         GXUtil.WriteLog("contagemresultadocontagens:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contagemresultadocontagens:[SendSecurityCheck value for]"+"ContagemResultado_TimeCnt:"+context.localUtil.Format( A481ContagemResultado_TimeCnt, "99:99"));
         GXUtil.WriteLog("contagemresultadocontagens:[SendSecurityCheck value for]"+"ContagemResultadoContagens_Prazo:"+context.localUtil.Format( A901ContagemResultadoContagens_Prazo, "99/99/99 99:99"));
         GXUtil.WriteLog("contagemresultadocontagens:[SendSecurityCheck value for]"+"ContagemResultado_Ultima:"+StringUtil.BoolToStr( A517ContagemResultado_Ultima));
         GXUtil.WriteLog("contagemresultadocontagens:[SendSecurityCheck value for]"+"ContagemResultado_CstUntPrd:"+context.localUtil.Format( A833ContagemResultado_CstUntPrd, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("contagemresultadocontagens:[SendSecurityCheck value for]"+"ContagemResultado_NvlCnt:"+context.localUtil.Format( (decimal)(A1756ContagemResultado_NvlCnt), "ZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadocontagens.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(AV18ContagemResultado_HoraCnt)) ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoContagens" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Contagens" ;
      }

      protected void InitializeNonKey1V72( )
      {
         A127Sistema_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         A146Modulo_Codigo = 0;
         n146Modulo_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
         A470ContagemResultado_ContadorFMCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
         A469ContagemResultado_NaoCnfCntCod = 0;
         n469ContagemResultado_NaoCnfCntCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A469ContagemResultado_NaoCnfCntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A469ContagemResultado_NaoCnfCntCod), 6, 0)));
         n469ContagemResultado_NaoCnfCntCod = ((0==A469ContagemResultado_NaoCnfCntCod) ? true : false);
         A462ContagemResultado_Divergencia = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A462ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( A462ContagemResultado_Divergencia, 6, 2)));
         A483ContagemResultado_StatusCnt = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
         A501ContagemResultado_OsFsOsFm = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         A485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A485ContagemResultado_EhValidacao", A485ContagemResultado_EhValidacao);
         A513Sistema_Coordenacao = "";
         n513Sistema_Coordenacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
         A457ContagemResultado_Demanda = "";
         n457ContagemResultado_Demanda = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
         A1553ContagemResultado_CntSrvCod = 0;
         n1553ContagemResultado_CntSrvCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
         A601ContagemResultado_Servico = 0;
         n601ContagemResultado_Servico = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A601ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0)));
         A490ContagemResultado_ContratadaCod = 0;
         n490ContagemResultado_ContratadaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
         A805ContagemResultado_ContratadaOrigemCod = 0;
         n805ContagemResultado_ContratadaOrigemCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A805ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0)));
         A52Contratada_AreaTrabalhoCod = 0;
         n52Contratada_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         A890ContagemResultado_Responsavel = 0;
         n890ContagemResultado_Responsavel = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
         A602ContagemResultado_OSVinculada = 0;
         n602ContagemResultado_OSVinculada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
         A468ContagemResultado_NaoCnfDmnCod = 0;
         n468ContagemResultado_NaoCnfDmnCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A468ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0)));
         A484ContagemResultado_StatusDmn = "";
         n484ContagemResultado_StatusDmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
         A481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         n481ContagemResultado_TimeCnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A481ContagemResultado_TimeCnt", context.localUtil.TToC( A481ContagemResultado_TimeCnt, 0, 5, 0, 3, "/", ":", " "));
         A901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         n901ContagemResultadoContagens_Prazo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A901ContagemResultadoContagens_Prazo", context.localUtil.TToC( A901ContagemResultadoContagens_Prazo, 8, 5, 0, 3, "/", ":", " "));
         A458ContagemResultado_PFBFS = 0;
         n458ContagemResultado_PFBFS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
         n458ContagemResultado_PFBFS = ((Convert.ToDecimal(0)==A458ContagemResultado_PFBFS) ? true : false);
         A459ContagemResultado_PFLFS = 0;
         n459ContagemResultado_PFLFS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)));
         n459ContagemResultado_PFLFS = ((Convert.ToDecimal(0)==A459ContagemResultado_PFLFS) ? true : false);
         A460ContagemResultado_PFBFM = 0;
         n460ContagemResultado_PFBFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
         n460ContagemResultado_PFBFM = ((Convert.ToDecimal(0)==A460ContagemResultado_PFBFM) ? true : false);
         A461ContagemResultado_PFLFM = 0;
         n461ContagemResultado_PFLFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
         n461ContagemResultado_PFLFM = ((Convert.ToDecimal(0)==A461ContagemResultado_PFLFM) ? true : false);
         A463ContagemResultado_ParecerTcn = "";
         n463ContagemResultado_ParecerTcn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A463ContagemResultado_ParecerTcn", A463ContagemResultado_ParecerTcn);
         n463ContagemResultado_ParecerTcn = (String.IsNullOrEmpty(StringUtil.RTrim( A463ContagemResultado_ParecerTcn)) ? true : false);
         A479ContagemResultado_CrFMPessoaCod = 0;
         n479ContagemResultado_CrFMPessoaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A479ContagemResultado_CrFMPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A479ContagemResultado_CrFMPessoaCod), 6, 0)));
         A474ContagemResultado_ContadorFMNom = "";
         n474ContagemResultado_ContadorFMNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A474ContagemResultado_ContadorFMNom", A474ContagemResultado_ContadorFMNom);
         A999ContagemResultado_CrFMEhContratada = false;
         n999ContagemResultado_CrFMEhContratada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A999ContagemResultado_CrFMEhContratada", A999ContagemResultado_CrFMEhContratada);
         A1000ContagemResultado_CrFMEhContratante = false;
         n1000ContagemResultado_CrFMEhContratante = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1000ContagemResultado_CrFMEhContratante", A1000ContagemResultado_CrFMEhContratante);
         A478ContagemResultado_NaoCnfCntNom = "";
         n478ContagemResultado_NaoCnfCntNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A478ContagemResultado_NaoCnfCntNom", A478ContagemResultado_NaoCnfCntNom);
         A800ContagemResultado_Deflator = 0;
         n800ContagemResultado_Deflator = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A800ContagemResultado_Deflator", StringUtil.LTrim( StringUtil.Str( A800ContagemResultado_Deflator, 6, 3)));
         n800ContagemResultado_Deflator = ((Convert.ToDecimal(0)==A800ContagemResultado_Deflator) ? true : false);
         A833ContagemResultado_CstUntPrd = 0;
         n833ContagemResultado_CstUntPrd = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A833ContagemResultado_CstUntPrd", StringUtil.LTrim( StringUtil.Str( A833ContagemResultado_CstUntPrd, 18, 5)));
         A852ContagemResultado_Planilha = "";
         n852ContagemResultado_Planilha = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A852ContagemResultado_Planilha", A852ContagemResultado_Planilha);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Planilha_Internalname, "URL", context.PathToRelativeUrl( A852ContagemResultado_Planilha));
         n852ContagemResultado_Planilha = (String.IsNullOrEmpty(StringUtil.RTrim( A852ContagemResultado_Planilha)) ? true : false);
         A1389ContagemResultado_RdmnIssueId = 0;
         n1389ContagemResultado_RdmnIssueId = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1389ContagemResultado_RdmnIssueId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1389ContagemResultado_RdmnIssueId), 6, 0)));
         A1756ContagemResultado_NvlCnt = 0;
         n1756ContagemResultado_NvlCnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1756ContagemResultado_NvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1756ContagemResultado_NvlCnt), 4, 0)));
         A854ContagemResultado_TipoPla = "";
         n854ContagemResultado_TipoPla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A854ContagemResultado_TipoPla", A854ContagemResultado_TipoPla);
         n854ContagemResultado_TipoPla = (String.IsNullOrEmpty(StringUtil.RTrim( A854ContagemResultado_TipoPla)) ? true : false);
         A853ContagemResultado_NomePla = "";
         n853ContagemResultado_NomePla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A853ContagemResultado_NomePla", A853ContagemResultado_NomePla);
         n853ContagemResultado_NomePla = (String.IsNullOrEmpty(StringUtil.RTrim( A853ContagemResultado_NomePla)) ? true : false);
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
         A482ContagemResultadoContagens_Esforco = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A482ContagemResultadoContagens_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0)));
         A517ContagemResultado_Ultima = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A517ContagemResultado_Ultima", A517ContagemResultado_Ultima);
         O483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
         Z482ContagemResultadoContagens_Esforco = 0;
         Z462ContagemResultado_Divergencia = 0;
         Z483ContagemResultado_StatusCnt = 0;
         Z481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         Z901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         Z458ContagemResultado_PFBFS = 0;
         Z459ContagemResultado_PFLFS = 0;
         Z460ContagemResultado_PFBFM = 0;
         Z461ContagemResultado_PFLFM = 0;
         Z517ContagemResultado_Ultima = false;
         Z800ContagemResultado_Deflator = 0;
         Z833ContagemResultado_CstUntPrd = 0;
         Z1756ContagemResultado_NvlCnt = 0;
         Z470ContagemResultado_ContadorFMCod = 0;
         Z469ContagemResultado_NaoCnfCntCod = 0;
      }

      protected void InitAll1V72( )
      {
         A456ContagemResultado_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
         A511ContagemResultado_HoraCnt = context.localUtil.Time( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
         InitializeNonKey1V72( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A482ContagemResultadoContagens_Esforco = i482ContagemResultadoContagens_Esforco;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A482ContagemResultadoContagens_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0)));
         A517ContagemResultado_Ultima = i517ContagemResultado_Ultima;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A517ContagemResultado_Ultima", A517ContagemResultado_Ultima);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216163249");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadocontagens.js", "?20206216163249");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultado_osfsosfm_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_OSFSOSFM";
         edtContagemResultado_OsFsOsFm_Internalname = "CONTAGEMRESULTADO_OSFSOSFM";
         lblTextblockcontagemresultado_datacnt_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATACNT";
         edtContagemResultado_DataCnt_Internalname = "CONTAGEMRESULTADO_DATACNT";
         edtContagemResultado_HoraCnt_Internalname = "CONTAGEMRESULTADO_HORACNT";
         tblTablemergedcontagemresultado_datacnt_Internalname = "TABLEMERGEDCONTAGEMRESULTADO_DATACNT";
         lblTextblockcontagemresultado_contadorfmcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CONTADORFMCOD";
         dynContagemResultado_ContadorFMCod_Internalname = "CONTAGEMRESULTADO_CONTADORFMCOD";
         lblTextblockcontagemresultado_pfbfs_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFS";
         edtContagemResultado_PFBFS_Internalname = "CONTAGEMRESULTADO_PFBFS";
         lblTextblockcontagemresultado_pflfs_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFS";
         edtContagemResultado_PFLFS_Internalname = "CONTAGEMRESULTADO_PFLFS";
         lblTextblockcontagemresultado_pfbfm_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFM";
         edtContagemResultado_PFBFM_Internalname = "CONTAGEMRESULTADO_PFBFM";
         lblTextblockcontagemresultado_pflfm_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFM";
         edtContagemResultado_PFLFM_Internalname = "CONTAGEMRESULTADO_PFLFM";
         lblTextblockcontagemresultado_deflator_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DEFLATOR";
         edtContagemResultado_Deflator_Internalname = "CONTAGEMRESULTADO_DEFLATOR";
         lblTextblockcontagemresultado_parecertcn_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PARECERTCN";
         edtContagemResultado_ParecerTcn_Internalname = "CONTAGEMRESULTADO_PARECERTCN";
         lblTextblockcontagemresultado_naocnfcntcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_NAOCNFCNTCOD";
         dynContagemResultado_NaoCnfCntCod_Internalname = "CONTAGEMRESULTADO_NAOCNFCNTCOD";
         lblTextblockcontagemresultado_statuscnt_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_STATUSCNT";
         cmbContagemResultado_StatusCnt_Internalname = "CONTAGEMRESULTADO_STATUSCNT";
         lblTextblockcontagemresultado_divergencia_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DIVERGENCIA";
         edtavContagemresultado_divergencia_Internalname = "vCONTAGEMRESULTADO_DIVERGENCIA";
         lblContagemresultado_divergencia_righttext_Internalname = "CONTAGEMRESULTADO_DIVERGENCIA_RIGHTTEXT";
         lblTextblockcontagemresultadocontagens_esforco_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCONTAGENS_ESFORCO";
         edtContagemResultadoContagens_Esforco_Internalname = "CONTAGEMRESULTADOCONTAGENS_ESFORCO";
         lblContagemresultadocontagens_esforco_righttext_Internalname = "CONTAGEMRESULTADOCONTAGENS_ESFORCO_RIGHTTEXT";
         cellContagemresultadocontagens_esforco_righttext_cell_Internalname = "CONTAGEMRESULTADOCONTAGENS_ESFORCO_RIGHTTEXT_CELL";
         tblTablemergedcontagemresultado_divergencia_Internalname = "TABLEMERGEDCONTAGEMRESULTADO_DIVERGENCIA";
         lblTextblockcontagemresultado_planilha_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PLANILHA";
         edtContagemResultado_Planilha_Internalname = "CONTAGEMRESULTADO_PLANILHA";
         lblTextblockcontagemresultado_nomepla_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_NOMEPLA";
         edtContagemResultado_NomePla_Internalname = "CONTAGEMRESULTADO_NOMEPLA";
         lblTextblockcontagemresultado_tipopla_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_TIPOPLA";
         edtContagemResultado_TipoPla_Internalname = "CONTAGEMRESULTADO_TIPOPLA";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Resultado da Contagem";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado Contagens";
         edtContagemResultado_Planilha_Filename = "";
         edtContagemResultado_HoraCnt_Jsonclick = "";
         edtContagemResultado_HoraCnt_Enabled = 1;
         edtContagemResultado_DataCnt_Jsonclick = "";
         edtContagemResultado_DataCnt_Enabled = 1;
         edtContagemResultadoContagens_Esforco_Jsonclick = "";
         edtContagemResultadoContagens_Esforco_Enabled = 1;
         edtavContagemresultado_divergencia_Jsonclick = "";
         edtavContagemresultado_divergencia_Enabled = 0;
         edtContagemResultado_TipoPla_Jsonclick = "";
         edtContagemResultado_TipoPla_Enabled = 0;
         edtContagemResultado_NomePla_Jsonclick = "";
         edtContagemResultado_NomePla_Enabled = 0;
         edtContagemResultado_Planilha_Jsonclick = "";
         edtContagemResultado_Planilha_Parameters = "";
         edtContagemResultado_Planilha_Contenttype = "";
         edtContagemResultado_Planilha_Filetype = "";
         edtContagemResultado_Planilha_Display = 0;
         edtContagemResultado_Planilha_Enabled = 1;
         cmbContagemResultado_StatusCnt_Jsonclick = "";
         cmbContagemResultado_StatusCnt.Enabled = 1;
         dynContagemResultado_NaoCnfCntCod_Jsonclick = "";
         dynContagemResultado_NaoCnfCntCod.Enabled = 1;
         edtContagemResultado_ParecerTcn_Enabled = 1;
         edtContagemResultado_Deflator_Jsonclick = "";
         edtContagemResultado_Deflator_Enabled = 1;
         edtContagemResultado_Deflator_Visible = 1;
         edtContagemResultado_PFLFM_Jsonclick = "";
         edtContagemResultado_PFLFM_Enabled = 1;
         edtContagemResultado_PFBFM_Jsonclick = "";
         edtContagemResultado_PFBFM_Enabled = 1;
         edtContagemResultado_PFLFS_Jsonclick = "";
         edtContagemResultado_PFLFS_Enabled = 1;
         edtContagemResultado_PFBFS_Jsonclick = "";
         edtContagemResultado_PFBFS_Enabled = 1;
         dynContagemResultado_ContadorFMCod_Jsonclick = "";
         dynContagemResultado_ContadorFMCod.Enabled = 1;
         dynContagemResultado_ContadorFMCod.Visible = 1;
         lblTextblockcontagemresultado_contadorfmcod_Visible = 1;
         edtContagemResultado_OsFsOsFm_Jsonclick = "";
         edtContagemResultado_OsFsOsFm_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContagemResultado_Codigo_Jsonclick = "";
         edtContagemResultado_Codigo_Enabled = 1;
         edtContagemResultado_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         GXACONTAGEMRESULTADO_CONTADORFMCOD_html1V72( A490ContagemResultado_ContratadaCod) ;
         /* End function dynload_actions */
      }

      protected void GXDLACONTAGEMRESULTADO_CONTADORFMCOD1V72( int A490ContagemResultado_ContratadaCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADO_CONTADORFMCOD_data1V72( A490ContagemResultado_ContratadaCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADO_CONTADORFMCOD_html1V72( int A490ContagemResultado_ContratadaCod )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADO_CONTADORFMCOD_data1V72( A490ContagemResultado_ContratadaCod) ;
         gxdynajaxindex = 1;
         dynContagemResultado_ContadorFMCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultado_ContadorFMCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADO_CONTADORFMCOD_data1V72( int A490ContagemResultado_ContratadaCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T001V37 */
         pr_default.execute(35, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         while ( (pr_default.getStatus(35) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001V37_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T001V37_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(35);
         }
         pr_default.close(35);
      }

      protected void GXDLACONTAGEMRESULTADO_NAOCNFCNTCOD1V72( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADO_NAOCNFCNTCOD_data1V72( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADO_NAOCNFCNTCOD_html1V72( )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADO_NAOCNFCNTCOD_data1V72( ) ;
         gxdynajaxindex = 1;
         dynContagemResultado_NaoCnfCntCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultado_NaoCnfCntCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADO_NAOCNFCNTCOD_data1V72( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor T001V38 */
         pr_default.execute(36);
         while ( (pr_default.getStatus(36) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001V38_A469ContagemResultado_NaoCnfCntCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T001V38_A478ContagemResultado_NaoCnfCntNom[0]));
            pr_default.readNext(36);
         }
         pr_default.close(36);
      }

      protected void GX27ASACONTAGEMRESULTADO_DIVERGENCIA1V72( decimal A462ContagemResultado_Divergencia ,
                                                               decimal AV27ContagemResultado_Divergencia )
      {
         if ( (Convert.ToDecimal(0)==AV27ContagemResultado_Divergencia) )
         {
            AV27ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV27ContagemResultado_Divergencia, 6, 2)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( AV27ContagemResultado_Divergencia, 6, 2, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX28ASACONTAGEMRESULTADO_DIVERGENCIA1V72( String Gx_mode ,
                                                               String AV20CalculoDivergencia ,
                                                               decimal A458ContagemResultado_PFBFS ,
                                                               decimal A460ContagemResultado_PFBFM ,
                                                               decimal A459ContagemResultado_PFLFS ,
                                                               decimal A461ContagemResultado_PFLFM )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            GXt_decimal1 = AV27ContagemResultado_Divergencia;
            new prc_calculardivergencia(context ).execute(  AV20CalculoDivergencia,  A458ContagemResultado_PFBFS,  A460ContagemResultado_PFBFM,  A459ContagemResultado_PFLFS,  A461ContagemResultado_PFLFM, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CalculoDivergencia", AV20CalculoDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
            AV27ContagemResultado_Divergencia = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( AV27ContagemResultado_Divergencia, 6, 2)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( AV27ContagemResultado_Divergencia, 6, 2, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX31ASACALCULOPFINAL1V72( int A1553ContagemResultado_CntSrvCod ,
                                               int A470ContagemResultado_ContadorFMCod ,
                                               decimal A833ContagemResultado_CstUntPrd )
      {
         GXt_char2 = AV35CalculoPFinal;
         new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  A470ContagemResultado_ContadorFMCod, ref  A833ContagemResultado_CstUntPrd, out  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A833ContagemResultado_CstUntPrd", StringUtil.LTrim( StringUtil.Str( A833ContagemResultado_CstUntPrd, 18, 5)));
         AV35CalculoPFinal = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35CalculoPFinal", AV35CalculoPFinal);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( AV35CalculoPFinal))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_39_1V72( int A456ContagemResultado_Codigo )
      {
         new prc_contagemresultado_ultima(context ).execute( ref  A456ContagemResultado_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Contagemresultado_codigo( int GX_Parm1 ,
                                                  int GX_Parm2 ,
                                                  int GX_Parm3 ,
                                                  String GX_Parm4 ,
                                                  String GX_Parm5 ,
                                                  int GX_Parm6 ,
                                                  int GX_Parm7 ,
                                                  bool GX_Parm8 ,
                                                  String GX_Parm9 ,
                                                  int GX_Parm10 ,
                                                  int GX_Parm11 ,
                                                  int GX_Parm12 ,
                                                  int GX_Parm13 ,
                                                  int GX_Parm14 ,
                                                  String GX_Parm15 ,
                                                  String GX_Parm16 ,
                                                  int GX_Parm17 ,
                                                  GXCombobox dynGX_Parm18 ,
                                                  int GX_Parm19 )
      {
         A456ContagemResultado_Codigo = GX_Parm1;
         A146Modulo_Codigo = GX_Parm2;
         n146Modulo_Codigo = false;
         A127Sistema_Codigo = GX_Parm3;
         A457ContagemResultado_Demanda = GX_Parm4;
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = GX_Parm5;
         n493ContagemResultado_DemandaFM = false;
         A490ContagemResultado_ContratadaCod = GX_Parm6;
         n490ContagemResultado_ContratadaCod = false;
         A1553ContagemResultado_CntSrvCod = GX_Parm7;
         n1553ContagemResultado_CntSrvCod = false;
         A485ContagemResultado_EhValidacao = GX_Parm8;
         n485ContagemResultado_EhValidacao = false;
         A484ContagemResultado_StatusDmn = GX_Parm9;
         n484ContagemResultado_StatusDmn = false;
         A1389ContagemResultado_RdmnIssueId = GX_Parm10;
         n1389ContagemResultado_RdmnIssueId = false;
         A890ContagemResultado_Responsavel = GX_Parm11;
         n890ContagemResultado_Responsavel = false;
         A468ContagemResultado_NaoCnfDmnCod = GX_Parm12;
         n468ContagemResultado_NaoCnfDmnCod = false;
         A805ContagemResultado_ContratadaOrigemCod = GX_Parm13;
         n805ContagemResultado_ContratadaOrigemCod = false;
         A602ContagemResultado_OSVinculada = GX_Parm14;
         n602ContagemResultado_OSVinculada = false;
         A513Sistema_Coordenacao = GX_Parm15;
         n513Sistema_Coordenacao = false;
         A501ContagemResultado_OsFsOsFm = GX_Parm16;
         A52Contratada_AreaTrabalhoCod = GX_Parm17;
         n52Contratada_AreaTrabalhoCod = false;
         dynContagemResultado_ContadorFMCod = dynGX_Parm18;
         A470ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynContagemResultado_ContadorFMCod.CurrentValue, "."));
         A601ContagemResultado_Servico = GX_Parm19;
         n601ContagemResultado_Servico = false;
         /* Using cursor T001V39 */
         pr_default.execute(37, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(37) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
         }
         A146Modulo_Codigo = T001V39_A146Modulo_Codigo[0];
         n146Modulo_Codigo = T001V39_n146Modulo_Codigo[0];
         A485ContagemResultado_EhValidacao = T001V39_A485ContagemResultado_EhValidacao[0];
         n485ContagemResultado_EhValidacao = T001V39_n485ContagemResultado_EhValidacao[0];
         A457ContagemResultado_Demanda = T001V39_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T001V39_n457ContagemResultado_Demanda[0];
         A484ContagemResultado_StatusDmn = T001V39_A484ContagemResultado_StatusDmn[0];
         n484ContagemResultado_StatusDmn = T001V39_n484ContagemResultado_StatusDmn[0];
         A1389ContagemResultado_RdmnIssueId = T001V39_A1389ContagemResultado_RdmnIssueId[0];
         n1389ContagemResultado_RdmnIssueId = T001V39_n1389ContagemResultado_RdmnIssueId[0];
         A493ContagemResultado_DemandaFM = T001V39_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T001V39_n493ContagemResultado_DemandaFM[0];
         A890ContagemResultado_Responsavel = T001V39_A890ContagemResultado_Responsavel[0];
         n890ContagemResultado_Responsavel = T001V39_n890ContagemResultado_Responsavel[0];
         A468ContagemResultado_NaoCnfDmnCod = T001V39_A468ContagemResultado_NaoCnfDmnCod[0];
         n468ContagemResultado_NaoCnfDmnCod = T001V39_n468ContagemResultado_NaoCnfDmnCod[0];
         A490ContagemResultado_ContratadaCod = T001V39_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T001V39_n490ContagemResultado_ContratadaCod[0];
         A805ContagemResultado_ContratadaOrigemCod = T001V39_A805ContagemResultado_ContratadaOrigemCod[0];
         n805ContagemResultado_ContratadaOrigemCod = T001V39_n805ContagemResultado_ContratadaOrigemCod[0];
         A1553ContagemResultado_CntSrvCod = T001V39_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = T001V39_n1553ContagemResultado_CntSrvCod[0];
         A602ContagemResultado_OSVinculada = T001V39_A602ContagemResultado_OSVinculada[0];
         n602ContagemResultado_OSVinculada = T001V39_n602ContagemResultado_OSVinculada[0];
         pr_default.close(37);
         /* Using cursor T001V40 */
         pr_default.execute(38, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(38) == 101) )
         {
            if ( ! ( (0==A146Modulo_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A127Sistema_Codigo = T001V40_A127Sistema_Codigo[0];
         pr_default.close(38);
         /* Using cursor T001V41 */
         pr_default.execute(39, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(39) == 101) )
         {
            if ( ! ( (0==A127Sistema_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A513Sistema_Coordenacao = T001V41_A513Sistema_Coordenacao[0];
         n513Sistema_Coordenacao = T001V41_n513Sistema_Coordenacao[0];
         pr_default.close(39);
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         /* Using cursor T001V42 */
         pr_default.execute(40, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(40) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A52Contratada_AreaTrabalhoCod = T001V42_A52Contratada_AreaTrabalhoCod[0];
         n52Contratada_AreaTrabalhoCod = T001V42_n52Contratada_AreaTrabalhoCod[0];
         pr_default.close(40);
         GXACONTAGEMRESULTADO_CONTADORFMCOD_html1V72( A490ContagemResultado_ContratadaCod) ;
         /* Using cursor T001V43 */
         pr_default.execute(41, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(41) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A601ContagemResultado_Servico = T001V43_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = T001V43_n601ContagemResultado_Servico[0];
         pr_default.close(41);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A146Modulo_Codigo = 0;
            n146Modulo_Codigo = false;
            A485ContagemResultado_EhValidacao = false;
            n485ContagemResultado_EhValidacao = false;
            A457ContagemResultado_Demanda = "";
            n457ContagemResultado_Demanda = false;
            A484ContagemResultado_StatusDmn = "";
            n484ContagemResultado_StatusDmn = false;
            A1389ContagemResultado_RdmnIssueId = 0;
            n1389ContagemResultado_RdmnIssueId = false;
            A493ContagemResultado_DemandaFM = "";
            n493ContagemResultado_DemandaFM = false;
            A890ContagemResultado_Responsavel = 0;
            n890ContagemResultado_Responsavel = false;
            A468ContagemResultado_NaoCnfDmnCod = 0;
            n468ContagemResultado_NaoCnfDmnCod = false;
            A490ContagemResultado_ContratadaCod = 0;
            n490ContagemResultado_ContratadaCod = false;
            A805ContagemResultado_ContratadaOrigemCod = 0;
            n805ContagemResultado_ContratadaOrigemCod = false;
            A1553ContagemResultado_CntSrvCod = 0;
            n1553ContagemResultado_CntSrvCod = false;
            A602ContagemResultado_OSVinculada = 0;
            n602ContagemResultado_OSVinculada = false;
            A127Sistema_Codigo = 0;
            A513Sistema_Coordenacao = "";
            n513Sistema_Coordenacao = false;
            A52Contratada_AreaTrabalhoCod = 0;
            n52Contratada_AreaTrabalhoCod = false;
            A601ContagemResultado_Servico = 0;
            n601ContagemResultado_Servico = false;
         }
         dynContagemResultado_ContadorFMCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(A485ContagemResultado_EhValidacao);
         isValidOutput.Add(A457ContagemResultado_Demanda);
         isValidOutput.Add(StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1389ContagemResultado_RdmnIssueId), 6, 0, ".", "")));
         isValidOutput.Add(A493ContagemResultado_DemandaFM);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A890ContagemResultado_Responsavel), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(A513Sistema_Coordenacao);
         isValidOutput.Add(A501ContagemResultado_OsFsOsFm);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
         if ( dynContagemResultado_ContadorFMCod.ItemCount > 0 )
         {
            A470ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynContagemResultado_ContadorFMCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0))), "."));
         }
         dynContagemResultado_ContadorFMCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0));
         isValidOutput.Add(dynContagemResultado_ContadorFMCod);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_contadorfmcod( GXCombobox dynGX_Parm1 ,
                                                         int GX_Parm2 ,
                                                         int GX_Parm3 ,
                                                         decimal GX_Parm4 ,
                                                         bool GX_Parm5 ,
                                                         bool GX_Parm6 ,
                                                         String GX_Parm7 ,
                                                         String GX_Parm8 )
      {
         dynContagemResultado_ContadorFMCod = dynGX_Parm1;
         A470ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynContagemResultado_ContadorFMCod.CurrentValue, "."));
         A479ContagemResultado_CrFMPessoaCod = GX_Parm2;
         n479ContagemResultado_CrFMPessoaCod = false;
         A1553ContagemResultado_CntSrvCod = GX_Parm3;
         n1553ContagemResultado_CntSrvCod = false;
         A833ContagemResultado_CstUntPrd = GX_Parm4;
         n833ContagemResultado_CstUntPrd = false;
         A999ContagemResultado_CrFMEhContratada = GX_Parm5;
         n999ContagemResultado_CrFMEhContratada = false;
         A1000ContagemResultado_CrFMEhContratante = GX_Parm6;
         n1000ContagemResultado_CrFMEhContratante = false;
         A474ContagemResultado_ContadorFMNom = GX_Parm7;
         n474ContagemResultado_ContadorFMNom = false;
         AV35CalculoPFinal = GX_Parm8;
         /* Using cursor T001V44 */
         pr_default.execute(42, new Object[] {A470ContagemResultado_ContadorFMCod});
         if ( (pr_default.getStatus(42) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Contador Fab. de M�t.'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTADORFMCOD");
            AnyError = 1;
            GX_FocusControl = dynContagemResultado_ContadorFMCod_Internalname;
         }
         A999ContagemResultado_CrFMEhContratada = T001V44_A999ContagemResultado_CrFMEhContratada[0];
         n999ContagemResultado_CrFMEhContratada = T001V44_n999ContagemResultado_CrFMEhContratada[0];
         A1000ContagemResultado_CrFMEhContratante = T001V44_A1000ContagemResultado_CrFMEhContratante[0];
         n1000ContagemResultado_CrFMEhContratante = T001V44_n1000ContagemResultado_CrFMEhContratante[0];
         A479ContagemResultado_CrFMPessoaCod = T001V44_A479ContagemResultado_CrFMPessoaCod[0];
         n479ContagemResultado_CrFMPessoaCod = T001V44_n479ContagemResultado_CrFMPessoaCod[0];
         pr_default.close(42);
         /* Using cursor T001V45 */
         pr_default.execute(43, new Object[] {n479ContagemResultado_CrFMPessoaCod, A479ContagemResultado_CrFMPessoaCod});
         if ( (pr_default.getStatus(43) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A474ContagemResultado_ContadorFMNom = T001V45_A474ContagemResultado_ContadorFMNom[0];
         n474ContagemResultado_ContadorFMNom = T001V45_n474ContagemResultado_ContadorFMNom[0];
         pr_default.close(43);
         GXt_char2 = AV35CalculoPFinal;
         new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  A470ContagemResultado_ContadorFMCod, ref  A833ContagemResultado_CstUntPrd, out  GXt_char2) ;
         AV35CalculoPFinal = GXt_char2;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A999ContagemResultado_CrFMEhContratada = false;
            n999ContagemResultado_CrFMEhContratada = false;
            A1000ContagemResultado_CrFMEhContratante = false;
            n1000ContagemResultado_CrFMEhContratante = false;
            A479ContagemResultado_CrFMPessoaCod = 0;
            n479ContagemResultado_CrFMPessoaCod = false;
            A474ContagemResultado_ContadorFMNom = "";
            n474ContagemResultado_ContadorFMNom = false;
         }
         isValidOutput.Add(A999ContagemResultado_CrFMEhContratada);
         isValidOutput.Add(A1000ContagemResultado_CrFMEhContratante);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A479ContagemResultado_CrFMPessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A474ContagemResultado_ContadorFMNom));
         isValidOutput.Add(StringUtil.RTrim( AV35CalculoPFinal));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_naocnfcntcod( GXCombobox dynGX_Parm1 ,
                                                        String GX_Parm2 )
      {
         dynContagemResultado_NaoCnfCntCod = dynGX_Parm1;
         A469ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( dynContagemResultado_NaoCnfCntCod.CurrentValue, "."));
         n469ContagemResultado_NaoCnfCntCod = false;
         A478ContagemResultado_NaoCnfCntNom = GX_Parm2;
         n478ContagemResultado_NaoCnfCntNom = false;
         /* Using cursor T001V46 */
         pr_default.execute(44, new Object[] {n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod});
         if ( (pr_default.getStatus(44) == 101) )
         {
            if ( ! ( (0==A469ContagemResultado_NaoCnfCntCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Nao Conformidade da Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_NAOCNFCNTCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_NaoCnfCntCod_Internalname;
            }
         }
         A478ContagemResultado_NaoCnfCntNom = T001V46_A478ContagemResultado_NaoCnfCntNom[0];
         n478ContagemResultado_NaoCnfCntNom = T001V46_n478ContagemResultado_NaoCnfCntNom[0];
         pr_default.close(44);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A478ContagemResultado_NaoCnfCntNom = "";
            n478ContagemResultado_NaoCnfCntNom = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A478ContagemResultado_NaoCnfCntNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8ContagemResultado_DataCnt',fld:'vCONTAGEMRESULTADO_DATACNT',pic:'',hsh:true,nv:''},{av:'AV18ContagemResultado_HoraCnt',fld:'vCONTAGEMRESULTADO_HORACNT',pic:'',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121V2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A483ContagemResultado_StatusCnt',fld:'CONTAGEMRESULTADO_STATUSCNT',pic:'Z9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV34Confirmado',fld:'vCONFIRMADO',pic:'',nv:false},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A805ContagemResultado_ContratadaOrigemCod',fld:'CONTAGEMRESULTADO_CONTRATADAORIGEMCOD',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A463ContagemResultado_ParecerTcn',fld:'CONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'A1389ContagemResultado_RdmnIssueId',fld:'CONTAGEMRESULTADO_RDMNISSUEID',pic:'ZZZZZ9',nv:0},{av:'AV12TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'AV34Confirmado',fld:'vCONFIRMADO',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("CONTAGEMRESULTADO_PFBFS.ISVALID","{handler:'E131V2',iparms:[{av:'AV20CalculoDivergencia',fld:'vCALCULODIVERGENCIA',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A459ContagemResultado_PFLFS',fld:'CONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A461ContagemResultado_PFLFM',fld:'CONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV27ContagemResultado_Divergencia',fld:'vCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0}]}");
         setEventMetadata("CONTAGEMRESULTADO_PFLFS.ISVALID","{handler:'E141V2',iparms:[{av:'AV20CalculoDivergencia',fld:'vCALCULODIVERGENCIA',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A459ContagemResultado_PFLFS',fld:'CONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A461ContagemResultado_PFLFM',fld:'CONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV27ContagemResultado_Divergencia',fld:'vCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0}]}");
         setEventMetadata("CONTAGEMRESULTADO_PFBFM.ISVALID","{handler:'E151V2',iparms:[{av:'AV20CalculoDivergencia',fld:'vCALCULODIVERGENCIA',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A459ContagemResultado_PFLFS',fld:'CONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A461ContagemResultado_PFLFM',fld:'CONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV27ContagemResultado_Divergencia',fld:'vCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0}]}");
         setEventMetadata("CONTAGEMRESULTADO_PFLFM.ISVALID","{handler:'E161V2',iparms:[{av:'AV20CalculoDivergencia',fld:'vCALCULODIVERGENCIA',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A459ContagemResultado_PFLFS',fld:'CONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A461ContagemResultado_PFLFM',fld:'CONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV27ContagemResultado_Divergencia',fld:'vCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(37);
         pr_default.close(26);
         pr_default.close(42);
         pr_default.close(31);
         pr_default.close(44);
         pr_default.close(33);
         pr_default.close(38);
         pr_default.close(27);
         pr_default.close(39);
         pr_default.close(28);
         pr_default.close(40);
         pr_default.close(29);
         pr_default.close(41);
         pr_default.close(30);
         pr_default.close(43);
         pr_default.close(32);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         wcpOAV8ContagemResultado_DataCnt = DateTime.MinValue;
         wcpOAV18ContagemResultado_HoraCnt = "";
         Z473ContagemResultado_DataCnt = DateTime.MinValue;
         Z511ContagemResultado_HoraCnt = "";
         Z481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         Z901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV20CalculoDivergencia = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontagemresultado_osfsosfm_Jsonclick = "";
         A501ContagemResultado_OsFsOsFm = "";
         lblTextblockcontagemresultado_datacnt_Jsonclick = "";
         lblTextblockcontagemresultado_contadorfmcod_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfs_Jsonclick = "";
         lblTextblockcontagemresultado_pflfs_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfm_Jsonclick = "";
         lblTextblockcontagemresultado_pflfm_Jsonclick = "";
         lblTextblockcontagemresultado_deflator_Jsonclick = "";
         lblTextblockcontagemresultado_parecertcn_Jsonclick = "";
         A463ContagemResultado_ParecerTcn = "";
         lblTextblockcontagemresultado_naocnfcntcod_Jsonclick = "";
         lblTextblockcontagemresultado_statuscnt_Jsonclick = "";
         lblTextblockcontagemresultado_divergencia_Jsonclick = "";
         lblTextblockcontagemresultado_planilha_Jsonclick = "";
         A853ContagemResultado_NomePla = "";
         A854ContagemResultado_TipoPla = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         A852ContagemResultado_Planilha = "";
         lblTextblockcontagemresultado_nomepla_Jsonclick = "";
         lblTextblockcontagemresultado_tipopla_Jsonclick = "";
         lblContagemresultado_divergencia_righttext_Jsonclick = "";
         lblTextblockcontagemresultadocontagens_esforco_Jsonclick = "";
         lblContagemresultadocontagens_esforco_righttext_Jsonclick = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         A481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         A901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         AV35CalculoPFinal = "";
         A484ContagemResultado_StatusDmn = "";
         A478ContagemResultado_NaoCnfCntNom = "";
         A513Sistema_Coordenacao = "";
         A474ContagemResultado_ContadorFMNom = "";
         AV37Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         GXCCtlgxBlob = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode72 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV12TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV14WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z463ContagemResultado_ParecerTcn = "";
         Z852ContagemResultado_Planilha = "";
         Z854ContagemResultado_TipoPla = "";
         Z853ContagemResultado_NomePla = "";
         Z457ContagemResultado_Demanda = "";
         Z484ContagemResultado_StatusDmn = "";
         Z493ContagemResultado_DemandaFM = "";
         Z513Sistema_Coordenacao = "";
         Z474ContagemResultado_ContadorFMNom = "";
         Z478ContagemResultado_NaoCnfCntNom = "";
         T001V6_A478ContagemResultado_NaoCnfCntNom = new String[] {""} ;
         T001V6_n478ContagemResultado_NaoCnfCntNom = new bool[] {false} ;
         T001V4_A146Modulo_Codigo = new int[1] ;
         T001V4_n146Modulo_Codigo = new bool[] {false} ;
         T001V4_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         T001V4_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         T001V4_A457ContagemResultado_Demanda = new String[] {""} ;
         T001V4_n457ContagemResultado_Demanda = new bool[] {false} ;
         T001V4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         T001V4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         T001V4_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         T001V4_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         T001V4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T001V4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T001V4_A890ContagemResultado_Responsavel = new int[1] ;
         T001V4_n890ContagemResultado_Responsavel = new bool[] {false} ;
         T001V4_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         T001V4_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         T001V4_A490ContagemResultado_ContratadaCod = new int[1] ;
         T001V4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T001V4_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         T001V4_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         T001V4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T001V4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T001V4_A602ContagemResultado_OSVinculada = new int[1] ;
         T001V4_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         T001V7_A127Sistema_Codigo = new int[1] ;
         T001V8_A513Sistema_Coordenacao = new String[] {""} ;
         T001V8_n513Sistema_Coordenacao = new bool[] {false} ;
         T001V9_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T001V9_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T001V10_A601ContagemResultado_Servico = new int[1] ;
         T001V10_n601ContagemResultado_Servico = new bool[] {false} ;
         T001V5_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         T001V5_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         T001V5_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         T001V5_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         T001V5_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         T001V5_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         T001V11_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         T001V11_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         T001V12_A146Modulo_Codigo = new int[1] ;
         T001V12_n146Modulo_Codigo = new bool[] {false} ;
         T001V12_A127Sistema_Codigo = new int[1] ;
         T001V12_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T001V12_A511ContagemResultado_HoraCnt = new String[] {""} ;
         T001V12_A482ContagemResultadoContagens_Esforco = new short[1] ;
         T001V12_A462ContagemResultado_Divergencia = new decimal[1] ;
         T001V12_A483ContagemResultado_StatusCnt = new short[1] ;
         T001V12_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         T001V12_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         T001V12_A513Sistema_Coordenacao = new String[] {""} ;
         T001V12_n513Sistema_Coordenacao = new bool[] {false} ;
         T001V12_A457ContagemResultado_Demanda = new String[] {""} ;
         T001V12_n457ContagemResultado_Demanda = new bool[] {false} ;
         T001V12_A484ContagemResultado_StatusDmn = new String[] {""} ;
         T001V12_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         T001V12_A481ContagemResultado_TimeCnt = new DateTime[] {DateTime.MinValue} ;
         T001V12_n481ContagemResultado_TimeCnt = new bool[] {false} ;
         T001V12_A901ContagemResultadoContagens_Prazo = new DateTime[] {DateTime.MinValue} ;
         T001V12_n901ContagemResultadoContagens_Prazo = new bool[] {false} ;
         T001V12_A458ContagemResultado_PFBFS = new decimal[1] ;
         T001V12_n458ContagemResultado_PFBFS = new bool[] {false} ;
         T001V12_A459ContagemResultado_PFLFS = new decimal[1] ;
         T001V12_n459ContagemResultado_PFLFS = new bool[] {false} ;
         T001V12_A460ContagemResultado_PFBFM = new decimal[1] ;
         T001V12_n460ContagemResultado_PFBFM = new bool[] {false} ;
         T001V12_A461ContagemResultado_PFLFM = new decimal[1] ;
         T001V12_n461ContagemResultado_PFLFM = new bool[] {false} ;
         T001V12_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         T001V12_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         T001V12_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         T001V12_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         T001V12_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         T001V12_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         T001V12_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         T001V12_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         T001V12_A478ContagemResultado_NaoCnfCntNom = new String[] {""} ;
         T001V12_n478ContagemResultado_NaoCnfCntNom = new bool[] {false} ;
         T001V12_A517ContagemResultado_Ultima = new bool[] {false} ;
         T001V12_A800ContagemResultado_Deflator = new decimal[1] ;
         T001V12_n800ContagemResultado_Deflator = new bool[] {false} ;
         T001V12_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         T001V12_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         T001V12_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         T001V12_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         T001V12_A1756ContagemResultado_NvlCnt = new short[1] ;
         T001V12_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         T001V12_A854ContagemResultado_TipoPla = new String[] {""} ;
         T001V12_n854ContagemResultado_TipoPla = new bool[] {false} ;
         T001V12_A853ContagemResultado_NomePla = new String[] {""} ;
         T001V12_n853ContagemResultado_NomePla = new bool[] {false} ;
         T001V12_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T001V12_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T001V12_A456ContagemResultado_Codigo = new int[1] ;
         T001V12_A470ContagemResultado_ContadorFMCod = new int[1] ;
         T001V12_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         T001V12_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         T001V12_A890ContagemResultado_Responsavel = new int[1] ;
         T001V12_n890ContagemResultado_Responsavel = new bool[] {false} ;
         T001V12_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         T001V12_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         T001V12_A490ContagemResultado_ContratadaCod = new int[1] ;
         T001V12_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T001V12_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T001V12_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T001V12_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         T001V12_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         T001V12_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T001V12_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T001V12_A601ContagemResultado_Servico = new int[1] ;
         T001V12_n601ContagemResultado_Servico = new bool[] {false} ;
         T001V12_A602ContagemResultado_OSVinculada = new int[1] ;
         T001V12_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         T001V12_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         T001V12_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         T001V12_A852ContagemResultado_Planilha = new String[] {""} ;
         T001V12_n852ContagemResultado_Planilha = new bool[] {false} ;
         T001V13_A146Modulo_Codigo = new int[1] ;
         T001V13_n146Modulo_Codigo = new bool[] {false} ;
         T001V13_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         T001V13_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         T001V13_A457ContagemResultado_Demanda = new String[] {""} ;
         T001V13_n457ContagemResultado_Demanda = new bool[] {false} ;
         T001V13_A484ContagemResultado_StatusDmn = new String[] {""} ;
         T001V13_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         T001V13_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         T001V13_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         T001V13_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T001V13_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T001V13_A890ContagemResultado_Responsavel = new int[1] ;
         T001V13_n890ContagemResultado_Responsavel = new bool[] {false} ;
         T001V13_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         T001V13_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         T001V13_A490ContagemResultado_ContratadaCod = new int[1] ;
         T001V13_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T001V13_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         T001V13_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         T001V13_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T001V13_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T001V13_A602ContagemResultado_OSVinculada = new int[1] ;
         T001V13_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         T001V14_A127Sistema_Codigo = new int[1] ;
         T001V15_A513Sistema_Coordenacao = new String[] {""} ;
         T001V15_n513Sistema_Coordenacao = new bool[] {false} ;
         T001V16_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T001V16_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T001V17_A601ContagemResultado_Servico = new int[1] ;
         T001V17_n601ContagemResultado_Servico = new bool[] {false} ;
         T001V18_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         T001V18_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         T001V18_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         T001V18_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         T001V18_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         T001V18_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         T001V19_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         T001V19_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         T001V20_A478ContagemResultado_NaoCnfCntNom = new String[] {""} ;
         T001V20_n478ContagemResultado_NaoCnfCntNom = new bool[] {false} ;
         T001V21_A456ContagemResultado_Codigo = new int[1] ;
         T001V21_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T001V21_A511ContagemResultado_HoraCnt = new String[] {""} ;
         T001V3_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T001V3_A511ContagemResultado_HoraCnt = new String[] {""} ;
         T001V3_A482ContagemResultadoContagens_Esforco = new short[1] ;
         T001V3_A462ContagemResultado_Divergencia = new decimal[1] ;
         T001V3_A483ContagemResultado_StatusCnt = new short[1] ;
         T001V3_A481ContagemResultado_TimeCnt = new DateTime[] {DateTime.MinValue} ;
         T001V3_n481ContagemResultado_TimeCnt = new bool[] {false} ;
         T001V3_A901ContagemResultadoContagens_Prazo = new DateTime[] {DateTime.MinValue} ;
         T001V3_n901ContagemResultadoContagens_Prazo = new bool[] {false} ;
         T001V3_A458ContagemResultado_PFBFS = new decimal[1] ;
         T001V3_n458ContagemResultado_PFBFS = new bool[] {false} ;
         T001V3_A459ContagemResultado_PFLFS = new decimal[1] ;
         T001V3_n459ContagemResultado_PFLFS = new bool[] {false} ;
         T001V3_A460ContagemResultado_PFBFM = new decimal[1] ;
         T001V3_n460ContagemResultado_PFBFM = new bool[] {false} ;
         T001V3_A461ContagemResultado_PFLFM = new decimal[1] ;
         T001V3_n461ContagemResultado_PFLFM = new bool[] {false} ;
         T001V3_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         T001V3_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         T001V3_A517ContagemResultado_Ultima = new bool[] {false} ;
         T001V3_A800ContagemResultado_Deflator = new decimal[1] ;
         T001V3_n800ContagemResultado_Deflator = new bool[] {false} ;
         T001V3_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         T001V3_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         T001V3_A1756ContagemResultado_NvlCnt = new short[1] ;
         T001V3_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         T001V3_A854ContagemResultado_TipoPla = new String[] {""} ;
         T001V3_n854ContagemResultado_TipoPla = new bool[] {false} ;
         T001V3_A853ContagemResultado_NomePla = new String[] {""} ;
         T001V3_n853ContagemResultado_NomePla = new bool[] {false} ;
         T001V3_A456ContagemResultado_Codigo = new int[1] ;
         T001V3_A470ContagemResultado_ContadorFMCod = new int[1] ;
         T001V3_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         T001V3_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         T001V3_A852ContagemResultado_Planilha = new String[] {""} ;
         T001V3_n852ContagemResultado_Planilha = new bool[] {false} ;
         T001V22_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T001V22_A511ContagemResultado_HoraCnt = new String[] {""} ;
         T001V22_A456ContagemResultado_Codigo = new int[1] ;
         T001V23_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T001V23_A511ContagemResultado_HoraCnt = new String[] {""} ;
         T001V23_A456ContagemResultado_Codigo = new int[1] ;
         T001V2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T001V2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         T001V2_A482ContagemResultadoContagens_Esforco = new short[1] ;
         T001V2_A462ContagemResultado_Divergencia = new decimal[1] ;
         T001V2_A483ContagemResultado_StatusCnt = new short[1] ;
         T001V2_A481ContagemResultado_TimeCnt = new DateTime[] {DateTime.MinValue} ;
         T001V2_n481ContagemResultado_TimeCnt = new bool[] {false} ;
         T001V2_A901ContagemResultadoContagens_Prazo = new DateTime[] {DateTime.MinValue} ;
         T001V2_n901ContagemResultadoContagens_Prazo = new bool[] {false} ;
         T001V2_A458ContagemResultado_PFBFS = new decimal[1] ;
         T001V2_n458ContagemResultado_PFBFS = new bool[] {false} ;
         T001V2_A459ContagemResultado_PFLFS = new decimal[1] ;
         T001V2_n459ContagemResultado_PFLFS = new bool[] {false} ;
         T001V2_A460ContagemResultado_PFBFM = new decimal[1] ;
         T001V2_n460ContagemResultado_PFBFM = new bool[] {false} ;
         T001V2_A461ContagemResultado_PFLFM = new decimal[1] ;
         T001V2_n461ContagemResultado_PFLFM = new bool[] {false} ;
         T001V2_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         T001V2_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         T001V2_A517ContagemResultado_Ultima = new bool[] {false} ;
         T001V2_A800ContagemResultado_Deflator = new decimal[1] ;
         T001V2_n800ContagemResultado_Deflator = new bool[] {false} ;
         T001V2_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         T001V2_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         T001V2_A1756ContagemResultado_NvlCnt = new short[1] ;
         T001V2_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         T001V2_A854ContagemResultado_TipoPla = new String[] {""} ;
         T001V2_n854ContagemResultado_TipoPla = new bool[] {false} ;
         T001V2_A853ContagemResultado_NomePla = new String[] {""} ;
         T001V2_n853ContagemResultado_NomePla = new bool[] {false} ;
         T001V2_A456ContagemResultado_Codigo = new int[1] ;
         T001V2_A470ContagemResultado_ContadorFMCod = new int[1] ;
         T001V2_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         T001V2_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         T001V2_A852ContagemResultado_Planilha = new String[] {""} ;
         T001V2_n852ContagemResultado_Planilha = new bool[] {false} ;
         T001V28_A146Modulo_Codigo = new int[1] ;
         T001V28_n146Modulo_Codigo = new bool[] {false} ;
         T001V28_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         T001V28_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         T001V28_A457ContagemResultado_Demanda = new String[] {""} ;
         T001V28_n457ContagemResultado_Demanda = new bool[] {false} ;
         T001V28_A484ContagemResultado_StatusDmn = new String[] {""} ;
         T001V28_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         T001V28_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         T001V28_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         T001V28_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T001V28_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T001V28_A890ContagemResultado_Responsavel = new int[1] ;
         T001V28_n890ContagemResultado_Responsavel = new bool[] {false} ;
         T001V28_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         T001V28_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         T001V28_A490ContagemResultado_ContratadaCod = new int[1] ;
         T001V28_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T001V28_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         T001V28_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         T001V28_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T001V28_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T001V28_A602ContagemResultado_OSVinculada = new int[1] ;
         T001V28_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         T001V29_A127Sistema_Codigo = new int[1] ;
         T001V30_A513Sistema_Coordenacao = new String[] {""} ;
         T001V30_n513Sistema_Coordenacao = new bool[] {false} ;
         T001V31_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T001V31_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T001V32_A601ContagemResultado_Servico = new int[1] ;
         T001V32_n601ContagemResultado_Servico = new bool[] {false} ;
         T001V33_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         T001V33_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         T001V33_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         T001V33_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         T001V33_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         T001V33_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         T001V34_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         T001V34_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         T001V35_A478ContagemResultado_NaoCnfCntNom = new String[] {""} ;
         T001V35_n478ContagemResultado_NaoCnfCntNom = new bool[] {false} ;
         T001V36_A456ContagemResultado_Codigo = new int[1] ;
         T001V36_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T001V36_A511ContagemResultado_HoraCnt = new String[] {""} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T001V37_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         T001V37_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T001V37_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         T001V37_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         T001V37_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T001V37_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T001V37_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T001V37_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T001V38_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         T001V38_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         T001V38_A478ContagemResultado_NaoCnfCntNom = new String[] {""} ;
         T001V38_n478ContagemResultado_NaoCnfCntNom = new bool[] {false} ;
         T001V39_A146Modulo_Codigo = new int[1] ;
         T001V39_n146Modulo_Codigo = new bool[] {false} ;
         T001V39_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         T001V39_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         T001V39_A457ContagemResultado_Demanda = new String[] {""} ;
         T001V39_n457ContagemResultado_Demanda = new bool[] {false} ;
         T001V39_A484ContagemResultado_StatusDmn = new String[] {""} ;
         T001V39_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         T001V39_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         T001V39_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         T001V39_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T001V39_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T001V39_A890ContagemResultado_Responsavel = new int[1] ;
         T001V39_n890ContagemResultado_Responsavel = new bool[] {false} ;
         T001V39_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         T001V39_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         T001V39_A490ContagemResultado_ContratadaCod = new int[1] ;
         T001V39_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T001V39_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         T001V39_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         T001V39_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T001V39_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T001V39_A602ContagemResultado_OSVinculada = new int[1] ;
         T001V39_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         T001V40_A127Sistema_Codigo = new int[1] ;
         T001V41_A513Sistema_Coordenacao = new String[] {""} ;
         T001V41_n513Sistema_Coordenacao = new bool[] {false} ;
         T001V42_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T001V42_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T001V43_A601ContagemResultado_Servico = new int[1] ;
         T001V43_n601ContagemResultado_Servico = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T001V44_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         T001V44_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         T001V44_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         T001V44_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         T001V44_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         T001V44_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         T001V45_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         T001V45_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         GXt_char2 = "";
         T001V46_A478ContagemResultado_NaoCnfCntNom = new String[] {""} ;
         T001V46_n478ContagemResultado_NaoCnfCntNom = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadocontagens__default(),
            new Object[][] {
                new Object[] {
               T001V2_A473ContagemResultado_DataCnt, T001V2_A511ContagemResultado_HoraCnt, T001V2_A482ContagemResultadoContagens_Esforco, T001V2_A462ContagemResultado_Divergencia, T001V2_A483ContagemResultado_StatusCnt, T001V2_A481ContagemResultado_TimeCnt, T001V2_n481ContagemResultado_TimeCnt, T001V2_A901ContagemResultadoContagens_Prazo, T001V2_n901ContagemResultadoContagens_Prazo, T001V2_A458ContagemResultado_PFBFS,
               T001V2_n458ContagemResultado_PFBFS, T001V2_A459ContagemResultado_PFLFS, T001V2_n459ContagemResultado_PFLFS, T001V2_A460ContagemResultado_PFBFM, T001V2_n460ContagemResultado_PFBFM, T001V2_A461ContagemResultado_PFLFM, T001V2_n461ContagemResultado_PFLFM, T001V2_A463ContagemResultado_ParecerTcn, T001V2_n463ContagemResultado_ParecerTcn, T001V2_A517ContagemResultado_Ultima,
               T001V2_A800ContagemResultado_Deflator, T001V2_n800ContagemResultado_Deflator, T001V2_A833ContagemResultado_CstUntPrd, T001V2_n833ContagemResultado_CstUntPrd, T001V2_A1756ContagemResultado_NvlCnt, T001V2_n1756ContagemResultado_NvlCnt, T001V2_A854ContagemResultado_TipoPla, T001V2_n854ContagemResultado_TipoPla, T001V2_A853ContagemResultado_NomePla, T001V2_n853ContagemResultado_NomePla,
               T001V2_A456ContagemResultado_Codigo, T001V2_A470ContagemResultado_ContadorFMCod, T001V2_A469ContagemResultado_NaoCnfCntCod, T001V2_n469ContagemResultado_NaoCnfCntCod, T001V2_A852ContagemResultado_Planilha, T001V2_n852ContagemResultado_Planilha
               }
               , new Object[] {
               T001V3_A473ContagemResultado_DataCnt, T001V3_A511ContagemResultado_HoraCnt, T001V3_A482ContagemResultadoContagens_Esforco, T001V3_A462ContagemResultado_Divergencia, T001V3_A483ContagemResultado_StatusCnt, T001V3_A481ContagemResultado_TimeCnt, T001V3_n481ContagemResultado_TimeCnt, T001V3_A901ContagemResultadoContagens_Prazo, T001V3_n901ContagemResultadoContagens_Prazo, T001V3_A458ContagemResultado_PFBFS,
               T001V3_n458ContagemResultado_PFBFS, T001V3_A459ContagemResultado_PFLFS, T001V3_n459ContagemResultado_PFLFS, T001V3_A460ContagemResultado_PFBFM, T001V3_n460ContagemResultado_PFBFM, T001V3_A461ContagemResultado_PFLFM, T001V3_n461ContagemResultado_PFLFM, T001V3_A463ContagemResultado_ParecerTcn, T001V3_n463ContagemResultado_ParecerTcn, T001V3_A517ContagemResultado_Ultima,
               T001V3_A800ContagemResultado_Deflator, T001V3_n800ContagemResultado_Deflator, T001V3_A833ContagemResultado_CstUntPrd, T001V3_n833ContagemResultado_CstUntPrd, T001V3_A1756ContagemResultado_NvlCnt, T001V3_n1756ContagemResultado_NvlCnt, T001V3_A854ContagemResultado_TipoPla, T001V3_n854ContagemResultado_TipoPla, T001V3_A853ContagemResultado_NomePla, T001V3_n853ContagemResultado_NomePla,
               T001V3_A456ContagemResultado_Codigo, T001V3_A470ContagemResultado_ContadorFMCod, T001V3_A469ContagemResultado_NaoCnfCntCod, T001V3_n469ContagemResultado_NaoCnfCntCod, T001V3_A852ContagemResultado_Planilha, T001V3_n852ContagemResultado_Planilha
               }
               , new Object[] {
               T001V4_A146Modulo_Codigo, T001V4_n146Modulo_Codigo, T001V4_A485ContagemResultado_EhValidacao, T001V4_n485ContagemResultado_EhValidacao, T001V4_A457ContagemResultado_Demanda, T001V4_n457ContagemResultado_Demanda, T001V4_A484ContagemResultado_StatusDmn, T001V4_n484ContagemResultado_StatusDmn, T001V4_A1389ContagemResultado_RdmnIssueId, T001V4_n1389ContagemResultado_RdmnIssueId,
               T001V4_A493ContagemResultado_DemandaFM, T001V4_n493ContagemResultado_DemandaFM, T001V4_A890ContagemResultado_Responsavel, T001V4_n890ContagemResultado_Responsavel, T001V4_A468ContagemResultado_NaoCnfDmnCod, T001V4_n468ContagemResultado_NaoCnfDmnCod, T001V4_A490ContagemResultado_ContratadaCod, T001V4_n490ContagemResultado_ContratadaCod, T001V4_A805ContagemResultado_ContratadaOrigemCod, T001V4_n805ContagemResultado_ContratadaOrigemCod,
               T001V4_A1553ContagemResultado_CntSrvCod, T001V4_n1553ContagemResultado_CntSrvCod, T001V4_A602ContagemResultado_OSVinculada, T001V4_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               T001V5_A999ContagemResultado_CrFMEhContratada, T001V5_n999ContagemResultado_CrFMEhContratada, T001V5_A1000ContagemResultado_CrFMEhContratante, T001V5_n1000ContagemResultado_CrFMEhContratante, T001V5_A479ContagemResultado_CrFMPessoaCod, T001V5_n479ContagemResultado_CrFMPessoaCod
               }
               , new Object[] {
               T001V6_A478ContagemResultado_NaoCnfCntNom, T001V6_n478ContagemResultado_NaoCnfCntNom
               }
               , new Object[] {
               T001V7_A127Sistema_Codigo
               }
               , new Object[] {
               T001V8_A513Sistema_Coordenacao, T001V8_n513Sistema_Coordenacao
               }
               , new Object[] {
               T001V9_A52Contratada_AreaTrabalhoCod, T001V9_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T001V10_A601ContagemResultado_Servico, T001V10_n601ContagemResultado_Servico
               }
               , new Object[] {
               T001V11_A474ContagemResultado_ContadorFMNom, T001V11_n474ContagemResultado_ContadorFMNom
               }
               , new Object[] {
               T001V12_A146Modulo_Codigo, T001V12_n146Modulo_Codigo, T001V12_A127Sistema_Codigo, T001V12_A473ContagemResultado_DataCnt, T001V12_A511ContagemResultado_HoraCnt, T001V12_A482ContagemResultadoContagens_Esforco, T001V12_A462ContagemResultado_Divergencia, T001V12_A483ContagemResultado_StatusCnt, T001V12_A485ContagemResultado_EhValidacao, T001V12_n485ContagemResultado_EhValidacao,
               T001V12_A513Sistema_Coordenacao, T001V12_n513Sistema_Coordenacao, T001V12_A457ContagemResultado_Demanda, T001V12_n457ContagemResultado_Demanda, T001V12_A484ContagemResultado_StatusDmn, T001V12_n484ContagemResultado_StatusDmn, T001V12_A481ContagemResultado_TimeCnt, T001V12_n481ContagemResultado_TimeCnt, T001V12_A901ContagemResultadoContagens_Prazo, T001V12_n901ContagemResultadoContagens_Prazo,
               T001V12_A458ContagemResultado_PFBFS, T001V12_n458ContagemResultado_PFBFS, T001V12_A459ContagemResultado_PFLFS, T001V12_n459ContagemResultado_PFLFS, T001V12_A460ContagemResultado_PFBFM, T001V12_n460ContagemResultado_PFBFM, T001V12_A461ContagemResultado_PFLFM, T001V12_n461ContagemResultado_PFLFM, T001V12_A463ContagemResultado_ParecerTcn, T001V12_n463ContagemResultado_ParecerTcn,
               T001V12_A474ContagemResultado_ContadorFMNom, T001V12_n474ContagemResultado_ContadorFMNom, T001V12_A999ContagemResultado_CrFMEhContratada, T001V12_n999ContagemResultado_CrFMEhContratada, T001V12_A1000ContagemResultado_CrFMEhContratante, T001V12_n1000ContagemResultado_CrFMEhContratante, T001V12_A478ContagemResultado_NaoCnfCntNom, T001V12_n478ContagemResultado_NaoCnfCntNom, T001V12_A517ContagemResultado_Ultima, T001V12_A800ContagemResultado_Deflator,
               T001V12_n800ContagemResultado_Deflator, T001V12_A833ContagemResultado_CstUntPrd, T001V12_n833ContagemResultado_CstUntPrd, T001V12_A1389ContagemResultado_RdmnIssueId, T001V12_n1389ContagemResultado_RdmnIssueId, T001V12_A1756ContagemResultado_NvlCnt, T001V12_n1756ContagemResultado_NvlCnt, T001V12_A854ContagemResultado_TipoPla, T001V12_n854ContagemResultado_TipoPla, T001V12_A853ContagemResultado_NomePla,
               T001V12_n853ContagemResultado_NomePla, T001V12_A493ContagemResultado_DemandaFM, T001V12_n493ContagemResultado_DemandaFM, T001V12_A456ContagemResultado_Codigo, T001V12_A470ContagemResultado_ContadorFMCod, T001V12_A469ContagemResultado_NaoCnfCntCod, T001V12_n469ContagemResultado_NaoCnfCntCod, T001V12_A890ContagemResultado_Responsavel, T001V12_n890ContagemResultado_Responsavel, T001V12_A468ContagemResultado_NaoCnfDmnCod,
               T001V12_n468ContagemResultado_NaoCnfDmnCod, T001V12_A490ContagemResultado_ContratadaCod, T001V12_n490ContagemResultado_ContratadaCod, T001V12_A52Contratada_AreaTrabalhoCod, T001V12_n52Contratada_AreaTrabalhoCod, T001V12_A805ContagemResultado_ContratadaOrigemCod, T001V12_n805ContagemResultado_ContratadaOrigemCod, T001V12_A1553ContagemResultado_CntSrvCod, T001V12_n1553ContagemResultado_CntSrvCod, T001V12_A601ContagemResultado_Servico,
               T001V12_n601ContagemResultado_Servico, T001V12_A602ContagemResultado_OSVinculada, T001V12_n602ContagemResultado_OSVinculada, T001V12_A479ContagemResultado_CrFMPessoaCod, T001V12_n479ContagemResultado_CrFMPessoaCod, T001V12_A852ContagemResultado_Planilha, T001V12_n852ContagemResultado_Planilha
               }
               , new Object[] {
               T001V13_A146Modulo_Codigo, T001V13_n146Modulo_Codigo, T001V13_A485ContagemResultado_EhValidacao, T001V13_n485ContagemResultado_EhValidacao, T001V13_A457ContagemResultado_Demanda, T001V13_n457ContagemResultado_Demanda, T001V13_A484ContagemResultado_StatusDmn, T001V13_n484ContagemResultado_StatusDmn, T001V13_A1389ContagemResultado_RdmnIssueId, T001V13_n1389ContagemResultado_RdmnIssueId,
               T001V13_A493ContagemResultado_DemandaFM, T001V13_n493ContagemResultado_DemandaFM, T001V13_A890ContagemResultado_Responsavel, T001V13_n890ContagemResultado_Responsavel, T001V13_A468ContagemResultado_NaoCnfDmnCod, T001V13_n468ContagemResultado_NaoCnfDmnCod, T001V13_A490ContagemResultado_ContratadaCod, T001V13_n490ContagemResultado_ContratadaCod, T001V13_A805ContagemResultado_ContratadaOrigemCod, T001V13_n805ContagemResultado_ContratadaOrigemCod,
               T001V13_A1553ContagemResultado_CntSrvCod, T001V13_n1553ContagemResultado_CntSrvCod, T001V13_A602ContagemResultado_OSVinculada, T001V13_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               T001V14_A127Sistema_Codigo
               }
               , new Object[] {
               T001V15_A513Sistema_Coordenacao, T001V15_n513Sistema_Coordenacao
               }
               , new Object[] {
               T001V16_A52Contratada_AreaTrabalhoCod, T001V16_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T001V17_A601ContagemResultado_Servico, T001V17_n601ContagemResultado_Servico
               }
               , new Object[] {
               T001V18_A999ContagemResultado_CrFMEhContratada, T001V18_n999ContagemResultado_CrFMEhContratada, T001V18_A1000ContagemResultado_CrFMEhContratante, T001V18_n1000ContagemResultado_CrFMEhContratante, T001V18_A479ContagemResultado_CrFMPessoaCod, T001V18_n479ContagemResultado_CrFMPessoaCod
               }
               , new Object[] {
               T001V19_A474ContagemResultado_ContadorFMNom, T001V19_n474ContagemResultado_ContadorFMNom
               }
               , new Object[] {
               T001V20_A478ContagemResultado_NaoCnfCntNom, T001V20_n478ContagemResultado_NaoCnfCntNom
               }
               , new Object[] {
               T001V21_A456ContagemResultado_Codigo, T001V21_A473ContagemResultado_DataCnt, T001V21_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               T001V22_A473ContagemResultado_DataCnt, T001V22_A511ContagemResultado_HoraCnt, T001V22_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T001V23_A473ContagemResultado_DataCnt, T001V23_A511ContagemResultado_HoraCnt, T001V23_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001V28_A146Modulo_Codigo, T001V28_n146Modulo_Codigo, T001V28_A485ContagemResultado_EhValidacao, T001V28_n485ContagemResultado_EhValidacao, T001V28_A457ContagemResultado_Demanda, T001V28_n457ContagemResultado_Demanda, T001V28_A484ContagemResultado_StatusDmn, T001V28_n484ContagemResultado_StatusDmn, T001V28_A1389ContagemResultado_RdmnIssueId, T001V28_n1389ContagemResultado_RdmnIssueId,
               T001V28_A493ContagemResultado_DemandaFM, T001V28_n493ContagemResultado_DemandaFM, T001V28_A890ContagemResultado_Responsavel, T001V28_n890ContagemResultado_Responsavel, T001V28_A468ContagemResultado_NaoCnfDmnCod, T001V28_n468ContagemResultado_NaoCnfDmnCod, T001V28_A490ContagemResultado_ContratadaCod, T001V28_n490ContagemResultado_ContratadaCod, T001V28_A805ContagemResultado_ContratadaOrigemCod, T001V28_n805ContagemResultado_ContratadaOrigemCod,
               T001V28_A1553ContagemResultado_CntSrvCod, T001V28_n1553ContagemResultado_CntSrvCod, T001V28_A602ContagemResultado_OSVinculada, T001V28_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               T001V29_A127Sistema_Codigo
               }
               , new Object[] {
               T001V30_A513Sistema_Coordenacao, T001V30_n513Sistema_Coordenacao
               }
               , new Object[] {
               T001V31_A52Contratada_AreaTrabalhoCod, T001V31_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T001V32_A601ContagemResultado_Servico, T001V32_n601ContagemResultado_Servico
               }
               , new Object[] {
               T001V33_A999ContagemResultado_CrFMEhContratada, T001V33_n999ContagemResultado_CrFMEhContratada, T001V33_A1000ContagemResultado_CrFMEhContratante, T001V33_n1000ContagemResultado_CrFMEhContratante, T001V33_A479ContagemResultado_CrFMPessoaCod, T001V33_n479ContagemResultado_CrFMPessoaCod
               }
               , new Object[] {
               T001V34_A474ContagemResultado_ContadorFMNom, T001V34_n474ContagemResultado_ContadorFMNom
               }
               , new Object[] {
               T001V35_A478ContagemResultado_NaoCnfCntNom, T001V35_n478ContagemResultado_NaoCnfCntNom
               }
               , new Object[] {
               T001V36_A456ContagemResultado_Codigo, T001V36_A473ContagemResultado_DataCnt, T001V36_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               T001V37_A70ContratadaUsuario_UsuarioPessoaCod, T001V37_n70ContratadaUsuario_UsuarioPessoaCod, T001V37_A69ContratadaUsuario_UsuarioCod, T001V37_A71ContratadaUsuario_UsuarioPessoaNom, T001V37_n71ContratadaUsuario_UsuarioPessoaNom, T001V37_A66ContratadaUsuario_ContratadaCod, T001V37_A1394ContratadaUsuario_UsuarioAtivo, T001V37_n1394ContratadaUsuario_UsuarioAtivo
               }
               , new Object[] {
               T001V38_A469ContagemResultado_NaoCnfCntCod, T001V38_A478ContagemResultado_NaoCnfCntNom, T001V38_n478ContagemResultado_NaoCnfCntNom
               }
               , new Object[] {
               T001V39_A146Modulo_Codigo, T001V39_n146Modulo_Codigo, T001V39_A485ContagemResultado_EhValidacao, T001V39_n485ContagemResultado_EhValidacao, T001V39_A457ContagemResultado_Demanda, T001V39_n457ContagemResultado_Demanda, T001V39_A484ContagemResultado_StatusDmn, T001V39_n484ContagemResultado_StatusDmn, T001V39_A1389ContagemResultado_RdmnIssueId, T001V39_n1389ContagemResultado_RdmnIssueId,
               T001V39_A493ContagemResultado_DemandaFM, T001V39_n493ContagemResultado_DemandaFM, T001V39_A890ContagemResultado_Responsavel, T001V39_n890ContagemResultado_Responsavel, T001V39_A468ContagemResultado_NaoCnfDmnCod, T001V39_n468ContagemResultado_NaoCnfDmnCod, T001V39_A490ContagemResultado_ContratadaCod, T001V39_n490ContagemResultado_ContratadaCod, T001V39_A805ContagemResultado_ContratadaOrigemCod, T001V39_n805ContagemResultado_ContratadaOrigemCod,
               T001V39_A1553ContagemResultado_CntSrvCod, T001V39_n1553ContagemResultado_CntSrvCod, T001V39_A602ContagemResultado_OSVinculada, T001V39_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               T001V40_A127Sistema_Codigo
               }
               , new Object[] {
               T001V41_A513Sistema_Coordenacao, T001V41_n513Sistema_Coordenacao
               }
               , new Object[] {
               T001V42_A52Contratada_AreaTrabalhoCod, T001V42_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T001V43_A601ContagemResultado_Servico, T001V43_n601ContagemResultado_Servico
               }
               , new Object[] {
               T001V44_A999ContagemResultado_CrFMEhContratada, T001V44_n999ContagemResultado_CrFMEhContratada, T001V44_A1000ContagemResultado_CrFMEhContratante, T001V44_n1000ContagemResultado_CrFMEhContratante, T001V44_A479ContagemResultado_CrFMPessoaCod, T001V44_n479ContagemResultado_CrFMPessoaCod
               }
               , new Object[] {
               T001V45_A474ContagemResultado_ContadorFMNom, T001V45_n474ContagemResultado_ContadorFMNom
               }
               , new Object[] {
               T001V46_A478ContagemResultado_NaoCnfCntNom, T001V46_n478ContagemResultado_NaoCnfCntNom
               }
            }
         );
         Z517ContagemResultado_Ultima = true;
         A517ContagemResultado_Ultima = true;
         i517ContagemResultado_Ultima = true;
         Z482ContagemResultadoContagens_Esforco = 0;
         A482ContagemResultadoContagens_Esforco = 0;
         i482ContagemResultadoContagens_Esforco = 0;
         Z511ContagemResultado_HoraCnt = context.localUtil.Time( );
         A511ContagemResultado_HoraCnt = context.localUtil.Time( );
         AV37Pgmname = "ContagemResultadoContagens";
      }

      private short Z482ContagemResultadoContagens_Esforco ;
      private short Z483ContagemResultado_StatusCnt ;
      private short Z1756ContagemResultado_NvlCnt ;
      private short O483ContagemResultado_StatusCnt ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A483ContagemResultado_StatusCnt ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short edtContagemResultado_Planilha_Display ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short A1756ContagemResultado_NvlCnt ;
      private short Gx_BScreen ;
      private short RcdFound72 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short i482ContagemResultadoContagens_Esforco ;
      private short wbTemp ;
      private int wcpOAV7ContagemResultado_Codigo ;
      private int Z456ContagemResultado_Codigo ;
      private int Z470ContagemResultado_ContadorFMCod ;
      private int Z469ContagemResultado_NaoCnfCntCod ;
      private int N469ContagemResultado_NaoCnfCntCod ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int AV7ContagemResultado_Codigo ;
      private int trnEnded ;
      private int edtContagemResultado_Codigo_Visible ;
      private int edtContagemResultado_Codigo_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContagemResultado_OsFsOsFm_Enabled ;
      private int lblTextblockcontagemresultado_contadorfmcod_Visible ;
      private int edtContagemResultado_PFBFS_Enabled ;
      private int edtContagemResultado_PFLFS_Enabled ;
      private int edtContagemResultado_PFBFM_Enabled ;
      private int edtContagemResultado_PFLFM_Enabled ;
      private int edtContagemResultado_Deflator_Enabled ;
      private int edtContagemResultado_Deflator_Visible ;
      private int edtContagemResultado_ParecerTcn_Enabled ;
      private int edtContagemResultado_Planilha_Enabled ;
      private int edtContagemResultado_NomePla_Enabled ;
      private int edtContagemResultado_TipoPla_Enabled ;
      private int edtavContagemresultado_divergencia_Enabled ;
      private int edtContagemResultadoContagens_Esforco_Enabled ;
      private int edtContagemResultado_DataCnt_Enabled ;
      private int edtContagemResultado_HoraCnt_Enabled ;
      private int AV10Insert_ContagemResultado_ContadorFMCod ;
      private int AV11Insert_ContagemResultado_NaoCnfCntCod ;
      private int A1389ContagemResultado_RdmnIssueId ;
      private int A890ContagemResultado_Responsavel ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A601ContagemResultado_Servico ;
      private int AV38GXV1 ;
      private int Z146Modulo_Codigo ;
      private int Z1389ContagemResultado_RdmnIssueId ;
      private int Z890ContagemResultado_Responsavel ;
      private int Z468ContagemResultado_NaoCnfDmnCod ;
      private int Z490ContagemResultado_ContratadaCod ;
      private int Z805ContagemResultado_ContratadaOrigemCod ;
      private int Z1553ContagemResultado_CntSrvCod ;
      private int Z602ContagemResultado_OSVinculada ;
      private int Z127Sistema_Codigo ;
      private int Z52Contratada_AreaTrabalhoCod ;
      private int Z601ContagemResultado_Servico ;
      private int Z479ContagemResultado_CrFMPessoaCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z462ContagemResultado_Divergencia ;
      private decimal Z458ContagemResultado_PFBFS ;
      private decimal Z459ContagemResultado_PFLFS ;
      private decimal Z460ContagemResultado_PFBFM ;
      private decimal Z461ContagemResultado_PFLFM ;
      private decimal Z800ContagemResultado_Deflator ;
      private decimal Z833ContagemResultado_CstUntPrd ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal AV27ContagemResultado_Divergencia ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal AV19IndiceDivergencia ;
      private decimal AV29ContagemResultado_ValorPF ;
      private decimal GXt_decimal1 ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String wcpOAV18ContagemResultado_HoraCnt ;
      private String Z511ContagemResultado_HoraCnt ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String AV20CalculoDivergencia ;
      private String AV18ContagemResultado_HoraCnt ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultado_DataCnt_Internalname ;
      private String TempTags ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagemresultado_osfsosfm_Internalname ;
      private String lblTextblockcontagemresultado_osfsosfm_Jsonclick ;
      private String edtContagemResultado_OsFsOsFm_Internalname ;
      private String edtContagemResultado_OsFsOsFm_Jsonclick ;
      private String lblTextblockcontagemresultado_datacnt_Internalname ;
      private String lblTextblockcontagemresultado_datacnt_Jsonclick ;
      private String lblTextblockcontagemresultado_contadorfmcod_Internalname ;
      private String lblTextblockcontagemresultado_contadorfmcod_Jsonclick ;
      private String dynContagemResultado_ContadorFMCod_Internalname ;
      private String dynContagemResultado_ContadorFMCod_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfs_Internalname ;
      private String lblTextblockcontagemresultado_pfbfs_Jsonclick ;
      private String edtContagemResultado_PFBFS_Internalname ;
      private String edtContagemResultado_PFBFS_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfs_Internalname ;
      private String lblTextblockcontagemresultado_pflfs_Jsonclick ;
      private String edtContagemResultado_PFLFS_Internalname ;
      private String edtContagemResultado_PFLFS_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfm_Internalname ;
      private String lblTextblockcontagemresultado_pfbfm_Jsonclick ;
      private String edtContagemResultado_PFBFM_Internalname ;
      private String edtContagemResultado_PFBFM_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfm_Internalname ;
      private String lblTextblockcontagemresultado_pflfm_Jsonclick ;
      private String edtContagemResultado_PFLFM_Internalname ;
      private String edtContagemResultado_PFLFM_Jsonclick ;
      private String lblTextblockcontagemresultado_deflator_Internalname ;
      private String lblTextblockcontagemresultado_deflator_Jsonclick ;
      private String edtContagemResultado_Deflator_Internalname ;
      private String edtContagemResultado_Deflator_Jsonclick ;
      private String lblTextblockcontagemresultado_parecertcn_Internalname ;
      private String lblTextblockcontagemresultado_parecertcn_Jsonclick ;
      private String edtContagemResultado_ParecerTcn_Internalname ;
      private String lblTextblockcontagemresultado_naocnfcntcod_Internalname ;
      private String lblTextblockcontagemresultado_naocnfcntcod_Jsonclick ;
      private String dynContagemResultado_NaoCnfCntCod_Internalname ;
      private String dynContagemResultado_NaoCnfCntCod_Jsonclick ;
      private String lblTextblockcontagemresultado_statuscnt_Internalname ;
      private String lblTextblockcontagemresultado_statuscnt_Jsonclick ;
      private String cmbContagemResultado_StatusCnt_Internalname ;
      private String cmbContagemResultado_StatusCnt_Jsonclick ;
      private String lblTextblockcontagemresultado_divergencia_Internalname ;
      private String lblTextblockcontagemresultado_divergencia_Jsonclick ;
      private String lblTextblockcontagemresultado_planilha_Internalname ;
      private String lblTextblockcontagemresultado_planilha_Jsonclick ;
      private String edtContagemResultado_Planilha_Filename ;
      private String A853ContagemResultado_NomePla ;
      private String edtContagemResultado_Planilha_Filetype ;
      private String edtContagemResultado_Planilha_Internalname ;
      private String A854ContagemResultado_TipoPla ;
      private String edtContagemResultado_Planilha_Contenttype ;
      private String edtContagemResultado_Planilha_Parameters ;
      private String edtContagemResultado_Planilha_Jsonclick ;
      private String lblTextblockcontagemresultado_nomepla_Internalname ;
      private String lblTextblockcontagemresultado_nomepla_Jsonclick ;
      private String edtContagemResultado_NomePla_Internalname ;
      private String edtContagemResultado_NomePla_Jsonclick ;
      private String lblTextblockcontagemresultado_tipopla_Internalname ;
      private String lblTextblockcontagemresultado_tipopla_Jsonclick ;
      private String edtContagemResultado_TipoPla_Internalname ;
      private String edtContagemResultado_TipoPla_Jsonclick ;
      private String tblTablemergedcontagemresultado_divergencia_Internalname ;
      private String edtavContagemresultado_divergencia_Internalname ;
      private String edtavContagemresultado_divergencia_Jsonclick ;
      private String lblContagemresultado_divergencia_righttext_Internalname ;
      private String lblContagemresultado_divergencia_righttext_Jsonclick ;
      private String lblTextblockcontagemresultadocontagens_esforco_Internalname ;
      private String lblTextblockcontagemresultadocontagens_esforco_Jsonclick ;
      private String edtContagemResultadoContagens_Esforco_Internalname ;
      private String edtContagemResultadoContagens_Esforco_Jsonclick ;
      private String cellContagemresultadocontagens_esforco_righttext_cell_Internalname ;
      private String lblContagemresultadocontagens_esforco_righttext_Internalname ;
      private String lblContagemresultadocontagens_esforco_righttext_Jsonclick ;
      private String tblTablemergedcontagemresultado_datacnt_Internalname ;
      private String edtContagemResultado_DataCnt_Jsonclick ;
      private String edtContagemResultado_HoraCnt_Internalname ;
      private String A511ContagemResultado_HoraCnt ;
      private String edtContagemResultado_HoraCnt_Jsonclick ;
      private String AV35CalculoPFinal ;
      private String A484ContagemResultado_StatusDmn ;
      private String A478ContagemResultado_NaoCnfCntNom ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String AV37Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String GXCCtlgxBlob ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode72 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z854ContagemResultado_TipoPla ;
      private String Z853ContagemResultado_NomePla ;
      private String Z484ContagemResultado_StatusDmn ;
      private String Z474ContagemResultado_ContadorFMNom ;
      private String Z478ContagemResultado_NaoCnfCntNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private String GXt_char2 ;
      private DateTime Z481ContagemResultado_TimeCnt ;
      private DateTime Z901ContagemResultadoContagens_Prazo ;
      private DateTime A481ContagemResultado_TimeCnt ;
      private DateTime A901ContagemResultadoContagens_Prazo ;
      private DateTime wcpOAV8ContagemResultado_DataCnt ;
      private DateTime Z473ContagemResultado_DataCnt ;
      private DateTime AV8ContagemResultado_DataCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool Z517ContagemResultado_Ultima ;
      private bool entryPointCalled ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool n146Modulo_Codigo ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n852ContagemResultado_Planilha ;
      private bool n800ContagemResultado_Deflator ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n481ContagemResultado_TimeCnt ;
      private bool n901ContagemResultadoContagens_Prazo ;
      private bool n1756ContagemResultado_NvlCnt ;
      private bool A517ContagemResultado_Ultima ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1389ContagemResultado_RdmnIssueId ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool A999ContagemResultado_CrFMEhContratada ;
      private bool n999ContagemResultado_CrFMEhContratada ;
      private bool A1000ContagemResultado_CrFMEhContratante ;
      private bool n1000ContagemResultado_CrFMEhContratante ;
      private bool n478ContagemResultado_NaoCnfCntNom ;
      private bool n513Sistema_Coordenacao ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool n854ContagemResultado_TipoPla ;
      private bool n853ContagemResultado_NomePla ;
      private bool returnInSub ;
      private bool AV34Confirmado ;
      private bool Z485ContagemResultado_EhValidacao ;
      private bool Z999ContagemResultado_CrFMEhContratada ;
      private bool Z1000ContagemResultado_CrFMEhContratante ;
      private bool Gx_longc ;
      private bool i517ContagemResultado_Ultima ;
      private String A463ContagemResultado_ParecerTcn ;
      private String Z463ContagemResultado_ParecerTcn ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A513Sistema_Coordenacao ;
      private String Z457ContagemResultado_Demanda ;
      private String Z493ContagemResultado_DemandaFM ;
      private String Z513Sistema_Coordenacao ;
      private String A852ContagemResultado_Planilha ;
      private String Z852ContagemResultado_Planilha ;
      private IGxSession AV14WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynContagemResultado_ContadorFMCod ;
      private GXCombobox dynContagemResultado_NaoCnfCntCod ;
      private GXCombobox cmbContagemResultado_StatusCnt ;
      private IDataStoreProvider pr_default ;
      private String[] T001V6_A478ContagemResultado_NaoCnfCntNom ;
      private bool[] T001V6_n478ContagemResultado_NaoCnfCntNom ;
      private int[] T001V4_A146Modulo_Codigo ;
      private bool[] T001V4_n146Modulo_Codigo ;
      private bool[] T001V4_A485ContagemResultado_EhValidacao ;
      private bool[] T001V4_n485ContagemResultado_EhValidacao ;
      private String[] T001V4_A457ContagemResultado_Demanda ;
      private bool[] T001V4_n457ContagemResultado_Demanda ;
      private String[] T001V4_A484ContagemResultado_StatusDmn ;
      private bool[] T001V4_n484ContagemResultado_StatusDmn ;
      private int[] T001V4_A1389ContagemResultado_RdmnIssueId ;
      private bool[] T001V4_n1389ContagemResultado_RdmnIssueId ;
      private String[] T001V4_A493ContagemResultado_DemandaFM ;
      private bool[] T001V4_n493ContagemResultado_DemandaFM ;
      private int[] T001V4_A890ContagemResultado_Responsavel ;
      private bool[] T001V4_n890ContagemResultado_Responsavel ;
      private int[] T001V4_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] T001V4_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] T001V4_A490ContagemResultado_ContratadaCod ;
      private bool[] T001V4_n490ContagemResultado_ContratadaCod ;
      private int[] T001V4_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] T001V4_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] T001V4_A1553ContagemResultado_CntSrvCod ;
      private bool[] T001V4_n1553ContagemResultado_CntSrvCod ;
      private int[] T001V4_A602ContagemResultado_OSVinculada ;
      private bool[] T001V4_n602ContagemResultado_OSVinculada ;
      private int[] T001V7_A127Sistema_Codigo ;
      private String[] T001V8_A513Sistema_Coordenacao ;
      private bool[] T001V8_n513Sistema_Coordenacao ;
      private int[] T001V9_A52Contratada_AreaTrabalhoCod ;
      private bool[] T001V9_n52Contratada_AreaTrabalhoCod ;
      private int[] T001V10_A601ContagemResultado_Servico ;
      private bool[] T001V10_n601ContagemResultado_Servico ;
      private bool[] T001V5_A999ContagemResultado_CrFMEhContratada ;
      private bool[] T001V5_n999ContagemResultado_CrFMEhContratada ;
      private bool[] T001V5_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] T001V5_n1000ContagemResultado_CrFMEhContratante ;
      private int[] T001V5_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] T001V5_n479ContagemResultado_CrFMPessoaCod ;
      private String[] T001V11_A474ContagemResultado_ContadorFMNom ;
      private bool[] T001V11_n474ContagemResultado_ContadorFMNom ;
      private int[] T001V12_A146Modulo_Codigo ;
      private bool[] T001V12_n146Modulo_Codigo ;
      private int[] T001V12_A127Sistema_Codigo ;
      private DateTime[] T001V12_A473ContagemResultado_DataCnt ;
      private String[] T001V12_A511ContagemResultado_HoraCnt ;
      private short[] T001V12_A482ContagemResultadoContagens_Esforco ;
      private decimal[] T001V12_A462ContagemResultado_Divergencia ;
      private short[] T001V12_A483ContagemResultado_StatusCnt ;
      private bool[] T001V12_A485ContagemResultado_EhValidacao ;
      private bool[] T001V12_n485ContagemResultado_EhValidacao ;
      private String[] T001V12_A513Sistema_Coordenacao ;
      private bool[] T001V12_n513Sistema_Coordenacao ;
      private String[] T001V12_A457ContagemResultado_Demanda ;
      private bool[] T001V12_n457ContagemResultado_Demanda ;
      private String[] T001V12_A484ContagemResultado_StatusDmn ;
      private bool[] T001V12_n484ContagemResultado_StatusDmn ;
      private DateTime[] T001V12_A481ContagemResultado_TimeCnt ;
      private bool[] T001V12_n481ContagemResultado_TimeCnt ;
      private DateTime[] T001V12_A901ContagemResultadoContagens_Prazo ;
      private bool[] T001V12_n901ContagemResultadoContagens_Prazo ;
      private decimal[] T001V12_A458ContagemResultado_PFBFS ;
      private bool[] T001V12_n458ContagemResultado_PFBFS ;
      private decimal[] T001V12_A459ContagemResultado_PFLFS ;
      private bool[] T001V12_n459ContagemResultado_PFLFS ;
      private decimal[] T001V12_A460ContagemResultado_PFBFM ;
      private bool[] T001V12_n460ContagemResultado_PFBFM ;
      private decimal[] T001V12_A461ContagemResultado_PFLFM ;
      private bool[] T001V12_n461ContagemResultado_PFLFM ;
      private String[] T001V12_A463ContagemResultado_ParecerTcn ;
      private bool[] T001V12_n463ContagemResultado_ParecerTcn ;
      private String[] T001V12_A474ContagemResultado_ContadorFMNom ;
      private bool[] T001V12_n474ContagemResultado_ContadorFMNom ;
      private bool[] T001V12_A999ContagemResultado_CrFMEhContratada ;
      private bool[] T001V12_n999ContagemResultado_CrFMEhContratada ;
      private bool[] T001V12_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] T001V12_n1000ContagemResultado_CrFMEhContratante ;
      private String[] T001V12_A478ContagemResultado_NaoCnfCntNom ;
      private bool[] T001V12_n478ContagemResultado_NaoCnfCntNom ;
      private bool[] T001V12_A517ContagemResultado_Ultima ;
      private decimal[] T001V12_A800ContagemResultado_Deflator ;
      private bool[] T001V12_n800ContagemResultado_Deflator ;
      private decimal[] T001V12_A833ContagemResultado_CstUntPrd ;
      private bool[] T001V12_n833ContagemResultado_CstUntPrd ;
      private int[] T001V12_A1389ContagemResultado_RdmnIssueId ;
      private bool[] T001V12_n1389ContagemResultado_RdmnIssueId ;
      private short[] T001V12_A1756ContagemResultado_NvlCnt ;
      private bool[] T001V12_n1756ContagemResultado_NvlCnt ;
      private String[] T001V12_A854ContagemResultado_TipoPla ;
      private bool[] T001V12_n854ContagemResultado_TipoPla ;
      private String[] T001V12_A853ContagemResultado_NomePla ;
      private bool[] T001V12_n853ContagemResultado_NomePla ;
      private String[] T001V12_A493ContagemResultado_DemandaFM ;
      private bool[] T001V12_n493ContagemResultado_DemandaFM ;
      private int[] T001V12_A456ContagemResultado_Codigo ;
      private int[] T001V12_A470ContagemResultado_ContadorFMCod ;
      private int[] T001V12_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] T001V12_n469ContagemResultado_NaoCnfCntCod ;
      private int[] T001V12_A890ContagemResultado_Responsavel ;
      private bool[] T001V12_n890ContagemResultado_Responsavel ;
      private int[] T001V12_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] T001V12_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] T001V12_A490ContagemResultado_ContratadaCod ;
      private bool[] T001V12_n490ContagemResultado_ContratadaCod ;
      private int[] T001V12_A52Contratada_AreaTrabalhoCod ;
      private bool[] T001V12_n52Contratada_AreaTrabalhoCod ;
      private int[] T001V12_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] T001V12_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] T001V12_A1553ContagemResultado_CntSrvCod ;
      private bool[] T001V12_n1553ContagemResultado_CntSrvCod ;
      private int[] T001V12_A601ContagemResultado_Servico ;
      private bool[] T001V12_n601ContagemResultado_Servico ;
      private int[] T001V12_A602ContagemResultado_OSVinculada ;
      private bool[] T001V12_n602ContagemResultado_OSVinculada ;
      private int[] T001V12_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] T001V12_n479ContagemResultado_CrFMPessoaCod ;
      private String[] T001V12_A852ContagemResultado_Planilha ;
      private bool[] T001V12_n852ContagemResultado_Planilha ;
      private int[] T001V13_A146Modulo_Codigo ;
      private bool[] T001V13_n146Modulo_Codigo ;
      private bool[] T001V13_A485ContagemResultado_EhValidacao ;
      private bool[] T001V13_n485ContagemResultado_EhValidacao ;
      private String[] T001V13_A457ContagemResultado_Demanda ;
      private bool[] T001V13_n457ContagemResultado_Demanda ;
      private String[] T001V13_A484ContagemResultado_StatusDmn ;
      private bool[] T001V13_n484ContagemResultado_StatusDmn ;
      private int[] T001V13_A1389ContagemResultado_RdmnIssueId ;
      private bool[] T001V13_n1389ContagemResultado_RdmnIssueId ;
      private String[] T001V13_A493ContagemResultado_DemandaFM ;
      private bool[] T001V13_n493ContagemResultado_DemandaFM ;
      private int[] T001V13_A890ContagemResultado_Responsavel ;
      private bool[] T001V13_n890ContagemResultado_Responsavel ;
      private int[] T001V13_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] T001V13_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] T001V13_A490ContagemResultado_ContratadaCod ;
      private bool[] T001V13_n490ContagemResultado_ContratadaCod ;
      private int[] T001V13_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] T001V13_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] T001V13_A1553ContagemResultado_CntSrvCod ;
      private bool[] T001V13_n1553ContagemResultado_CntSrvCod ;
      private int[] T001V13_A602ContagemResultado_OSVinculada ;
      private bool[] T001V13_n602ContagemResultado_OSVinculada ;
      private int[] T001V14_A127Sistema_Codigo ;
      private String[] T001V15_A513Sistema_Coordenacao ;
      private bool[] T001V15_n513Sistema_Coordenacao ;
      private int[] T001V16_A52Contratada_AreaTrabalhoCod ;
      private bool[] T001V16_n52Contratada_AreaTrabalhoCod ;
      private int[] T001V17_A601ContagemResultado_Servico ;
      private bool[] T001V17_n601ContagemResultado_Servico ;
      private bool[] T001V18_A999ContagemResultado_CrFMEhContratada ;
      private bool[] T001V18_n999ContagemResultado_CrFMEhContratada ;
      private bool[] T001V18_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] T001V18_n1000ContagemResultado_CrFMEhContratante ;
      private int[] T001V18_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] T001V18_n479ContagemResultado_CrFMPessoaCod ;
      private String[] T001V19_A474ContagemResultado_ContadorFMNom ;
      private bool[] T001V19_n474ContagemResultado_ContadorFMNom ;
      private String[] T001V20_A478ContagemResultado_NaoCnfCntNom ;
      private bool[] T001V20_n478ContagemResultado_NaoCnfCntNom ;
      private int[] T001V21_A456ContagemResultado_Codigo ;
      private DateTime[] T001V21_A473ContagemResultado_DataCnt ;
      private String[] T001V21_A511ContagemResultado_HoraCnt ;
      private DateTime[] T001V3_A473ContagemResultado_DataCnt ;
      private String[] T001V3_A511ContagemResultado_HoraCnt ;
      private short[] T001V3_A482ContagemResultadoContagens_Esforco ;
      private decimal[] T001V3_A462ContagemResultado_Divergencia ;
      private short[] T001V3_A483ContagemResultado_StatusCnt ;
      private DateTime[] T001V3_A481ContagemResultado_TimeCnt ;
      private bool[] T001V3_n481ContagemResultado_TimeCnt ;
      private DateTime[] T001V3_A901ContagemResultadoContagens_Prazo ;
      private bool[] T001V3_n901ContagemResultadoContagens_Prazo ;
      private decimal[] T001V3_A458ContagemResultado_PFBFS ;
      private bool[] T001V3_n458ContagemResultado_PFBFS ;
      private decimal[] T001V3_A459ContagemResultado_PFLFS ;
      private bool[] T001V3_n459ContagemResultado_PFLFS ;
      private decimal[] T001V3_A460ContagemResultado_PFBFM ;
      private bool[] T001V3_n460ContagemResultado_PFBFM ;
      private decimal[] T001V3_A461ContagemResultado_PFLFM ;
      private bool[] T001V3_n461ContagemResultado_PFLFM ;
      private String[] T001V3_A463ContagemResultado_ParecerTcn ;
      private bool[] T001V3_n463ContagemResultado_ParecerTcn ;
      private bool[] T001V3_A517ContagemResultado_Ultima ;
      private decimal[] T001V3_A800ContagemResultado_Deflator ;
      private bool[] T001V3_n800ContagemResultado_Deflator ;
      private decimal[] T001V3_A833ContagemResultado_CstUntPrd ;
      private bool[] T001V3_n833ContagemResultado_CstUntPrd ;
      private short[] T001V3_A1756ContagemResultado_NvlCnt ;
      private bool[] T001V3_n1756ContagemResultado_NvlCnt ;
      private String[] T001V3_A854ContagemResultado_TipoPla ;
      private bool[] T001V3_n854ContagemResultado_TipoPla ;
      private String[] T001V3_A853ContagemResultado_NomePla ;
      private bool[] T001V3_n853ContagemResultado_NomePla ;
      private int[] T001V3_A456ContagemResultado_Codigo ;
      private int[] T001V3_A470ContagemResultado_ContadorFMCod ;
      private int[] T001V3_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] T001V3_n469ContagemResultado_NaoCnfCntCod ;
      private String[] T001V3_A852ContagemResultado_Planilha ;
      private bool[] T001V3_n852ContagemResultado_Planilha ;
      private DateTime[] T001V22_A473ContagemResultado_DataCnt ;
      private String[] T001V22_A511ContagemResultado_HoraCnt ;
      private int[] T001V22_A456ContagemResultado_Codigo ;
      private DateTime[] T001V23_A473ContagemResultado_DataCnt ;
      private String[] T001V23_A511ContagemResultado_HoraCnt ;
      private int[] T001V23_A456ContagemResultado_Codigo ;
      private DateTime[] T001V2_A473ContagemResultado_DataCnt ;
      private String[] T001V2_A511ContagemResultado_HoraCnt ;
      private short[] T001V2_A482ContagemResultadoContagens_Esforco ;
      private decimal[] T001V2_A462ContagemResultado_Divergencia ;
      private short[] T001V2_A483ContagemResultado_StatusCnt ;
      private DateTime[] T001V2_A481ContagemResultado_TimeCnt ;
      private bool[] T001V2_n481ContagemResultado_TimeCnt ;
      private DateTime[] T001V2_A901ContagemResultadoContagens_Prazo ;
      private bool[] T001V2_n901ContagemResultadoContagens_Prazo ;
      private decimal[] T001V2_A458ContagemResultado_PFBFS ;
      private bool[] T001V2_n458ContagemResultado_PFBFS ;
      private decimal[] T001V2_A459ContagemResultado_PFLFS ;
      private bool[] T001V2_n459ContagemResultado_PFLFS ;
      private decimal[] T001V2_A460ContagemResultado_PFBFM ;
      private bool[] T001V2_n460ContagemResultado_PFBFM ;
      private decimal[] T001V2_A461ContagemResultado_PFLFM ;
      private bool[] T001V2_n461ContagemResultado_PFLFM ;
      private String[] T001V2_A463ContagemResultado_ParecerTcn ;
      private bool[] T001V2_n463ContagemResultado_ParecerTcn ;
      private bool[] T001V2_A517ContagemResultado_Ultima ;
      private decimal[] T001V2_A800ContagemResultado_Deflator ;
      private bool[] T001V2_n800ContagemResultado_Deflator ;
      private decimal[] T001V2_A833ContagemResultado_CstUntPrd ;
      private bool[] T001V2_n833ContagemResultado_CstUntPrd ;
      private short[] T001V2_A1756ContagemResultado_NvlCnt ;
      private bool[] T001V2_n1756ContagemResultado_NvlCnt ;
      private String[] T001V2_A854ContagemResultado_TipoPla ;
      private bool[] T001V2_n854ContagemResultado_TipoPla ;
      private String[] T001V2_A853ContagemResultado_NomePla ;
      private bool[] T001V2_n853ContagemResultado_NomePla ;
      private int[] T001V2_A456ContagemResultado_Codigo ;
      private int[] T001V2_A470ContagemResultado_ContadorFMCod ;
      private int[] T001V2_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] T001V2_n469ContagemResultado_NaoCnfCntCod ;
      private String[] T001V2_A852ContagemResultado_Planilha ;
      private bool[] T001V2_n852ContagemResultado_Planilha ;
      private int[] T001V28_A146Modulo_Codigo ;
      private bool[] T001V28_n146Modulo_Codigo ;
      private bool[] T001V28_A485ContagemResultado_EhValidacao ;
      private bool[] T001V28_n485ContagemResultado_EhValidacao ;
      private String[] T001V28_A457ContagemResultado_Demanda ;
      private bool[] T001V28_n457ContagemResultado_Demanda ;
      private String[] T001V28_A484ContagemResultado_StatusDmn ;
      private bool[] T001V28_n484ContagemResultado_StatusDmn ;
      private int[] T001V28_A1389ContagemResultado_RdmnIssueId ;
      private bool[] T001V28_n1389ContagemResultado_RdmnIssueId ;
      private String[] T001V28_A493ContagemResultado_DemandaFM ;
      private bool[] T001V28_n493ContagemResultado_DemandaFM ;
      private int[] T001V28_A890ContagemResultado_Responsavel ;
      private bool[] T001V28_n890ContagemResultado_Responsavel ;
      private int[] T001V28_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] T001V28_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] T001V28_A490ContagemResultado_ContratadaCod ;
      private bool[] T001V28_n490ContagemResultado_ContratadaCod ;
      private int[] T001V28_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] T001V28_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] T001V28_A1553ContagemResultado_CntSrvCod ;
      private bool[] T001V28_n1553ContagemResultado_CntSrvCod ;
      private int[] T001V28_A602ContagemResultado_OSVinculada ;
      private bool[] T001V28_n602ContagemResultado_OSVinculada ;
      private int[] T001V29_A127Sistema_Codigo ;
      private String[] T001V30_A513Sistema_Coordenacao ;
      private bool[] T001V30_n513Sistema_Coordenacao ;
      private int[] T001V31_A52Contratada_AreaTrabalhoCod ;
      private bool[] T001V31_n52Contratada_AreaTrabalhoCod ;
      private int[] T001V32_A601ContagemResultado_Servico ;
      private bool[] T001V32_n601ContagemResultado_Servico ;
      private bool[] T001V33_A999ContagemResultado_CrFMEhContratada ;
      private bool[] T001V33_n999ContagemResultado_CrFMEhContratada ;
      private bool[] T001V33_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] T001V33_n1000ContagemResultado_CrFMEhContratante ;
      private int[] T001V33_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] T001V33_n479ContagemResultado_CrFMPessoaCod ;
      private String[] T001V34_A474ContagemResultado_ContadorFMNom ;
      private bool[] T001V34_n474ContagemResultado_ContadorFMNom ;
      private String[] T001V35_A478ContagemResultado_NaoCnfCntNom ;
      private bool[] T001V35_n478ContagemResultado_NaoCnfCntNom ;
      private int[] T001V36_A456ContagemResultado_Codigo ;
      private DateTime[] T001V36_A473ContagemResultado_DataCnt ;
      private String[] T001V36_A511ContagemResultado_HoraCnt ;
      private int[] T001V37_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] T001V37_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] T001V37_A69ContratadaUsuario_UsuarioCod ;
      private String[] T001V37_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] T001V37_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] T001V37_A66ContratadaUsuario_ContratadaCod ;
      private bool[] T001V37_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] T001V37_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] T001V38_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] T001V38_n469ContagemResultado_NaoCnfCntCod ;
      private String[] T001V38_A478ContagemResultado_NaoCnfCntNom ;
      private bool[] T001V38_n478ContagemResultado_NaoCnfCntNom ;
      private int[] T001V39_A146Modulo_Codigo ;
      private bool[] T001V39_n146Modulo_Codigo ;
      private bool[] T001V39_A485ContagemResultado_EhValidacao ;
      private bool[] T001V39_n485ContagemResultado_EhValidacao ;
      private String[] T001V39_A457ContagemResultado_Demanda ;
      private bool[] T001V39_n457ContagemResultado_Demanda ;
      private String[] T001V39_A484ContagemResultado_StatusDmn ;
      private bool[] T001V39_n484ContagemResultado_StatusDmn ;
      private int[] T001V39_A1389ContagemResultado_RdmnIssueId ;
      private bool[] T001V39_n1389ContagemResultado_RdmnIssueId ;
      private String[] T001V39_A493ContagemResultado_DemandaFM ;
      private bool[] T001V39_n493ContagemResultado_DemandaFM ;
      private int[] T001V39_A890ContagemResultado_Responsavel ;
      private bool[] T001V39_n890ContagemResultado_Responsavel ;
      private int[] T001V39_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] T001V39_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] T001V39_A490ContagemResultado_ContratadaCod ;
      private bool[] T001V39_n490ContagemResultado_ContratadaCod ;
      private int[] T001V39_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] T001V39_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] T001V39_A1553ContagemResultado_CntSrvCod ;
      private bool[] T001V39_n1553ContagemResultado_CntSrvCod ;
      private int[] T001V39_A602ContagemResultado_OSVinculada ;
      private bool[] T001V39_n602ContagemResultado_OSVinculada ;
      private int[] T001V40_A127Sistema_Codigo ;
      private String[] T001V41_A513Sistema_Coordenacao ;
      private bool[] T001V41_n513Sistema_Coordenacao ;
      private int[] T001V42_A52Contratada_AreaTrabalhoCod ;
      private bool[] T001V42_n52Contratada_AreaTrabalhoCod ;
      private int[] T001V43_A601ContagemResultado_Servico ;
      private bool[] T001V43_n601ContagemResultado_Servico ;
      private bool[] T001V44_A999ContagemResultado_CrFMEhContratada ;
      private bool[] T001V44_n999ContagemResultado_CrFMEhContratada ;
      private bool[] T001V44_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] T001V44_n1000ContagemResultado_CrFMEhContratante ;
      private int[] T001V44_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] T001V44_n479ContagemResultado_CrFMPessoaCod ;
      private String[] T001V45_A474ContagemResultado_ContadorFMNom ;
      private bool[] T001V45_n474ContagemResultado_ContadorFMNom ;
      private String[] T001V46_A478ContagemResultado_NaoCnfCntNom ;
      private bool[] T001V46_n478ContagemResultado_NaoCnfCntNom ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV12TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV15WWPContext ;
   }

   public class contagemresultadocontagens__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new UpdateCursor(def[22])
         ,new UpdateCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new UpdateCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001V12 ;
          prmT001V12 = new Object[] {
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT001V12 ;
          cmdBufferT001V12=" SELECT T2.[Modulo_Codigo], T3.[Sistema_Codigo], TM1.[ContagemResultado_DataCnt], TM1.[ContagemResultado_HoraCnt], TM1.[ContagemResultadoContagens_Esforco], TM1.[ContagemResultado_Divergencia], TM1.[ContagemResultado_StatusCnt], T2.[ContagemResultado_EhValidacao], T4.[Sistema_Coordenacao], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_StatusDmn], TM1.[ContagemResultado_TimeCnt], TM1.[ContagemResultadoContagens_Prazo], TM1.[ContagemResultado_PFBFS], TM1.[ContagemResultado_PFLFS], TM1.[ContagemResultado_PFBFM], TM1.[ContagemResultado_PFLFM], TM1.[ContagemResultado_ParecerTcn], T8.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom, T7.[Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada, T7.[Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, T9.[NaoConformidade_Nome] AS ContagemResultado_NaoCnfCntNom, TM1.[ContagemResultado_Ultima], TM1.[ContagemResultado_Deflator], TM1.[ContagemResultado_CstUntPrd], T2.[ContagemResultado_RdmnIssueId], TM1.[ContagemResultado_NvlCnt], TM1.[ContagemResultado_TipoPla], TM1.[ContagemResultado_NomePla], T2.[ContagemResultado_DemandaFM], TM1.[ContagemResultado_Codigo], TM1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, TM1.[ContagemResultado_NaoCnfCntCod] AS ContagemResultado_NaoCnfCntCod, T2.[ContagemResultado_Responsavel], T2.[ContagemResultado_NaoCnfDmnCod], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T5.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_ContratadaOrigemCod], T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T6.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_OSVinculada], T7.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, TM1.[ContagemResultado_Planilha] FROM (((((((([ContagemResultadoContagens] TM1 WITH (NOLOCK) "
          + " INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[ContagemResultado_Codigo]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T2.[Modulo_Codigo]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T3.[Sistema_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T6 WITH (NOLOCK) ON T6.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) INNER JOIN [Usuario] T7 WITH (NOLOCK) ON T7.[Usuario_Codigo] = TM1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T8 WITH (NOLOCK) ON T8.[Pessoa_Codigo] = T7.[Usuario_PessoaCod]) LEFT JOIN [NaoConformidade] T9 WITH (NOLOCK) ON T9.[NaoConformidade_Codigo] = TM1.[ContagemResultado_NaoCnfCntCod]) WHERE TM1.[ContagemResultado_DataCnt] = @ContagemResultado_DataCnt and TM1.[ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt and TM1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY TM1.[ContagemResultado_Codigo], TM1.[ContagemResultado_DataCnt], TM1.[ContagemResultado_HoraCnt]  OPTION (FAST 100)" ;
          Object[] prmT001V4 ;
          prmT001V4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V7 ;
          prmT001V7 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V8 ;
          prmT001V8 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V9 ;
          prmT001V9 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V10 ;
          prmT001V10 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V5 ;
          prmT001V5 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V11 ;
          prmT001V11 = new Object[] {
          new Object[] {"@ContagemResultado_CrFMPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V6 ;
          prmT001V6 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V13 ;
          prmT001V13 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V14 ;
          prmT001V14 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V15 ;
          prmT001V15 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V16 ;
          prmT001V16 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V17 ;
          prmT001V17 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V18 ;
          prmT001V18 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V19 ;
          prmT001V19 = new Object[] {
          new Object[] {"@ContagemResultado_CrFMPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V20 ;
          prmT001V20 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V21 ;
          prmT001V21 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmT001V3 ;
          prmT001V3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmT001V22 ;
          prmT001V22 = new Object[] {
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V23 ;
          prmT001V23 = new Object[] {
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V2 ;
          prmT001V2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmT001V24 ;
          prmT001V24 = new Object[] {
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_TimeCnt",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultadoContagens_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_NvlCnt",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V25 ;
          prmT001V25 = new Object[] {
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_TimeCnt",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultadoContagens_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_NvlCnt",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmT001V26 ;
          prmT001V26 = new Object[] {
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmT001V27 ;
          prmT001V27 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmT001V28 ;
          prmT001V28 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V29 ;
          prmT001V29 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V30 ;
          prmT001V30 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V31 ;
          prmT001V31 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V32 ;
          prmT001V32 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V33 ;
          prmT001V33 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V34 ;
          prmT001V34 = new Object[] {
          new Object[] {"@ContagemResultado_CrFMPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V35 ;
          prmT001V35 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V36 ;
          prmT001V36 = new Object[] {
          } ;
          Object[] prmT001V37 ;
          prmT001V37 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V38 ;
          prmT001V38 = new Object[] {
          } ;
          Object[] prmT001V39 ;
          prmT001V39 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V40 ;
          prmT001V40 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V41 ;
          prmT001V41 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V42 ;
          prmT001V42 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V43 ;
          prmT001V43 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V44 ;
          prmT001V44 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V45 ;
          prmT001V45 = new Object[] {
          new Object[] {"@ContagemResultado_CrFMPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001V46 ;
          prmT001V46 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001V2", "SELECT [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Divergencia], [ContagemResultado_StatusCnt], [ContagemResultado_TimeCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_ParecerTcn], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_NvlCnt], [ContagemResultado_TipoPla], [ContagemResultado_NomePla], [ContagemResultado_Codigo], [ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, [ContagemResultado_NaoCnfCntCod] AS ContagemResultado_NaoCnfCntCod, [ContagemResultado_Planilha] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V2,1,0,true,false )
             ,new CursorDef("T001V3", "SELECT [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Divergencia], [ContagemResultado_StatusCnt], [ContagemResultado_TimeCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_ParecerTcn], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_NvlCnt], [ContagemResultado_TipoPla], [ContagemResultado_NomePla], [ContagemResultado_Codigo], [ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, [ContagemResultado_NaoCnfCntCod] AS ContagemResultado_NaoCnfCntCod, [ContagemResultado_Planilha] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V3,1,0,true,false )
             ,new CursorDef("T001V4", "SELECT [Modulo_Codigo], [ContagemResultado_EhValidacao], [ContagemResultado_Demanda], [ContagemResultado_StatusDmn], [ContagemResultado_RdmnIssueId], [ContagemResultado_DemandaFM], [ContagemResultado_Responsavel], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V4,1,0,true,false )
             ,new CursorDef("T001V5", "SELECT [Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada, [Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, [Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFMCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V5,1,0,true,false )
             ,new CursorDef("T001V6", "SELECT [NaoConformidade_Nome] AS ContagemResultado_NaoCnfCntNom FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V6,1,0,true,false )
             ,new CursorDef("T001V7", "SELECT [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V7,1,0,true,false )
             ,new CursorDef("T001V8", "SELECT [Sistema_Coordenacao] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V8,1,0,true,false )
             ,new CursorDef("T001V9", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V9,1,0,true,false )
             ,new CursorDef("T001V10", "SELECT [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V10,1,0,true,false )
             ,new CursorDef("T001V11", "SELECT [Pessoa_Nome] AS ContagemResultado_ContadorFMNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_CrFMPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V11,1,0,true,false )
             ,new CursorDef("T001V12", cmdBufferT001V12,true, GxErrorMask.GX_NOMASK, false, this,prmT001V12,100,0,true,false )
             ,new CursorDef("T001V13", "SELECT [Modulo_Codigo], [ContagemResultado_EhValidacao], [ContagemResultado_Demanda], [ContagemResultado_StatusDmn], [ContagemResultado_RdmnIssueId], [ContagemResultado_DemandaFM], [ContagemResultado_Responsavel], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V13,1,0,true,false )
             ,new CursorDef("T001V14", "SELECT [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V14,1,0,true,false )
             ,new CursorDef("T001V15", "SELECT [Sistema_Coordenacao] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V15,1,0,true,false )
             ,new CursorDef("T001V16", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V16,1,0,true,false )
             ,new CursorDef("T001V17", "SELECT [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V17,1,0,true,false )
             ,new CursorDef("T001V18", "SELECT [Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada, [Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, [Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFMCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V18,1,0,true,false )
             ,new CursorDef("T001V19", "SELECT [Pessoa_Nome] AS ContagemResultado_ContadorFMNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_CrFMPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V19,1,0,true,false )
             ,new CursorDef("T001V20", "SELECT [NaoConformidade_Nome] AS ContagemResultado_NaoCnfCntNom FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V20,1,0,true,false )
             ,new CursorDef("T001V21", "SELECT [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001V21,1,0,true,false )
             ,new CursorDef("T001V22", "SELECT TOP 1 [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ( [ContagemResultado_DataCnt] > @ContagemResultado_DataCnt or [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt and [ContagemResultado_HoraCnt] > @ContagemResultado_HoraCnt or [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt and [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt and [ContagemResultado_Codigo] > @ContagemResultado_Codigo) ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001V22,1,0,true,true )
             ,new CursorDef("T001V23", "SELECT TOP 1 [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ( [ContagemResultado_DataCnt] < @ContagemResultado_DataCnt or [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt and [ContagemResultado_HoraCnt] < @ContagemResultado_HoraCnt or [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt and [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt and [ContagemResultado_Codigo] < @ContagemResultado_Codigo) ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001V23,1,0,true,true )
             ,new CursorDef("T001V24", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Divergencia], [ContagemResultado_StatusCnt], [ContagemResultado_TimeCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_ParecerTcn], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NvlCnt], [ContagemResultado_TipoPla], [ContagemResultado_NomePla], [ContagemResultado_Codigo], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod]) VALUES(@ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Divergencia, @ContagemResultado_StatusCnt, @ContagemResultado_TimeCnt, @ContagemResultadoContagens_Prazo, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_ParecerTcn, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, @ContagemResultado_Planilha, @ContagemResultado_NvlCnt, @ContagemResultado_TipoPla, @ContagemResultado_NomePla, @ContagemResultado_Codigo, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod)", GxErrorMask.GX_NOMASK,prmT001V24)
             ,new CursorDef("T001V25", "UPDATE [ContagemResultadoContagens] SET [ContagemResultadoContagens_Esforco]=@ContagemResultadoContagens_Esforco, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_TimeCnt]=@ContagemResultado_TimeCnt, [ContagemResultadoContagens_Prazo]=@ContagemResultadoContagens_Prazo, [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_PFBFM]=@ContagemResultado_PFBFM, [ContagemResultado_PFLFM]=@ContagemResultado_PFLFM, [ContagemResultado_ParecerTcn]=@ContagemResultado_ParecerTcn, [ContagemResultado_Ultima]=@ContagemResultado_Ultima, [ContagemResultado_Deflator]=@ContagemResultado_Deflator, [ContagemResultado_CstUntPrd]=@ContagemResultado_CstUntPrd, [ContagemResultado_NvlCnt]=@ContagemResultado_NvlCnt, [ContagemResultado_TipoPla]=@ContagemResultado_TipoPla, [ContagemResultado_NomePla]=@ContagemResultado_NomePla, [ContagemResultado_ContadorFMCod]=@ContagemResultado_ContadorFMCod, [ContagemResultado_NaoCnfCntCod]=@ContagemResultado_NaoCnfCntCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK,prmT001V25)
             ,new CursorDef("T001V26", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Planilha]=@ContagemResultado_Planilha  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK,prmT001V26)
             ,new CursorDef("T001V27", "DELETE FROM [ContagemResultadoContagens]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK,prmT001V27)
             ,new CursorDef("T001V28", "SELECT [Modulo_Codigo], [ContagemResultado_EhValidacao], [ContagemResultado_Demanda], [ContagemResultado_StatusDmn], [ContagemResultado_RdmnIssueId], [ContagemResultado_DemandaFM], [ContagemResultado_Responsavel], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V28,1,0,true,false )
             ,new CursorDef("T001V29", "SELECT [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V29,1,0,true,false )
             ,new CursorDef("T001V30", "SELECT [Sistema_Coordenacao] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V30,1,0,true,false )
             ,new CursorDef("T001V31", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V31,1,0,true,false )
             ,new CursorDef("T001V32", "SELECT [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V32,1,0,true,false )
             ,new CursorDef("T001V33", "SELECT [Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada, [Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, [Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFMCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V33,1,0,true,false )
             ,new CursorDef("T001V34", "SELECT [Pessoa_Nome] AS ContagemResultado_ContadorFMNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_CrFMPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V34,1,0,true,false )
             ,new CursorDef("T001V35", "SELECT [NaoConformidade_Nome] AS ContagemResultado_NaoCnfCntNom FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V35,1,0,true,false )
             ,new CursorDef("T001V36", "SELECT [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001V36,100,0,true,false )
             ,new CursorDef("T001V37", "SELECT T3.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod], T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T2.[Usuario_Ativo] = 1) AND (T1.[ContratadaUsuario_ContratadaCod] = @ContagemResultado_ContratadaCod) ORDER BY T3.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V37,0,0,true,false )
             ,new CursorDef("T001V38", "SELECT [NaoConformidade_Codigo] AS ContagemResultado_NaoCnfCntCod, [NaoConformidade_Nome] AS ContagemResultado_NaoCnfCntNom FROM [NaoConformidade] WITH (NOLOCK) ORDER BY [NaoConformidade_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V38,0,0,true,false )
             ,new CursorDef("T001V39", "SELECT [Modulo_Codigo], [ContagemResultado_EhValidacao], [ContagemResultado_Demanda], [ContagemResultado_StatusDmn], [ContagemResultado_RdmnIssueId], [ContagemResultado_DemandaFM], [ContagemResultado_Responsavel], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V39,1,0,true,false )
             ,new CursorDef("T001V40", "SELECT [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V40,1,0,true,false )
             ,new CursorDef("T001V41", "SELECT [Sistema_Coordenacao] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V41,1,0,true,false )
             ,new CursorDef("T001V42", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V42,1,0,true,false )
             ,new CursorDef("T001V43", "SELECT [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V43,1,0,true,false )
             ,new CursorDef("T001V44", "SELECT [Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada, [Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, [Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFMCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V44,1,0,true,false )
             ,new CursorDef("T001V45", "SELECT [Pessoa_Nome] AS ContagemResultado_ContadorFMNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_CrFMPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V45,1,0,true,false )
             ,new CursorDef("T001V46", "SELECT [NaoConformidade_Nome] AS ContagemResultado_NaoCnfCntNom FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001V46,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 5) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((bool[]) buf[19])[0] = rslt.getBool(13) ;
                ((decimal[]) buf[20])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((short[]) buf[24])[0] = rslt.getShort(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((String[]) buf[26])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((String[]) buf[28])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((int[]) buf[30])[0] = rslt.getInt(19) ;
                ((int[]) buf[31])[0] = rslt.getInt(20) ;
                ((int[]) buf[32])[0] = rslt.getInt(21) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(21);
                ((String[]) buf[34])[0] = rslt.getBLOBFile(22, rslt.getString(17, 10), rslt.getString(18, 50)) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 5) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((bool[]) buf[19])[0] = rslt.getBool(13) ;
                ((decimal[]) buf[20])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((short[]) buf[24])[0] = rslt.getShort(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((String[]) buf[26])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((String[]) buf[28])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((int[]) buf[30])[0] = rslt.getInt(19) ;
                ((int[]) buf[31])[0] = rslt.getInt(20) ;
                ((int[]) buf[32])[0] = rslt.getInt(21) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(21);
                ((String[]) buf[34])[0] = rslt.getBLOBFile(22, rslt.getString(17, 10), rslt.getString(18, 50)) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 5) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[7])[0] = rslt.getShort(7) ;
                ((bool[]) buf[8])[0] = rslt.getBool(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((String[]) buf[10])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((String[]) buf[12])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((decimal[]) buf[24])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((String[]) buf[28])[0] = rslt.getLongVarchar(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((String[]) buf[30])[0] = rslt.getString(19, 100) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((bool[]) buf[32])[0] = rslt.getBool(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((bool[]) buf[34])[0] = rslt.getBool(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((String[]) buf[36])[0] = rslt.getString(22, 50) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((bool[]) buf[38])[0] = rslt.getBool(23) ;
                ((decimal[]) buf[39])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(24);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(25);
                ((int[]) buf[43])[0] = rslt.getInt(26) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(26);
                ((short[]) buf[45])[0] = rslt.getShort(27) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(27);
                ((String[]) buf[47])[0] = rslt.getString(28, 10) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(28);
                ((String[]) buf[49])[0] = rslt.getString(29, 50) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(29);
                ((String[]) buf[51])[0] = rslt.getVarchar(30) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(30);
                ((int[]) buf[53])[0] = rslt.getInt(31) ;
                ((int[]) buf[54])[0] = rslt.getInt(32) ;
                ((int[]) buf[55])[0] = rslt.getInt(33) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(33);
                ((int[]) buf[57])[0] = rslt.getInt(34) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(34);
                ((int[]) buf[59])[0] = rslt.getInt(35) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(35);
                ((int[]) buf[61])[0] = rslt.getInt(36) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(36);
                ((int[]) buf[63])[0] = rslt.getInt(37) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(37);
                ((int[]) buf[65])[0] = rslt.getInt(38) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(38);
                ((int[]) buf[67])[0] = rslt.getInt(39) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(39);
                ((int[]) buf[69])[0] = rslt.getInt(40) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(40);
                ((int[]) buf[71])[0] = rslt.getInt(41) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(41);
                ((int[]) buf[73])[0] = rslt.getInt(42) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(42);
                ((String[]) buf[75])[0] = rslt.getBLOBFile(43, rslt.getString(28, 10), rslt.getString(29, 50)) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(43);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                return;
             case 20 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 5) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 21 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 5) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 31 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 32 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 33 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 39 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 42 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 43 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 44 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 20 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 21 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 22 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(7, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[18]);
                }
                stmt.SetParameter(13, (bool)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 16 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 17 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(17, (short)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 18 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[31]);
                }
                stmt.SetParameter(20, (int)parms[32]);
                stmt.SetParameter(21, (int)parms[33]);
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[35]);
                }
                return;
             case 23 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                stmt.SetParameter(11, (bool)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 15 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[27]);
                }
                stmt.SetParameter(17, (int)parms[28]);
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[30]);
                }
                stmt.SetParameter(19, (int)parms[31]);
                stmt.SetParameter(20, (DateTime)parms[32]);
                stmt.SetParameter(21, (String)parms[33]);
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (DateTime)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 33 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 35 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 40 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 41 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 42 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 43 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 44 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
