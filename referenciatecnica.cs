/*
               File: ReferenciaTecnica
        Description: Refer�ncia T�cnica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:58:27.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class referenciatecnica : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A93Guia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A93Guia_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ReferenciaTecnica_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaTecnica_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREFERENCIATECNICA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ReferenciaTecnica_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbReferenciaTecnica_Unidade.Name = "REFERENCIATECNICA_UNIDADE";
         cmbReferenciaTecnica_Unidade.WebTags = "";
         cmbReferenciaTecnica_Unidade.addItem("1", "Percentual", 0);
         cmbReferenciaTecnica_Unidade.addItem("2", "Ponto Fun��o", 0);
         if ( cmbReferenciaTecnica_Unidade.ItemCount > 0 )
         {
            A114ReferenciaTecnica_Unidade = (short)(NumberUtil.Val( cmbReferenciaTecnica_Unidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A114ReferenciaTecnica_Unidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Refer�ncia T�cnica", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtReferenciaTecnica_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public referenciatecnica( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public referenciatecnica( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ReferenciaTecnica_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ReferenciaTecnica_Codigo = aP1_ReferenciaTecnica_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbReferenciaTecnica_Unidade = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbReferenciaTecnica_Unidade.ItemCount > 0 )
         {
            A114ReferenciaTecnica_Unidade = (short)(NumberUtil.Val( cmbReferenciaTecnica_Unidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A114ReferenciaTecnica_Unidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0I19( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0I19e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0I19( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblReferenciatecnicatitle_Internalname, "Refer�ncia T�cnica", "", "", lblReferenciatecnicatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_0I19( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_0I19e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_54_0I19( true) ;
         }
         return  ;
      }

      protected void wb_table3_54_0I19e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0I19e( true) ;
         }
         else
         {
            wb_table1_2_0I19e( false) ;
         }
      }

      protected void wb_table3_54_0I19( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_54_0I19e( true) ;
         }
         else
         {
            wb_table3_54_0I19e( false) ;
         }
      }

      protected void wb_table2_8_0I19( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_0I19( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_0I19e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_0I19e( true) ;
         }
         else
         {
            wb_table2_8_0I19e( false) ;
         }
      }

      protected void wb_table4_16_0I19( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciatecnica_codigo_Internalname, "C�digo", "", "", lblTextblockreferenciatecnica_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtReferenciaTecnica_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0, ",", "")), ((edtReferenciaTecnica_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A97ReferenciaTecnica_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A97ReferenciaTecnica_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReferenciaTecnica_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtReferenciaTecnica_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciatecnica_nome_Internalname, "Refer�ncia", "", "", lblTextblockreferenciatecnica_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtReferenciaTecnica_Nome_Internalname, StringUtil.RTrim( A98ReferenciaTecnica_Nome), StringUtil.RTrim( context.localUtil.Format( A98ReferenciaTecnica_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReferenciaTecnica_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtReferenciaTecnica_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciatecnica_descricao_Internalname, "Descri��o", "", "", lblTextblockreferenciatecnica_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtReferenciaTecnica_Descricao_Internalname, A99ReferenciaTecnica_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", 0, 1, edtReferenciaTecnica_Descricao_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciatecnica_unidade_Internalname, "Unidade", "", "", lblTextblockreferenciatecnica_unidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbReferenciaTecnica_Unidade, cmbReferenciaTecnica_Unidade_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)), 1, cmbReferenciaTecnica_Unidade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbReferenciaTecnica_Unidade.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_ReferenciaTecnica.htm");
            cmbReferenciaTecnica_Unidade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbReferenciaTecnica_Unidade_Internalname, "Values", (String)(cmbReferenciaTecnica_Unidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciatecnica_valor_Internalname, "Valor", "", "", lblTextblockreferenciatecnica_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtReferenciaTecnica_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A100ReferenciaTecnica_Valor, 18, 5, ",", "")), ((edtReferenciaTecnica_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A100ReferenciaTecnica_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A100ReferenciaTecnica_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReferenciaTecnica_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtReferenciaTecnica_Valor_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockguia_codigo_Internalname, "Guia", "", "", lblTextblockguia_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_46_0I19( true) ;
         }
         return  ;
      }

      protected void wb_table5_46_0I19e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_0I19e( true) ;
         }
         else
         {
            wb_table4_16_0I19e( false) ;
         }
      }

      protected void wb_table5_46_0I19( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedguia_codigo_Internalname, tblTablemergedguia_codigo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGuia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A93Guia_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGuia_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ReferenciaTecnica.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_93_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_93_Link, "", "", context.GetTheme( ), imgprompt_93_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGuia_Nome_Internalname, StringUtil.RTrim( A94Guia_Nome), StringUtil.RTrim( context.localUtil.Format( A94Guia_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGuia_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_46_0I19e( true) ;
         }
         else
         {
            wb_table5_46_0I19e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110I2 */
         E110I2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A97ReferenciaTecnica_Codigo = (int)(context.localUtil.CToN( cgiGet( edtReferenciaTecnica_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
               A98ReferenciaTecnica_Nome = StringUtil.Upper( cgiGet( edtReferenciaTecnica_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A98ReferenciaTecnica_Nome", A98ReferenciaTecnica_Nome);
               A99ReferenciaTecnica_Descricao = cgiGet( edtReferenciaTecnica_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A99ReferenciaTecnica_Descricao", A99ReferenciaTecnica_Descricao);
               cmbReferenciaTecnica_Unidade.CurrentValue = cgiGet( cmbReferenciaTecnica_Unidade_Internalname);
               A114ReferenciaTecnica_Unidade = (short)(NumberUtil.Val( cgiGet( cmbReferenciaTecnica_Unidade_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A114ReferenciaTecnica_Unidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtReferenciaTecnica_Valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtReferenciaTecnica_Valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REFERENCIATECNICA_VALOR");
                  AnyError = 1;
                  GX_FocusControl = edtReferenciaTecnica_Valor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A100ReferenciaTecnica_Valor = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A100ReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( A100ReferenciaTecnica_Valor, 18, 5)));
               }
               else
               {
                  A100ReferenciaTecnica_Valor = context.localUtil.CToN( cgiGet( edtReferenciaTecnica_Valor_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A100ReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( A100ReferenciaTecnica_Valor, 18, 5)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GUIA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtGuia_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A93Guia_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
               }
               else
               {
                  A93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
               }
               A94Guia_Nome = StringUtil.Upper( cgiGet( edtGuia_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
               /* Read saved values. */
               Z97ReferenciaTecnica_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z97ReferenciaTecnica_Codigo"), ",", "."));
               Z98ReferenciaTecnica_Nome = cgiGet( "Z98ReferenciaTecnica_Nome");
               Z114ReferenciaTecnica_Unidade = (short)(context.localUtil.CToN( cgiGet( "Z114ReferenciaTecnica_Unidade"), ",", "."));
               Z100ReferenciaTecnica_Valor = context.localUtil.CToN( cgiGet( "Z100ReferenciaTecnica_Valor"), ",", ".");
               Z430ReferenciaTecnica_Tipo = cgiGet( "Z430ReferenciaTecnica_Tipo");
               Z93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z93Guia_Codigo"), ",", "."));
               A430ReferenciaTecnica_Tipo = cgiGet( "Z430ReferenciaTecnica_Tipo");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( "N93Guia_Codigo"), ",", "."));
               AV7ReferenciaTecnica_Codigo = (int)(context.localUtil.CToN( cgiGet( "vREFERENCIATECNICA_CODIGO"), ",", "."));
               AV11Insert_Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_GUIA_CODIGO"), ",", "."));
               A430ReferenciaTecnica_Tipo = cgiGet( "REFERENCIATECNICA_TIPO");
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ReferenciaTecnica";
               A97ReferenciaTecnica_Codigo = (int)(context.localUtil.CToN( cgiGet( edtReferenciaTecnica_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A97ReferenciaTecnica_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A430ReferenciaTecnica_Tipo, ""));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A97ReferenciaTecnica_Codigo != Z97ReferenciaTecnica_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("referenciatecnica:[SecurityCheckFailed value for]"+"ReferenciaTecnica_Codigo:"+context.localUtil.Format( (decimal)(A97ReferenciaTecnica_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("referenciatecnica:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("referenciatecnica:[SecurityCheckFailed value for]"+"ReferenciaTecnica_Tipo:"+StringUtil.RTrim( context.localUtil.Format( A430ReferenciaTecnica_Tipo, "")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A97ReferenciaTecnica_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode19 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode19;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound19 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0I0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "REFERENCIATECNICA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtReferenciaTecnica_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110I2 */
                           E110I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120I2 */
                           E120I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120I2 */
            E120I2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0I19( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0I19( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0I0( )
      {
         BeforeValidate0I19( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0I19( ) ;
            }
            else
            {
               CheckExtendedTable0I19( ) ;
               CloseExtendedTableCursors0I19( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0I0( )
      {
      }

      protected void E110I2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Guia_Codigo") == 0 )
               {
                  AV11Insert_Guia_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Guia_Codigo), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
      }

      protected void E120I2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwreferenciatecnica.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0I19( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z98ReferenciaTecnica_Nome = T000I3_A98ReferenciaTecnica_Nome[0];
               Z114ReferenciaTecnica_Unidade = T000I3_A114ReferenciaTecnica_Unidade[0];
               Z100ReferenciaTecnica_Valor = T000I3_A100ReferenciaTecnica_Valor[0];
               Z430ReferenciaTecnica_Tipo = T000I3_A430ReferenciaTecnica_Tipo[0];
               Z93Guia_Codigo = T000I3_A93Guia_Codigo[0];
            }
            else
            {
               Z98ReferenciaTecnica_Nome = A98ReferenciaTecnica_Nome;
               Z114ReferenciaTecnica_Unidade = A114ReferenciaTecnica_Unidade;
               Z100ReferenciaTecnica_Valor = A100ReferenciaTecnica_Valor;
               Z430ReferenciaTecnica_Tipo = A430ReferenciaTecnica_Tipo;
               Z93Guia_Codigo = A93Guia_Codigo;
            }
         }
         if ( GX_JID == -9 )
         {
            Z97ReferenciaTecnica_Codigo = A97ReferenciaTecnica_Codigo;
            Z98ReferenciaTecnica_Nome = A98ReferenciaTecnica_Nome;
            Z99ReferenciaTecnica_Descricao = A99ReferenciaTecnica_Descricao;
            Z114ReferenciaTecnica_Unidade = A114ReferenciaTecnica_Unidade;
            Z100ReferenciaTecnica_Valor = A100ReferenciaTecnica_Valor;
            Z430ReferenciaTecnica_Tipo = A430ReferenciaTecnica_Tipo;
            Z93Guia_Codigo = A93Guia_Codigo;
            Z94Guia_Nome = A94Guia_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         edtReferenciaTecnica_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaTecnica_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaTecnica_Codigo_Enabled), 5, 0)));
         AV13Pgmname = "ReferenciaTecnica";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         imgprompt_93_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptguia.aspx"+"',["+"{Ctrl:gx.dom.el('"+"GUIA_CODIGO"+"'), id:'"+"GUIA_CODIGO"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"GUIA_NOME"+"'), id:'"+"GUIA_NOME"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         edtReferenciaTecnica_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaTecnica_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaTecnica_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ReferenciaTecnica_Codigo) )
         {
            A97ReferenciaTecnica_Codigo = AV7ReferenciaTecnica_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Guia_Codigo) )
         {
            edtGuia_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtGuia_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Codigo_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Guia_Codigo) )
         {
            A93Guia_Codigo = AV11Insert_Guia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000I4 */
            pr_default.execute(2, new Object[] {A93Guia_Codigo});
            A94Guia_Nome = T000I4_A94Guia_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
            pr_default.close(2);
         }
      }

      protected void Load0I19( )
      {
         /* Using cursor T000I5 */
         pr_default.execute(3, new Object[] {A97ReferenciaTecnica_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound19 = 1;
            A98ReferenciaTecnica_Nome = T000I5_A98ReferenciaTecnica_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A98ReferenciaTecnica_Nome", A98ReferenciaTecnica_Nome);
            A99ReferenciaTecnica_Descricao = T000I5_A99ReferenciaTecnica_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A99ReferenciaTecnica_Descricao", A99ReferenciaTecnica_Descricao);
            A114ReferenciaTecnica_Unidade = T000I5_A114ReferenciaTecnica_Unidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A114ReferenciaTecnica_Unidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)));
            A100ReferenciaTecnica_Valor = T000I5_A100ReferenciaTecnica_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A100ReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( A100ReferenciaTecnica_Valor, 18, 5)));
            A430ReferenciaTecnica_Tipo = T000I5_A430ReferenciaTecnica_Tipo[0];
            A94Guia_Nome = T000I5_A94Guia_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
            A93Guia_Codigo = T000I5_A93Guia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
            ZM0I19( -9) ;
         }
         pr_default.close(3);
         OnLoadActions0I19( ) ;
      }

      protected void OnLoadActions0I19( )
      {
      }

      protected void CheckExtendedTable0I19( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( ( A114ReferenciaTecnica_Unidade == 1 ) || ( A114ReferenciaTecnica_Unidade == 2 ) ) )
         {
            GX_msglist.addItem("Campo Unidade fora do intervalo", "OutOfRange", 1, "REFERENCIATECNICA_UNIDADE");
            AnyError = 1;
            GX_FocusControl = cmbReferenciaTecnica_Unidade_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000I4 */
         pr_default.execute(2, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Guia'.", "ForeignKeyNotFound", 1, "GUIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGuia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A94Guia_Nome = T000I4_A94Guia_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors0I19( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A93Guia_Codigo )
      {
         /* Using cursor T000I6 */
         pr_default.execute(4, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Guia'.", "ForeignKeyNotFound", 1, "GUIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGuia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A94Guia_Nome = T000I6_A94Guia_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A94Guia_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey0I19( )
      {
         /* Using cursor T000I7 */
         pr_default.execute(5, new Object[] {A97ReferenciaTecnica_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound19 = 1;
         }
         else
         {
            RcdFound19 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000I3 */
         pr_default.execute(1, new Object[] {A97ReferenciaTecnica_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0I19( 9) ;
            RcdFound19 = 1;
            A97ReferenciaTecnica_Codigo = T000I3_A97ReferenciaTecnica_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
            A98ReferenciaTecnica_Nome = T000I3_A98ReferenciaTecnica_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A98ReferenciaTecnica_Nome", A98ReferenciaTecnica_Nome);
            A99ReferenciaTecnica_Descricao = T000I3_A99ReferenciaTecnica_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A99ReferenciaTecnica_Descricao", A99ReferenciaTecnica_Descricao);
            A114ReferenciaTecnica_Unidade = T000I3_A114ReferenciaTecnica_Unidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A114ReferenciaTecnica_Unidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)));
            A100ReferenciaTecnica_Valor = T000I3_A100ReferenciaTecnica_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A100ReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( A100ReferenciaTecnica_Valor, 18, 5)));
            A430ReferenciaTecnica_Tipo = T000I3_A430ReferenciaTecnica_Tipo[0];
            A93Guia_Codigo = T000I3_A93Guia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
            Z97ReferenciaTecnica_Codigo = A97ReferenciaTecnica_Codigo;
            sMode19 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0I19( ) ;
            if ( AnyError == 1 )
            {
               RcdFound19 = 0;
               InitializeNonKey0I19( ) ;
            }
            Gx_mode = sMode19;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound19 = 0;
            InitializeNonKey0I19( ) ;
            sMode19 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode19;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0I19( ) ;
         if ( RcdFound19 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound19 = 0;
         /* Using cursor T000I8 */
         pr_default.execute(6, new Object[] {A97ReferenciaTecnica_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T000I8_A97ReferenciaTecnica_Codigo[0] < A97ReferenciaTecnica_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T000I8_A97ReferenciaTecnica_Codigo[0] > A97ReferenciaTecnica_Codigo ) ) )
            {
               A97ReferenciaTecnica_Codigo = T000I8_A97ReferenciaTecnica_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
               RcdFound19 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound19 = 0;
         /* Using cursor T000I9 */
         pr_default.execute(7, new Object[] {A97ReferenciaTecnica_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T000I9_A97ReferenciaTecnica_Codigo[0] > A97ReferenciaTecnica_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T000I9_A97ReferenciaTecnica_Codigo[0] < A97ReferenciaTecnica_Codigo ) ) )
            {
               A97ReferenciaTecnica_Codigo = T000I9_A97ReferenciaTecnica_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
               RcdFound19 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0I19( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtReferenciaTecnica_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0I19( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound19 == 1 )
            {
               if ( A97ReferenciaTecnica_Codigo != Z97ReferenciaTecnica_Codigo )
               {
                  A97ReferenciaTecnica_Codigo = Z97ReferenciaTecnica_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "REFERENCIATECNICA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtReferenciaTecnica_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtReferenciaTecnica_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0I19( ) ;
                  GX_FocusControl = edtReferenciaTecnica_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A97ReferenciaTecnica_Codigo != Z97ReferenciaTecnica_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtReferenciaTecnica_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0I19( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "REFERENCIATECNICA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtReferenciaTecnica_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtReferenciaTecnica_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0I19( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A97ReferenciaTecnica_Codigo != Z97ReferenciaTecnica_Codigo )
         {
            A97ReferenciaTecnica_Codigo = Z97ReferenciaTecnica_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "REFERENCIATECNICA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtReferenciaTecnica_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtReferenciaTecnica_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0I19( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000I2 */
            pr_default.execute(0, new Object[] {A97ReferenciaTecnica_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReferenciaTecnica"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z98ReferenciaTecnica_Nome, T000I2_A98ReferenciaTecnica_Nome[0]) != 0 ) || ( Z114ReferenciaTecnica_Unidade != T000I2_A114ReferenciaTecnica_Unidade[0] ) || ( Z100ReferenciaTecnica_Valor != T000I2_A100ReferenciaTecnica_Valor[0] ) || ( StringUtil.StrCmp(Z430ReferenciaTecnica_Tipo, T000I2_A430ReferenciaTecnica_Tipo[0]) != 0 ) || ( Z93Guia_Codigo != T000I2_A93Guia_Codigo[0] ) )
            {
               if ( StringUtil.StrCmp(Z98ReferenciaTecnica_Nome, T000I2_A98ReferenciaTecnica_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("referenciatecnica:[seudo value changed for attri]"+"ReferenciaTecnica_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z98ReferenciaTecnica_Nome);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A98ReferenciaTecnica_Nome[0]);
               }
               if ( Z114ReferenciaTecnica_Unidade != T000I2_A114ReferenciaTecnica_Unidade[0] )
               {
                  GXUtil.WriteLog("referenciatecnica:[seudo value changed for attri]"+"ReferenciaTecnica_Unidade");
                  GXUtil.WriteLogRaw("Old: ",Z114ReferenciaTecnica_Unidade);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A114ReferenciaTecnica_Unidade[0]);
               }
               if ( Z100ReferenciaTecnica_Valor != T000I2_A100ReferenciaTecnica_Valor[0] )
               {
                  GXUtil.WriteLog("referenciatecnica:[seudo value changed for attri]"+"ReferenciaTecnica_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z100ReferenciaTecnica_Valor);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A100ReferenciaTecnica_Valor[0]);
               }
               if ( StringUtil.StrCmp(Z430ReferenciaTecnica_Tipo, T000I2_A430ReferenciaTecnica_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("referenciatecnica:[seudo value changed for attri]"+"ReferenciaTecnica_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z430ReferenciaTecnica_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A430ReferenciaTecnica_Tipo[0]);
               }
               if ( Z93Guia_Codigo != T000I2_A93Guia_Codigo[0] )
               {
                  GXUtil.WriteLog("referenciatecnica:[seudo value changed for attri]"+"Guia_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z93Guia_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A93Guia_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ReferenciaTecnica"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0I19( )
      {
         BeforeValidate0I19( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0I19( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0I19( 0) ;
            CheckOptimisticConcurrency0I19( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0I19( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0I19( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000I10 */
                     pr_default.execute(8, new Object[] {A98ReferenciaTecnica_Nome, A99ReferenciaTecnica_Descricao, A114ReferenciaTecnica_Unidade, A100ReferenciaTecnica_Valor, A430ReferenciaTecnica_Tipo, A93Guia_Codigo});
                     A97ReferenciaTecnica_Codigo = T000I10_A97ReferenciaTecnica_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaTecnica") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0I0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0I19( ) ;
            }
            EndLevel0I19( ) ;
         }
         CloseExtendedTableCursors0I19( ) ;
      }

      protected void Update0I19( )
      {
         BeforeValidate0I19( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0I19( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0I19( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0I19( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0I19( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000I11 */
                     pr_default.execute(9, new Object[] {A98ReferenciaTecnica_Nome, A99ReferenciaTecnica_Descricao, A114ReferenciaTecnica_Unidade, A100ReferenciaTecnica_Valor, A430ReferenciaTecnica_Tipo, A93Guia_Codigo, A97ReferenciaTecnica_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ReferenciaTecnica") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReferenciaTecnica"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0I19( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0I19( ) ;
         }
         CloseExtendedTableCursors0I19( ) ;
      }

      protected void DeferredUpdate0I19( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0I19( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0I19( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0I19( ) ;
            AfterConfirm0I19( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0I19( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000I12 */
                  pr_default.execute(10, new Object[] {A97ReferenciaTecnica_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ReferenciaTecnica") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode19 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0I19( ) ;
         Gx_mode = sMode19;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0I19( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000I13 */
            pr_default.execute(11, new Object[] {A93Guia_Codigo});
            A94Guia_Nome = T000I13_A94Guia_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
            pr_default.close(11);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000I14 */
            pr_default.execute(12, new Object[] {A97ReferenciaTecnica_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item"+" ("+"ST_Contagem Item_FMReferencia Tecnica"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor T000I15 */
            pr_default.execute(13, new Object[] {A97ReferenciaTecnica_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item"+" ("+"Refer�ncia T�cnica"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel0I19( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0I19( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "ReferenciaTecnica");
            if ( AnyError == 0 )
            {
               ConfirmValues0I0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "ReferenciaTecnica");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0I19( )
      {
         /* Scan By routine */
         /* Using cursor T000I16 */
         pr_default.execute(14);
         RcdFound19 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound19 = 1;
            A97ReferenciaTecnica_Codigo = T000I16_A97ReferenciaTecnica_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0I19( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound19 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound19 = 1;
            A97ReferenciaTecnica_Codigo = T000I16_A97ReferenciaTecnica_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd0I19( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm0I19( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0I19( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0I19( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0I19( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0I19( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0I19( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0I19( )
      {
         edtReferenciaTecnica_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaTecnica_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaTecnica_Codigo_Enabled), 5, 0)));
         edtReferenciaTecnica_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaTecnica_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaTecnica_Nome_Enabled), 5, 0)));
         edtReferenciaTecnica_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaTecnica_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaTecnica_Descricao_Enabled), 5, 0)));
         cmbReferenciaTecnica_Unidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbReferenciaTecnica_Unidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbReferenciaTecnica_Unidade.Enabled), 5, 0)));
         edtReferenciaTecnica_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaTecnica_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaTecnica_Valor_Enabled), 5, 0)));
         edtGuia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Codigo_Enabled), 5, 0)));
         edtGuia_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Nome_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0I0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822582860");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("referenciatecnica.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ReferenciaTecnica_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z97ReferenciaTecnica_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z98ReferenciaTecnica_Nome", StringUtil.RTrim( Z98ReferenciaTecnica_Nome));
         GxWebStd.gx_hidden_field( context, "Z114ReferenciaTecnica_Unidade", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z114ReferenciaTecnica_Unidade), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z100ReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.NToC( Z100ReferenciaTecnica_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z430ReferenciaTecnica_Tipo", StringUtil.RTrim( Z430ReferenciaTecnica_Tipo));
         GxWebStd.gx_hidden_field( context, "Z93Guia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z93Guia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N93Guia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A93Guia_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vREFERENCIATECNICA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ReferenciaTecnica_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_GUIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Guia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REFERENCIATECNICA_TIPO", StringUtil.RTrim( A430ReferenciaTecnica_Tipo));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vREFERENCIATECNICA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ReferenciaTecnica_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ReferenciaTecnica";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A97ReferenciaTecnica_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A430ReferenciaTecnica_Tipo, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("referenciatecnica:[SendSecurityCheck value for]"+"ReferenciaTecnica_Codigo:"+context.localUtil.Format( (decimal)(A97ReferenciaTecnica_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("referenciatecnica:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("referenciatecnica:[SendSecurityCheck value for]"+"ReferenciaTecnica_Tipo:"+StringUtil.RTrim( context.localUtil.Format( A430ReferenciaTecnica_Tipo, "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("referenciatecnica.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ReferenciaTecnica_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ReferenciaTecnica" ;
      }

      public override String GetPgmdesc( )
      {
         return "Refer�ncia T�cnica" ;
      }

      protected void InitializeNonKey0I19( )
      {
         A93Guia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
         A98ReferenciaTecnica_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A98ReferenciaTecnica_Nome", A98ReferenciaTecnica_Nome);
         A99ReferenciaTecnica_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A99ReferenciaTecnica_Descricao", A99ReferenciaTecnica_Descricao);
         A114ReferenciaTecnica_Unidade = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A114ReferenciaTecnica_Unidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)));
         A100ReferenciaTecnica_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A100ReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( A100ReferenciaTecnica_Valor, 18, 5)));
         A430ReferenciaTecnica_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A430ReferenciaTecnica_Tipo", A430ReferenciaTecnica_Tipo);
         A94Guia_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
         Z98ReferenciaTecnica_Nome = "";
         Z114ReferenciaTecnica_Unidade = 0;
         Z100ReferenciaTecnica_Valor = 0;
         Z430ReferenciaTecnica_Tipo = "";
         Z93Guia_Codigo = 0;
      }

      protected void InitAll0I19( )
      {
         A97ReferenciaTecnica_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
         InitializeNonKey0I19( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822582878");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("referenciatecnica.js", "?202042822582878");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblReferenciatecnicatitle_Internalname = "REFERENCIATECNICATITLE";
         lblTextblockreferenciatecnica_codigo_Internalname = "TEXTBLOCKREFERENCIATECNICA_CODIGO";
         edtReferenciaTecnica_Codigo_Internalname = "REFERENCIATECNICA_CODIGO";
         lblTextblockreferenciatecnica_nome_Internalname = "TEXTBLOCKREFERENCIATECNICA_NOME";
         edtReferenciaTecnica_Nome_Internalname = "REFERENCIATECNICA_NOME";
         lblTextblockreferenciatecnica_descricao_Internalname = "TEXTBLOCKREFERENCIATECNICA_DESCRICAO";
         edtReferenciaTecnica_Descricao_Internalname = "REFERENCIATECNICA_DESCRICAO";
         lblTextblockreferenciatecnica_unidade_Internalname = "TEXTBLOCKREFERENCIATECNICA_UNIDADE";
         cmbReferenciaTecnica_Unidade_Internalname = "REFERENCIATECNICA_UNIDADE";
         lblTextblockreferenciatecnica_valor_Internalname = "TEXTBLOCKREFERENCIATECNICA_VALOR";
         edtReferenciaTecnica_Valor_Internalname = "REFERENCIATECNICA_VALOR";
         lblTextblockguia_codigo_Internalname = "TEXTBLOCKGUIA_CODIGO";
         edtGuia_Codigo_Internalname = "GUIA_CODIGO";
         edtGuia_Nome_Internalname = "GUIA_NOME";
         tblTablemergedguia_codigo_Internalname = "TABLEMERGEDGUIA_CODIGO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         imgprompt_93_Internalname = "PROMPT_93";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Principal";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Refer�ncia T�cnica";
         edtGuia_Nome_Jsonclick = "";
         edtGuia_Nome_Enabled = 0;
         imgprompt_93_Visible = 1;
         imgprompt_93_Link = "";
         edtGuia_Codigo_Jsonclick = "";
         edtGuia_Codigo_Enabled = 1;
         edtReferenciaTecnica_Valor_Jsonclick = "";
         edtReferenciaTecnica_Valor_Enabled = 1;
         cmbReferenciaTecnica_Unidade_Jsonclick = "";
         cmbReferenciaTecnica_Unidade.Enabled = 1;
         edtReferenciaTecnica_Descricao_Enabled = 1;
         edtReferenciaTecnica_Nome_Jsonclick = "";
         edtReferenciaTecnica_Nome_Enabled = 1;
         edtReferenciaTecnica_Codigo_Jsonclick = "";
         edtReferenciaTecnica_Codigo_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Guia_codigo( int GX_Parm1 ,
                                     String GX_Parm2 )
      {
         A93Guia_Codigo = GX_Parm1;
         A94Guia_Nome = GX_Parm2;
         /* Using cursor T000I13 */
         pr_default.execute(11, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Guia'.", "ForeignKeyNotFound", 1, "GUIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGuia_Codigo_Internalname;
         }
         A94Guia_Nome = T000I13_A94Guia_Nome[0];
         pr_default.close(11);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A94Guia_Nome = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A94Guia_Nome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ReferenciaTecnica_Codigo',fld:'vREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120I2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z98ReferenciaTecnica_Nome = "";
         Z430ReferenciaTecnica_Tipo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         lblReferenciatecnicatitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockreferenciatecnica_codigo_Jsonclick = "";
         lblTextblockreferenciatecnica_nome_Jsonclick = "";
         A98ReferenciaTecnica_Nome = "";
         lblTextblockreferenciatecnica_descricao_Jsonclick = "";
         A99ReferenciaTecnica_Descricao = "";
         lblTextblockreferenciatecnica_unidade_Jsonclick = "";
         lblTextblockreferenciatecnica_valor_Jsonclick = "";
         lblTextblockguia_codigo_Jsonclick = "";
         A94Guia_Nome = "";
         A430ReferenciaTecnica_Tipo = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode19 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z99ReferenciaTecnica_Descricao = "";
         Z94Guia_Nome = "";
         T000I4_A94Guia_Nome = new String[] {""} ;
         T000I5_A97ReferenciaTecnica_Codigo = new int[1] ;
         T000I5_A98ReferenciaTecnica_Nome = new String[] {""} ;
         T000I5_A99ReferenciaTecnica_Descricao = new String[] {""} ;
         T000I5_A114ReferenciaTecnica_Unidade = new short[1] ;
         T000I5_A100ReferenciaTecnica_Valor = new decimal[1] ;
         T000I5_A430ReferenciaTecnica_Tipo = new String[] {""} ;
         T000I5_A94Guia_Nome = new String[] {""} ;
         T000I5_A93Guia_Codigo = new int[1] ;
         T000I6_A94Guia_Nome = new String[] {""} ;
         T000I7_A97ReferenciaTecnica_Codigo = new int[1] ;
         T000I3_A97ReferenciaTecnica_Codigo = new int[1] ;
         T000I3_A98ReferenciaTecnica_Nome = new String[] {""} ;
         T000I3_A99ReferenciaTecnica_Descricao = new String[] {""} ;
         T000I3_A114ReferenciaTecnica_Unidade = new short[1] ;
         T000I3_A100ReferenciaTecnica_Valor = new decimal[1] ;
         T000I3_A430ReferenciaTecnica_Tipo = new String[] {""} ;
         T000I3_A93Guia_Codigo = new int[1] ;
         T000I8_A97ReferenciaTecnica_Codigo = new int[1] ;
         T000I9_A97ReferenciaTecnica_Codigo = new int[1] ;
         T000I2_A97ReferenciaTecnica_Codigo = new int[1] ;
         T000I2_A98ReferenciaTecnica_Nome = new String[] {""} ;
         T000I2_A99ReferenciaTecnica_Descricao = new String[] {""} ;
         T000I2_A114ReferenciaTecnica_Unidade = new short[1] ;
         T000I2_A100ReferenciaTecnica_Valor = new decimal[1] ;
         T000I2_A430ReferenciaTecnica_Tipo = new String[] {""} ;
         T000I2_A93Guia_Codigo = new int[1] ;
         T000I10_A97ReferenciaTecnica_Codigo = new int[1] ;
         T000I13_A94Guia_Nome = new String[] {""} ;
         T000I14_A224ContagemItem_Lancamento = new int[1] ;
         T000I15_A224ContagemItem_Lancamento = new int[1] ;
         T000I16_A97ReferenciaTecnica_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.referenciatecnica__default(),
            new Object[][] {
                new Object[] {
               T000I2_A97ReferenciaTecnica_Codigo, T000I2_A98ReferenciaTecnica_Nome, T000I2_A99ReferenciaTecnica_Descricao, T000I2_A114ReferenciaTecnica_Unidade, T000I2_A100ReferenciaTecnica_Valor, T000I2_A430ReferenciaTecnica_Tipo, T000I2_A93Guia_Codigo
               }
               , new Object[] {
               T000I3_A97ReferenciaTecnica_Codigo, T000I3_A98ReferenciaTecnica_Nome, T000I3_A99ReferenciaTecnica_Descricao, T000I3_A114ReferenciaTecnica_Unidade, T000I3_A100ReferenciaTecnica_Valor, T000I3_A430ReferenciaTecnica_Tipo, T000I3_A93Guia_Codigo
               }
               , new Object[] {
               T000I4_A94Guia_Nome
               }
               , new Object[] {
               T000I5_A97ReferenciaTecnica_Codigo, T000I5_A98ReferenciaTecnica_Nome, T000I5_A99ReferenciaTecnica_Descricao, T000I5_A114ReferenciaTecnica_Unidade, T000I5_A100ReferenciaTecnica_Valor, T000I5_A430ReferenciaTecnica_Tipo, T000I5_A94Guia_Nome, T000I5_A93Guia_Codigo
               }
               , new Object[] {
               T000I6_A94Guia_Nome
               }
               , new Object[] {
               T000I7_A97ReferenciaTecnica_Codigo
               }
               , new Object[] {
               T000I8_A97ReferenciaTecnica_Codigo
               }
               , new Object[] {
               T000I9_A97ReferenciaTecnica_Codigo
               }
               , new Object[] {
               T000I10_A97ReferenciaTecnica_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000I13_A94Guia_Nome
               }
               , new Object[] {
               T000I14_A224ContagemItem_Lancamento
               }
               , new Object[] {
               T000I15_A224ContagemItem_Lancamento
               }
               , new Object[] {
               T000I16_A97ReferenciaTecnica_Codigo
               }
            }
         );
         AV13Pgmname = "ReferenciaTecnica";
      }

      private short Z114ReferenciaTecnica_Unidade ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A114ReferenciaTecnica_Unidade ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound19 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ReferenciaTecnica_Codigo ;
      private int Z97ReferenciaTecnica_Codigo ;
      private int Z93Guia_Codigo ;
      private int N93Guia_Codigo ;
      private int A93Guia_Codigo ;
      private int AV7ReferenciaTecnica_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A97ReferenciaTecnica_Codigo ;
      private int edtReferenciaTecnica_Codigo_Enabled ;
      private int edtReferenciaTecnica_Nome_Enabled ;
      private int edtReferenciaTecnica_Descricao_Enabled ;
      private int edtReferenciaTecnica_Valor_Enabled ;
      private int edtGuia_Codigo_Enabled ;
      private int imgprompt_93_Visible ;
      private int edtGuia_Nome_Enabled ;
      private int AV11Insert_Guia_Codigo ;
      private int AV14GXV1 ;
      private int idxLst ;
      private decimal Z100ReferenciaTecnica_Valor ;
      private decimal A100ReferenciaTecnica_Valor ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z98ReferenciaTecnica_Nome ;
      private String Z430ReferenciaTecnica_Tipo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtReferenciaTecnica_Nome_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblReferenciatecnicatitle_Internalname ;
      private String lblReferenciatecnicatitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockreferenciatecnica_codigo_Internalname ;
      private String lblTextblockreferenciatecnica_codigo_Jsonclick ;
      private String edtReferenciaTecnica_Codigo_Internalname ;
      private String edtReferenciaTecnica_Codigo_Jsonclick ;
      private String lblTextblockreferenciatecnica_nome_Internalname ;
      private String lblTextblockreferenciatecnica_nome_Jsonclick ;
      private String A98ReferenciaTecnica_Nome ;
      private String edtReferenciaTecnica_Nome_Jsonclick ;
      private String lblTextblockreferenciatecnica_descricao_Internalname ;
      private String lblTextblockreferenciatecnica_descricao_Jsonclick ;
      private String edtReferenciaTecnica_Descricao_Internalname ;
      private String lblTextblockreferenciatecnica_unidade_Internalname ;
      private String lblTextblockreferenciatecnica_unidade_Jsonclick ;
      private String cmbReferenciaTecnica_Unidade_Internalname ;
      private String cmbReferenciaTecnica_Unidade_Jsonclick ;
      private String lblTextblockreferenciatecnica_valor_Internalname ;
      private String lblTextblockreferenciatecnica_valor_Jsonclick ;
      private String edtReferenciaTecnica_Valor_Internalname ;
      private String edtReferenciaTecnica_Valor_Jsonclick ;
      private String lblTextblockguia_codigo_Internalname ;
      private String lblTextblockguia_codigo_Jsonclick ;
      private String tblTablemergedguia_codigo_Internalname ;
      private String edtGuia_Codigo_Internalname ;
      private String edtGuia_Codigo_Jsonclick ;
      private String imgprompt_93_Internalname ;
      private String imgprompt_93_Link ;
      private String edtGuia_Nome_Internalname ;
      private String A94Guia_Nome ;
      private String edtGuia_Nome_Jsonclick ;
      private String A430ReferenciaTecnica_Tipo ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode19 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z94Guia_Nome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A99ReferenciaTecnica_Descricao ;
      private String Z99ReferenciaTecnica_Descricao ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbReferenciaTecnica_Unidade ;
      private IDataStoreProvider pr_default ;
      private String[] T000I4_A94Guia_Nome ;
      private int[] T000I5_A97ReferenciaTecnica_Codigo ;
      private String[] T000I5_A98ReferenciaTecnica_Nome ;
      private String[] T000I5_A99ReferenciaTecnica_Descricao ;
      private short[] T000I5_A114ReferenciaTecnica_Unidade ;
      private decimal[] T000I5_A100ReferenciaTecnica_Valor ;
      private String[] T000I5_A430ReferenciaTecnica_Tipo ;
      private String[] T000I5_A94Guia_Nome ;
      private int[] T000I5_A93Guia_Codigo ;
      private String[] T000I6_A94Guia_Nome ;
      private int[] T000I7_A97ReferenciaTecnica_Codigo ;
      private int[] T000I3_A97ReferenciaTecnica_Codigo ;
      private String[] T000I3_A98ReferenciaTecnica_Nome ;
      private String[] T000I3_A99ReferenciaTecnica_Descricao ;
      private short[] T000I3_A114ReferenciaTecnica_Unidade ;
      private decimal[] T000I3_A100ReferenciaTecnica_Valor ;
      private String[] T000I3_A430ReferenciaTecnica_Tipo ;
      private int[] T000I3_A93Guia_Codigo ;
      private int[] T000I8_A97ReferenciaTecnica_Codigo ;
      private int[] T000I9_A97ReferenciaTecnica_Codigo ;
      private int[] T000I2_A97ReferenciaTecnica_Codigo ;
      private String[] T000I2_A98ReferenciaTecnica_Nome ;
      private String[] T000I2_A99ReferenciaTecnica_Descricao ;
      private short[] T000I2_A114ReferenciaTecnica_Unidade ;
      private decimal[] T000I2_A100ReferenciaTecnica_Valor ;
      private String[] T000I2_A430ReferenciaTecnica_Tipo ;
      private int[] T000I2_A93Guia_Codigo ;
      private int[] T000I10_A97ReferenciaTecnica_Codigo ;
      private String[] T000I13_A94Guia_Nome ;
      private int[] T000I14_A224ContagemItem_Lancamento ;
      private int[] T000I15_A224ContagemItem_Lancamento ;
      private int[] T000I16_A97ReferenciaTecnica_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class referenciatecnica__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000I5 ;
          prmT000I5 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I4 ;
          prmT000I4 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I6 ;
          prmT000I6 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I7 ;
          prmT000I7 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I3 ;
          prmT000I3 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I8 ;
          prmT000I8 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I9 ;
          prmT000I9 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I2 ;
          prmT000I2 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I10 ;
          prmT000I10 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@ReferenciaTecnica_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ReferenciaTecnica_Unidade",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ReferenciaTecnica_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ReferenciaTecnica_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I11 ;
          prmT000I11 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@ReferenciaTecnica_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ReferenciaTecnica_Unidade",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ReferenciaTecnica_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ReferenciaTecnica_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ReferenciaTecnica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I12 ;
          prmT000I12 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I14 ;
          prmT000I14 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I15 ;
          prmT000I15 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000I16 ;
          prmT000I16 = new Object[] {
          } ;
          Object[] prmT000I13 ;
          prmT000I13 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000I2", "SELECT [ReferenciaTecnica_Codigo], [ReferenciaTecnica_Nome], [ReferenciaTecnica_Descricao], [ReferenciaTecnica_Unidade], [ReferenciaTecnica_Valor], [ReferenciaTecnica_Tipo], [Guia_Codigo] FROM [ReferenciaTecnica] WITH (UPDLOCK) WHERE [ReferenciaTecnica_Codigo] = @ReferenciaTecnica_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I2,1,0,true,false )
             ,new CursorDef("T000I3", "SELECT [ReferenciaTecnica_Codigo], [ReferenciaTecnica_Nome], [ReferenciaTecnica_Descricao], [ReferenciaTecnica_Unidade], [ReferenciaTecnica_Valor], [ReferenciaTecnica_Tipo], [Guia_Codigo] FROM [ReferenciaTecnica] WITH (NOLOCK) WHERE [ReferenciaTecnica_Codigo] = @ReferenciaTecnica_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I3,1,0,true,false )
             ,new CursorDef("T000I4", "SELECT [Guia_Nome] FROM [Guia] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I4,1,0,true,false )
             ,new CursorDef("T000I5", "SELECT TM1.[ReferenciaTecnica_Codigo], TM1.[ReferenciaTecnica_Nome], TM1.[ReferenciaTecnica_Descricao], TM1.[ReferenciaTecnica_Unidade], TM1.[ReferenciaTecnica_Valor], TM1.[ReferenciaTecnica_Tipo], T2.[Guia_Nome], TM1.[Guia_Codigo] FROM ([ReferenciaTecnica] TM1 WITH (NOLOCK) INNER JOIN [Guia] T2 WITH (NOLOCK) ON T2.[Guia_Codigo] = TM1.[Guia_Codigo]) WHERE TM1.[ReferenciaTecnica_Codigo] = @ReferenciaTecnica_Codigo ORDER BY TM1.[ReferenciaTecnica_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000I5,100,0,true,false )
             ,new CursorDef("T000I6", "SELECT [Guia_Nome] FROM [Guia] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I6,1,0,true,false )
             ,new CursorDef("T000I7", "SELECT [ReferenciaTecnica_Codigo] FROM [ReferenciaTecnica] WITH (NOLOCK) WHERE [ReferenciaTecnica_Codigo] = @ReferenciaTecnica_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000I7,1,0,true,false )
             ,new CursorDef("T000I8", "SELECT TOP 1 [ReferenciaTecnica_Codigo] FROM [ReferenciaTecnica] WITH (NOLOCK) WHERE ( [ReferenciaTecnica_Codigo] > @ReferenciaTecnica_Codigo) ORDER BY [ReferenciaTecnica_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000I8,1,0,true,true )
             ,new CursorDef("T000I9", "SELECT TOP 1 [ReferenciaTecnica_Codigo] FROM [ReferenciaTecnica] WITH (NOLOCK) WHERE ( [ReferenciaTecnica_Codigo] < @ReferenciaTecnica_Codigo) ORDER BY [ReferenciaTecnica_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000I9,1,0,true,true )
             ,new CursorDef("T000I10", "INSERT INTO [ReferenciaTecnica]([ReferenciaTecnica_Nome], [ReferenciaTecnica_Descricao], [ReferenciaTecnica_Unidade], [ReferenciaTecnica_Valor], [ReferenciaTecnica_Tipo], [Guia_Codigo]) VALUES(@ReferenciaTecnica_Nome, @ReferenciaTecnica_Descricao, @ReferenciaTecnica_Unidade, @ReferenciaTecnica_Valor, @ReferenciaTecnica_Tipo, @Guia_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000I10)
             ,new CursorDef("T000I11", "UPDATE [ReferenciaTecnica] SET [ReferenciaTecnica_Nome]=@ReferenciaTecnica_Nome, [ReferenciaTecnica_Descricao]=@ReferenciaTecnica_Descricao, [ReferenciaTecnica_Unidade]=@ReferenciaTecnica_Unidade, [ReferenciaTecnica_Valor]=@ReferenciaTecnica_Valor, [ReferenciaTecnica_Tipo]=@ReferenciaTecnica_Tipo, [Guia_Codigo]=@Guia_Codigo  WHERE [ReferenciaTecnica_Codigo] = @ReferenciaTecnica_Codigo", GxErrorMask.GX_NOMASK,prmT000I11)
             ,new CursorDef("T000I12", "DELETE FROM [ReferenciaTecnica]  WHERE [ReferenciaTecnica_Codigo] = @ReferenciaTecnica_Codigo", GxErrorMask.GX_NOMASK,prmT000I12)
             ,new CursorDef("T000I13", "SELECT [Guia_Nome] FROM [Guia] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I13,1,0,true,false )
             ,new CursorDef("T000I14", "SELECT TOP 1 [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_FMReferenciaTecnicaCod] = @ReferenciaTecnica_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I14,1,0,true,true )
             ,new CursorDef("T000I15", "SELECT TOP 1 [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_FSReferenciaTecnicaCod] = @ReferenciaTecnica_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I15,1,0,true,true )
             ,new CursorDef("T000I16", "SELECT [ReferenciaTecnica_Codigo] FROM [ReferenciaTecnica] WITH (NOLOCK) ORDER BY [ReferenciaTecnica_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000I16,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 1) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 1) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 1) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (int)parms[5]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (int)parms[5]);
                stmt.SetParameter(7, (int)parms[6]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
