/*
               File: UsuarioAreasDeTrabalhoWC
        Description: Usuario Areas De Trabalho WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:1:40.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuarioareasdetrabalhowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public usuarioareasdetrabalhowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public usuarioareasdetrabalhowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1Usuario_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_19 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_19_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_19_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  A1020ContratanteUsuario_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1020ContratanteUsuario_AreaTrabalhoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1020ContratanteUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV16Areas);
                  A1228ContratadaUsuario_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1228ContratadaUsuario_AreaTrabalhoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1228ContratadaUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0)));
                  A6AreaTrabalho_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
                  A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  A60ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( A1Usuario_Codigo, A1020ContratanteUsuario_AreaTrabalhoCod, AV16Areas, A1228ContratadaUsuario_AreaTrabalhoCod, A6AreaTrabalho_Descricao, A5AreaTrabalho_Codigo, A60ContratanteUsuario_UsuarioCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PARE2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV27Pgmname = "UsuarioAreasDeTrabalhoWC";
               context.Gx_err = 0;
               edtavContratada_codigo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_codigo_Enabled), 5, 0)));
               edtavContratante_codigo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_codigo_Enabled), 5, 0)));
               edtavAreatrabalho_descricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAreatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0)));
               WSRE2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Usuario Areas De Trabalho WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205211814023");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("usuarioareasdetrabalhowc.aspx") + "?" + UrlEncode("" +A1Usuario_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_19", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_19), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1Usuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTEUSUARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAREAS", AV16Areas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAREAS", AV16Areas);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"AREATRABALHO_DESCRICAO", A6AreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20AreaTrabalho_Codigo), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormRE2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("usuarioareasdetrabalhowc.js", "?20205211814025");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "UsuarioAreasDeTrabalhoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Usuario Areas De Trabalho WC" ;
      }

      protected void WBRE0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "usuarioareasdetrabalhowc.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_RE2( true) ;
         }
         else
         {
            wb_table1_2_RE2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_RE2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTRE2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Usuario Areas De Trabalho WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPRE0( ) ;
            }
         }
      }

      protected void WSRE2( )
      {
         STARTRE2( ) ;
         EVTRE2( ) ;
      }

      protected void EVTRE2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavContratada_codigo_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VPERFIS.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VPERFIS.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRE0( ) ;
                              }
                              nGXsfl_19_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
                              SubsflControlProps_192( ) ;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADA_CODIGO");
                                 GX_FocusControl = edtavContratada_codigo_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV14Contratada_codigo = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContratada_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Contratada_codigo), 6, 0)));
                              }
                              else
                              {
                                 AV14Contratada_codigo = (int)(context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContratada_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Contratada_codigo), 6, 0)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavContratante_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratante_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATANTE_CODIGO");
                                 GX_FocusControl = edtavContratante_codigo_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV15Contratante_Codigo = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContratante_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Contratante_Codigo), 6, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATANTE_CODIGO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( (decimal)(AV15Contratante_Codigo), "ZZZZZ9")));
                              }
                              else
                              {
                                 AV15Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContratante_codigo_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContratante_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Contratante_Codigo), 6, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATANTE_CODIGO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( (decimal)(AV15Contratante_Codigo), "ZZZZZ9")));
                              }
                              AV13AreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtavAreatrabalho_descricao_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAreatrabalho_descricao_Internalname, AV13AreaTrabalho_Descricao);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vAREATRABALHO_DESCRICAO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, StringUtil.RTrim( context.localUtil.Format( AV13AreaTrabalho_Descricao, "@!"))));
                              AV17Perfis = cgiGet( edtavPerfis_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPerfis_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17Perfis)) ? AV28Perfis_GXI : context.convertURL( context.PathToRelativeUrl( AV17Perfis))));
                              AV18Servicos = cgiGet( edtavServicos_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServicos_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV18Servicos)) ? AV29Servicos_GXI : context.convertURL( context.PathToRelativeUrl( AV18Servicos))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratada_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E11RE2 */
                                          E11RE2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratada_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12RE2 */
                                          E12RE2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratada_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13RE2 */
                                          E13RE2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VPERFIS.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratada_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14RE2 */
                                          E14RE2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratada_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPRE0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratada_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WERE2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormRE2( ) ;
            }
         }
      }

      protected void PARE2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavPerfil_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_192( ) ;
         while ( nGXsfl_19_idx <= nRC_GXsfl_19 )
         {
            sendrow_192( ) ;
            nGXsfl_19_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
            sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
            SubsflControlProps_192( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int A1Usuario_Codigo ,
                                       int A1020ContratanteUsuario_AreaTrabalhoCod ,
                                       IGxCollection AV16Areas ,
                                       int A1228ContratadaUsuario_AreaTrabalhoCod ,
                                       String A6AreaTrabalho_Descricao ,
                                       int A5AreaTrabalho_Codigo ,
                                       int A60ContratanteUsuario_UsuarioCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFRE2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(AV15Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Contratante_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vAREATRABALHO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV13AreaTrabalho_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vAREATRABALHO_DESCRICAO", AV13AreaTrabalho_Descricao);
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFRE2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV27Pgmname = "UsuarioAreasDeTrabalhoWC";
         context.Gx_err = 0;
         edtavContratada_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_codigo_Enabled), 5, 0)));
         edtavContratante_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_codigo_Enabled), 5, 0)));
         edtavAreatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAreatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0)));
      }

      protected void RFRE2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 19;
         /* Execute user event: E13RE2 */
         E13RE2 ();
         nGXsfl_19_idx = 1;
         sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
         SubsflControlProps_192( ) ;
         nGXsfl_19_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_192( ) ;
            /* Using cursor H00RE2 */
            pr_default.execute(0, new Object[] {A1Usuario_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               /* Execute user event: E12RE2 */
               E12RE2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            wbEnd = 19;
            WBRE0( ) ;
         }
         nGXsfl_19_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPRE0( )
      {
         /* Before Start, stand alone formulas. */
         AV27Pgmname = "UsuarioAreasDeTrabalhoWC";
         context.Gx_err = 0;
         edtavContratada_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_codigo_Enabled), 5, 0)));
         edtavContratante_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_codigo_Enabled), 5, 0)));
         edtavAreatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAreatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11RE2 */
         E11RE2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPerfil_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPerfil_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPERFIL_AREATRABALHOCOD");
               GX_FocusControl = edtavPerfil_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12Perfil_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Perfil_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV12Perfil_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavPerfil_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Perfil_AreaTrabalhoCod), 6, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_19 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_19"), ",", "."));
            wcpOA1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1Usuario_Codigo"), ",", "."));
            A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"USUARIO_CODIGO"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11RE2 */
         E11RE2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11RE2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      private void E12RE2( )
      {
         /* Load Routine */
         /* Using cursor H00RE4 */
         pr_default.execute(1, new Object[] {A1Usuario_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A63ContratanteUsuario_ContratanteCod = H00RE4_A63ContratanteUsuario_ContratanteCod[0];
            A60ContratanteUsuario_UsuarioCod = H00RE4_A60ContratanteUsuario_UsuarioCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = H00RE4_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = H00RE4_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = H00RE4_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = H00RE4_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            AV16Areas.Add(A1020ContratanteUsuario_AreaTrabalhoCod, 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         /* Using cursor H00RE5 */
         pr_default.execute(2, new Object[] {A1Usuario_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A66ContratadaUsuario_ContratadaCod = H00RE5_A66ContratadaUsuario_ContratadaCod[0];
            A69ContratadaUsuario_UsuarioCod = H00RE5_A69ContratadaUsuario_UsuarioCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = H00RE5_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = H00RE5_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = H00RE5_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = H00RE5_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            AV16Areas.Add(A1228ContratadaUsuario_AreaTrabalhoCod, 0);
            pr_default.readNext(2);
         }
         pr_default.close(2);
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A5AreaTrabalho_Codigo ,
                                              AV16Areas },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor H00RE6 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            A29Contratante_Codigo = H00RE6_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00RE6_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = H00RE6_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = H00RE6_A6AreaTrabalho_Descricao[0];
            AV20AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20AreaTrabalho_Codigo), 6, 0)));
            AV13AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAreatrabalho_descricao_Internalname, AV13AreaTrabalho_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vAREATRABALHO_DESCRICAO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, StringUtil.RTrim( context.localUtil.Format( AV13AreaTrabalho_Descricao, "@!"))));
            edtavServicos_Visible = 1;
            /* Using cursor H00RE8 */
            pr_default.execute(4, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo, A1Usuario_Codigo, A5AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A63ContratanteUsuario_ContratanteCod = H00RE8_A63ContratanteUsuario_ContratanteCod[0];
               A60ContratanteUsuario_UsuarioCod = H00RE8_A60ContratanteUsuario_UsuarioCod[0];
               A1020ContratanteUsuario_AreaTrabalhoCod = H00RE8_A1020ContratanteUsuario_AreaTrabalhoCod[0];
               n1020ContratanteUsuario_AreaTrabalhoCod = H00RE8_n1020ContratanteUsuario_AreaTrabalhoCod[0];
               A1020ContratanteUsuario_AreaTrabalhoCod = H00RE8_A1020ContratanteUsuario_AreaTrabalhoCod[0];
               n1020ContratanteUsuario_AreaTrabalhoCod = H00RE8_n1020ContratanteUsuario_AreaTrabalhoCod[0];
               edtavServicos_Visible = 0;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
            sendrow_192( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_19_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(19, GridRow);
            }
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV27Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Usuario";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Usuario_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E13RE2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV17Perfis = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPerfis_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17Perfis)) ? AV28Perfis_GXI : context.convertURL( context.PathToRelativeUrl( AV17Perfis))));
         AV28Perfis_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPerfis_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17Perfis)) ? AV28Perfis_GXI : context.convertURL( context.PathToRelativeUrl( AV17Perfis))));
         AV18Servicos = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServicos_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV18Servicos)) ? AV29Servicos_GXI : context.convertURL( context.PathToRelativeUrl( AV18Servicos))));
         AV29Servicos_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServicos_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV18Servicos)) ? AV29Servicos_GXI : context.convertURL( context.PathToRelativeUrl( AV18Servicos))));
      }

      protected void E14RE2( )
      {
         /* Perfis_Click Routine */
         AV19WebSession.Set("Area", StringUtil.Str( (decimal)(AV20AreaTrabalho_Codigo), 6, 0));
         context.PopUp(formatLink("wp_associationusuariousuarioperfil.aspx") + "?" + UrlEncode("" +A1Usuario_Codigo), new Object[] {});
      }

      protected void wb_table1_2_RE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_RE2( true) ;
         }
         else
         {
            wb_table2_5_RE2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_RE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_RE2e( true) ;
         }
         else
         {
            wb_table1_2_RE2e( false) ;
         }
      }

      protected void wb_table2_5_RE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_8_RE2( true) ;
         }
         else
         {
            wb_table3_8_RE2( false) ;
         }
         return  ;
      }

      protected void wb_table3_8_RE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_RE2e( true) ;
         }
         else
         {
            wb_table2_5_RE2e( false) ;
         }
      }

      protected void wb_table3_8_RE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextperfil_areatrabalhocod_Internalname, "C�d. �rea", "", "", lblFiltertextperfil_areatrabalhocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_UsuarioAreasDeTrabalhoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPerfil_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Perfil_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12Perfil_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,13);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPerfil_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioAreasDeTrabalhoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_16_RE2( true) ;
         }
         else
         {
            wb_table4_16_RE2( false) ;
         }
         return  ;
      }

      protected void wb_table4_16_RE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_8_RE2e( true) ;
         }
         else
         {
            wb_table3_8_RE2e( false) ;
         }
      }

      protected void wb_table4_16_RE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"19\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "�rea de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Perfis") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavServicos_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Servi�os") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Contratada_codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContratada_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Contratante_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContratante_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV13AreaTrabalho_Descricao);
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV17Perfis));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV18Servicos));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavServicos_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 19 )
         {
            wbEnd = 0;
            nRC_GXsfl_19 = (short)(nGXsfl_19_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_RE2e( true) ;
         }
         else
         {
            wb_table4_16_RE2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1Usuario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PARE2( ) ;
         WSRE2( ) ;
         WERE2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1Usuario_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PARE2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "usuarioareasdetrabalhowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PARE2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1Usuario_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         }
         wcpOA1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1Usuario_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1Usuario_Codigo != wcpOA1Usuario_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1Usuario_Codigo = A1Usuario_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1Usuario_Codigo = cgiGet( sPrefix+"A1Usuario_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1Usuario_Codigo) > 0 )
         {
            A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1Usuario_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         }
         else
         {
            A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1Usuario_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PARE2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSRE2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSRE2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1Usuario_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1Usuario_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1Usuario_Codigo_CTRL", StringUtil.RTrim( sCtrlA1Usuario_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WERE2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205211814067");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("usuarioareasdetrabalhowc.js", "?20205211814067");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_192( )
      {
         edtavContratada_codigo_Internalname = sPrefix+"vCONTRATADA_CODIGO_"+sGXsfl_19_idx;
         edtavContratante_codigo_Internalname = sPrefix+"vCONTRATANTE_CODIGO_"+sGXsfl_19_idx;
         edtavAreatrabalho_descricao_Internalname = sPrefix+"vAREATRABALHO_DESCRICAO_"+sGXsfl_19_idx;
         edtavPerfis_Internalname = sPrefix+"vPERFIS_"+sGXsfl_19_idx;
         edtavServicos_Internalname = sPrefix+"vSERVICOS_"+sGXsfl_19_idx;
      }

      protected void SubsflControlProps_fel_192( )
      {
         edtavContratada_codigo_Internalname = sPrefix+"vCONTRATADA_CODIGO_"+sGXsfl_19_fel_idx;
         edtavContratante_codigo_Internalname = sPrefix+"vCONTRATANTE_CODIGO_"+sGXsfl_19_fel_idx;
         edtavAreatrabalho_descricao_Internalname = sPrefix+"vAREATRABALHO_DESCRICAO_"+sGXsfl_19_fel_idx;
         edtavPerfis_Internalname = sPrefix+"vPERFIS_"+sGXsfl_19_fel_idx;
         edtavServicos_Internalname = sPrefix+"vSERVICOS_"+sGXsfl_19_fel_idx;
      }

      protected void sendrow_192( )
      {
         SubsflControlProps_192( ) ;
         WBRE0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_19_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_19_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContratada_codigo_Enabled!=0)&&(edtavContratada_codigo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 20,'"+sPrefix+"',false,'"+sGXsfl_19_idx+"',19)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContratada_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Contratada_codigo), 6, 0, ",", "")),((edtavContratada_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14Contratada_codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV14Contratada_codigo), "ZZZZZ9")),TempTags+((edtavContratada_codigo_Enabled!=0)&&(edtavContratada_codigo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContratada_codigo_Enabled!=0)&&(edtavContratada_codigo_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,20);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContratada_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavContratada_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContratante_codigo_Enabled!=0)&&(edtavContratante_codigo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 21,'"+sPrefix+"',false,'"+sGXsfl_19_idx+"',19)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContratante_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Contratante_Codigo), 6, 0, ",", "")),((edtavContratante_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15Contratante_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV15Contratante_Codigo), "ZZZZZ9")),TempTags+((edtavContratante_codigo_Enabled!=0)&&(edtavContratante_codigo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContratante_codigo_Enabled!=0)&&(edtavContratante_codigo_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContratante_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavContratante_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavAreatrabalho_descricao_Enabled!=0)&&(edtavAreatrabalho_descricao_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 22,'"+sPrefix+"',false,'"+sGXsfl_19_idx+"',19)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAreatrabalho_descricao_Internalname,(String)AV13AreaTrabalho_Descricao,StringUtil.RTrim( context.localUtil.Format( AV13AreaTrabalho_Descricao, "@!")),TempTags+((edtavAreatrabalho_descricao_Enabled!=0)&&(edtavAreatrabalho_descricao_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavAreatrabalho_descricao_Enabled!=0)&&(edtavAreatrabalho_descricao_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,22);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAreatrabalho_descricao_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavAreatrabalho_descricao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavPerfis_Enabled!=0)&&(edtavPerfis_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 23,'"+sPrefix+"',false,'',19)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV17Perfis_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV17Perfis))&&String.IsNullOrEmpty(StringUtil.RTrim( AV28Perfis_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV17Perfis)));
         GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavPerfis_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV17Perfis)) ? AV28Perfis_GXI : context.PathToRelativeUrl( AV17Perfis)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavPerfis_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVPERFIS.CLICK."+sGXsfl_19_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV17Perfis_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavServicos_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavServicos_Enabled!=0)&&(edtavServicos_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 24,'"+sPrefix+"',false,'',19)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV18Servicos_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV18Servicos))&&String.IsNullOrEmpty(StringUtil.RTrim( AV29Servicos_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV18Servicos)));
         GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavServicos_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV18Servicos)) ? AV29Servicos_GXI : context.PathToRelativeUrl( AV18Servicos)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavServicos_Visible,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavServicos_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e15re2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV18Servicos_IsBlob,(bool)false});
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTRATANTE_CODIGO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( (decimal)(AV15Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vAREATRABALHO_DESCRICAO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, StringUtil.RTrim( context.localUtil.Format( AV13AreaTrabalho_Descricao, "@!"))));
         GridContainer.AddRow(GridRow);
         nGXsfl_19_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
         sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
         SubsflControlProps_192( ) ;
         /* End function sendrow_192 */
      }

      protected void init_default_properties( )
      {
         lblFiltertextperfil_areatrabalhocod_Internalname = sPrefix+"FILTERTEXTPERFIL_AREATRABALHOCOD";
         edtavPerfil_areatrabalhocod_Internalname = sPrefix+"vPERFIL_AREATRABALHOCOD";
         edtavContratada_codigo_Internalname = sPrefix+"vCONTRATADA_CODIGO";
         edtavContratante_codigo_Internalname = sPrefix+"vCONTRATANTE_CODIGO";
         edtavAreatrabalho_descricao_Internalname = sPrefix+"vAREATRABALHO_DESCRICAO";
         edtavPerfis_Internalname = sPrefix+"vPERFIS";
         edtavServicos_Internalname = sPrefix+"vSERVICOS";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         tblUsertable_Internalname = sPrefix+"USERTABLE";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavServicos_Jsonclick = "";
         edtavServicos_Enabled = 1;
         edtavPerfis_Jsonclick = "";
         edtavPerfis_Visible = -1;
         edtavPerfis_Enabled = 1;
         edtavAreatrabalho_descricao_Jsonclick = "";
         edtavAreatrabalho_descricao_Visible = -1;
         edtavContratante_codigo_Jsonclick = "";
         edtavContratante_codigo_Visible = 0;
         edtavContratada_codigo_Jsonclick = "";
         edtavContratada_codigo_Visible = 0;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavAreatrabalho_descricao_Enabled = 1;
         edtavContratante_codigo_Enabled = 1;
         edtavContratada_codigo_Enabled = 1;
         edtavServicos_Visible = -1;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavPerfil_areatrabalhocod_Jsonclick = "";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1020ContratanteUsuario_AreaTrabalhoCod',fld:'CONTRATANTEUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Areas',fld:'vAREAS',pic:'',nv:null},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV17Perfis',fld:'vPERFIS',pic:'',nv:''},{av:'AV18Servicos',fld:'vSERVICOS',pic:'',nv:''}]}");
         setEventMetadata("VPERFIS.CLICK","{handler:'E14RE2',iparms:[{av:'AV20AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("VSERVICOS.CLICK","{handler:'E15RE2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Contratada_codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV14Contratada_codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16Areas = new GxSimpleCollection();
         A6AreaTrabalho_Descricao = "";
         GXKey = "";
         AV27Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV13AreaTrabalho_Descricao = "";
         AV17Perfis = "";
         AV28Perfis_GXI = "";
         AV18Servicos = "";
         AV29Servicos_GXI = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00RE2_A1Usuario_Codigo = new int[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00RE4_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00RE4_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00RE4_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         H00RE4_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00RE5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00RE5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00RE5_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00RE5_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00RE6_A29Contratante_Codigo = new int[1] ;
         H00RE6_n29Contratante_Codigo = new bool[] {false} ;
         H00RE6_A5AreaTrabalho_Codigo = new int[1] ;
         H00RE6_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00RE8_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00RE8_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00RE8_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         H00RE8_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         GridRow = new GXWebRow();
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         AV19WebSession = context.GetSession();
         sStyleString = "";
         lblFiltertextperfil_areatrabalhocod_Jsonclick = "";
         TempTags = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1Usuario_Codigo = "";
         ROClassString = "";
         ClassString = "";
         StyleString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuarioareasdetrabalhowc__default(),
            new Object[][] {
                new Object[] {
               H00RE2_A1Usuario_Codigo
               }
               , new Object[] {
               H00RE4_A63ContratanteUsuario_ContratanteCod, H00RE4_A60ContratanteUsuario_UsuarioCod, H00RE4_A1020ContratanteUsuario_AreaTrabalhoCod, H00RE4_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               H00RE5_A66ContratadaUsuario_ContratadaCod, H00RE5_A69ContratadaUsuario_UsuarioCod, H00RE5_A1228ContratadaUsuario_AreaTrabalhoCod, H00RE5_n1228ContratadaUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               H00RE6_A29Contratante_Codigo, H00RE6_n29Contratante_Codigo, H00RE6_A5AreaTrabalho_Codigo, H00RE6_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00RE8_A63ContratanteUsuario_ContratanteCod, H00RE8_A60ContratanteUsuario_UsuarioCod, H00RE8_A1020ContratanteUsuario_AreaTrabalhoCod, H00RE8_n1020ContratanteUsuario_AreaTrabalhoCod
               }
            }
         );
         AV27Pgmname = "UsuarioAreasDeTrabalhoWC";
         /* GeneXus formulas. */
         AV27Pgmname = "UsuarioAreasDeTrabalhoWC";
         context.Gx_err = 0;
         edtavContratada_codigo_Enabled = 0;
         edtavContratante_codigo_Enabled = 0;
         edtavAreatrabalho_descricao_Enabled = 0;
      }

      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_19 ;
      private short nGXsfl_19_idx=1 ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_19_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short GRID_nEOF ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int A1Usuario_Codigo ;
      private int wcpOA1Usuario_Codigo ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A5AreaTrabalho_Codigo ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int edtavContratada_codigo_Enabled ;
      private int edtavContratante_codigo_Enabled ;
      private int edtavAreatrabalho_descricao_Enabled ;
      private int AV20AreaTrabalho_Codigo ;
      private int AV14Contratada_codigo ;
      private int AV15Contratante_Codigo ;
      private int subGrid_Islastpage ;
      private int AV12Perfil_AreaTrabalhoCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A29Contratante_Codigo ;
      private int edtavServicos_Visible ;
      private int AV7Usuario_Codigo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavContratada_codigo_Visible ;
      private int edtavContratante_codigo_Visible ;
      private int edtavAreatrabalho_descricao_Visible ;
      private int edtavPerfis_Enabled ;
      private int edtavPerfis_Visible ;
      private int edtavServicos_Enabled ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_19_idx="0001" ;
      private String GXKey ;
      private String AV27Pgmname ;
      private String edtavContratada_codigo_Internalname ;
      private String edtavContratante_codigo_Internalname ;
      private String edtavAreatrabalho_descricao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavPerfis_Internalname ;
      private String edtavServicos_Internalname ;
      private String edtavPerfil_areatrabalhocod_Internalname ;
      private String scmdbuf ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUsertable_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String lblFiltertextperfil_areatrabalhocod_Internalname ;
      private String lblFiltertextperfil_areatrabalhocod_Jsonclick ;
      private String TempTags ;
      private String edtavPerfil_areatrabalhocod_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlA1Usuario_Codigo ;
      private String sGXsfl_19_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavContratada_codigo_Jsonclick ;
      private String edtavContratante_codigo_Jsonclick ;
      private String edtavAreatrabalho_descricao_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavPerfis_Jsonclick ;
      private String edtavServicos_Jsonclick ;
      private bool entryPointCalled ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n29Contratante_Codigo ;
      private bool gx_refresh_fired ;
      private bool AV17Perfis_IsBlob ;
      private bool AV18Servicos_IsBlob ;
      private String A6AreaTrabalho_Descricao ;
      private String AV13AreaTrabalho_Descricao ;
      private String AV28Perfis_GXI ;
      private String AV29Servicos_GXI ;
      private String AV17Perfis ;
      private String AV18Servicos ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00RE2_A1Usuario_Codigo ;
      private int[] H00RE4_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00RE4_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00RE4_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] H00RE4_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private int[] H00RE5_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00RE5_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00RE5_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00RE5_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00RE6_A29Contratante_Codigo ;
      private bool[] H00RE6_n29Contratante_Codigo ;
      private int[] H00RE6_A5AreaTrabalho_Codigo ;
      private String[] H00RE6_A6AreaTrabalho_Descricao ;
      private int[] H00RE8_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00RE8_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00RE8_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] H00RE8_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private IGxSession AV19WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV16Areas ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class usuarioareasdetrabalhowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00RE6( IGxContext context ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV16Areas )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT [Contratante_Codigo], [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV16Areas, "[AreaTrabalho_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [AreaTrabalho_Descricao]";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_H00RE6(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00RE2 ;
          prmH00RE2 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RE4 ;
          prmH00RE4 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RE5 ;
          prmH00RE5 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RE8 ;
          prmH00RE8 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RE6 ;
          prmH00RE6 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00RE2", "SELECT [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RE2,1,0,true,true )
             ,new CursorDef("H00RE4", "SELECT T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod], COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T3.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] FROM [AreaTrabalho] T3 WITH (NOLOCK),  [ContratanteUsuario] T4 WITH (NOLOCK) WHERE T3.[Contratante_Codigo] = T4.[ContratanteUsuario_ContratanteCod] GROUP BY T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] ) T2 ON T2.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T2.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE T1.[ContratanteUsuario_UsuarioCod] = @Usuario_Codigo ORDER BY T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RE4,100,0,false,false )
             ,new CursorDef("H00RE5", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE T1.[ContratadaUsuario_UsuarioCod] = @Usuario_Codigo ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RE5,100,0,false,false )
             ,new CursorDef("H00RE6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RE6,100,0,true,false )
             ,new CursorDef("H00RE8", "SELECT TOP 1 T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod], COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T3.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] FROM [AreaTrabalho] T3 WITH (NOLOCK),  [ContratanteUsuario] T4 WITH (NOLOCK) WHERE T3.[Contratante_Codigo] = T4.[ContratanteUsuario_ContratanteCod] GROUP BY T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] ) T2 ON T2.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T2.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_ContratanteCod] = @Contratante_Codigo and T1.[ContratanteUsuario_UsuarioCod] = @Usuario_Codigo) AND (COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) = @AreaTrabalho_Codigo) ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RE8,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
       }
    }

 }

}
