/*
               File: Tmp_File
        Description: Arquivo a processar:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:2:27.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tmp_file : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               nDynComponent = 1;
               sCompPrefix = GetNextPar( );
               sSFPrefix = GetNextPar( );
               setjustcreated();
               componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix});
               componentstart();
               context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
               componentdraw();
               context.httpAjaxContext.ajax_rspEndCmp();
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
            {
               A435File_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxLoad_2( A435File_UsuarioCod) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Arquivo a processar:", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
            if ( nDynComponent == 0 )
            {
               context.HttpContext.Response.StatusDescription = 404.ToString();
               context.HttpContext.Response.StatusCode = 404;
               GXUtil.WriteLog("send_http_error_code " + 404.ToString());
               GxWebError = 1;
            }
         }
         GX_FocusControl = edtFile_UsuarioCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public tmp_file( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public tmp_file( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            UserMain( ) ;
            if ( ! isFullAjaxMode( ) )
            {
               Draw( ) ;
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "tmp_file.aspx");
            }
            wb_table1_2_1R73( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1R73e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
            RenderHtmlCloseForm1R73( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1R73( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1R73( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1R73e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Arquivo a processar:", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_Tmp_File.htm");
            wb_table3_28_1R73( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_1R73e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1R73e( true) ;
         }
         else
         {
            wb_table1_2_1R73e( false) ;
         }
      }

      protected void wb_table3_28_1R73( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_1R73( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_1R73e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tmp_File.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tmp_File.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tmp_File.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_1R73e( true) ;
         }
         else
         {
            wb_table3_28_1R73e( false) ;
         }
      }

      protected void wb_table4_34_1R73( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfile_usuariocod_Internalname, "Usuario", "", "", lblTextblockfile_usuariocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tmp_File.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFile_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A435File_UsuarioCod), 6, 0, ",", "")), ((edtFile_UsuarioCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A435File_UsuarioCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A435File_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFile_UsuarioCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtFile_UsuarioCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Tmp_File.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfile_row_Internalname, "N�", "", "", lblTextblockfile_row_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tmp_File.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFile_Row_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A504File_Row), 8, 0, ",", "")), ((edtFile_Row_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A504File_Row), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A504File_Row), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFile_Row_Jsonclick, 0, "Attribute", "", "", "", 1, edtFile_Row_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Tmp_File.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfile_col1_Internalname, "1", "", "", lblTextblockfile_col1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tmp_File.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A505File_Col1", A505File_Col1);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFile_Col1_Internalname, A505File_Col1, StringUtil.RTrim( context.localUtil.Format( A505File_Col1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFile_Col1_Jsonclick, 0, "Attribute", "", "", "", 1, edtFile_Col1_Enabled, 0, "text", "", 200, "px", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Tmp_File.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfile_col2_Internalname, "2", "", "", lblTextblockfile_col2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tmp_File.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A506File_Col2", A506File_Col2);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFile_Col2_Internalname, A506File_Col2, StringUtil.RTrim( context.localUtil.Format( A506File_Col2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFile_Col2_Jsonclick, 0, "Attribute", "", "", "", 1, edtFile_Col2_Enabled, 0, "text", "", 200, "px", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Tmp_File.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfile_col3_Internalname, "3", "", "", lblTextblockfile_col3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Tmp_File.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A507File_Col3", A507File_Col3);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFile_Col3_Internalname, A507File_Col3, StringUtil.RTrim( context.localUtil.Format( A507File_Col3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFile_Col3_Jsonclick, 0, "Attribute", "", "", "", 1, edtFile_Col3_Enabled, 0, "text", "", 200, "px", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Tmp_File.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_1R73e( true) ;
         }
         else
         {
            wb_table4_34_1R73e( false) ;
         }
      }

      protected void wb_table2_5_1R73( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Tmp_File.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1R73e( true) ;
         }
         else
         {
            wb_table2_5_1R73e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               standaloneStartupServer( ) ;
            }
         }
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         nDoneStart = 1;
         if ( AnyError == 0 )
         {
            sXEvt = cgiGet( "_EventName");
            if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtFile_UsuarioCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFile_UsuarioCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FILE_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtFile_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A435File_UsuarioCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
               }
               else
               {
                  A435File_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtFile_UsuarioCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFile_Row_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFile_Row_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FILE_ROW");
                  AnyError = 1;
                  GX_FocusControl = edtFile_Row_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A504File_Row = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
               }
               else
               {
                  A504File_Row = (int)(context.localUtil.CToN( cgiGet( edtFile_Row_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
               }
               A505File_Col1 = cgiGet( edtFile_Col1_Internalname);
               n505File_Col1 = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A505File_Col1", A505File_Col1);
               n505File_Col1 = (String.IsNullOrEmpty(StringUtil.RTrim( A505File_Col1)) ? true : false);
               A506File_Col2 = cgiGet( edtFile_Col2_Internalname);
               n506File_Col2 = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A506File_Col2", A506File_Col2);
               n506File_Col2 = (String.IsNullOrEmpty(StringUtil.RTrim( A506File_Col2)) ? true : false);
               A507File_Col3 = cgiGet( edtFile_Col3_Internalname);
               n507File_Col3 = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A507File_Col3", A507File_Col3);
               n507File_Col3 = (String.IsNullOrEmpty(StringUtil.RTrim( A507File_Col3)) ? true : false);
               /* Read saved values. */
               Z435File_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"Z435File_UsuarioCod"), ",", "."));
               Z504File_Row = (int)(context.localUtil.CToN( cgiGet( sPrefix+"Z504File_Row"), ",", "."));
               Z505File_Col1 = cgiGet( sPrefix+"Z505File_Col1");
               n505File_Col1 = (String.IsNullOrEmpty(StringUtil.RTrim( A505File_Col1)) ? true : false);
               Z506File_Col2 = cgiGet( sPrefix+"Z506File_Col2");
               n506File_Col2 = (String.IsNullOrEmpty(StringUtil.RTrim( A506File_Col2)) ? true : false);
               Z507File_Col3 = cgiGet( sPrefix+"Z507File_Col3");
               n507File_Col3 = (String.IsNullOrEmpty(StringUtil.RTrim( A507File_Col3)) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( sPrefix+"IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( sPrefix+"IsModified"), ",", "."));
               Gx_mode = cgiGet( sPrefix+"Mode");
               Gx_mode = cgiGet( sPrefix+"vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                  A435File_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
                  A504File_Row = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read Transaction buttons. */
            if ( context.wbHandled == 0 )
            {
               if ( StringUtil.Len( sPrefix) == 0 )
               {
                  sEvt = cgiGet( "_EventName");
                  EvtGridId = cgiGet( "_EventGridId");
                  EvtRowId = cgiGet( "_EventRowId");
               }
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 btn_enter( ) ;
                              }
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 btn_first( ) ;
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 btn_previous( ) ;
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 btn_next( ) ;
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 btn_last( ) ;
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 btn_select( ) ;
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 btn_delete( ) ;
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 AfterKeyLoadScreen( ) ;
                              }
                           }
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1R73( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFile_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFile_UsuarioCod_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFile_Row_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFile_Row_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFile_Col1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFile_Col1_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFile_Col2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFile_Col2_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFile_Col3_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFile_Col3_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes1R73( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption1R0( )
      {
      }

      protected void ZM1R73( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z505File_Col1 = T001R3_A505File_Col1[0];
               Z506File_Col2 = T001R3_A506File_Col2[0];
               Z507File_Col3 = T001R3_A507File_Col3[0];
            }
            else
            {
               Z505File_Col1 = A505File_Col1;
               Z506File_Col2 = A506File_Col2;
               Z507File_Col3 = A507File_Col3;
            }
         }
         if ( GX_JID == -1 )
         {
            Z504File_Row = A504File_Row;
            Z505File_Col1 = A505File_Col1;
            Z506File_Col2 = A506File_Col2;
            Z507File_Col3 = A507File_Col3;
            Z435File_UsuarioCod = A435File_UsuarioCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load1R73( )
      {
         /* Using cursor T001R5 */
         pr_default.execute(3, new Object[] {A435File_UsuarioCod, A504File_Row});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound73 = 1;
            A505File_Col1 = T001R5_A505File_Col1[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A505File_Col1", A505File_Col1);
            n505File_Col1 = T001R5_n505File_Col1[0];
            A506File_Col2 = T001R5_A506File_Col2[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A506File_Col2", A506File_Col2);
            n506File_Col2 = T001R5_n506File_Col2[0];
            A507File_Col3 = T001R5_A507File_Col3[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A507File_Col3", A507File_Col3);
            n507File_Col3 = T001R5_n507File_Col3[0];
            ZM1R73( -1) ;
         }
         pr_default.close(3);
         OnLoadActions1R73( ) ;
      }

      protected void OnLoadActions1R73( )
      {
      }

      protected void CheckExtendedTable1R73( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T001R4 */
         pr_default.execute(2, new Object[] {A435File_UsuarioCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'File_Usuario'.", "ForeignKeyNotFound", 1, "FILE_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtFile_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors1R73( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A435File_UsuarioCod )
      {
         /* Using cursor T001R6 */
         pr_default.execute(4, new Object[] {A435File_UsuarioCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'File_Usuario'.", "ForeignKeyNotFound", 1, "FILE_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtFile_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey1R73( )
      {
         /* Using cursor T001R7 */
         pr_default.execute(5, new Object[] {A435File_UsuarioCod, A504File_Row});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound73 = 1;
         }
         else
         {
            RcdFound73 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001R3 */
         pr_default.execute(1, new Object[] {A435File_UsuarioCod, A504File_Row});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1R73( 1) ;
            RcdFound73 = 1;
            A504File_Row = T001R3_A504File_Row[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
            A505File_Col1 = T001R3_A505File_Col1[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A505File_Col1", A505File_Col1);
            n505File_Col1 = T001R3_n505File_Col1[0];
            A506File_Col2 = T001R3_A506File_Col2[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A506File_Col2", A506File_Col2);
            n506File_Col2 = T001R3_n506File_Col2[0];
            A507File_Col3 = T001R3_A507File_Col3[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A507File_Col3", A507File_Col3);
            n507File_Col3 = T001R3_n507File_Col3[0];
            A435File_UsuarioCod = T001R3_A435File_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
            Z435File_UsuarioCod = A435File_UsuarioCod;
            Z504File_Row = A504File_Row;
            sMode73 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load1R73( ) ;
            if ( AnyError == 1 )
            {
               RcdFound73 = 0;
               InitializeNonKey1R73( ) ;
            }
            Gx_mode = sMode73;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound73 = 0;
            InitializeNonKey1R73( ) ;
            sMode73 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode73;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1R73( ) ;
         if ( RcdFound73 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound73 = 0;
         /* Using cursor T001R8 */
         pr_default.execute(6, new Object[] {A435File_UsuarioCod, A504File_Row});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T001R8_A435File_UsuarioCod[0] < A435File_UsuarioCod ) || ( T001R8_A435File_UsuarioCod[0] == A435File_UsuarioCod ) && ( T001R8_A504File_Row[0] < A504File_Row ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T001R8_A435File_UsuarioCod[0] > A435File_UsuarioCod ) || ( T001R8_A435File_UsuarioCod[0] == A435File_UsuarioCod ) && ( T001R8_A504File_Row[0] > A504File_Row ) ) )
            {
               A435File_UsuarioCod = T001R8_A435File_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
               A504File_Row = T001R8_A504File_Row[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
               RcdFound73 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound73 = 0;
         /* Using cursor T001R9 */
         pr_default.execute(7, new Object[] {A435File_UsuarioCod, A504File_Row});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T001R9_A435File_UsuarioCod[0] > A435File_UsuarioCod ) || ( T001R9_A435File_UsuarioCod[0] == A435File_UsuarioCod ) && ( T001R9_A504File_Row[0] > A504File_Row ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T001R9_A435File_UsuarioCod[0] < A435File_UsuarioCod ) || ( T001R9_A435File_UsuarioCod[0] == A435File_UsuarioCod ) && ( T001R9_A504File_Row[0] < A504File_Row ) ) )
            {
               A435File_UsuarioCod = T001R9_A435File_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
               A504File_Row = T001R9_A504File_Row[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
               RcdFound73 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1R73( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtFile_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            Insert1R73( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound73 == 1 )
            {
               if ( ( A435File_UsuarioCod != Z435File_UsuarioCod ) || ( A504File_Row != Z504File_Row ) )
               {
                  A435File_UsuarioCod = Z435File_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
                  A504File_Row = Z504File_Row;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FILE_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtFile_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtFile_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update1R73( ) ;
                  GX_FocusControl = edtFile_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A435File_UsuarioCod != Z435File_UsuarioCod ) || ( A504File_Row != Z504File_Row ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtFile_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                  Insert1R73( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FILE_USUARIOCOD");
                     AnyError = 1;
                     GX_FocusControl = edtFile_UsuarioCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtFile_UsuarioCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                     Insert1R73( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A435File_UsuarioCod != Z435File_UsuarioCod ) || ( A504File_Row != Z504File_Row ) )
         {
            A435File_UsuarioCod = Z435File_UsuarioCod;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
            A504File_Row = Z504File_Row;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FILE_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtFile_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtFile_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound73 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "FILE_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtFile_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtFile_Col1_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1R73( ) ;
         if ( RcdFound73 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtFile_Col1_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         ScanEnd1R73( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound73 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtFile_Col1_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound73 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtFile_Col1_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1R73( ) ;
         if ( RcdFound73 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound73 != 0 )
            {
               ScanNext1R73( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtFile_Col1_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         ScanEnd1R73( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency1R73( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001R2 */
            pr_default.execute(0, new Object[] {A435File_UsuarioCod, A504File_Row});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tmp_File"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z505File_Col1, T001R2_A505File_Col1[0]) != 0 ) || ( StringUtil.StrCmp(Z506File_Col2, T001R2_A506File_Col2[0]) != 0 ) || ( StringUtil.StrCmp(Z507File_Col3, T001R2_A507File_Col3[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z505File_Col1, T001R2_A505File_Col1[0]) != 0 )
               {
                  GXUtil.WriteLog("tmp_file:[seudo value changed for attri]"+"File_Col1");
                  GXUtil.WriteLogRaw("Old: ",Z505File_Col1);
                  GXUtil.WriteLogRaw("Current: ",T001R2_A505File_Col1[0]);
               }
               if ( StringUtil.StrCmp(Z506File_Col2, T001R2_A506File_Col2[0]) != 0 )
               {
                  GXUtil.WriteLog("tmp_file:[seudo value changed for attri]"+"File_Col2");
                  GXUtil.WriteLogRaw("Old: ",Z506File_Col2);
                  GXUtil.WriteLogRaw("Current: ",T001R2_A506File_Col2[0]);
               }
               if ( StringUtil.StrCmp(Z507File_Col3, T001R2_A507File_Col3[0]) != 0 )
               {
                  GXUtil.WriteLog("tmp_file:[seudo value changed for attri]"+"File_Col3");
                  GXUtil.WriteLogRaw("Old: ",Z507File_Col3);
                  GXUtil.WriteLogRaw("Current: ",T001R2_A507File_Col3[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Tmp_File"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1R73( )
      {
         BeforeValidate1R73( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1R73( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1R73( 0) ;
            CheckOptimisticConcurrency1R73( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1R73( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1R73( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001R10 */
                     pr_default.execute(8, new Object[] {A504File_Row, n505File_Col1, A505File_Col1, n506File_Col2, A506File_Col2, n507File_Col3, A507File_Col3, A435File_UsuarioCod});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Tmp_File") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1R0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1R73( ) ;
            }
            EndLevel1R73( ) ;
         }
         CloseExtendedTableCursors1R73( ) ;
      }

      protected void Update1R73( )
      {
         BeforeValidate1R73( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1R73( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1R73( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1R73( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1R73( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001R11 */
                     pr_default.execute(9, new Object[] {n505File_Col1, A505File_Col1, n506File_Col2, A506File_Col2, n507File_Col3, A507File_Col3, A435File_UsuarioCod, A504File_Row});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Tmp_File") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tmp_File"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1R73( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption1R0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1R73( ) ;
         }
         CloseExtendedTableCursors1R73( ) ;
      }

      protected void DeferredUpdate1R73( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         BeforeValidate1R73( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1R73( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1R73( ) ;
            AfterConfirm1R73( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1R73( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001R12 */
                  pr_default.execute(10, new Object[] {A435File_UsuarioCod, A504File_Row});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Tmp_File") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound73 == 0 )
                        {
                           InitAll1R73( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption1R0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode73 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         EndLevel1R73( ) ;
         Gx_mode = sMode73;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls1R73( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel1R73( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1R73( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Tmp_File");
            if ( AnyError == 0 )
            {
               ConfirmValues1R0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Tmp_File");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1R73( )
      {
         /* Using cursor T001R13 */
         pr_default.execute(11);
         RcdFound73 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound73 = 1;
            A435File_UsuarioCod = T001R13_A435File_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
            A504File_Row = T001R13_A504File_Row[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1R73( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound73 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound73 = 1;
            A435File_UsuarioCod = T001R13_A435File_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
            A504File_Row = T001R13_A504File_Row[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
         }
      }

      protected void ScanEnd1R73( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm1R73( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1R73( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1R73( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1R73( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1R73( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1R73( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1R73( )
      {
         edtFile_UsuarioCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFile_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFile_UsuarioCod_Enabled), 5, 0)));
         edtFile_Row_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFile_Row_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFile_Row_Enabled), 5, 0)));
         edtFile_Col1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFile_Col1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFile_Col1_Enabled), 5, 0)));
         edtFile_Col2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFile_Col2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFile_Col2_Enabled), 5, 0)));
         edtFile_Col3_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFile_Col3_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFile_Col3_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1R0( )
      {
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Arquivo a processar:") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020521182296");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tmp_file.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"Z435File_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z435File_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"Z504File_Row", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z504File_Row), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"Z505File_Col1", Z505File_Col1);
         GxWebStd.gx_hidden_field( context, sPrefix+"Z506File_Col2", Z506File_Col2);
         GxWebStd.gx_hidden_field( context, sPrefix+"Z507File_Col3", Z507File_Col3);
         GxWebStd.gx_hidden_field( context, sPrefix+"IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, sPrefix+"vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm1R73( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("tmp_file.js", "?2020521182299");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "Tmp_File" ;
      }

      public override String GetPgmdesc( )
      {
         return "Arquivo a processar:" ;
      }

      protected void InitializeNonKey1R73( )
      {
         A505File_Col1 = "";
         n505File_Col1 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A505File_Col1", A505File_Col1);
         n505File_Col1 = (String.IsNullOrEmpty(StringUtil.RTrim( A505File_Col1)) ? true : false);
         A506File_Col2 = "";
         n506File_Col2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A506File_Col2", A506File_Col2);
         n506File_Col2 = (String.IsNullOrEmpty(StringUtil.RTrim( A506File_Col2)) ? true : false);
         A507File_Col3 = "";
         n507File_Col3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A507File_Col3", A507File_Col3);
         n507File_Col3 = (String.IsNullOrEmpty(StringUtil.RTrim( A507File_Col3)) ? true : false);
         Z505File_Col1 = "";
         Z506File_Col2 = "";
         Z507File_Col3 = "";
      }

      protected void InitAll1R73( )
      {
         A435File_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A435File_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A435File_UsuarioCod), 6, 0)));
         A504File_Row = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A504File_Row", StringUtil.LTrim( StringUtil.Str( (decimal)(A504File_Row), 8, 0)));
         InitializeNonKey1R73( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         if ( StringUtil.Len( sPrefix) != 0 )
         {
            initialize_properties( ) ;
         }
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         if ( nDoneStart == 0 )
         {
            createObjects();
            initialize();
         }
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "tmp_file");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITENV( ) ;
            INITTRN( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
         }
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITENV( ) ;
         INITTRN( ) ;
         nDraw = 0;
         sEvt = sCompEvt;
         if ( isFullAjaxMode( ) )
         {
            UserMain( ) ;
         }
         else
         {
            WCParametersGet( ) ;
         }
         Process( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         UserMain( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         Draw( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205211822913");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("tmp_file.js", "?20205211822913");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = sPrefix+"BTN_FIRST";
         imgBtn_first_separator_Internalname = sPrefix+"BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = sPrefix+"BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = sPrefix+"BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = sPrefix+"BTN_NEXT";
         imgBtn_next_separator_Internalname = sPrefix+"BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = sPrefix+"BTN_LAST";
         imgBtn_last_separator_Internalname = sPrefix+"BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = sPrefix+"BTN_SELECT";
         imgBtn_select_separator_Internalname = sPrefix+"BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = sPrefix+"BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = sPrefix+"BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = sPrefix+"BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = sPrefix+"BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = sPrefix+"BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = sPrefix+"BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = sPrefix+"SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = sPrefix+"TABLETOOLBAR";
         lblTextblockfile_usuariocod_Internalname = sPrefix+"TEXTBLOCKFILE_USUARIOCOD";
         edtFile_UsuarioCod_Internalname = sPrefix+"FILE_USUARIOCOD";
         lblTextblockfile_row_Internalname = sPrefix+"TEXTBLOCKFILE_ROW";
         edtFile_Row_Internalname = sPrefix+"FILE_ROW";
         lblTextblockfile_col1_Internalname = sPrefix+"TEXTBLOCKFILE_COL1";
         edtFile_Col1_Internalname = sPrefix+"FILE_COL1";
         lblTextblockfile_col2_Internalname = sPrefix+"TEXTBLOCKFILE_COL2";
         edtFile_Col2_Internalname = sPrefix+"FILE_COL2";
         lblTextblockfile_col3_Internalname = sPrefix+"TEXTBLOCKFILE_COL3";
         edtFile_Col3_Internalname = sPrefix+"FILE_COL3";
         tblTable2_Internalname = sPrefix+"TABLE2";
         bttBtn_enter_Internalname = sPrefix+"BTN_ENTER";
         bttBtn_cancel_Internalname = sPrefix+"BTN_CANCEL";
         bttBtn_delete_Internalname = sPrefix+"BTN_DELETE";
         tblTable1_Internalname = sPrefix+"TABLE1";
         grpGroupdata_Internalname = sPrefix+"GROUPDATA";
         tblTablemain_Internalname = sPrefix+"TABLEMAIN";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtFile_Col3_Jsonclick = "";
         edtFile_Col3_Enabled = 1;
         edtFile_Col2_Jsonclick = "";
         edtFile_Col2_Enabled = 1;
         edtFile_Col1_Jsonclick = "";
         edtFile_Col1_Enabled = 1;
         edtFile_Row_Jsonclick = "";
         edtFile_Row_Enabled = 1;
         edtFile_UsuarioCod_Jsonclick = "";
         edtFile_UsuarioCod_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T001R14 */
         pr_default.execute(12, new Object[] {A435File_UsuarioCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'File_Usuario'.", "ForeignKeyNotFound", 1, "FILE_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtFile_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(12);
         GX_FocusControl = edtFile_Col1_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_File_usuariocod( int GX_Parm1 ,
                                         String GX_Parm2 )
      {
         A435File_UsuarioCod = GX_Parm1;
         sPrefix = GX_Parm2;
         /* Using cursor T001R14 */
         pr_default.execute(12, new Object[] {A435File_UsuarioCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'File_Usuario'.", "ForeignKeyNotFound", 1, "FILE_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtFile_UsuarioCod_Internalname;
         }
         pr_default.close(12);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_File_row( int GX_Parm1 ,
                                  int GX_Parm2 ,
                                  String GX_Parm3 ,
                                  String GX_Parm4 ,
                                  String GX_Parm5 ,
                                  String GX_Parm6 )
      {
         A435File_UsuarioCod = GX_Parm1;
         A504File_Row = GX_Parm2;
         A505File_Col1 = GX_Parm3;
         n505File_Col1 = false;
         A506File_Col2 = GX_Parm4;
         n506File_Col2 = false;
         A507File_Col3 = GX_Parm5;
         n507File_Col3 = false;
         sPrefix = GX_Parm6;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
         {
            standaloneStartupServer( ) ;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            context.wbHandled = 1;
            if ( ! wbErr )
            {
               AfterKeyLoadScreen( ) ;
            }
         }
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(A505File_Col1);
         isValidOutput.Add(A506File_Col2);
         isValidOutput.Add(A507File_Col3);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z435File_UsuarioCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z504File_Row), 8, 0, ",", "")));
         isValidOutput.Add(Z505File_Col1);
         isValidOutput.Add(Z506File_Col2);
         isValidOutput.Add(Z507File_Col3);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'componentprocess',iparms:[{postForm:true},{sPrefix:true},{sSFPrefix:true},{sCompEvt:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z505File_Col1 = "";
         Z506File_Col2 = "";
         Z507File_Col3 = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         sXEvt = "";
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockfile_usuariocod_Jsonclick = "";
         lblTextblockfile_row_Jsonclick = "";
         lblTextblockfile_col1_Jsonclick = "";
         A505File_Col1 = "";
         lblTextblockfile_col2_Jsonclick = "";
         A506File_Col2 = "";
         lblTextblockfile_col3_Jsonclick = "";
         A507File_Col3 = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T001R5_A504File_Row = new int[1] ;
         T001R5_A505File_Col1 = new String[] {""} ;
         T001R5_n505File_Col1 = new bool[] {false} ;
         T001R5_A506File_Col2 = new String[] {""} ;
         T001R5_n506File_Col2 = new bool[] {false} ;
         T001R5_A507File_Col3 = new String[] {""} ;
         T001R5_n507File_Col3 = new bool[] {false} ;
         T001R5_A435File_UsuarioCod = new int[1] ;
         T001R4_A435File_UsuarioCod = new int[1] ;
         T001R6_A435File_UsuarioCod = new int[1] ;
         T001R7_A435File_UsuarioCod = new int[1] ;
         T001R7_A504File_Row = new int[1] ;
         T001R3_A504File_Row = new int[1] ;
         T001R3_A505File_Col1 = new String[] {""} ;
         T001R3_n505File_Col1 = new bool[] {false} ;
         T001R3_A506File_Col2 = new String[] {""} ;
         T001R3_n506File_Col2 = new bool[] {false} ;
         T001R3_A507File_Col3 = new String[] {""} ;
         T001R3_n507File_Col3 = new bool[] {false} ;
         T001R3_A435File_UsuarioCod = new int[1] ;
         sMode73 = "";
         T001R8_A435File_UsuarioCod = new int[1] ;
         T001R8_A504File_Row = new int[1] ;
         T001R9_A435File_UsuarioCod = new int[1] ;
         T001R9_A504File_Row = new int[1] ;
         T001R2_A504File_Row = new int[1] ;
         T001R2_A505File_Col1 = new String[] {""} ;
         T001R2_n505File_Col1 = new bool[] {false} ;
         T001R2_A506File_Col2 = new String[] {""} ;
         T001R2_n506File_Col2 = new bool[] {false} ;
         T001R2_A507File_Col3 = new String[] {""} ;
         T001R2_n507File_Col3 = new bool[] {false} ;
         T001R2_A435File_UsuarioCod = new int[1] ;
         T001R13_A435File_UsuarioCod = new int[1] ;
         T001R13_A504File_Row = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         T001R14_A435File_UsuarioCod = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tmp_file__default(),
            new Object[][] {
                new Object[] {
               T001R2_A504File_Row, T001R2_A505File_Col1, T001R2_n505File_Col1, T001R2_A506File_Col2, T001R2_n506File_Col2, T001R2_A507File_Col3, T001R2_n507File_Col3, T001R2_A435File_UsuarioCod
               }
               , new Object[] {
               T001R3_A504File_Row, T001R3_A505File_Col1, T001R3_n505File_Col1, T001R3_A506File_Col2, T001R3_n506File_Col2, T001R3_A507File_Col3, T001R3_n507File_Col3, T001R3_A435File_UsuarioCod
               }
               , new Object[] {
               T001R4_A435File_UsuarioCod
               }
               , new Object[] {
               T001R5_A504File_Row, T001R5_A505File_Col1, T001R5_n505File_Col1, T001R5_A506File_Col2, T001R5_n506File_Col2, T001R5_A507File_Col3, T001R5_n507File_Col3, T001R5_A435File_UsuarioCod
               }
               , new Object[] {
               T001R6_A435File_UsuarioCod
               }
               , new Object[] {
               T001R7_A435File_UsuarioCod, T001R7_A504File_Row
               }
               , new Object[] {
               T001R8_A435File_UsuarioCod, T001R8_A504File_Row
               }
               , new Object[] {
               T001R9_A435File_UsuarioCod, T001R9_A504File_Row
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001R13_A435File_UsuarioCod, T001R13_A504File_Row
               }
               , new Object[] {
               T001R14_A435File_UsuarioCod
               }
            }
         );
      }

      private short GxWebError ;
      private short nDynComponent ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short nDraw ;
      private short nDoneStart ;
      private short GX_JID ;
      private short RcdFound73 ;
      private short Gx_BScreen ;
      private short wbTemp ;
      private int Z435File_UsuarioCod ;
      private int Z504File_Row ;
      private int A435File_UsuarioCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtFile_UsuarioCod_Enabled ;
      private int A504File_Row ;
      private int edtFile_Row_Enabled ;
      private int edtFile_Col1_Enabled ;
      private int edtFile_Col2_Enabled ;
      private int edtFile_Col3_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String sXEvt ;
      private String GX_FocusControl ;
      private String edtFile_UsuarioCod_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockfile_usuariocod_Internalname ;
      private String lblTextblockfile_usuariocod_Jsonclick ;
      private String edtFile_UsuarioCod_Jsonclick ;
      private String lblTextblockfile_row_Internalname ;
      private String lblTextblockfile_row_Jsonclick ;
      private String edtFile_Row_Internalname ;
      private String edtFile_Row_Jsonclick ;
      private String lblTextblockfile_col1_Internalname ;
      private String lblTextblockfile_col1_Jsonclick ;
      private String edtFile_Col1_Internalname ;
      private String edtFile_Col1_Jsonclick ;
      private String lblTextblockfile_col2_Internalname ;
      private String lblTextblockfile_col2_Jsonclick ;
      private String edtFile_Col2_Internalname ;
      private String edtFile_Col2_Jsonclick ;
      private String lblTextblockfile_col3_Internalname ;
      private String lblTextblockfile_col3_Jsonclick ;
      private String edtFile_Col3_Internalname ;
      private String edtFile_Col3_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode73 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n505File_Col1 ;
      private bool n506File_Col2 ;
      private bool n507File_Col3 ;
      private String Z505File_Col1 ;
      private String Z506File_Col2 ;
      private String Z507File_Col3 ;
      private String A505File_Col1 ;
      private String A506File_Col2 ;
      private String A507File_Col3 ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T001R5_A504File_Row ;
      private String[] T001R5_A505File_Col1 ;
      private bool[] T001R5_n505File_Col1 ;
      private String[] T001R5_A506File_Col2 ;
      private bool[] T001R5_n506File_Col2 ;
      private String[] T001R5_A507File_Col3 ;
      private bool[] T001R5_n507File_Col3 ;
      private int[] T001R5_A435File_UsuarioCod ;
      private int[] T001R4_A435File_UsuarioCod ;
      private int[] T001R6_A435File_UsuarioCod ;
      private int[] T001R7_A435File_UsuarioCod ;
      private int[] T001R7_A504File_Row ;
      private int[] T001R3_A504File_Row ;
      private String[] T001R3_A505File_Col1 ;
      private bool[] T001R3_n505File_Col1 ;
      private String[] T001R3_A506File_Col2 ;
      private bool[] T001R3_n506File_Col2 ;
      private String[] T001R3_A507File_Col3 ;
      private bool[] T001R3_n507File_Col3 ;
      private int[] T001R3_A435File_UsuarioCod ;
      private int[] T001R8_A435File_UsuarioCod ;
      private int[] T001R8_A504File_Row ;
      private int[] T001R9_A435File_UsuarioCod ;
      private int[] T001R9_A504File_Row ;
      private int[] T001R2_A504File_Row ;
      private String[] T001R2_A505File_Col1 ;
      private bool[] T001R2_n505File_Col1 ;
      private String[] T001R2_A506File_Col2 ;
      private bool[] T001R2_n506File_Col2 ;
      private String[] T001R2_A507File_Col3 ;
      private bool[] T001R2_n507File_Col3 ;
      private int[] T001R2_A435File_UsuarioCod ;
      private int[] T001R13_A435File_UsuarioCod ;
      private int[] T001R13_A504File_Row ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] T001R14_A435File_UsuarioCod ;
   }

   public class tmp_file__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001R5 ;
          prmT001R5 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmT001R4 ;
          prmT001R4 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001R6 ;
          prmT001R6 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001R7 ;
          prmT001R7 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmT001R3 ;
          prmT001R3 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmT001R8 ;
          prmT001R8 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmT001R9 ;
          prmT001R9 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmT001R2 ;
          prmT001R2 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmT001R10 ;
          prmT001R10 = new Object[] {
          new Object[] {"@File_Row",SqlDbType.Int,8,0} ,
          new Object[] {"@File_Col1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_Col2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_Col3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001R11 ;
          prmT001R11 = new Object[] {
          new Object[] {"@File_Col1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_Col2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_Col3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmT001R12 ;
          prmT001R12 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmT001R13 ;
          prmT001R13 = new Object[] {
          } ;
          Object[] prmT001R14 ;
          prmT001R14 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001R2", "SELECT [File_Row], [File_Col1], [File_Col2], [File_Col3], [File_UsuarioCod] AS File_UsuarioCod FROM [Tmp_File] WITH (UPDLOCK) WHERE [File_UsuarioCod] = @File_UsuarioCod AND [File_Row] = @File_Row ",true, GxErrorMask.GX_NOMASK, false, this,prmT001R2,1,0,true,false )
             ,new CursorDef("T001R3", "SELECT [File_Row], [File_Col1], [File_Col2], [File_Col3], [File_UsuarioCod] AS File_UsuarioCod FROM [Tmp_File] WITH (NOLOCK) WHERE [File_UsuarioCod] = @File_UsuarioCod AND [File_Row] = @File_Row ",true, GxErrorMask.GX_NOMASK, false, this,prmT001R3,1,0,true,false )
             ,new CursorDef("T001R4", "SELECT [Usuario_Codigo] AS File_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @File_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001R4,1,0,true,false )
             ,new CursorDef("T001R5", "SELECT TM1.[File_Row], TM1.[File_Col1], TM1.[File_Col2], TM1.[File_Col3], TM1.[File_UsuarioCod] AS File_UsuarioCod FROM [Tmp_File] TM1 WITH (NOLOCK) WHERE TM1.[File_UsuarioCod] = @File_UsuarioCod and TM1.[File_Row] = @File_Row ORDER BY TM1.[File_UsuarioCod], TM1.[File_Row]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001R5,100,0,true,false )
             ,new CursorDef("T001R6", "SELECT [Usuario_Codigo] AS File_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @File_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001R6,1,0,true,false )
             ,new CursorDef("T001R7", "SELECT [File_UsuarioCod] AS File_UsuarioCod, [File_Row] FROM [Tmp_File] WITH (NOLOCK) WHERE [File_UsuarioCod] = @File_UsuarioCod AND [File_Row] = @File_Row  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001R7,1,0,true,false )
             ,new CursorDef("T001R8", "SELECT TOP 1 [File_UsuarioCod] AS File_UsuarioCod, [File_Row] FROM [Tmp_File] WITH (NOLOCK) WHERE ( [File_UsuarioCod] > @File_UsuarioCod or [File_UsuarioCod] = @File_UsuarioCod and [File_Row] > @File_Row) ORDER BY [File_UsuarioCod], [File_Row]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001R8,1,0,true,true )
             ,new CursorDef("T001R9", "SELECT TOP 1 [File_UsuarioCod] AS File_UsuarioCod, [File_Row] FROM [Tmp_File] WITH (NOLOCK) WHERE ( [File_UsuarioCod] < @File_UsuarioCod or [File_UsuarioCod] = @File_UsuarioCod and [File_Row] < @File_Row) ORDER BY [File_UsuarioCod] DESC, [File_Row] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001R9,1,0,true,true )
             ,new CursorDef("T001R10", "INSERT INTO [Tmp_File]([File_Row], [File_Col1], [File_Col2], [File_Col3], [File_UsuarioCod]) VALUES(@File_Row, @File_Col1, @File_Col2, @File_Col3, @File_UsuarioCod)", GxErrorMask.GX_NOMASK,prmT001R10)
             ,new CursorDef("T001R11", "UPDATE [Tmp_File] SET [File_Col1]=@File_Col1, [File_Col2]=@File_Col2, [File_Col3]=@File_Col3  WHERE [File_UsuarioCod] = @File_UsuarioCod AND [File_Row] = @File_Row", GxErrorMask.GX_NOMASK,prmT001R11)
             ,new CursorDef("T001R12", "DELETE FROM [Tmp_File]  WHERE [File_UsuarioCod] = @File_UsuarioCod AND [File_Row] = @File_Row", GxErrorMask.GX_NOMASK,prmT001R12)
             ,new CursorDef("T001R13", "SELECT [File_UsuarioCod] AS File_UsuarioCod, [File_Row] FROM [Tmp_File] WITH (NOLOCK) ORDER BY [File_UsuarioCod], [File_Row]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001R13,100,0,true,false )
             ,new CursorDef("T001R14", "SELECT [Usuario_Codigo] AS File_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @File_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001R14,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
