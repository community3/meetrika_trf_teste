/*
               File: Geral_Grupo_Funcao
        Description: Grupo de Func�es
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:29:58.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class geral_grupo_funcao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A2179GrupoFuncao_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2179GrupoFuncao_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2179GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2179GrupoFuncao_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A2179GrupoFuncao_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7GrupoFuncao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GrupoFuncao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRUPOFUNCAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7GrupoFuncao_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkGrupoFuncao_Ativo.Name = "GRUPOFUNCAO_ATIVO";
         chkGrupoFuncao_Ativo.WebTags = "";
         chkGrupoFuncao_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkGrupoFuncao_Ativo_Internalname, "TitleCaption", chkGrupoFuncao_Ativo.Caption);
         chkGrupoFuncao_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Grupo de Func�es", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtGrupoFuncao_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public geral_grupo_funcao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public geral_grupo_funcao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_GrupoFuncao_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7GrupoFuncao_Codigo = aP1_GrupoFuncao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkGrupoFuncao_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2683( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2683e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGrupoFuncao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A619GrupoFuncao_Codigo), 6, 0, ",", "")), ((edtGrupoFuncao_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGrupoFuncao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtGrupoFuncao_Codigo_Visible, edtGrupoFuncao_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Geral_Grupo_Funcao.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2683( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2683( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2683e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_2683( true) ;
         }
         return  ;
      }

      protected void wb_table3_26_2683e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2683e( true) ;
         }
         else
         {
            wb_table1_2_2683e( false) ;
         }
      }

      protected void wb_table3_26_2683( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_2683e( true) ;
         }
         else
         {
            wb_table3_26_2683e( false) ;
         }
      }

      protected void wb_table2_5_2683( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2683( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2683e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2683e( true) ;
         }
         else
         {
            wb_table2_5_2683e( false) ;
         }
      }

      protected void wb_table4_13_2683( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgrupofuncao_nome_Internalname, "Nome", "", "", lblTextblockgrupofuncao_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGrupoFuncao_Nome_Internalname, A620GrupoFuncao_Nome, StringUtil.RTrim( context.localUtil.Format( A620GrupoFuncao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGrupoFuncao_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGrupoFuncao_Nome_Enabled, 0, "text", "", 380, "px", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgrupofuncao_ativo_Internalname, "Ativo?", "", "", lblTextblockgrupofuncao_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockgrupofuncao_ativo_Visible, 1, 0, "HLP_Geral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkGrupoFuncao_Ativo_Internalname, StringUtil.BoolToStr( A625GrupoFuncao_Ativo), "", "", chkGrupoFuncao_Ativo.Visible, chkGrupoFuncao_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(23, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2683e( true) ;
         }
         else
         {
            wb_table4_13_2683e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11262 */
         E11262 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A620GrupoFuncao_Nome = StringUtil.Upper( cgiGet( edtGrupoFuncao_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A620GrupoFuncao_Nome", A620GrupoFuncao_Nome);
               A625GrupoFuncao_Ativo = StringUtil.StrToBool( cgiGet( chkGrupoFuncao_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A625GrupoFuncao_Ativo", A625GrupoFuncao_Ativo);
               A619GrupoFuncao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGrupoFuncao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
               /* Read saved values. */
               Z619GrupoFuncao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z619GrupoFuncao_Codigo"), ",", "."));
               Z620GrupoFuncao_Nome = cgiGet( "Z620GrupoFuncao_Nome");
               Z625GrupoFuncao_Ativo = StringUtil.StrToBool( cgiGet( "Z625GrupoFuncao_Ativo"));
               Z2179GrupoFuncao_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z2179GrupoFuncao_AreaTrabalhoCod"), ",", "."));
               n2179GrupoFuncao_AreaTrabalhoCod = ((0==A2179GrupoFuncao_AreaTrabalhoCod) ? true : false);
               A2179GrupoFuncao_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z2179GrupoFuncao_AreaTrabalhoCod"), ",", "."));
               n2179GrupoFuncao_AreaTrabalhoCod = false;
               n2179GrupoFuncao_AreaTrabalhoCod = ((0==A2179GrupoFuncao_AreaTrabalhoCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N2179GrupoFuncao_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N2179GrupoFuncao_AreaTrabalhoCod"), ",", "."));
               n2179GrupoFuncao_AreaTrabalhoCod = ((0==A2179GrupoFuncao_AreaTrabalhoCod) ? true : false);
               AV7GrupoFuncao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vGRUPOFUNCAO_CODIGO"), ",", "."));
               AV11Insert_GrupoFuncao_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_GRUPOFUNCAO_AREATRABALHOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A2179GrupoFuncao_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "GRUPOFUNCAO_AREATRABALHOCOD"), ",", "."));
               n2179GrupoFuncao_AreaTrabalhoCod = ((0==A2179GrupoFuncao_AreaTrabalhoCod) ? true : false);
               A2180GrupoFuncao_AreaTrabalhoDesc = cgiGet( "GRUPOFUNCAO_AREATRABALHODESC");
               n2180GrupoFuncao_AreaTrabalhoDesc = false;
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Geral_Grupo_Funcao";
               A619GrupoFuncao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGrupoFuncao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2179GrupoFuncao_AreaTrabalhoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A619GrupoFuncao_Codigo != Z619GrupoFuncao_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("geral_grupo_funcao:[SecurityCheckFailed value for]"+"GrupoFuncao_Codigo:"+context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("geral_grupo_funcao:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("geral_grupo_funcao:[SecurityCheckFailed value for]"+"GrupoFuncao_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A2179GrupoFuncao_AreaTrabalhoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A619GrupoFuncao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode83 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode83;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound83 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_260( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "GRUPOFUNCAO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtGrupoFuncao_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11262 */
                           E11262 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12262 */
                           E12262 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12262 */
            E12262 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2683( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2683( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_260( )
      {
         BeforeValidate2683( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2683( ) ;
            }
            else
            {
               CheckExtendedTable2683( ) ;
               CloseExtendedTableCursors2683( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption260( )
      {
      }

      protected void E11262( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "GrupoFuncao_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_GrupoFuncao_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_GrupoFuncao_AreaTrabalhoCod), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtGrupoFuncao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoFuncao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGrupoFuncao_Codigo_Visible), 5, 0)));
      }

      protected void E12262( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwgeral_grupo_funcao.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2683( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z620GrupoFuncao_Nome = T00263_A620GrupoFuncao_Nome[0];
               Z625GrupoFuncao_Ativo = T00263_A625GrupoFuncao_Ativo[0];
               Z2179GrupoFuncao_AreaTrabalhoCod = T00263_A2179GrupoFuncao_AreaTrabalhoCod[0];
            }
            else
            {
               Z620GrupoFuncao_Nome = A620GrupoFuncao_Nome;
               Z625GrupoFuncao_Ativo = A625GrupoFuncao_Ativo;
               Z2179GrupoFuncao_AreaTrabalhoCod = A2179GrupoFuncao_AreaTrabalhoCod;
            }
         }
         if ( GX_JID == -10 )
         {
            Z619GrupoFuncao_Codigo = A619GrupoFuncao_Codigo;
            Z620GrupoFuncao_Nome = A620GrupoFuncao_Nome;
            Z625GrupoFuncao_Ativo = A625GrupoFuncao_Ativo;
            Z2179GrupoFuncao_AreaTrabalhoCod = A2179GrupoFuncao_AreaTrabalhoCod;
            Z2180GrupoFuncao_AreaTrabalhoDesc = A2180GrupoFuncao_AreaTrabalhoDesc;
         }
      }

      protected void standaloneNotModal( )
      {
         edtGrupoFuncao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoFuncao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGrupoFuncao_Codigo_Enabled), 5, 0)));
         AV14Pgmname = "Geral_Grupo_Funcao";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtGrupoFuncao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoFuncao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGrupoFuncao_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7GrupoFuncao_Codigo) )
         {
            A619GrupoFuncao_Codigo = AV7GrupoFuncao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkGrupoFuncao_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkGrupoFuncao_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkGrupoFuncao_Ativo.Visible), 5, 0)));
         lblTextblockgrupofuncao_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockgrupofuncao_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockgrupofuncao_ativo_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_GrupoFuncao_AreaTrabalhoCod) )
         {
            A2179GrupoFuncao_AreaTrabalhoCod = AV11Insert_GrupoFuncao_AreaTrabalhoCod;
            n2179GrupoFuncao_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2179GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2179GrupoFuncao_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A2179GrupoFuncao_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
            {
               A2179GrupoFuncao_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               n2179GrupoFuncao_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2179GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2179GrupoFuncao_AreaTrabalhoCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A625GrupoFuncao_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A625GrupoFuncao_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A625GrupoFuncao_Ativo", A625GrupoFuncao_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00264 */
            pr_default.execute(2, new Object[] {n2179GrupoFuncao_AreaTrabalhoCod, A2179GrupoFuncao_AreaTrabalhoCod});
            A2180GrupoFuncao_AreaTrabalhoDesc = T00264_A2180GrupoFuncao_AreaTrabalhoDesc[0];
            n2180GrupoFuncao_AreaTrabalhoDesc = T00264_n2180GrupoFuncao_AreaTrabalhoDesc[0];
            pr_default.close(2);
         }
      }

      protected void Load2683( )
      {
         /* Using cursor T00265 */
         pr_default.execute(3, new Object[] {A619GrupoFuncao_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound83 = 1;
            A620GrupoFuncao_Nome = T00265_A620GrupoFuncao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A620GrupoFuncao_Nome", A620GrupoFuncao_Nome);
            A625GrupoFuncao_Ativo = T00265_A625GrupoFuncao_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A625GrupoFuncao_Ativo", A625GrupoFuncao_Ativo);
            A2180GrupoFuncao_AreaTrabalhoDesc = T00265_A2180GrupoFuncao_AreaTrabalhoDesc[0];
            n2180GrupoFuncao_AreaTrabalhoDesc = T00265_n2180GrupoFuncao_AreaTrabalhoDesc[0];
            A2179GrupoFuncao_AreaTrabalhoCod = T00265_A2179GrupoFuncao_AreaTrabalhoCod[0];
            n2179GrupoFuncao_AreaTrabalhoCod = T00265_n2179GrupoFuncao_AreaTrabalhoCod[0];
            ZM2683( -10) ;
         }
         pr_default.close(3);
         OnLoadActions2683( ) ;
      }

      protected void OnLoadActions2683( )
      {
      }

      protected void CheckExtendedTable2683( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T00264 */
         pr_default.execute(2, new Object[] {n2179GrupoFuncao_AreaTrabalhoCod, A2179GrupoFuncao_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A2179GrupoFuncao_AreaTrabalhoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_GrupoFucaoAreaTrabalho'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A2180GrupoFuncao_AreaTrabalhoDesc = T00264_A2180GrupoFuncao_AreaTrabalhoDesc[0];
         n2180GrupoFuncao_AreaTrabalhoDesc = T00264_n2180GrupoFuncao_AreaTrabalhoDesc[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors2683( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_11( int A2179GrupoFuncao_AreaTrabalhoCod )
      {
         /* Using cursor T00266 */
         pr_default.execute(4, new Object[] {n2179GrupoFuncao_AreaTrabalhoCod, A2179GrupoFuncao_AreaTrabalhoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A2179GrupoFuncao_AreaTrabalhoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_GrupoFucaoAreaTrabalho'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A2180GrupoFuncao_AreaTrabalhoDesc = T00266_A2180GrupoFuncao_AreaTrabalhoDesc[0];
         n2180GrupoFuncao_AreaTrabalhoDesc = T00266_n2180GrupoFuncao_AreaTrabalhoDesc[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A2180GrupoFuncao_AreaTrabalhoDesc)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey2683( )
      {
         /* Using cursor T00267 */
         pr_default.execute(5, new Object[] {A619GrupoFuncao_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound83 = 1;
         }
         else
         {
            RcdFound83 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00263 */
         pr_default.execute(1, new Object[] {A619GrupoFuncao_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2683( 10) ;
            RcdFound83 = 1;
            A619GrupoFuncao_Codigo = T00263_A619GrupoFuncao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
            A620GrupoFuncao_Nome = T00263_A620GrupoFuncao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A620GrupoFuncao_Nome", A620GrupoFuncao_Nome);
            A625GrupoFuncao_Ativo = T00263_A625GrupoFuncao_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A625GrupoFuncao_Ativo", A625GrupoFuncao_Ativo);
            A2179GrupoFuncao_AreaTrabalhoCod = T00263_A2179GrupoFuncao_AreaTrabalhoCod[0];
            n2179GrupoFuncao_AreaTrabalhoCod = T00263_n2179GrupoFuncao_AreaTrabalhoCod[0];
            Z619GrupoFuncao_Codigo = A619GrupoFuncao_Codigo;
            sMode83 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2683( ) ;
            if ( AnyError == 1 )
            {
               RcdFound83 = 0;
               InitializeNonKey2683( ) ;
            }
            Gx_mode = sMode83;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound83 = 0;
            InitializeNonKey2683( ) ;
            sMode83 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode83;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2683( ) ;
         if ( RcdFound83 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound83 = 0;
         /* Using cursor T00268 */
         pr_default.execute(6, new Object[] {A619GrupoFuncao_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00268_A619GrupoFuncao_Codigo[0] < A619GrupoFuncao_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00268_A619GrupoFuncao_Codigo[0] > A619GrupoFuncao_Codigo ) ) )
            {
               A619GrupoFuncao_Codigo = T00268_A619GrupoFuncao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
               RcdFound83 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound83 = 0;
         /* Using cursor T00269 */
         pr_default.execute(7, new Object[] {A619GrupoFuncao_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00269_A619GrupoFuncao_Codigo[0] > A619GrupoFuncao_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00269_A619GrupoFuncao_Codigo[0] < A619GrupoFuncao_Codigo ) ) )
            {
               A619GrupoFuncao_Codigo = T00269_A619GrupoFuncao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
               RcdFound83 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2683( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtGrupoFuncao_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2683( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound83 == 1 )
            {
               if ( A619GrupoFuncao_Codigo != Z619GrupoFuncao_Codigo )
               {
                  A619GrupoFuncao_Codigo = Z619GrupoFuncao_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "GRUPOFUNCAO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtGrupoFuncao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtGrupoFuncao_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2683( ) ;
                  GX_FocusControl = edtGrupoFuncao_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A619GrupoFuncao_Codigo != Z619GrupoFuncao_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtGrupoFuncao_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2683( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "GRUPOFUNCAO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtGrupoFuncao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtGrupoFuncao_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2683( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A619GrupoFuncao_Codigo != Z619GrupoFuncao_Codigo )
         {
            A619GrupoFuncao_Codigo = Z619GrupoFuncao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "GRUPOFUNCAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGrupoFuncao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtGrupoFuncao_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2683( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00262 */
            pr_default.execute(0, new Object[] {A619GrupoFuncao_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Geral_Grupo_Funcao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z620GrupoFuncao_Nome, T00262_A620GrupoFuncao_Nome[0]) != 0 ) || ( Z625GrupoFuncao_Ativo != T00262_A625GrupoFuncao_Ativo[0] ) || ( Z2179GrupoFuncao_AreaTrabalhoCod != T00262_A2179GrupoFuncao_AreaTrabalhoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z620GrupoFuncao_Nome, T00262_A620GrupoFuncao_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("geral_grupo_funcao:[seudo value changed for attri]"+"GrupoFuncao_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z620GrupoFuncao_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00262_A620GrupoFuncao_Nome[0]);
               }
               if ( Z625GrupoFuncao_Ativo != T00262_A625GrupoFuncao_Ativo[0] )
               {
                  GXUtil.WriteLog("geral_grupo_funcao:[seudo value changed for attri]"+"GrupoFuncao_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z625GrupoFuncao_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00262_A625GrupoFuncao_Ativo[0]);
               }
               if ( Z2179GrupoFuncao_AreaTrabalhoCod != T00262_A2179GrupoFuncao_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("geral_grupo_funcao:[seudo value changed for attri]"+"GrupoFuncao_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z2179GrupoFuncao_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T00262_A2179GrupoFuncao_AreaTrabalhoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Geral_Grupo_Funcao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2683( )
      {
         BeforeValidate2683( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2683( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2683( 0) ;
            CheckOptimisticConcurrency2683( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2683( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2683( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002610 */
                     pr_default.execute(8, new Object[] {A620GrupoFuncao_Nome, A625GrupoFuncao_Ativo, n2179GrupoFuncao_AreaTrabalhoCod, A2179GrupoFuncao_AreaTrabalhoCod});
                     A619GrupoFuncao_Codigo = T002610_A619GrupoFuncao_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Geral_Grupo_Funcao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption260( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2683( ) ;
            }
            EndLevel2683( ) ;
         }
         CloseExtendedTableCursors2683( ) ;
      }

      protected void Update2683( )
      {
         BeforeValidate2683( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2683( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2683( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2683( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2683( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002611 */
                     pr_default.execute(9, new Object[] {A620GrupoFuncao_Nome, A625GrupoFuncao_Ativo, n2179GrupoFuncao_AreaTrabalhoCod, A2179GrupoFuncao_AreaTrabalhoCod, A619GrupoFuncao_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Geral_Grupo_Funcao") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Geral_Grupo_Funcao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2683( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2683( ) ;
         }
         CloseExtendedTableCursors2683( ) ;
      }

      protected void DeferredUpdate2683( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2683( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2683( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2683( ) ;
            AfterConfirm2683( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2683( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002612 */
                  pr_default.execute(10, new Object[] {A619GrupoFuncao_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Geral_Grupo_Funcao") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode83 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2683( ) ;
         Gx_mode = sMode83;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2683( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002613 */
            pr_default.execute(11, new Object[] {n2179GrupoFuncao_AreaTrabalhoCod, A2179GrupoFuncao_AreaTrabalhoCod});
            A2180GrupoFuncao_AreaTrabalhoDesc = T002613_A2180GrupoFuncao_AreaTrabalhoDesc[0];
            n2180GrupoFuncao_AreaTrabalhoDesc = T002613_n2180GrupoFuncao_AreaTrabalhoDesc[0];
            pr_default.close(11);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002614 */
            pr_default.execute(12, new Object[] {A619GrupoFuncao_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Funcao"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
         }
      }

      protected void EndLevel2683( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2683( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "Geral_Grupo_Funcao");
            if ( AnyError == 0 )
            {
               ConfirmValues260( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "Geral_Grupo_Funcao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2683( )
      {
         /* Scan By routine */
         /* Using cursor T002615 */
         pr_default.execute(13);
         RcdFound83 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound83 = 1;
            A619GrupoFuncao_Codigo = T002615_A619GrupoFuncao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2683( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound83 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound83 = 1;
            A619GrupoFuncao_Codigo = T002615_A619GrupoFuncao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2683( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm2683( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2683( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2683( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2683( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2683( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2683( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2683( )
      {
         edtGrupoFuncao_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoFuncao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGrupoFuncao_Nome_Enabled), 5, 0)));
         chkGrupoFuncao_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkGrupoFuncao_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkGrupoFuncao_Ativo.Enabled), 5, 0)));
         edtGrupoFuncao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoFuncao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGrupoFuncao_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues260( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299295935");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("geral_grupo_funcao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7GrupoFuncao_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z619GrupoFuncao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z620GrupoFuncao_Nome", Z620GrupoFuncao_Nome);
         GxWebStd.gx_boolean_hidden_field( context, "Z625GrupoFuncao_Ativo", Z625GrupoFuncao_Ativo);
         GxWebStd.gx_hidden_field( context, "Z2179GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2179GrupoFuncao_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N2179GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2179GrupoFuncao_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vGRUPOFUNCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7GrupoFuncao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_GRUPOFUNCAO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_GrupoFuncao_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRUPOFUNCAO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2179GrupoFuncao_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRUPOFUNCAO_AREATRABALHODESC", A2180GrupoFuncao_AreaTrabalhoDesc);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRUPOFUNCAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7GrupoFuncao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Geral_Grupo_Funcao";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2179GrupoFuncao_AreaTrabalhoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("geral_grupo_funcao:[SendSecurityCheck value for]"+"GrupoFuncao_Codigo:"+context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("geral_grupo_funcao:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("geral_grupo_funcao:[SendSecurityCheck value for]"+"GrupoFuncao_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A2179GrupoFuncao_AreaTrabalhoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("geral_grupo_funcao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7GrupoFuncao_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Geral_Grupo_Funcao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Grupo de Func�es" ;
      }

      protected void InitializeNonKey2683( )
      {
         A620GrupoFuncao_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A620GrupoFuncao_Nome", A620GrupoFuncao_Nome);
         A2180GrupoFuncao_AreaTrabalhoDesc = "";
         n2180GrupoFuncao_AreaTrabalhoDesc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2180GrupoFuncao_AreaTrabalhoDesc", A2180GrupoFuncao_AreaTrabalhoDesc);
         A2179GrupoFuncao_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         n2179GrupoFuncao_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2179GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2179GrupoFuncao_AreaTrabalhoCod), 6, 0)));
         A625GrupoFuncao_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A625GrupoFuncao_Ativo", A625GrupoFuncao_Ativo);
         Z620GrupoFuncao_Nome = "";
         Z625GrupoFuncao_Ativo = false;
         Z2179GrupoFuncao_AreaTrabalhoCod = 0;
      }

      protected void InitAll2683( )
      {
         A619GrupoFuncao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A619GrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A619GrupoFuncao_Codigo), 6, 0)));
         InitializeNonKey2683( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2179GrupoFuncao_AreaTrabalhoCod = i2179GrupoFuncao_AreaTrabalhoCod;
         n2179GrupoFuncao_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2179GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2179GrupoFuncao_AreaTrabalhoCod), 6, 0)));
         A625GrupoFuncao_Ativo = i625GrupoFuncao_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A625GrupoFuncao_Ativo", A625GrupoFuncao_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299295953");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("geral_grupo_funcao.js", "?20205299295954");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockgrupofuncao_nome_Internalname = "TEXTBLOCKGRUPOFUNCAO_NOME";
         edtGrupoFuncao_Nome_Internalname = "GRUPOFUNCAO_NOME";
         lblTextblockgrupofuncao_ativo_Internalname = "TEXTBLOCKGRUPOFUNCAO_ATIVO";
         chkGrupoFuncao_Ativo_Internalname = "GRUPOFUNCAO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtGrupoFuncao_Codigo_Internalname = "GRUPOFUNCAO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Grupo de Func�es";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Grupo de Func�es";
         chkGrupoFuncao_Ativo.Enabled = 1;
         chkGrupoFuncao_Ativo.Visible = 1;
         lblTextblockgrupofuncao_ativo_Visible = 1;
         edtGrupoFuncao_Nome_Jsonclick = "";
         edtGrupoFuncao_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtGrupoFuncao_Codigo_Jsonclick = "";
         edtGrupoFuncao_Codigo_Enabled = 0;
         edtGrupoFuncao_Codigo_Visible = 1;
         chkGrupoFuncao_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7GrupoFuncao_Codigo',fld:'vGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12262',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z620GrupoFuncao_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockgrupofuncao_nome_Jsonclick = "";
         A620GrupoFuncao_Nome = "";
         lblTextblockgrupofuncao_ativo_Jsonclick = "";
         A2180GrupoFuncao_AreaTrabalhoDesc = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode83 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z2180GrupoFuncao_AreaTrabalhoDesc = "";
         T00264_A2180GrupoFuncao_AreaTrabalhoDesc = new String[] {""} ;
         T00264_n2180GrupoFuncao_AreaTrabalhoDesc = new bool[] {false} ;
         T00265_A619GrupoFuncao_Codigo = new int[1] ;
         T00265_A620GrupoFuncao_Nome = new String[] {""} ;
         T00265_A625GrupoFuncao_Ativo = new bool[] {false} ;
         T00265_A2180GrupoFuncao_AreaTrabalhoDesc = new String[] {""} ;
         T00265_n2180GrupoFuncao_AreaTrabalhoDesc = new bool[] {false} ;
         T00265_A2179GrupoFuncao_AreaTrabalhoCod = new int[1] ;
         T00265_n2179GrupoFuncao_AreaTrabalhoCod = new bool[] {false} ;
         T00266_A2180GrupoFuncao_AreaTrabalhoDesc = new String[] {""} ;
         T00266_n2180GrupoFuncao_AreaTrabalhoDesc = new bool[] {false} ;
         T00267_A619GrupoFuncao_Codigo = new int[1] ;
         T00263_A619GrupoFuncao_Codigo = new int[1] ;
         T00263_A620GrupoFuncao_Nome = new String[] {""} ;
         T00263_A625GrupoFuncao_Ativo = new bool[] {false} ;
         T00263_A2179GrupoFuncao_AreaTrabalhoCod = new int[1] ;
         T00263_n2179GrupoFuncao_AreaTrabalhoCod = new bool[] {false} ;
         T00268_A619GrupoFuncao_Codigo = new int[1] ;
         T00269_A619GrupoFuncao_Codigo = new int[1] ;
         T00262_A619GrupoFuncao_Codigo = new int[1] ;
         T00262_A620GrupoFuncao_Nome = new String[] {""} ;
         T00262_A625GrupoFuncao_Ativo = new bool[] {false} ;
         T00262_A2179GrupoFuncao_AreaTrabalhoCod = new int[1] ;
         T00262_n2179GrupoFuncao_AreaTrabalhoCod = new bool[] {false} ;
         T002610_A619GrupoFuncao_Codigo = new int[1] ;
         T002613_A2180GrupoFuncao_AreaTrabalhoDesc = new String[] {""} ;
         T002613_n2180GrupoFuncao_AreaTrabalhoDesc = new bool[] {false} ;
         T002614_A621Funcao_Codigo = new int[1] ;
         T002615_A619GrupoFuncao_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.geral_grupo_funcao__default(),
            new Object[][] {
                new Object[] {
               T00262_A619GrupoFuncao_Codigo, T00262_A620GrupoFuncao_Nome, T00262_A625GrupoFuncao_Ativo, T00262_A2179GrupoFuncao_AreaTrabalhoCod, T00262_n2179GrupoFuncao_AreaTrabalhoCod
               }
               , new Object[] {
               T00263_A619GrupoFuncao_Codigo, T00263_A620GrupoFuncao_Nome, T00263_A625GrupoFuncao_Ativo, T00263_A2179GrupoFuncao_AreaTrabalhoCod, T00263_n2179GrupoFuncao_AreaTrabalhoCod
               }
               , new Object[] {
               T00264_A2180GrupoFuncao_AreaTrabalhoDesc, T00264_n2180GrupoFuncao_AreaTrabalhoDesc
               }
               , new Object[] {
               T00265_A619GrupoFuncao_Codigo, T00265_A620GrupoFuncao_Nome, T00265_A625GrupoFuncao_Ativo, T00265_A2180GrupoFuncao_AreaTrabalhoDesc, T00265_n2180GrupoFuncao_AreaTrabalhoDesc, T00265_A2179GrupoFuncao_AreaTrabalhoCod, T00265_n2179GrupoFuncao_AreaTrabalhoCod
               }
               , new Object[] {
               T00266_A2180GrupoFuncao_AreaTrabalhoDesc, T00266_n2180GrupoFuncao_AreaTrabalhoDesc
               }
               , new Object[] {
               T00267_A619GrupoFuncao_Codigo
               }
               , new Object[] {
               T00268_A619GrupoFuncao_Codigo
               }
               , new Object[] {
               T00269_A619GrupoFuncao_Codigo
               }
               , new Object[] {
               T002610_A619GrupoFuncao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002613_A2180GrupoFuncao_AreaTrabalhoDesc, T002613_n2180GrupoFuncao_AreaTrabalhoDesc
               }
               , new Object[] {
               T002614_A621Funcao_Codigo
               }
               , new Object[] {
               T002615_A619GrupoFuncao_Codigo
               }
            }
         );
         Z625GrupoFuncao_Ativo = true;
         A625GrupoFuncao_Ativo = true;
         i625GrupoFuncao_Ativo = true;
         AV14Pgmname = "Geral_Grupo_Funcao";
         Z2179GrupoFuncao_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         n2179GrupoFuncao_AreaTrabalhoCod = false;
         N2179GrupoFuncao_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         n2179GrupoFuncao_AreaTrabalhoCod = false;
         i2179GrupoFuncao_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         n2179GrupoFuncao_AreaTrabalhoCod = false;
         A2179GrupoFuncao_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         n2179GrupoFuncao_AreaTrabalhoCod = false;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound83 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7GrupoFuncao_Codigo ;
      private int Z619GrupoFuncao_Codigo ;
      private int Z2179GrupoFuncao_AreaTrabalhoCod ;
      private int N2179GrupoFuncao_AreaTrabalhoCod ;
      private int A2179GrupoFuncao_AreaTrabalhoCod ;
      private int AV7GrupoFuncao_Codigo ;
      private int trnEnded ;
      private int A619GrupoFuncao_Codigo ;
      private int edtGrupoFuncao_Codigo_Enabled ;
      private int edtGrupoFuncao_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtGrupoFuncao_Nome_Enabled ;
      private int lblTextblockgrupofuncao_ativo_Visible ;
      private int AV11Insert_GrupoFuncao_AreaTrabalhoCod ;
      private int AV15GXV1 ;
      private int i2179GrupoFuncao_AreaTrabalhoCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkGrupoFuncao_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtGrupoFuncao_Nome_Internalname ;
      private String edtGrupoFuncao_Codigo_Internalname ;
      private String edtGrupoFuncao_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockgrupofuncao_nome_Internalname ;
      private String lblTextblockgrupofuncao_nome_Jsonclick ;
      private String edtGrupoFuncao_Nome_Jsonclick ;
      private String lblTextblockgrupofuncao_ativo_Internalname ;
      private String lblTextblockgrupofuncao_ativo_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode83 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z625GrupoFuncao_Ativo ;
      private bool entryPointCalled ;
      private bool n2179GrupoFuncao_AreaTrabalhoCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A625GrupoFuncao_Ativo ;
      private bool n2180GrupoFuncao_AreaTrabalhoDesc ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i625GrupoFuncao_Ativo ;
      private String Z620GrupoFuncao_Nome ;
      private String A620GrupoFuncao_Nome ;
      private String A2180GrupoFuncao_AreaTrabalhoDesc ;
      private String Z2180GrupoFuncao_AreaTrabalhoDesc ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkGrupoFuncao_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T00264_A2180GrupoFuncao_AreaTrabalhoDesc ;
      private bool[] T00264_n2180GrupoFuncao_AreaTrabalhoDesc ;
      private int[] T00265_A619GrupoFuncao_Codigo ;
      private String[] T00265_A620GrupoFuncao_Nome ;
      private bool[] T00265_A625GrupoFuncao_Ativo ;
      private String[] T00265_A2180GrupoFuncao_AreaTrabalhoDesc ;
      private bool[] T00265_n2180GrupoFuncao_AreaTrabalhoDesc ;
      private int[] T00265_A2179GrupoFuncao_AreaTrabalhoCod ;
      private bool[] T00265_n2179GrupoFuncao_AreaTrabalhoCod ;
      private String[] T00266_A2180GrupoFuncao_AreaTrabalhoDesc ;
      private bool[] T00266_n2180GrupoFuncao_AreaTrabalhoDesc ;
      private int[] T00267_A619GrupoFuncao_Codigo ;
      private int[] T00263_A619GrupoFuncao_Codigo ;
      private String[] T00263_A620GrupoFuncao_Nome ;
      private bool[] T00263_A625GrupoFuncao_Ativo ;
      private int[] T00263_A2179GrupoFuncao_AreaTrabalhoCod ;
      private bool[] T00263_n2179GrupoFuncao_AreaTrabalhoCod ;
      private int[] T00268_A619GrupoFuncao_Codigo ;
      private int[] T00269_A619GrupoFuncao_Codigo ;
      private int[] T00262_A619GrupoFuncao_Codigo ;
      private String[] T00262_A620GrupoFuncao_Nome ;
      private bool[] T00262_A625GrupoFuncao_Ativo ;
      private int[] T00262_A2179GrupoFuncao_AreaTrabalhoCod ;
      private bool[] T00262_n2179GrupoFuncao_AreaTrabalhoCod ;
      private int[] T002610_A619GrupoFuncao_Codigo ;
      private String[] T002613_A2180GrupoFuncao_AreaTrabalhoDesc ;
      private bool[] T002613_n2180GrupoFuncao_AreaTrabalhoDesc ;
      private int[] T002614_A621Funcao_Codigo ;
      private int[] T002615_A619GrupoFuncao_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class geral_grupo_funcao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00265 ;
          prmT00265 = new Object[] {
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00264 ;
          prmT00264 = new Object[] {
          new Object[] {"@GrupoFuncao_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00266 ;
          prmT00266 = new Object[] {
          new Object[] {"@GrupoFuncao_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00267 ;
          prmT00267 = new Object[] {
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00263 ;
          prmT00263 = new Object[] {
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00268 ;
          prmT00268 = new Object[] {
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00269 ;
          prmT00269 = new Object[] {
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00262 ;
          prmT00262 = new Object[] {
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002610 ;
          prmT002610 = new Object[] {
          new Object[] {"@GrupoFuncao_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@GrupoFuncao_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@GrupoFuncao_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002611 ;
          prmT002611 = new Object[] {
          new Object[] {"@GrupoFuncao_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@GrupoFuncao_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@GrupoFuncao_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002612 ;
          prmT002612 = new Object[] {
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002613 ;
          prmT002613 = new Object[] {
          new Object[] {"@GrupoFuncao_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002614 ;
          prmT002614 = new Object[] {
          new Object[] {"@GrupoFuncao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002615 ;
          prmT002615 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T00262", "SELECT [GrupoFuncao_Codigo], [GrupoFuncao_Nome], [GrupoFuncao_Ativo], [GrupoFuncao_AreaTrabalhoCod] AS GrupoFuncao_AreaTrabalhoCod FROM [Geral_Grupo_Funcao] WITH (UPDLOCK) WHERE [GrupoFuncao_Codigo] = @GrupoFuncao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00262,1,0,true,false )
             ,new CursorDef("T00263", "SELECT [GrupoFuncao_Codigo], [GrupoFuncao_Nome], [GrupoFuncao_Ativo], [GrupoFuncao_AreaTrabalhoCod] AS GrupoFuncao_AreaTrabalhoCod FROM [Geral_Grupo_Funcao] WITH (NOLOCK) WHERE [GrupoFuncao_Codigo] = @GrupoFuncao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00263,1,0,true,false )
             ,new CursorDef("T00264", "SELECT [AreaTrabalho_Descricao] AS GrupoFuncao_AreaTrabalhoDesc FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @GrupoFuncao_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00264,1,0,true,false )
             ,new CursorDef("T00265", "SELECT TM1.[GrupoFuncao_Codigo], TM1.[GrupoFuncao_Nome], TM1.[GrupoFuncao_Ativo], T2.[AreaTrabalho_Descricao] AS GrupoFuncao_AreaTrabalhoDesc, TM1.[GrupoFuncao_AreaTrabalhoCod] AS GrupoFuncao_AreaTrabalhoCod FROM ([Geral_Grupo_Funcao] TM1 WITH (NOLOCK) LEFT JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[GrupoFuncao_AreaTrabalhoCod]) WHERE TM1.[GrupoFuncao_Codigo] = @GrupoFuncao_Codigo ORDER BY TM1.[GrupoFuncao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00265,100,0,true,false )
             ,new CursorDef("T00266", "SELECT [AreaTrabalho_Descricao] AS GrupoFuncao_AreaTrabalhoDesc FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @GrupoFuncao_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00266,1,0,true,false )
             ,new CursorDef("T00267", "SELECT [GrupoFuncao_Codigo] FROM [Geral_Grupo_Funcao] WITH (NOLOCK) WHERE [GrupoFuncao_Codigo] = @GrupoFuncao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00267,1,0,true,false )
             ,new CursorDef("T00268", "SELECT TOP 1 [GrupoFuncao_Codigo] FROM [Geral_Grupo_Funcao] WITH (NOLOCK) WHERE ( [GrupoFuncao_Codigo] > @GrupoFuncao_Codigo) ORDER BY [GrupoFuncao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00268,1,0,true,true )
             ,new CursorDef("T00269", "SELECT TOP 1 [GrupoFuncao_Codigo] FROM [Geral_Grupo_Funcao] WITH (NOLOCK) WHERE ( [GrupoFuncao_Codigo] < @GrupoFuncao_Codigo) ORDER BY [GrupoFuncao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00269,1,0,true,true )
             ,new CursorDef("T002610", "INSERT INTO [Geral_Grupo_Funcao]([GrupoFuncao_Nome], [GrupoFuncao_Ativo], [GrupoFuncao_AreaTrabalhoCod]) VALUES(@GrupoFuncao_Nome, @GrupoFuncao_Ativo, @GrupoFuncao_AreaTrabalhoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002610)
             ,new CursorDef("T002611", "UPDATE [Geral_Grupo_Funcao] SET [GrupoFuncao_Nome]=@GrupoFuncao_Nome, [GrupoFuncao_Ativo]=@GrupoFuncao_Ativo, [GrupoFuncao_AreaTrabalhoCod]=@GrupoFuncao_AreaTrabalhoCod  WHERE [GrupoFuncao_Codigo] = @GrupoFuncao_Codigo", GxErrorMask.GX_NOMASK,prmT002611)
             ,new CursorDef("T002612", "DELETE FROM [Geral_Grupo_Funcao]  WHERE [GrupoFuncao_Codigo] = @GrupoFuncao_Codigo", GxErrorMask.GX_NOMASK,prmT002612)
             ,new CursorDef("T002613", "SELECT [AreaTrabalho_Descricao] AS GrupoFuncao_AreaTrabalhoDesc FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @GrupoFuncao_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002613,1,0,true,false )
             ,new CursorDef("T002614", "SELECT TOP 1 [Funcao_Codigo] FROM [Geral_Funcao] WITH (NOLOCK) WHERE [GrupoFuncao_Codigo] = @GrupoFuncao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002614,1,0,true,true )
             ,new CursorDef("T002615", "SELECT [GrupoFuncao_Codigo] FROM [Geral_Grupo_Funcao] WITH (NOLOCK) ORDER BY [GrupoFuncao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002615,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
