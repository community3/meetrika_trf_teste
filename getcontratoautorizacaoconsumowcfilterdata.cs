/*
               File: GetContratoAutorizacaoConsumoWCFilterData
        Description: Get Contrato Autorizacao Consumo WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 1:57:39.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratoautorizacaoconsumowcfilterdata : GXProcedure
   {
      public getcontratoautorizacaoconsumowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratoautorizacaoconsumowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV27DDOName = aP0_DDOName;
         this.AV25SearchTxt = aP1_SearchTxt;
         this.AV26SearchTxtTo = aP2_SearchTxtTo;
         this.AV31OptionsJson = "" ;
         this.AV34OptionsDescJson = "" ;
         this.AV36OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV31OptionsJson;
         aP4_OptionsDescJson=this.AV34OptionsDescJson;
         aP5_OptionIndexesJson=this.AV36OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV27DDOName = aP0_DDOName;
         this.AV25SearchTxt = aP1_SearchTxt;
         this.AV26SearchTxtTo = aP2_SearchTxtTo;
         this.AV31OptionsJson = "" ;
         this.AV34OptionsDescJson = "" ;
         this.AV36OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV31OptionsJson;
         aP4_OptionsDescJson=this.AV34OptionsDescJson;
         aP5_OptionIndexesJson=this.AV36OptionIndexesJson;
         return AV36OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratoautorizacaoconsumowcfilterdata objgetcontratoautorizacaoconsumowcfilterdata;
         objgetcontratoautorizacaoconsumowcfilterdata = new getcontratoautorizacaoconsumowcfilterdata();
         objgetcontratoautorizacaoconsumowcfilterdata.AV27DDOName = aP0_DDOName;
         objgetcontratoautorizacaoconsumowcfilterdata.AV25SearchTxt = aP1_SearchTxt;
         objgetcontratoautorizacaoconsumowcfilterdata.AV26SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratoautorizacaoconsumowcfilterdata.AV31OptionsJson = "" ;
         objgetcontratoautorizacaoconsumowcfilterdata.AV34OptionsDescJson = "" ;
         objgetcontratoautorizacaoconsumowcfilterdata.AV36OptionIndexesJson = "" ;
         objgetcontratoautorizacaoconsumowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratoautorizacaoconsumowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratoautorizacaoconsumowcfilterdata);
         aP3_OptionsJson=this.AV31OptionsJson;
         aP4_OptionsDescJson=this.AV34OptionsDescJson;
         aP5_OptionIndexesJson=this.AV36OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratoautorizacaoconsumowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV30Options = (IGxCollection)(new GxSimpleCollection());
         AV33OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV35OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV27DDOName), "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM") == 0 )
         {
            /* Execute user subroutine: 'LOADAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV31OptionsJson = AV30Options.ToJSonString(false);
         AV34OptionsDescJson = AV33OptionsDesc.ToJSonString(false);
         AV36OptionIndexesJson = AV35OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV38Session.Get("ContratoAutorizacaoConsumoWCGridState"), "") == 0 )
         {
            AV40GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoAutorizacaoConsumoWCGridState"), "");
         }
         else
         {
            AV40GridState.FromXml(AV38Session.Get("ContratoAutorizacaoConsumoWCGridState"), "");
         }
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV40GridState.gxTpr_Filtervalues.Count )
         {
            AV41GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV40GridState.gxTpr_Filtervalues.Item(AV46GXV1));
            if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_CODIGO") == 0 )
            {
               AV10TFAutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
               AV11TFAutorizacaoConsumo_Codigo_To = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
            {
               AV12TFAutorizacaoConsumo_VigenciaInicio = context.localUtil.CToD( AV41GridStateFilterValue.gxTpr_Value, 2);
               AV13TFAutorizacaoConsumo_VigenciaInicio_To = context.localUtil.CToD( AV41GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VIGENCIAFIM") == 0 )
            {
               AV14TFAutorizacaoConsumo_VigenciaFim = context.localUtil.CToD( AV41GridStateFilterValue.gxTpr_Value, 2);
               AV15TFAutorizacaoConsumo_VigenciaFim_To = context.localUtil.CToD( AV41GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM") == 0 )
            {
               AV16TFAutorizacaoConsumo_UnidadeMedicaoNom = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL") == 0 )
            {
               AV17TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_QUANTIDADE") == 0 )
            {
               AV18TFAutorizacaoConsumo_Quantidade = (short)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
               AV19TFAutorizacaoConsumo_Quantidade_To = (short)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VALOR") == 0 )
            {
               AV20TFAutorizacaoConsumo_Valor = NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, ".");
               AV21TFAutorizacaoConsumo_Valor_To = NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_STATUS_SEL") == 0 )
            {
               AV22TFAutorizacaoConsumo_Status_SelsJson = AV41GridStateFilterValue.gxTpr_Value;
               AV23TFAutorizacaoConsumo_Status_Sels.FromJSonString(AV22TFAutorizacaoConsumo_Status_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_ATIVO_SEL") == 0 )
            {
               AV24TFAutorizacaoConsumo_Ativo_Sel = (short)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATO_CODIGO") == 0 )
            {
               AV43Contrato_Codigo = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
            }
            AV46GXV1 = (int)(AV46GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMOPTIONS' Routine */
         AV16TFAutorizacaoConsumo_UnidadeMedicaoNom = AV25SearchTxt;
         AV17TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1787AutorizacaoConsumo_Status ,
                                              AV23TFAutorizacaoConsumo_Status_Sels ,
                                              AV10TFAutorizacaoConsumo_Codigo ,
                                              AV11TFAutorizacaoConsumo_Codigo_To ,
                                              AV12TFAutorizacaoConsumo_VigenciaInicio ,
                                              AV13TFAutorizacaoConsumo_VigenciaInicio_To ,
                                              AV14TFAutorizacaoConsumo_VigenciaFim ,
                                              AV15TFAutorizacaoConsumo_VigenciaFim_To ,
                                              AV17TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ,
                                              AV16TFAutorizacaoConsumo_UnidadeMedicaoNom ,
                                              AV18TFAutorizacaoConsumo_Quantidade ,
                                              AV19TFAutorizacaoConsumo_Quantidade_To ,
                                              AV20TFAutorizacaoConsumo_Valor ,
                                              AV21TFAutorizacaoConsumo_Valor_To ,
                                              AV23TFAutorizacaoConsumo_Status_Sels.Count ,
                                              AV24TFAutorizacaoConsumo_Ativo_Sel ,
                                              A1774AutorizacaoConsumo_Codigo ,
                                              A1775AutorizacaoConsumo_VigenciaInicio ,
                                              A1776AutorizacaoConsumo_VigenciaFim ,
                                              A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                              A1779AutorizacaoConsumo_Quantidade ,
                                              A1782AutorizacaoConsumo_Valor ,
                                              A1780AutorizacaoConsumo_Ativo ,
                                              AV43Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV16TFAutorizacaoConsumo_UnidadeMedicaoNom = StringUtil.PadR( StringUtil.RTrim( AV16TFAutorizacaoConsumo_UnidadeMedicaoNom), 50, "%");
         /* Using cursor P00J02 */
         pr_default.execute(0, new Object[] {AV43Contrato_Codigo, AV10TFAutorizacaoConsumo_Codigo, AV11TFAutorizacaoConsumo_Codigo_To, AV12TFAutorizacaoConsumo_VigenciaInicio, AV13TFAutorizacaoConsumo_VigenciaInicio_To, AV14TFAutorizacaoConsumo_VigenciaFim, AV15TFAutorizacaoConsumo_VigenciaFim_To, lV16TFAutorizacaoConsumo_UnidadeMedicaoNom, AV17TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV18TFAutorizacaoConsumo_Quantidade, AV19TFAutorizacaoConsumo_Quantidade_To, AV20TFAutorizacaoConsumo_Valor, AV21TFAutorizacaoConsumo_Valor_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJ02 = false;
            A74Contrato_Codigo = P00J02_A74Contrato_Codigo[0];
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = P00J02_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
            A1780AutorizacaoConsumo_Ativo = P00J02_A1780AutorizacaoConsumo_Ativo[0];
            A1787AutorizacaoConsumo_Status = P00J02_A1787AutorizacaoConsumo_Status[0];
            A1782AutorizacaoConsumo_Valor = P00J02_A1782AutorizacaoConsumo_Valor[0];
            A1779AutorizacaoConsumo_Quantidade = P00J02_A1779AutorizacaoConsumo_Quantidade[0];
            A1778AutorizacaoConsumo_UnidadeMedicaoNom = P00J02_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            n1778AutorizacaoConsumo_UnidadeMedicaoNom = P00J02_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            A1776AutorizacaoConsumo_VigenciaFim = P00J02_A1776AutorizacaoConsumo_VigenciaFim[0];
            A1775AutorizacaoConsumo_VigenciaInicio = P00J02_A1775AutorizacaoConsumo_VigenciaInicio[0];
            A1774AutorizacaoConsumo_Codigo = P00J02_A1774AutorizacaoConsumo_Codigo[0];
            A1778AutorizacaoConsumo_UnidadeMedicaoNom = P00J02_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            n1778AutorizacaoConsumo_UnidadeMedicaoNom = P00J02_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            AV37count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00J02_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( P00J02_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0] == A1777AutorizacaoConsumo_UnidadeMedicaoCod ) )
            {
               BRKJ02 = false;
               A1774AutorizacaoConsumo_Codigo = P00J02_A1774AutorizacaoConsumo_Codigo[0];
               AV37count = (long)(AV37count+1);
               BRKJ02 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1778AutorizacaoConsumo_UnidadeMedicaoNom)) )
            {
               AV29Option = A1778AutorizacaoConsumo_UnidadeMedicaoNom;
               AV28InsertIndex = 1;
               while ( ( AV28InsertIndex <= AV30Options.Count ) && ( StringUtil.StrCmp(((String)AV30Options.Item(AV28InsertIndex)), AV29Option) < 0 ) )
               {
                  AV28InsertIndex = (int)(AV28InsertIndex+1);
               }
               AV30Options.Add(AV29Option, AV28InsertIndex);
               AV35OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV37count), "Z,ZZZ,ZZZ,ZZ9")), AV28InsertIndex);
            }
            if ( AV30Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJ02 )
            {
               BRKJ02 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV30Options = new GxSimpleCollection();
         AV33OptionsDesc = new GxSimpleCollection();
         AV35OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV38Session = context.GetSession();
         AV40GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV41GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFAutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         AV13TFAutorizacaoConsumo_VigenciaInicio_To = DateTime.MinValue;
         AV14TFAutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         AV15TFAutorizacaoConsumo_VigenciaFim_To = DateTime.MinValue;
         AV16TFAutorizacaoConsumo_UnidadeMedicaoNom = "";
         AV17TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "";
         AV22TFAutorizacaoConsumo_Status_SelsJson = "";
         AV23TFAutorizacaoConsumo_Status_Sels = new GxSimpleCollection();
         scmdbuf = "";
         lV16TFAutorizacaoConsumo_UnidadeMedicaoNom = "";
         A1787AutorizacaoConsumo_Status = "";
         A1775AutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         A1776AutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = "";
         P00J02_A74Contrato_Codigo = new int[1] ;
         P00J02_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         P00J02_A1780AutorizacaoConsumo_Ativo = new bool[] {false} ;
         P00J02_A1787AutorizacaoConsumo_Status = new String[] {""} ;
         P00J02_A1782AutorizacaoConsumo_Valor = new decimal[1] ;
         P00J02_A1779AutorizacaoConsumo_Quantidade = new short[1] ;
         P00J02_A1778AutorizacaoConsumo_UnidadeMedicaoNom = new String[] {""} ;
         P00J02_n1778AutorizacaoConsumo_UnidadeMedicaoNom = new bool[] {false} ;
         P00J02_A1776AutorizacaoConsumo_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         P00J02_A1775AutorizacaoConsumo_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00J02_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         AV29Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratoautorizacaoconsumowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00J02_A74Contrato_Codigo, P00J02_A1777AutorizacaoConsumo_UnidadeMedicaoCod, P00J02_A1780AutorizacaoConsumo_Ativo, P00J02_A1787AutorizacaoConsumo_Status, P00J02_A1782AutorizacaoConsumo_Valor, P00J02_A1779AutorizacaoConsumo_Quantidade, P00J02_A1778AutorizacaoConsumo_UnidadeMedicaoNom, P00J02_n1778AutorizacaoConsumo_UnidadeMedicaoNom, P00J02_A1776AutorizacaoConsumo_VigenciaFim, P00J02_A1775AutorizacaoConsumo_VigenciaInicio,
               P00J02_A1774AutorizacaoConsumo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFAutorizacaoConsumo_Quantidade ;
      private short AV19TFAutorizacaoConsumo_Quantidade_To ;
      private short AV24TFAutorizacaoConsumo_Ativo_Sel ;
      private short A1779AutorizacaoConsumo_Quantidade ;
      private int AV46GXV1 ;
      private int AV10TFAutorizacaoConsumo_Codigo ;
      private int AV11TFAutorizacaoConsumo_Codigo_To ;
      private int AV43Contrato_Codigo ;
      private int AV23TFAutorizacaoConsumo_Status_Sels_Count ;
      private int A1774AutorizacaoConsumo_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int AV28InsertIndex ;
      private long AV37count ;
      private decimal AV20TFAutorizacaoConsumo_Valor ;
      private decimal AV21TFAutorizacaoConsumo_Valor_To ;
      private decimal A1782AutorizacaoConsumo_Valor ;
      private String AV16TFAutorizacaoConsumo_UnidadeMedicaoNom ;
      private String AV17TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ;
      private String scmdbuf ;
      private String lV16TFAutorizacaoConsumo_UnidadeMedicaoNom ;
      private String A1787AutorizacaoConsumo_Status ;
      private String A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private DateTime AV12TFAutorizacaoConsumo_VigenciaInicio ;
      private DateTime AV13TFAutorizacaoConsumo_VigenciaInicio_To ;
      private DateTime AV14TFAutorizacaoConsumo_VigenciaFim ;
      private DateTime AV15TFAutorizacaoConsumo_VigenciaFim_To ;
      private DateTime A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime A1776AutorizacaoConsumo_VigenciaFim ;
      private bool returnInSub ;
      private bool A1780AutorizacaoConsumo_Ativo ;
      private bool BRKJ02 ;
      private bool n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String AV36OptionIndexesJson ;
      private String AV31OptionsJson ;
      private String AV34OptionsDescJson ;
      private String AV22TFAutorizacaoConsumo_Status_SelsJson ;
      private String AV27DDOName ;
      private String AV25SearchTxt ;
      private String AV26SearchTxtTo ;
      private String AV29Option ;
      private IGxSession AV38Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00J02_A74Contrato_Codigo ;
      private int[] P00J02_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private bool[] P00J02_A1780AutorizacaoConsumo_Ativo ;
      private String[] P00J02_A1787AutorizacaoConsumo_Status ;
      private decimal[] P00J02_A1782AutorizacaoConsumo_Valor ;
      private short[] P00J02_A1779AutorizacaoConsumo_Quantidade ;
      private String[] P00J02_A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool[] P00J02_n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private DateTime[] P00J02_A1776AutorizacaoConsumo_VigenciaFim ;
      private DateTime[] P00J02_A1775AutorizacaoConsumo_VigenciaInicio ;
      private int[] P00J02_A1774AutorizacaoConsumo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23TFAutorizacaoConsumo_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV40GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV41GridStateFilterValue ;
   }

   public class getcontratoautorizacaoconsumowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00J02( IGxContext context ,
                                             String A1787AutorizacaoConsumo_Status ,
                                             IGxCollection AV23TFAutorizacaoConsumo_Status_Sels ,
                                             int AV10TFAutorizacaoConsumo_Codigo ,
                                             int AV11TFAutorizacaoConsumo_Codigo_To ,
                                             DateTime AV12TFAutorizacaoConsumo_VigenciaInicio ,
                                             DateTime AV13TFAutorizacaoConsumo_VigenciaInicio_To ,
                                             DateTime AV14TFAutorizacaoConsumo_VigenciaFim ,
                                             DateTime AV15TFAutorizacaoConsumo_VigenciaFim_To ,
                                             String AV17TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ,
                                             String AV16TFAutorizacaoConsumo_UnidadeMedicaoNom ,
                                             short AV18TFAutorizacaoConsumo_Quantidade ,
                                             short AV19TFAutorizacaoConsumo_Quantidade_To ,
                                             decimal AV20TFAutorizacaoConsumo_Valor ,
                                             decimal AV21TFAutorizacaoConsumo_Valor_To ,
                                             int AV23TFAutorizacaoConsumo_Status_Sels_Count ,
                                             short AV24TFAutorizacaoConsumo_Ativo_Sel ,
                                             int A1774AutorizacaoConsumo_Codigo ,
                                             DateTime A1775AutorizacaoConsumo_VigenciaInicio ,
                                             DateTime A1776AutorizacaoConsumo_VigenciaFim ,
                                             String A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                             short A1779AutorizacaoConsumo_Quantidade ,
                                             decimal A1782AutorizacaoConsumo_Valor ,
                                             bool A1780AutorizacaoConsumo_Ativo ,
                                             int AV43Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_UnidadeMedicaoCod] AS AutorizacaoConsumo_UnidadeMedicaoCod, T1.[AutorizacaoConsumo_Ativo], T1.[AutorizacaoConsumo_Status], T1.[AutorizacaoConsumo_Valor], T1.[AutorizacaoConsumo_Quantidade], T2.[UnidadeMedicao_Nome] AS AutorizacaoConsumo_UnidadeMedicaoNom, T1.[AutorizacaoConsumo_VigenciaFim], T1.[AutorizacaoConsumo_VigenciaInicio], T1.[AutorizacaoConsumo_Codigo] FROM ([AutorizacaoConsumo] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[AutorizacaoConsumo_UnidadeMedicaoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Codigo] = @AV43Contrato_Codigo)";
         if ( ! (0==AV10TFAutorizacaoConsumo_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] >= @AV10TFAutorizacaoConsumo_Codigo)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (0==AV11TFAutorizacaoConsumo_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] <= @AV11TFAutorizacaoConsumo_Codigo_To)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFAutorizacaoConsumo_VigenciaInicio) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV12TFAutorizacaoConsumo_VigenciaInicio)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFAutorizacaoConsumo_VigenciaInicio_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV13TFAutorizacaoConsumo_VigenciaInicio_To)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFAutorizacaoConsumo_VigenciaFim) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] >= @AV14TFAutorizacaoConsumo_VigenciaFim)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFAutorizacaoConsumo_VigenciaFim_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] <= @AV15TFAutorizacaoConsumo_VigenciaFim_To)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFAutorizacaoConsumo_UnidadeMedicaoNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV16TFAutorizacaoConsumo_UnidadeMedicaoNom)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV17TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV18TFAutorizacaoConsumo_Quantidade) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] >= @AV18TFAutorizacaoConsumo_Quantidade)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (0==AV19TFAutorizacaoConsumo_Quantidade_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] <= @AV19TFAutorizacaoConsumo_Quantidade_To)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV20TFAutorizacaoConsumo_Valor) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Valor] >= @AV20TFAutorizacaoConsumo_Valor)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFAutorizacaoConsumo_Valor_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Valor] <= @AV21TFAutorizacaoConsumo_Valor_To)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV23TFAutorizacaoConsumo_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV23TFAutorizacaoConsumo_Status_Sels, "T1.[AutorizacaoConsumo_Status] IN (", ")") + ")";
         }
         if ( AV24TFAutorizacaoConsumo_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Ativo] = 1)";
         }
         if ( AV24TFAutorizacaoConsumo_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_UnidadeMedicaoCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00J02(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (short)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (int)dynConstraints[14] , (short)dynConstraints[15] , (int)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (decimal)dynConstraints[21] , (bool)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00J02 ;
          prmP00J02 = new Object[] {
          new Object[] {"@AV43Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFAutorizacaoConsumo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFAutorizacaoConsumo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFAutorizacaoConsumo_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV13TFAutorizacaoConsumo_VigenciaInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV14TFAutorizacaoConsumo_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFAutorizacaoConsumo_VigenciaFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFAutorizacaoConsumo_UnidadeMedicaoNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV17TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV18TFAutorizacaoConsumo_Quantidade",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV19TFAutorizacaoConsumo_Quantidade_To",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV20TFAutorizacaoConsumo_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21TFAutorizacaoConsumo_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00J02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00J02,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratoautorizacaoconsumowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratoautorizacaoconsumowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratoautorizacaoconsumowcfilterdata") )
          {
             return  ;
          }
          getcontratoautorizacaoconsumowcfilterdata worker = new getcontratoautorizacaoconsumowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
