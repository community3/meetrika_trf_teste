/*
               File: PRC_GetContratoServico
        Description: Get Contrato Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:3:8.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getcontratoservico : GXProcedure
   {
      public prc_getcontratoservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getcontratoservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           out int aP1_ServicoGrupo_Codigo ,
                           out int aP2_Servico_Codigo ,
                           out int aP3_SubServico_Codigo ,
                           out bool aP4_TemSubServicos )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV10ServicoGrupo_Codigo = 0 ;
         this.AV11Servico_Codigo = 0 ;
         this.AV9SubServico_Codigo = 0 ;
         this.AV12TemSubServicos = false ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_ServicoGrupo_Codigo=this.AV10ServicoGrupo_Codigo;
         aP2_Servico_Codigo=this.AV11Servico_Codigo;
         aP3_SubServico_Codigo=this.AV9SubServico_Codigo;
         aP4_TemSubServicos=this.AV12TemSubServicos;
      }

      public bool executeUdp( ref int aP0_ContratoServicos_Codigo ,
                              out int aP1_ServicoGrupo_Codigo ,
                              out int aP2_Servico_Codigo ,
                              out int aP3_SubServico_Codigo )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV10ServicoGrupo_Codigo = 0 ;
         this.AV11Servico_Codigo = 0 ;
         this.AV9SubServico_Codigo = 0 ;
         this.AV12TemSubServicos = false ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_ServicoGrupo_Codigo=this.AV10ServicoGrupo_Codigo;
         aP2_Servico_Codigo=this.AV11Servico_Codigo;
         aP3_SubServico_Codigo=this.AV9SubServico_Codigo;
         aP4_TemSubServicos=this.AV12TemSubServicos;
         return AV12TemSubServicos ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 out int aP1_ServicoGrupo_Codigo ,
                                 out int aP2_Servico_Codigo ,
                                 out int aP3_SubServico_Codigo ,
                                 out bool aP4_TemSubServicos )
      {
         prc_getcontratoservico objprc_getcontratoservico;
         objprc_getcontratoservico = new prc_getcontratoservico();
         objprc_getcontratoservico.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_getcontratoservico.AV10ServicoGrupo_Codigo = 0 ;
         objprc_getcontratoservico.AV11Servico_Codigo = 0 ;
         objprc_getcontratoservico.AV9SubServico_Codigo = 0 ;
         objprc_getcontratoservico.AV12TemSubServicos = false ;
         objprc_getcontratoservico.context.SetSubmitInitialConfig(context);
         objprc_getcontratoservico.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getcontratoservico);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_ServicoGrupo_Codigo=this.AV10ServicoGrupo_Codigo;
         aP2_Servico_Codigo=this.AV11Servico_Codigo;
         aP3_SubServico_Codigo=this.AV9SubServico_Codigo;
         aP4_TemSubServicos=this.AV12TemSubServicos;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getcontratoservico)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P004Q2 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A631Servico_Vinculado = P004Q2_A631Servico_Vinculado[0];
            n631Servico_Vinculado = P004Q2_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = P004Q2_A157ServicoGrupo_Codigo[0];
            A155Servico_Codigo = P004Q2_A155Servico_Codigo[0];
            A631Servico_Vinculado = P004Q2_A631Servico_Vinculado[0];
            n631Servico_Vinculado = P004Q2_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = P004Q2_A157ServicoGrupo_Codigo[0];
            GXt_int1 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int1) ;
            A640Servico_Vinculados = GXt_int1;
            if ( (0==A631Servico_Vinculado) )
            {
               AV11Servico_Codigo = A155Servico_Codigo;
               AV10ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            }
            else
            {
               AV12TemSubServicos = true;
               AV11Servico_Codigo = A631Servico_Vinculado;
               AV9SubServico_Codigo = A155Servico_Codigo;
               /* Execute user subroutine: 'GRUPODOSRVPAI' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
            }
            if ( A640Servico_Vinculados > 0 )
            {
               AV12TemSubServicos = true;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'GRUPODOSRVPAI' Routine */
         /* Using cursor P004Q3 */
         pr_default.execute(1, new Object[] {AV11Servico_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A155Servico_Codigo = P004Q3_A155Servico_Codigo[0];
            A157ServicoGrupo_Codigo = P004Q3_A157ServicoGrupo_Codigo[0];
            AV10ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P004Q2_A160ContratoServicos_Codigo = new int[1] ;
         P004Q2_A631Servico_Vinculado = new int[1] ;
         P004Q2_n631Servico_Vinculado = new bool[] {false} ;
         P004Q2_A157ServicoGrupo_Codigo = new int[1] ;
         P004Q2_A155Servico_Codigo = new int[1] ;
         P004Q3_A155Servico_Codigo = new int[1] ;
         P004Q3_A157ServicoGrupo_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getcontratoservico__default(),
            new Object[][] {
                new Object[] {
               P004Q2_A160ContratoServicos_Codigo, P004Q2_A631Servico_Vinculado, P004Q2_n631Servico_Vinculado, P004Q2_A157ServicoGrupo_Codigo, P004Q2_A155Servico_Codigo
               }
               , new Object[] {
               P004Q3_A155Servico_Codigo, P004Q3_A157ServicoGrupo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A640Servico_Vinculados ;
      private short GXt_int1 ;
      private int A160ContratoServicos_Codigo ;
      private int A631Servico_Vinculado ;
      private int A157ServicoGrupo_Codigo ;
      private int A155Servico_Codigo ;
      private int AV11Servico_Codigo ;
      private int AV10ServicoGrupo_Codigo ;
      private int AV9SubServico_Codigo ;
      private String scmdbuf ;
      private bool AV12TemSubServicos ;
      private bool n631Servico_Vinculado ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P004Q2_A160ContratoServicos_Codigo ;
      private int[] P004Q2_A631Servico_Vinculado ;
      private bool[] P004Q2_n631Servico_Vinculado ;
      private int[] P004Q2_A157ServicoGrupo_Codigo ;
      private int[] P004Q2_A155Servico_Codigo ;
      private int[] P004Q3_A155Servico_Codigo ;
      private int[] P004Q3_A157ServicoGrupo_Codigo ;
      private int aP1_ServicoGrupo_Codigo ;
      private int aP2_Servico_Codigo ;
      private int aP3_SubServico_Codigo ;
      private bool aP4_TemSubServicos ;
   }

   public class prc_getcontratoservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004Q2 ;
          prmP004Q2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004Q3 ;
          prmP004Q3 = new Object[] {
          new Object[] {"@AV11Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004Q2", "SELECT TOP 1 T1.[ContratoServicos_Codigo], T2.[Servico_Vinculado], T2.[ServicoGrupo_Codigo], T1.[Servico_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004Q2,1,0,true,true )
             ,new CursorDef("P004Q3", "SELECT [Servico_Codigo], [ServicoGrupo_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV11Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004Q3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
