/*
               File: ContratanteContratanteUsuarioWC
        Description: Contratante Contratante Usuario WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:29:24.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratantecontratanteusuariowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratantecontratanteusuariowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratantecontratanteusuariowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratanteUsuario_ContratanteCod )
      {
         this.AV31ContratanteUsuario_ContratanteCod = aP0_ContratanteUsuario_ContratanteCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         chkContratanteUsuario_EhFiscal = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV31ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContratanteUsuario_ContratanteCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV31ContratanteUsuario_ContratanteCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_40 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_40_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_40_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV32OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV15DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
                  AV17ContratanteUsuario_UsuarioPessoaNom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratanteUsuario_UsuarioPessoaNom1", AV17ContratanteUsuario_UsuarioPessoaNom1);
                  AV31ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContratanteUsuario_ContratanteCod), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  AV79Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A60ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A62ContratanteUsuario_UsuarioPessoaNom = GetNextPar( );
                  n62ContratanteUsuario_UsuarioPessoaNom = false;
                  A54Usuario_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  n54Usuario_Ativo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A54Usuario_Ativo", A54Usuario_Ativo);
                  A1908Usuario_DeFerias = (bool)(BooleanUtil.Val(GetNextPar( )));
                  n1908Usuario_DeFerias = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1908Usuario_DeFerias", A1908Usuario_DeFerias);
                  A341Usuario_UserGamGuid = GetNextPar( );
                  n341Usuario_UserGamGuid = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratanteUsuario_UsuarioPessoaNom1, AV31ContratanteUsuario_ContratanteCod, AV6WWPContext, AV79Pgmname, AV11GridState, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A62ContratanteUsuario_UsuarioPessoaNom, A54Usuario_Ativo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA892( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV79Pgmname = "ContratanteContratanteUsuarioWC";
               context.Gx_err = 0;
               edtavGamemail_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGamemail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGamemail_Enabled), 5, 0)));
               edtavNotificar_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotificar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotificar_Enabled), 5, 0)));
               WS892( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contratante Contratante Usuario WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202053021292591");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratantecontratanteusuariowc.aspx") + "?" + UrlEncode("" +AV31ContratanteUsuario_ContratanteCod)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATANTEUSUARIO_USUARIOPESSOANOM1", StringUtil.RTrim( AV17ContratanteUsuario_UsuarioPessoaNom1));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_40", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_40), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV31ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV31ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV79Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"USUARIO_ATIVO", A54Usuario_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"USUARIO_DEFERIAS", A1908Usuario_DeFerias);
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_USERGAMGUID", StringUtil.RTrim( A341Usuario_UserGamGuid));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm892( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratantecontratanteusuariowc.js", "?20205302129264");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratanteContratanteUsuarioWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratante Contratante Usuario WC" ;
      }

      protected void WB890( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratantecontratanteusuariowc.aspx");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_892( true) ;
         }
         else
         {
            wb_table1_2_892( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_892e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
         }
         wbLoad = true;
      }

      protected void START892( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contratante Contratante Usuario WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP890( ) ;
            }
         }
      }

      protected void WS892( )
      {
         START892( ) ;
         EVT892( ) ;
      }

      protected void EVT892( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP890( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP890( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11892 */
                                    E11892 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP890( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12892 */
                                    E12892 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOIMPORTAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP890( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13892 */
                                    E13892 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP890( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14892 */
                                    E14892 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP890( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavGamemail_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP890( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "USUARIO_NOME.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VATUALIZAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VFERIAS.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VATUALIZAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "USUARIO_NOME.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VFERIAS.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP890( ) ;
                              }
                              nGXsfl_40_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_40_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_40_idx), 4, 0)), 4, "0");
                              SubsflControlProps_402( ) ;
                              AV62Atualizar = cgiGet( edtavAtualizar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAtualizar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV62Atualizar)) ? AV72Atualizar_GXI : context.convertURL( context.PathToRelativeUrl( AV62Atualizar))));
                              AV28Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)) ? AV73Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV28Delete))));
                              A63ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( edtContratanteUsuario_ContratanteCod_Internalname), ",", "."));
                              A60ContratanteUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContratanteUsuario_UsuarioCod_Internalname), ",", "."));
                              A61ContratanteUsuario_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratanteUsuario_UsuarioPessoaCod_Internalname), ",", "."));
                              n61ContratanteUsuario_UsuarioPessoaCod = false;
                              A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
                              n2Usuario_Nome = false;
                              A62ContratanteUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtContratanteUsuario_UsuarioPessoaNom_Internalname));
                              n62ContratanteUsuario_UsuarioPessoaNom = false;
                              A492ContratanteUsuario_UsuarioPessoaDoc = cgiGet( edtContratanteUsuario_UsuarioPessoaDoc_Internalname);
                              n492ContratanteUsuario_UsuarioPessoaDoc = false;
                              AV36GAMEMail = cgiGet( edtavGamemail_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavGamemail_Internalname, AV36GAMEMail);
                              AV68Notificar = cgiGet( edtavNotificar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNotificar_Internalname, AV68Notificar);
                              A1728ContratanteUsuario_EhFiscal = StringUtil.StrToBool( cgiGet( chkContratanteUsuario_EhFiscal_Internalname));
                              n1728ContratanteUsuario_EhFiscal = false;
                              AV59Ferias = cgiGet( edtavFerias_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV59Ferias)) ? AV74Ferias_GXI : context.convertURL( context.PathToRelativeUrl( AV59Ferias))));
                              AV33AssociarPerfil = cgiGet( edtavAssociarperfil_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarperfil_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33AssociarPerfil)) ? AV75Associarperfil_GXI : context.convertURL( context.PathToRelativeUrl( AV33AssociarPerfil))));
                              AV38AssociarSistema = cgiGet( edtavAssociarsistema_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarsistema_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV38AssociarSistema)) ? AV76Associarsistema_GXI : context.convertURL( context.PathToRelativeUrl( AV38AssociarSistema))));
                              AV69Config = cgiGet( edtavConfig_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavConfig_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV69Config)) ? AV77Config_GXI : context.convertURL( context.PathToRelativeUrl( AV69Config))));
                              AV64ClonarDados = cgiGet( edtavClonardados_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavClonardados_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV64ClonarDados)) ? AV78Clonardados_GXI : context.convertURL( context.PathToRelativeUrl( AV64ClonarDados))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15892 */
                                          E15892 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16892 */
                                          E16892 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17892 */
                                          E17892 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "USUARIO_NOME.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18892 */
                                          E18892 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VATUALIZAR.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19892 */
                                          E19892 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VFERIAS.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E20892 */
                                          E20892 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV32OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratanteusuario_usuariopessoanom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTEUSUARIO_USUARIOPESSOANOM1"), AV17ContratanteUsuario_UsuarioPessoaNom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP890( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGamemail_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE892( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm892( ) ;
            }
         }
      }

      protected void PA892( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV32OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATANTEUSUARIO_USUARIOPESSOANOM", "Pessoa", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            GXCCtl = "CONTRATANTEUSUARIO_EHFISCAL_" + sGXsfl_40_idx;
            chkContratanteUsuario_EhFiscal.Name = GXCCtl;
            chkContratanteUsuario_EhFiscal.WebTags = "";
            chkContratanteUsuario_EhFiscal.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratanteUsuario_EhFiscal_Internalname, "TitleCaption", chkContratanteUsuario_EhFiscal.Caption);
            chkContratanteUsuario_EhFiscal.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_402( ) ;
         while ( nGXsfl_40_idx <= nRC_GXsfl_40 )
         {
            sendrow_402( ) ;
            nGXsfl_40_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_40_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_40_idx+1));
            sGXsfl_40_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_40_idx), 4, 0)), 4, "0");
            SubsflControlProps_402( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV32OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17ContratanteUsuario_UsuarioPessoaNom1 ,
                                       int AV31ContratanteUsuario_ContratanteCod ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV79Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       int A63ContratanteUsuario_ContratanteCod ,
                                       int A60ContratanteUsuario_UsuarioCod ,
                                       String A62ContratanteUsuario_UsuarioPessoaNom ,
                                       bool A54Usuario_Ativo ,
                                       bool A1908Usuario_DeFerias ,
                                       String A341Usuario_UserGamGuid ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF892( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTEUSUARIO_CONTRATANTECOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A63ContratanteUsuario_ContratanteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTEUSUARIO_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A60ContratanteUsuario_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTEUSUARIO_EHFISCAL", GetSecureSignedToken( sPrefix, A1728ContratanteUsuario_EhFiscal));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTEUSUARIO_EHFISCAL", StringUtil.BoolToStr( A1728ContratanteUsuario_EhFiscal));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV32OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF892( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV79Pgmname = "ContratanteContratanteUsuarioWC";
         context.Gx_err = 0;
         edtavGamemail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGamemail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGamemail_Enabled), 5, 0)));
         edtavNotificar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotificar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotificar_Enabled), 5, 0)));
      }

      protected void RF892( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 40;
         /* Execute user event: E16892 */
         E16892 ();
         nGXsfl_40_idx = 1;
         sGXsfl_40_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_40_idx), 4, 0)), 4, "0");
         SubsflControlProps_402( ) ;
         nGXsfl_40_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_402( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV17ContratanteUsuario_UsuarioPessoaNom1 ,
                                                 A62ContratanteUsuario_UsuarioPessoaNom ,
                                                 AV32OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A63ContratanteUsuario_ContratanteCod ,
                                                 AV31ContratanteUsuario_ContratanteCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV17ContratanteUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV17ContratanteUsuario_UsuarioPessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratanteUsuario_UsuarioPessoaNom1", AV17ContratanteUsuario_UsuarioPessoaNom1);
            /* Using cursor H00893 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV31ContratanteUsuario_ContratanteCod, lV17ContratanteUsuario_UsuarioPessoaNom1, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_40_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A54Usuario_Ativo = H00893_A54Usuario_Ativo[0];
               n54Usuario_Ativo = H00893_n54Usuario_Ativo[0];
               A1908Usuario_DeFerias = H00893_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H00893_n1908Usuario_DeFerias[0];
               A341Usuario_UserGamGuid = H00893_A341Usuario_UserGamGuid[0];
               n341Usuario_UserGamGuid = H00893_n341Usuario_UserGamGuid[0];
               A1728ContratanteUsuario_EhFiscal = H00893_A1728ContratanteUsuario_EhFiscal[0];
               n1728ContratanteUsuario_EhFiscal = H00893_n1728ContratanteUsuario_EhFiscal[0];
               A492ContratanteUsuario_UsuarioPessoaDoc = H00893_A492ContratanteUsuario_UsuarioPessoaDoc[0];
               n492ContratanteUsuario_UsuarioPessoaDoc = H00893_n492ContratanteUsuario_UsuarioPessoaDoc[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00893_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00893_n62ContratanteUsuario_UsuarioPessoaNom[0];
               A2Usuario_Nome = H00893_A2Usuario_Nome[0];
               n2Usuario_Nome = H00893_n2Usuario_Nome[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H00893_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00893_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A60ContratanteUsuario_UsuarioCod = H00893_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = H00893_A63ContratanteUsuario_ContratanteCod[0];
               A40000UsuarioNotifica_NoStatus = H00893_A40000UsuarioNotifica_NoStatus[0];
               n40000UsuarioNotifica_NoStatus = H00893_n40000UsuarioNotifica_NoStatus[0];
               A54Usuario_Ativo = H00893_A54Usuario_Ativo[0];
               n54Usuario_Ativo = H00893_n54Usuario_Ativo[0];
               A1908Usuario_DeFerias = H00893_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H00893_n1908Usuario_DeFerias[0];
               A341Usuario_UserGamGuid = H00893_A341Usuario_UserGamGuid[0];
               n341Usuario_UserGamGuid = H00893_n341Usuario_UserGamGuid[0];
               A2Usuario_Nome = H00893_A2Usuario_Nome[0];
               n2Usuario_Nome = H00893_n2Usuario_Nome[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H00893_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00893_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A492ContratanteUsuario_UsuarioPessoaDoc = H00893_A492ContratanteUsuario_UsuarioPessoaDoc[0];
               n492ContratanteUsuario_UsuarioPessoaDoc = H00893_n492ContratanteUsuario_UsuarioPessoaDoc[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00893_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00893_n62ContratanteUsuario_UsuarioPessoaNom[0];
               A40000UsuarioNotifica_NoStatus = H00893_A40000UsuarioNotifica_NoStatus[0];
               n40000UsuarioNotifica_NoStatus = H00893_n40000UsuarioNotifica_NoStatus[0];
               /* Execute user event: E17892 */
               E17892 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 40;
            WB890( ) ;
         }
         nGXsfl_40_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV17ContratanteUsuario_UsuarioPessoaNom1 ,
                                              A62ContratanteUsuario_UsuarioPessoaNom ,
                                              AV32OrderedBy ,
                                              AV14OrderedDsc ,
                                              A63ContratanteUsuario_ContratanteCod ,
                                              AV31ContratanteUsuario_ContratanteCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV17ContratanteUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV17ContratanteUsuario_UsuarioPessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratanteUsuario_UsuarioPessoaNom1", AV17ContratanteUsuario_UsuarioPessoaNom1);
         /* Using cursor H00895 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV31ContratanteUsuario_ContratanteCod, lV17ContratanteUsuario_UsuarioPessoaNom1});
         GRID_nRecordCount = H00895_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratanteUsuario_UsuarioPessoaNom1, AV31ContratanteUsuario_ContratanteCod, AV6WWPContext, AV79Pgmname, AV11GridState, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A62ContratanteUsuario_UsuarioPessoaNom, A54Usuario_Ativo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratanteUsuario_UsuarioPessoaNom1, AV31ContratanteUsuario_ContratanteCod, AV6WWPContext, AV79Pgmname, AV11GridState, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A62ContratanteUsuario_UsuarioPessoaNom, A54Usuario_Ativo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratanteUsuario_UsuarioPessoaNom1, AV31ContratanteUsuario_ContratanteCod, AV6WWPContext, AV79Pgmname, AV11GridState, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A62ContratanteUsuario_UsuarioPessoaNom, A54Usuario_Ativo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratanteUsuario_UsuarioPessoaNom1, AV31ContratanteUsuario_ContratanteCod, AV6WWPContext, AV79Pgmname, AV11GridState, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A62ContratanteUsuario_UsuarioPessoaNom, A54Usuario_Ativo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratanteUsuario_UsuarioPessoaNom1, AV31ContratanteUsuario_ContratanteCod, AV6WWPContext, AV79Pgmname, AV11GridState, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A62ContratanteUsuario_UsuarioPessoaNom, A54Usuario_Ativo, A1908Usuario_DeFerias, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP890( )
      {
         /* Before Start, stand alone formulas. */
         AV79Pgmname = "ContratanteContratanteUsuarioWC";
         context.Gx_err = 0;
         edtavGamemail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGamemail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGamemail_Enabled), 5, 0)));
         edtavNotificar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotificar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotificar_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E15892 */
         E15892 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV32OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17ContratanteUsuario_UsuarioPessoaNom1 = StringUtil.Upper( cgiGet( edtavContratanteusuario_usuariopessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratanteUsuario_UsuarioPessoaNom1", AV17ContratanteUsuario_UsuarioPessoaNom1);
            /* Read saved values. */
            nRC_GXsfl_40 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_40"), ",", "."));
            wcpOAV31ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV31ContratanteUsuario_ContratanteCod"), ",", "."));
            AV31ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"vCONTRATANTEUSUARIO_CONTRATANTECOD"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV32OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTEUSUARIO_USUARIOPESSOANOM1"), AV17ContratanteUsuario_UsuarioPessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E15892 */
         E15892 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15892( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 13/05/2020 23:50", 0) ;
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV15DynamicFiltersSelector1 = "CONTRATANTEUSUARIO_USUARIOPESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Pessoa", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "CPF", 0);
         cmbavOrderedby.addItem("4", "Fiscal", 0);
         if ( AV32OrderedBy < 1 )
         {
            AV32OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E16892( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUsuario_Nome_Titleformat = 2;
         edtUsuario_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV32OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUsuario_Nome_Internalname, "Title", edtUsuario_Nome_Title);
         edtContratanteUsuario_UsuarioPessoaNom_Titleformat = 2;
         edtContratanteUsuario_UsuarioPessoaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV32OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Pessoa", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratanteUsuario_UsuarioPessoaNom_Internalname, "Title", edtContratanteUsuario_UsuarioPessoaNom_Title);
         edtContratanteUsuario_UsuarioPessoaDoc_Titleformat = 2;
         edtContratanteUsuario_UsuarioPessoaDoc_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV32OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "CPF", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratanteUsuario_UsuarioPessoaDoc_Internalname, "Title", edtContratanteUsuario_UsuarioPessoaDoc_Title);
         chkContratanteUsuario_EhFiscal_Titleformat = 2;
         chkContratanteUsuario_EhFiscal.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV32OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Fiscal", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratanteUsuario_EhFiscal_Internalname, "Title", chkContratanteUsuario_EhFiscal.Title.Text);
         edtavAssociarperfil_Title = "Perfis";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarperfil_Internalname, "Title", edtavAssociarperfil_Title);
         edtavAssociarsistema_Title = "Sistemas";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarsistema_Internalname, "Title", edtavAssociarsistema_Title);
         edtavFerias_Title = "F�rias";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Title", edtavFerias_Title);
         edtavClonardados_Title = "Clonar";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavClonardados_Internalname, "Title", edtavClonardados_Title);
         edtavAssociarperfil_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarperfil_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAssociarperfil_Visible), 5, 0)));
         edtavClonardados_Visible = (AV6WWPContext.gxTpr_Userehcontratante||AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavClonardados_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClonardados_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      private void E17892( )
      {
         /* Grid_Load Routine */
         edtavAtualizar_Tooltiptext = "";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            AV62Atualizar = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtualizar_Internalname, AV62Atualizar);
            AV72Atualizar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavAtualizar_Enabled = 1;
         }
         else
         {
            AV62Atualizar = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAtualizar_Internalname, AV62Atualizar);
            AV72Atualizar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavAtualizar_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("contratanteusuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A63ContratanteUsuario_ContratanteCod) + "," + UrlEncode("" +A60ContratanteUsuario_UsuarioCod);
            AV28Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV28Delete);
            AV73Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV28Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV28Delete);
            AV73Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         AV59Ferias = context.GetImagePath( "f3aa001a-9bb4-4496-90e2-794d4244d8d1", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFerias_Internalname, AV59Ferias);
         AV74Ferias_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f3aa001a-9bb4-4496-90e2-794d4244d8d1", "", context.GetTheme( )));
         edtavFerias_Tooltiptext = "Clique aqui p/ marcar o usu�rio como ausente";
         AV33AssociarPerfil = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAssociarperfil_Internalname, AV33AssociarPerfil);
         AV75Associarperfil_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarperfil_Tooltiptext = "Associar os perfis deste usu�rio!";
         AV38AssociarSistema = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAssociarsistema_Internalname, AV38AssociarSistema);
         AV76Associarsistema_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarsistema_Tooltiptext = "Associar os sistemas deste usu�rio!";
         edtavConfig_Tooltiptext = "Permiss�es do usu�rio";
         if ( AV6WWPContext.gxTpr_Userehadministradorgam || AV6WWPContext.gxTpr_Userehgestor )
         {
            edtavConfig_Link = formatLink("wp_cfgperfil.aspx") + "?" + UrlEncode("" +A60ContratanteUsuario_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(A62ContratanteUsuario_UsuarioPessoaNom));
            AV69Config = context.GetImagePath( "70d5df5e-e723-4cca-9402-f0ce8679d27f", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavConfig_Internalname, AV69Config);
            AV77Config_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "70d5df5e-e723-4cca-9402-f0ce8679d27f", "", context.GetTheme( )));
            edtavConfig_Enabled = 1;
         }
         else
         {
            edtavConfig_Link = "";
            AV69Config = context.GetImagePath( "70d5df5e-e723-4cca-9402-f0ce8679d27f", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavConfig_Internalname, AV69Config);
            AV77Config_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "70d5df5e-e723-4cca-9402-f0ce8679d27f", "", context.GetTheme( )));
            edtavConfig_Enabled = 0;
         }
         edtavClonardados_Tooltiptext = "Clonar dados neste usu�rio!";
         if ( AV6WWPContext.gxTpr_Userehcontratante || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            AV64ClonarDados = context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavClonardados_Internalname, AV64ClonarDados);
            AV78Clonardados_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( )));
            edtavClonardados_Enabled = 1;
         }
         else
         {
            AV64ClonarDados = context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavClonardados_Internalname, AV64ClonarDados);
            AV78Clonardados_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( )));
            edtavClonardados_Enabled = 0;
         }
         if ( A54Usuario_Ativo )
         {
            AV63Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV63Color = GXUtil.RGB( 255, 0, 0);
         }
         edtUsuario_Nome_Forecolor = (int)(AV63Color);
         edtContratanteUsuario_UsuarioPessoaNom_Forecolor = (int)(AV63Color);
         edtContratanteUsuario_UsuarioPessoaDoc_Forecolor = (int)(AV63Color);
         edtavGamemail_Forecolor = (int)(AV63Color);
         edtavNotificar_Forecolor = (int)(AV63Color);
         edtavFerias_Visible = (AV6WWPContext.gxTpr_Userehgestor||(AV6WWPContext.gxTpr_Userid==A60ContratanteUsuario_UsuarioCod) ? 1 : 0);
         if ( A1908Usuario_DeFerias )
         {
            AV59Ferias = context.GetImagePath( "4474bccb-05fc-489f-ad8d-570ee718e701", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFerias_Internalname, AV59Ferias);
            AV74Ferias_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "4474bccb-05fc-489f-ad8d-570ee718e701", "", context.GetTheme( )));
            edtavFerias_Tooltiptext = "Marcar o retorno do usu�rio �s atividades";
         }
         AV37GamUser.load( A341Usuario_UserGamGuid);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
         AV36GAMEMail = AV37GamUser.gxTpr_Email;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavGamemail_Internalname, AV36GAMEMail);
         GXt_char1 = AV68Notificar;
         new prc_listadestatus(context ).execute(  A40000UsuarioNotifica_NoStatus, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A40000UsuarioNotifica_NoStatus", A40000UsuarioNotifica_NoStatus);
         AV68Notificar = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNotificar_Internalname, AV68Notificar);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 40;
         }
         sendrow_402( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_40_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(40, GridRow);
         }
      }

      protected void E11892( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E14892( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12892( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("wp_novousuario.aspx") + "?" + UrlEncode("" +AV31ContratanteUsuario_ContratanteCod) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void E13892( )
      {
         /* 'DoImportar' Routine */
         context.wjLoc = formatLink("wp_importarusuarios.aspx") + "?" + UrlEncode("" +AV31ContratanteUsuario_ContratanteCod) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S142( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtnimportar_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnimportar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnimportar_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratanteusuario_usuariopessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratanteusuario_usuariopessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratanteusuario_usuariopessoanom1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            edtavContratanteusuario_usuariopessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratanteusuario_usuariopessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratanteusuario_usuariopessoanom1_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV79Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV79Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV30Session.Get(AV79Pgmname+"GridState"), "");
         }
         AV32OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV17ContratanteUsuario_UsuarioPessoaNom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratanteUsuario_UsuarioPessoaNom1", AV17ContratanteUsuario_UsuarioPessoaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV30Session.Get(AV79Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV32OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV79Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV13GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratanteUsuario_UsuarioPessoaNom1)) )
         {
            AV13GridStateDynamicFilter.gxTpr_Value = AV17ContratanteUsuario_UsuarioPessoaNom1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
         }
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV79Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratanteUsuario";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratanteUsuario_ContratanteCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV31ContratanteUsuario_ContratanteCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV30Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E18892( )
      {
         /* Usuario_Nome_Click Routine */
         AV34Websession.Set("Entidade", "E"+StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
         context.wjLoc = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A60ContratanteUsuario_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
      }

      protected void E19892( )
      {
         /* Atualizar_Click Routine */
         AV34Websession.Set("Entidade", "E"+StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
         context.wjLoc = formatLink("usuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A60ContratanteUsuario_UsuarioCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E20892( )
      {
         /* Ferias_Click Routine */
         AV61Usuario.Load(A60ContratanteUsuario_UsuarioCod);
         AV65AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV65AuditingObject.gxTpr_Mode = "UPD";
         AV66AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV66AuditingObjectRecordItem.gxTpr_Tablename = "Usuario";
         AV66AuditingObjectRecordItem.gxTpr_Mode = "UPD";
         AV65AuditingObject.gxTpr_Record.Add(AV66AuditingObjectRecordItem, 0);
         AV67AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         AV67AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Usuario_DeFerias";
         AV67AuditingObjectRecordItemAttributeItem.gxTpr_Description = "De f�rias";
         AV67AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
         AV67AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
         AV67AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( AV61Usuario.gxTpr_Usuario_deferias);
         if ( AV61Usuario.gxTpr_Usuario_deferias )
         {
            AV59Ferias = context.GetImagePath( "f3aa001a-9bb4-4496-90e2-794d4244d8d1", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV59Ferias)) ? AV74Ferias_GXI : context.convertURL( context.PathToRelativeUrl( AV59Ferias))));
            AV74Ferias_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f3aa001a-9bb4-4496-90e2-794d4244d8d1", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV59Ferias)) ? AV74Ferias_GXI : context.convertURL( context.PathToRelativeUrl( AV59Ferias))));
            edtavFerias_Tooltiptext = "Marcar o usu�rio como afastado das atividades";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Tooltiptext", edtavFerias_Tooltiptext);
            AV61Usuario.gxTpr_Usuario_deferias = false;
         }
         else
         {
            AV59Ferias = context.GetImagePath( "4474bccb-05fc-489f-ad8d-570ee718e701", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV59Ferias)) ? AV74Ferias_GXI : context.convertURL( context.PathToRelativeUrl( AV59Ferias))));
            AV74Ferias_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "4474bccb-05fc-489f-ad8d-570ee718e701", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV59Ferias)) ? AV74Ferias_GXI : context.convertURL( context.PathToRelativeUrl( AV59Ferias))));
            edtavFerias_Tooltiptext = "Marcar o retorno do usu�rio �s atividades";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFerias_Internalname, "Tooltiptext", edtavFerias_Tooltiptext);
            AV61Usuario.gxTpr_Usuario_deferias = true;
         }
         AV67AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( AV61Usuario.gxTpr_Usuario_deferias);
         AV66AuditingObjectRecordItem.gxTpr_Attribute.Add(AV67AuditingObjectRecordItemAttributeItem, 0);
         AV61Usuario.Save();
         context.CommitDataStores( "ContratanteContratanteUsuarioWC");
         new wwpbaseobjects.audittransaction(context ).execute(  AV65AuditingObject,  AV79Pgmname) ;
      }

      protected void wb_table1_2_892( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_892( true) ;
         }
         else
         {
            wb_table2_8_892( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_892e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_37_892( true) ;
         }
         else
         {
            wb_table3_37_892( false) ;
         }
         return  ;
      }

      protected void wb_table3_37_892e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_892e( true) ;
         }
         else
         {
            wb_table1_2_892e( false) ;
         }
      }

      protected void wb_table3_37_892( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedgrid_Internalname, tblTablemergedgrid_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"40\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contratante Usuario_Contratante Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratanteUsuario_UsuarioPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratanteUsuario_UsuarioPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratanteUsuario_UsuarioPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratanteUsuario_UsuarioPessoaDoc_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratanteUsuario_UsuarioPessoaDoc_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratanteUsuario_UsuarioPessoaDoc_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "E-Mail") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Notificar") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContratanteUsuario_EhFiscal_Titleformat == 0 )
               {
                  context.SendWebValue( chkContratanteUsuario_EhFiscal.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContratanteUsuario_EhFiscal.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavFerias_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavFerias_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavAssociarperfil_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarperfil_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarsistema_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavClonardados_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavClonardados_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV62Atualizar));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAtualizar_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAtualizar_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2Usuario_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_Nome_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A62ContratanteUsuario_UsuarioPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratanteUsuario_UsuarioPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratanteUsuario_UsuarioPessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratanteUsuario_UsuarioPessoaNom_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A492ContratanteUsuario_UsuarioPessoaDoc);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratanteUsuario_UsuarioPessoaDoc_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratanteUsuario_UsuarioPessoaDoc_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratanteUsuario_UsuarioPessoaDoc_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV36GAMEMail);
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavGamemail_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavGamemail_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV68Notificar));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNotificar_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNotificar_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1728ContratanteUsuario_EhFiscal));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContratanteUsuario_EhFiscal.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContratanteUsuario_EhFiscal_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV59Ferias));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavFerias_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavFerias_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFerias_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33AssociarPerfil));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarperfil_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarperfil_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAssociarperfil_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV38AssociarSistema));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarsistema_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarsistema_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV69Config));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavConfig_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavConfig_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavConfig_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV64ClonarDados));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavClonardados_Title));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavClonardados_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavClonardados_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavClonardados_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 40 )
         {
            wbEnd = 0;
            nRC_GXsfl_40 = (short)(nGXsfl_40_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratanteContratanteUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_37_892e( true) ;
         }
         else
         {
            wb_table3_37_892e( false) ;
         }
      }

      protected void wb_table2_8_892( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_892( true) ;
         }
         else
         {
            wb_table4_11_892( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_892e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ContratanteContratanteUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_40_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_ContratanteContratanteUsuarioWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_40_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratanteContratanteUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_892( true) ;
         }
         else
         {
            wb_table5_21_892( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_892e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_892e( true) ;
         }
         else
         {
            wb_table2_8_892e( false) ;
         }
      }

      protected void wb_table5_21_892( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_24_892( true) ;
         }
         else
         {
            wb_table6_24_892( false) ;
         }
         return  ;
      }

      protected void wb_table6_24_892e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_892e( true) ;
         }
         else
         {
            wb_table5_21_892e( false) ;
         }
      }

      protected void wb_table6_24_892( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratanteContratanteUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_40_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_ContratanteContratanteUsuarioWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratanteContratanteUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_40_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratanteusuario_usuariopessoanom1_Internalname, StringUtil.RTrim( AV17ContratanteUsuario_UsuarioPessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV17ContratanteUsuario_UsuarioPessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratanteusuario_usuariopessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratanteusuario_usuariopessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratanteContratanteUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_24_892e( true) ;
         }
         else
         {
            wb_table6_24_892e( false) ;
         }
      }

      protected void wb_table4_11_892( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnimportar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(40), 2, 0)+","+"null"+");", "Importar", bttBtnimportar_Jsonclick, 5, "Importar Usu�rios de outra �rea de Trabalho", "", StyleString, ClassString, 1, bttBtnimportar_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOIMPORTAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratanteContratanteUsuarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_892e( true) ;
         }
         else
         {
            wb_table4_11_892e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV31ContratanteUsuario_ContratanteCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContratanteUsuario_ContratanteCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA892( ) ;
         WS892( ) ;
         WE892( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV31ContratanteUsuario_ContratanteCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA892( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratantecontratanteusuariowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA892( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV31ContratanteUsuario_ContratanteCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContratanteUsuario_ContratanteCod), 6, 0)));
         }
         wcpOAV31ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV31ContratanteUsuario_ContratanteCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV31ContratanteUsuario_ContratanteCod != wcpOAV31ContratanteUsuario_ContratanteCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV31ContratanteUsuario_ContratanteCod = AV31ContratanteUsuario_ContratanteCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV31ContratanteUsuario_ContratanteCod = cgiGet( sPrefix+"AV31ContratanteUsuario_ContratanteCod_CTRL");
         if ( StringUtil.Len( sCtrlAV31ContratanteUsuario_ContratanteCod) > 0 )
         {
            AV31ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV31ContratanteUsuario_ContratanteCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContratanteUsuario_ContratanteCod), 6, 0)));
         }
         else
         {
            AV31ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV31ContratanteUsuario_ContratanteCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA892( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS892( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS892( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV31ContratanteUsuario_ContratanteCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV31ContratanteUsuario_ContratanteCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV31ContratanteUsuario_ContratanteCod_CTRL", StringUtil.RTrim( sCtrlAV31ContratanteUsuario_ContratanteCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE892( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202053021293042");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("contratantecontratanteusuariowc.js", "?202053021293043");
            context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_402( )
      {
         edtavAtualizar_Internalname = sPrefix+"vATUALIZAR_"+sGXsfl_40_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_40_idx;
         edtContratanteUsuario_ContratanteCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_CONTRATANTECOD_"+sGXsfl_40_idx;
         edtContratanteUsuario_UsuarioCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOCOD_"+sGXsfl_40_idx;
         edtContratanteUsuario_UsuarioPessoaCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOPESSOACOD_"+sGXsfl_40_idx;
         edtUsuario_Nome_Internalname = sPrefix+"USUARIO_NOME_"+sGXsfl_40_idx;
         edtContratanteUsuario_UsuarioPessoaNom_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOPESSOANOM_"+sGXsfl_40_idx;
         edtContratanteUsuario_UsuarioPessoaDoc_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOPESSOADOC_"+sGXsfl_40_idx;
         edtavGamemail_Internalname = sPrefix+"vGAMEMAIL_"+sGXsfl_40_idx;
         edtavNotificar_Internalname = sPrefix+"vNOTIFICAR_"+sGXsfl_40_idx;
         chkContratanteUsuario_EhFiscal_Internalname = sPrefix+"CONTRATANTEUSUARIO_EHFISCAL_"+sGXsfl_40_idx;
         edtavFerias_Internalname = sPrefix+"vFERIAS_"+sGXsfl_40_idx;
         edtavAssociarperfil_Internalname = sPrefix+"vASSOCIARPERFIL_"+sGXsfl_40_idx;
         edtavAssociarsistema_Internalname = sPrefix+"vASSOCIARSISTEMA_"+sGXsfl_40_idx;
         edtavConfig_Internalname = sPrefix+"vCONFIG_"+sGXsfl_40_idx;
         edtavClonardados_Internalname = sPrefix+"vCLONARDADOS_"+sGXsfl_40_idx;
      }

      protected void SubsflControlProps_fel_402( )
      {
         edtavAtualizar_Internalname = sPrefix+"vATUALIZAR_"+sGXsfl_40_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_40_fel_idx;
         edtContratanteUsuario_ContratanteCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_CONTRATANTECOD_"+sGXsfl_40_fel_idx;
         edtContratanteUsuario_UsuarioCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOCOD_"+sGXsfl_40_fel_idx;
         edtContratanteUsuario_UsuarioPessoaCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOPESSOACOD_"+sGXsfl_40_fel_idx;
         edtUsuario_Nome_Internalname = sPrefix+"USUARIO_NOME_"+sGXsfl_40_fel_idx;
         edtContratanteUsuario_UsuarioPessoaNom_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOPESSOANOM_"+sGXsfl_40_fel_idx;
         edtContratanteUsuario_UsuarioPessoaDoc_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOPESSOADOC_"+sGXsfl_40_fel_idx;
         edtavGamemail_Internalname = sPrefix+"vGAMEMAIL_"+sGXsfl_40_fel_idx;
         edtavNotificar_Internalname = sPrefix+"vNOTIFICAR_"+sGXsfl_40_fel_idx;
         chkContratanteUsuario_EhFiscal_Internalname = sPrefix+"CONTRATANTEUSUARIO_EHFISCAL_"+sGXsfl_40_fel_idx;
         edtavFerias_Internalname = sPrefix+"vFERIAS_"+sGXsfl_40_fel_idx;
         edtavAssociarperfil_Internalname = sPrefix+"vASSOCIARPERFIL_"+sGXsfl_40_fel_idx;
         edtavAssociarsistema_Internalname = sPrefix+"vASSOCIARSISTEMA_"+sGXsfl_40_fel_idx;
         edtavConfig_Internalname = sPrefix+"vCONFIG_"+sGXsfl_40_fel_idx;
         edtavClonardados_Internalname = sPrefix+"vCLONARDADOS_"+sGXsfl_40_fel_idx;
      }

      protected void sendrow_402( )
      {
         SubsflControlProps_402( ) ;
         WB890( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_40_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_40_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_40_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAtualizar_Enabled!=0)&&(edtavAtualizar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 41,'"+sPrefix+"',false,'',40)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV62Atualizar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV62Atualizar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV72Atualizar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV62Atualizar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAtualizar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV62Atualizar)) ? AV72Atualizar_GXI : context.PathToRelativeUrl( AV62Atualizar)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavAtualizar_Enabled,(String)"",(String)edtavAtualizar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavAtualizar_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVATUALIZAR.CLICK."+sGXsfl_40_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV62Atualizar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV73Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)) ? AV73Delete_GXI : context.PathToRelativeUrl( AV28Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratanteUsuario_ContratanteCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A63ContratanteUsuario_ContratanteCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratanteUsuario_ContratanteCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)40,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratanteUsuario_UsuarioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A60ContratanteUsuario_UsuarioCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratanteUsuario_UsuarioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)40,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratanteUsuario_UsuarioPessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratanteUsuario_UsuarioPessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)40,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Nome_Internalname,StringUtil.RTrim( A2Usuario_Nome),StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")),(String)"","'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EUSUARIO_NOME.CLICK."+sGXsfl_40_idx+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Nome_Jsonclick,(short)5,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtUsuario_Nome_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)40,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratanteUsuario_UsuarioPessoaNom_Internalname,StringUtil.RTrim( A62ContratanteUsuario_UsuarioPessoaNom),StringUtil.RTrim( context.localUtil.Format( A62ContratanteUsuario_UsuarioPessoaNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratanteUsuario_UsuarioPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratanteUsuario_UsuarioPessoaNom_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)40,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratanteUsuario_UsuarioPessoaDoc_Internalname,(String)A492ContratanteUsuario_UsuarioPessoaDoc,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratanteUsuario_UsuarioPessoaDoc_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratanteUsuario_UsuarioPessoaDoc_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)40,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavGamemail_Enabled!=0)&&(edtavGamemail_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 49,'"+sPrefix+"',false,'"+sGXsfl_40_idx+"',40)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavGamemail_Internalname,(String)AV36GAMEMail,(String)"",TempTags+((edtavGamemail_Enabled!=0)&&(edtavGamemail_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavGamemail_Enabled!=0)&&(edtavGamemail_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,49);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavGamemail_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavGamemail_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavGamemail_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)40,(short)1,(short)-1,(short)0,(bool)true,(String)"GAMEMail",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavNotificar_Enabled!=0)&&(edtavNotificar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 50,'"+sPrefix+"',false,'"+sGXsfl_40_idx+"',40)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavNotificar_Internalname,StringUtil.RTrim( AV68Notificar),(String)"",TempTags+((edtavNotificar_Enabled!=0)&&(edtavNotificar_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavNotificar_Enabled!=0)&&(edtavNotificar_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,50);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavNotificar_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavNotificar_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavNotificar_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)40,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContratanteUsuario_EhFiscal_Internalname,StringUtil.BoolToStr( A1728ContratanteUsuario_EhFiscal),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavFerias_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavFerias_Enabled!=0)&&(edtavFerias_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 52,'"+sPrefix+"',false,'',40)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV59Ferias_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV59Ferias))&&String.IsNullOrEmpty(StringUtil.RTrim( AV74Ferias_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV59Ferias)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavFerias_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV59Ferias)) ? AV74Ferias_GXI : context.PathToRelativeUrl( AV59Ferias)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavFerias_Visible,(short)1,(String)"",(String)edtavFerias_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavFerias_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVFERIAS.CLICK."+sGXsfl_40_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV59Ferias_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavAssociarperfil_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarperfil_Enabled!=0)&&(edtavAssociarperfil_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 53,'"+sPrefix+"',false,'',40)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV33AssociarPerfil_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33AssociarPerfil))&&String.IsNullOrEmpty(StringUtil.RTrim( AV75Associarperfil_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33AssociarPerfil)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarperfil_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33AssociarPerfil)) ? AV75Associarperfil_GXI : context.PathToRelativeUrl( AV33AssociarPerfil)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavAssociarperfil_Visible,(short)1,(String)"",(String)edtavAssociarperfil_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarperfil_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e21892_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV33AssociarPerfil_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarsistema_Enabled!=0)&&(edtavAssociarsistema_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 54,'"+sPrefix+"',false,'',40)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV38AssociarSistema_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV38AssociarSistema))&&String.IsNullOrEmpty(StringUtil.RTrim( AV76Associarsistema_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV38AssociarSistema)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarsistema_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV38AssociarSistema)) ? AV76Associarsistema_GXI : context.PathToRelativeUrl( AV38AssociarSistema)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavAssociarsistema_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarsistema_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e22892_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV38AssociarSistema_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV69Config_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV69Config))&&String.IsNullOrEmpty(StringUtil.RTrim( AV77Config_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV69Config)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavConfig_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV69Config)) ? AV77Config_GXI : context.PathToRelativeUrl( AV69Config)),(String)edtavConfig_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavConfig_Enabled,(String)"",(String)edtavConfig_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV69Config_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavClonardados_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavClonardados_Enabled!=0)&&(edtavClonardados_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 56,'"+sPrefix+"',false,'',40)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV64ClonarDados_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV64ClonarDados))&&String.IsNullOrEmpty(StringUtil.RTrim( AV78Clonardados_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV64ClonarDados)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavClonardados_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV64ClonarDados)) ? AV78Clonardados_GXI : context.PathToRelativeUrl( AV64ClonarDados)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavClonardados_Visible,(int)edtavClonardados_Enabled,(String)"",(String)edtavClonardados_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavClonardados_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e23892_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV64ClonarDados_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTEUSUARIO_CONTRATANTECOD"+"_"+sGXsfl_40_idx, GetSecureSignedToken( sPrefix+sGXsfl_40_idx, context.localUtil.Format( (decimal)(A63ContratanteUsuario_ContratanteCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTEUSUARIO_USUARIOCOD"+"_"+sGXsfl_40_idx, GetSecureSignedToken( sPrefix+sGXsfl_40_idx, context.localUtil.Format( (decimal)(A60ContratanteUsuario_UsuarioCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTEUSUARIO_EHFISCAL"+"_"+sGXsfl_40_idx, GetSecureSignedToken( sPrefix+sGXsfl_40_idx, A1728ContratanteUsuario_EhFiscal));
            GridContainer.AddRow(GridRow);
            nGXsfl_40_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_40_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_40_idx+1));
            sGXsfl_40_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_40_idx), 4, 0)), 4, "0");
            SubsflControlProps_402( ) ;
         }
         /* End function sendrow_402 */
      }

      protected void init_default_properties( )
      {
         bttBtnimportar_Internalname = sPrefix+"BTNIMPORTAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavContratanteusuario_usuariopessoanom1_Internalname = sPrefix+"vCONTRATANTEUSUARIO_USUARIOPESSOANOM1";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavAtualizar_Internalname = sPrefix+"vATUALIZAR";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtContratanteUsuario_ContratanteCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_CONTRATANTECOD";
         edtContratanteUsuario_UsuarioCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOCOD";
         edtContratanteUsuario_UsuarioPessoaCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOPESSOACOD";
         edtUsuario_Nome_Internalname = sPrefix+"USUARIO_NOME";
         edtContratanteUsuario_UsuarioPessoaNom_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOPESSOANOM";
         edtContratanteUsuario_UsuarioPessoaDoc_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOPESSOADOC";
         edtavGamemail_Internalname = sPrefix+"vGAMEMAIL";
         edtavNotificar_Internalname = sPrefix+"vNOTIFICAR";
         chkContratanteUsuario_EhFiscal_Internalname = sPrefix+"CONTRATANTEUSUARIO_EHFISCAL";
         edtavFerias_Internalname = sPrefix+"vFERIAS";
         edtavAssociarperfil_Internalname = sPrefix+"vASSOCIARPERFIL";
         edtavAssociarsistema_Internalname = sPrefix+"vASSOCIARSISTEMA";
         edtavConfig_Internalname = sPrefix+"vCONFIG";
         edtavClonardados_Internalname = sPrefix+"vCLONARDADOS";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTablemergedgrid_Internalname = sPrefix+"TABLEMERGEDGRID";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavClonardados_Jsonclick = "";
         edtavAssociarsistema_Jsonclick = "";
         edtavAssociarsistema_Visible = -1;
         edtavAssociarsistema_Enabled = 1;
         edtavAssociarperfil_Jsonclick = "";
         edtavAssociarperfil_Enabled = 1;
         edtavFerias_Jsonclick = "";
         edtavFerias_Enabled = 1;
         edtavNotificar_Jsonclick = "";
         edtavNotificar_Visible = -1;
         edtavGamemail_Jsonclick = "";
         edtavGamemail_Visible = -1;
         edtContratanteUsuario_UsuarioPessoaDoc_Jsonclick = "";
         edtContratanteUsuario_UsuarioPessoaNom_Jsonclick = "";
         edtUsuario_Nome_Jsonclick = "";
         edtContratanteUsuario_UsuarioPessoaCod_Jsonclick = "";
         edtContratanteUsuario_UsuarioCod_Jsonclick = "";
         edtContratanteUsuario_ContratanteCod_Jsonclick = "";
         edtavAtualizar_Jsonclick = "";
         edtavAtualizar_Visible = -1;
         bttBtnimportar_Enabled = 1;
         edtavContratanteusuario_usuariopessoanom1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavClonardados_Tooltiptext = "Clonar dados neste usu�rio!";
         edtavClonardados_Enabled = 1;
         edtavConfig_Tooltiptext = "Permiss�es do usu�rio";
         edtavConfig_Link = "";
         edtavConfig_Enabled = 1;
         edtavAssociarsistema_Tooltiptext = "Associar os sistemas deste usu�rio!";
         edtavAssociarperfil_Tooltiptext = "Associar os perfis deste usu�rio!";
         edtavNotificar_Enabled = 1;
         edtavNotificar_Forecolor = (int)(0xFFFFFF);
         edtavGamemail_Enabled = 1;
         edtavGamemail_Forecolor = (int)(0xFFFFFF);
         edtContratanteUsuario_UsuarioPessoaDoc_Forecolor = (int)(0xFFFFFF);
         edtContratanteUsuario_UsuarioPessoaNom_Forecolor = (int)(0xFFFFFF);
         edtUsuario_Nome_Forecolor = (int)(0xFFFFFF);
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavAtualizar_Tooltiptext = "";
         edtavAtualizar_Enabled = 1;
         edtavFerias_Visible = -1;
         chkContratanteUsuario_EhFiscal_Titleformat = 0;
         edtContratanteUsuario_UsuarioPessoaDoc_Titleformat = 0;
         edtContratanteUsuario_UsuarioPessoaNom_Titleformat = 0;
         edtUsuario_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavFerias_Tooltiptext = "Clique aqui p/ marcar o usu�rio como ausente";
         edtavContratanteusuario_usuariopessoanom1_Visible = 1;
         edtavClonardados_Visible = -1;
         edtavAssociarperfil_Visible = -1;
         edtavClonardados_Title = "";
         edtavFerias_Title = "";
         edtavAssociarsistema_Title = "";
         edtavAssociarperfil_Title = "";
         chkContratanteUsuario_EhFiscal.Title.Text = "Fiscal";
         edtContratanteUsuario_UsuarioPessoaDoc_Title = "CPF";
         edtContratanteUsuario_UsuarioPessoaNom_Title = "Pessoa";
         edtUsuario_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkContratanteUsuario_EhFiscal.Caption = "";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV31ContratanteUsuario_ContratanteCod',fld:'vCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A62ContratanteUsuario_UsuarioPessoaNom',fld:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaDoc_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOADOC',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaDoc_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOADOC',prop:'Title'},{av:'chkContratanteUsuario_EhFiscal_Titleformat',ctrl:'CONTRATANTEUSUARIO_EHFISCAL',prop:'Titleformat'},{av:'chkContratanteUsuario_EhFiscal.Title.Text',ctrl:'CONTRATANTEUSUARIO_EHFISCAL',prop:'Title'},{av:'edtavAssociarperfil_Title',ctrl:'vASSOCIARPERFIL',prop:'Title'},{av:'edtavAssociarsistema_Title',ctrl:'vASSOCIARSISTEMA',prop:'Title'},{av:'edtavFerias_Title',ctrl:'vFERIAS',prop:'Title'},{av:'edtavClonardados_Title',ctrl:'vCLONARDADOS',prop:'Title'},{av:'edtavAssociarperfil_Visible',ctrl:'vASSOCIARPERFIL',prop:'Visible'},{av:'edtavClonardados_Visible',ctrl:'vCLONARDADOS',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{ctrl:'BTNIMPORTAR',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID.LOAD","{handler:'E17892',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A62ContratanteUsuario_UsuarioPessoaNom',fld:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''}],oparms:[{av:'edtavAtualizar_Tooltiptext',ctrl:'vATUALIZAR',prop:'Tooltiptext'},{av:'AV62Atualizar',fld:'vATUALIZAR',pic:'',nv:''},{av:'edtavAtualizar_Enabled',ctrl:'vATUALIZAR',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV28Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'AV59Ferias',fld:'vFERIAS',pic:'',nv:''},{av:'edtavFerias_Tooltiptext',ctrl:'vFERIAS',prop:'Tooltiptext'},{av:'AV33AssociarPerfil',fld:'vASSOCIARPERFIL',pic:'',nv:''},{av:'edtavAssociarperfil_Tooltiptext',ctrl:'vASSOCIARPERFIL',prop:'Tooltiptext'},{av:'AV38AssociarSistema',fld:'vASSOCIARSISTEMA',pic:'',nv:''},{av:'edtavAssociarsistema_Tooltiptext',ctrl:'vASSOCIARSISTEMA',prop:'Tooltiptext'},{av:'edtavConfig_Tooltiptext',ctrl:'vCONFIG',prop:'Tooltiptext'},{av:'edtavConfig_Link',ctrl:'vCONFIG',prop:'Link'},{av:'AV69Config',fld:'vCONFIG',pic:'',nv:''},{av:'edtavConfig_Enabled',ctrl:'vCONFIG',prop:'Enabled'},{av:'edtavClonardados_Tooltiptext',ctrl:'vCLONARDADOS',prop:'Tooltiptext'},{av:'AV64ClonarDados',fld:'vCLONARDADOS',pic:'',nv:''},{av:'edtavClonardados_Enabled',ctrl:'vCLONARDADOS',prop:'Enabled'},{av:'edtUsuario_Nome_Forecolor',ctrl:'USUARIO_NOME',prop:'Forecolor'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Forecolor',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Forecolor'},{av:'edtContratanteUsuario_UsuarioPessoaDoc_Forecolor',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOADOC',prop:'Forecolor'},{av:'edtavGamemail_Forecolor',ctrl:'vGAMEMAIL',prop:'Forecolor'},{av:'edtavNotificar_Forecolor',ctrl:'vNOTIFICAR',prop:'Forecolor'},{av:'edtavFerias_Visible',ctrl:'vFERIAS',prop:'Visible'},{av:'AV36GAMEMail',fld:'vGAMEMAIL',pic:'',nv:''},{av:'AV68Notificar',fld:'vNOTIFICAR',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E11892',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV31ContratanteUsuario_ContratanteCod',fld:'vCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A62ContratanteUsuario_UsuarioPessoaNom',fld:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E14892',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratanteusuario_usuariopessoanom1_Visible',ctrl:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E12892',iparms:[{av:'AV31ContratanteUsuario_ContratanteCod',fld:'vCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOASSOCIARPERFIL'","{handler:'E21892',iparms:[{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOASSOCIARSISTEMA'","{handler:'E22892',iparms:[{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOCLONARDADOS'","{handler:'E23892',iparms:[{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV31ContratanteUsuario_ContratanteCod',fld:'vCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOIMPORTAR'","{handler:'E13892',iparms:[{av:'AV31ContratanteUsuario_ContratanteCod',fld:'vCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("USUARIO_NOME.CLICK","{handler:'E18892',iparms:[{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("VATUALIZAR.CLICK","{handler:'E19892',iparms:[{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("VFERIAS.CLICK","{handler:'E20892',iparms:[{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV59Ferias',fld:'vFERIAS',pic:'',nv:''},{av:'edtavFerias_Tooltiptext',ctrl:'vFERIAS',prop:'Tooltiptext'}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV31ContratanteUsuario_ContratanteCod',fld:'vCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A62ContratanteUsuario_UsuarioPessoaNom',fld:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaDoc_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOADOC',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaDoc_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOADOC',prop:'Title'},{av:'chkContratanteUsuario_EhFiscal_Titleformat',ctrl:'CONTRATANTEUSUARIO_EHFISCAL',prop:'Titleformat'},{av:'chkContratanteUsuario_EhFiscal.Title.Text',ctrl:'CONTRATANTEUSUARIO_EHFISCAL',prop:'Title'},{av:'edtavAssociarperfil_Title',ctrl:'vASSOCIARPERFIL',prop:'Title'},{av:'edtavAssociarsistema_Title',ctrl:'vASSOCIARSISTEMA',prop:'Title'},{av:'edtavFerias_Title',ctrl:'vFERIAS',prop:'Title'},{av:'edtavClonardados_Title',ctrl:'vCLONARDADOS',prop:'Title'},{av:'edtavAssociarperfil_Visible',ctrl:'vASSOCIARPERFIL',prop:'Visible'},{av:'edtavClonardados_Visible',ctrl:'vCLONARDADOS',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{ctrl:'BTNIMPORTAR',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV31ContratanteUsuario_ContratanteCod',fld:'vCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A62ContratanteUsuario_UsuarioPessoaNom',fld:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaDoc_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOADOC',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaDoc_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOADOC',prop:'Title'},{av:'chkContratanteUsuario_EhFiscal_Titleformat',ctrl:'CONTRATANTEUSUARIO_EHFISCAL',prop:'Titleformat'},{av:'chkContratanteUsuario_EhFiscal.Title.Text',ctrl:'CONTRATANTEUSUARIO_EHFISCAL',prop:'Title'},{av:'edtavAssociarperfil_Title',ctrl:'vASSOCIARPERFIL',prop:'Title'},{av:'edtavAssociarsistema_Title',ctrl:'vASSOCIARSISTEMA',prop:'Title'},{av:'edtavFerias_Title',ctrl:'vFERIAS',prop:'Title'},{av:'edtavClonardados_Title',ctrl:'vCLONARDADOS',prop:'Title'},{av:'edtavAssociarperfil_Visible',ctrl:'vASSOCIARPERFIL',prop:'Visible'},{av:'edtavClonardados_Visible',ctrl:'vCLONARDADOS',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{ctrl:'BTNIMPORTAR',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV31ContratanteUsuario_ContratanteCod',fld:'vCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A62ContratanteUsuario_UsuarioPessoaNom',fld:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaDoc_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOADOC',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaDoc_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOADOC',prop:'Title'},{av:'chkContratanteUsuario_EhFiscal_Titleformat',ctrl:'CONTRATANTEUSUARIO_EHFISCAL',prop:'Titleformat'},{av:'chkContratanteUsuario_EhFiscal.Title.Text',ctrl:'CONTRATANTEUSUARIO_EHFISCAL',prop:'Title'},{av:'edtavAssociarperfil_Title',ctrl:'vASSOCIARPERFIL',prop:'Title'},{av:'edtavAssociarsistema_Title',ctrl:'vASSOCIARSISTEMA',prop:'Title'},{av:'edtavFerias_Title',ctrl:'vFERIAS',prop:'Title'},{av:'edtavClonardados_Title',ctrl:'vCLONARDADOS',prop:'Title'},{av:'edtavAssociarperfil_Visible',ctrl:'vASSOCIARPERFIL',prop:'Visible'},{av:'edtavClonardados_Visible',ctrl:'vCLONARDADOS',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{ctrl:'BTNIMPORTAR',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV31ContratanteUsuario_ContratanteCod',fld:'vCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A62ContratanteUsuario_UsuarioPessoaNom',fld:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratanteUsuario_UsuarioPessoaNom1',fld:'vCONTRATANTEUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'edtContratanteUsuario_UsuarioPessoaDoc_Titleformat',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOADOC',prop:'Titleformat'},{av:'edtContratanteUsuario_UsuarioPessoaDoc_Title',ctrl:'CONTRATANTEUSUARIO_USUARIOPESSOADOC',prop:'Title'},{av:'chkContratanteUsuario_EhFiscal_Titleformat',ctrl:'CONTRATANTEUSUARIO_EHFISCAL',prop:'Titleformat'},{av:'chkContratanteUsuario_EhFiscal.Title.Text',ctrl:'CONTRATANTEUSUARIO_EHFISCAL',prop:'Title'},{av:'edtavAssociarperfil_Title',ctrl:'vASSOCIARPERFIL',prop:'Title'},{av:'edtavAssociarsistema_Title',ctrl:'vASSOCIARSISTEMA',prop:'Title'},{av:'edtavFerias_Title',ctrl:'vFERIAS',prop:'Title'},{av:'edtavClonardados_Title',ctrl:'vCLONARDADOS',prop:'Title'},{av:'edtavAssociarperfil_Visible',ctrl:'vASSOCIARPERFIL',prop:'Visible'},{av:'edtavClonardados_Visible',ctrl:'vCLONARDADOS',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{ctrl:'BTNIMPORTAR',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ContratanteUsuario_UsuarioPessoaNom1 = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV79Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         A341Usuario_UserGamGuid = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV62Atualizar = "";
         AV72Atualizar_GXI = "";
         AV28Delete = "";
         AV73Delete_GXI = "";
         A2Usuario_Nome = "";
         A492ContratanteUsuario_UsuarioPessoaDoc = "";
         AV36GAMEMail = "";
         AV68Notificar = "";
         AV59Ferias = "";
         AV74Ferias_GXI = "";
         AV33AssociarPerfil = "";
         AV75Associarperfil_GXI = "";
         AV38AssociarSistema = "";
         AV76Associarsistema_GXI = "";
         AV69Config = "";
         AV77Config_GXI = "";
         AV64ClonarDados = "";
         AV78Clonardados_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17ContratanteUsuario_UsuarioPessoaNom1 = "";
         H00893_A54Usuario_Ativo = new bool[] {false} ;
         H00893_n54Usuario_Ativo = new bool[] {false} ;
         H00893_A1908Usuario_DeFerias = new bool[] {false} ;
         H00893_n1908Usuario_DeFerias = new bool[] {false} ;
         H00893_A341Usuario_UserGamGuid = new String[] {""} ;
         H00893_n341Usuario_UserGamGuid = new bool[] {false} ;
         H00893_A1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         H00893_n1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         H00893_A492ContratanteUsuario_UsuarioPessoaDoc = new String[] {""} ;
         H00893_n492ContratanteUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         H00893_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00893_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00893_A2Usuario_Nome = new String[] {""} ;
         H00893_n2Usuario_Nome = new bool[] {false} ;
         H00893_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00893_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00893_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00893_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00893_A40000UsuarioNotifica_NoStatus = new String[] {""} ;
         H00893_n40000UsuarioNotifica_NoStatus = new bool[] {false} ;
         A40000UsuarioNotifica_NoStatus = "";
         H00895_AGRID_nRecordCount = new long[1] ;
         AV37GamUser = new SdtGAMUser(context);
         GXt_char1 = "";
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV30Session = context.GetSession();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV34Websession = context.GetSession();
         AV61Usuario = new SdtUsuario(context);
         AV65AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV66AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV67AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         bttBtnimportar_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV31ContratanteUsuario_ContratanteCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratantecontratanteusuariowc__default(),
            new Object[][] {
                new Object[] {
               H00893_A54Usuario_Ativo, H00893_n54Usuario_Ativo, H00893_A1908Usuario_DeFerias, H00893_n1908Usuario_DeFerias, H00893_A341Usuario_UserGamGuid, H00893_n341Usuario_UserGamGuid, H00893_A1728ContratanteUsuario_EhFiscal, H00893_n1728ContratanteUsuario_EhFiscal, H00893_A492ContratanteUsuario_UsuarioPessoaDoc, H00893_n492ContratanteUsuario_UsuarioPessoaDoc,
               H00893_A62ContratanteUsuario_UsuarioPessoaNom, H00893_n62ContratanteUsuario_UsuarioPessoaNom, H00893_A2Usuario_Nome, H00893_n2Usuario_Nome, H00893_A61ContratanteUsuario_UsuarioPessoaCod, H00893_n61ContratanteUsuario_UsuarioPessoaCod, H00893_A60ContratanteUsuario_UsuarioCod, H00893_A63ContratanteUsuario_ContratanteCod, H00893_A40000UsuarioNotifica_NoStatus, H00893_n40000UsuarioNotifica_NoStatus
               }
               , new Object[] {
               H00895_AGRID_nRecordCount
               }
            }
         );
         AV79Pgmname = "ContratanteContratanteUsuarioWC";
         /* GeneXus formulas. */
         AV79Pgmname = "ContratanteContratanteUsuarioWC";
         context.Gx_err = 0;
         edtavGamemail_Enabled = 0;
         edtavNotificar_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_40 ;
      private short nGXsfl_40_idx=1 ;
      private short AV32OrderedBy ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_40_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtUsuario_Nome_Titleformat ;
      private short edtContratanteUsuario_UsuarioPessoaNom_Titleformat ;
      private short edtContratanteUsuario_UsuarioPessoaDoc_Titleformat ;
      private short chkContratanteUsuario_EhFiscal_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int AV31ContratanteUsuario_ContratanteCod ;
      private int wcpOAV31ContratanteUsuario_ContratanteCod ;
      private int subGrid_Rows ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int edtavGamemail_Enabled ;
      private int edtavNotificar_Enabled ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int edtavAssociarperfil_Visible ;
      private int edtavClonardados_Visible ;
      private int edtavAtualizar_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavConfig_Enabled ;
      private int edtavClonardados_Enabled ;
      private int edtUsuario_Nome_Forecolor ;
      private int edtContratanteUsuario_UsuarioPessoaNom_Forecolor ;
      private int edtContratanteUsuario_UsuarioPessoaDoc_Forecolor ;
      private int edtavGamemail_Forecolor ;
      private int edtavNotificar_Forecolor ;
      private int edtavFerias_Visible ;
      private int imgInsert_Enabled ;
      private int bttBtnimportar_Enabled ;
      private int edtavContratanteusuario_usuariopessoanom1_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavAtualizar_Visible ;
      private int edtavGamemail_Visible ;
      private int edtavNotificar_Visible ;
      private int edtavFerias_Enabled ;
      private int edtavAssociarperfil_Enabled ;
      private int edtavAssociarsistema_Enabled ;
      private int edtavAssociarsistema_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV63Color ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_40_idx="0001" ;
      private String AV17ContratanteUsuario_UsuarioPessoaNom1 ;
      private String AV79Pgmname ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String A341Usuario_UserGamGuid ;
      private String GXKey ;
      private String edtavGamemail_Internalname ;
      private String edtavNotificar_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavAtualizar_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratanteUsuario_ContratanteCod_Internalname ;
      private String edtContratanteUsuario_UsuarioCod_Internalname ;
      private String edtContratanteUsuario_UsuarioPessoaCod_Internalname ;
      private String A2Usuario_Nome ;
      private String edtUsuario_Nome_Internalname ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Internalname ;
      private String edtContratanteUsuario_UsuarioPessoaDoc_Internalname ;
      private String AV68Notificar ;
      private String chkContratanteUsuario_EhFiscal_Internalname ;
      private String edtavFerias_Internalname ;
      private String edtavAssociarperfil_Internalname ;
      private String edtavAssociarsistema_Internalname ;
      private String edtavConfig_Internalname ;
      private String edtavClonardados_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV17ContratanteUsuario_UsuarioPessoaNom1 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContratanteusuario_usuariopessoanom1_Internalname ;
      private String edtUsuario_Nome_Title ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Title ;
      private String edtContratanteUsuario_UsuarioPessoaDoc_Title ;
      private String edtavAssociarperfil_Title ;
      private String edtavAssociarsistema_Title ;
      private String edtavFerias_Title ;
      private String edtavClonardados_Title ;
      private String edtavAtualizar_Tooltiptext ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavFerias_Tooltiptext ;
      private String edtavAssociarperfil_Tooltiptext ;
      private String edtavAssociarsistema_Tooltiptext ;
      private String edtavConfig_Tooltiptext ;
      private String edtavConfig_Link ;
      private String edtavClonardados_Tooltiptext ;
      private String GXt_char1 ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String bttBtnimportar_Internalname ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablemergedgrid_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String TempTags ;
      private String imgInsert_Jsonclick ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavContratanteusuario_usuariopessoanom1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String bttBtnimportar_Jsonclick ;
      private String sCtrlAV31ContratanteUsuario_ContratanteCod ;
      private String sGXsfl_40_fel_idx="0001" ;
      private String edtavAtualizar_Jsonclick ;
      private String ROClassString ;
      private String edtContratanteUsuario_ContratanteCod_Jsonclick ;
      private String edtContratanteUsuario_UsuarioCod_Jsonclick ;
      private String edtContratanteUsuario_UsuarioPessoaCod_Jsonclick ;
      private String edtUsuario_Nome_Jsonclick ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Jsonclick ;
      private String edtContratanteUsuario_UsuarioPessoaDoc_Jsonclick ;
      private String edtavGamemail_Jsonclick ;
      private String edtavNotificar_Jsonclick ;
      private String edtavFerias_Jsonclick ;
      private String edtavAssociarperfil_Jsonclick ;
      private String edtavAssociarsistema_Jsonclick ;
      private String edtavClonardados_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool A54Usuario_Ativo ;
      private bool n54Usuario_Ativo ;
      private bool A1908Usuario_DeFerias ;
      private bool n1908Usuario_DeFerias ;
      private bool n341Usuario_UserGamGuid ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n2Usuario_Nome ;
      private bool n492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool A1728ContratanteUsuario_EhFiscal ;
      private bool n1728ContratanteUsuario_EhFiscal ;
      private bool n40000UsuarioNotifica_NoStatus ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV62Atualizar_IsBlob ;
      private bool AV28Delete_IsBlob ;
      private bool AV59Ferias_IsBlob ;
      private bool AV33AssociarPerfil_IsBlob ;
      private bool AV38AssociarSistema_IsBlob ;
      private bool AV69Config_IsBlob ;
      private bool AV64ClonarDados_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV72Atualizar_GXI ;
      private String AV73Delete_GXI ;
      private String A492ContratanteUsuario_UsuarioPessoaDoc ;
      private String AV36GAMEMail ;
      private String AV74Ferias_GXI ;
      private String AV75Associarperfil_GXI ;
      private String AV76Associarsistema_GXI ;
      private String AV77Config_GXI ;
      private String AV78Clonardados_GXI ;
      private String A40000UsuarioNotifica_NoStatus ;
      private String AV62Atualizar ;
      private String AV28Delete ;
      private String AV59Ferias ;
      private String AV33AssociarPerfil ;
      private String AV38AssociarSistema ;
      private String AV69Config ;
      private String AV64ClonarDados ;
      private String imgInsert_Bitmap ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCheckbox chkContratanteUsuario_EhFiscal ;
      private IDataStoreProvider pr_default ;
      private bool[] H00893_A54Usuario_Ativo ;
      private bool[] H00893_n54Usuario_Ativo ;
      private bool[] H00893_A1908Usuario_DeFerias ;
      private bool[] H00893_n1908Usuario_DeFerias ;
      private String[] H00893_A341Usuario_UserGamGuid ;
      private bool[] H00893_n341Usuario_UserGamGuid ;
      private bool[] H00893_A1728ContratanteUsuario_EhFiscal ;
      private bool[] H00893_n1728ContratanteUsuario_EhFiscal ;
      private String[] H00893_A492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] H00893_n492ContratanteUsuario_UsuarioPessoaDoc ;
      private String[] H00893_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00893_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] H00893_A2Usuario_Nome ;
      private bool[] H00893_n2Usuario_Nome ;
      private int[] H00893_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00893_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00893_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00893_A63ContratanteUsuario_ContratanteCod ;
      private String[] H00893_A40000UsuarioNotifica_NoStatus ;
      private bool[] H00893_n40000UsuarioNotifica_NoStatus ;
      private long[] H00895_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private IGxSession AV34Websession ;
      private SdtUsuario AV61Usuario ;
      private wwpbaseobjects.SdtAuditingObject AV65AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV66AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV67AuditingObjectRecordItemAttributeItem ;
      private SdtGAMUser AV37GamUser ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contratantecontratanteusuariowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00893( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV17ContratanteUsuario_UsuarioPessoaNom1 ,
                                             String A62ContratanteUsuario_UsuarioPessoaNom ,
                                             short AV32OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             int AV31ContratanteUsuario_ContratanteCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [8] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Usuario_Ativo], T2.[Usuario_DeFerias], T2.[Usuario_UserGamGuid], T1.[ContratanteUsuario_EhFiscal], T3.[Pessoa_Docto] AS ContratanteUsuario_UsuarioPessoaDoc, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPessoaNom, T2.[Usuario_Nome], T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T1.[ContratanteUsuario_ContratanteCod], COALESCE( T4.[UsuarioNotifica_NoStatus], '') AS UsuarioNotifica_NoStatus";
         sFromString = " FROM ((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T5.[UsuarioNotifica_NoStatus]) AS UsuarioNotifica_NoStatus, T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [UsuarioNotifica] T5 WITH (NOLOCK),  [ContratanteUsuario] T6 WITH (NOLOCK) WHERE (T5.[UsuarioNotifica_UsuarioCod] = T6.[ContratanteUsuario_UsuarioCod]) AND (T5.[UsuarioNotifica_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) GROUP BY T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] ) T4 ON T4.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T4.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratanteUsuario_ContratanteCod] = @AV31ContratanteUsuario_ContratanteCod)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratanteUsuario_UsuarioPessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV17ContratanteUsuario_UsuarioPessoaNom1 + '%')";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( AV32OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod], T3.[Pessoa_Nome]";
         }
         else if ( ( AV32OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod] DESC, T3.[Pessoa_Nome] DESC";
         }
         else if ( ( AV32OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod], T2.[Usuario_Nome]";
         }
         else if ( ( AV32OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod] DESC, T2.[Usuario_Nome] DESC";
         }
         else if ( ( AV32OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod], T3.[Pessoa_Docto]";
         }
         else if ( ( AV32OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod] DESC, T3.[Pessoa_Docto] DESC";
         }
         else if ( ( AV32OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_EhFiscal]";
         }
         else if ( ( AV32OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod] DESC, T1.[ContratanteUsuario_EhFiscal] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00895( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV17ContratanteUsuario_UsuarioPessoaNom1 ,
                                             String A62ContratanteUsuario_UsuarioPessoaNom ,
                                             short AV32OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             int AV31ContratanteUsuario_ContratanteCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [3] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T5.[UsuarioNotifica_NoStatus]) AS UsuarioNotifica_NoStatus, T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [UsuarioNotifica] T5 WITH (NOLOCK),  [ContratanteUsuario] T6 WITH (NOLOCK) WHERE (T5.[UsuarioNotifica_UsuarioCod] = T6.[ContratanteUsuario_UsuarioCod]) AND (T5.[UsuarioNotifica_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) GROUP BY T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] ) T4 ON T4.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T4.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratanteUsuario_ContratanteCod] = @AV31ContratanteUsuario_ContratanteCod)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratanteUsuario_UsuarioPessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV17ContratanteUsuario_UsuarioPessoaNom1 + '%')";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV32OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV32OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV32OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV32OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV32OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV32OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV32OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV32OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00893(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (bool)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] );
               case 1 :
                     return conditional_H00895(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (bool)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00893 ;
          prmH00893 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV31ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17ContratanteUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00895 ;
          prmH00895 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV31ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17ContratanteUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00893", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00893,11,0,true,false )
             ,new CursorDef("H00895", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00895,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 40) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

}
