/*
               File: GAMExampleWWRolePermissions
        Description: Role`s permissions
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:11:39.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexamplewwrolepermissions : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamexamplewwrolepermissions( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexamplewwrolepermissions( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref long aP0_RoleId ,
                           ref long aP1_pApplicationId )
      {
         this.AV29RoleId = aP0_RoleId;
         this.AV25pApplicationId = aP1_pApplicationId;
         executePrivate();
         aP0_RoleId=this.AV29RoleId;
         aP1_pApplicationId=this.AV25pApplicationId;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavApplicationid = new GXCombobox();
         cmbavPermissionaccesstype = new GXCombobox();
         cmbavBoolenfilter = new GXCombobox();
         cmbavOld_accesstype = new GXCombobox();
         chkavOld_inherited = new GXCheckbox();
         cmbavAccesstype = new GXCombobox();
         chkavInherited = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridww") == 0 )
            {
               nRC_GXsfl_49 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_49_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_49_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridww_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridww") == 0 )
            {
               subGridww_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV29RoleId = (long)(NumberUtil.Val( GetNextPar( ), "."));
               AV9ApplicationId = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ApplicationId), 12, 0)));
               AV17FilName = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FilName", AV17FilName);
               AV32PermissionAccessType = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32PermissionAccessType", AV32PermissionAccessType);
               AV31BoolenFilter = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31BoolenFilter", AV31BoolenFilter);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridww_refresh( subGridww_Rows, AV29RoleId, AV9ApplicationId, AV17FilName, AV32PermissionAccessType, AV31BoolenFilter) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV29RoleId = (long)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV25pApplicationId = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25pApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25pApplicationId), 12, 0)));
               }
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpagepopup", "GeneXus.Programs.gammasterpagepopup", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA212( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START212( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823114199");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexamplewwrolepermissions.aspx") + "?" + UrlEncode("" +AV29RoleId) + "," + UrlEncode("" +AV25pApplicationId)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_49", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_49), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPAPPLICATIONID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25pApplicationId), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDWW_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDWW_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDWW_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDWW_nEOF), 1, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE212( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT212( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamexamplewwrolepermissions.aspx") + "?" + UrlEncode("" +AV29RoleId) + "," + UrlEncode("" +AV25pApplicationId) ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleWWRolePermissions" ;
      }

      public override String GetPgmdesc( )
      {
         return "Role`s permissions" ;
      }

      protected void WB210( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table1_3_212( true) ;
         }
         else
         {
            wb_table1_3_212( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_212e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START212( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Role`s permissions", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP210( ) ;
      }

      protected void WS212( )
      {
         START212( ) ;
         EVT212( ) ;
      }

      protected void EVT212( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDNEW'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11212 */
                              E11212 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'SAVE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12212 */
                              E12212 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDWWPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDWWPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgridww_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgridww_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgridww_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgridww_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 11), "GRIDWW.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VBTNDLT.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VBTNDLT.CLICK") == 0 ) )
                           {
                              nGXsfl_49_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_49_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_49_idx), 4, 0)), 4, "0");
                              SubsflControlProps_492( ) ;
                              cmbavOld_accesstype.Name = cmbavOld_accesstype_Internalname;
                              cmbavOld_accesstype.CurrentValue = cgiGet( cmbavOld_accesstype_Internalname);
                              AV23old_AccessType = cgiGet( cmbavOld_accesstype_Internalname);
                              AV24old_Inherited = StringUtil.StrToBool( cgiGet( chkavOld_inherited_Internalname));
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavAppid_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAppid_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAPPID");
                                 GX_FocusControl = edtavAppid_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV6AppId = 0;
                              }
                              else
                              {
                                 AV6AppId = (long)(context.localUtil.CToN( cgiGet( edtavAppid_Internalname), ",", "."));
                              }
                              AV29RoleId = (long)(context.localUtil.CToN( cgiGet( edtavRoleid_Internalname), ",", "."));
                              AV11BtnDlt = cgiGet( edtavBtndlt_Internalname);
                              AV19Id = cgiGet( edtavId_Internalname);
                              AV22Name = cgiGet( edtavName_Internalname);
                              AV14Dsc = cgiGet( edtavDsc_Internalname);
                              cmbavAccesstype.Name = cmbavAccesstype_Internalname;
                              cmbavAccesstype.CurrentValue = cgiGet( cmbavAccesstype_Internalname);
                              AV5AccessType = cgiGet( cmbavAccesstype_Internalname);
                              AV20Inherited = StringUtil.StrToBool( cgiGet( chkavInherited_Internalname));
                              CheckSecurityRow2149( sGXsfl_49_idx) ;
                              if ( GxWebError != 0 )
                              {
                                 return  ;
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13212 */
                                    E13212 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDWW.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14212 */
                                    E14212 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VBTNDLT.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15212 */
                                    E15212 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE212( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA212( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavApplicationid.Name = "vAPPLICATIONID";
            cmbavApplicationid.WebTags = "";
            if ( cmbavApplicationid.ItemCount > 0 )
            {
               AV9ApplicationId = (long)(NumberUtil.Val( cmbavApplicationid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9ApplicationId), 12, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ApplicationId), 12, 0)));
            }
            cmbavPermissionaccesstype.Name = "vPERMISSIONACCESSTYPE";
            cmbavPermissionaccesstype.WebTags = "";
            cmbavPermissionaccesstype.addItem("", "(Nenhum)", 0);
            cmbavPermissionaccesstype.addItem("A", "Allow", 0);
            cmbavPermissionaccesstype.addItem("D", "Deny", 0);
            cmbavPermissionaccesstype.addItem("R", "Restricted", 0);
            if ( cmbavPermissionaccesstype.ItemCount > 0 )
            {
               AV32PermissionAccessType = cmbavPermissionaccesstype.getValidValue(AV32PermissionAccessType);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32PermissionAccessType", AV32PermissionAccessType);
            }
            cmbavBoolenfilter.Name = "vBOOLENFILTER";
            cmbavBoolenfilter.WebTags = "";
            cmbavBoolenfilter.addItem("A", "All", 0);
            cmbavBoolenfilter.addItem("T", "Yes", 0);
            cmbavBoolenfilter.addItem("F", "No", 0);
            if ( cmbavBoolenfilter.ItemCount > 0 )
            {
               AV31BoolenFilter = cmbavBoolenfilter.getValidValue(AV31BoolenFilter);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31BoolenFilter", AV31BoolenFilter);
            }
            GXCCtl = "vOLD_ACCESSTYPE_" + sGXsfl_49_idx;
            cmbavOld_accesstype.Name = GXCCtl;
            cmbavOld_accesstype.WebTags = "";
            cmbavOld_accesstype.addItem("A", "Allow", 0);
            cmbavOld_accesstype.addItem("D", "Deny", 0);
            cmbavOld_accesstype.addItem("R", "Restricted", 0);
            if ( cmbavOld_accesstype.ItemCount > 0 )
            {
               AV23old_AccessType = cmbavOld_accesstype.getValidValue(AV23old_AccessType);
            }
            GXCCtl = "vOLD_INHERITED_" + sGXsfl_49_idx;
            chkavOld_inherited.Name = GXCCtl;
            chkavOld_inherited.WebTags = "";
            chkavOld_inherited.Caption = "";
            chkavOld_inherited.CheckedValue = "false";
            GXCCtl = "vACCESSTYPE_" + sGXsfl_49_idx;
            cmbavAccesstype.Name = GXCCtl;
            cmbavAccesstype.WebTags = "";
            cmbavAccesstype.addItem("A", "Allow", 0);
            cmbavAccesstype.addItem("D", "Deny", 0);
            cmbavAccesstype.addItem("R", "Restricted", 0);
            if ( cmbavAccesstype.ItemCount > 0 )
            {
               AV5AccessType = cmbavAccesstype.getValidValue(AV5AccessType);
            }
            GXCCtl = "vINHERITED_" + sGXsfl_49_idx;
            chkavInherited.Name = GXCCtl;
            chkavInherited.WebTags = "";
            chkavInherited.Caption = "";
            chkavInherited.CheckedValue = "false";
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavCtlname_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridww_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_492( ) ;
         while ( nGXsfl_49_idx <= nRC_GXsfl_49 )
         {
            sendrow_492( ) ;
            sendsecurityrow_492( ) ;
            nGXsfl_49_idx = (short)(((subGridww_Islastpage==1)&&(nGXsfl_49_idx+1>subGridww_Recordsperpage( )) ? 1 : nGXsfl_49_idx+1));
            sGXsfl_49_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_49_idx), 4, 0)), 4, "0");
            SubsflControlProps_492( ) ;
         }
         context.GX_webresponse.AddString(GridwwContainer.ToJavascriptSource());
         /* End function gxnrGridww_newrow */
      }

      protected void gxgrGridww_refresh( int subGridww_Rows ,
                                         long AV29RoleId ,
                                         long AV9ApplicationId ,
                                         String AV17FilName ,
                                         String AV32PermissionAccessType ,
                                         String AV31BoolenFilter )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         GRIDWW_nCurrentRecord = 0;
         RF212( ) ;
         context.GX_msglist = BackMsgLst;
         /* End function gxgrGridww_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavApplicationid.ItemCount > 0 )
         {
            AV9ApplicationId = (long)(NumberUtil.Val( cmbavApplicationid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9ApplicationId), 12, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ApplicationId), 12, 0)));
         }
         if ( cmbavPermissionaccesstype.ItemCount > 0 )
         {
            AV32PermissionAccessType = cmbavPermissionaccesstype.getValidValue(AV32PermissionAccessType);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32PermissionAccessType", AV32PermissionAccessType);
         }
         if ( cmbavBoolenfilter.ItemCount > 0 )
         {
            AV31BoolenFilter = cmbavBoolenfilter.getValidValue(AV31BoolenFilter);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31BoolenFilter", AV31BoolenFilter);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF212( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname_Enabled), 5, 0)));
         cmbavOld_accesstype.Enabled = 0;
         chkavOld_inherited.Enabled = 0;
         edtavAppid_Enabled = 0;
         edtavRoleid_Enabled = 0;
         edtavId_Enabled = 0;
         edtavName_Enabled = 0;
         edtavDsc_Enabled = 0;
      }

      protected void RF212( )
      {
         if ( isAjaxCallMode( ) )
         {
            GridwwContainer.ClearRows();
         }
         wbStart = 49;
         nGXsfl_49_idx = 1;
         sGXsfl_49_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_49_idx), 4, 0)), 4, "0");
         SubsflControlProps_492( ) ;
         nGXsfl_49_Refreshing = 1;
         GridwwContainer.AddObjectProperty("GridName", "Gridww");
         GridwwContainer.AddObjectProperty("CmpContext", "");
         GridwwContainer.AddObjectProperty("InMasterPage", "false");
         GridwwContainer.AddObjectProperty("Class", "WorkWith");
         GridwwContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridwwContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridwwContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Backcolorstyle), 1, 0, ".", "")));
         GridwwContainer.PageSize = subGridww_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_492( ) ;
            /* Execute user event: E14212 */
            E14212 ();
            if ( ( GRIDWW_nCurrentRecord > 0 ) && ( GRIDWW_nGridOutOfScope == 0 ) && ( nGXsfl_49_idx == 1 ) )
            {
               GRIDWW_nCurrentRecord = 0;
               GRIDWW_nGridOutOfScope = 1;
               subgridww_firstpage( ) ;
               /* Execute user event: E14212 */
               E14212 ();
            }
            wbEnd = 49;
            WB210( ) ;
         }
         nGXsfl_49_Refreshing = 0;
      }

      protected void CheckSecurityRow2149( String sGXChecksfl_idx )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected int subGridww_Pagecount( )
      {
         GRIDWW_nRecordCount = subGridww_Recordcount( );
         if ( ((int)((GRIDWW_nRecordCount) % (subGridww_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDWW_nRecordCount/ (decimal)(subGridww_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDWW_nRecordCount/ (decimal)(subGridww_Recordsperpage( ))))+1) ;
      }

      protected int subGridww_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridww_Recordsperpage( )
      {
         return (int)(15*1) ;
      }

      protected int subGridww_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgridww_firstpage( )
      {
         GRIDWW_nFirstRecordOnPage = 0;
         return 0 ;
      }

      protected short subgridww_nextpage( )
      {
         if ( GRIDWW_nEOF == 0 )
         {
            GRIDWW_nFirstRecordOnPage = (long)(GRIDWW_nFirstRecordOnPage+subGridww_Recordsperpage( ));
         }
         return (short)(((GRIDWW_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridww_previouspage( )
      {
         if ( GRIDWW_nFirstRecordOnPage >= subGridww_Recordsperpage( ) )
         {
            GRIDWW_nFirstRecordOnPage = (long)(GRIDWW_nFirstRecordOnPage-subGridww_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         return 0 ;
      }

      protected short subgridww_lastpage( )
      {
         subGridww_Islastpage = 1;
         return 0 ;
      }

      protected int subgridww_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDWW_nFirstRecordOnPage = (long)(subGridww_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDWW_nFirstRecordOnPage = 0;
         }
         return (int)(0) ;
      }

      protected void STRUP210( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname_Enabled), 5, 0)));
         cmbavOld_accesstype.Enabled = 0;
         chkavOld_inherited.Enabled = 0;
         edtavAppid_Enabled = 0;
         edtavRoleid_Enabled = 0;
         edtavId_Enabled = 0;
         edtavName_Enabled = 0;
         edtavDsc_Enabled = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13212 */
         E13212 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV18GAMRole.gxTpr_Name = cgiGet( edtavCtlname_Internalname);
            cmbavApplicationid.Name = cmbavApplicationid_Internalname;
            cmbavApplicationid.CurrentValue = cgiGet( cmbavApplicationid_Internalname);
            AV9ApplicationId = (long)(NumberUtil.Val( cgiGet( cmbavApplicationid_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ApplicationId), 12, 0)));
            AV17FilName = cgiGet( edtavFilname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FilName", AV17FilName);
            cmbavPermissionaccesstype.Name = cmbavPermissionaccesstype_Internalname;
            cmbavPermissionaccesstype.CurrentValue = cgiGet( cmbavPermissionaccesstype_Internalname);
            AV32PermissionAccessType = cgiGet( cmbavPermissionaccesstype_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32PermissionAccessType", AV32PermissionAccessType);
            cmbavBoolenfilter.Name = cmbavBoolenfilter_Internalname;
            cmbavBoolenfilter.CurrentValue = cgiGet( cmbavBoolenfilter_Internalname);
            AV31BoolenFilter = cgiGet( cmbavBoolenfilter_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31BoolenFilter", AV31BoolenFilter);
            /* Read saved values. */
            nRC_GXsfl_49 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_49"), ",", "."));
            GRIDWW_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDWW_nFirstRecordOnPage"), ",", "."));
            GRIDWW_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDWW_nEOF"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13212 */
         E13212 ();
         if (returnInSub) return;
      }

      protected void E13212( )
      {
         /* Start Routine */
         cmbavApplicationid.removeAllItems();
         cmbavApplicationid.addItem("0", "(Select)", 0);
         AV38GXV3 = 1;
         AV37GXV2 = new SdtGAMRepository(context).getapplications(AV8ApplicationFilter, out  AV16Errors);
         while ( AV38GXV3 <= AV37GXV2.Count )
         {
            AV7Application = ((SdtGAMApplication)AV37GXV2.Item(AV38GXV3));
            cmbavApplicationid.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV7Application.gxTpr_Id), 12, 0)), AV7Application.gxTpr_Name, 0);
            AV38GXV3 = (int)(AV38GXV3+1);
         }
         if ( cmbavApplicationid.ItemCount == 2 )
         {
            AV9ApplicationId = AV7Application.gxTpr_Id;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ApplicationId), 12, 0)));
         }
         else
         {
            AV9ApplicationId = AV25pApplicationId;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ApplicationId), 12, 0)));
         }
      }

      private void E14212( )
      {
         /* Gridww_Load Routine */
         AV18GAMRole.load( AV29RoleId);
         AV33RolePermissionFilter.gxTpr_Applicationid = AV9ApplicationId;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33RolePermissionFilter", AV33RolePermissionFilter);
         AV33RolePermissionFilter.gxTpr_Name = AV17FilName;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33RolePermissionFilter", AV33RolePermissionFilter);
         AV33RolePermissionFilter.gxTpr_Accesstype = AV32PermissionAccessType;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33RolePermissionFilter", AV33RolePermissionFilter);
         AV33RolePermissionFilter.gxTpr_Inherited = AV31BoolenFilter;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33RolePermissionFilter", AV33RolePermissionFilter);
         if ( ! (0==AV9ApplicationId) )
         {
            AV40GXV5 = 1;
            AV39GXV4 = AV18GAMRole.getpermissions(AV33RolePermissionFilter, out  AV16Errors);
            while ( AV40GXV5 <= AV39GXV4.Count )
            {
               AV26Permission = ((SdtGAMPermission)AV39GXV4.Item(AV40GXV5));
               AV11BtnDlt = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
               AV41Btndlt_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
               AV19Id = AV26Permission.gxTpr_Guid;
               AV22Name = AV26Permission.gxTpr_Name;
               AV14Dsc = AV26Permission.gxTpr_Description;
               AV5AccessType = AV26Permission.gxTpr_Type;
               AV20Inherited = AV26Permission.gxTpr_Inherited;
               AV23old_AccessType = AV26Permission.gxTpr_Type;
               AV24old_Inherited = AV26Permission.gxTpr_Inherited;
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 49;
               }
               if ( ( subGridww_Islastpage == 1 ) || ( 15 == 0 ) || ( ( GRIDWW_nCurrentRecord >= GRIDWW_nFirstRecordOnPage ) && ( GRIDWW_nCurrentRecord < GRIDWW_nFirstRecordOnPage + subGridww_Recordsperpage( ) ) ) )
               {
                  sendrow_492( ) ;
                  sendsecurityrow_492( ) ;
                  GRIDWW_nEOF = 1;
                  if ( ( subGridww_Islastpage == 1 ) && ( ((int)((GRIDWW_nCurrentRecord) % (subGridww_Recordsperpage( )))) == 0 ) )
                  {
                     GRIDWW_nFirstRecordOnPage = GRIDWW_nCurrentRecord;
                  }
               }
               if ( GRIDWW_nCurrentRecord >= GRIDWW_nFirstRecordOnPage + subGridww_Recordsperpage( ) )
               {
                  GRIDWW_nEOF = 0;
               }
               GRIDWW_nCurrentRecord = (long)(GRIDWW_nCurrentRecord+1);
               AV40GXV5 = (int)(AV40GXV5+1);
            }
         }
      }

      protected void E11212( )
      {
         /* 'AddNew' Routine */
         if ( ! (0==AV9ApplicationId) )
         {
            context.wjLoc = formatLink("gamexamplerolepermissionselect.aspx") + "?" + UrlEncode("" +AV29RoleId) + "," + UrlEncode("" +AV9ApplicationId);
            context.wjLocDisableFrm = 1;
         }
         else
         {
            GX_msglist.addItem("You must select Application.");
         }
      }

      protected void E12212( )
      {
         /* 'Save' Routine */
         AV18GAMRole.load( AV29RoleId);
         /* Start For Each Line */
         nRC_GXsfl_49 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_49"), ",", "."));
         nGXsfl_49_fel_idx = 0;
         while ( nGXsfl_49_fel_idx < nRC_GXsfl_49 )
         {
            nGXsfl_49_fel_idx = (short)(((subGridww_Islastpage==1)&&(nGXsfl_49_fel_idx+1>subGridww_Recordsperpage( )) ? 1 : nGXsfl_49_fel_idx+1));
            sGXsfl_49_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_49_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_492( ) ;
            cmbavOld_accesstype.Name = cmbavOld_accesstype_Internalname;
            cmbavOld_accesstype.CurrentValue = cgiGet( cmbavOld_accesstype_Internalname);
            AV23old_AccessType = cgiGet( cmbavOld_accesstype_Internalname);
            AV24old_Inherited = StringUtil.StrToBool( cgiGet( chkavOld_inherited_Internalname));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAppid_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAppid_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAPPID");
               GX_FocusControl = edtavAppid_Internalname;
               wbErr = true;
               AV6AppId = 0;
            }
            else
            {
               AV6AppId = (long)(context.localUtil.CToN( cgiGet( edtavAppid_Internalname), ",", "."));
            }
            AV29RoleId = (long)(context.localUtil.CToN( cgiGet( edtavRoleid_Internalname), ",", "."));
            AV11BtnDlt = cgiGet( edtavBtndlt_Internalname);
            AV19Id = cgiGet( edtavId_Internalname);
            AV22Name = cgiGet( edtavName_Internalname);
            AV14Dsc = cgiGet( edtavDsc_Internalname);
            cmbavAccesstype.Name = cmbavAccesstype_Internalname;
            cmbavAccesstype.CurrentValue = cgiGet( cmbavAccesstype_Internalname);
            AV5AccessType = cgiGet( cmbavAccesstype_Internalname);
            AV20Inherited = StringUtil.StrToBool( cgiGet( chkavInherited_Internalname));
            CheckSecurityRow2149( sGXsfl_49_fel_idx) ;
            if ( GxWebError != 0 )
            {
               return  ;
            }
            if ( ( StringUtil.StrCmp(AV5AccessType, AV23old_AccessType) != 0 ) || ( AV20Inherited != AV24old_Inherited ) )
            {
               AV27PermissionUpd.gxTpr_Applicationid = AV9ApplicationId;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27PermissionUpd", AV27PermissionUpd);
               AV27PermissionUpd.gxTpr_Guid = AV19Id;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27PermissionUpd", AV27PermissionUpd);
               AV27PermissionUpd.gxTpr_Type = AV5AccessType;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27PermissionUpd", AV27PermissionUpd);
               AV27PermissionUpd.gxTpr_Inherited = AV20Inherited;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27PermissionUpd", AV27PermissionUpd);
               AV21isOK = AV18GAMRole.updatepermission(AV27PermissionUpd, out  AV16Errors);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21isOK", AV21isOK);
               if ( ! AV21isOK )
               {
                  AV43GXV6 = 1;
                  while ( AV43GXV6 <= AV16Errors.Count )
                  {
                     AV15Error = ((SdtGAMError)AV16Errors.Item(AV43GXV6));
                     GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV15Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
                     AV43GXV6 = (int)(AV43GXV6+1);
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
            }
            /* End For Each Line */
         }
         if ( nGXsfl_49_fel_idx == 0 )
         {
            nGXsfl_49_idx = 1;
            sGXsfl_49_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_49_idx), 4, 0)), 4, "0");
            SubsflControlProps_492( ) ;
         }
         nGXsfl_49_fel_idx = 1;
         if ( AV21isOK )
         {
            context.CommitDataStores( "GAMExampleWWRolePermissions");
            GX_msglist.addItem("Changes saved successfully.");
         }
         else
         {
            AV44GXV7 = 1;
            while ( AV44GXV7 <= AV16Errors.Count )
            {
               AV15Error = ((SdtGAMError)AV16Errors.Item(AV44GXV7));
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV15Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
               AV44GXV7 = (int)(AV44GXV7+1);
            }
         }
      }

      protected void E15212( )
      {
         /* Btndlt_Click Routine */
         AV18GAMRole.load( AV29RoleId);
         AV21isOK = AV18GAMRole.deletepermissionbyid(AV9ApplicationId, AV19Id, out  AV16Errors);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21isOK", AV21isOK);
         if ( AV21isOK )
         {
            context.CommitDataStores( "GAMExampleWWRolePermissions");
         }
         else
         {
            AV45GXV8 = 1;
            while ( AV45GXV8 <= AV16Errors.Count )
            {
               AV15Error = ((SdtGAMError)AV16Errors.Item(AV45GXV8));
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV15Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
               AV45GXV8 = (int)(AV45GXV8+1);
            }
         }
      }

      protected void wb_table1_3_212( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(500), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(350), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblpage_Internalname, tblTblpage_Internalname, "", "Table", 0, "center", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom;height:75px")+"\">") ;
            wb_table2_6_212( true) ;
         }
         else
         {
            wb_table2_6_212( false) ;
         }
         return  ;
      }

      protected void wb_table2_6_212e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\">") ;
            wb_table3_41_212( true) ;
         }
         else
         {
            wb_table3_41_212( false) ;
         }
         return  ;
      }

      protected void wb_table3_41_212e( bool wbgen )
      {
         if ( wbgen )
         {
            /*  Grid Control  */
            GridwwContainer.SetWrapped(nGXWrapped);
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridwwContainer"+"DivS\" data-gxgridid=\"49\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridww_Internalname, subGridww_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridww_Backcolorstyle == 0 )
               {
                  subGridww_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridww_Class) > 0 )
                  {
                     subGridww_Linesclass = subGridww_Class+"Title";
                  }
               }
               else
               {
                  subGridww_Titlebackstyle = 1;
                  if ( subGridww_Backcolorstyle == 1 )
                  {
                     subGridww_Titlebackcolor = subGridww_Allbackcolor;
                     if ( StringUtil.Len( subGridww_Class) > 0 )
                     {
                        subGridww_Linesclass = subGridww_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridww_Class) > 0 )
                     {
                        subGridww_Linesclass = subGridww_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "old_Access Type") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "old_Inherited") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "App Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Role Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Revoke") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(200), 4, 0))+"px"+" class=\""+subGridww_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Permission name") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(250), 4, 0))+"px"+" class=\""+subGridww_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Description") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Access Type") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Inherited") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridwwContainer.AddObjectProperty("GridName", "Gridww");
            }
            else
            {
               GridwwContainer.AddObjectProperty("GridName", "Gridww");
               GridwwContainer.AddObjectProperty("Class", "WorkWith");
               GridwwContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Backcolorstyle), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("CmpContext", "");
               GridwwContainer.AddObjectProperty("InMasterPage", "false");
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV23old_AccessType));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavOld_accesstype.Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV24old_Inherited));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavOld_inherited.Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6AppId), 12, 0, ".", "")));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAppid_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29RoleId), 12, 0, ".", "")));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRoleid_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", context.convertURL( AV11BtnDlt));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV19Id));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavId_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV22Name));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavName_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV14Dsc));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDsc_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV5AccessType));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV20Inherited));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Allowselection), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Selectioncolor), 9, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Allowhovering), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Hoveringcolor), 9, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Allowcollapsing), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 49 )
         {
            wbEnd = 0;
            nRC_GXsfl_49 = (short)(nGXsfl_49_idx-1);
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridwwContainer.AddObjectProperty("GRIDWW_nEOF", GRIDWW_nEOF);
               GridwwContainer.AddObjectProperty("GRIDWW_nFirstRecordOnPage", GRIDWW_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridwwContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridww", GridwwContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridwwContainerData", GridwwContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridwwContainerData"+"V", GridwwContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridwwContainerData"+"V"+"\" value='"+GridwwContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_212e( true) ;
         }
         else
         {
            wb_table1_3_212e( false) ;
         }
      }

      protected void wb_table3_41_212( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(95), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblbtn_Internalname, tblTblbtn_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:334px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnadd_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(49), 2, 0)+","+"null"+");", "Add Permission", bttBtnadd_Jsonclick, 5, "Add Permission", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ADDNEW\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleWWRolePermissions.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnsave_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(49), 2, 0)+","+"null"+");", "Save changes", bttBtnsave_Jsonclick, 5, "Save changes", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'SAVE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleWWRolePermissions.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;width:194px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnback_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(49), 2, 0)+","+"null"+");", "Back", bttBtnback_Jsonclick, 1, "Back", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleWWRolePermissions.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_212e( true) ;
         }
         else
         {
            wb_table3_41_212e( false) ;
         }
      }

      protected void wb_table2_6_212( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTbladd_Internalname, tblTbladd_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;height:5px;width:150px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbrole_Internalname, "Role:", "", "", lblTbrole_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWRolePermissions.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavCtlname_Internalname, StringUtil.RTrim( AV18GAMRole.gxTpr_Name), StringUtil.RTrim( context.localUtil.Format( AV18GAMRole.gxTpr_Name, "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlname_Jsonclick, 0, "Attribute", "font-family:'Arial'; font-size:9.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavCtlname_Enabled, 0, "text", "", 420, "px", 1, "row", 254, 0, 0, 0, 1, 0, -1, true, "", "", true, "HLP_GAMExampleWWRolePermissions.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;height:25px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbadd_Internalname, "Application:", "", "", lblTbadd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWRolePermissions.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_49_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavApplicationid, cmbavApplicationid_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9ApplicationId), 12, 0)), 1, cmbavApplicationid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_GAMExampleWWRolePermissions.htm");
            cmbavApplicationid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9ApplicationId), 12, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavApplicationid_Internalname, "Values", (String)(cmbavApplicationid.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;height:20px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname_Internalname, "Name", "", "", lblTbname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWRolePermissions.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_49_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilname_Internalname, StringUtil.RTrim( AV17FilName), StringUtil.RTrim( context.localUtil.Format( AV17FilName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilname_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 421, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleWWRolePermissions.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:27px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbaccesstype_Internalname, "Access type", "", "", lblTbaccesstype_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWRolePermissions.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_49_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavPermissionaccesstype, cmbavPermissionaccesstype_Internalname, StringUtil.RTrim( AV32PermissionAccessType), 1, cmbavPermissionaccesstype_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", true, "HLP_GAMExampleWWRolePermissions.htm");
            cmbavPermissionaccesstype.CurrentValue = StringUtil.RTrim( AV32PermissionAccessType);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPermissionaccesstype_Internalname, "Values", (String)(cmbavPermissionaccesstype.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:26px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbinherited_Internalname, "Is Inherited?", "", "", lblTbinherited_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWRolePermissions.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_49_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavBoolenfilter, cmbavBoolenfilter_Internalname, StringUtil.RTrim( AV31BoolenFilter), 1, cmbavBoolenfilter_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_GAMExampleWWRolePermissions.htm");
            cmbavBoolenfilter.CurrentValue = StringUtil.RTrim( AV31BoolenFilter);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavBoolenfilter_Internalname, "Values", (String)(cmbavBoolenfilter.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_6_212e( true) ;
         }
         else
         {
            wb_table2_6_212e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV29RoleId = Convert.ToInt64(getParm(obj,0));
         AV25pApplicationId = Convert.ToInt64(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25pApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25pApplicationId), 12, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA212( ) ;
         WS212( ) ;
         WE212( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249785");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823114484");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexamplewwrolepermissions.js", "?202042823114486");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_492( )
      {
         cmbavOld_accesstype_Internalname = "vOLD_ACCESSTYPE_"+sGXsfl_49_idx;
         chkavOld_inherited_Internalname = "vOLD_INHERITED_"+sGXsfl_49_idx;
         edtavAppid_Internalname = "vAPPID_"+sGXsfl_49_idx;
         edtavRoleid_Internalname = "vROLEID_"+sGXsfl_49_idx;
         edtavBtndlt_Internalname = "vBTNDLT_"+sGXsfl_49_idx;
         edtavId_Internalname = "vID_"+sGXsfl_49_idx;
         edtavName_Internalname = "vNAME_"+sGXsfl_49_idx;
         edtavDsc_Internalname = "vDSC_"+sGXsfl_49_idx;
         cmbavAccesstype_Internalname = "vACCESSTYPE_"+sGXsfl_49_idx;
         chkavInherited_Internalname = "vINHERITED_"+sGXsfl_49_idx;
      }

      protected void SubsflControlProps_fel_492( )
      {
         cmbavOld_accesstype_Internalname = "vOLD_ACCESSTYPE_"+sGXsfl_49_fel_idx;
         chkavOld_inherited_Internalname = "vOLD_INHERITED_"+sGXsfl_49_fel_idx;
         edtavAppid_Internalname = "vAPPID_"+sGXsfl_49_fel_idx;
         edtavRoleid_Internalname = "vROLEID_"+sGXsfl_49_fel_idx;
         edtavBtndlt_Internalname = "vBTNDLT_"+sGXsfl_49_fel_idx;
         edtavId_Internalname = "vID_"+sGXsfl_49_fel_idx;
         edtavName_Internalname = "vNAME_"+sGXsfl_49_fel_idx;
         edtavDsc_Internalname = "vDSC_"+sGXsfl_49_fel_idx;
         cmbavAccesstype_Internalname = "vACCESSTYPE_"+sGXsfl_49_fel_idx;
         chkavInherited_Internalname = "vINHERITED_"+sGXsfl_49_fel_idx;
      }

      protected void sendrow_492( )
      {
         SubsflControlProps_492( ) ;
         WB210( ) ;
         if ( ( 15 * 1 == 0 ) || ( nGXsfl_49_idx <= subGridww_Recordsperpage( ) * 1 ) )
         {
            GridwwRow = GXWebRow.GetNew(context,GridwwContainer);
            if ( subGridww_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridww_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
               {
                  subGridww_Linesclass = subGridww_Class+"Odd";
               }
            }
            else if ( subGridww_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridww_Backstyle = 0;
               subGridww_Backcolor = subGridww_Allbackcolor;
               if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
               {
                  subGridww_Linesclass = subGridww_Class+"Uniform";
               }
            }
            else if ( subGridww_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridww_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
               {
                  subGridww_Linesclass = subGridww_Class+"Odd";
               }
               subGridww_Backcolor = (int)(0x0);
            }
            else if ( subGridww_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridww_Backstyle = 1;
               if ( ((int)((nGXsfl_49_idx) % (2))) == 0 )
               {
                  subGridww_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
                  {
                     subGridww_Linesclass = subGridww_Class+"Even";
                  }
               }
               else
               {
                  subGridww_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
                  {
                     subGridww_Linesclass = subGridww_Class+"Odd";
                  }
               }
            }
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridww_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_49_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            TempTags = " " + ((cmbavOld_accesstype.Enabled!=0)&&(cmbavOld_accesstype.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 50,'',false,'"+sGXsfl_49_idx+"',49)\"" : " ");
            if ( ( nGXsfl_49_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vOLD_ACCESSTYPE_" + sGXsfl_49_idx;
               cmbavOld_accesstype.Name = GXCCtl;
               cmbavOld_accesstype.WebTags = "";
               cmbavOld_accesstype.addItem("A", "Allow", 0);
               cmbavOld_accesstype.addItem("D", "Deny", 0);
               cmbavOld_accesstype.addItem("R", "Restricted", 0);
               if ( cmbavOld_accesstype.ItemCount > 0 )
               {
                  AV23old_AccessType = cmbavOld_accesstype.getValidValue(AV23old_AccessType);
               }
            }
            /* ComboBox */
            GridwwRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavOld_accesstype,(String)cmbavOld_accesstype_Internalname,StringUtil.RTrim( AV23old_AccessType),(short)1,(String)cmbavOld_accesstype_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)0,cmbavOld_accesstype.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavOld_accesstype.Enabled!=0)&&(cmbavOld_accesstype.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavOld_accesstype.Enabled!=0)&&(cmbavOld_accesstype.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,50);\"" : " "),(String)"",(bool)true});
            cmbavOld_accesstype.CurrentValue = StringUtil.RTrim( AV23old_AccessType);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOld_accesstype_Internalname, "Values", (String)(cmbavOld_accesstype.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavOld_inherited.Enabled!=0)&&(chkavOld_inherited.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 51,'',false,'"+sGXsfl_49_idx+"',49)\"" : " ");
            ClassString = "Attribute";
            StyleString = "";
            GridwwRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavOld_inherited_Internalname,StringUtil.BoolToStr( AV24old_Inherited),(String)"",(String)"",(short)0,chkavOld_inherited.Enabled,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavOld_inherited.Enabled!=0)&&(chkavOld_inherited.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(51, this, 'true', 'false');gx.evt.onchange(this);\" " : " ")+((chkavOld_inherited.Enabled!=0)&&(chkavOld_inherited.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,51);\"" : " ")});
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavAppid_Enabled!=0)&&(edtavAppid_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 52,'',false,'"+sGXsfl_49_idx+"',49)\"" : " ");
            ROClassString = "Attribute";
            GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAppid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6AppId), 12, 0, ",", "")),((edtavAppid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6AppId), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV6AppId), "ZZZZZZZZZZZ9")),TempTags+((edtavAppid_Enabled!=0)&&(edtavAppid_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavAppid_Enabled!=0)&&(edtavAppid_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,52);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAppid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavAppid_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)49,(short)1,(short)-1,(short)0,(bool)true,(String)"GAMKeyNumLong",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRoleid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29RoleId), 12, 0, ",", "")),((edtavRoleid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29RoleId), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV29RoleId), "ZZZZZZZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavRoleid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavRoleid_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)49,(short)1,(short)-1,(short)0,(bool)true,(String)"GAMKeyNumLong",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtndlt_Enabled!=0)&&(edtavBtndlt_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 54,'',false,'',49)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV11BtnDlt_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV11BtnDlt))&&String.IsNullOrEmpty(StringUtil.RTrim( AV41Btndlt_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV11BtnDlt)));
            GridwwRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtndlt_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV11BtnDlt)) ? AV41Btndlt_GXI : context.PathToRelativeUrl( AV11BtnDlt)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtndlt_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVBTNDLT.CLICK."+sGXsfl_49_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV11BtnDlt_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavId_Enabled!=0)&&(edtavId_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 55,'',false,'"+sGXsfl_49_idx+"',49)\"" : " ");
            ROClassString = "Attribute";
            GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavId_Internalname,StringUtil.RTrim( AV19Id),(String)"",TempTags+((edtavId_Enabled!=0)&&(edtavId_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavId_Enabled!=0)&&(edtavId_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,55);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavId_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)49,(short)1,(short)-1,(short)0,(bool)true,(String)"GAMGUID",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavName_Enabled!=0)&&(edtavName_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 56,'',false,'"+sGXsfl_49_idx+"',49)\"" : " ");
            ROClassString = "Attribute";
            GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavName_Internalname,StringUtil.RTrim( AV22Name),(String)"",TempTags+((edtavName_Enabled!=0)&&(edtavName_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavName_Enabled!=0)&&(edtavName_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,56);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavName_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavName_Enabled,(short)0,(String)"text",(String)"",(short)200,(String)"px",(short)17,(String)"px",(short)120,(short)0,(short)0,(short)49,(short)1,(short)-1,(short)-1,(bool)true,(String)"GAMDescriptionMedium",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavDsc_Enabled!=0)&&(edtavDsc_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 57,'',false,'"+sGXsfl_49_idx+"',49)\"" : " ");
            ROClassString = "Attribute";
            GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDsc_Internalname,StringUtil.RTrim( AV14Dsc),(String)"",TempTags+((edtavDsc_Enabled!=0)&&(edtavDsc_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavDsc_Enabled!=0)&&(edtavDsc_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,57);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDsc_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavDsc_Enabled,(short)0,(String)"text",(String)"",(short)250,(String)"px",(short)17,(String)"px",(short)254,(short)0,(short)0,(short)49,(short)1,(short)-1,(short)-1,(bool)true,(String)"GAMDescriptionLong",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            TempTags = " " + ((cmbavAccesstype.Enabled!=0)&&(cmbavAccesstype.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 58,'',false,'"+sGXsfl_49_idx+"',49)\"" : " ");
            if ( ( nGXsfl_49_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vACCESSTYPE_" + sGXsfl_49_idx;
               cmbavAccesstype.Name = GXCCtl;
               cmbavAccesstype.WebTags = "";
               cmbavAccesstype.addItem("A", "Allow", 0);
               cmbavAccesstype.addItem("D", "Deny", 0);
               cmbavAccesstype.addItem("R", "Restricted", 0);
               if ( cmbavAccesstype.ItemCount > 0 )
               {
                  AV5AccessType = cmbavAccesstype.getValidValue(AV5AccessType);
               }
            }
            /* ComboBox */
            GridwwRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavAccesstype,(String)cmbavAccesstype_Internalname,StringUtil.RTrim( AV5AccessType),(short)1,(String)cmbavAccesstype_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)1,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavAccesstype.Enabled!=0)&&(cmbavAccesstype.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavAccesstype.Enabled!=0)&&(cmbavAccesstype.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,58);\"" : " "),(String)"",(bool)true});
            cmbavAccesstype.CurrentValue = StringUtil.RTrim( AV5AccessType);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAccesstype_Internalname, "Values", (String)(cmbavAccesstype.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavInherited.Enabled!=0)&&(chkavInherited.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 59,'',false,'"+sGXsfl_49_idx+"',49)\"" : " ");
            ClassString = "Attribute";
            StyleString = "";
            GridwwRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavInherited_Internalname,StringUtil.BoolToStr( AV20Inherited),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavInherited.Enabled!=0)&&(chkavInherited.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(59, this, 'true', 'false');gx.evt.onchange(this);\" " : " ")+((chkavInherited.Enabled!=0)&&(chkavInherited.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,59);\"" : " ")});
            GridwwContainer.AddRow(GridwwRow);
            sendsecurityrow_492( ) ;
            nGXsfl_49_idx = (short)(((subGridww_Islastpage==1)&&(nGXsfl_49_idx+1>subGridww_Recordsperpage( )) ? 1 : nGXsfl_49_idx+1));
            sGXsfl_49_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_49_idx), 4, 0)), 4, "0");
            SubsflControlProps_492( ) ;
         }
         /* End function sendrow_492 */
      }

      protected void sendsecurityrow_492( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         /* End function sendsecurityrow_492 */
      }

      protected void init_default_properties( )
      {
         lblTbrole_Internalname = "TBROLE";
         edtavCtlname_Internalname = "CTLNAME";
         lblTbadd_Internalname = "TBADD";
         cmbavApplicationid_Internalname = "vAPPLICATIONID";
         lblTbname_Internalname = "TBNAME";
         edtavFilname_Internalname = "vFILNAME";
         lblTbaccesstype_Internalname = "TBACCESSTYPE";
         cmbavPermissionaccesstype_Internalname = "vPERMISSIONACCESSTYPE";
         lblTbinherited_Internalname = "TBINHERITED";
         cmbavBoolenfilter_Internalname = "vBOOLENFILTER";
         tblTbladd_Internalname = "TBLADD";
         bttBtnadd_Internalname = "BTNADD";
         bttBtnsave_Internalname = "BTNSAVE";
         bttBtnback_Internalname = "BTNBACK";
         tblTblbtn_Internalname = "TBLBTN";
         tblTblpage_Internalname = "TBLPAGE";
         Form.Internalname = "FORM";
         subGridww_Internalname = "GRIDWW";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         chkavInherited.Visible = -1;
         chkavInherited.Enabled = 1;
         cmbavAccesstype_Jsonclick = "";
         cmbavAccesstype.Visible = -1;
         cmbavAccesstype.Enabled = 1;
         edtavDsc_Jsonclick = "";
         edtavDsc_Visible = -1;
         edtavName_Jsonclick = "";
         edtavName_Visible = -1;
         edtavId_Jsonclick = "";
         edtavId_Visible = 0;
         edtavBtndlt_Jsonclick = "";
         edtavBtndlt_Visible = -1;
         edtavBtndlt_Enabled = 1;
         edtavRoleid_Jsonclick = "";
         edtavAppid_Jsonclick = "";
         edtavAppid_Visible = 0;
         chkavOld_inherited.Visible = 0;
         cmbavOld_accesstype_Jsonclick = "";
         cmbavOld_accesstype.Visible = 0;
         cmbavBoolenfilter_Jsonclick = "";
         cmbavPermissionaccesstype_Jsonclick = "";
         edtavFilname_Jsonclick = "";
         cmbavApplicationid_Jsonclick = "";
         edtavCtlname_Jsonclick = "";
         edtavCtlname_Enabled = 0;
         subGridww_Allowcollapsing = 0;
         subGridww_Allowselection = 0;
         edtavDsc_Enabled = 1;
         edtavName_Enabled = 1;
         edtavId_Enabled = 1;
         edtavRoleid_Enabled = 0;
         edtavAppid_Enabled = 1;
         chkavOld_inherited.Enabled = 1;
         cmbavOld_accesstype.Enabled = 1;
         subGridww_Class = "WorkWith";
         subGridww_Backcolorstyle = 0;
         edtavCtlname_Enabled = -1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Role`s permissions";
         subGridww_Rows = 15;
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV17FilName = "";
         AV32PermissionAccessType = "";
         AV31BoolenFilter = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         cmbavOld_accesstype_Internalname = "";
         AV23old_AccessType = "";
         chkavOld_inherited_Internalname = "";
         edtavAppid_Internalname = "";
         edtavRoleid_Internalname = "";
         AV11BtnDlt = "";
         edtavBtndlt_Internalname = "";
         AV19Id = "";
         edtavId_Internalname = "";
         AV22Name = "";
         edtavName_Internalname = "";
         AV14Dsc = "";
         edtavDsc_Internalname = "";
         cmbavAccesstype_Internalname = "";
         AV5AccessType = "";
         chkavInherited_Internalname = "";
         GXCCtl = "";
         GridwwContainer = new GXWebGrid( context);
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         AV18GAMRole = new SdtGAMRole(context);
         AV37GXV2 = new GxExternalCollection( context, "SdtGAMApplication", "GeneXus.Programs");
         AV8ApplicationFilter = new SdtGAMApplicationFilter(context);
         AV16Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV7Application = new SdtGAMApplication(context);
         AV33RolePermissionFilter = new SdtGAMPermissionFilter(context);
         AV39GXV4 = new GxExternalCollection( context, "SdtGAMPermission", "GeneXus.Programs");
         AV26Permission = new SdtGAMPermission(context);
         AV41Btndlt_GXI = "";
         AV27PermissionUpd = new SdtGAMPermission(context);
         AV15Error = new SdtGAMError(context);
         sStyleString = "";
         subGridww_Linesclass = "";
         GridwwColumn = new GXWebColumn();
         TempTags = "";
         bttBtnadd_Jsonclick = "";
         bttBtnsave_Jsonclick = "";
         bttBtnback_Jsonclick = "";
         lblTbrole_Jsonclick = "";
         lblTbadd_Jsonclick = "";
         lblTbname_Jsonclick = "";
         lblTbaccesstype_Jsonclick = "";
         lblTbinherited_Jsonclick = "";
         GridwwRow = new GXWebRow();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexamplewwrolepermissions__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
         cmbavOld_accesstype.Enabled = 0;
         chkavOld_inherited.Enabled = 0;
         edtavAppid_Enabled = 0;
         edtavRoleid_Enabled = 0;
         edtavId_Enabled = 0;
         edtavName_Enabled = 0;
         edtavDsc_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_49 ;
      private short nGXsfl_49_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRIDWW_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_49_Refreshing=0 ;
      private short subGridww_Backcolorstyle ;
      private short nGXsfl_49_fel_idx=1 ;
      private short subGridww_Titlebackstyle ;
      private short subGridww_Allowselection ;
      private short subGridww_Allowhovering ;
      private short subGridww_Allowcollapsing ;
      private short subGridww_Collapsed ;
      private short nGXWrapped ;
      private short subGridww_Backstyle ;
      private int subGridww_Rows ;
      private int subGridww_Islastpage ;
      private int edtavCtlname_Enabled ;
      private int edtavAppid_Enabled ;
      private int edtavRoleid_Enabled ;
      private int edtavId_Enabled ;
      private int edtavName_Enabled ;
      private int edtavDsc_Enabled ;
      private int GRIDWW_nGridOutOfScope ;
      private int AV38GXV3 ;
      private int AV40GXV5 ;
      private int AV43GXV6 ;
      private int AV44GXV7 ;
      private int AV45GXV8 ;
      private int subGridww_Titlebackcolor ;
      private int subGridww_Allbackcolor ;
      private int subGridww_Selectioncolor ;
      private int subGridww_Hoveringcolor ;
      private int idxLst ;
      private int subGridww_Backcolor ;
      private int edtavAppid_Visible ;
      private int edtavBtndlt_Enabled ;
      private int edtavBtndlt_Visible ;
      private int edtavId_Visible ;
      private int edtavName_Visible ;
      private int edtavDsc_Visible ;
      private long AV29RoleId ;
      private long AV25pApplicationId ;
      private long wcpOAV29RoleId ;
      private long wcpOAV25pApplicationId ;
      private long AV9ApplicationId ;
      private long GRIDWW_nFirstRecordOnPage ;
      private long AV6AppId ;
      private long GRIDWW_nCurrentRecord ;
      private long GRIDWW_nRecordCount ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_49_idx="0001" ;
      private String AV17FilName ;
      private String AV32PermissionAccessType ;
      private String AV31BoolenFilter ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOld_accesstype_Internalname ;
      private String AV23old_AccessType ;
      private String chkavOld_inherited_Internalname ;
      private String edtavAppid_Internalname ;
      private String edtavRoleid_Internalname ;
      private String edtavBtndlt_Internalname ;
      private String AV19Id ;
      private String edtavId_Internalname ;
      private String AV22Name ;
      private String edtavName_Internalname ;
      private String AV14Dsc ;
      private String edtavDsc_Internalname ;
      private String cmbavAccesstype_Internalname ;
      private String AV5AccessType ;
      private String chkavInherited_Internalname ;
      private String GXCCtl ;
      private String edtavCtlname_Internalname ;
      private String cmbavApplicationid_Internalname ;
      private String edtavFilname_Internalname ;
      private String cmbavPermissionaccesstype_Internalname ;
      private String cmbavBoolenfilter_Internalname ;
      private String sGXsfl_49_fel_idx="0001" ;
      private String sStyleString ;
      private String tblTblpage_Internalname ;
      private String subGridww_Internalname ;
      private String subGridww_Class ;
      private String subGridww_Linesclass ;
      private String tblTblbtn_Internalname ;
      private String TempTags ;
      private String bttBtnadd_Internalname ;
      private String bttBtnadd_Jsonclick ;
      private String bttBtnsave_Internalname ;
      private String bttBtnsave_Jsonclick ;
      private String bttBtnback_Internalname ;
      private String bttBtnback_Jsonclick ;
      private String tblTbladd_Internalname ;
      private String lblTbrole_Internalname ;
      private String lblTbrole_Jsonclick ;
      private String edtavCtlname_Jsonclick ;
      private String lblTbadd_Internalname ;
      private String lblTbadd_Jsonclick ;
      private String cmbavApplicationid_Jsonclick ;
      private String lblTbname_Internalname ;
      private String lblTbname_Jsonclick ;
      private String edtavFilname_Jsonclick ;
      private String lblTbaccesstype_Internalname ;
      private String lblTbaccesstype_Jsonclick ;
      private String cmbavPermissionaccesstype_Jsonclick ;
      private String lblTbinherited_Internalname ;
      private String lblTbinherited_Jsonclick ;
      private String cmbavBoolenfilter_Jsonclick ;
      private String cmbavOld_accesstype_Jsonclick ;
      private String ROClassString ;
      private String edtavAppid_Jsonclick ;
      private String edtavRoleid_Jsonclick ;
      private String edtavBtndlt_Jsonclick ;
      private String edtavId_Jsonclick ;
      private String edtavName_Jsonclick ;
      private String edtavDsc_Jsonclick ;
      private String cmbavAccesstype_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV24old_Inherited ;
      private bool AV20Inherited ;
      private bool returnInSub ;
      private bool AV21isOK ;
      private bool AV11BtnDlt_IsBlob ;
      private String AV41Btndlt_GXI ;
      private String AV11BtnDlt ;
      private GXWebGrid GridwwContainer ;
      private GXWebRow GridwwRow ;
      private GXWebColumn GridwwColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private long aP0_RoleId ;
      private long aP1_pApplicationId ;
      private GXCombobox cmbavApplicationid ;
      private GXCombobox cmbavPermissionaccesstype ;
      private GXCombobox cmbavBoolenfilter ;
      private GXCombobox cmbavOld_accesstype ;
      private GXCheckbox chkavOld_inherited ;
      private GXCombobox cmbavAccesstype ;
      private GXCheckbox chkavInherited ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMApplication ))]
      private IGxCollection AV37GXV2 ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV16Errors ;
      [ObjectCollection(ItemType=typeof( SdtGAMPermission ))]
      private IGxCollection AV39GXV4 ;
      private GXWebForm Form ;
      private SdtGAMApplication AV7Application ;
      private SdtGAMError AV15Error ;
      private SdtGAMRole AV18GAMRole ;
      private SdtGAMPermission AV26Permission ;
      private SdtGAMPermission AV27PermissionUpd ;
      private SdtGAMPermissionFilter AV33RolePermissionFilter ;
      private SdtGAMApplicationFilter AV8ApplicationFilter ;
   }

   public class gamexamplewwrolepermissions__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
