/*
               File: PRC_Email_Get_GUID_by_Key
        Description: PRC_Email_Get_GUID_by_Key
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:41.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_email_get_guid_by_key : GXProcedure
   {
      public prc_email_get_guid_by_key( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_email_get_guid_by_key( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Email_Key ,
                           out Guid aP1_Email_Guid )
      {
         this.AV9Email_Key = aP0_Email_Key;
         this.AV8Email_Guid = System.Guid.Empty ;
         initialize();
         executePrivate();
         aP1_Email_Guid=this.AV8Email_Guid;
      }

      public Guid executeUdp( String aP0_Email_Key )
      {
         this.AV9Email_Key = aP0_Email_Key;
         this.AV8Email_Guid = System.Guid.Empty ;
         initialize();
         executePrivate();
         aP1_Email_Guid=this.AV8Email_Guid;
         return AV8Email_Guid ;
      }

      public void executeSubmit( String aP0_Email_Key ,
                                 out Guid aP1_Email_Guid )
      {
         prc_email_get_guid_by_key objprc_email_get_guid_by_key;
         objprc_email_get_guid_by_key = new prc_email_get_guid_by_key();
         objprc_email_get_guid_by_key.AV9Email_Key = aP0_Email_Key;
         objprc_email_get_guid_by_key.AV8Email_Guid = System.Guid.Empty ;
         objprc_email_get_guid_by_key.context.SetSubmitInitialConfig(context);
         objprc_email_get_guid_by_key.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_email_get_guid_by_key);
         aP1_Email_Guid=this.AV8Email_Guid;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_email_get_guid_by_key)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00C22 */
         pr_default.execute(0, new Object[] {AV9Email_Key});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1672Email_Key = P00C22_A1672Email_Key[0];
            A1665Email_Guid = (Guid)((Guid)(P00C22_A1665Email_Guid[0]));
            AV8Email_Guid = (Guid)(A1665Email_Guid);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00C22_A1672Email_Key = new String[] {""} ;
         P00C22_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         A1672Email_Key = "";
         A1665Email_Guid = (Guid)(System.Guid.Empty);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_email_get_guid_by_key__default(),
            new Object[][] {
                new Object[] {
               P00C22_A1672Email_Key, P00C22_A1665Email_Guid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String scmdbuf ;
      private String AV9Email_Key ;
      private String A1672Email_Key ;
      private Guid AV8Email_Guid ;
      private Guid A1665Email_Guid ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00C22_A1672Email_Key ;
      private Guid[] P00C22_A1665Email_Guid ;
      private Guid aP1_Email_Guid ;
   }

   public class prc_email_get_guid_by_key__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00C22 ;
          prmP00C22 = new Object[] {
          new Object[] {"@AV9Email_Key",SqlDbType.VarChar,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00C22", "SELECT TOP 1 [Email_Key], [Email_Guid] FROM [Email] WITH (NOLOCK) WHERE [Email_Key] = @AV9Email_Key ORDER BY [Email_Guid] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00C22,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
