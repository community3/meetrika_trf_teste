/*
               File: WWAtributos
        Description:  Atributos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:42:16.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwatributos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwatributos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwatributos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         dynavSistema_areatrabalhocod = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbAtributos_TipoDados = new GXCombobox();
         chkAtributos_Ativo = new GXCheckbox();
         chkAtributos_PK = new GXCheckbox();
         chkAtributos_FK = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSISTEMA_AREATRABALHOCOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSISTEMA_AREATRABALHOCOD4C2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_79 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_79_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_79_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV60Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Atributos_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Atributos_Nome1", AV17Atributos_Nome1);
               AV61Atributos_TabelaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Atributos_TabelaNom1", AV61Atributos_TabelaNom1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23Atributos_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Atributos_Nome2", AV23Atributos_Nome2);
               AV62Atributos_TabelaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Atributos_TabelaNom2", AV62Atributos_TabelaNom2);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV67TFAtributos_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFAtributos_Nome", AV67TFAtributos_Nome);
               AV68TFAtributos_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFAtributos_Nome_Sel", AV68TFAtributos_Nome_Sel);
               AV75TFAtributos_Detalhes = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFAtributos_Detalhes", AV75TFAtributos_Detalhes);
               AV76TFAtributos_Detalhes_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFAtributos_Detalhes_Sel", AV76TFAtributos_Detalhes_Sel);
               AV79TFAtributos_TabelaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFAtributos_TabelaNom", AV79TFAtributos_TabelaNom);
               AV80TFAtributos_TabelaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAtributos_TabelaNom_Sel", AV80TFAtributos_TabelaNom_Sel);
               AV83TFAtributos_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAtributos_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFAtributos_Ativo_Sel), 1, 0));
               AV86TFAtributos_PK_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFAtributos_PK_Sel", StringUtil.Str( (decimal)(AV86TFAtributos_PK_Sel), 1, 0));
               AV89TFAtributos_FK_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFAtributos_FK_Sel", StringUtil.Str( (decimal)(AV89TFAtributos_FK_Sel), 1, 0));
               AV69ddo_Atributos_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Atributos_NomeTitleControlIdToReplace", AV69ddo_Atributos_NomeTitleControlIdToReplace);
               AV73ddo_Atributos_TipoDadosTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Atributos_TipoDadosTitleControlIdToReplace", AV73ddo_Atributos_TipoDadosTitleControlIdToReplace);
               AV77ddo_Atributos_DetalhesTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Atributos_DetalhesTitleControlIdToReplace", AV77ddo_Atributos_DetalhesTitleControlIdToReplace);
               AV81ddo_Atributos_TabelaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Atributos_TabelaNomTitleControlIdToReplace", AV81ddo_Atributos_TabelaNomTitleControlIdToReplace);
               AV84ddo_Atributos_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Atributos_AtivoTitleControlIdToReplace", AV84ddo_Atributos_AtivoTitleControlIdToReplace);
               AV87ddo_Atributos_PKTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_Atributos_PKTitleControlIdToReplace", AV87ddo_Atributos_PKTitleControlIdToReplace);
               AV90ddo_Atributos_FKTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ddo_Atributos_FKTitleControlIdToReplace", AV90ddo_Atributos_FKTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV72TFAtributos_TipoDados_Sels);
               AV134Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV33DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
               AV32DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
               A176Atributos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV63Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A356Atributos_TabelaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV60Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Atributos_Nome1, AV61Atributos_TabelaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Atributos_Nome2, AV62Atributos_TabelaNom2, AV20DynamicFiltersEnabled2, AV67TFAtributos_Nome, AV68TFAtributos_Nome_Sel, AV75TFAtributos_Detalhes, AV76TFAtributos_Detalhes_Sel, AV79TFAtributos_TabelaNom, AV80TFAtributos_TabelaNom_Sel, AV83TFAtributos_Ativo_Sel, AV86TFAtributos_PK_Sel, AV89TFAtributos_FK_Sel, AV69ddo_Atributos_NomeTitleControlIdToReplace, AV73ddo_Atributos_TipoDadosTitleControlIdToReplace, AV77ddo_Atributos_DetalhesTitleControlIdToReplace, AV81ddo_Atributos_TabelaNomTitleControlIdToReplace, AV84ddo_Atributos_AtivoTitleControlIdToReplace, AV87ddo_Atributos_PKTitleControlIdToReplace, AV90ddo_Atributos_FKTitleControlIdToReplace, AV72TFAtributos_TipoDados_Sels, AV134Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A176Atributos_Codigo, AV63Sistema_Codigo, A356Atributos_TabelaCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA4C2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START4C2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020548421723");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwatributos.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vATRIBUTOS_NOME1", StringUtil.RTrim( AV17Atributos_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vATRIBUTOS_TABELANOM1", StringUtil.RTrim( AV61Atributos_TabelaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vATRIBUTOS_NOME2", StringUtil.RTrim( AV23Atributos_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vATRIBUTOS_TABELANOM2", StringUtil.RTrim( AV62Atributos_TabelaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFATRIBUTOS_NOME", StringUtil.RTrim( AV67TFAtributos_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFATRIBUTOS_NOME_SEL", StringUtil.RTrim( AV68TFAtributos_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFATRIBUTOS_DETALHES", StringUtil.RTrim( AV75TFAtributos_Detalhes));
         GxWebStd.gx_hidden_field( context, "GXH_vTFATRIBUTOS_DETALHES_SEL", StringUtil.RTrim( AV76TFAtributos_Detalhes_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFATRIBUTOS_TABELANOM", StringUtil.RTrim( AV79TFAtributos_TabelaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFATRIBUTOS_TABELANOM_SEL", StringUtil.RTrim( AV80TFAtributos_TabelaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFATRIBUTOS_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83TFAtributos_Ativo_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFATRIBUTOS_PK_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86TFAtributos_PK_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFATRIBUTOS_FK_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV89TFAtributos_FK_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_79", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_79), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV93GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV94GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV91DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV91DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATRIBUTOS_NOMETITLEFILTERDATA", AV66Atributos_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATRIBUTOS_NOMETITLEFILTERDATA", AV66Atributos_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATRIBUTOS_TIPODADOSTITLEFILTERDATA", AV70Atributos_TipoDadosTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATRIBUTOS_TIPODADOSTITLEFILTERDATA", AV70Atributos_TipoDadosTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATRIBUTOS_DETALHESTITLEFILTERDATA", AV74Atributos_DetalhesTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATRIBUTOS_DETALHESTITLEFILTERDATA", AV74Atributos_DetalhesTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATRIBUTOS_TABELANOMTITLEFILTERDATA", AV78Atributos_TabelaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATRIBUTOS_TABELANOMTITLEFILTERDATA", AV78Atributos_TabelaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATRIBUTOS_ATIVOTITLEFILTERDATA", AV82Atributos_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATRIBUTOS_ATIVOTITLEFILTERDATA", AV82Atributos_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATRIBUTOS_PKTITLEFILTERDATA", AV85Atributos_PKTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATRIBUTOS_PKTITLEFILTERDATA", AV85Atributos_PKTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATRIBUTOS_FKTITLEFILTERDATA", AV88Atributos_FKTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATRIBUTOS_FKTITLEFILTERDATA", AV88Atributos_FKTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFATRIBUTOS_TIPODADOS_SELS", AV72TFAtributos_TipoDados_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFATRIBUTOS_TIPODADOS_SELS", AV72TFAtributos_TipoDados_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV134Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_TABELACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Caption", StringUtil.RTrim( Ddo_atributos_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Tooltip", StringUtil.RTrim( Ddo_atributos_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Cls", StringUtil.RTrim( Ddo_atributos_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_atributos_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_atributos_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_atributos_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_atributos_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_atributos_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_atributos_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Sortedstatus", StringUtil.RTrim( Ddo_atributos_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Includefilter", StringUtil.BoolToStr( Ddo_atributos_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Filtertype", StringUtil.RTrim( Ddo_atributos_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_atributos_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_atributos_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Datalisttype", StringUtil.RTrim( Ddo_atributos_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_atributos_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Datalistproc", StringUtil.RTrim( Ddo_atributos_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_atributos_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Sortasc", StringUtil.RTrim( Ddo_atributos_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Sortdsc", StringUtil.RTrim( Ddo_atributos_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Loadingdata", StringUtil.RTrim( Ddo_atributos_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Cleanfilter", StringUtil.RTrim( Ddo_atributos_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_atributos_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Rangefilterto", StringUtil.RTrim( Ddo_atributos_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Noresultsfound", StringUtil.RTrim( Ddo_atributos_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_atributos_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Caption", StringUtil.RTrim( Ddo_atributos_tipodados_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Tooltip", StringUtil.RTrim( Ddo_atributos_tipodados_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Cls", StringUtil.RTrim( Ddo_atributos_tipodados_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Selectedvalue_set", StringUtil.RTrim( Ddo_atributos_tipodados_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Dropdownoptionstype", StringUtil.RTrim( Ddo_atributos_tipodados_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_atributos_tipodados_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Includesortasc", StringUtil.BoolToStr( Ddo_atributos_tipodados_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Includesortdsc", StringUtil.BoolToStr( Ddo_atributos_tipodados_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Sortedstatus", StringUtil.RTrim( Ddo_atributos_tipodados_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Includefilter", StringUtil.BoolToStr( Ddo_atributos_tipodados_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Filterisrange", StringUtil.BoolToStr( Ddo_atributos_tipodados_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Includedatalist", StringUtil.BoolToStr( Ddo_atributos_tipodados_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Datalisttype", StringUtil.RTrim( Ddo_atributos_tipodados_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_atributos_tipodados_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Datalistfixedvalues", StringUtil.RTrim( Ddo_atributos_tipodados_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_atributos_tipodados_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Sortasc", StringUtil.RTrim( Ddo_atributos_tipodados_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Sortdsc", StringUtil.RTrim( Ddo_atributos_tipodados_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Loadingdata", StringUtil.RTrim( Ddo_atributos_tipodados_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Cleanfilter", StringUtil.RTrim( Ddo_atributos_tipodados_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Rangefilterfrom", StringUtil.RTrim( Ddo_atributos_tipodados_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Rangefilterto", StringUtil.RTrim( Ddo_atributos_tipodados_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Noresultsfound", StringUtil.RTrim( Ddo_atributos_tipodados_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Searchbuttontext", StringUtil.RTrim( Ddo_atributos_tipodados_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Caption", StringUtil.RTrim( Ddo_atributos_detalhes_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Tooltip", StringUtil.RTrim( Ddo_atributos_detalhes_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Cls", StringUtil.RTrim( Ddo_atributos_detalhes_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Filteredtext_set", StringUtil.RTrim( Ddo_atributos_detalhes_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Selectedvalue_set", StringUtil.RTrim( Ddo_atributos_detalhes_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Dropdownoptionstype", StringUtil.RTrim( Ddo_atributos_detalhes_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_atributos_detalhes_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Includesortasc", StringUtil.BoolToStr( Ddo_atributos_detalhes_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Includesortdsc", StringUtil.BoolToStr( Ddo_atributos_detalhes_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Sortedstatus", StringUtil.RTrim( Ddo_atributos_detalhes_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Includefilter", StringUtil.BoolToStr( Ddo_atributos_detalhes_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Filtertype", StringUtil.RTrim( Ddo_atributos_detalhes_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Filterisrange", StringUtil.BoolToStr( Ddo_atributos_detalhes_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Includedatalist", StringUtil.BoolToStr( Ddo_atributos_detalhes_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Datalisttype", StringUtil.RTrim( Ddo_atributos_detalhes_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Datalistfixedvalues", StringUtil.RTrim( Ddo_atributos_detalhes_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Datalistproc", StringUtil.RTrim( Ddo_atributos_detalhes_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_atributos_detalhes_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Sortasc", StringUtil.RTrim( Ddo_atributos_detalhes_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Sortdsc", StringUtil.RTrim( Ddo_atributos_detalhes_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Loadingdata", StringUtil.RTrim( Ddo_atributos_detalhes_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Cleanfilter", StringUtil.RTrim( Ddo_atributos_detalhes_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Rangefilterfrom", StringUtil.RTrim( Ddo_atributos_detalhes_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Rangefilterto", StringUtil.RTrim( Ddo_atributos_detalhes_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Noresultsfound", StringUtil.RTrim( Ddo_atributos_detalhes_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Searchbuttontext", StringUtil.RTrim( Ddo_atributos_detalhes_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Caption", StringUtil.RTrim( Ddo_atributos_tabelanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Tooltip", StringUtil.RTrim( Ddo_atributos_tabelanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Cls", StringUtil.RTrim( Ddo_atributos_tabelanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Filteredtext_set", StringUtil.RTrim( Ddo_atributos_tabelanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_atributos_tabelanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_atributos_tabelanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_atributos_tabelanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Includesortasc", StringUtil.BoolToStr( Ddo_atributos_tabelanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_atributos_tabelanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Sortedstatus", StringUtil.RTrim( Ddo_atributos_tabelanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Includefilter", StringUtil.BoolToStr( Ddo_atributos_tabelanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Filtertype", StringUtil.RTrim( Ddo_atributos_tabelanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Filterisrange", StringUtil.BoolToStr( Ddo_atributos_tabelanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Includedatalist", StringUtil.BoolToStr( Ddo_atributos_tabelanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Datalisttype", StringUtil.RTrim( Ddo_atributos_tabelanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Datalistfixedvalues", StringUtil.RTrim( Ddo_atributos_tabelanom_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Datalistproc", StringUtil.RTrim( Ddo_atributos_tabelanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_atributos_tabelanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Sortasc", StringUtil.RTrim( Ddo_atributos_tabelanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Sortdsc", StringUtil.RTrim( Ddo_atributos_tabelanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Loadingdata", StringUtil.RTrim( Ddo_atributos_tabelanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Cleanfilter", StringUtil.RTrim( Ddo_atributos_tabelanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Rangefilterfrom", StringUtil.RTrim( Ddo_atributos_tabelanom_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Rangefilterto", StringUtil.RTrim( Ddo_atributos_tabelanom_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Noresultsfound", StringUtil.RTrim( Ddo_atributos_tabelanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Searchbuttontext", StringUtil.RTrim( Ddo_atributos_tabelanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Caption", StringUtil.RTrim( Ddo_atributos_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Tooltip", StringUtil.RTrim( Ddo_atributos_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Cls", StringUtil.RTrim( Ddo_atributos_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_atributos_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_atributos_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_atributos_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_atributos_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_atributos_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_atributos_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_atributos_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Filterisrange", StringUtil.BoolToStr( Ddo_atributos_ativo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_atributos_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_atributos_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_atributos_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_atributos_ativo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Sortasc", StringUtil.RTrim( Ddo_atributos_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_atributos_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Loadingdata", StringUtil.RTrim( Ddo_atributos_ativo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_atributos_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Rangefilterfrom", StringUtil.RTrim( Ddo_atributos_ativo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Rangefilterto", StringUtil.RTrim( Ddo_atributos_ativo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Noresultsfound", StringUtil.RTrim( Ddo_atributos_ativo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_atributos_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Caption", StringUtil.RTrim( Ddo_atributos_pk_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Tooltip", StringUtil.RTrim( Ddo_atributos_pk_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Cls", StringUtil.RTrim( Ddo_atributos_pk_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Selectedvalue_set", StringUtil.RTrim( Ddo_atributos_pk_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Dropdownoptionstype", StringUtil.RTrim( Ddo_atributos_pk_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_atributos_pk_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Includesortasc", StringUtil.BoolToStr( Ddo_atributos_pk_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Includesortdsc", StringUtil.BoolToStr( Ddo_atributos_pk_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Sortedstatus", StringUtil.RTrim( Ddo_atributos_pk_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Includefilter", StringUtil.BoolToStr( Ddo_atributos_pk_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Filterisrange", StringUtil.BoolToStr( Ddo_atributos_pk_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Includedatalist", StringUtil.BoolToStr( Ddo_atributos_pk_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Datalisttype", StringUtil.RTrim( Ddo_atributos_pk_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Datalistfixedvalues", StringUtil.RTrim( Ddo_atributos_pk_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_atributos_pk_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Sortasc", StringUtil.RTrim( Ddo_atributos_pk_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Sortdsc", StringUtil.RTrim( Ddo_atributos_pk_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Loadingdata", StringUtil.RTrim( Ddo_atributos_pk_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Cleanfilter", StringUtil.RTrim( Ddo_atributos_pk_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Rangefilterfrom", StringUtil.RTrim( Ddo_atributos_pk_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Rangefilterto", StringUtil.RTrim( Ddo_atributos_pk_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Noresultsfound", StringUtil.RTrim( Ddo_atributos_pk_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Searchbuttontext", StringUtil.RTrim( Ddo_atributos_pk_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Caption", StringUtil.RTrim( Ddo_atributos_fk_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Tooltip", StringUtil.RTrim( Ddo_atributos_fk_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Cls", StringUtil.RTrim( Ddo_atributos_fk_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Selectedvalue_set", StringUtil.RTrim( Ddo_atributos_fk_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Dropdownoptionstype", StringUtil.RTrim( Ddo_atributos_fk_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_atributos_fk_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Includesortasc", StringUtil.BoolToStr( Ddo_atributos_fk_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Includesortdsc", StringUtil.BoolToStr( Ddo_atributos_fk_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Sortedstatus", StringUtil.RTrim( Ddo_atributos_fk_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Includefilter", StringUtil.BoolToStr( Ddo_atributos_fk_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Filterisrange", StringUtil.BoolToStr( Ddo_atributos_fk_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Includedatalist", StringUtil.BoolToStr( Ddo_atributos_fk_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Datalisttype", StringUtil.RTrim( Ddo_atributos_fk_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Datalistfixedvalues", StringUtil.RTrim( Ddo_atributos_fk_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_atributos_fk_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Sortasc", StringUtil.RTrim( Ddo_atributos_fk_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Sortdsc", StringUtil.RTrim( Ddo_atributos_fk_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Loadingdata", StringUtil.RTrim( Ddo_atributos_fk_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Cleanfilter", StringUtil.RTrim( Ddo_atributos_fk_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Rangefilterfrom", StringUtil.RTrim( Ddo_atributos_fk_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Rangefilterto", StringUtil.RTrim( Ddo_atributos_fk_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Noresultsfound", StringUtil.RTrim( Ddo_atributos_fk_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Searchbuttontext", StringUtil.RTrim( Ddo_atributos_fk_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Activeeventkey", StringUtil.RTrim( Ddo_atributos_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_atributos_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_atributos_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Activeeventkey", StringUtil.RTrim( Ddo_atributos_tipodados_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TIPODADOS_Selectedvalue_get", StringUtil.RTrim( Ddo_atributos_tipodados_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Activeeventkey", StringUtil.RTrim( Ddo_atributos_detalhes_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Filteredtext_get", StringUtil.RTrim( Ddo_atributos_detalhes_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_DETALHES_Selectedvalue_get", StringUtil.RTrim( Ddo_atributos_detalhes_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Activeeventkey", StringUtil.RTrim( Ddo_atributos_tabelanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Filteredtext_get", StringUtil.RTrim( Ddo_atributos_tabelanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_TABELANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_atributos_tabelanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_atributos_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_atributos_ativo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Activeeventkey", StringUtil.RTrim( Ddo_atributos_pk_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_PK_Selectedvalue_get", StringUtil.RTrim( Ddo_atributos_pk_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Activeeventkey", StringUtil.RTrim( Ddo_atributos_fk_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ATRIBUTOS_FK_Selectedvalue_get", StringUtil.RTrim( Ddo_atributos_fk_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE4C2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT4C2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwatributos.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWAtributos" ;
      }

      public override String GetPgmdesc( )
      {
         return " Atributos" ;
      }

      protected void WB4C0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_4C2( true) ;
         }
         else
         {
            wb_table1_2_4C2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_4C2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(95, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfatributos_nome_Internalname, StringUtil.RTrim( AV67TFAtributos_Nome), StringUtil.RTrim( context.localUtil.Format( AV67TFAtributos_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfatributos_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfatributos_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfatributos_nome_sel_Internalname, StringUtil.RTrim( AV68TFAtributos_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV68TFAtributos_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfatributos_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfatributos_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfatributos_detalhes_Internalname, StringUtil.RTrim( AV75TFAtributos_Detalhes), StringUtil.RTrim( context.localUtil.Format( AV75TFAtributos_Detalhes, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfatributos_detalhes_Jsonclick, 0, "Attribute", "", "", "", edtavTfatributos_detalhes_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfatributos_detalhes_sel_Internalname, StringUtil.RTrim( AV76TFAtributos_Detalhes_Sel), StringUtil.RTrim( context.localUtil.Format( AV76TFAtributos_Detalhes_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfatributos_detalhes_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfatributos_detalhes_sel_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfatributos_tabelanom_Internalname, StringUtil.RTrim( AV79TFAtributos_TabelaNom), StringUtil.RTrim( context.localUtil.Format( AV79TFAtributos_TabelaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfatributos_tabelanom_Jsonclick, 0, "Attribute", "", "", "", edtavTfatributos_tabelanom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfatributos_tabelanom_sel_Internalname, StringUtil.RTrim( AV80TFAtributos_TabelaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV80TFAtributos_TabelaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfatributos_tabelanom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfatributos_tabelanom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfatributos_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83TFAtributos_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV83TFAtributos_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfatributos_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfatributos_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfatributos_pk_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86TFAtributos_PK_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV86TFAtributos_PK_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfatributos_pk_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfatributos_pk_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfatributos_fk_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV89TFAtributos_FK_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV89TFAtributos_FK_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfatributos_fk_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfatributos_fk_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ATRIBUTOS_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_atributos_nometitlecontrolidtoreplace_Internalname, AV69ddo_Atributos_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavDdo_atributos_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ATRIBUTOS_TIPODADOSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_atributos_tipodadostitlecontrolidtoreplace_Internalname, AV73ddo_Atributos_TipoDadosTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_atributos_tipodadostitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ATRIBUTOS_DETALHESContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_atributos_detalhestitlecontrolidtoreplace_Internalname, AV77ddo_Atributos_DetalhesTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavDdo_atributos_detalhestitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ATRIBUTOS_TABELANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_atributos_tabelanomtitlecontrolidtoreplace_Internalname, AV81ddo_Atributos_TabelaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_atributos_tabelanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ATRIBUTOS_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_atributos_ativotitlecontrolidtoreplace_Internalname, AV84ddo_Atributos_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavDdo_atributos_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ATRIBUTOS_PKContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_atributos_pktitlecontrolidtoreplace_Internalname, AV87ddo_Atributos_PKTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", 0, edtavDdo_atributos_pktitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ATRIBUTOS_FKContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_atributos_fktitlecontrolidtoreplace_Internalname, AV90ddo_Atributos_FKTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavDdo_atributos_fktitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAtributos.htm");
         }
         wbLoad = true;
      }

      protected void START4C2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Atributos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP4C0( ) ;
      }

      protected void WS4C2( )
      {
         START4C2( ) ;
         EVT4C2( ) ;
      }

      protected void EVT4C2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E114C2 */
                              E114C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ATRIBUTOS_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E124C2 */
                              E124C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ATRIBUTOS_TIPODADOS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E134C2 */
                              E134C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ATRIBUTOS_DETALHES.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E144C2 */
                              E144C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ATRIBUTOS_TABELANOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E154C2 */
                              E154C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ATRIBUTOS_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E164C2 */
                              E164C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ATRIBUTOS_PK.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E174C2 */
                              E174C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ATRIBUTOS_FK.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E184C2 */
                              E184C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E194C2 */
                              E194C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E204C2 */
                              E204C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E214C2 */
                              E214C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E224C2 */
                              E224C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E234C2 */
                              E234C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E244C2 */
                              E244C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E254C2 */
                              E254C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_79_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
                              SubsflControlProps_792( ) ;
                              AV34Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)) ? AV131Update_GXI : context.convertURL( context.PathToRelativeUrl( AV34Update))));
                              AV35Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV132Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV35Delete))));
                              AV58Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV58Display)) ? AV133Display_GXI : context.convertURL( context.PathToRelativeUrl( AV58Display))));
                              A176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAtributos_Codigo_Internalname), ",", "."));
                              A177Atributos_Nome = StringUtil.Upper( cgiGet( edtAtributos_Nome_Internalname));
                              cmbAtributos_TipoDados.Name = cmbAtributos_TipoDados_Internalname;
                              cmbAtributos_TipoDados.CurrentValue = cgiGet( cmbAtributos_TipoDados_Internalname);
                              A178Atributos_TipoDados = cgiGet( cmbAtributos_TipoDados_Internalname);
                              n178Atributos_TipoDados = false;
                              A390Atributos_Detalhes = cgiGet( edtAtributos_Detalhes_Internalname);
                              n390Atributos_Detalhes = false;
                              A357Atributos_TabelaNom = StringUtil.Upper( cgiGet( edtAtributos_TabelaNom_Internalname));
                              n357Atributos_TabelaNom = false;
                              A180Atributos_Ativo = StringUtil.StrToBool( cgiGet( chkAtributos_Ativo_Internalname));
                              A400Atributos_PK = StringUtil.StrToBool( cgiGet( chkAtributos_PK_Internalname));
                              n400Atributos_PK = false;
                              A401Atributos_FK = StringUtil.StrToBool( cgiGet( chkAtributos_FK_Internalname));
                              n401Atributos_FK = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E264C2 */
                                    E264C2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E274C2 */
                                    E274C2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E284C2 */
                                    E284C2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_areatrabalhocod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vSISTEMA_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV60Sistema_AreaTrabalhoCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Atributos_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vATRIBUTOS_NOME1"), AV17Atributos_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Atributos_tabelanom1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vATRIBUTOS_TABELANOM1"), AV61Atributos_TabelaNom1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Atributos_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vATRIBUTOS_NOME2"), AV23Atributos_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Atributos_tabelanom2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vATRIBUTOS_TABELANOM2"), AV62Atributos_TabelaNom2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfatributos_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_NOME"), AV67TFAtributos_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfatributos_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_NOME_SEL"), AV68TFAtributos_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfatributos_detalhes Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_DETALHES"), AV75TFAtributos_Detalhes) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfatributos_detalhes_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_DETALHES_SEL"), AV76TFAtributos_Detalhes_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfatributos_tabelanom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_TABELANOM"), AV79TFAtributos_TabelaNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfatributos_tabelanom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_TABELANOM_SEL"), AV80TFAtributos_TabelaNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfatributos_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFATRIBUTOS_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV83TFAtributos_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfatributos_pk_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFATRIBUTOS_PK_SEL"), ",", ".") != Convert.ToDecimal( AV86TFAtributos_PK_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfatributos_fk_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFATRIBUTOS_FK_SEL"), ",", ".") != Convert.ToDecimal( AV89TFAtributos_FK_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE4C2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA4C2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            dynavSistema_areatrabalhocod.Name = "vSISTEMA_AREATRABALHOCOD";
            dynavSistema_areatrabalhocod.WebTags = "";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("ATRIBUTOS_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("ATRIBUTOS_TABELANOM", "Tabela", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("ATRIBUTOS_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("ATRIBUTOS_TABELANOM", "Tabela", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            GXCCtl = "ATRIBUTOS_TIPODADOS_" + sGXsfl_79_idx;
            cmbAtributos_TipoDados.Name = GXCCtl;
            cmbAtributos_TipoDados.WebTags = "";
            cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
            cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
            cmbAtributos_TipoDados.addItem("C", "Character", 0);
            cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
            cmbAtributos_TipoDados.addItem("D", "Date", 0);
            cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
            cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
            cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
            cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
            if ( cmbAtributos_TipoDados.ItemCount > 0 )
            {
               A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
               n178Atributos_TipoDados = false;
            }
            GXCCtl = "ATRIBUTOS_ATIVO_" + sGXsfl_79_idx;
            chkAtributos_Ativo.Name = GXCCtl;
            chkAtributos_Ativo.WebTags = "";
            chkAtributos_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_Ativo_Internalname, "TitleCaption", chkAtributos_Ativo.Caption);
            chkAtributos_Ativo.CheckedValue = "false";
            GXCCtl = "ATRIBUTOS_PK_" + sGXsfl_79_idx;
            chkAtributos_PK.Name = GXCCtl;
            chkAtributos_PK.WebTags = "";
            chkAtributos_PK.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_PK_Internalname, "TitleCaption", chkAtributos_PK.Caption);
            chkAtributos_PK.CheckedValue = "false";
            GXCCtl = "ATRIBUTOS_FK_" + sGXsfl_79_idx;
            chkAtributos_FK.Name = GXCCtl;
            chkAtributos_FK.WebTags = "";
            chkAtributos_FK.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_FK_Internalname, "TitleCaption", chkAtributos_FK.Caption);
            chkAtributos_FK.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSISTEMA_AREATRABALHOCOD4C2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSISTEMA_AREATRABALHOCOD_data4C2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSISTEMA_AREATRABALHOCOD_html4C2( )
      {
         int gxdynajaxvalue ;
         GXDLVvSISTEMA_AREATRABALHOCOD_data4C2( ) ;
         gxdynajaxindex = 1;
         dynavSistema_areatrabalhocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSistema_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSistema_areatrabalhocod.ItemCount > 0 )
         {
            AV60Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavSistema_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void GXDLVvSISTEMA_AREATRABALHOCOD_data4C2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H004C2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H004C2_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H004C2_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_792( ) ;
         while ( nGXsfl_79_idx <= nRC_GXsfl_79 )
         {
            sendrow_792( ) ;
            nGXsfl_79_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_79_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_79_idx+1));
            sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
            SubsflControlProps_792( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV60Sistema_AreaTrabalhoCod ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Atributos_Nome1 ,
                                       String AV61Atributos_TabelaNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23Atributos_Nome2 ,
                                       String AV62Atributos_TabelaNom2 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       String AV67TFAtributos_Nome ,
                                       String AV68TFAtributos_Nome_Sel ,
                                       String AV75TFAtributos_Detalhes ,
                                       String AV76TFAtributos_Detalhes_Sel ,
                                       String AV79TFAtributos_TabelaNom ,
                                       String AV80TFAtributos_TabelaNom_Sel ,
                                       short AV83TFAtributos_Ativo_Sel ,
                                       short AV86TFAtributos_PK_Sel ,
                                       short AV89TFAtributos_FK_Sel ,
                                       String AV69ddo_Atributos_NomeTitleControlIdToReplace ,
                                       String AV73ddo_Atributos_TipoDadosTitleControlIdToReplace ,
                                       String AV77ddo_Atributos_DetalhesTitleControlIdToReplace ,
                                       String AV81ddo_Atributos_TabelaNomTitleControlIdToReplace ,
                                       String AV84ddo_Atributos_AtivoTitleControlIdToReplace ,
                                       String AV87ddo_Atributos_PKTitleControlIdToReplace ,
                                       String AV90ddo_Atributos_FKTitleControlIdToReplace ,
                                       IGxCollection AV72TFAtributos_TipoDados_Sels ,
                                       String AV134Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV33DynamicFiltersIgnoreFirst ,
                                       bool AV32DynamicFiltersRemoving ,
                                       int A176Atributos_Codigo ,
                                       int AV63Sistema_Codigo ,
                                       int A356Atributos_TabelaCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF4C2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_NOME", StringUtil.RTrim( A177Atributos_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_TIPODADOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A178Atributos_TipoDados, ""))));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_TIPODADOS", StringUtil.RTrim( A178Atributos_TipoDados));
         GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_DETALHES", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A390Atributos_Detalhes, ""))));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_DETALHES", StringUtil.RTrim( A390Atributos_Detalhes));
         GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_ATIVO", GetSecureSignedToken( "", A180Atributos_Ativo));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_ATIVO", StringUtil.BoolToStr( A180Atributos_Ativo));
         GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_PK", GetSecureSignedToken( "", A400Atributos_PK));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_PK", StringUtil.BoolToStr( A400Atributos_PK));
         GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_FK", GetSecureSignedToken( "", A401Atributos_FK));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_FK", StringUtil.BoolToStr( A401Atributos_FK));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( dynavSistema_areatrabalhocod.ItemCount > 0 )
         {
            AV60Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavSistema_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF4C2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV134Pgmname = "WWAtributos";
         context.Gx_err = 0;
      }

      protected void RF4C2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 79;
         /* Execute user event: E274C2 */
         E274C2 ();
         nGXsfl_79_idx = 1;
         sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
         SubsflControlProps_792( ) ;
         nGXsfl_79_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_792( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A178Atributos_TipoDados ,
                                                 AV123WWAtributosDS_13_Tfatributos_tipodados_sels ,
                                                 AV111WWAtributosDS_1_Sistema_areatrabalhocod ,
                                                 AV112WWAtributosDS_2_Dynamicfiltersselector1 ,
                                                 AV113WWAtributosDS_3_Dynamicfiltersoperator1 ,
                                                 AV114WWAtributosDS_4_Atributos_nome1 ,
                                                 AV115WWAtributosDS_5_Atributos_tabelanom1 ,
                                                 AV116WWAtributosDS_6_Dynamicfiltersenabled2 ,
                                                 AV117WWAtributosDS_7_Dynamicfiltersselector2 ,
                                                 AV118WWAtributosDS_8_Dynamicfiltersoperator2 ,
                                                 AV119WWAtributosDS_9_Atributos_nome2 ,
                                                 AV120WWAtributosDS_10_Atributos_tabelanom2 ,
                                                 AV122WWAtributosDS_12_Tfatributos_nome_sel ,
                                                 AV121WWAtributosDS_11_Tfatributos_nome ,
                                                 AV123WWAtributosDS_13_Tfatributos_tipodados_sels.Count ,
                                                 AV125WWAtributosDS_15_Tfatributos_detalhes_sel ,
                                                 AV124WWAtributosDS_14_Tfatributos_detalhes ,
                                                 AV127WWAtributosDS_17_Tfatributos_tabelanom_sel ,
                                                 AV126WWAtributosDS_16_Tfatributos_tabelanom ,
                                                 AV128WWAtributosDS_18_Tfatributos_ativo_sel ,
                                                 AV129WWAtributosDS_19_Tfatributos_pk_sel ,
                                                 AV130WWAtributosDS_20_Tfatributos_fk_sel ,
                                                 A135Sistema_AreaTrabalhoCod ,
                                                 A177Atributos_Nome ,
                                                 A357Atributos_TabelaNom ,
                                                 A390Atributos_Detalhes ,
                                                 A180Atributos_Ativo ,
                                                 A400Atributos_PK ,
                                                 A401Atributos_FK ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                                 TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV114WWAtributosDS_4_Atributos_nome1 = StringUtil.PadR( StringUtil.RTrim( AV114WWAtributosDS_4_Atributos_nome1), 50, "%");
            lV114WWAtributosDS_4_Atributos_nome1 = StringUtil.PadR( StringUtil.RTrim( AV114WWAtributosDS_4_Atributos_nome1), 50, "%");
            lV115WWAtributosDS_5_Atributos_tabelanom1 = StringUtil.PadR( StringUtil.RTrim( AV115WWAtributosDS_5_Atributos_tabelanom1), 50, "%");
            lV115WWAtributosDS_5_Atributos_tabelanom1 = StringUtil.PadR( StringUtil.RTrim( AV115WWAtributosDS_5_Atributos_tabelanom1), 50, "%");
            lV119WWAtributosDS_9_Atributos_nome2 = StringUtil.PadR( StringUtil.RTrim( AV119WWAtributosDS_9_Atributos_nome2), 50, "%");
            lV119WWAtributosDS_9_Atributos_nome2 = StringUtil.PadR( StringUtil.RTrim( AV119WWAtributosDS_9_Atributos_nome2), 50, "%");
            lV120WWAtributosDS_10_Atributos_tabelanom2 = StringUtil.PadR( StringUtil.RTrim( AV120WWAtributosDS_10_Atributos_tabelanom2), 50, "%");
            lV120WWAtributosDS_10_Atributos_tabelanom2 = StringUtil.PadR( StringUtil.RTrim( AV120WWAtributosDS_10_Atributos_tabelanom2), 50, "%");
            lV121WWAtributosDS_11_Tfatributos_nome = StringUtil.PadR( StringUtil.RTrim( AV121WWAtributosDS_11_Tfatributos_nome), 50, "%");
            lV124WWAtributosDS_14_Tfatributos_detalhes = StringUtil.PadR( StringUtil.RTrim( AV124WWAtributosDS_14_Tfatributos_detalhes), 10, "%");
            lV126WWAtributosDS_16_Tfatributos_tabelanom = StringUtil.PadR( StringUtil.RTrim( AV126WWAtributosDS_16_Tfatributos_tabelanom), 50, "%");
            /* Using cursor H004C3 */
            pr_default.execute(1, new Object[] {AV111WWAtributosDS_1_Sistema_areatrabalhocod, lV114WWAtributosDS_4_Atributos_nome1, lV114WWAtributosDS_4_Atributos_nome1, lV115WWAtributosDS_5_Atributos_tabelanom1, lV115WWAtributosDS_5_Atributos_tabelanom1, lV119WWAtributosDS_9_Atributos_nome2, lV119WWAtributosDS_9_Atributos_nome2, lV120WWAtributosDS_10_Atributos_tabelanom2, lV120WWAtributosDS_10_Atributos_tabelanom2, lV121WWAtributosDS_11_Tfatributos_nome, AV122WWAtributosDS_12_Tfatributos_nome_sel, lV124WWAtributosDS_14_Tfatributos_detalhes, AV125WWAtributosDS_15_Tfatributos_detalhes_sel, lV126WWAtributosDS_16_Tfatributos_tabelanom, AV127WWAtributosDS_17_Tfatributos_tabelanom_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_79_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A190Tabela_SistemaCod = H004C3_A190Tabela_SistemaCod[0];
               n190Tabela_SistemaCod = H004C3_n190Tabela_SistemaCod[0];
               A135Sistema_AreaTrabalhoCod = H004C3_A135Sistema_AreaTrabalhoCod[0];
               n135Sistema_AreaTrabalhoCod = H004C3_n135Sistema_AreaTrabalhoCod[0];
               A356Atributos_TabelaCod = H004C3_A356Atributos_TabelaCod[0];
               A401Atributos_FK = H004C3_A401Atributos_FK[0];
               n401Atributos_FK = H004C3_n401Atributos_FK[0];
               A400Atributos_PK = H004C3_A400Atributos_PK[0];
               n400Atributos_PK = H004C3_n400Atributos_PK[0];
               A180Atributos_Ativo = H004C3_A180Atributos_Ativo[0];
               A357Atributos_TabelaNom = H004C3_A357Atributos_TabelaNom[0];
               n357Atributos_TabelaNom = H004C3_n357Atributos_TabelaNom[0];
               A390Atributos_Detalhes = H004C3_A390Atributos_Detalhes[0];
               n390Atributos_Detalhes = H004C3_n390Atributos_Detalhes[0];
               A178Atributos_TipoDados = H004C3_A178Atributos_TipoDados[0];
               n178Atributos_TipoDados = H004C3_n178Atributos_TipoDados[0];
               A177Atributos_Nome = H004C3_A177Atributos_Nome[0];
               A176Atributos_Codigo = H004C3_A176Atributos_Codigo[0];
               A190Tabela_SistemaCod = H004C3_A190Tabela_SistemaCod[0];
               n190Tabela_SistemaCod = H004C3_n190Tabela_SistemaCod[0];
               A357Atributos_TabelaNom = H004C3_A357Atributos_TabelaNom[0];
               n357Atributos_TabelaNom = H004C3_n357Atributos_TabelaNom[0];
               A135Sistema_AreaTrabalhoCod = H004C3_A135Sistema_AreaTrabalhoCod[0];
               n135Sistema_AreaTrabalhoCod = H004C3_n135Sistema_AreaTrabalhoCod[0];
               /* Execute user event: E284C2 */
               E284C2 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 79;
            WB4C0( ) ;
         }
         nGXsfl_79_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV111WWAtributosDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV112WWAtributosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV113WWAtributosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV114WWAtributosDS_4_Atributos_nome1 = AV17Atributos_Nome1;
         AV115WWAtributosDS_5_Atributos_tabelanom1 = AV61Atributos_TabelaNom1;
         AV116WWAtributosDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV117WWAtributosDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV118WWAtributosDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV119WWAtributosDS_9_Atributos_nome2 = AV23Atributos_Nome2;
         AV120WWAtributosDS_10_Atributos_tabelanom2 = AV62Atributos_TabelaNom2;
         AV121WWAtributosDS_11_Tfatributos_nome = AV67TFAtributos_Nome;
         AV122WWAtributosDS_12_Tfatributos_nome_sel = AV68TFAtributos_Nome_Sel;
         AV123WWAtributosDS_13_Tfatributos_tipodados_sels = AV72TFAtributos_TipoDados_Sels;
         AV124WWAtributosDS_14_Tfatributos_detalhes = AV75TFAtributos_Detalhes;
         AV125WWAtributosDS_15_Tfatributos_detalhes_sel = AV76TFAtributos_Detalhes_Sel;
         AV126WWAtributosDS_16_Tfatributos_tabelanom = AV79TFAtributos_TabelaNom;
         AV127WWAtributosDS_17_Tfatributos_tabelanom_sel = AV80TFAtributos_TabelaNom_Sel;
         AV128WWAtributosDS_18_Tfatributos_ativo_sel = AV83TFAtributos_Ativo_Sel;
         AV129WWAtributosDS_19_Tfatributos_pk_sel = AV86TFAtributos_PK_Sel;
         AV130WWAtributosDS_20_Tfatributos_fk_sel = AV89TFAtributos_FK_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A178Atributos_TipoDados ,
                                              AV123WWAtributosDS_13_Tfatributos_tipodados_sels ,
                                              AV111WWAtributosDS_1_Sistema_areatrabalhocod ,
                                              AV112WWAtributosDS_2_Dynamicfiltersselector1 ,
                                              AV113WWAtributosDS_3_Dynamicfiltersoperator1 ,
                                              AV114WWAtributosDS_4_Atributos_nome1 ,
                                              AV115WWAtributosDS_5_Atributos_tabelanom1 ,
                                              AV116WWAtributosDS_6_Dynamicfiltersenabled2 ,
                                              AV117WWAtributosDS_7_Dynamicfiltersselector2 ,
                                              AV118WWAtributosDS_8_Dynamicfiltersoperator2 ,
                                              AV119WWAtributosDS_9_Atributos_nome2 ,
                                              AV120WWAtributosDS_10_Atributos_tabelanom2 ,
                                              AV122WWAtributosDS_12_Tfatributos_nome_sel ,
                                              AV121WWAtributosDS_11_Tfatributos_nome ,
                                              AV123WWAtributosDS_13_Tfatributos_tipodados_sels.Count ,
                                              AV125WWAtributosDS_15_Tfatributos_detalhes_sel ,
                                              AV124WWAtributosDS_14_Tfatributos_detalhes ,
                                              AV127WWAtributosDS_17_Tfatributos_tabelanom_sel ,
                                              AV126WWAtributosDS_16_Tfatributos_tabelanom ,
                                              AV128WWAtributosDS_18_Tfatributos_ativo_sel ,
                                              AV129WWAtributosDS_19_Tfatributos_pk_sel ,
                                              AV130WWAtributosDS_20_Tfatributos_fk_sel ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A177Atributos_Nome ,
                                              A357Atributos_TabelaNom ,
                                              A390Atributos_Detalhes ,
                                              A180Atributos_Ativo ,
                                              A400Atributos_PK ,
                                              A401Atributos_FK ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV114WWAtributosDS_4_Atributos_nome1 = StringUtil.PadR( StringUtil.RTrim( AV114WWAtributosDS_4_Atributos_nome1), 50, "%");
         lV114WWAtributosDS_4_Atributos_nome1 = StringUtil.PadR( StringUtil.RTrim( AV114WWAtributosDS_4_Atributos_nome1), 50, "%");
         lV115WWAtributosDS_5_Atributos_tabelanom1 = StringUtil.PadR( StringUtil.RTrim( AV115WWAtributosDS_5_Atributos_tabelanom1), 50, "%");
         lV115WWAtributosDS_5_Atributos_tabelanom1 = StringUtil.PadR( StringUtil.RTrim( AV115WWAtributosDS_5_Atributos_tabelanom1), 50, "%");
         lV119WWAtributosDS_9_Atributos_nome2 = StringUtil.PadR( StringUtil.RTrim( AV119WWAtributosDS_9_Atributos_nome2), 50, "%");
         lV119WWAtributosDS_9_Atributos_nome2 = StringUtil.PadR( StringUtil.RTrim( AV119WWAtributosDS_9_Atributos_nome2), 50, "%");
         lV120WWAtributosDS_10_Atributos_tabelanom2 = StringUtil.PadR( StringUtil.RTrim( AV120WWAtributosDS_10_Atributos_tabelanom2), 50, "%");
         lV120WWAtributosDS_10_Atributos_tabelanom2 = StringUtil.PadR( StringUtil.RTrim( AV120WWAtributosDS_10_Atributos_tabelanom2), 50, "%");
         lV121WWAtributosDS_11_Tfatributos_nome = StringUtil.PadR( StringUtil.RTrim( AV121WWAtributosDS_11_Tfatributos_nome), 50, "%");
         lV124WWAtributosDS_14_Tfatributos_detalhes = StringUtil.PadR( StringUtil.RTrim( AV124WWAtributosDS_14_Tfatributos_detalhes), 10, "%");
         lV126WWAtributosDS_16_Tfatributos_tabelanom = StringUtil.PadR( StringUtil.RTrim( AV126WWAtributosDS_16_Tfatributos_tabelanom), 50, "%");
         /* Using cursor H004C4 */
         pr_default.execute(2, new Object[] {AV111WWAtributosDS_1_Sistema_areatrabalhocod, lV114WWAtributosDS_4_Atributos_nome1, lV114WWAtributosDS_4_Atributos_nome1, lV115WWAtributosDS_5_Atributos_tabelanom1, lV115WWAtributosDS_5_Atributos_tabelanom1, lV119WWAtributosDS_9_Atributos_nome2, lV119WWAtributosDS_9_Atributos_nome2, lV120WWAtributosDS_10_Atributos_tabelanom2, lV120WWAtributosDS_10_Atributos_tabelanom2, lV121WWAtributosDS_11_Tfatributos_nome, AV122WWAtributosDS_12_Tfatributos_nome_sel, lV124WWAtributosDS_14_Tfatributos_detalhes, AV125WWAtributosDS_15_Tfatributos_detalhes_sel, lV126WWAtributosDS_16_Tfatributos_tabelanom, AV127WWAtributosDS_17_Tfatributos_tabelanom_sel});
         GRID_nRecordCount = H004C4_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV111WWAtributosDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV112WWAtributosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV113WWAtributosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV114WWAtributosDS_4_Atributos_nome1 = AV17Atributos_Nome1;
         AV115WWAtributosDS_5_Atributos_tabelanom1 = AV61Atributos_TabelaNom1;
         AV116WWAtributosDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV117WWAtributosDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV118WWAtributosDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV119WWAtributosDS_9_Atributos_nome2 = AV23Atributos_Nome2;
         AV120WWAtributosDS_10_Atributos_tabelanom2 = AV62Atributos_TabelaNom2;
         AV121WWAtributosDS_11_Tfatributos_nome = AV67TFAtributos_Nome;
         AV122WWAtributosDS_12_Tfatributos_nome_sel = AV68TFAtributos_Nome_Sel;
         AV123WWAtributosDS_13_Tfatributos_tipodados_sels = AV72TFAtributos_TipoDados_Sels;
         AV124WWAtributosDS_14_Tfatributos_detalhes = AV75TFAtributos_Detalhes;
         AV125WWAtributosDS_15_Tfatributos_detalhes_sel = AV76TFAtributos_Detalhes_Sel;
         AV126WWAtributosDS_16_Tfatributos_tabelanom = AV79TFAtributos_TabelaNom;
         AV127WWAtributosDS_17_Tfatributos_tabelanom_sel = AV80TFAtributos_TabelaNom_Sel;
         AV128WWAtributosDS_18_Tfatributos_ativo_sel = AV83TFAtributos_Ativo_Sel;
         AV129WWAtributosDS_19_Tfatributos_pk_sel = AV86TFAtributos_PK_Sel;
         AV130WWAtributosDS_20_Tfatributos_fk_sel = AV89TFAtributos_FK_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV60Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Atributos_Nome1, AV61Atributos_TabelaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Atributos_Nome2, AV62Atributos_TabelaNom2, AV20DynamicFiltersEnabled2, AV67TFAtributos_Nome, AV68TFAtributos_Nome_Sel, AV75TFAtributos_Detalhes, AV76TFAtributos_Detalhes_Sel, AV79TFAtributos_TabelaNom, AV80TFAtributos_TabelaNom_Sel, AV83TFAtributos_Ativo_Sel, AV86TFAtributos_PK_Sel, AV89TFAtributos_FK_Sel, AV69ddo_Atributos_NomeTitleControlIdToReplace, AV73ddo_Atributos_TipoDadosTitleControlIdToReplace, AV77ddo_Atributos_DetalhesTitleControlIdToReplace, AV81ddo_Atributos_TabelaNomTitleControlIdToReplace, AV84ddo_Atributos_AtivoTitleControlIdToReplace, AV87ddo_Atributos_PKTitleControlIdToReplace, AV90ddo_Atributos_FKTitleControlIdToReplace, AV72TFAtributos_TipoDados_Sels, AV134Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A176Atributos_Codigo, AV63Sistema_Codigo, A356Atributos_TabelaCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV111WWAtributosDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV112WWAtributosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV113WWAtributosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV114WWAtributosDS_4_Atributos_nome1 = AV17Atributos_Nome1;
         AV115WWAtributosDS_5_Atributos_tabelanom1 = AV61Atributos_TabelaNom1;
         AV116WWAtributosDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV117WWAtributosDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV118WWAtributosDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV119WWAtributosDS_9_Atributos_nome2 = AV23Atributos_Nome2;
         AV120WWAtributosDS_10_Atributos_tabelanom2 = AV62Atributos_TabelaNom2;
         AV121WWAtributosDS_11_Tfatributos_nome = AV67TFAtributos_Nome;
         AV122WWAtributosDS_12_Tfatributos_nome_sel = AV68TFAtributos_Nome_Sel;
         AV123WWAtributosDS_13_Tfatributos_tipodados_sels = AV72TFAtributos_TipoDados_Sels;
         AV124WWAtributosDS_14_Tfatributos_detalhes = AV75TFAtributos_Detalhes;
         AV125WWAtributosDS_15_Tfatributos_detalhes_sel = AV76TFAtributos_Detalhes_Sel;
         AV126WWAtributosDS_16_Tfatributos_tabelanom = AV79TFAtributos_TabelaNom;
         AV127WWAtributosDS_17_Tfatributos_tabelanom_sel = AV80TFAtributos_TabelaNom_Sel;
         AV128WWAtributosDS_18_Tfatributos_ativo_sel = AV83TFAtributos_Ativo_Sel;
         AV129WWAtributosDS_19_Tfatributos_pk_sel = AV86TFAtributos_PK_Sel;
         AV130WWAtributosDS_20_Tfatributos_fk_sel = AV89TFAtributos_FK_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV60Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Atributos_Nome1, AV61Atributos_TabelaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Atributos_Nome2, AV62Atributos_TabelaNom2, AV20DynamicFiltersEnabled2, AV67TFAtributos_Nome, AV68TFAtributos_Nome_Sel, AV75TFAtributos_Detalhes, AV76TFAtributos_Detalhes_Sel, AV79TFAtributos_TabelaNom, AV80TFAtributos_TabelaNom_Sel, AV83TFAtributos_Ativo_Sel, AV86TFAtributos_PK_Sel, AV89TFAtributos_FK_Sel, AV69ddo_Atributos_NomeTitleControlIdToReplace, AV73ddo_Atributos_TipoDadosTitleControlIdToReplace, AV77ddo_Atributos_DetalhesTitleControlIdToReplace, AV81ddo_Atributos_TabelaNomTitleControlIdToReplace, AV84ddo_Atributos_AtivoTitleControlIdToReplace, AV87ddo_Atributos_PKTitleControlIdToReplace, AV90ddo_Atributos_FKTitleControlIdToReplace, AV72TFAtributos_TipoDados_Sels, AV134Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A176Atributos_Codigo, AV63Sistema_Codigo, A356Atributos_TabelaCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV111WWAtributosDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV112WWAtributosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV113WWAtributosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV114WWAtributosDS_4_Atributos_nome1 = AV17Atributos_Nome1;
         AV115WWAtributosDS_5_Atributos_tabelanom1 = AV61Atributos_TabelaNom1;
         AV116WWAtributosDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV117WWAtributosDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV118WWAtributosDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV119WWAtributosDS_9_Atributos_nome2 = AV23Atributos_Nome2;
         AV120WWAtributosDS_10_Atributos_tabelanom2 = AV62Atributos_TabelaNom2;
         AV121WWAtributosDS_11_Tfatributos_nome = AV67TFAtributos_Nome;
         AV122WWAtributosDS_12_Tfatributos_nome_sel = AV68TFAtributos_Nome_Sel;
         AV123WWAtributosDS_13_Tfatributos_tipodados_sels = AV72TFAtributos_TipoDados_Sels;
         AV124WWAtributosDS_14_Tfatributos_detalhes = AV75TFAtributos_Detalhes;
         AV125WWAtributosDS_15_Tfatributos_detalhes_sel = AV76TFAtributos_Detalhes_Sel;
         AV126WWAtributosDS_16_Tfatributos_tabelanom = AV79TFAtributos_TabelaNom;
         AV127WWAtributosDS_17_Tfatributos_tabelanom_sel = AV80TFAtributos_TabelaNom_Sel;
         AV128WWAtributosDS_18_Tfatributos_ativo_sel = AV83TFAtributos_Ativo_Sel;
         AV129WWAtributosDS_19_Tfatributos_pk_sel = AV86TFAtributos_PK_Sel;
         AV130WWAtributosDS_20_Tfatributos_fk_sel = AV89TFAtributos_FK_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV60Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Atributos_Nome1, AV61Atributos_TabelaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Atributos_Nome2, AV62Atributos_TabelaNom2, AV20DynamicFiltersEnabled2, AV67TFAtributos_Nome, AV68TFAtributos_Nome_Sel, AV75TFAtributos_Detalhes, AV76TFAtributos_Detalhes_Sel, AV79TFAtributos_TabelaNom, AV80TFAtributos_TabelaNom_Sel, AV83TFAtributos_Ativo_Sel, AV86TFAtributos_PK_Sel, AV89TFAtributos_FK_Sel, AV69ddo_Atributos_NomeTitleControlIdToReplace, AV73ddo_Atributos_TipoDadosTitleControlIdToReplace, AV77ddo_Atributos_DetalhesTitleControlIdToReplace, AV81ddo_Atributos_TabelaNomTitleControlIdToReplace, AV84ddo_Atributos_AtivoTitleControlIdToReplace, AV87ddo_Atributos_PKTitleControlIdToReplace, AV90ddo_Atributos_FKTitleControlIdToReplace, AV72TFAtributos_TipoDados_Sels, AV134Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A176Atributos_Codigo, AV63Sistema_Codigo, A356Atributos_TabelaCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV111WWAtributosDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV112WWAtributosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV113WWAtributosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV114WWAtributosDS_4_Atributos_nome1 = AV17Atributos_Nome1;
         AV115WWAtributosDS_5_Atributos_tabelanom1 = AV61Atributos_TabelaNom1;
         AV116WWAtributosDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV117WWAtributosDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV118WWAtributosDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV119WWAtributosDS_9_Atributos_nome2 = AV23Atributos_Nome2;
         AV120WWAtributosDS_10_Atributos_tabelanom2 = AV62Atributos_TabelaNom2;
         AV121WWAtributosDS_11_Tfatributos_nome = AV67TFAtributos_Nome;
         AV122WWAtributosDS_12_Tfatributos_nome_sel = AV68TFAtributos_Nome_Sel;
         AV123WWAtributosDS_13_Tfatributos_tipodados_sels = AV72TFAtributos_TipoDados_Sels;
         AV124WWAtributosDS_14_Tfatributos_detalhes = AV75TFAtributos_Detalhes;
         AV125WWAtributosDS_15_Tfatributos_detalhes_sel = AV76TFAtributos_Detalhes_Sel;
         AV126WWAtributosDS_16_Tfatributos_tabelanom = AV79TFAtributos_TabelaNom;
         AV127WWAtributosDS_17_Tfatributos_tabelanom_sel = AV80TFAtributos_TabelaNom_Sel;
         AV128WWAtributosDS_18_Tfatributos_ativo_sel = AV83TFAtributos_Ativo_Sel;
         AV129WWAtributosDS_19_Tfatributos_pk_sel = AV86TFAtributos_PK_Sel;
         AV130WWAtributosDS_20_Tfatributos_fk_sel = AV89TFAtributos_FK_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV60Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Atributos_Nome1, AV61Atributos_TabelaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Atributos_Nome2, AV62Atributos_TabelaNom2, AV20DynamicFiltersEnabled2, AV67TFAtributos_Nome, AV68TFAtributos_Nome_Sel, AV75TFAtributos_Detalhes, AV76TFAtributos_Detalhes_Sel, AV79TFAtributos_TabelaNom, AV80TFAtributos_TabelaNom_Sel, AV83TFAtributos_Ativo_Sel, AV86TFAtributos_PK_Sel, AV89TFAtributos_FK_Sel, AV69ddo_Atributos_NomeTitleControlIdToReplace, AV73ddo_Atributos_TipoDadosTitleControlIdToReplace, AV77ddo_Atributos_DetalhesTitleControlIdToReplace, AV81ddo_Atributos_TabelaNomTitleControlIdToReplace, AV84ddo_Atributos_AtivoTitleControlIdToReplace, AV87ddo_Atributos_PKTitleControlIdToReplace, AV90ddo_Atributos_FKTitleControlIdToReplace, AV72TFAtributos_TipoDados_Sels, AV134Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A176Atributos_Codigo, AV63Sistema_Codigo, A356Atributos_TabelaCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV111WWAtributosDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV112WWAtributosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV113WWAtributosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV114WWAtributosDS_4_Atributos_nome1 = AV17Atributos_Nome1;
         AV115WWAtributosDS_5_Atributos_tabelanom1 = AV61Atributos_TabelaNom1;
         AV116WWAtributosDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV117WWAtributosDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV118WWAtributosDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV119WWAtributosDS_9_Atributos_nome2 = AV23Atributos_Nome2;
         AV120WWAtributosDS_10_Atributos_tabelanom2 = AV62Atributos_TabelaNom2;
         AV121WWAtributosDS_11_Tfatributos_nome = AV67TFAtributos_Nome;
         AV122WWAtributosDS_12_Tfatributos_nome_sel = AV68TFAtributos_Nome_Sel;
         AV123WWAtributosDS_13_Tfatributos_tipodados_sels = AV72TFAtributos_TipoDados_Sels;
         AV124WWAtributosDS_14_Tfatributos_detalhes = AV75TFAtributos_Detalhes;
         AV125WWAtributosDS_15_Tfatributos_detalhes_sel = AV76TFAtributos_Detalhes_Sel;
         AV126WWAtributosDS_16_Tfatributos_tabelanom = AV79TFAtributos_TabelaNom;
         AV127WWAtributosDS_17_Tfatributos_tabelanom_sel = AV80TFAtributos_TabelaNom_Sel;
         AV128WWAtributosDS_18_Tfatributos_ativo_sel = AV83TFAtributos_Ativo_Sel;
         AV129WWAtributosDS_19_Tfatributos_pk_sel = AV86TFAtributos_PK_Sel;
         AV130WWAtributosDS_20_Tfatributos_fk_sel = AV89TFAtributos_FK_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV60Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Atributos_Nome1, AV61Atributos_TabelaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Atributos_Nome2, AV62Atributos_TabelaNom2, AV20DynamicFiltersEnabled2, AV67TFAtributos_Nome, AV68TFAtributos_Nome_Sel, AV75TFAtributos_Detalhes, AV76TFAtributos_Detalhes_Sel, AV79TFAtributos_TabelaNom, AV80TFAtributos_TabelaNom_Sel, AV83TFAtributos_Ativo_Sel, AV86TFAtributos_PK_Sel, AV89TFAtributos_FK_Sel, AV69ddo_Atributos_NomeTitleControlIdToReplace, AV73ddo_Atributos_TipoDadosTitleControlIdToReplace, AV77ddo_Atributos_DetalhesTitleControlIdToReplace, AV81ddo_Atributos_TabelaNomTitleControlIdToReplace, AV84ddo_Atributos_AtivoTitleControlIdToReplace, AV87ddo_Atributos_PKTitleControlIdToReplace, AV90ddo_Atributos_FKTitleControlIdToReplace, AV72TFAtributos_TipoDados_Sels, AV134Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A176Atributos_Codigo, AV63Sistema_Codigo, A356Atributos_TabelaCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUP4C0( )
      {
         /* Before Start, stand alone formulas. */
         AV134Pgmname = "WWAtributos";
         context.Gx_err = 0;
         GXVvSISTEMA_AREATRABALHOCOD_html4C2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E264C2 */
         E264C2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV91DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vATRIBUTOS_NOMETITLEFILTERDATA"), AV66Atributos_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vATRIBUTOS_TIPODADOSTITLEFILTERDATA"), AV70Atributos_TipoDadosTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vATRIBUTOS_DETALHESTITLEFILTERDATA"), AV74Atributos_DetalhesTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vATRIBUTOS_TABELANOMTITLEFILTERDATA"), AV78Atributos_TabelaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vATRIBUTOS_ATIVOTITLEFILTERDATA"), AV82Atributos_AtivoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vATRIBUTOS_PKTITLEFILTERDATA"), AV85Atributos_PKTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vATRIBUTOS_FKTITLEFILTERDATA"), AV88Atributos_FKTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            dynavSistema_areatrabalhocod.Name = dynavSistema_areatrabalhocod_Internalname;
            dynavSistema_areatrabalhocod.CurrentValue = cgiGet( dynavSistema_areatrabalhocod_Internalname);
            AV60Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynavSistema_areatrabalhocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Atributos_Nome1 = StringUtil.Upper( cgiGet( edtavAtributos_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Atributos_Nome1", AV17Atributos_Nome1);
            AV61Atributos_TabelaNom1 = StringUtil.Upper( cgiGet( edtavAtributos_tabelanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Atributos_TabelaNom1", AV61Atributos_TabelaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23Atributos_Nome2 = StringUtil.Upper( cgiGet( edtavAtributos_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Atributos_Nome2", AV23Atributos_Nome2);
            AV62Atributos_TabelaNom2 = StringUtil.Upper( cgiGet( edtavAtributos_tabelanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Atributos_TabelaNom2", AV62Atributos_TabelaNom2);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV67TFAtributos_Nome = StringUtil.Upper( cgiGet( edtavTfatributos_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFAtributos_Nome", AV67TFAtributos_Nome);
            AV68TFAtributos_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfatributos_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFAtributos_Nome_Sel", AV68TFAtributos_Nome_Sel);
            AV75TFAtributos_Detalhes = cgiGet( edtavTfatributos_detalhes_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFAtributos_Detalhes", AV75TFAtributos_Detalhes);
            AV76TFAtributos_Detalhes_Sel = cgiGet( edtavTfatributos_detalhes_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFAtributos_Detalhes_Sel", AV76TFAtributos_Detalhes_Sel);
            AV79TFAtributos_TabelaNom = StringUtil.Upper( cgiGet( edtavTfatributos_tabelanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFAtributos_TabelaNom", AV79TFAtributos_TabelaNom);
            AV80TFAtributos_TabelaNom_Sel = StringUtil.Upper( cgiGet( edtavTfatributos_tabelanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAtributos_TabelaNom_Sel", AV80TFAtributos_TabelaNom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfatributos_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfatributos_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFATRIBUTOS_ATIVO_SEL");
               GX_FocusControl = edtavTfatributos_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83TFAtributos_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAtributos_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFAtributos_Ativo_Sel), 1, 0));
            }
            else
            {
               AV83TFAtributos_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfatributos_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAtributos_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFAtributos_Ativo_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfatributos_pk_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfatributos_pk_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFATRIBUTOS_PK_SEL");
               GX_FocusControl = edtavTfatributos_pk_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV86TFAtributos_PK_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFAtributos_PK_Sel", StringUtil.Str( (decimal)(AV86TFAtributos_PK_Sel), 1, 0));
            }
            else
            {
               AV86TFAtributos_PK_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfatributos_pk_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFAtributos_PK_Sel", StringUtil.Str( (decimal)(AV86TFAtributos_PK_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfatributos_fk_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfatributos_fk_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFATRIBUTOS_FK_SEL");
               GX_FocusControl = edtavTfatributos_fk_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV89TFAtributos_FK_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFAtributos_FK_Sel", StringUtil.Str( (decimal)(AV89TFAtributos_FK_Sel), 1, 0));
            }
            else
            {
               AV89TFAtributos_FK_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfatributos_fk_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFAtributos_FK_Sel", StringUtil.Str( (decimal)(AV89TFAtributos_FK_Sel), 1, 0));
            }
            AV69ddo_Atributos_NomeTitleControlIdToReplace = cgiGet( edtavDdo_atributos_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Atributos_NomeTitleControlIdToReplace", AV69ddo_Atributos_NomeTitleControlIdToReplace);
            AV73ddo_Atributos_TipoDadosTitleControlIdToReplace = cgiGet( edtavDdo_atributos_tipodadostitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Atributos_TipoDadosTitleControlIdToReplace", AV73ddo_Atributos_TipoDadosTitleControlIdToReplace);
            AV77ddo_Atributos_DetalhesTitleControlIdToReplace = cgiGet( edtavDdo_atributos_detalhestitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Atributos_DetalhesTitleControlIdToReplace", AV77ddo_Atributos_DetalhesTitleControlIdToReplace);
            AV81ddo_Atributos_TabelaNomTitleControlIdToReplace = cgiGet( edtavDdo_atributos_tabelanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Atributos_TabelaNomTitleControlIdToReplace", AV81ddo_Atributos_TabelaNomTitleControlIdToReplace);
            AV84ddo_Atributos_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_atributos_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Atributos_AtivoTitleControlIdToReplace", AV84ddo_Atributos_AtivoTitleControlIdToReplace);
            AV87ddo_Atributos_PKTitleControlIdToReplace = cgiGet( edtavDdo_atributos_pktitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_Atributos_PKTitleControlIdToReplace", AV87ddo_Atributos_PKTitleControlIdToReplace);
            AV90ddo_Atributos_FKTitleControlIdToReplace = cgiGet( edtavDdo_atributos_fktitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ddo_Atributos_FKTitleControlIdToReplace", AV90ddo_Atributos_FKTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_79 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_79"), ",", "."));
            AV93GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV94GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_atributos_nome_Caption = cgiGet( "DDO_ATRIBUTOS_NOME_Caption");
            Ddo_atributos_nome_Tooltip = cgiGet( "DDO_ATRIBUTOS_NOME_Tooltip");
            Ddo_atributos_nome_Cls = cgiGet( "DDO_ATRIBUTOS_NOME_Cls");
            Ddo_atributos_nome_Filteredtext_set = cgiGet( "DDO_ATRIBUTOS_NOME_Filteredtext_set");
            Ddo_atributos_nome_Selectedvalue_set = cgiGet( "DDO_ATRIBUTOS_NOME_Selectedvalue_set");
            Ddo_atributos_nome_Dropdownoptionstype = cgiGet( "DDO_ATRIBUTOS_NOME_Dropdownoptionstype");
            Ddo_atributos_nome_Titlecontrolidtoreplace = cgiGet( "DDO_ATRIBUTOS_NOME_Titlecontrolidtoreplace");
            Ddo_atributos_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_NOME_Includesortasc"));
            Ddo_atributos_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_NOME_Includesortdsc"));
            Ddo_atributos_nome_Sortedstatus = cgiGet( "DDO_ATRIBUTOS_NOME_Sortedstatus");
            Ddo_atributos_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_NOME_Includefilter"));
            Ddo_atributos_nome_Filtertype = cgiGet( "DDO_ATRIBUTOS_NOME_Filtertype");
            Ddo_atributos_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_NOME_Filterisrange"));
            Ddo_atributos_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_NOME_Includedatalist"));
            Ddo_atributos_nome_Datalisttype = cgiGet( "DDO_ATRIBUTOS_NOME_Datalisttype");
            Ddo_atributos_nome_Datalistfixedvalues = cgiGet( "DDO_ATRIBUTOS_NOME_Datalistfixedvalues");
            Ddo_atributos_nome_Datalistproc = cgiGet( "DDO_ATRIBUTOS_NOME_Datalistproc");
            Ddo_atributos_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ATRIBUTOS_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_atributos_nome_Sortasc = cgiGet( "DDO_ATRIBUTOS_NOME_Sortasc");
            Ddo_atributos_nome_Sortdsc = cgiGet( "DDO_ATRIBUTOS_NOME_Sortdsc");
            Ddo_atributos_nome_Loadingdata = cgiGet( "DDO_ATRIBUTOS_NOME_Loadingdata");
            Ddo_atributos_nome_Cleanfilter = cgiGet( "DDO_ATRIBUTOS_NOME_Cleanfilter");
            Ddo_atributos_nome_Rangefilterfrom = cgiGet( "DDO_ATRIBUTOS_NOME_Rangefilterfrom");
            Ddo_atributos_nome_Rangefilterto = cgiGet( "DDO_ATRIBUTOS_NOME_Rangefilterto");
            Ddo_atributos_nome_Noresultsfound = cgiGet( "DDO_ATRIBUTOS_NOME_Noresultsfound");
            Ddo_atributos_nome_Searchbuttontext = cgiGet( "DDO_ATRIBUTOS_NOME_Searchbuttontext");
            Ddo_atributos_tipodados_Caption = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Caption");
            Ddo_atributos_tipodados_Tooltip = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Tooltip");
            Ddo_atributos_tipodados_Cls = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Cls");
            Ddo_atributos_tipodados_Selectedvalue_set = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Selectedvalue_set");
            Ddo_atributos_tipodados_Dropdownoptionstype = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Dropdownoptionstype");
            Ddo_atributos_tipodados_Titlecontrolidtoreplace = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Titlecontrolidtoreplace");
            Ddo_atributos_tipodados_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Includesortasc"));
            Ddo_atributos_tipodados_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Includesortdsc"));
            Ddo_atributos_tipodados_Sortedstatus = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Sortedstatus");
            Ddo_atributos_tipodados_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Includefilter"));
            Ddo_atributos_tipodados_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Filterisrange"));
            Ddo_atributos_tipodados_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Includedatalist"));
            Ddo_atributos_tipodados_Datalisttype = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Datalisttype");
            Ddo_atributos_tipodados_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Allowmultipleselection"));
            Ddo_atributos_tipodados_Datalistfixedvalues = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Datalistfixedvalues");
            Ddo_atributos_tipodados_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_atributos_tipodados_Sortasc = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Sortasc");
            Ddo_atributos_tipodados_Sortdsc = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Sortdsc");
            Ddo_atributos_tipodados_Loadingdata = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Loadingdata");
            Ddo_atributos_tipodados_Cleanfilter = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Cleanfilter");
            Ddo_atributos_tipodados_Rangefilterfrom = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Rangefilterfrom");
            Ddo_atributos_tipodados_Rangefilterto = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Rangefilterto");
            Ddo_atributos_tipodados_Noresultsfound = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Noresultsfound");
            Ddo_atributos_tipodados_Searchbuttontext = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Searchbuttontext");
            Ddo_atributos_detalhes_Caption = cgiGet( "DDO_ATRIBUTOS_DETALHES_Caption");
            Ddo_atributos_detalhes_Tooltip = cgiGet( "DDO_ATRIBUTOS_DETALHES_Tooltip");
            Ddo_atributos_detalhes_Cls = cgiGet( "DDO_ATRIBUTOS_DETALHES_Cls");
            Ddo_atributos_detalhes_Filteredtext_set = cgiGet( "DDO_ATRIBUTOS_DETALHES_Filteredtext_set");
            Ddo_atributos_detalhes_Selectedvalue_set = cgiGet( "DDO_ATRIBUTOS_DETALHES_Selectedvalue_set");
            Ddo_atributos_detalhes_Dropdownoptionstype = cgiGet( "DDO_ATRIBUTOS_DETALHES_Dropdownoptionstype");
            Ddo_atributos_detalhes_Titlecontrolidtoreplace = cgiGet( "DDO_ATRIBUTOS_DETALHES_Titlecontrolidtoreplace");
            Ddo_atributos_detalhes_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_DETALHES_Includesortasc"));
            Ddo_atributos_detalhes_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_DETALHES_Includesortdsc"));
            Ddo_atributos_detalhes_Sortedstatus = cgiGet( "DDO_ATRIBUTOS_DETALHES_Sortedstatus");
            Ddo_atributos_detalhes_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_DETALHES_Includefilter"));
            Ddo_atributos_detalhes_Filtertype = cgiGet( "DDO_ATRIBUTOS_DETALHES_Filtertype");
            Ddo_atributos_detalhes_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_DETALHES_Filterisrange"));
            Ddo_atributos_detalhes_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_DETALHES_Includedatalist"));
            Ddo_atributos_detalhes_Datalisttype = cgiGet( "DDO_ATRIBUTOS_DETALHES_Datalisttype");
            Ddo_atributos_detalhes_Datalistfixedvalues = cgiGet( "DDO_ATRIBUTOS_DETALHES_Datalistfixedvalues");
            Ddo_atributos_detalhes_Datalistproc = cgiGet( "DDO_ATRIBUTOS_DETALHES_Datalistproc");
            Ddo_atributos_detalhes_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ATRIBUTOS_DETALHES_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_atributos_detalhes_Sortasc = cgiGet( "DDO_ATRIBUTOS_DETALHES_Sortasc");
            Ddo_atributos_detalhes_Sortdsc = cgiGet( "DDO_ATRIBUTOS_DETALHES_Sortdsc");
            Ddo_atributos_detalhes_Loadingdata = cgiGet( "DDO_ATRIBUTOS_DETALHES_Loadingdata");
            Ddo_atributos_detalhes_Cleanfilter = cgiGet( "DDO_ATRIBUTOS_DETALHES_Cleanfilter");
            Ddo_atributos_detalhes_Rangefilterfrom = cgiGet( "DDO_ATRIBUTOS_DETALHES_Rangefilterfrom");
            Ddo_atributos_detalhes_Rangefilterto = cgiGet( "DDO_ATRIBUTOS_DETALHES_Rangefilterto");
            Ddo_atributos_detalhes_Noresultsfound = cgiGet( "DDO_ATRIBUTOS_DETALHES_Noresultsfound");
            Ddo_atributos_detalhes_Searchbuttontext = cgiGet( "DDO_ATRIBUTOS_DETALHES_Searchbuttontext");
            Ddo_atributos_tabelanom_Caption = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Caption");
            Ddo_atributos_tabelanom_Tooltip = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Tooltip");
            Ddo_atributos_tabelanom_Cls = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Cls");
            Ddo_atributos_tabelanom_Filteredtext_set = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Filteredtext_set");
            Ddo_atributos_tabelanom_Selectedvalue_set = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Selectedvalue_set");
            Ddo_atributos_tabelanom_Dropdownoptionstype = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Dropdownoptionstype");
            Ddo_atributos_tabelanom_Titlecontrolidtoreplace = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Titlecontrolidtoreplace");
            Ddo_atributos_tabelanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_TABELANOM_Includesortasc"));
            Ddo_atributos_tabelanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_TABELANOM_Includesortdsc"));
            Ddo_atributos_tabelanom_Sortedstatus = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Sortedstatus");
            Ddo_atributos_tabelanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_TABELANOM_Includefilter"));
            Ddo_atributos_tabelanom_Filtertype = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Filtertype");
            Ddo_atributos_tabelanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_TABELANOM_Filterisrange"));
            Ddo_atributos_tabelanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_TABELANOM_Includedatalist"));
            Ddo_atributos_tabelanom_Datalisttype = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Datalisttype");
            Ddo_atributos_tabelanom_Datalistfixedvalues = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Datalistfixedvalues");
            Ddo_atributos_tabelanom_Datalistproc = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Datalistproc");
            Ddo_atributos_tabelanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ATRIBUTOS_TABELANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_atributos_tabelanom_Sortasc = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Sortasc");
            Ddo_atributos_tabelanom_Sortdsc = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Sortdsc");
            Ddo_atributos_tabelanom_Loadingdata = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Loadingdata");
            Ddo_atributos_tabelanom_Cleanfilter = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Cleanfilter");
            Ddo_atributos_tabelanom_Rangefilterfrom = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Rangefilterfrom");
            Ddo_atributos_tabelanom_Rangefilterto = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Rangefilterto");
            Ddo_atributos_tabelanom_Noresultsfound = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Noresultsfound");
            Ddo_atributos_tabelanom_Searchbuttontext = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Searchbuttontext");
            Ddo_atributos_ativo_Caption = cgiGet( "DDO_ATRIBUTOS_ATIVO_Caption");
            Ddo_atributos_ativo_Tooltip = cgiGet( "DDO_ATRIBUTOS_ATIVO_Tooltip");
            Ddo_atributos_ativo_Cls = cgiGet( "DDO_ATRIBUTOS_ATIVO_Cls");
            Ddo_atributos_ativo_Selectedvalue_set = cgiGet( "DDO_ATRIBUTOS_ATIVO_Selectedvalue_set");
            Ddo_atributos_ativo_Dropdownoptionstype = cgiGet( "DDO_ATRIBUTOS_ATIVO_Dropdownoptionstype");
            Ddo_atributos_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_ATRIBUTOS_ATIVO_Titlecontrolidtoreplace");
            Ddo_atributos_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_ATIVO_Includesortasc"));
            Ddo_atributos_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_ATIVO_Includesortdsc"));
            Ddo_atributos_ativo_Sortedstatus = cgiGet( "DDO_ATRIBUTOS_ATIVO_Sortedstatus");
            Ddo_atributos_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_ATIVO_Includefilter"));
            Ddo_atributos_ativo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_ATIVO_Filterisrange"));
            Ddo_atributos_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_ATIVO_Includedatalist"));
            Ddo_atributos_ativo_Datalisttype = cgiGet( "DDO_ATRIBUTOS_ATIVO_Datalisttype");
            Ddo_atributos_ativo_Datalistfixedvalues = cgiGet( "DDO_ATRIBUTOS_ATIVO_Datalistfixedvalues");
            Ddo_atributos_ativo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ATRIBUTOS_ATIVO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_atributos_ativo_Sortasc = cgiGet( "DDO_ATRIBUTOS_ATIVO_Sortasc");
            Ddo_atributos_ativo_Sortdsc = cgiGet( "DDO_ATRIBUTOS_ATIVO_Sortdsc");
            Ddo_atributos_ativo_Loadingdata = cgiGet( "DDO_ATRIBUTOS_ATIVO_Loadingdata");
            Ddo_atributos_ativo_Cleanfilter = cgiGet( "DDO_ATRIBUTOS_ATIVO_Cleanfilter");
            Ddo_atributos_ativo_Rangefilterfrom = cgiGet( "DDO_ATRIBUTOS_ATIVO_Rangefilterfrom");
            Ddo_atributos_ativo_Rangefilterto = cgiGet( "DDO_ATRIBUTOS_ATIVO_Rangefilterto");
            Ddo_atributos_ativo_Noresultsfound = cgiGet( "DDO_ATRIBUTOS_ATIVO_Noresultsfound");
            Ddo_atributos_ativo_Searchbuttontext = cgiGet( "DDO_ATRIBUTOS_ATIVO_Searchbuttontext");
            Ddo_atributos_pk_Caption = cgiGet( "DDO_ATRIBUTOS_PK_Caption");
            Ddo_atributos_pk_Tooltip = cgiGet( "DDO_ATRIBUTOS_PK_Tooltip");
            Ddo_atributos_pk_Cls = cgiGet( "DDO_ATRIBUTOS_PK_Cls");
            Ddo_atributos_pk_Selectedvalue_set = cgiGet( "DDO_ATRIBUTOS_PK_Selectedvalue_set");
            Ddo_atributos_pk_Dropdownoptionstype = cgiGet( "DDO_ATRIBUTOS_PK_Dropdownoptionstype");
            Ddo_atributos_pk_Titlecontrolidtoreplace = cgiGet( "DDO_ATRIBUTOS_PK_Titlecontrolidtoreplace");
            Ddo_atributos_pk_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_PK_Includesortasc"));
            Ddo_atributos_pk_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_PK_Includesortdsc"));
            Ddo_atributos_pk_Sortedstatus = cgiGet( "DDO_ATRIBUTOS_PK_Sortedstatus");
            Ddo_atributos_pk_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_PK_Includefilter"));
            Ddo_atributos_pk_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_PK_Filterisrange"));
            Ddo_atributos_pk_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_PK_Includedatalist"));
            Ddo_atributos_pk_Datalisttype = cgiGet( "DDO_ATRIBUTOS_PK_Datalisttype");
            Ddo_atributos_pk_Datalistfixedvalues = cgiGet( "DDO_ATRIBUTOS_PK_Datalistfixedvalues");
            Ddo_atributos_pk_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ATRIBUTOS_PK_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_atributos_pk_Sortasc = cgiGet( "DDO_ATRIBUTOS_PK_Sortasc");
            Ddo_atributos_pk_Sortdsc = cgiGet( "DDO_ATRIBUTOS_PK_Sortdsc");
            Ddo_atributos_pk_Loadingdata = cgiGet( "DDO_ATRIBUTOS_PK_Loadingdata");
            Ddo_atributos_pk_Cleanfilter = cgiGet( "DDO_ATRIBUTOS_PK_Cleanfilter");
            Ddo_atributos_pk_Rangefilterfrom = cgiGet( "DDO_ATRIBUTOS_PK_Rangefilterfrom");
            Ddo_atributos_pk_Rangefilterto = cgiGet( "DDO_ATRIBUTOS_PK_Rangefilterto");
            Ddo_atributos_pk_Noresultsfound = cgiGet( "DDO_ATRIBUTOS_PK_Noresultsfound");
            Ddo_atributos_pk_Searchbuttontext = cgiGet( "DDO_ATRIBUTOS_PK_Searchbuttontext");
            Ddo_atributos_fk_Caption = cgiGet( "DDO_ATRIBUTOS_FK_Caption");
            Ddo_atributos_fk_Tooltip = cgiGet( "DDO_ATRIBUTOS_FK_Tooltip");
            Ddo_atributos_fk_Cls = cgiGet( "DDO_ATRIBUTOS_FK_Cls");
            Ddo_atributos_fk_Selectedvalue_set = cgiGet( "DDO_ATRIBUTOS_FK_Selectedvalue_set");
            Ddo_atributos_fk_Dropdownoptionstype = cgiGet( "DDO_ATRIBUTOS_FK_Dropdownoptionstype");
            Ddo_atributos_fk_Titlecontrolidtoreplace = cgiGet( "DDO_ATRIBUTOS_FK_Titlecontrolidtoreplace");
            Ddo_atributos_fk_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_FK_Includesortasc"));
            Ddo_atributos_fk_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_FK_Includesortdsc"));
            Ddo_atributos_fk_Sortedstatus = cgiGet( "DDO_ATRIBUTOS_FK_Sortedstatus");
            Ddo_atributos_fk_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_FK_Includefilter"));
            Ddo_atributos_fk_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_FK_Filterisrange"));
            Ddo_atributos_fk_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ATRIBUTOS_FK_Includedatalist"));
            Ddo_atributos_fk_Datalisttype = cgiGet( "DDO_ATRIBUTOS_FK_Datalisttype");
            Ddo_atributos_fk_Datalistfixedvalues = cgiGet( "DDO_ATRIBUTOS_FK_Datalistfixedvalues");
            Ddo_atributos_fk_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ATRIBUTOS_FK_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_atributos_fk_Sortasc = cgiGet( "DDO_ATRIBUTOS_FK_Sortasc");
            Ddo_atributos_fk_Sortdsc = cgiGet( "DDO_ATRIBUTOS_FK_Sortdsc");
            Ddo_atributos_fk_Loadingdata = cgiGet( "DDO_ATRIBUTOS_FK_Loadingdata");
            Ddo_atributos_fk_Cleanfilter = cgiGet( "DDO_ATRIBUTOS_FK_Cleanfilter");
            Ddo_atributos_fk_Rangefilterfrom = cgiGet( "DDO_ATRIBUTOS_FK_Rangefilterfrom");
            Ddo_atributos_fk_Rangefilterto = cgiGet( "DDO_ATRIBUTOS_FK_Rangefilterto");
            Ddo_atributos_fk_Noresultsfound = cgiGet( "DDO_ATRIBUTOS_FK_Noresultsfound");
            Ddo_atributos_fk_Searchbuttontext = cgiGet( "DDO_ATRIBUTOS_FK_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_atributos_nome_Activeeventkey = cgiGet( "DDO_ATRIBUTOS_NOME_Activeeventkey");
            Ddo_atributos_nome_Filteredtext_get = cgiGet( "DDO_ATRIBUTOS_NOME_Filteredtext_get");
            Ddo_atributos_nome_Selectedvalue_get = cgiGet( "DDO_ATRIBUTOS_NOME_Selectedvalue_get");
            Ddo_atributos_tipodados_Activeeventkey = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Activeeventkey");
            Ddo_atributos_tipodados_Selectedvalue_get = cgiGet( "DDO_ATRIBUTOS_TIPODADOS_Selectedvalue_get");
            Ddo_atributos_detalhes_Activeeventkey = cgiGet( "DDO_ATRIBUTOS_DETALHES_Activeeventkey");
            Ddo_atributos_detalhes_Filteredtext_get = cgiGet( "DDO_ATRIBUTOS_DETALHES_Filteredtext_get");
            Ddo_atributos_detalhes_Selectedvalue_get = cgiGet( "DDO_ATRIBUTOS_DETALHES_Selectedvalue_get");
            Ddo_atributos_tabelanom_Activeeventkey = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Activeeventkey");
            Ddo_atributos_tabelanom_Filteredtext_get = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Filteredtext_get");
            Ddo_atributos_tabelanom_Selectedvalue_get = cgiGet( "DDO_ATRIBUTOS_TABELANOM_Selectedvalue_get");
            Ddo_atributos_ativo_Activeeventkey = cgiGet( "DDO_ATRIBUTOS_ATIVO_Activeeventkey");
            Ddo_atributos_ativo_Selectedvalue_get = cgiGet( "DDO_ATRIBUTOS_ATIVO_Selectedvalue_get");
            Ddo_atributos_pk_Activeeventkey = cgiGet( "DDO_ATRIBUTOS_PK_Activeeventkey");
            Ddo_atributos_pk_Selectedvalue_get = cgiGet( "DDO_ATRIBUTOS_PK_Selectedvalue_get");
            Ddo_atributos_fk_Activeeventkey = cgiGet( "DDO_ATRIBUTOS_FK_Activeeventkey");
            Ddo_atributos_fk_Selectedvalue_get = cgiGet( "DDO_ATRIBUTOS_FK_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSISTEMA_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV60Sistema_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vATRIBUTOS_NOME1"), AV17Atributos_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vATRIBUTOS_TABELANOM1"), AV61Atributos_TabelaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vATRIBUTOS_NOME2"), AV23Atributos_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vATRIBUTOS_TABELANOM2"), AV62Atributos_TabelaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_NOME"), AV67TFAtributos_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_NOME_SEL"), AV68TFAtributos_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_DETALHES"), AV75TFAtributos_Detalhes) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_DETALHES_SEL"), AV76TFAtributos_Detalhes_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_TABELANOM"), AV79TFAtributos_TabelaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFATRIBUTOS_TABELANOM_SEL"), AV80TFAtributos_TabelaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFATRIBUTOS_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV83TFAtributos_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFATRIBUTOS_PK_SEL"), ",", ".") != Convert.ToDecimal( AV86TFAtributos_PK_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFATRIBUTOS_FK_SEL"), ",", ".") != Convert.ToDecimal( AV89TFAtributos_FK_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E264C2 */
         E264C2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E264C2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "ATRIBUTOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "ATRIBUTOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfatributos_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfatributos_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfatributos_nome_Visible), 5, 0)));
         edtavTfatributos_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfatributos_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfatributos_nome_sel_Visible), 5, 0)));
         edtavTfatributos_detalhes_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfatributos_detalhes_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfatributos_detalhes_Visible), 5, 0)));
         edtavTfatributos_detalhes_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfatributos_detalhes_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfatributos_detalhes_sel_Visible), 5, 0)));
         edtavTfatributos_tabelanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfatributos_tabelanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfatributos_tabelanom_Visible), 5, 0)));
         edtavTfatributos_tabelanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfatributos_tabelanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfatributos_tabelanom_sel_Visible), 5, 0)));
         edtavTfatributos_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfatributos_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfatributos_ativo_sel_Visible), 5, 0)));
         edtavTfatributos_pk_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfatributos_pk_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfatributos_pk_sel_Visible), 5, 0)));
         edtavTfatributos_fk_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfatributos_fk_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfatributos_fk_sel_Visible), 5, 0)));
         Ddo_atributos_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Atributos_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_nome_Internalname, "TitleControlIdToReplace", Ddo_atributos_nome_Titlecontrolidtoreplace);
         AV69ddo_Atributos_NomeTitleControlIdToReplace = Ddo_atributos_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Atributos_NomeTitleControlIdToReplace", AV69ddo_Atributos_NomeTitleControlIdToReplace);
         edtavDdo_atributos_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_atributos_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_atributos_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_atributos_tipodados_Titlecontrolidtoreplace = subGrid_Internalname+"_Atributos_TipoDados";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tipodados_Internalname, "TitleControlIdToReplace", Ddo_atributos_tipodados_Titlecontrolidtoreplace);
         AV73ddo_Atributos_TipoDadosTitleControlIdToReplace = Ddo_atributos_tipodados_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Atributos_TipoDadosTitleControlIdToReplace", AV73ddo_Atributos_TipoDadosTitleControlIdToReplace);
         edtavDdo_atributos_tipodadostitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_atributos_tipodadostitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_atributos_tipodadostitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_atributos_detalhes_Titlecontrolidtoreplace = subGrid_Internalname+"_Atributos_Detalhes";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_detalhes_Internalname, "TitleControlIdToReplace", Ddo_atributos_detalhes_Titlecontrolidtoreplace);
         AV77ddo_Atributos_DetalhesTitleControlIdToReplace = Ddo_atributos_detalhes_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Atributos_DetalhesTitleControlIdToReplace", AV77ddo_Atributos_DetalhesTitleControlIdToReplace);
         edtavDdo_atributos_detalhestitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_atributos_detalhestitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_atributos_detalhestitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_atributos_tabelanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Atributos_TabelaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tabelanom_Internalname, "TitleControlIdToReplace", Ddo_atributos_tabelanom_Titlecontrolidtoreplace);
         AV81ddo_Atributos_TabelaNomTitleControlIdToReplace = Ddo_atributos_tabelanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Atributos_TabelaNomTitleControlIdToReplace", AV81ddo_Atributos_TabelaNomTitleControlIdToReplace);
         edtavDdo_atributos_tabelanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_atributos_tabelanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_atributos_tabelanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_atributos_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Atributos_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_ativo_Internalname, "TitleControlIdToReplace", Ddo_atributos_ativo_Titlecontrolidtoreplace);
         AV84ddo_Atributos_AtivoTitleControlIdToReplace = Ddo_atributos_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Atributos_AtivoTitleControlIdToReplace", AV84ddo_Atributos_AtivoTitleControlIdToReplace);
         edtavDdo_atributos_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_atributos_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_atributos_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_atributos_pk_Titlecontrolidtoreplace = subGrid_Internalname+"_Atributos_PK";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_pk_Internalname, "TitleControlIdToReplace", Ddo_atributos_pk_Titlecontrolidtoreplace);
         AV87ddo_Atributos_PKTitleControlIdToReplace = Ddo_atributos_pk_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_Atributos_PKTitleControlIdToReplace", AV87ddo_Atributos_PKTitleControlIdToReplace);
         edtavDdo_atributos_pktitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_atributos_pktitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_atributos_pktitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_atributos_fk_Titlecontrolidtoreplace = subGrid_Internalname+"_Atributos_FK";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_fk_Internalname, "TitleControlIdToReplace", Ddo_atributos_fk_Titlecontrolidtoreplace);
         AV90ddo_Atributos_FKTitleControlIdToReplace = Ddo_atributos_fk_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ddo_Atributos_FKTitleControlIdToReplace", AV90ddo_Atributos_FKTitleControlIdToReplace);
         edtavDdo_atributos_fktitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_atributos_fktitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_atributos_fktitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Atributos";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         cmbavOrderedby.addItem("3", "Detalhes", 0);
         cmbavOrderedby.addItem("4", "Tabela", 0);
         cmbavOrderedby.addItem("5", "Ativo", 0);
         cmbavOrderedby.addItem("6", "PK", 0);
         cmbavOrderedby.addItem("7", "FK", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV91DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV91DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E274C2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV66Atributos_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70Atributos_TipoDadosTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74Atributos_DetalhesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78Atributos_TabelaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82Atributos_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV85Atributos_PKTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV88Atributos_FKTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtAtributos_Nome_Titleformat = 2;
         edtAtributos_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV69ddo_Atributos_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAtributos_Nome_Internalname, "Title", edtAtributos_Nome_Title);
         cmbAtributos_TipoDados_Titleformat = 2;
         cmbAtributos_TipoDados.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV73ddo_Atributos_TipoDadosTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAtributos_TipoDados_Internalname, "Title", cmbAtributos_TipoDados.Title.Text);
         edtAtributos_Detalhes_Titleformat = 2;
         edtAtributos_Detalhes_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Detalhes", AV77ddo_Atributos_DetalhesTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAtributos_Detalhes_Internalname, "Title", edtAtributos_Detalhes_Title);
         edtAtributos_TabelaNom_Titleformat = 2;
         edtAtributos_TabelaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tabela", AV81ddo_Atributos_TabelaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAtributos_TabelaNom_Internalname, "Title", edtAtributos_TabelaNom_Title);
         chkAtributos_Ativo_Titleformat = 2;
         chkAtributos_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV84ddo_Atributos_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_Ativo_Internalname, "Title", chkAtributos_Ativo.Title.Text);
         chkAtributos_PK_Titleformat = 2;
         chkAtributos_PK.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PK", AV87ddo_Atributos_PKTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_PK_Internalname, "Title", chkAtributos_PK.Title.Text);
         chkAtributos_FK_Titleformat = 2;
         chkAtributos_FK.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "FK", AV90ddo_Atributos_FKTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_FK_Internalname, "Title", chkAtributos_FK.Title.Text);
         AV93GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV93GridCurrentPage), 10, 0)));
         AV94GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV94GridPageCount), 10, 0)));
         AV60Sistema_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
         AV111WWAtributosDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV112WWAtributosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV113WWAtributosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV114WWAtributosDS_4_Atributos_nome1 = AV17Atributos_Nome1;
         AV115WWAtributosDS_5_Atributos_tabelanom1 = AV61Atributos_TabelaNom1;
         AV116WWAtributosDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV117WWAtributosDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV118WWAtributosDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV119WWAtributosDS_9_Atributos_nome2 = AV23Atributos_Nome2;
         AV120WWAtributosDS_10_Atributos_tabelanom2 = AV62Atributos_TabelaNom2;
         AV121WWAtributosDS_11_Tfatributos_nome = AV67TFAtributos_Nome;
         AV122WWAtributosDS_12_Tfatributos_nome_sel = AV68TFAtributos_Nome_Sel;
         AV123WWAtributosDS_13_Tfatributos_tipodados_sels = AV72TFAtributos_TipoDados_Sels;
         AV124WWAtributosDS_14_Tfatributos_detalhes = AV75TFAtributos_Detalhes;
         AV125WWAtributosDS_15_Tfatributos_detalhes_sel = AV76TFAtributos_Detalhes_Sel;
         AV126WWAtributosDS_16_Tfatributos_tabelanom = AV79TFAtributos_TabelaNom;
         AV127WWAtributosDS_17_Tfatributos_tabelanom_sel = AV80TFAtributos_TabelaNom_Sel;
         AV128WWAtributosDS_18_Tfatributos_ativo_sel = AV83TFAtributos_Ativo_Sel;
         AV129WWAtributosDS_19_Tfatributos_pk_sel = AV86TFAtributos_PK_Sel;
         AV130WWAtributosDS_20_Tfatributos_fk_sel = AV89TFAtributos_FK_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66Atributos_NomeTitleFilterData", AV66Atributos_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70Atributos_TipoDadosTitleFilterData", AV70Atributos_TipoDadosTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV74Atributos_DetalhesTitleFilterData", AV74Atributos_DetalhesTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV78Atributos_TabelaNomTitleFilterData", AV78Atributos_TabelaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82Atributos_AtivoTitleFilterData", AV82Atributos_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV85Atributos_PKTitleFilterData", AV85Atributos_PKTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV88Atributos_FKTitleFilterData", AV88Atributos_FKTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         dynavSistema_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_areatrabalhocod_Internalname, "Values", dynavSistema_areatrabalhocod.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E114C2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV92PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV92PageToGo) ;
         }
      }

      protected void E124C2( )
      {
         /* Ddo_atributos_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_atributos_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_nome_Internalname, "SortedStatus", Ddo_atributos_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_nome_Internalname, "SortedStatus", Ddo_atributos_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFAtributos_Nome = Ddo_atributos_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFAtributos_Nome", AV67TFAtributos_Nome);
            AV68TFAtributos_Nome_Sel = Ddo_atributos_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFAtributos_Nome_Sel", AV68TFAtributos_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E134C2( )
      {
         /* Ddo_atributos_tipodados_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_atributos_tipodados_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_tipodados_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tipodados_Internalname, "SortedStatus", Ddo_atributos_tipodados_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_tipodados_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_tipodados_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tipodados_Internalname, "SortedStatus", Ddo_atributos_tipodados_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_tipodados_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV71TFAtributos_TipoDados_SelsJson = Ddo_atributos_tipodados_Selectedvalue_get;
            AV72TFAtributos_TipoDados_Sels.FromJSonString(AV71TFAtributos_TipoDados_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72TFAtributos_TipoDados_Sels", AV72TFAtributos_TipoDados_Sels);
      }

      protected void E144C2( )
      {
         /* Ddo_atributos_detalhes_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_atributos_detalhes_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_detalhes_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_detalhes_Internalname, "SortedStatus", Ddo_atributos_detalhes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_detalhes_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_detalhes_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_detalhes_Internalname, "SortedStatus", Ddo_atributos_detalhes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_detalhes_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV75TFAtributos_Detalhes = Ddo_atributos_detalhes_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFAtributos_Detalhes", AV75TFAtributos_Detalhes);
            AV76TFAtributos_Detalhes_Sel = Ddo_atributos_detalhes_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFAtributos_Detalhes_Sel", AV76TFAtributos_Detalhes_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E154C2( )
      {
         /* Ddo_atributos_tabelanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_atributos_tabelanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_tabelanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tabelanom_Internalname, "SortedStatus", Ddo_atributos_tabelanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_tabelanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_tabelanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tabelanom_Internalname, "SortedStatus", Ddo_atributos_tabelanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_tabelanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV79TFAtributos_TabelaNom = Ddo_atributos_tabelanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFAtributos_TabelaNom", AV79TFAtributos_TabelaNom);
            AV80TFAtributos_TabelaNom_Sel = Ddo_atributos_tabelanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAtributos_TabelaNom_Sel", AV80TFAtributos_TabelaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E164C2( )
      {
         /* Ddo_atributos_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_atributos_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_ativo_Internalname, "SortedStatus", Ddo_atributos_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_ativo_Internalname, "SortedStatus", Ddo_atributos_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV83TFAtributos_Ativo_Sel = (short)(NumberUtil.Val( Ddo_atributos_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAtributos_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFAtributos_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E174C2( )
      {
         /* Ddo_atributos_pk_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_atributos_pk_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_pk_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_pk_Internalname, "SortedStatus", Ddo_atributos_pk_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_pk_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_pk_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_pk_Internalname, "SortedStatus", Ddo_atributos_pk_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_pk_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV86TFAtributos_PK_Sel = (short)(NumberUtil.Val( Ddo_atributos_pk_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFAtributos_PK_Sel", StringUtil.Str( (decimal)(AV86TFAtributos_PK_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E184C2( )
      {
         /* Ddo_atributos_fk_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_atributos_fk_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_fk_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_fk_Internalname, "SortedStatus", Ddo_atributos_fk_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_fk_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_atributos_fk_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_fk_Internalname, "SortedStatus", Ddo_atributos_fk_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_atributos_fk_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV89TFAtributos_FK_Sel = (short)(NumberUtil.Val( Ddo_atributos_fk_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFAtributos_FK_Sel", StringUtil.Str( (decimal)(AV89TFAtributos_FK_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E284C2( )
      {
         /* Grid_Load Routine */
         AV34Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV34Update);
         AV131Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode("" +AV63Sistema_Codigo);
         AV35Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV35Delete);
         AV132Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode("" +AV63Sistema_Codigo);
         AV58Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV58Display);
         AV133Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewatributos.aspx") + "?" + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtAtributos_Nome_Link = formatLink("viewatributos.aspx") + "?" + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtAtributos_TabelaNom_Link = formatLink("viewtabela.aspx") + "?" + UrlEncode("" +A356Atributos_TabelaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 79;
         }
         sendrow_792( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_79_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(79, GridRow);
         }
      }

      protected void E194C2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E234C2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV60Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Atributos_Nome1, AV61Atributos_TabelaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Atributos_Nome2, AV62Atributos_TabelaNom2, AV20DynamicFiltersEnabled2, AV67TFAtributos_Nome, AV68TFAtributos_Nome_Sel, AV75TFAtributos_Detalhes, AV76TFAtributos_Detalhes_Sel, AV79TFAtributos_TabelaNom, AV80TFAtributos_TabelaNom_Sel, AV83TFAtributos_Ativo_Sel, AV86TFAtributos_PK_Sel, AV89TFAtributos_FK_Sel, AV69ddo_Atributos_NomeTitleControlIdToReplace, AV73ddo_Atributos_TipoDadosTitleControlIdToReplace, AV77ddo_Atributos_DetalhesTitleControlIdToReplace, AV81ddo_Atributos_TabelaNomTitleControlIdToReplace, AV84ddo_Atributos_AtivoTitleControlIdToReplace, AV87ddo_Atributos_PKTitleControlIdToReplace, AV90ddo_Atributos_FKTitleControlIdToReplace, AV72TFAtributos_TipoDados_Sels, AV134Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A176Atributos_Codigo, AV63Sistema_Codigo, A356Atributos_TabelaCod) ;
      }

      protected void E204C2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV60Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Atributos_Nome1, AV61Atributos_TabelaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Atributos_Nome2, AV62Atributos_TabelaNom2, AV20DynamicFiltersEnabled2, AV67TFAtributos_Nome, AV68TFAtributos_Nome_Sel, AV75TFAtributos_Detalhes, AV76TFAtributos_Detalhes_Sel, AV79TFAtributos_TabelaNom, AV80TFAtributos_TabelaNom_Sel, AV83TFAtributos_Ativo_Sel, AV86TFAtributos_PK_Sel, AV89TFAtributos_FK_Sel, AV69ddo_Atributos_NomeTitleControlIdToReplace, AV73ddo_Atributos_TipoDadosTitleControlIdToReplace, AV77ddo_Atributos_DetalhesTitleControlIdToReplace, AV81ddo_Atributos_TabelaNomTitleControlIdToReplace, AV84ddo_Atributos_AtivoTitleControlIdToReplace, AV87ddo_Atributos_PKTitleControlIdToReplace, AV90ddo_Atributos_FKTitleControlIdToReplace, AV72TFAtributos_TipoDados_Sels, AV134Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A176Atributos_Codigo, AV63Sistema_Codigo, A356Atributos_TabelaCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E244C2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E214C2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV60Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Atributos_Nome1, AV61Atributos_TabelaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Atributos_Nome2, AV62Atributos_TabelaNom2, AV20DynamicFiltersEnabled2, AV67TFAtributos_Nome, AV68TFAtributos_Nome_Sel, AV75TFAtributos_Detalhes, AV76TFAtributos_Detalhes_Sel, AV79TFAtributos_TabelaNom, AV80TFAtributos_TabelaNom_Sel, AV83TFAtributos_Ativo_Sel, AV86TFAtributos_PK_Sel, AV89TFAtributos_FK_Sel, AV69ddo_Atributos_NomeTitleControlIdToReplace, AV73ddo_Atributos_TipoDadosTitleControlIdToReplace, AV77ddo_Atributos_DetalhesTitleControlIdToReplace, AV81ddo_Atributos_TabelaNomTitleControlIdToReplace, AV84ddo_Atributos_AtivoTitleControlIdToReplace, AV87ddo_Atributos_PKTitleControlIdToReplace, AV90ddo_Atributos_FKTitleControlIdToReplace, AV72TFAtributos_TipoDados_Sels, AV134Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A176Atributos_Codigo, AV63Sistema_Codigo, A356Atributos_TabelaCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E254C2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E224C2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavSistema_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_areatrabalhocod_Internalname, "Values", dynavSistema_areatrabalhocod.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72TFAtributos_TipoDados_Sels", AV72TFAtributos_TipoDados_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_atributos_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_nome_Internalname, "SortedStatus", Ddo_atributos_nome_Sortedstatus);
         Ddo_atributos_tipodados_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tipodados_Internalname, "SortedStatus", Ddo_atributos_tipodados_Sortedstatus);
         Ddo_atributos_detalhes_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_detalhes_Internalname, "SortedStatus", Ddo_atributos_detalhes_Sortedstatus);
         Ddo_atributos_tabelanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tabelanom_Internalname, "SortedStatus", Ddo_atributos_tabelanom_Sortedstatus);
         Ddo_atributos_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_ativo_Internalname, "SortedStatus", Ddo_atributos_ativo_Sortedstatus);
         Ddo_atributos_pk_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_pk_Internalname, "SortedStatus", Ddo_atributos_pk_Sortedstatus);
         Ddo_atributos_fk_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_fk_Internalname, "SortedStatus", Ddo_atributos_fk_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_atributos_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_nome_Internalname, "SortedStatus", Ddo_atributos_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_atributos_tipodados_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tipodados_Internalname, "SortedStatus", Ddo_atributos_tipodados_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_atributos_detalhes_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_detalhes_Internalname, "SortedStatus", Ddo_atributos_detalhes_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_atributos_tabelanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tabelanom_Internalname, "SortedStatus", Ddo_atributos_tabelanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_atributos_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_ativo_Internalname, "SortedStatus", Ddo_atributos_ativo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_atributos_pk_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_pk_Internalname, "SortedStatus", Ddo_atributos_pk_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_atributos_fk_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_fk_Internalname, "SortedStatus", Ddo_atributos_fk_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavAtributos_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtributos_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome1_Visible), 5, 0)));
         edtavAtributos_tabelanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtributos_tabelanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_tabelanom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 )
         {
            edtavAtributos_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtributos_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 )
         {
            edtavAtributos_tabelanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtributos_tabelanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_tabelanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavAtributos_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtributos_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome2_Visible), 5, 0)));
         edtavAtributos_tabelanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtributos_tabelanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_tabelanom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 )
         {
            edtavAtributos_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtributos_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 )
         {
            edtavAtributos_tabelanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtributos_tabelanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtributos_tabelanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "ATRIBUTOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23Atributos_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Atributos_Nome2", AV23Atributos_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV60Sistema_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
         AV67TFAtributos_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFAtributos_Nome", AV67TFAtributos_Nome);
         Ddo_atributos_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_nome_Internalname, "FilteredText_set", Ddo_atributos_nome_Filteredtext_set);
         AV68TFAtributos_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFAtributos_Nome_Sel", AV68TFAtributos_Nome_Sel);
         Ddo_atributos_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_nome_Internalname, "SelectedValue_set", Ddo_atributos_nome_Selectedvalue_set);
         AV72TFAtributos_TipoDados_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_atributos_tipodados_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tipodados_Internalname, "SelectedValue_set", Ddo_atributos_tipodados_Selectedvalue_set);
         AV75TFAtributos_Detalhes = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFAtributos_Detalhes", AV75TFAtributos_Detalhes);
         Ddo_atributos_detalhes_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_detalhes_Internalname, "FilteredText_set", Ddo_atributos_detalhes_Filteredtext_set);
         AV76TFAtributos_Detalhes_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFAtributos_Detalhes_Sel", AV76TFAtributos_Detalhes_Sel);
         Ddo_atributos_detalhes_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_detalhes_Internalname, "SelectedValue_set", Ddo_atributos_detalhes_Selectedvalue_set);
         AV79TFAtributos_TabelaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFAtributos_TabelaNom", AV79TFAtributos_TabelaNom);
         Ddo_atributos_tabelanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tabelanom_Internalname, "FilteredText_set", Ddo_atributos_tabelanom_Filteredtext_set);
         AV80TFAtributos_TabelaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAtributos_TabelaNom_Sel", AV80TFAtributos_TabelaNom_Sel);
         Ddo_atributos_tabelanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tabelanom_Internalname, "SelectedValue_set", Ddo_atributos_tabelanom_Selectedvalue_set);
         AV83TFAtributos_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAtributos_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFAtributos_Ativo_Sel), 1, 0));
         Ddo_atributos_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_ativo_Internalname, "SelectedValue_set", Ddo_atributos_ativo_Selectedvalue_set);
         AV86TFAtributos_PK_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFAtributos_PK_Sel", StringUtil.Str( (decimal)(AV86TFAtributos_PK_Sel), 1, 0));
         Ddo_atributos_pk_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_pk_Internalname, "SelectedValue_set", Ddo_atributos_pk_Selectedvalue_set);
         AV89TFAtributos_FK_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFAtributos_FK_Sel", StringUtil.Str( (decimal)(AV89TFAtributos_FK_Sel), 1, 0));
         Ddo_atributos_fk_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_fk_Internalname, "SelectedValue_set", Ddo_atributos_fk_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "ATRIBUTOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Atributos_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Atributos_Nome1", AV17Atributos_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV36Session.Get(AV134Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV134Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV36Session.Get(AV134Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV135GXV1 = 1;
         while ( AV135GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV135GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "SISTEMA_AREATRABALHOCOD") == 0 )
            {
               AV60Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_NOME") == 0 )
            {
               AV67TFAtributos_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFAtributos_Nome", AV67TFAtributos_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFAtributos_Nome)) )
               {
                  Ddo_atributos_nome_Filteredtext_set = AV67TFAtributos_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_nome_Internalname, "FilteredText_set", Ddo_atributos_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_NOME_SEL") == 0 )
            {
               AV68TFAtributos_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFAtributos_Nome_Sel", AV68TFAtributos_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFAtributos_Nome_Sel)) )
               {
                  Ddo_atributos_nome_Selectedvalue_set = AV68TFAtributos_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_nome_Internalname, "SelectedValue_set", Ddo_atributos_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_TIPODADOS_SEL") == 0 )
            {
               AV71TFAtributos_TipoDados_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV72TFAtributos_TipoDados_Sels.FromJSonString(AV71TFAtributos_TipoDados_SelsJson);
               if ( ! ( AV72TFAtributos_TipoDados_Sels.Count == 0 ) )
               {
                  Ddo_atributos_tipodados_Selectedvalue_set = AV71TFAtributos_TipoDados_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tipodados_Internalname, "SelectedValue_set", Ddo_atributos_tipodados_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_DETALHES") == 0 )
            {
               AV75TFAtributos_Detalhes = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFAtributos_Detalhes", AV75TFAtributos_Detalhes);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFAtributos_Detalhes)) )
               {
                  Ddo_atributos_detalhes_Filteredtext_set = AV75TFAtributos_Detalhes;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_detalhes_Internalname, "FilteredText_set", Ddo_atributos_detalhes_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_DETALHES_SEL") == 0 )
            {
               AV76TFAtributos_Detalhes_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFAtributos_Detalhes_Sel", AV76TFAtributos_Detalhes_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFAtributos_Detalhes_Sel)) )
               {
                  Ddo_atributos_detalhes_Selectedvalue_set = AV76TFAtributos_Detalhes_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_detalhes_Internalname, "SelectedValue_set", Ddo_atributos_detalhes_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_TABELANOM") == 0 )
            {
               AV79TFAtributos_TabelaNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFAtributos_TabelaNom", AV79TFAtributos_TabelaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFAtributos_TabelaNom)) )
               {
                  Ddo_atributos_tabelanom_Filteredtext_set = AV79TFAtributos_TabelaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tabelanom_Internalname, "FilteredText_set", Ddo_atributos_tabelanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_TABELANOM_SEL") == 0 )
            {
               AV80TFAtributos_TabelaNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAtributos_TabelaNom_Sel", AV80TFAtributos_TabelaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFAtributos_TabelaNom_Sel)) )
               {
                  Ddo_atributos_tabelanom_Selectedvalue_set = AV80TFAtributos_TabelaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_tabelanom_Internalname, "SelectedValue_set", Ddo_atributos_tabelanom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_ATIVO_SEL") == 0 )
            {
               AV83TFAtributos_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAtributos_Ativo_Sel", StringUtil.Str( (decimal)(AV83TFAtributos_Ativo_Sel), 1, 0));
               if ( ! (0==AV83TFAtributos_Ativo_Sel) )
               {
                  Ddo_atributos_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV83TFAtributos_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_ativo_Internalname, "SelectedValue_set", Ddo_atributos_ativo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_PK_SEL") == 0 )
            {
               AV86TFAtributos_PK_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFAtributos_PK_Sel", StringUtil.Str( (decimal)(AV86TFAtributos_PK_Sel), 1, 0));
               if ( ! (0==AV86TFAtributos_PK_Sel) )
               {
                  Ddo_atributos_pk_Selectedvalue_set = StringUtil.Str( (decimal)(AV86TFAtributos_PK_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_pk_Internalname, "SelectedValue_set", Ddo_atributos_pk_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_FK_SEL") == 0 )
            {
               AV89TFAtributos_FK_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFAtributos_FK_Sel", StringUtil.Str( (decimal)(AV89TFAtributos_FK_Sel), 1, 0));
               if ( ! (0==AV89TFAtributos_FK_Sel) )
               {
                  Ddo_atributos_fk_Selectedvalue_set = StringUtil.Str( (decimal)(AV89TFAtributos_FK_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_atributos_fk_Internalname, "SelectedValue_set", Ddo_atributos_fk_Selectedvalue_set);
               }
            }
            AV135GXV1 = (int)(AV135GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Atributos_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Atributos_Nome1", AV17Atributos_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV61Atributos_TabelaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Atributos_TabelaNom1", AV61Atributos_TabelaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23Atributos_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Atributos_Nome2", AV23Atributos_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV62Atributos_TabelaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Atributos_TabelaNom2", AV62Atributos_TabelaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV36Session.Get(AV134Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV60Sistema_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "SISTEMA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFAtributos_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFATRIBUTOS_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV67TFAtributos_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFAtributos_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFATRIBUTOS_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV68TFAtributos_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV72TFAtributos_TipoDados_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFATRIBUTOS_TIPODADOS_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV72TFAtributos_TipoDados_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFAtributos_Detalhes)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFATRIBUTOS_DETALHES";
            AV11GridStateFilterValue.gxTpr_Value = AV75TFAtributos_Detalhes;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFAtributos_Detalhes_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFATRIBUTOS_DETALHES_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV76TFAtributos_Detalhes_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFAtributos_TabelaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFATRIBUTOS_TABELANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV79TFAtributos_TabelaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFAtributos_TabelaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFATRIBUTOS_TABELANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV80TFAtributos_TabelaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV83TFAtributos_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFATRIBUTOS_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV83TFAtributos_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV86TFAtributos_PK_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFATRIBUTOS_PK_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV86TFAtributos_PK_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV89TFAtributos_FK_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFATRIBUTOS_FK_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV89TFAtributos_FK_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV134Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Atributos_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Atributos_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV61Atributos_TabelaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV61Atributos_TabelaNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Atributos_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23Atributos_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV62Atributos_TabelaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV62Atributos_TabelaNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV134Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Atributos";
         AV36Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_4C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_4C2( true) ;
         }
         else
         {
            wb_table2_8_4C2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_4C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_73_4C2( true) ;
         }
         else
         {
            wb_table3_73_4C2( false) ;
         }
         return  ;
      }

      protected void wb_table3_73_4C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4C2e( true) ;
         }
         else
         {
            wb_table1_2_4C2e( false) ;
         }
      }

      protected void wb_table3_73_4C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_76_4C2( true) ;
         }
         else
         {
            wb_table4_76_4C2( false) ;
         }
         return  ;
      }

      protected void wb_table4_76_4C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_73_4C2e( true) ;
         }
         else
         {
            wb_table3_73_4C2e( false) ;
         }
      }

      protected void wb_table4_76_4C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"79\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAtributos_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtAtributos_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAtributos_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbAtributos_TipoDados_Titleformat == 0 )
               {
                  context.SendWebValue( cmbAtributos_TipoDados.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbAtributos_TipoDados.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAtributos_Detalhes_Titleformat == 0 )
               {
                  context.SendWebValue( edtAtributos_Detalhes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAtributos_Detalhes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAtributos_TabelaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtAtributos_TabelaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAtributos_TabelaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkAtributos_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkAtributos_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkAtributos_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkAtributos_PK_Titleformat == 0 )
               {
                  context.SendWebValue( chkAtributos_PK.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkAtributos_PK.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkAtributos_FK_Titleformat == 0 )
               {
                  context.SendWebValue( chkAtributos_FK.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkAtributos_FK.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV58Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A177Atributos_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAtributos_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAtributos_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtAtributos_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A178Atributos_TipoDados));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbAtributos_TipoDados.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAtributos_TipoDados_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A390Atributos_Detalhes));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAtributos_Detalhes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAtributos_Detalhes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A357Atributos_TabelaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAtributos_TabelaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAtributos_TabelaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtAtributos_TabelaNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A180Atributos_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkAtributos_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkAtributos_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A400Atributos_PK));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkAtributos_PK.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkAtributos_PK_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A401Atributos_FK));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkAtributos_FK.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkAtributos_FK_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 79 )
         {
            wbEnd = 0;
            nRC_GXsfl_79 = (short)(nGXsfl_79_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_76_4C2e( true) ;
         }
         else
         {
            wb_table4_76_4C2e( false) ;
         }
      }

      protected void wb_table2_8_4C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAtributostitle_Internalname, "Atributos", "", "", lblAtributostitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_13_4C2( true) ;
         }
         else
         {
            wb_table5_13_4C2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_4C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WWAtributos.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_22_4C2( true) ;
         }
         else
         {
            wb_table6_22_4C2( false) ;
         }
         return  ;
      }

      protected void wb_table6_22_4C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_4C2e( true) ;
         }
         else
         {
            wb_table2_8_4C2e( false) ;
         }
      }

      protected void wb_table6_22_4C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextsistema_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblFiltertextsistema_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSistema_areatrabalhocod, dynavSistema_areatrabalhocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)), 1, dynavSistema_areatrabalhocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_WWAtributos.htm");
            dynavSistema_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_areatrabalhocod_Internalname, "Values", (String)(dynavSistema_areatrabalhocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_31_4C2( true) ;
         }
         else
         {
            wb_table7_31_4C2( false) ;
         }
         return  ;
      }

      protected void wb_table7_31_4C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_22_4C2e( true) ;
         }
         else
         {
            wb_table6_22_4C2e( false) ;
         }
      }

      protected void wb_table7_31_4C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_34_4C2( true) ;
         }
         else
         {
            wb_table8_34_4C2( false) ;
         }
         return  ;
      }

      protected void wb_table8_34_4C2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_31_4C2e( true) ;
         }
         else
         {
            wb_table7_31_4C2e( false) ;
         }
      }

      protected void wb_table8_34_4C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_WWAtributos.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_43_4C2( true) ;
         }
         else
         {
            wb_table9_43_4C2( false) ;
         }
         return  ;
      }

      protected void wb_table9_43_4C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAtributos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWAtributos.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_61_4C2( true) ;
         }
         else
         {
            wb_table10_61_4C2( false) ;
         }
         return  ;
      }

      protected void wb_table10_61_4C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_34_4C2e( true) ;
         }
         else
         {
            wb_table8_34_4C2e( false) ;
         }
      }

      protected void wb_table10_61_4C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", true, "HLP_WWAtributos.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAtributos_nome2_Internalname, StringUtil.RTrim( AV23Atributos_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23Atributos_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAtributos_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAtributos_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAtributos_tabelanom2_Internalname, StringUtil.RTrim( AV62Atributos_TabelaNom2), StringUtil.RTrim( context.localUtil.Format( AV62Atributos_TabelaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAtributos_tabelanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAtributos_tabelanom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_61_4C2e( true) ;
         }
         else
         {
            wb_table10_61_4C2e( false) ;
         }
      }

      protected void wb_table9_43_4C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_WWAtributos.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAtributos_nome1_Internalname, StringUtil.RTrim( AV17Atributos_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Atributos_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAtributos_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAtributos_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAtributos_tabelanom1_Internalname, StringUtil.RTrim( AV61Atributos_TabelaNom1), StringUtil.RTrim( context.localUtil.Format( AV61Atributos_TabelaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAtributos_tabelanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAtributos_tabelanom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_43_4C2e( true) ;
         }
         else
         {
            wb_table9_43_4C2e( false) ;
         }
      }

      protected void wb_table5_13_4C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_4C2e( true) ;
         }
         else
         {
            wb_table5_13_4C2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA4C2( ) ;
         WS4C2( ) ;
         WE4C2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020548422992");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwatributos.js", "?2020548422992");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_792( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_79_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_79_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_79_idx;
         edtAtributos_Codigo_Internalname = "ATRIBUTOS_CODIGO_"+sGXsfl_79_idx;
         edtAtributos_Nome_Internalname = "ATRIBUTOS_NOME_"+sGXsfl_79_idx;
         cmbAtributos_TipoDados_Internalname = "ATRIBUTOS_TIPODADOS_"+sGXsfl_79_idx;
         edtAtributos_Detalhes_Internalname = "ATRIBUTOS_DETALHES_"+sGXsfl_79_idx;
         edtAtributos_TabelaNom_Internalname = "ATRIBUTOS_TABELANOM_"+sGXsfl_79_idx;
         chkAtributos_Ativo_Internalname = "ATRIBUTOS_ATIVO_"+sGXsfl_79_idx;
         chkAtributos_PK_Internalname = "ATRIBUTOS_PK_"+sGXsfl_79_idx;
         chkAtributos_FK_Internalname = "ATRIBUTOS_FK_"+sGXsfl_79_idx;
      }

      protected void SubsflControlProps_fel_792( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_79_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_79_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_79_fel_idx;
         edtAtributos_Codigo_Internalname = "ATRIBUTOS_CODIGO_"+sGXsfl_79_fel_idx;
         edtAtributos_Nome_Internalname = "ATRIBUTOS_NOME_"+sGXsfl_79_fel_idx;
         cmbAtributos_TipoDados_Internalname = "ATRIBUTOS_TIPODADOS_"+sGXsfl_79_fel_idx;
         edtAtributos_Detalhes_Internalname = "ATRIBUTOS_DETALHES_"+sGXsfl_79_fel_idx;
         edtAtributos_TabelaNom_Internalname = "ATRIBUTOS_TABELANOM_"+sGXsfl_79_fel_idx;
         chkAtributos_Ativo_Internalname = "ATRIBUTOS_ATIVO_"+sGXsfl_79_fel_idx;
         chkAtributos_PK_Internalname = "ATRIBUTOS_PK_"+sGXsfl_79_fel_idx;
         chkAtributos_FK_Internalname = "ATRIBUTOS_FK_"+sGXsfl_79_fel_idx;
      }

      protected void sendrow_792( )
      {
         SubsflControlProps_792( ) ;
         WB4C0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_79_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_79_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_79_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV34Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV131Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)) ? AV131Update_GXI : context.PathToRelativeUrl( AV34Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV34Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV132Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV132Delete_GXI : context.PathToRelativeUrl( AV35Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV58Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV58Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV133Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV58Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV58Display)) ? AV133Display_GXI : context.PathToRelativeUrl( AV58Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV58Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Nome_Internalname,StringUtil.RTrim( A177Atributos_Nome),StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtAtributos_Nome_Link,(String)"",(String)"",(String)"",(String)edtAtributos_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_79_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "ATRIBUTOS_TIPODADOS_" + sGXsfl_79_idx;
               cmbAtributos_TipoDados.Name = GXCCtl;
               cmbAtributos_TipoDados.WebTags = "";
               cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
               cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
               cmbAtributos_TipoDados.addItem("C", "Character", 0);
               cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
               cmbAtributos_TipoDados.addItem("D", "Date", 0);
               cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
               cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
               cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
               cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
               if ( cmbAtributos_TipoDados.ItemCount > 0 )
               {
                  A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
                  n178Atributos_TipoDados = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAtributos_TipoDados,(String)cmbAtributos_TipoDados_Internalname,StringUtil.RTrim( A178Atributos_TipoDados),(short)1,(String)cmbAtributos_TipoDados_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbAtributos_TipoDados.CurrentValue = StringUtil.RTrim( A178Atributos_TipoDados);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAtributos_TipoDados_Internalname, "Values", (String)(cmbAtributos_TipoDados.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Detalhes_Internalname,StringUtil.RTrim( A390Atributos_Detalhes),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_Detalhes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_TabelaNom_Internalname,StringUtil.RTrim( A357Atributos_TabelaNom),StringUtil.RTrim( context.localUtil.Format( A357Atributos_TabelaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtAtributos_TabelaNom_Link,(String)"",(String)"",(String)"",(String)edtAtributos_TabelaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAtributos_Ativo_Internalname,StringUtil.BoolToStr( A180Atributos_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAtributos_PK_Internalname,StringUtil.BoolToStr( A400Atributos_PK),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAtributos_FK_Internalname,StringUtil.BoolToStr( A401Atributos_FK),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_CODIGO"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_NOME"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_TIPODADOS"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A178Atributos_TipoDados, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_DETALHES"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A390Atributos_Detalhes, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_ATIVO"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, A180Atributos_Ativo));
            GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_PK"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, A400Atributos_PK));
            GxWebStd.gx_hidden_field( context, "gxhash_ATRIBUTOS_FK"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, A401Atributos_FK));
            GridContainer.AddRow(GridRow);
            nGXsfl_79_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_79_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_79_idx+1));
            sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
            SubsflControlProps_792( ) ;
         }
         /* End function sendrow_792 */
      }

      protected void init_default_properties( )
      {
         lblAtributostitle_Internalname = "ATRIBUTOSTITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextsistema_areatrabalhocod_Internalname = "FILTERTEXTSISTEMA_AREATRABALHOCOD";
         dynavSistema_areatrabalhocod_Internalname = "vSISTEMA_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavAtributos_nome1_Internalname = "vATRIBUTOS_NOME1";
         edtavAtributos_tabelanom1_Internalname = "vATRIBUTOS_TABELANOM1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavAtributos_nome2_Internalname = "vATRIBUTOS_NOME2";
         edtavAtributos_tabelanom2_Internalname = "vATRIBUTOS_TABELANOM2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtAtributos_Codigo_Internalname = "ATRIBUTOS_CODIGO";
         edtAtributos_Nome_Internalname = "ATRIBUTOS_NOME";
         cmbAtributos_TipoDados_Internalname = "ATRIBUTOS_TIPODADOS";
         edtAtributos_Detalhes_Internalname = "ATRIBUTOS_DETALHES";
         edtAtributos_TabelaNom_Internalname = "ATRIBUTOS_TABELANOM";
         chkAtributos_Ativo_Internalname = "ATRIBUTOS_ATIVO";
         chkAtributos_PK_Internalname = "ATRIBUTOS_PK";
         chkAtributos_FK_Internalname = "ATRIBUTOS_FK";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfatributos_nome_Internalname = "vTFATRIBUTOS_NOME";
         edtavTfatributos_nome_sel_Internalname = "vTFATRIBUTOS_NOME_SEL";
         edtavTfatributos_detalhes_Internalname = "vTFATRIBUTOS_DETALHES";
         edtavTfatributos_detalhes_sel_Internalname = "vTFATRIBUTOS_DETALHES_SEL";
         edtavTfatributos_tabelanom_Internalname = "vTFATRIBUTOS_TABELANOM";
         edtavTfatributos_tabelanom_sel_Internalname = "vTFATRIBUTOS_TABELANOM_SEL";
         edtavTfatributos_ativo_sel_Internalname = "vTFATRIBUTOS_ATIVO_SEL";
         edtavTfatributos_pk_sel_Internalname = "vTFATRIBUTOS_PK_SEL";
         edtavTfatributos_fk_sel_Internalname = "vTFATRIBUTOS_FK_SEL";
         Ddo_atributos_nome_Internalname = "DDO_ATRIBUTOS_NOME";
         edtavDdo_atributos_nometitlecontrolidtoreplace_Internalname = "vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE";
         Ddo_atributos_tipodados_Internalname = "DDO_ATRIBUTOS_TIPODADOS";
         edtavDdo_atributos_tipodadostitlecontrolidtoreplace_Internalname = "vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE";
         Ddo_atributos_detalhes_Internalname = "DDO_ATRIBUTOS_DETALHES";
         edtavDdo_atributos_detalhestitlecontrolidtoreplace_Internalname = "vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE";
         Ddo_atributos_tabelanom_Internalname = "DDO_ATRIBUTOS_TABELANOM";
         edtavDdo_atributos_tabelanomtitlecontrolidtoreplace_Internalname = "vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE";
         Ddo_atributos_ativo_Internalname = "DDO_ATRIBUTOS_ATIVO";
         edtavDdo_atributos_ativotitlecontrolidtoreplace_Internalname = "vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE";
         Ddo_atributos_pk_Internalname = "DDO_ATRIBUTOS_PK";
         edtavDdo_atributos_pktitlecontrolidtoreplace_Internalname = "vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE";
         Ddo_atributos_fk_Internalname = "DDO_ATRIBUTOS_FK";
         edtavDdo_atributos_fktitlecontrolidtoreplace_Internalname = "vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtAtributos_TabelaNom_Jsonclick = "";
         edtAtributos_Detalhes_Jsonclick = "";
         cmbAtributos_TipoDados_Jsonclick = "";
         edtAtributos_Nome_Jsonclick = "";
         edtAtributos_Codigo_Jsonclick = "";
         edtavAtributos_tabelanom1_Jsonclick = "";
         edtavAtributos_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavAtributos_tabelanom2_Jsonclick = "";
         edtavAtributos_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavSistema_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtAtributos_TabelaNom_Link = "";
         edtAtributos_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkAtributos_FK_Titleformat = 0;
         chkAtributos_PK_Titleformat = 0;
         chkAtributos_Ativo_Titleformat = 0;
         edtAtributos_TabelaNom_Titleformat = 0;
         edtAtributos_Detalhes_Titleformat = 0;
         cmbAtributos_TipoDados_Titleformat = 0;
         edtAtributos_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavAtributos_tabelanom2_Visible = 1;
         edtavAtributos_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavAtributos_tabelanom1_Visible = 1;
         edtavAtributos_nome1_Visible = 1;
         chkAtributos_FK.Title.Text = "FK";
         chkAtributos_PK.Title.Text = "PK";
         chkAtributos_Ativo.Title.Text = "Ativo";
         edtAtributos_TabelaNom_Title = "Tabela";
         edtAtributos_Detalhes_Title = "Detalhes";
         cmbAtributos_TipoDados.Title.Text = "Tipo";
         edtAtributos_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         chkAtributos_FK.Caption = "";
         chkAtributos_PK.Caption = "";
         chkAtributos_Ativo.Caption = "";
         edtavDdo_atributos_fktitlecontrolidtoreplace_Visible = 1;
         edtavDdo_atributos_pktitlecontrolidtoreplace_Visible = 1;
         edtavDdo_atributos_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_atributos_tabelanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_atributos_detalhestitlecontrolidtoreplace_Visible = 1;
         edtavDdo_atributos_tipodadostitlecontrolidtoreplace_Visible = 1;
         edtavDdo_atributos_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfatributos_fk_sel_Jsonclick = "";
         edtavTfatributos_fk_sel_Visible = 1;
         edtavTfatributos_pk_sel_Jsonclick = "";
         edtavTfatributos_pk_sel_Visible = 1;
         edtavTfatributos_ativo_sel_Jsonclick = "";
         edtavTfatributos_ativo_sel_Visible = 1;
         edtavTfatributos_tabelanom_sel_Jsonclick = "";
         edtavTfatributos_tabelanom_sel_Visible = 1;
         edtavTfatributos_tabelanom_Jsonclick = "";
         edtavTfatributos_tabelanom_Visible = 1;
         edtavTfatributos_detalhes_sel_Jsonclick = "";
         edtavTfatributos_detalhes_sel_Visible = 1;
         edtavTfatributos_detalhes_Jsonclick = "";
         edtavTfatributos_detalhes_Visible = 1;
         edtavTfatributos_nome_sel_Jsonclick = "";
         edtavTfatributos_nome_sel_Visible = 1;
         edtavTfatributos_nome_Jsonclick = "";
         edtavTfatributos_nome_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_atributos_fk_Searchbuttontext = "Pesquisar";
         Ddo_atributos_fk_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_atributos_fk_Rangefilterto = "At�";
         Ddo_atributos_fk_Rangefilterfrom = "Desde";
         Ddo_atributos_fk_Cleanfilter = "Limpar pesquisa";
         Ddo_atributos_fk_Loadingdata = "Carregando dados...";
         Ddo_atributos_fk_Sortdsc = "Ordenar de Z � A";
         Ddo_atributos_fk_Sortasc = "Ordenar de A � Z";
         Ddo_atributos_fk_Datalistupdateminimumcharacters = 0;
         Ddo_atributos_fk_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_atributos_fk_Datalisttype = "FixedValues";
         Ddo_atributos_fk_Includedatalist = Convert.ToBoolean( -1);
         Ddo_atributos_fk_Filterisrange = Convert.ToBoolean( 0);
         Ddo_atributos_fk_Includefilter = Convert.ToBoolean( 0);
         Ddo_atributos_fk_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_atributos_fk_Includesortasc = Convert.ToBoolean( -1);
         Ddo_atributos_fk_Titlecontrolidtoreplace = "";
         Ddo_atributos_fk_Dropdownoptionstype = "GridTitleSettings";
         Ddo_atributos_fk_Cls = "ColumnSettings";
         Ddo_atributos_fk_Tooltip = "Op��es";
         Ddo_atributos_fk_Caption = "";
         Ddo_atributos_pk_Searchbuttontext = "Pesquisar";
         Ddo_atributos_pk_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_atributos_pk_Rangefilterto = "At�";
         Ddo_atributos_pk_Rangefilterfrom = "Desde";
         Ddo_atributos_pk_Cleanfilter = "Limpar pesquisa";
         Ddo_atributos_pk_Loadingdata = "Carregando dados...";
         Ddo_atributos_pk_Sortdsc = "Ordenar de Z � A";
         Ddo_atributos_pk_Sortasc = "Ordenar de A � Z";
         Ddo_atributos_pk_Datalistupdateminimumcharacters = 0;
         Ddo_atributos_pk_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_atributos_pk_Datalisttype = "FixedValues";
         Ddo_atributos_pk_Includedatalist = Convert.ToBoolean( -1);
         Ddo_atributos_pk_Filterisrange = Convert.ToBoolean( 0);
         Ddo_atributos_pk_Includefilter = Convert.ToBoolean( 0);
         Ddo_atributos_pk_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_atributos_pk_Includesortasc = Convert.ToBoolean( -1);
         Ddo_atributos_pk_Titlecontrolidtoreplace = "";
         Ddo_atributos_pk_Dropdownoptionstype = "GridTitleSettings";
         Ddo_atributos_pk_Cls = "ColumnSettings";
         Ddo_atributos_pk_Tooltip = "Op��es";
         Ddo_atributos_pk_Caption = "";
         Ddo_atributos_ativo_Searchbuttontext = "Pesquisar";
         Ddo_atributos_ativo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_atributos_ativo_Rangefilterto = "At�";
         Ddo_atributos_ativo_Rangefilterfrom = "Desde";
         Ddo_atributos_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_atributos_ativo_Loadingdata = "Carregando dados...";
         Ddo_atributos_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_atributos_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_atributos_ativo_Datalistupdateminimumcharacters = 0;
         Ddo_atributos_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_atributos_ativo_Datalisttype = "FixedValues";
         Ddo_atributos_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_atributos_ativo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_atributos_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_atributos_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_atributos_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_atributos_ativo_Titlecontrolidtoreplace = "";
         Ddo_atributos_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_atributos_ativo_Cls = "ColumnSettings";
         Ddo_atributos_ativo_Tooltip = "Op��es";
         Ddo_atributos_ativo_Caption = "";
         Ddo_atributos_tabelanom_Searchbuttontext = "Pesquisar";
         Ddo_atributos_tabelanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_atributos_tabelanom_Rangefilterto = "At�";
         Ddo_atributos_tabelanom_Rangefilterfrom = "Desde";
         Ddo_atributos_tabelanom_Cleanfilter = "Limpar pesquisa";
         Ddo_atributos_tabelanom_Loadingdata = "Carregando dados...";
         Ddo_atributos_tabelanom_Sortdsc = "Ordenar de Z � A";
         Ddo_atributos_tabelanom_Sortasc = "Ordenar de A � Z";
         Ddo_atributos_tabelanom_Datalistupdateminimumcharacters = 0;
         Ddo_atributos_tabelanom_Datalistproc = "GetWWAtributosFilterData";
         Ddo_atributos_tabelanom_Datalistfixedvalues = "";
         Ddo_atributos_tabelanom_Datalisttype = "Dynamic";
         Ddo_atributos_tabelanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_atributos_tabelanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_atributos_tabelanom_Filtertype = "Character";
         Ddo_atributos_tabelanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_atributos_tabelanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_atributos_tabelanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_atributos_tabelanom_Titlecontrolidtoreplace = "";
         Ddo_atributos_tabelanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_atributos_tabelanom_Cls = "ColumnSettings";
         Ddo_atributos_tabelanom_Tooltip = "Op��es";
         Ddo_atributos_tabelanom_Caption = "";
         Ddo_atributos_detalhes_Searchbuttontext = "Pesquisar";
         Ddo_atributos_detalhes_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_atributos_detalhes_Rangefilterto = "At�";
         Ddo_atributos_detalhes_Rangefilterfrom = "Desde";
         Ddo_atributos_detalhes_Cleanfilter = "Limpar pesquisa";
         Ddo_atributos_detalhes_Loadingdata = "Carregando dados...";
         Ddo_atributos_detalhes_Sortdsc = "Ordenar de Z � A";
         Ddo_atributos_detalhes_Sortasc = "Ordenar de A � Z";
         Ddo_atributos_detalhes_Datalistupdateminimumcharacters = 0;
         Ddo_atributos_detalhes_Datalistproc = "GetWWAtributosFilterData";
         Ddo_atributos_detalhes_Datalistfixedvalues = "";
         Ddo_atributos_detalhes_Datalisttype = "Dynamic";
         Ddo_atributos_detalhes_Includedatalist = Convert.ToBoolean( -1);
         Ddo_atributos_detalhes_Filterisrange = Convert.ToBoolean( 0);
         Ddo_atributos_detalhes_Filtertype = "Character";
         Ddo_atributos_detalhes_Includefilter = Convert.ToBoolean( -1);
         Ddo_atributos_detalhes_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_atributos_detalhes_Includesortasc = Convert.ToBoolean( -1);
         Ddo_atributos_detalhes_Titlecontrolidtoreplace = "";
         Ddo_atributos_detalhes_Dropdownoptionstype = "GridTitleSettings";
         Ddo_atributos_detalhes_Cls = "ColumnSettings";
         Ddo_atributos_detalhes_Tooltip = "Op��es";
         Ddo_atributos_detalhes_Caption = "";
         Ddo_atributos_tipodados_Searchbuttontext = "Filtrar Selecionados";
         Ddo_atributos_tipodados_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_atributos_tipodados_Rangefilterto = "At�";
         Ddo_atributos_tipodados_Rangefilterfrom = "Desde";
         Ddo_atributos_tipodados_Cleanfilter = "Limpar pesquisa";
         Ddo_atributos_tipodados_Loadingdata = "Carregando dados...";
         Ddo_atributos_tipodados_Sortdsc = "Ordenar de Z � A";
         Ddo_atributos_tipodados_Sortasc = "Ordenar de A � Z";
         Ddo_atributos_tipodados_Datalistupdateminimumcharacters = 0;
         Ddo_atributos_tipodados_Datalistfixedvalues = "N:Numeric,C:Character,VC:Varchar,D:Date,DT:Date Time,Bool:Boolean,Blob:Blob,Outr:Outros";
         Ddo_atributos_tipodados_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_atributos_tipodados_Datalisttype = "FixedValues";
         Ddo_atributos_tipodados_Includedatalist = Convert.ToBoolean( -1);
         Ddo_atributos_tipodados_Filterisrange = Convert.ToBoolean( 0);
         Ddo_atributos_tipodados_Includefilter = Convert.ToBoolean( 0);
         Ddo_atributos_tipodados_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_atributos_tipodados_Includesortasc = Convert.ToBoolean( -1);
         Ddo_atributos_tipodados_Titlecontrolidtoreplace = "";
         Ddo_atributos_tipodados_Dropdownoptionstype = "GridTitleSettings";
         Ddo_atributos_tipodados_Cls = "ColumnSettings";
         Ddo_atributos_tipodados_Tooltip = "Op��es";
         Ddo_atributos_tipodados_Caption = "";
         Ddo_atributos_nome_Searchbuttontext = "Pesquisar";
         Ddo_atributos_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_atributos_nome_Rangefilterto = "At�";
         Ddo_atributos_nome_Rangefilterfrom = "Desde";
         Ddo_atributos_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_atributos_nome_Loadingdata = "Carregando dados...";
         Ddo_atributos_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_atributos_nome_Sortasc = "Ordenar de A � Z";
         Ddo_atributos_nome_Datalistupdateminimumcharacters = 0;
         Ddo_atributos_nome_Datalistproc = "GetWWAtributosFilterData";
         Ddo_atributos_nome_Datalistfixedvalues = "";
         Ddo_atributos_nome_Datalisttype = "Dynamic";
         Ddo_atributos_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_atributos_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_atributos_nome_Filtertype = "Character";
         Ddo_atributos_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_atributos_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_atributos_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_atributos_nome_Titlecontrolidtoreplace = "";
         Ddo_atributos_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_atributos_nome_Cls = "ColumnSettings";
         Ddo_atributos_nome_Tooltip = "Op��es";
         Ddo_atributos_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Atributos";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV66Atributos_NomeTitleFilterData',fld:'vATRIBUTOS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV70Atributos_TipoDadosTitleFilterData',fld:'vATRIBUTOS_TIPODADOSTITLEFILTERDATA',pic:'',nv:null},{av:'AV74Atributos_DetalhesTitleFilterData',fld:'vATRIBUTOS_DETALHESTITLEFILTERDATA',pic:'',nv:null},{av:'AV78Atributos_TabelaNomTitleFilterData',fld:'vATRIBUTOS_TABELANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV82Atributos_AtivoTitleFilterData',fld:'vATRIBUTOS_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV85Atributos_PKTitleFilterData',fld:'vATRIBUTOS_PKTITLEFILTERDATA',pic:'',nv:null},{av:'AV88Atributos_FKTitleFilterData',fld:'vATRIBUTOS_FKTITLEFILTERDATA',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtAtributos_Nome_Titleformat',ctrl:'ATRIBUTOS_NOME',prop:'Titleformat'},{av:'edtAtributos_Nome_Title',ctrl:'ATRIBUTOS_NOME',prop:'Title'},{av:'cmbAtributos_TipoDados'},{av:'edtAtributos_Detalhes_Titleformat',ctrl:'ATRIBUTOS_DETALHES',prop:'Titleformat'},{av:'edtAtributos_Detalhes_Title',ctrl:'ATRIBUTOS_DETALHES',prop:'Title'},{av:'edtAtributos_TabelaNom_Titleformat',ctrl:'ATRIBUTOS_TABELANOM',prop:'Titleformat'},{av:'edtAtributos_TabelaNom_Title',ctrl:'ATRIBUTOS_TABELANOM',prop:'Title'},{av:'chkAtributos_Ativo_Titleformat',ctrl:'ATRIBUTOS_ATIVO',prop:'Titleformat'},{av:'chkAtributos_Ativo.Title.Text',ctrl:'ATRIBUTOS_ATIVO',prop:'Title'},{av:'chkAtributos_PK_Titleformat',ctrl:'ATRIBUTOS_PK',prop:'Titleformat'},{av:'chkAtributos_PK.Title.Text',ctrl:'ATRIBUTOS_PK',prop:'Title'},{av:'chkAtributos_FK_Titleformat',ctrl:'ATRIBUTOS_FK',prop:'Titleformat'},{av:'chkAtributos_FK.Title.Text',ctrl:'ATRIBUTOS_FK',prop:'Title'},{av:'AV93GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV94GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E114C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_ATRIBUTOS_NOME.ONOPTIONCLICKED","{handler:'E124C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_atributos_nome_Activeeventkey',ctrl:'DDO_ATRIBUTOS_NOME',prop:'ActiveEventKey'},{av:'Ddo_atributos_nome_Filteredtext_get',ctrl:'DDO_ATRIBUTOS_NOME',prop:'FilteredText_get'},{av:'Ddo_atributos_nome_Selectedvalue_get',ctrl:'DDO_ATRIBUTOS_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_atributos_nome_Sortedstatus',ctrl:'DDO_ATRIBUTOS_NOME',prop:'SortedStatus'},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_atributos_tipodados_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TIPODADOS',prop:'SortedStatus'},{av:'Ddo_atributos_detalhes_Sortedstatus',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'SortedStatus'},{av:'Ddo_atributos_tabelanom_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'SortedStatus'},{av:'Ddo_atributos_ativo_Sortedstatus',ctrl:'DDO_ATRIBUTOS_ATIVO',prop:'SortedStatus'},{av:'Ddo_atributos_pk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_PK',prop:'SortedStatus'},{av:'Ddo_atributos_fk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_FK',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ATRIBUTOS_TIPODADOS.ONOPTIONCLICKED","{handler:'E134C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_atributos_tipodados_Activeeventkey',ctrl:'DDO_ATRIBUTOS_TIPODADOS',prop:'ActiveEventKey'},{av:'Ddo_atributos_tipodados_Selectedvalue_get',ctrl:'DDO_ATRIBUTOS_TIPODADOS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_atributos_tipodados_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TIPODADOS',prop:'SortedStatus'},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'Ddo_atributos_nome_Sortedstatus',ctrl:'DDO_ATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_atributos_detalhes_Sortedstatus',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'SortedStatus'},{av:'Ddo_atributos_tabelanom_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'SortedStatus'},{av:'Ddo_atributos_ativo_Sortedstatus',ctrl:'DDO_ATRIBUTOS_ATIVO',prop:'SortedStatus'},{av:'Ddo_atributos_pk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_PK',prop:'SortedStatus'},{av:'Ddo_atributos_fk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_FK',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ATRIBUTOS_DETALHES.ONOPTIONCLICKED","{handler:'E144C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_atributos_detalhes_Activeeventkey',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'ActiveEventKey'},{av:'Ddo_atributos_detalhes_Filteredtext_get',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'FilteredText_get'},{av:'Ddo_atributos_detalhes_Selectedvalue_get',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_atributos_detalhes_Sortedstatus',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'SortedStatus'},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'Ddo_atributos_nome_Sortedstatus',ctrl:'DDO_ATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_atributos_tipodados_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TIPODADOS',prop:'SortedStatus'},{av:'Ddo_atributos_tabelanom_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'SortedStatus'},{av:'Ddo_atributos_ativo_Sortedstatus',ctrl:'DDO_ATRIBUTOS_ATIVO',prop:'SortedStatus'},{av:'Ddo_atributos_pk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_PK',prop:'SortedStatus'},{av:'Ddo_atributos_fk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_FK',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ATRIBUTOS_TABELANOM.ONOPTIONCLICKED","{handler:'E154C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_atributos_tabelanom_Activeeventkey',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'ActiveEventKey'},{av:'Ddo_atributos_tabelanom_Filteredtext_get',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'FilteredText_get'},{av:'Ddo_atributos_tabelanom_Selectedvalue_get',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_atributos_tabelanom_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'SortedStatus'},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'Ddo_atributos_nome_Sortedstatus',ctrl:'DDO_ATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_atributos_tipodados_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TIPODADOS',prop:'SortedStatus'},{av:'Ddo_atributos_detalhes_Sortedstatus',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'SortedStatus'},{av:'Ddo_atributos_ativo_Sortedstatus',ctrl:'DDO_ATRIBUTOS_ATIVO',prop:'SortedStatus'},{av:'Ddo_atributos_pk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_PK',prop:'SortedStatus'},{av:'Ddo_atributos_fk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_FK',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ATRIBUTOS_ATIVO.ONOPTIONCLICKED","{handler:'E164C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_atributos_ativo_Activeeventkey',ctrl:'DDO_ATRIBUTOS_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_atributos_ativo_Selectedvalue_get',ctrl:'DDO_ATRIBUTOS_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_atributos_ativo_Sortedstatus',ctrl:'DDO_ATRIBUTOS_ATIVO',prop:'SortedStatus'},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_atributos_nome_Sortedstatus',ctrl:'DDO_ATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_atributos_tipodados_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TIPODADOS',prop:'SortedStatus'},{av:'Ddo_atributos_detalhes_Sortedstatus',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'SortedStatus'},{av:'Ddo_atributos_tabelanom_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'SortedStatus'},{av:'Ddo_atributos_pk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_PK',prop:'SortedStatus'},{av:'Ddo_atributos_fk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_FK',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ATRIBUTOS_PK.ONOPTIONCLICKED","{handler:'E174C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_atributos_pk_Activeeventkey',ctrl:'DDO_ATRIBUTOS_PK',prop:'ActiveEventKey'},{av:'Ddo_atributos_pk_Selectedvalue_get',ctrl:'DDO_ATRIBUTOS_PK',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_atributos_pk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_PK',prop:'SortedStatus'},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'Ddo_atributos_nome_Sortedstatus',ctrl:'DDO_ATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_atributos_tipodados_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TIPODADOS',prop:'SortedStatus'},{av:'Ddo_atributos_detalhes_Sortedstatus',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'SortedStatus'},{av:'Ddo_atributos_tabelanom_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'SortedStatus'},{av:'Ddo_atributos_ativo_Sortedstatus',ctrl:'DDO_ATRIBUTOS_ATIVO',prop:'SortedStatus'},{av:'Ddo_atributos_fk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_FK',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ATRIBUTOS_FK.ONOPTIONCLICKED","{handler:'E184C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_atributos_fk_Activeeventkey',ctrl:'DDO_ATRIBUTOS_FK',prop:'ActiveEventKey'},{av:'Ddo_atributos_fk_Selectedvalue_get',ctrl:'DDO_ATRIBUTOS_FK',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_atributos_fk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_FK',prop:'SortedStatus'},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'Ddo_atributos_nome_Sortedstatus',ctrl:'DDO_ATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_atributos_tipodados_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TIPODADOS',prop:'SortedStatus'},{av:'Ddo_atributos_detalhes_Sortedstatus',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'SortedStatus'},{av:'Ddo_atributos_tabelanom_Sortedstatus',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'SortedStatus'},{av:'Ddo_atributos_ativo_Sortedstatus',ctrl:'DDO_ATRIBUTOS_ATIVO',prop:'SortedStatus'},{av:'Ddo_atributos_pk_Sortedstatus',ctrl:'DDO_ATRIBUTOS_PK',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E284C2',iparms:[{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV34Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV35Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV58Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtAtributos_Nome_Link',ctrl:'ATRIBUTOS_NOME',prop:'Link'},{av:'edtAtributos_TabelaNom_Link',ctrl:'ATRIBUTOS_TABELANOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E194C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E234C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E204C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'edtavAtributos_nome2_Visible',ctrl:'vATRIBUTOS_NOME2',prop:'Visible'},{av:'edtavAtributos_tabelanom2_Visible',ctrl:'vATRIBUTOS_TABELANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavAtributos_nome1_Visible',ctrl:'vATRIBUTOS_NOME1',prop:'Visible'},{av:'edtavAtributos_tabelanom1_Visible',ctrl:'vATRIBUTOS_TABELANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E244C2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavAtributos_nome1_Visible',ctrl:'vATRIBUTOS_NOME1',prop:'Visible'},{av:'edtavAtributos_tabelanom1_Visible',ctrl:'vATRIBUTOS_TABELANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E214C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'edtavAtributos_nome2_Visible',ctrl:'vATRIBUTOS_NOME2',prop:'Visible'},{av:'edtavAtributos_tabelanom2_Visible',ctrl:'vATRIBUTOS_TABELANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavAtributos_nome1_Visible',ctrl:'vATRIBUTOS_NOME1',prop:'Visible'},{av:'edtavAtributos_tabelanom1_Visible',ctrl:'vATRIBUTOS_TABELANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E254C2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavAtributos_nome2_Visible',ctrl:'vATRIBUTOS_NOME2',prop:'Visible'},{av:'edtavAtributos_tabelanom2_Visible',ctrl:'vATRIBUTOS_TABELANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E224C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'AV69ddo_Atributos_NomeTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Atributos_TipoDadosTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TIPODADOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Atributos_DetalhesTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_DETALHESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Atributos_TabelaNomTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_TABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Atributos_AtivoTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Atributos_PKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_PKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Atributos_FKTitleControlIdToReplace',fld:'vDDO_ATRIBUTOS_FKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'AV134Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV63Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67TFAtributos_Nome',fld:'vTFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'Ddo_atributos_nome_Filteredtext_set',ctrl:'DDO_ATRIBUTOS_NOME',prop:'FilteredText_set'},{av:'AV68TFAtributos_Nome_Sel',fld:'vTFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_atributos_nome_Selectedvalue_set',ctrl:'DDO_ATRIBUTOS_NOME',prop:'SelectedValue_set'},{av:'AV72TFAtributos_TipoDados_Sels',fld:'vTFATRIBUTOS_TIPODADOS_SELS',pic:'',nv:null},{av:'Ddo_atributos_tipodados_Selectedvalue_set',ctrl:'DDO_ATRIBUTOS_TIPODADOS',prop:'SelectedValue_set'},{av:'AV75TFAtributos_Detalhes',fld:'vTFATRIBUTOS_DETALHES',pic:'',nv:''},{av:'Ddo_atributos_detalhes_Filteredtext_set',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'FilteredText_set'},{av:'AV76TFAtributos_Detalhes_Sel',fld:'vTFATRIBUTOS_DETALHES_SEL',pic:'',nv:''},{av:'Ddo_atributos_detalhes_Selectedvalue_set',ctrl:'DDO_ATRIBUTOS_DETALHES',prop:'SelectedValue_set'},{av:'AV79TFAtributos_TabelaNom',fld:'vTFATRIBUTOS_TABELANOM',pic:'@!',nv:''},{av:'Ddo_atributos_tabelanom_Filteredtext_set',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'FilteredText_set'},{av:'AV80TFAtributos_TabelaNom_Sel',fld:'vTFATRIBUTOS_TABELANOM_SEL',pic:'@!',nv:''},{av:'Ddo_atributos_tabelanom_Selectedvalue_set',ctrl:'DDO_ATRIBUTOS_TABELANOM',prop:'SelectedValue_set'},{av:'AV83TFAtributos_Ativo_Sel',fld:'vTFATRIBUTOS_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_atributos_ativo_Selectedvalue_set',ctrl:'DDO_ATRIBUTOS_ATIVO',prop:'SelectedValue_set'},{av:'AV86TFAtributos_PK_Sel',fld:'vTFATRIBUTOS_PK_SEL',pic:'9',nv:0},{av:'Ddo_atributos_pk_Selectedvalue_set',ctrl:'DDO_ATRIBUTOS_PK',prop:'SelectedValue_set'},{av:'AV89TFAtributos_FK_Sel',fld:'vTFATRIBUTOS_FK_SEL',pic:'9',nv:0},{av:'Ddo_atributos_fk_Selectedvalue_set',ctrl:'DDO_ATRIBUTOS_FK',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Atributos_Nome1',fld:'vATRIBUTOS_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavAtributos_nome1_Visible',ctrl:'vATRIBUTOS_NOME1',prop:'Visible'},{av:'edtavAtributos_tabelanom1_Visible',ctrl:'vATRIBUTOS_TABELANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Atributos_Nome2',fld:'vATRIBUTOS_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV61Atributos_TabelaNom1',fld:'vATRIBUTOS_TABELANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV62Atributos_TabelaNom2',fld:'vATRIBUTOS_TABELANOM2',pic:'@!',nv:''},{av:'edtavAtributos_nome2_Visible',ctrl:'vATRIBUTOS_NOME2',prop:'Visible'},{av:'edtavAtributos_tabelanom2_Visible',ctrl:'vATRIBUTOS_TABELANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_atributos_nome_Activeeventkey = "";
         Ddo_atributos_nome_Filteredtext_get = "";
         Ddo_atributos_nome_Selectedvalue_get = "";
         Ddo_atributos_tipodados_Activeeventkey = "";
         Ddo_atributos_tipodados_Selectedvalue_get = "";
         Ddo_atributos_detalhes_Activeeventkey = "";
         Ddo_atributos_detalhes_Filteredtext_get = "";
         Ddo_atributos_detalhes_Selectedvalue_get = "";
         Ddo_atributos_tabelanom_Activeeventkey = "";
         Ddo_atributos_tabelanom_Filteredtext_get = "";
         Ddo_atributos_tabelanom_Selectedvalue_get = "";
         Ddo_atributos_ativo_Activeeventkey = "";
         Ddo_atributos_ativo_Selectedvalue_get = "";
         Ddo_atributos_pk_Activeeventkey = "";
         Ddo_atributos_pk_Selectedvalue_get = "";
         Ddo_atributos_fk_Activeeventkey = "";
         Ddo_atributos_fk_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Atributos_Nome1 = "";
         AV61Atributos_TabelaNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23Atributos_Nome2 = "";
         AV62Atributos_TabelaNom2 = "";
         AV67TFAtributos_Nome = "";
         AV68TFAtributos_Nome_Sel = "";
         AV75TFAtributos_Detalhes = "";
         AV76TFAtributos_Detalhes_Sel = "";
         AV79TFAtributos_TabelaNom = "";
         AV80TFAtributos_TabelaNom_Sel = "";
         AV69ddo_Atributos_NomeTitleControlIdToReplace = "";
         AV73ddo_Atributos_TipoDadosTitleControlIdToReplace = "";
         AV77ddo_Atributos_DetalhesTitleControlIdToReplace = "";
         AV81ddo_Atributos_TabelaNomTitleControlIdToReplace = "";
         AV84ddo_Atributos_AtivoTitleControlIdToReplace = "";
         AV87ddo_Atributos_PKTitleControlIdToReplace = "";
         AV90ddo_Atributos_FKTitleControlIdToReplace = "";
         AV72TFAtributos_TipoDados_Sels = new GxSimpleCollection();
         AV134Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV91DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV66Atributos_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70Atributos_TipoDadosTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74Atributos_DetalhesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78Atributos_TabelaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82Atributos_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV85Atributos_PKTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV88Atributos_FKTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_atributos_nome_Filteredtext_set = "";
         Ddo_atributos_nome_Selectedvalue_set = "";
         Ddo_atributos_nome_Sortedstatus = "";
         Ddo_atributos_tipodados_Selectedvalue_set = "";
         Ddo_atributos_tipodados_Sortedstatus = "";
         Ddo_atributos_detalhes_Filteredtext_set = "";
         Ddo_atributos_detalhes_Selectedvalue_set = "";
         Ddo_atributos_detalhes_Sortedstatus = "";
         Ddo_atributos_tabelanom_Filteredtext_set = "";
         Ddo_atributos_tabelanom_Selectedvalue_set = "";
         Ddo_atributos_tabelanom_Sortedstatus = "";
         Ddo_atributos_ativo_Selectedvalue_set = "";
         Ddo_atributos_ativo_Sortedstatus = "";
         Ddo_atributos_pk_Selectedvalue_set = "";
         Ddo_atributos_pk_Sortedstatus = "";
         Ddo_atributos_fk_Selectedvalue_set = "";
         Ddo_atributos_fk_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV34Update = "";
         AV131Update_GXI = "";
         AV35Delete = "";
         AV132Delete_GXI = "";
         AV58Display = "";
         AV133Display_GXI = "";
         A177Atributos_Nome = "";
         A178Atributos_TipoDados = "";
         A390Atributos_Detalhes = "";
         A357Atributos_TabelaNom = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H004C2_A5AreaTrabalho_Codigo = new int[1] ;
         H004C2_A6AreaTrabalho_Descricao = new String[] {""} ;
         H004C2_A72AreaTrabalho_Ativo = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         AV123WWAtributosDS_13_Tfatributos_tipodados_sels = new GxSimpleCollection();
         lV114WWAtributosDS_4_Atributos_nome1 = "";
         lV115WWAtributosDS_5_Atributos_tabelanom1 = "";
         lV119WWAtributosDS_9_Atributos_nome2 = "";
         lV120WWAtributosDS_10_Atributos_tabelanom2 = "";
         lV121WWAtributosDS_11_Tfatributos_nome = "";
         lV124WWAtributosDS_14_Tfatributos_detalhes = "";
         lV126WWAtributosDS_16_Tfatributos_tabelanom = "";
         AV112WWAtributosDS_2_Dynamicfiltersselector1 = "";
         AV114WWAtributosDS_4_Atributos_nome1 = "";
         AV115WWAtributosDS_5_Atributos_tabelanom1 = "";
         AV117WWAtributosDS_7_Dynamicfiltersselector2 = "";
         AV119WWAtributosDS_9_Atributos_nome2 = "";
         AV120WWAtributosDS_10_Atributos_tabelanom2 = "";
         AV122WWAtributosDS_12_Tfatributos_nome_sel = "";
         AV121WWAtributosDS_11_Tfatributos_nome = "";
         AV125WWAtributosDS_15_Tfatributos_detalhes_sel = "";
         AV124WWAtributosDS_14_Tfatributos_detalhes = "";
         AV127WWAtributosDS_17_Tfatributos_tabelanom_sel = "";
         AV126WWAtributosDS_16_Tfatributos_tabelanom = "";
         H004C3_A190Tabela_SistemaCod = new int[1] ;
         H004C3_n190Tabela_SistemaCod = new bool[] {false} ;
         H004C3_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H004C3_n135Sistema_AreaTrabalhoCod = new bool[] {false} ;
         H004C3_A356Atributos_TabelaCod = new int[1] ;
         H004C3_A401Atributos_FK = new bool[] {false} ;
         H004C3_n401Atributos_FK = new bool[] {false} ;
         H004C3_A400Atributos_PK = new bool[] {false} ;
         H004C3_n400Atributos_PK = new bool[] {false} ;
         H004C3_A180Atributos_Ativo = new bool[] {false} ;
         H004C3_A357Atributos_TabelaNom = new String[] {""} ;
         H004C3_n357Atributos_TabelaNom = new bool[] {false} ;
         H004C3_A390Atributos_Detalhes = new String[] {""} ;
         H004C3_n390Atributos_Detalhes = new bool[] {false} ;
         H004C3_A178Atributos_TipoDados = new String[] {""} ;
         H004C3_n178Atributos_TipoDados = new bool[] {false} ;
         H004C3_A177Atributos_Nome = new String[] {""} ;
         H004C3_A176Atributos_Codigo = new int[1] ;
         H004C4_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV71TFAtributos_TipoDados_SelsJson = "";
         GridRow = new GXWebRow();
         AV36Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblAtributostitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextsistema_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwatributos__default(),
            new Object[][] {
                new Object[] {
               H004C2_A5AreaTrabalho_Codigo, H004C2_A6AreaTrabalho_Descricao, H004C2_A72AreaTrabalho_Ativo
               }
               , new Object[] {
               H004C3_A190Tabela_SistemaCod, H004C3_n190Tabela_SistemaCod, H004C3_A135Sistema_AreaTrabalhoCod, H004C3_n135Sistema_AreaTrabalhoCod, H004C3_A356Atributos_TabelaCod, H004C3_A401Atributos_FK, H004C3_n401Atributos_FK, H004C3_A400Atributos_PK, H004C3_n400Atributos_PK, H004C3_A180Atributos_Ativo,
               H004C3_A357Atributos_TabelaNom, H004C3_n357Atributos_TabelaNom, H004C3_A390Atributos_Detalhes, H004C3_n390Atributos_Detalhes, H004C3_A178Atributos_TipoDados, H004C3_n178Atributos_TipoDados, H004C3_A177Atributos_Nome, H004C3_A176Atributos_Codigo
               }
               , new Object[] {
               H004C4_AGRID_nRecordCount
               }
            }
         );
         AV134Pgmname = "WWAtributos";
         /* GeneXus formulas. */
         AV134Pgmname = "WWAtributos";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_79 ;
      private short nGXsfl_79_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV83TFAtributos_Ativo_Sel ;
      private short AV86TFAtributos_PK_Sel ;
      private short AV89TFAtributos_FK_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_79_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV113WWAtributosDS_3_Dynamicfiltersoperator1 ;
      private short AV118WWAtributosDS_8_Dynamicfiltersoperator2 ;
      private short AV128WWAtributosDS_18_Tfatributos_ativo_sel ;
      private short AV129WWAtributosDS_19_Tfatributos_pk_sel ;
      private short AV130WWAtributosDS_20_Tfatributos_fk_sel ;
      private short edtAtributos_Nome_Titleformat ;
      private short cmbAtributos_TipoDados_Titleformat ;
      private short edtAtributos_Detalhes_Titleformat ;
      private short edtAtributos_TabelaNom_Titleformat ;
      private short chkAtributos_Ativo_Titleformat ;
      private short chkAtributos_PK_Titleformat ;
      private short chkAtributos_FK_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV60Sistema_AreaTrabalhoCod ;
      private int A176Atributos_Codigo ;
      private int AV63Sistema_Codigo ;
      private int A356Atributos_TabelaCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_atributos_nome_Datalistupdateminimumcharacters ;
      private int Ddo_atributos_tipodados_Datalistupdateminimumcharacters ;
      private int Ddo_atributos_detalhes_Datalistupdateminimumcharacters ;
      private int Ddo_atributos_tabelanom_Datalistupdateminimumcharacters ;
      private int Ddo_atributos_ativo_Datalistupdateminimumcharacters ;
      private int Ddo_atributos_pk_Datalistupdateminimumcharacters ;
      private int Ddo_atributos_fk_Datalistupdateminimumcharacters ;
      private int edtavTfatributos_nome_Visible ;
      private int edtavTfatributos_nome_sel_Visible ;
      private int edtavTfatributos_detalhes_Visible ;
      private int edtavTfatributos_detalhes_sel_Visible ;
      private int edtavTfatributos_tabelanom_Visible ;
      private int edtavTfatributos_tabelanom_sel_Visible ;
      private int edtavTfatributos_ativo_sel_Visible ;
      private int edtavTfatributos_pk_sel_Visible ;
      private int edtavTfatributos_fk_sel_Visible ;
      private int edtavDdo_atributos_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_atributos_tipodadostitlecontrolidtoreplace_Visible ;
      private int edtavDdo_atributos_detalhestitlecontrolidtoreplace_Visible ;
      private int edtavDdo_atributos_tabelanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_atributos_ativotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_atributos_pktitlecontrolidtoreplace_Visible ;
      private int edtavDdo_atributos_fktitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV123WWAtributosDS_13_Tfatributos_tipodados_sels_Count ;
      private int AV111WWAtributosDS_1_Sistema_areatrabalhocod ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A190Tabela_SistemaCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV92PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavAtributos_nome1_Visible ;
      private int edtavAtributos_tabelanom1_Visible ;
      private int edtavAtributos_nome2_Visible ;
      private int edtavAtributos_tabelanom2_Visible ;
      private int AV135GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV93GridCurrentPage ;
      private long AV94GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_atributos_nome_Activeeventkey ;
      private String Ddo_atributos_nome_Filteredtext_get ;
      private String Ddo_atributos_nome_Selectedvalue_get ;
      private String Ddo_atributos_tipodados_Activeeventkey ;
      private String Ddo_atributos_tipodados_Selectedvalue_get ;
      private String Ddo_atributos_detalhes_Activeeventkey ;
      private String Ddo_atributos_detalhes_Filteredtext_get ;
      private String Ddo_atributos_detalhes_Selectedvalue_get ;
      private String Ddo_atributos_tabelanom_Activeeventkey ;
      private String Ddo_atributos_tabelanom_Filteredtext_get ;
      private String Ddo_atributos_tabelanom_Selectedvalue_get ;
      private String Ddo_atributos_ativo_Activeeventkey ;
      private String Ddo_atributos_ativo_Selectedvalue_get ;
      private String Ddo_atributos_pk_Activeeventkey ;
      private String Ddo_atributos_pk_Selectedvalue_get ;
      private String Ddo_atributos_fk_Activeeventkey ;
      private String Ddo_atributos_fk_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_79_idx="0001" ;
      private String AV17Atributos_Nome1 ;
      private String AV61Atributos_TabelaNom1 ;
      private String AV23Atributos_Nome2 ;
      private String AV62Atributos_TabelaNom2 ;
      private String AV67TFAtributos_Nome ;
      private String AV68TFAtributos_Nome_Sel ;
      private String AV75TFAtributos_Detalhes ;
      private String AV76TFAtributos_Detalhes_Sel ;
      private String AV79TFAtributos_TabelaNom ;
      private String AV80TFAtributos_TabelaNom_Sel ;
      private String AV134Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_atributos_nome_Caption ;
      private String Ddo_atributos_nome_Tooltip ;
      private String Ddo_atributos_nome_Cls ;
      private String Ddo_atributos_nome_Filteredtext_set ;
      private String Ddo_atributos_nome_Selectedvalue_set ;
      private String Ddo_atributos_nome_Dropdownoptionstype ;
      private String Ddo_atributos_nome_Titlecontrolidtoreplace ;
      private String Ddo_atributos_nome_Sortedstatus ;
      private String Ddo_atributos_nome_Filtertype ;
      private String Ddo_atributos_nome_Datalisttype ;
      private String Ddo_atributos_nome_Datalistfixedvalues ;
      private String Ddo_atributos_nome_Datalistproc ;
      private String Ddo_atributos_nome_Sortasc ;
      private String Ddo_atributos_nome_Sortdsc ;
      private String Ddo_atributos_nome_Loadingdata ;
      private String Ddo_atributos_nome_Cleanfilter ;
      private String Ddo_atributos_nome_Rangefilterfrom ;
      private String Ddo_atributos_nome_Rangefilterto ;
      private String Ddo_atributos_nome_Noresultsfound ;
      private String Ddo_atributos_nome_Searchbuttontext ;
      private String Ddo_atributos_tipodados_Caption ;
      private String Ddo_atributos_tipodados_Tooltip ;
      private String Ddo_atributos_tipodados_Cls ;
      private String Ddo_atributos_tipodados_Selectedvalue_set ;
      private String Ddo_atributos_tipodados_Dropdownoptionstype ;
      private String Ddo_atributos_tipodados_Titlecontrolidtoreplace ;
      private String Ddo_atributos_tipodados_Sortedstatus ;
      private String Ddo_atributos_tipodados_Datalisttype ;
      private String Ddo_atributos_tipodados_Datalistfixedvalues ;
      private String Ddo_atributos_tipodados_Sortasc ;
      private String Ddo_atributos_tipodados_Sortdsc ;
      private String Ddo_atributos_tipodados_Loadingdata ;
      private String Ddo_atributos_tipodados_Cleanfilter ;
      private String Ddo_atributos_tipodados_Rangefilterfrom ;
      private String Ddo_atributos_tipodados_Rangefilterto ;
      private String Ddo_atributos_tipodados_Noresultsfound ;
      private String Ddo_atributos_tipodados_Searchbuttontext ;
      private String Ddo_atributos_detalhes_Caption ;
      private String Ddo_atributos_detalhes_Tooltip ;
      private String Ddo_atributos_detalhes_Cls ;
      private String Ddo_atributos_detalhes_Filteredtext_set ;
      private String Ddo_atributos_detalhes_Selectedvalue_set ;
      private String Ddo_atributos_detalhes_Dropdownoptionstype ;
      private String Ddo_atributos_detalhes_Titlecontrolidtoreplace ;
      private String Ddo_atributos_detalhes_Sortedstatus ;
      private String Ddo_atributos_detalhes_Filtertype ;
      private String Ddo_atributos_detalhes_Datalisttype ;
      private String Ddo_atributos_detalhes_Datalistfixedvalues ;
      private String Ddo_atributos_detalhes_Datalistproc ;
      private String Ddo_atributos_detalhes_Sortasc ;
      private String Ddo_atributos_detalhes_Sortdsc ;
      private String Ddo_atributos_detalhes_Loadingdata ;
      private String Ddo_atributos_detalhes_Cleanfilter ;
      private String Ddo_atributos_detalhes_Rangefilterfrom ;
      private String Ddo_atributos_detalhes_Rangefilterto ;
      private String Ddo_atributos_detalhes_Noresultsfound ;
      private String Ddo_atributos_detalhes_Searchbuttontext ;
      private String Ddo_atributos_tabelanom_Caption ;
      private String Ddo_atributos_tabelanom_Tooltip ;
      private String Ddo_atributos_tabelanom_Cls ;
      private String Ddo_atributos_tabelanom_Filteredtext_set ;
      private String Ddo_atributos_tabelanom_Selectedvalue_set ;
      private String Ddo_atributos_tabelanom_Dropdownoptionstype ;
      private String Ddo_atributos_tabelanom_Titlecontrolidtoreplace ;
      private String Ddo_atributos_tabelanom_Sortedstatus ;
      private String Ddo_atributos_tabelanom_Filtertype ;
      private String Ddo_atributos_tabelanom_Datalisttype ;
      private String Ddo_atributos_tabelanom_Datalistfixedvalues ;
      private String Ddo_atributos_tabelanom_Datalistproc ;
      private String Ddo_atributos_tabelanom_Sortasc ;
      private String Ddo_atributos_tabelanom_Sortdsc ;
      private String Ddo_atributos_tabelanom_Loadingdata ;
      private String Ddo_atributos_tabelanom_Cleanfilter ;
      private String Ddo_atributos_tabelanom_Rangefilterfrom ;
      private String Ddo_atributos_tabelanom_Rangefilterto ;
      private String Ddo_atributos_tabelanom_Noresultsfound ;
      private String Ddo_atributos_tabelanom_Searchbuttontext ;
      private String Ddo_atributos_ativo_Caption ;
      private String Ddo_atributos_ativo_Tooltip ;
      private String Ddo_atributos_ativo_Cls ;
      private String Ddo_atributos_ativo_Selectedvalue_set ;
      private String Ddo_atributos_ativo_Dropdownoptionstype ;
      private String Ddo_atributos_ativo_Titlecontrolidtoreplace ;
      private String Ddo_atributos_ativo_Sortedstatus ;
      private String Ddo_atributos_ativo_Datalisttype ;
      private String Ddo_atributos_ativo_Datalistfixedvalues ;
      private String Ddo_atributos_ativo_Sortasc ;
      private String Ddo_atributos_ativo_Sortdsc ;
      private String Ddo_atributos_ativo_Loadingdata ;
      private String Ddo_atributos_ativo_Cleanfilter ;
      private String Ddo_atributos_ativo_Rangefilterfrom ;
      private String Ddo_atributos_ativo_Rangefilterto ;
      private String Ddo_atributos_ativo_Noresultsfound ;
      private String Ddo_atributos_ativo_Searchbuttontext ;
      private String Ddo_atributos_pk_Caption ;
      private String Ddo_atributos_pk_Tooltip ;
      private String Ddo_atributos_pk_Cls ;
      private String Ddo_atributos_pk_Selectedvalue_set ;
      private String Ddo_atributos_pk_Dropdownoptionstype ;
      private String Ddo_atributos_pk_Titlecontrolidtoreplace ;
      private String Ddo_atributos_pk_Sortedstatus ;
      private String Ddo_atributos_pk_Datalisttype ;
      private String Ddo_atributos_pk_Datalistfixedvalues ;
      private String Ddo_atributos_pk_Sortasc ;
      private String Ddo_atributos_pk_Sortdsc ;
      private String Ddo_atributos_pk_Loadingdata ;
      private String Ddo_atributos_pk_Cleanfilter ;
      private String Ddo_atributos_pk_Rangefilterfrom ;
      private String Ddo_atributos_pk_Rangefilterto ;
      private String Ddo_atributos_pk_Noresultsfound ;
      private String Ddo_atributos_pk_Searchbuttontext ;
      private String Ddo_atributos_fk_Caption ;
      private String Ddo_atributos_fk_Tooltip ;
      private String Ddo_atributos_fk_Cls ;
      private String Ddo_atributos_fk_Selectedvalue_set ;
      private String Ddo_atributos_fk_Dropdownoptionstype ;
      private String Ddo_atributos_fk_Titlecontrolidtoreplace ;
      private String Ddo_atributos_fk_Sortedstatus ;
      private String Ddo_atributos_fk_Datalisttype ;
      private String Ddo_atributos_fk_Datalistfixedvalues ;
      private String Ddo_atributos_fk_Sortasc ;
      private String Ddo_atributos_fk_Sortdsc ;
      private String Ddo_atributos_fk_Loadingdata ;
      private String Ddo_atributos_fk_Cleanfilter ;
      private String Ddo_atributos_fk_Rangefilterfrom ;
      private String Ddo_atributos_fk_Rangefilterto ;
      private String Ddo_atributos_fk_Noresultsfound ;
      private String Ddo_atributos_fk_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfatributos_nome_Internalname ;
      private String edtavTfatributos_nome_Jsonclick ;
      private String edtavTfatributos_nome_sel_Internalname ;
      private String edtavTfatributos_nome_sel_Jsonclick ;
      private String edtavTfatributos_detalhes_Internalname ;
      private String edtavTfatributos_detalhes_Jsonclick ;
      private String edtavTfatributos_detalhes_sel_Internalname ;
      private String edtavTfatributos_detalhes_sel_Jsonclick ;
      private String edtavTfatributos_tabelanom_Internalname ;
      private String edtavTfatributos_tabelanom_Jsonclick ;
      private String edtavTfatributos_tabelanom_sel_Internalname ;
      private String edtavTfatributos_tabelanom_sel_Jsonclick ;
      private String edtavTfatributos_ativo_sel_Internalname ;
      private String edtavTfatributos_ativo_sel_Jsonclick ;
      private String edtavTfatributos_pk_sel_Internalname ;
      private String edtavTfatributos_pk_sel_Jsonclick ;
      private String edtavTfatributos_fk_sel_Internalname ;
      private String edtavTfatributos_fk_sel_Jsonclick ;
      private String edtavDdo_atributos_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_atributos_tipodadostitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_atributos_detalhestitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_atributos_tabelanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_atributos_ativotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_atributos_pktitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_atributos_fktitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtAtributos_Codigo_Internalname ;
      private String A177Atributos_Nome ;
      private String edtAtributos_Nome_Internalname ;
      private String cmbAtributos_TipoDados_Internalname ;
      private String A178Atributos_TipoDados ;
      private String A390Atributos_Detalhes ;
      private String edtAtributos_Detalhes_Internalname ;
      private String A357Atributos_TabelaNom ;
      private String edtAtributos_TabelaNom_Internalname ;
      private String chkAtributos_Ativo_Internalname ;
      private String chkAtributos_PK_Internalname ;
      private String chkAtributos_FK_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV114WWAtributosDS_4_Atributos_nome1 ;
      private String lV115WWAtributosDS_5_Atributos_tabelanom1 ;
      private String lV119WWAtributosDS_9_Atributos_nome2 ;
      private String lV120WWAtributosDS_10_Atributos_tabelanom2 ;
      private String lV121WWAtributosDS_11_Tfatributos_nome ;
      private String lV124WWAtributosDS_14_Tfatributos_detalhes ;
      private String lV126WWAtributosDS_16_Tfatributos_tabelanom ;
      private String AV114WWAtributosDS_4_Atributos_nome1 ;
      private String AV115WWAtributosDS_5_Atributos_tabelanom1 ;
      private String AV119WWAtributosDS_9_Atributos_nome2 ;
      private String AV120WWAtributosDS_10_Atributos_tabelanom2 ;
      private String AV122WWAtributosDS_12_Tfatributos_nome_sel ;
      private String AV121WWAtributosDS_11_Tfatributos_nome ;
      private String AV125WWAtributosDS_15_Tfatributos_detalhes_sel ;
      private String AV124WWAtributosDS_14_Tfatributos_detalhes ;
      private String AV127WWAtributosDS_17_Tfatributos_tabelanom_sel ;
      private String AV126WWAtributosDS_16_Tfatributos_tabelanom ;
      private String edtavOrdereddsc_Internalname ;
      private String dynavSistema_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavAtributos_nome1_Internalname ;
      private String edtavAtributos_tabelanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavAtributos_nome2_Internalname ;
      private String edtavAtributos_tabelanom2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_atributos_nome_Internalname ;
      private String Ddo_atributos_tipodados_Internalname ;
      private String Ddo_atributos_detalhes_Internalname ;
      private String Ddo_atributos_tabelanom_Internalname ;
      private String Ddo_atributos_ativo_Internalname ;
      private String Ddo_atributos_pk_Internalname ;
      private String Ddo_atributos_fk_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtAtributos_Nome_Title ;
      private String edtAtributos_Detalhes_Title ;
      private String edtAtributos_TabelaNom_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtAtributos_Nome_Link ;
      private String edtAtributos_TabelaNom_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblAtributostitle_Internalname ;
      private String lblAtributostitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextsistema_areatrabalhocod_Internalname ;
      private String lblFiltertextsistema_areatrabalhocod_Jsonclick ;
      private String dynavSistema_areatrabalhocod_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavAtributos_nome2_Jsonclick ;
      private String edtavAtributos_tabelanom2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavAtributos_nome1_Jsonclick ;
      private String edtavAtributos_tabelanom1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_79_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAtributos_Codigo_Jsonclick ;
      private String edtAtributos_Nome_Jsonclick ;
      private String cmbAtributos_TipoDados_Jsonclick ;
      private String edtAtributos_Detalhes_Jsonclick ;
      private String edtAtributos_TabelaNom_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_atributos_nome_Includesortasc ;
      private bool Ddo_atributos_nome_Includesortdsc ;
      private bool Ddo_atributos_nome_Includefilter ;
      private bool Ddo_atributos_nome_Filterisrange ;
      private bool Ddo_atributos_nome_Includedatalist ;
      private bool Ddo_atributos_tipodados_Includesortasc ;
      private bool Ddo_atributos_tipodados_Includesortdsc ;
      private bool Ddo_atributos_tipodados_Includefilter ;
      private bool Ddo_atributos_tipodados_Filterisrange ;
      private bool Ddo_atributos_tipodados_Includedatalist ;
      private bool Ddo_atributos_tipodados_Allowmultipleselection ;
      private bool Ddo_atributos_detalhes_Includesortasc ;
      private bool Ddo_atributos_detalhes_Includesortdsc ;
      private bool Ddo_atributos_detalhes_Includefilter ;
      private bool Ddo_atributos_detalhes_Filterisrange ;
      private bool Ddo_atributos_detalhes_Includedatalist ;
      private bool Ddo_atributos_tabelanom_Includesortasc ;
      private bool Ddo_atributos_tabelanom_Includesortdsc ;
      private bool Ddo_atributos_tabelanom_Includefilter ;
      private bool Ddo_atributos_tabelanom_Filterisrange ;
      private bool Ddo_atributos_tabelanom_Includedatalist ;
      private bool Ddo_atributos_ativo_Includesortasc ;
      private bool Ddo_atributos_ativo_Includesortdsc ;
      private bool Ddo_atributos_ativo_Includefilter ;
      private bool Ddo_atributos_ativo_Filterisrange ;
      private bool Ddo_atributos_ativo_Includedatalist ;
      private bool Ddo_atributos_pk_Includesortasc ;
      private bool Ddo_atributos_pk_Includesortdsc ;
      private bool Ddo_atributos_pk_Includefilter ;
      private bool Ddo_atributos_pk_Filterisrange ;
      private bool Ddo_atributos_pk_Includedatalist ;
      private bool Ddo_atributos_fk_Includesortasc ;
      private bool Ddo_atributos_fk_Includesortdsc ;
      private bool Ddo_atributos_fk_Includefilter ;
      private bool Ddo_atributos_fk_Filterisrange ;
      private bool Ddo_atributos_fk_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n178Atributos_TipoDados ;
      private bool n390Atributos_Detalhes ;
      private bool n357Atributos_TabelaNom ;
      private bool A180Atributos_Ativo ;
      private bool A400Atributos_PK ;
      private bool n400Atributos_PK ;
      private bool A401Atributos_FK ;
      private bool n401Atributos_FK ;
      private bool AV116WWAtributosDS_6_Dynamicfiltersenabled2 ;
      private bool n190Tabela_SistemaCod ;
      private bool n135Sistema_AreaTrabalhoCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV34Update_IsBlob ;
      private bool AV35Delete_IsBlob ;
      private bool AV58Display_IsBlob ;
      private String AV71TFAtributos_TipoDados_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV69ddo_Atributos_NomeTitleControlIdToReplace ;
      private String AV73ddo_Atributos_TipoDadosTitleControlIdToReplace ;
      private String AV77ddo_Atributos_DetalhesTitleControlIdToReplace ;
      private String AV81ddo_Atributos_TabelaNomTitleControlIdToReplace ;
      private String AV84ddo_Atributos_AtivoTitleControlIdToReplace ;
      private String AV87ddo_Atributos_PKTitleControlIdToReplace ;
      private String AV90ddo_Atributos_FKTitleControlIdToReplace ;
      private String AV131Update_GXI ;
      private String AV132Delete_GXI ;
      private String AV133Display_GXI ;
      private String AV112WWAtributosDS_2_Dynamicfiltersselector1 ;
      private String AV117WWAtributosDS_7_Dynamicfiltersselector2 ;
      private String AV34Update ;
      private String AV35Delete ;
      private String AV58Display ;
      private IGxSession AV36Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox dynavSistema_areatrabalhocod ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbAtributos_TipoDados ;
      private GXCheckbox chkAtributos_Ativo ;
      private GXCheckbox chkAtributos_PK ;
      private GXCheckbox chkAtributos_FK ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H004C2_A5AreaTrabalho_Codigo ;
      private String[] H004C2_A6AreaTrabalho_Descricao ;
      private bool[] H004C2_A72AreaTrabalho_Ativo ;
      private int[] H004C3_A190Tabela_SistemaCod ;
      private bool[] H004C3_n190Tabela_SistemaCod ;
      private int[] H004C3_A135Sistema_AreaTrabalhoCod ;
      private bool[] H004C3_n135Sistema_AreaTrabalhoCod ;
      private int[] H004C3_A356Atributos_TabelaCod ;
      private bool[] H004C3_A401Atributos_FK ;
      private bool[] H004C3_n401Atributos_FK ;
      private bool[] H004C3_A400Atributos_PK ;
      private bool[] H004C3_n400Atributos_PK ;
      private bool[] H004C3_A180Atributos_Ativo ;
      private String[] H004C3_A357Atributos_TabelaNom ;
      private bool[] H004C3_n357Atributos_TabelaNom ;
      private String[] H004C3_A390Atributos_Detalhes ;
      private bool[] H004C3_n390Atributos_Detalhes ;
      private String[] H004C3_A178Atributos_TipoDados ;
      private bool[] H004C3_n178Atributos_TipoDados ;
      private String[] H004C3_A177Atributos_Nome ;
      private int[] H004C3_A176Atributos_Codigo ;
      private long[] H004C4_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV72TFAtributos_TipoDados_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV123WWAtributosDS_13_Tfatributos_tipodados_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66Atributos_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV70Atributos_TipoDadosTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV74Atributos_DetalhesTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV78Atributos_TabelaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV82Atributos_AtivoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV85Atributos_PKTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV88Atributos_FKTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV91DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwatributos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H004C3( IGxContext context ,
                                             String A178Atributos_TipoDados ,
                                             IGxCollection AV123WWAtributosDS_13_Tfatributos_tipodados_sels ,
                                             int AV111WWAtributosDS_1_Sistema_areatrabalhocod ,
                                             String AV112WWAtributosDS_2_Dynamicfiltersselector1 ,
                                             short AV113WWAtributosDS_3_Dynamicfiltersoperator1 ,
                                             String AV114WWAtributosDS_4_Atributos_nome1 ,
                                             String AV115WWAtributosDS_5_Atributos_tabelanom1 ,
                                             bool AV116WWAtributosDS_6_Dynamicfiltersenabled2 ,
                                             String AV117WWAtributosDS_7_Dynamicfiltersselector2 ,
                                             short AV118WWAtributosDS_8_Dynamicfiltersoperator2 ,
                                             String AV119WWAtributosDS_9_Atributos_nome2 ,
                                             String AV120WWAtributosDS_10_Atributos_tabelanom2 ,
                                             String AV122WWAtributosDS_12_Tfatributos_nome_sel ,
                                             String AV121WWAtributosDS_11_Tfatributos_nome ,
                                             int AV123WWAtributosDS_13_Tfatributos_tipodados_sels_Count ,
                                             String AV125WWAtributosDS_15_Tfatributos_detalhes_sel ,
                                             String AV124WWAtributosDS_14_Tfatributos_detalhes ,
                                             String AV127WWAtributosDS_17_Tfatributos_tabelanom_sel ,
                                             String AV126WWAtributosDS_16_Tfatributos_tabelanom ,
                                             short AV128WWAtributosDS_18_Tfatributos_ativo_sel ,
                                             short AV129WWAtributosDS_19_Tfatributos_pk_sel ,
                                             short AV130WWAtributosDS_20_Tfatributos_fk_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A177Atributos_Nome ,
                                             String A357Atributos_TabelaNom ,
                                             String A390Atributos_Detalhes ,
                                             bool A180Atributos_Ativo ,
                                             bool A400Atributos_PK ,
                                             bool A401Atributos_FK ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [20] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Tabela_SistemaCod] AS Tabela_SistemaCod, T3.[Sistema_AreaTrabalhoCod], T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T1.[Atributos_FK], T1.[Atributos_PK], T1.[Atributos_Ativo], T2.[Tabela_Nome] AS Atributos_TabelaNom, T1.[Atributos_Detalhes], T1.[Atributos_TipoDados], T1.[Atributos_Nome], T1.[Atributos_Codigo]";
         sFromString = " FROM (([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Tabela_SistemaCod])";
         sOrderString = "";
         if ( ! (0==AV111WWAtributosDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_AreaTrabalhoCod] = @AV111WWAtributosDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_AreaTrabalhoCod] = @AV111WWAtributosDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV112WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_NOME") == 0 ) && ( AV113WWAtributosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWAtributosDS_4_Atributos_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV114WWAtributosDS_4_Atributos_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV114WWAtributosDS_4_Atributos_nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV112WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_NOME") == 0 ) && ( AV113WWAtributosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWAtributosDS_4_Atributos_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV114WWAtributosDS_4_Atributos_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV114WWAtributosDS_4_Atributos_nome1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV112WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV113WWAtributosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWAtributosDS_5_Atributos_tabelanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV115WWAtributosDS_5_Atributos_tabelanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV115WWAtributosDS_5_Atributos_tabelanom1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV112WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV113WWAtributosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWAtributosDS_5_Atributos_tabelanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV115WWAtributosDS_5_Atributos_tabelanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV115WWAtributosDS_5_Atributos_tabelanom1)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV116WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_NOME") == 0 ) && ( AV118WWAtributosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWAtributosDS_9_Atributos_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV119WWAtributosDS_9_Atributos_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV119WWAtributosDS_9_Atributos_nome2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV116WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_NOME") == 0 ) && ( AV118WWAtributosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWAtributosDS_9_Atributos_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV119WWAtributosDS_9_Atributos_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV119WWAtributosDS_9_Atributos_nome2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV116WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV118WWAtributosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAtributosDS_10_Atributos_tabelanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV120WWAtributosDS_10_Atributos_tabelanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV120WWAtributosDS_10_Atributos_tabelanom2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV116WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV118WWAtributosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAtributosDS_10_Atributos_tabelanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV120WWAtributosDS_10_Atributos_tabelanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV120WWAtributosDS_10_Atributos_tabelanom2)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV122WWAtributosDS_12_Tfatributos_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWAtributosDS_11_Tfatributos_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV121WWAtributosDS_11_Tfatributos_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV121WWAtributosDS_11_Tfatributos_nome)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WWAtributosDS_12_Tfatributos_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] = @AV122WWAtributosDS_12_Tfatributos_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] = @AV122WWAtributosDS_12_Tfatributos_nome_sel)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV123WWAtributosDS_13_Tfatributos_tipodados_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV123WWAtributosDS_13_Tfatributos_tipodados_sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV123WWAtributosDS_13_Tfatributos_tipodados_sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV125WWAtributosDS_15_Tfatributos_detalhes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWAtributosDS_14_Tfatributos_detalhes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] like @lV124WWAtributosDS_14_Tfatributos_detalhes)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] like @lV124WWAtributosDS_14_Tfatributos_detalhes)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125WWAtributosDS_15_Tfatributos_detalhes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] = @AV125WWAtributosDS_15_Tfatributos_detalhes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] = @AV125WWAtributosDS_15_Tfatributos_detalhes_sel)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV127WWAtributosDS_17_Tfatributos_tabelanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126WWAtributosDS_16_Tfatributos_tabelanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV126WWAtributosDS_16_Tfatributos_tabelanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV126WWAtributosDS_16_Tfatributos_tabelanom)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127WWAtributosDS_17_Tfatributos_tabelanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV127WWAtributosDS_17_Tfatributos_tabelanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] = @AV127WWAtributosDS_17_Tfatributos_tabelanom_sel)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV128WWAtributosDS_18_Tfatributos_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 1)";
            }
         }
         if ( AV128WWAtributosDS_18_Tfatributos_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 0)";
            }
         }
         if ( AV129WWAtributosDS_19_Tfatributos_pk_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 1)";
            }
         }
         if ( AV129WWAtributosDS_19_Tfatributos_pk_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 0)";
            }
         }
         if ( AV130WWAtributosDS_20_Tfatributos_fk_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 1)";
            }
         }
         if ( AV130WWAtributosDS_20_Tfatributos_fk_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TipoDados]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_TipoDados] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_Detalhes]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_Detalhes] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Tabela_Nome]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Tabela_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_Ativo]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_Ativo] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_PK]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_PK] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_FK]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_FK] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Atributos_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H004C4( IGxContext context ,
                                             String A178Atributos_TipoDados ,
                                             IGxCollection AV123WWAtributosDS_13_Tfatributos_tipodados_sels ,
                                             int AV111WWAtributosDS_1_Sistema_areatrabalhocod ,
                                             String AV112WWAtributosDS_2_Dynamicfiltersselector1 ,
                                             short AV113WWAtributosDS_3_Dynamicfiltersoperator1 ,
                                             String AV114WWAtributosDS_4_Atributos_nome1 ,
                                             String AV115WWAtributosDS_5_Atributos_tabelanom1 ,
                                             bool AV116WWAtributosDS_6_Dynamicfiltersenabled2 ,
                                             String AV117WWAtributosDS_7_Dynamicfiltersselector2 ,
                                             short AV118WWAtributosDS_8_Dynamicfiltersoperator2 ,
                                             String AV119WWAtributosDS_9_Atributos_nome2 ,
                                             String AV120WWAtributosDS_10_Atributos_tabelanom2 ,
                                             String AV122WWAtributosDS_12_Tfatributos_nome_sel ,
                                             String AV121WWAtributosDS_11_Tfatributos_nome ,
                                             int AV123WWAtributosDS_13_Tfatributos_tipodados_sels_Count ,
                                             String AV125WWAtributosDS_15_Tfatributos_detalhes_sel ,
                                             String AV124WWAtributosDS_14_Tfatributos_detalhes ,
                                             String AV127WWAtributosDS_17_Tfatributos_tabelanom_sel ,
                                             String AV126WWAtributosDS_16_Tfatributos_tabelanom ,
                                             short AV128WWAtributosDS_18_Tfatributos_ativo_sel ,
                                             short AV129WWAtributosDS_19_Tfatributos_pk_sel ,
                                             short AV130WWAtributosDS_20_Tfatributos_fk_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A177Atributos_Nome ,
                                             String A357Atributos_TabelaNom ,
                                             String A390Atributos_Detalhes ,
                                             bool A180Atributos_Ativo ,
                                             bool A400Atributos_PK ,
                                             bool A401Atributos_FK ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [15] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Tabela_SistemaCod])";
         if ( ! (0==AV111WWAtributosDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_AreaTrabalhoCod] = @AV111WWAtributosDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_AreaTrabalhoCod] = @AV111WWAtributosDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV112WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_NOME") == 0 ) && ( AV113WWAtributosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWAtributosDS_4_Atributos_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV114WWAtributosDS_4_Atributos_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV114WWAtributosDS_4_Atributos_nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV112WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_NOME") == 0 ) && ( AV113WWAtributosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWAtributosDS_4_Atributos_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV114WWAtributosDS_4_Atributos_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV114WWAtributosDS_4_Atributos_nome1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV112WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV113WWAtributosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWAtributosDS_5_Atributos_tabelanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV115WWAtributosDS_5_Atributos_tabelanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV115WWAtributosDS_5_Atributos_tabelanom1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV112WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV113WWAtributosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWAtributosDS_5_Atributos_tabelanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV115WWAtributosDS_5_Atributos_tabelanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV115WWAtributosDS_5_Atributos_tabelanom1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV116WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_NOME") == 0 ) && ( AV118WWAtributosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWAtributosDS_9_Atributos_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV119WWAtributosDS_9_Atributos_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV119WWAtributosDS_9_Atributos_nome2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV116WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_NOME") == 0 ) && ( AV118WWAtributosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWAtributosDS_9_Atributos_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV119WWAtributosDS_9_Atributos_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV119WWAtributosDS_9_Atributos_nome2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV116WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV118WWAtributosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAtributosDS_10_Atributos_tabelanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV120WWAtributosDS_10_Atributos_tabelanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV120WWAtributosDS_10_Atributos_tabelanom2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV116WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV117WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV118WWAtributosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWAtributosDS_10_Atributos_tabelanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV120WWAtributosDS_10_Atributos_tabelanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV120WWAtributosDS_10_Atributos_tabelanom2)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV122WWAtributosDS_12_Tfatributos_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWAtributosDS_11_Tfatributos_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV121WWAtributosDS_11_Tfatributos_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV121WWAtributosDS_11_Tfatributos_nome)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WWAtributosDS_12_Tfatributos_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] = @AV122WWAtributosDS_12_Tfatributos_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] = @AV122WWAtributosDS_12_Tfatributos_nome_sel)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV123WWAtributosDS_13_Tfatributos_tipodados_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV123WWAtributosDS_13_Tfatributos_tipodados_sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV123WWAtributosDS_13_Tfatributos_tipodados_sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV125WWAtributosDS_15_Tfatributos_detalhes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWAtributosDS_14_Tfatributos_detalhes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] like @lV124WWAtributosDS_14_Tfatributos_detalhes)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] like @lV124WWAtributosDS_14_Tfatributos_detalhes)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125WWAtributosDS_15_Tfatributos_detalhes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] = @AV125WWAtributosDS_15_Tfatributos_detalhes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] = @AV125WWAtributosDS_15_Tfatributos_detalhes_sel)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV127WWAtributosDS_17_Tfatributos_tabelanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126WWAtributosDS_16_Tfatributos_tabelanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV126WWAtributosDS_16_Tfatributos_tabelanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV126WWAtributosDS_16_Tfatributos_tabelanom)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127WWAtributosDS_17_Tfatributos_tabelanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV127WWAtributosDS_17_Tfatributos_tabelanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] = @AV127WWAtributosDS_17_Tfatributos_tabelanom_sel)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV128WWAtributosDS_18_Tfatributos_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 1)";
            }
         }
         if ( AV128WWAtributosDS_18_Tfatributos_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 0)";
            }
         }
         if ( AV129WWAtributosDS_19_Tfatributos_pk_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 1)";
            }
         }
         if ( AV129WWAtributosDS_19_Tfatributos_pk_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 0)";
            }
         }
         if ( AV130WWAtributosDS_20_Tfatributos_fk_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 1)";
            }
         }
         if ( AV130WWAtributosDS_20_Tfatributos_fk_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H004C3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (bool)dynConstraints[27] , (bool)dynConstraints[28] , (short)dynConstraints[29] , (bool)dynConstraints[30] );
               case 2 :
                     return conditional_H004C4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (bool)dynConstraints[27] , (bool)dynConstraints[28] , (short)dynConstraints[29] , (bool)dynConstraints[30] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH004C2 ;
          prmH004C2 = new Object[] {
          } ;
          Object[] prmH004C3 ;
          prmH004C3 = new Object[] {
          new Object[] {"@AV111WWAtributosDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV114WWAtributosDS_4_Atributos_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV114WWAtributosDS_4_Atributos_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV115WWAtributosDS_5_Atributos_tabelanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV115WWAtributosDS_5_Atributos_tabelanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV119WWAtributosDS_9_Atributos_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV119WWAtributosDS_9_Atributos_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV120WWAtributosDS_10_Atributos_tabelanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV120WWAtributosDS_10_Atributos_tabelanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV121WWAtributosDS_11_Tfatributos_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV122WWAtributosDS_12_Tfatributos_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV124WWAtributosDS_14_Tfatributos_detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@AV125WWAtributosDS_15_Tfatributos_detalhes_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV126WWAtributosDS_16_Tfatributos_tabelanom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV127WWAtributosDS_17_Tfatributos_tabelanom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH004C4 ;
          prmH004C4 = new Object[] {
          new Object[] {"@AV111WWAtributosDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV114WWAtributosDS_4_Atributos_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV114WWAtributosDS_4_Atributos_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV115WWAtributosDS_5_Atributos_tabelanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV115WWAtributosDS_5_Atributos_tabelanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV119WWAtributosDS_9_Atributos_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV119WWAtributosDS_9_Atributos_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV120WWAtributosDS_10_Atributos_tabelanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV120WWAtributosDS_10_Atributos_tabelanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV121WWAtributosDS_11_Tfatributos_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV122WWAtributosDS_12_Tfatributos_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV124WWAtributosDS_14_Tfatributos_detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@AV125WWAtributosDS_15_Tfatributos_detalhes_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV126WWAtributosDS_16_Tfatributos_tabelanom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV127WWAtributosDS_17_Tfatributos_tabelanom_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H004C2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao], [AreaTrabalho_Ativo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Ativo] = 1 ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH004C2,0,0,true,false )
             ,new CursorDef("H004C3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH004C3,11,0,true,false )
             ,new CursorDef("H004C4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH004C4,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 4) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
       }
    }

 }

}
