/*
               File: PRC_EmailInstanciaDestinatario_Submit
        Description: PRC_Email Instancia Destinatario_Submit
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:43.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_emailinstanciadestinatario_submit : GXProcedure
   {
      public prc_emailinstanciadestinatario_submit( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_emailinstanciadestinatario_submit( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         prc_emailinstanciadestinatario_submit objprc_emailinstanciadestinatario_submit;
         objprc_emailinstanciadestinatario_submit = new prc_emailinstanciadestinatario_submit();
         objprc_emailinstanciadestinatario_submit.context.SetSubmitInitialConfig(context);
         objprc_emailinstanciadestinatario_submit.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_emailinstanciadestinatario_submit);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_emailinstanciadestinatario_submit)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CA2 */
         pr_default.execute(0, new Object[] {AV8Email_Instancia_Destinatarios_Guid});
         while ( (pr_default.getStatus(0) != 101) )
         {
            GXTCA2 = 0;
            A1666Email_Instancia_Guid = (Guid)((Guid)(P00CA2_A1666Email_Instancia_Guid[0]));
            A1667Email_Instancia_Destinatarios_Guid = (Guid)((Guid)(P00CA2_A1667Email_Instancia_Destinatarios_Guid[0]));
            A1675Email_Instancia_Destinatarios_Email = P00CA2_A1675Email_Instancia_Destinatarios_Email[0];
            A1673Email_Instancia_Titulo = P00CA2_A1673Email_Instancia_Titulo[0];
            A1674Email_Instancia_Corpo = P00CA2_A1674Email_Instancia_Corpo[0];
            A1676Email_Instancia_Destinatarios_IsEnviado = P00CA2_A1676Email_Instancia_Destinatarios_IsEnviado[0];
            A1673Email_Instancia_Titulo = P00CA2_A1673Email_Instancia_Titulo[0];
            A1674Email_Instancia_Corpo = P00CA2_A1674Email_Instancia_Corpo[0];
            new prc_enviar_email_new(context ).execute(  A1675Email_Instancia_Destinatarios_Email,  A1675Email_Instancia_Destinatarios_Email,  A1673Email_Instancia_Titulo,  A1674Email_Instancia_Corpo) ;
            A1676Email_Instancia_Destinatarios_IsEnviado = true;
            GXTCA2 = 1;
            /* Using cursor P00CA3 */
            pr_default.execute(1, new Object[] {A1676Email_Instancia_Destinatarios_IsEnviado, A1666Email_Instancia_Guid, A1667Email_Instancia_Destinatarios_Guid});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("Email_Instancia_Destinatarios") ;
            if ( GXTCA2 == 1 )
            {
               context.CommitDataStores( "PRC_EmailInstanciaDestinatario_Submit");
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_EmailInstanciaDestinatario_Submit");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         AV8Email_Instancia_Destinatarios_Guid = (Guid)(System.Guid.Empty);
         P00CA2_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         P00CA2_A1667Email_Instancia_Destinatarios_Guid = new Guid[] {System.Guid.Empty} ;
         P00CA2_A1675Email_Instancia_Destinatarios_Email = new String[] {""} ;
         P00CA2_A1673Email_Instancia_Titulo = new String[] {""} ;
         P00CA2_A1674Email_Instancia_Corpo = new String[] {""} ;
         P00CA2_A1676Email_Instancia_Destinatarios_IsEnviado = new bool[] {false} ;
         A1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         A1667Email_Instancia_Destinatarios_Guid = (Guid)(System.Guid.Empty);
         A1675Email_Instancia_Destinatarios_Email = "";
         A1673Email_Instancia_Titulo = "";
         A1674Email_Instancia_Corpo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_emailinstanciadestinatario_submit__default(),
            new Object[][] {
                new Object[] {
               P00CA2_A1666Email_Instancia_Guid, P00CA2_A1667Email_Instancia_Destinatarios_Guid, P00CA2_A1675Email_Instancia_Destinatarios_Email, P00CA2_A1673Email_Instancia_Titulo, P00CA2_A1674Email_Instancia_Corpo, P00CA2_A1676Email_Instancia_Destinatarios_IsEnviado
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short GXTCA2 ;
      private String scmdbuf ;
      private bool A1676Email_Instancia_Destinatarios_IsEnviado ;
      private String A1674Email_Instancia_Corpo ;
      private String A1675Email_Instancia_Destinatarios_Email ;
      private String A1673Email_Instancia_Titulo ;
      private Guid AV8Email_Instancia_Destinatarios_Guid ;
      private Guid A1666Email_Instancia_Guid ;
      private Guid A1667Email_Instancia_Destinatarios_Guid ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private Guid[] P00CA2_A1666Email_Instancia_Guid ;
      private Guid[] P00CA2_A1667Email_Instancia_Destinatarios_Guid ;
      private String[] P00CA2_A1675Email_Instancia_Destinatarios_Email ;
      private String[] P00CA2_A1673Email_Instancia_Titulo ;
      private String[] P00CA2_A1674Email_Instancia_Corpo ;
      private bool[] P00CA2_A1676Email_Instancia_Destinatarios_IsEnviado ;
   }

   public class prc_emailinstanciadestinatario_submit__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CA2 ;
          prmP00CA2 = new Object[] {
          new Object[] {"@AV8Email_Instancia_Destinatarios_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmP00CA3 ;
          prmP00CA3 = new Object[] {
          new Object[] {"@Email_Instancia_Destinatarios_IsEnviado",SqlDbType.Bit,4,0} ,
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Destinatarios_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CA2", "SELECT T1.[Email_Instancia_Guid], T1.[Email_Instancia_Destinatarios_Guid], T1.[Email_Instancia_Destinatarios_Email], T2.[Email_Instancia_Titulo], T2.[Email_Instancia_Corpo], T1.[Email_Instancia_Destinatarios_IsEnviado] FROM ([Email_Instancia_Destinatarios] T1 WITH (UPDLOCK) INNER JOIN [Email_Instancia] T2 WITH (NOLOCK) ON T2.[Email_Instancia_Guid] = T1.[Email_Instancia_Guid]) WHERE T1.[Email_Instancia_Destinatarios_Guid] = @AV8Email_Instancia_Destinatarios_Guid ORDER BY T1.[Email_Instancia_Guid], T1.[Email_Instancia_Destinatarios_Guid] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CA2,1,0,true,false )
             ,new CursorDef("P00CA3", "UPDATE [Email_Instancia_Destinatarios] SET [Email_Instancia_Destinatarios_IsEnviado]=@Email_Instancia_Destinatarios_IsEnviado  WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid AND [Email_Instancia_Destinatarios_Guid] = @Email_Instancia_Destinatarios_Guid", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00CA3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (Guid)parms[1]);
                stmt.SetParameter(3, (Guid)parms[2]);
                return;
       }
    }

 }

}
