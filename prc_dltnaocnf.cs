/*
               File: PRC_DLTNaoCnf
        Description: PRC_DLTNao Cnf
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:15.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_dltnaocnf : GXProcedure
   {
      public prc_dltnaocnf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_dltnaocnf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoNaoCnf_Codigo )
      {
         this.A2024ContagemResultadoNaoCnf_Codigo = aP0_ContagemResultadoNaoCnf_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultadoNaoCnf_Codigo=this.A2024ContagemResultadoNaoCnf_Codigo;
      }

      public int executeUdp( )
      {
         this.A2024ContagemResultadoNaoCnf_Codigo = aP0_ContagemResultadoNaoCnf_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultadoNaoCnf_Codigo=this.A2024ContagemResultadoNaoCnf_Codigo;
         return A2024ContagemResultadoNaoCnf_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoNaoCnf_Codigo )
      {
         prc_dltnaocnf objprc_dltnaocnf;
         objprc_dltnaocnf = new prc_dltnaocnf();
         objprc_dltnaocnf.A2024ContagemResultadoNaoCnf_Codigo = aP0_ContagemResultadoNaoCnf_Codigo;
         objprc_dltnaocnf.context.SetSubmitInitialConfig(context);
         objprc_dltnaocnf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_dltnaocnf);
         aP0_ContagemResultadoNaoCnf_Codigo=this.A2024ContagemResultadoNaoCnf_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_dltnaocnf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00X02 */
         pr_default.execute(0, new Object[] {A2024ContagemResultadoNaoCnf_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            /* Using cursor P00X03 */
            pr_default.execute(1, new Object[] {A2024ContagemResultadoNaoCnf_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNaoCnf") ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00X02_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_dltnaocnf__default(),
            new Object[][] {
                new Object[] {
               P00X02_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A2024ContagemResultadoNaoCnf_Codigo ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoNaoCnf_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00X02_A2024ContagemResultadoNaoCnf_Codigo ;
   }

   public class prc_dltnaocnf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00X02 ;
          prmP00X02 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X03 ;
          prmP00X03 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00X02", "SELECT TOP 1 [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (UPDLOCK) WHERE [ContagemResultadoNaoCnf_Codigo] = @ContagemResultadoNaoCnf_Codigo ORDER BY [ContagemResultadoNaoCnf_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00X02,1,0,true,true )
             ,new CursorDef("P00X03", "DELETE FROM [ContagemResultadoNaoCnf]  WHERE [ContagemResultadoNaoCnf_Codigo] = @ContagemResultadoNaoCnf_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X03)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
