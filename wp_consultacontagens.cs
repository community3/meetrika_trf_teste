/*
               File: WP_ConsultaContagens
        Description: Consulta de Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:39:16.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_consultacontagens : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_consultacontagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_consultacontagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavSistema_codigo = new GXCombobox();
         cmbavContagem_tipo = new GXCombobox();
         cmbavFiltro_ano = new GXCombobox();
         cmbavFiltro_mes = new GXCombobox();
         cmbContagem_Tipo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSISTEMA_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSISTEMA_CODIGOGH2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_21 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_21_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_21_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               AV9Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
               AV24Filtro_Coordenacao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Filtro_Coordenacao", AV24Filtro_Coordenacao);
               AV60Contagem_Tipo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Contagem_Tipo", AV60Contagem_Tipo);
               AV23Filtro_Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)));
               AV26Filtro_Mes = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV57WWPContext);
               AV55tQtdItm = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55tQtdItm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55tQtdItm), 8, 0)));
               A264Contagem_QtdItens = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n264Contagem_QtdItens = false;
               AV52tPFB = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52tPFB", StringUtil.LTrim( StringUtil.Str( AV52tPFB, 14, 5)));
               A943Contagem_PFB = NumberUtil.Val( GetNextPar( ), ".");
               n943Contagem_PFB = false;
               AV53tPFL = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFL", StringUtil.LTrim( StringUtil.Str( AV53tPFL, 14, 5)));
               A944Contagem_PFL = NumberUtil.Val( GetNextPar( ), ".");
               n944Contagem_PFL = false;
               AV44Quantidade = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Quantidade), 4, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( AV9Sistema_Codigo, AV24Filtro_Coordenacao, AV60Contagem_Tipo, AV23Filtro_Ano, AV26Filtro_Mes, AV57WWPContext, AV55tQtdItm, A264Contagem_QtdItens, AV52tPFB, A943Contagem_PFB, AV53tPFL, A944Contagem_PFL, AV44Quantidade) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "WP_ConsultaContagens";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV44Quantidade), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV52tPFB, "ZZ,ZZZ,ZZ9.999");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV53tPFL, "ZZ,ZZZ,ZZ9.999");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("wp_consultacontagens:[SendSecurityCheck value for]"+"Quantidade:"+context.localUtil.Format( (decimal)(AV44Quantidade), "ZZZ9"));
               GXUtil.WriteLog("wp_consultacontagens:[SendSecurityCheck value for]"+"tPFB:"+context.localUtil.Format( AV52tPFB, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_consultacontagens:[SendSecurityCheck value for]"+"tPFL:"+context.localUtil.Format( AV53tPFL, "ZZ,ZZZ,ZZ9.999"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAGH2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTGH2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299391698");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_consultacontagens.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFILTRO_COORDENACAO", AV24Filtro_Coordenacao);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_TIPO", StringUtil.RTrim( AV60Contagem_Tipo));
         GxWebStd.gx_hidden_field( context, "GXH_vFILTRO_ANO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Filtro_Ano), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFILTRO_MES", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26Filtro_Mes), 10, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_21", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_21), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV57WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV57WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vTQTDITM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55tQtdItm), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_ConsultaContagens";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV44Quantidade), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV52tPFB, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV53tPFL, "ZZ,ZZZ,ZZ9.999");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_consultacontagens:[SendSecurityCheck value for]"+"Quantidade:"+context.localUtil.Format( (decimal)(AV44Quantidade), "ZZZ9"));
         GXUtil.WriteLog("wp_consultacontagens:[SendSecurityCheck value for]"+"tPFB:"+context.localUtil.Format( AV52tPFB, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_consultacontagens:[SendSecurityCheck value for]"+"tPFL:"+context.localUtil.Format( AV53tPFL, "ZZ,ZZZ,ZZ9.999"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEGH2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTGH2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_consultacontagens.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ConsultaContagens" ;
      }

      public override String GetPgmdesc( )
      {
         return "Consulta de Contagens" ;
      }

      protected void WBGH0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_GH2( true) ;
         }
         else
         {
            wb_table1_2_GH2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GH2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOWContainer"+"\"></div>") ;
            context.WriteHtmlText( "<font size=\"2\" face=\"Courier New\" color=\"#8b0000\">") ;
            context.WriteHtmlText( "<font size=\"2\" face=\"Courier New\" color=\"#8b0000\">") ;
            context.WriteHtmlText( "<font size=\"2\" face=\"Courier New\" color=\"#8b0000\"></font>") ;
            context.WriteHtmlText( "</font>") ;
            context.WriteHtmlText( "</font>") ;
         }
         wbLoad = true;
      }

      protected void STARTGH2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Consulta de Contagens", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPGH0( ) ;
      }

      protected void WSGH2( )
      {
         STARTGH2( ) ;
         EVTGH2( ) ;
      }

      protected void EVTGH2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11GH2 */
                              E11GH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_21_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_21_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_21_idx), 4, 0)), 4, "0");
                              SubsflControlProps_212( ) ;
                              AV59PlanoDeContagem = cgiGet( edtavPlanodecontagem_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPlanodecontagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV59PlanoDeContagem)) ? AV64Planodecontagem_GXI : context.convertURL( context.PathToRelativeUrl( AV59PlanoDeContagem))));
                              A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
                              A197Contagem_DataCriacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagem_DataCriacao_Internalname), 0));
                              cmbContagem_Tipo.Name = cmbContagem_Tipo_Internalname;
                              cmbContagem_Tipo.CurrentValue = cgiGet( cmbContagem_Tipo_Internalname);
                              A196Contagem_Tipo = cgiGet( cmbContagem_Tipo_Internalname);
                              n196Contagem_Tipo = false;
                              A941Contagem_SistemaSigla = StringUtil.Upper( cgiGet( edtContagem_SistemaSigla_Internalname));
                              n941Contagem_SistemaSigla = false;
                              A949Contagem_SistemaCoord = StringUtil.Upper( cgiGet( edtContagem_SistemaCoord_Internalname));
                              n949Contagem_SistemaCoord = false;
                              A943Contagem_PFB = context.localUtil.CToN( cgiGet( edtContagem_PFB_Internalname), ",", ".");
                              n943Contagem_PFB = false;
                              A944Contagem_PFL = context.localUtil.CToN( cgiGet( edtContagem_PFL_Internalname), ",", ".");
                              n944Contagem_PFL = false;
                              A264Contagem_QtdItens = (short)(context.localUtil.CToN( cgiGet( edtContagem_QtdItens_Internalname), ",", "."));
                              n264Contagem_QtdItens = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12GH2 */
                                    E12GH2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13GH2 */
                                    E13GH2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Sistema_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vSISTEMA_CODIGO"), ",", ".") != Convert.ToDecimal( AV9Sistema_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Filtro_coordenacao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFILTRO_COORDENACAO"), AV24Filtro_Coordenacao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagem_tipo Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_TIPO"), AV60Contagem_Tipo) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Filtro_ano Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vFILTRO_ANO"), ",", ".") != Convert.ToDecimal( AV23Filtro_Ano )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Filtro_mes Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vFILTRO_MES"), ",", ".") != Convert.ToDecimal( AV26Filtro_Mes )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGH2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAGH2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavSistema_codigo.Name = "vSISTEMA_CODIGO";
            dynavSistema_codigo.WebTags = "";
            cmbavContagem_tipo.Name = "vCONTAGEM_TIPO";
            cmbavContagem_tipo.WebTags = "";
            cmbavContagem_tipo.addItem("", "Todos", 0);
            cmbavContagem_tipo.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbavContagem_tipo.addItem("M", "Projeto de Melhor�a", 0);
            cmbavContagem_tipo.addItem("C", "Projeto de Contagem", 0);
            cmbavContagem_tipo.addItem("A", "Aplica��o", 0);
            if ( cmbavContagem_tipo.ItemCount > 0 )
            {
               AV60Contagem_Tipo = cmbavContagem_tipo.getValidValue(AV60Contagem_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Contagem_Tipo", AV60Contagem_Tipo);
            }
            cmbavFiltro_ano.Name = "vFILTRO_ANO";
            cmbavFiltro_ano.WebTags = "";
            cmbavFiltro_ano.addItem("2014", "2014", 0);
            cmbavFiltro_ano.addItem("2015", "2015", 0);
            if ( cmbavFiltro_ano.ItemCount > 0 )
            {
               AV23Filtro_Ano = (short)(NumberUtil.Val( cmbavFiltro_ano.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)));
            }
            cmbavFiltro_mes.Name = "vFILTRO_MES";
            cmbavFiltro_mes.WebTags = "";
            cmbavFiltro_mes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "Todos", 0);
            cmbavFiltro_mes.addItem("1", "Janeiro", 0);
            cmbavFiltro_mes.addItem("2", "Fevereiro", 0);
            cmbavFiltro_mes.addItem("3", "Mar�o", 0);
            cmbavFiltro_mes.addItem("4", "Abril", 0);
            cmbavFiltro_mes.addItem("5", "Maio", 0);
            cmbavFiltro_mes.addItem("6", "Junho", 0);
            cmbavFiltro_mes.addItem("7", "Julho", 0);
            cmbavFiltro_mes.addItem("8", "Agosto", 0);
            cmbavFiltro_mes.addItem("9", "Setembro", 0);
            cmbavFiltro_mes.addItem("10", "Outubro", 0);
            cmbavFiltro_mes.addItem("11", "Novembro", 0);
            cmbavFiltro_mes.addItem("12", "Dezembro", 0);
            if ( cmbavFiltro_mes.ItemCount > 0 )
            {
               AV26Filtro_Mes = (long)(NumberUtil.Val( cmbavFiltro_mes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)));
            }
            GXCCtl = "CONTAGEM_TIPO_" + sGXsfl_21_idx;
            cmbContagem_Tipo.Name = GXCCtl;
            cmbContagem_Tipo.WebTags = "";
            cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
            cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
            cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
            cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
            if ( cmbContagem_Tipo.ItemCount > 0 )
            {
               A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
               n196Contagem_Tipo = false;
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavSistema_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSISTEMA_CODIGOGH2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSISTEMA_CODIGO_dataGH2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSISTEMA_CODIGO_htmlGH2( )
      {
         int gxdynajaxvalue ;
         GXDLVvSISTEMA_CODIGO_dataGH2( ) ;
         gxdynajaxindex = 1;
         dynavSistema_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV9Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSISTEMA_CODIGO_dataGH2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Todos");
         /* Using cursor H00GH2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00GH2_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00GH2_A129Sistema_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_212( ) ;
         while ( nGXsfl_21_idx <= nRC_GXsfl_21 )
         {
            sendrow_212( ) ;
            nGXsfl_21_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_21_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_21_idx+1));
            sGXsfl_21_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_21_idx), 4, 0)), 4, "0");
            SubsflControlProps_212( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int AV9Sistema_Codigo ,
                                       String AV24Filtro_Coordenacao ,
                                       String AV60Contagem_Tipo ,
                                       short AV23Filtro_Ano ,
                                       long AV26Filtro_Mes ,
                                       wwpbaseobjects.SdtWWPContext AV57WWPContext ,
                                       int AV55tQtdItm ,
                                       short A264Contagem_QtdItens ,
                                       decimal AV52tPFB ,
                                       decimal A943Contagem_PFB ,
                                       decimal AV53tPFL ,
                                       decimal A944Contagem_PFL ,
                                       short AV44Quantidade )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFGH2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_DATACRIACAO", GetSecureSignedToken( "", A197Contagem_DataCriacao));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_DATACRIACAO", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A196Contagem_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_TIPO", StringUtil.RTrim( A196Contagem_Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_PFB", GetSecureSignedToken( "", context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_PFB", StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_PFL", GetSecureSignedToken( "", context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_PFL", StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV9Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
         }
         if ( cmbavContagem_tipo.ItemCount > 0 )
         {
            AV60Contagem_Tipo = cmbavContagem_tipo.getValidValue(AV60Contagem_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Contagem_Tipo", AV60Contagem_Tipo);
         }
         if ( cmbavFiltro_ano.ItemCount > 0 )
         {
            AV23Filtro_Ano = (short)(NumberUtil.Val( cmbavFiltro_ano.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)));
         }
         if ( cmbavFiltro_mes.ItemCount > 0 )
         {
            AV26Filtro_Mes = (long)(NumberUtil.Val( cmbavFiltro_mes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGH2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavQtditm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtditm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtditm_Enabled), 5, 0)));
         edtavTpfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfb_Enabled), 5, 0)));
         edtavTpfl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfl_Enabled), 5, 0)));
      }

      protected void RFGH2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 21;
         /* Execute user event: E11GH2 */
         E11GH2 ();
         nGXsfl_21_idx = 1;
         sGXsfl_21_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_21_idx), 4, 0)), 4, "0");
         SubsflControlProps_212( ) ;
         nGXsfl_21_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_212( ) ;
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV9Sistema_Codigo ,
                                                 AV24Filtro_Coordenacao ,
                                                 AV26Filtro_Mes ,
                                                 AV60Contagem_Tipo ,
                                                 A940Contagem_SistemaCod ,
                                                 A949Contagem_SistemaCoord ,
                                                 A197Contagem_DataCriacao ,
                                                 A196Contagem_Tipo ,
                                                 AV23Filtro_Ano ,
                                                 AV57WWPContext.gxTpr_Areatrabalho_codigo ,
                                                 A193Contagem_AreaTrabalhoCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00GH4 */
            pr_default.execute(1, new Object[] {AV57WWPContext.gxTpr_Areatrabalho_codigo, AV23Filtro_Ano, AV9Sistema_Codigo, AV24Filtro_Coordenacao, AV26Filtro_Mes, AV60Contagem_Tipo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A940Contagem_SistemaCod = H00GH4_A940Contagem_SistemaCod[0];
               n940Contagem_SistemaCod = H00GH4_n940Contagem_SistemaCod[0];
               A193Contagem_AreaTrabalhoCod = H00GH4_A193Contagem_AreaTrabalhoCod[0];
               A944Contagem_PFL = H00GH4_A944Contagem_PFL[0];
               n944Contagem_PFL = H00GH4_n944Contagem_PFL[0];
               A943Contagem_PFB = H00GH4_A943Contagem_PFB[0];
               n943Contagem_PFB = H00GH4_n943Contagem_PFB[0];
               A949Contagem_SistemaCoord = H00GH4_A949Contagem_SistemaCoord[0];
               n949Contagem_SistemaCoord = H00GH4_n949Contagem_SistemaCoord[0];
               A941Contagem_SistemaSigla = H00GH4_A941Contagem_SistemaSigla[0];
               n941Contagem_SistemaSigla = H00GH4_n941Contagem_SistemaSigla[0];
               A196Contagem_Tipo = H00GH4_A196Contagem_Tipo[0];
               n196Contagem_Tipo = H00GH4_n196Contagem_Tipo[0];
               A197Contagem_DataCriacao = H00GH4_A197Contagem_DataCriacao[0];
               A192Contagem_Codigo = H00GH4_A192Contagem_Codigo[0];
               A264Contagem_QtdItens = H00GH4_A264Contagem_QtdItens[0];
               n264Contagem_QtdItens = H00GH4_n264Contagem_QtdItens[0];
               A949Contagem_SistemaCoord = H00GH4_A949Contagem_SistemaCoord[0];
               n949Contagem_SistemaCoord = H00GH4_n949Contagem_SistemaCoord[0];
               A941Contagem_SistemaSigla = H00GH4_A941Contagem_SistemaSigla[0];
               n941Contagem_SistemaSigla = H00GH4_n941Contagem_SistemaSigla[0];
               A264Contagem_QtdItens = H00GH4_A264Contagem_QtdItens[0];
               n264Contagem_QtdItens = H00GH4_n264Contagem_QtdItens[0];
               /* Execute user event: E13GH2 */
               E13GH2 ();
               pr_default.readNext(1);
            }
            pr_default.close(1);
            wbEnd = 21;
            WBGH0( ) ;
         }
         nGXsfl_21_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPGH0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavQtditm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtditm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtditm_Enabled), 5, 0)));
         edtavTpfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfb_Enabled), 5, 0)));
         edtavTpfl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfl_Enabled), 5, 0)));
         GXVvSISTEMA_CODIGO_htmlGH2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12GH2 */
         E12GH2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavSistema_codigo.Name = dynavSistema_codigo_Internalname;
            dynavSistema_codigo.CurrentValue = cgiGet( dynavSistema_codigo_Internalname);
            AV9Sistema_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSistema_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
            AV24Filtro_Coordenacao = StringUtil.Upper( cgiGet( edtavFiltro_coordenacao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Filtro_Coordenacao", AV24Filtro_Coordenacao);
            cmbavContagem_tipo.Name = cmbavContagem_tipo_Internalname;
            cmbavContagem_tipo.CurrentValue = cgiGet( cmbavContagem_tipo_Internalname);
            AV60Contagem_Tipo = cgiGet( cmbavContagem_tipo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Contagem_Tipo", AV60Contagem_Tipo);
            cmbavFiltro_ano.Name = cmbavFiltro_ano_Internalname;
            cmbavFiltro_ano.CurrentValue = cgiGet( cmbavFiltro_ano_Internalname);
            AV23Filtro_Ano = (short)(NumberUtil.Val( cgiGet( cmbavFiltro_ano_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)));
            cmbavFiltro_mes.Name = cmbavFiltro_mes_Internalname;
            cmbavFiltro_mes.CurrentValue = cgiGet( cmbavFiltro_mes_Internalname);
            AV26Filtro_Mes = (long)(NumberUtil.Val( cgiGet( cmbavFiltro_mes_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQUANTIDADE");
               GX_FocusControl = edtavQuantidade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44Quantidade = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Quantidade), 4, 0)));
            }
            else
            {
               AV44Quantidade = (short)(context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Quantidade), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtditm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtditm_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDITM");
               GX_FocusControl = edtavQtditm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43QtdItm = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43QtdItm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43QtdItm), 8, 0)));
            }
            else
            {
               AV43QtdItm = (int)(context.localUtil.CToN( cgiGet( edtavQtditm_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43QtdItm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43QtdItm), 8, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTpfb_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTpfb_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTPFB");
               GX_FocusControl = edtavTpfb_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52tPFB = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52tPFB", StringUtil.LTrim( StringUtil.Str( AV52tPFB, 14, 5)));
            }
            else
            {
               AV52tPFB = context.localUtil.CToN( cgiGet( edtavTpfb_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52tPFB", StringUtil.LTrim( StringUtil.Str( AV52tPFB, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTpfl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTpfl_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTPFL");
               GX_FocusControl = edtavTpfl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53tPFL = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFL", StringUtil.LTrim( StringUtil.Str( AV53tPFL, 14, 5)));
            }
            else
            {
               AV53tPFL = context.localUtil.CToN( cgiGet( edtavTpfl_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFL", StringUtil.LTrim( StringUtil.Str( AV53tPFL, 14, 5)));
            }
            /* Read saved values. */
            nRC_GXsfl_21 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_21"), ",", "."));
            Innewwindow_Target = cgiGet( "INNEWWINDOW_Target");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_ConsultaContagens";
            AV44Quantidade = (short)(context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Quantidade), 4, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV44Quantidade), "ZZZ9");
            AV52tPFB = context.localUtil.CToN( cgiGet( edtavTpfb_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52tPFB", StringUtil.LTrim( StringUtil.Str( AV52tPFB, 14, 5)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV52tPFB, "ZZ,ZZZ,ZZ9.999");
            AV53tPFL = context.localUtil.CToN( cgiGet( edtavTpfl_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFL", StringUtil.LTrim( StringUtil.Str( AV53tPFL, 14, 5)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV53tPFL, "ZZ,ZZZ,ZZ9.999");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_consultacontagens:[SecurityCheckFailed value for]"+"Quantidade:"+context.localUtil.Format( (decimal)(AV44Quantidade), "ZZZ9"));
               GXUtil.WriteLog("wp_consultacontagens:[SecurityCheckFailed value for]"+"tPFB:"+context.localUtil.Format( AV52tPFB, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_consultacontagens:[SecurityCheckFailed value for]"+"tPFL:"+context.localUtil.Format( AV53tPFL, "ZZ,ZZZ,ZZ9.999"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12GH2 */
         E12GH2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12GH2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV57WWPContext) ;
         AV23Filtro_Ano = (short)(DateTimeUtil.Year( Gx_date));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)));
         AV26Filtro_Mes = DateTimeUtil.Month( Gx_date);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)));
         if ( DateTimeUtil.Year( Gx_date) > 2015 )
         {
            AV27i = 2016;
            while ( AV27i <= DateTimeUtil.Year( Gx_date) )
            {
               cmbavFiltro_ano.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV27i), 4, 0)), StringUtil.Str( (decimal)(AV27i), 4, 0), 0);
               AV27i = (short)(AV27i+1);
            }
         }
      }

      protected void E11GH2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV8Ano = AV23Filtro_Ano;
         AV55tQtdItm = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55tQtdItm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55tQtdItm), 8, 0)));
         AV52tPFB = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52tPFB", StringUtil.LTrim( StringUtil.Str( AV52tPFB, 14, 5)));
         AV53tPFL = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFL", StringUtil.LTrim( StringUtil.Str( AV53tPFL, 14, 5)));
         AV44Quantidade = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Quantidade), 4, 0)));
      }

      private void E13GH2( )
      {
         /* Load Routine */
         AV59PlanoDeContagem = context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPlanodecontagem_Internalname, AV59PlanoDeContagem);
         AV64Planodecontagem_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )));
         AV55tQtdItm = (int)(AV55tQtdItm+A264Contagem_QtdItens);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55tQtdItm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55tQtdItm), 8, 0)));
         AV52tPFB = (decimal)(AV52tPFB+A943Contagem_PFB);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52tPFB", StringUtil.LTrim( StringUtil.Str( AV52tPFB, 14, 5)));
         AV53tPFL = (decimal)(AV53tPFL+A944Contagem_PFL);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFL", StringUtil.LTrim( StringUtil.Str( AV53tPFL, 14, 5)));
         AV44Quantidade = (short)(AV44Quantidade+1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Quantidade), 4, 0)));
         sendrow_212( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_21_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(21, GridRow);
         }
      }

      protected void wb_table1_2_GH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:220px")+"\" class='GridHeaderCell'>") ;
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadotitle_Internalname, "Consulta de Contagens", "", "", lblContagemresultadotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ConsultaContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Sistema:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContagens.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'" + sGXsfl_21_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSistema_codigo, dynavSistema_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)), 1, dynavSistema_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,9);\"", "", true, "HLP_WP_ConsultaContagens.htm");
            dynavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_codigo_Internalname, "Values", (String)(dynavSistema_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "�rea gestora:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContagens.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltro_coordenacao_Internalname, AV24Filtro_Coordenacao, StringUtil.RTrim( context.localUtil.Format( AV24Filtro_Coordenacao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltro_coordenacao_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ConsultaContagens.htm");
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Tipo:", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContagens.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'" + sGXsfl_21_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagem_tipo, cmbavContagem_tipo_Internalname, StringUtil.RTrim( AV60Contagem_Tipo), 1, cmbavContagem_tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "", true, "HLP_WP_ConsultaContagens.htm");
            cmbavContagem_tipo.CurrentValue = StringUtil.RTrim( AV60Contagem_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagem_tipo_Internalname, "Values", (String)(cmbavContagem_tipo.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Ano:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContagens.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'" + sGXsfl_21_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltro_ano, cmbavFiltro_ano_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)), 1, cmbavFiltro_ano_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "", true, "HLP_WP_ConsultaContagens.htm");
            cmbavFiltro_ano.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltro_ano_Internalname, "Values", (String)(cmbavFiltro_ano.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Mes:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContagens.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'" + sGXsfl_21_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltro_mes, cmbavFiltro_mes_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)), 1, cmbavFiltro_mes_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "", true, "HLP_WP_ConsultaContagens.htm");
            cmbavFiltro_mes.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltro_mes_Internalname, "Values", (String)(cmbavFiltro_mes.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(21), 2, 0)+","+"null"+");", "Atualizar", bttButton1_Jsonclick, 5, "Atualizar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EREFRESH."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ConsultaContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"21\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Plano") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Data Cria��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "�rea gestora") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFB") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFL") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Itens") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV59PlanoDeContagem));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A196Contagem_Tipo));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A941Contagem_SistemaSigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A949Contagem_SistemaCoord);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A264Contagem_QtdItens), 3, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 21 )
         {
            wbEnd = 0;
            nRC_GXsfl_21 = (short)(nGXsfl_21_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table2_33_GH2( true) ;
         }
         else
         {
            wb_table2_33_GH2( false) ;
         }
         return  ;
      }

      protected void wb_table2_33_GH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GH2e( true) ;
         }
         else
         {
            wb_table1_2_GH2e( false) ;
         }
      }

      protected void wb_table2_33_GH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfstotal_Internalname, "Contagens: ", "", "", lblTextblockcontagemresultado_pflfstotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQuantidade_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44Quantidade), 4, 0, ",", "")), ((edtavQuantidade_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44Quantidade), "ZZZ9")) : context.localUtil.Format( (decimal)(AV44Quantidade), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQuantidade_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavQuantidade_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfmtotal_Internalname, " - Itens: ", "", "", lblTextblockcontagemresultado_pfbfmtotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtditm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43QtdItm), 8, 0, ",", "")), ((edtavQtditm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43QtdItm), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV43QtdItm), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtditm_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavQtditm_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfmtotal2_Internalname, " - PF B: ", "", "", lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpfb_Internalname, StringUtil.LTrim( StringUtil.NToC( AV52tPFB, 14, 5, ",", "")), ((edtavTpfb_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV52tPFB, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV52tPFB, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpfb_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTpfb_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_ConsultaContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal_Internalname, " - PF L: ", "", "", lblTextblockcontagemresultado_pflfmtotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_21_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpfl_Internalname, StringUtil.LTrim( StringUtil.NToC( AV53tPFL, 14, 5, ",", "")), ((edtavTpfl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV53tPFL, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV53tPFL, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpfl_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTpfl_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_ConsultaContagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_33_GH2e( true) ;
         }
         else
         {
            wb_table2_33_GH2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGH2( ) ;
         WSGH2( ) ;
         WEGH2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299391762");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_consultacontagens.js", "?20205299391762");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_212( )
      {
         edtavPlanodecontagem_Internalname = "vPLANODECONTAGEM_"+sGXsfl_21_idx;
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO_"+sGXsfl_21_idx;
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO_"+sGXsfl_21_idx;
         cmbContagem_Tipo_Internalname = "CONTAGEM_TIPO_"+sGXsfl_21_idx;
         edtContagem_SistemaSigla_Internalname = "CONTAGEM_SISTEMASIGLA_"+sGXsfl_21_idx;
         edtContagem_SistemaCoord_Internalname = "CONTAGEM_SISTEMACOORD_"+sGXsfl_21_idx;
         edtContagem_PFB_Internalname = "CONTAGEM_PFB_"+sGXsfl_21_idx;
         edtContagem_PFL_Internalname = "CONTAGEM_PFL_"+sGXsfl_21_idx;
         edtContagem_QtdItens_Internalname = "CONTAGEM_QTDITENS_"+sGXsfl_21_idx;
      }

      protected void SubsflControlProps_fel_212( )
      {
         edtavPlanodecontagem_Internalname = "vPLANODECONTAGEM_"+sGXsfl_21_fel_idx;
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO_"+sGXsfl_21_fel_idx;
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO_"+sGXsfl_21_fel_idx;
         cmbContagem_Tipo_Internalname = "CONTAGEM_TIPO_"+sGXsfl_21_fel_idx;
         edtContagem_SistemaSigla_Internalname = "CONTAGEM_SISTEMASIGLA_"+sGXsfl_21_fel_idx;
         edtContagem_SistemaCoord_Internalname = "CONTAGEM_SISTEMACOORD_"+sGXsfl_21_fel_idx;
         edtContagem_PFB_Internalname = "CONTAGEM_PFB_"+sGXsfl_21_fel_idx;
         edtContagem_PFL_Internalname = "CONTAGEM_PFL_"+sGXsfl_21_fel_idx;
         edtContagem_QtdItens_Internalname = "CONTAGEM_QTDITENS_"+sGXsfl_21_fel_idx;
      }

      protected void sendrow_212( )
      {
         SubsflControlProps_212( ) ;
         WBGH0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_21_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_21_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavPlanodecontagem_Enabled!=0)&&(edtavPlanodecontagem_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 22,'',false,'',21)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV59PlanoDeContagem_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV59PlanoDeContagem))&&String.IsNullOrEmpty(StringUtil.RTrim( AV64Planodecontagem_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV59PlanoDeContagem)));
         GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavPlanodecontagem_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV59PlanoDeContagem)) ? AV64Planodecontagem_GXI : context.PathToRelativeUrl( AV59PlanoDeContagem)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"Plano de Contagem",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavPlanodecontagem_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e14gh2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV59PlanoDeContagem_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_DataCriacao_Internalname,context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"),context.localUtil.Format( A197Contagem_DataCriacao, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_DataCriacao_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         if ( ( nGXsfl_21_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CONTAGEM_TIPO_" + sGXsfl_21_idx;
            cmbContagem_Tipo.Name = GXCCtl;
            cmbContagem_Tipo.WebTags = "";
            cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
            cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
            cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
            cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
            if ( cmbContagem_Tipo.ItemCount > 0 )
            {
               A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
               n196Contagem_Tipo = false;
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagem_Tipo,(String)cmbContagem_Tipo_Internalname,StringUtil.RTrim( A196Contagem_Tipo),(short)1,(String)cmbContagem_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
         cmbContagem_Tipo.CurrentValue = StringUtil.RTrim( A196Contagem_Tipo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tipo_Internalname, "Values", (String)(cmbContagem_Tipo.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_SistemaSigla_Internalname,StringUtil.RTrim( A941Contagem_SistemaSigla),StringUtil.RTrim( context.localUtil.Format( A941Contagem_SistemaSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_SistemaSigla_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_SistemaCoord_Internalname,(String)A949Contagem_SistemaCoord,StringUtil.RTrim( context.localUtil.Format( A949Contagem_SistemaCoord, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_SistemaCoord_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_PFB_Internalname,StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ",", "")),context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_PFB_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)13,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_PFL_Internalname,StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ",", "")),context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_PFL_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)13,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_QtdItens_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A264Contagem_QtdItens), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A264Contagem_QtdItens), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_QtdItens_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)21,(short)1,(short)-1,(short)0,(bool)true,(String)"Quantidade",(String)"right",(bool)false});
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_CODIGO"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sGXsfl_21_idx, context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_DATACRIACAO"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sGXsfl_21_idx, A197Contagem_DataCriacao));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_TIPO"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sGXsfl_21_idx, StringUtil.RTrim( context.localUtil.Format( A196Contagem_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_PFB"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sGXsfl_21_idx, context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_PFL"+"_"+sGXsfl_21_idx, GetSecureSignedToken( sGXsfl_21_idx, context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999")));
         GridContainer.AddRow(GridRow);
         nGXsfl_21_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_21_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_21_idx+1));
         sGXsfl_21_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_21_idx), 4, 0)), 4, "0");
         SubsflControlProps_212( ) ;
         /* End function sendrow_212 */
      }

      protected void init_default_properties( )
      {
         lblContagemresultadotitle_Internalname = "CONTAGEMRESULTADOTITLE";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         dynavSistema_codigo_Internalname = "vSISTEMA_CODIGO";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavFiltro_coordenacao_Internalname = "vFILTRO_COORDENACAO";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         cmbavContagem_tipo_Internalname = "vCONTAGEM_TIPO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         cmbavFiltro_ano_Internalname = "vFILTRO_ANO";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavFiltro_mes_Internalname = "vFILTRO_MES";
         bttButton1_Internalname = "BUTTON1";
         edtavPlanodecontagem_Internalname = "vPLANODECONTAGEM";
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO";
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO";
         cmbContagem_Tipo_Internalname = "CONTAGEM_TIPO";
         edtContagem_SistemaSigla_Internalname = "CONTAGEM_SISTEMASIGLA";
         edtContagem_SistemaCoord_Internalname = "CONTAGEM_SISTEMACOORD";
         edtContagem_PFB_Internalname = "CONTAGEM_PFB";
         edtContagem_PFL_Internalname = "CONTAGEM_PFL";
         edtContagem_QtdItens_Internalname = "CONTAGEM_QTDITENS";
         lblTextblockcontagemresultado_pflfstotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFSTOTAL";
         edtavQuantidade_Internalname = "vQUANTIDADE";
         lblTextblockcontagemresultado_pfbfmtotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFMTOTAL";
         edtavQtditm_Internalname = "vQTDITM";
         lblTextblockcontagemresultado_pfbfmtotal2_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFMTOTAL2";
         edtavTpfb_Internalname = "vTPFB";
         lblTextblockcontagemresultado_pflfmtotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL";
         edtavTpfl_Internalname = "vTPFL";
         tblTable3_Internalname = "TABLE3";
         tblTable4_Internalname = "TABLE4";
         Innewwindow_Internalname = "INNEWWINDOW";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagem_QtdItens_Jsonclick = "";
         edtContagem_PFL_Jsonclick = "";
         edtContagem_PFB_Jsonclick = "";
         edtContagem_SistemaCoord_Jsonclick = "";
         edtContagem_SistemaSigla_Jsonclick = "";
         cmbContagem_Tipo_Jsonclick = "";
         edtContagem_DataCriacao_Jsonclick = "";
         edtContagem_Codigo_Jsonclick = "";
         edtavPlanodecontagem_Jsonclick = "";
         edtavPlanodecontagem_Visible = -1;
         edtavPlanodecontagem_Enabled = 1;
         edtavTpfl_Jsonclick = "";
         edtavTpfl_Enabled = 1;
         edtavTpfb_Jsonclick = "";
         edtavTpfb_Enabled = 1;
         edtavQtditm_Jsonclick = "";
         edtavQtditm_Enabled = 1;
         edtavQuantidade_Jsonclick = "";
         edtavQuantidade_Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         subGrid_Class = "WorkWith";
         cmbavFiltro_mes_Jsonclick = "";
         cmbavFiltro_ano_Jsonclick = "";
         cmbavContagem_tipo_Jsonclick = "";
         edtavFiltro_coordenacao_Jsonclick = "";
         dynavSistema_codigo_Jsonclick = "";
         subGrid_Sortable = 0;
         subGrid_Titleforecolor = (int)(0x000000);
         subGrid_Backcolorstyle = 3;
         Innewwindow_Target = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Consulta de Contagens";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV9Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Filtro_Coordenacao',fld:'vFILTRO_COORDENACAO',pic:'@!',nv:''},{av:'AV60Contagem_Tipo',fld:'vCONTAGEM_TIPO',pic:'',nv:''},{av:'AV26Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV57WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV55tQtdItm',fld:'vTQTDITM',pic:'ZZZZZZZ9',nv:0},{av:'A264Contagem_QtdItens',fld:'CONTAGEM_QTDITENS',pic:'ZZ9',nv:0},{av:'AV52tPFB',fld:'vTPFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A943Contagem_PFB',fld:'CONTAGEM_PFB',pic:'ZZZZZZ9.99999',hsh:true,nv:0.0},{av:'AV53tPFL',fld:'vTPFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A944Contagem_PFL',fld:'CONTAGEM_PFL',pic:'ZZZZZZ9.99999',hsh:true,nv:0.0},{av:'AV44Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'AV23Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV55tQtdItm',fld:'vTQTDITM',pic:'ZZZZZZZ9',nv:0},{av:'AV52tPFB',fld:'vTPFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV53tPFL',fld:'vTPFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("'PLANODECONTAGEM'","{handler:'E14GH2',iparms:[{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV24Filtro_Coordenacao = "";
         AV60Contagem_Tipo = "";
         AV57WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV59PlanoDeContagem = "";
         AV64Planodecontagem_GXI = "";
         A197Contagem_DataCriacao = DateTime.MinValue;
         A196Contagem_Tipo = "";
         A941Contagem_SistemaSigla = "";
         A949Contagem_SistemaCoord = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00GH2_A127Sistema_Codigo = new int[1] ;
         H00GH2_A129Sistema_Sigla = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         Gx_date = DateTime.MinValue;
         H00GH4_A940Contagem_SistemaCod = new int[1] ;
         H00GH4_n940Contagem_SistemaCod = new bool[] {false} ;
         H00GH4_A193Contagem_AreaTrabalhoCod = new int[1] ;
         H00GH4_A944Contagem_PFL = new decimal[1] ;
         H00GH4_n944Contagem_PFL = new bool[] {false} ;
         H00GH4_A943Contagem_PFB = new decimal[1] ;
         H00GH4_n943Contagem_PFB = new bool[] {false} ;
         H00GH4_A949Contagem_SistemaCoord = new String[] {""} ;
         H00GH4_n949Contagem_SistemaCoord = new bool[] {false} ;
         H00GH4_A941Contagem_SistemaSigla = new String[] {""} ;
         H00GH4_n941Contagem_SistemaSigla = new bool[] {false} ;
         H00GH4_A196Contagem_Tipo = new String[] {""} ;
         H00GH4_n196Contagem_Tipo = new bool[] {false} ;
         H00GH4_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         H00GH4_A192Contagem_Codigo = new int[1] ;
         H00GH4_A264Contagem_QtdItens = new short[1] ;
         H00GH4_n264Contagem_QtdItens = new bool[] {false} ;
         hsh = "";
         GridRow = new GXWebRow();
         sStyleString = "";
         lblContagemresultadotitle_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         TempTags = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttButton1_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTextblockcontagemresultado_pflfstotal_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfmtotal_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_consultacontagens__default(),
            new Object[][] {
                new Object[] {
               H00GH2_A127Sistema_Codigo, H00GH2_A129Sistema_Sigla
               }
               , new Object[] {
               H00GH4_A940Contagem_SistemaCod, H00GH4_n940Contagem_SistemaCod, H00GH4_A193Contagem_AreaTrabalhoCod, H00GH4_A944Contagem_PFL, H00GH4_n944Contagem_PFL, H00GH4_A943Contagem_PFB, H00GH4_n943Contagem_PFB, H00GH4_A949Contagem_SistemaCoord, H00GH4_n949Contagem_SistemaCoord, H00GH4_A941Contagem_SistemaSigla,
               H00GH4_n941Contagem_SistemaSigla, H00GH4_A196Contagem_Tipo, H00GH4_n196Contagem_Tipo, H00GH4_A197Contagem_DataCriacao, H00GH4_A192Contagem_Codigo, H00GH4_A264Contagem_QtdItens, H00GH4_n264Contagem_QtdItens
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavQuantidade_Enabled = 0;
         edtavQtditm_Enabled = 0;
         edtavTpfb_Enabled = 0;
         edtavTpfl_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_21 ;
      private short nGXsfl_21_idx=1 ;
      private short AV23Filtro_Ano ;
      private short A264Contagem_QtdItens ;
      private short AV44Quantidade ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_21_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV27i ;
      private short AV8Ano ;
      private short GRID_nEOF ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV9Sistema_Codigo ;
      private int AV55tQtdItm ;
      private int A192Contagem_Codigo ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int edtavQuantidade_Enabled ;
      private int edtavQtditm_Enabled ;
      private int edtavTpfb_Enabled ;
      private int edtavTpfl_Enabled ;
      private int subGrid_Titleforecolor ;
      private int AV57WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A940Contagem_SistemaCod ;
      private int A193Contagem_AreaTrabalhoCod ;
      private int AV43QtdItm ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavPlanodecontagem_Enabled ;
      private int edtavPlanodecontagem_Visible ;
      private long AV26Filtro_Mes ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private decimal AV52tPFB ;
      private decimal A943Contagem_PFB ;
      private decimal AV53tPFL ;
      private decimal A944Contagem_PFL ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_21_idx="0001" ;
      private String AV60Contagem_Tipo ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Innewwindow_Target ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavPlanodecontagem_Internalname ;
      private String edtContagem_Codigo_Internalname ;
      private String edtContagem_DataCriacao_Internalname ;
      private String cmbContagem_Tipo_Internalname ;
      private String A196Contagem_Tipo ;
      private String A941Contagem_SistemaSigla ;
      private String edtContagem_SistemaSigla_Internalname ;
      private String edtContagem_SistemaCoord_Internalname ;
      private String edtContagem_PFB_Internalname ;
      private String edtContagem_PFL_Internalname ;
      private String edtContagem_QtdItens_Internalname ;
      private String GXCCtl ;
      private String dynavSistema_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavQuantidade_Internalname ;
      private String edtavQtditm_Internalname ;
      private String edtavTpfb_Internalname ;
      private String edtavTpfl_Internalname ;
      private String edtavFiltro_coordenacao_Internalname ;
      private String cmbavContagem_tipo_Internalname ;
      private String cmbavFiltro_ano_Internalname ;
      private String cmbavFiltro_mes_Internalname ;
      private String hsh ;
      private String sStyleString ;
      private String tblTable4_Internalname ;
      private String lblContagemresultadotitle_Internalname ;
      private String lblContagemresultadotitle_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String TempTags ;
      private String dynavSistema_codigo_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavFiltro_coordenacao_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String cmbavContagem_tipo_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String cmbavFiltro_ano_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String cmbavFiltro_mes_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTable3_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Jsonclick ;
      private String edtavQuantidade_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfmtotal_Internalname ;
      private String lblTextblockcontagemresultado_pfbfmtotal_Jsonclick ;
      private String edtavQtditm_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfmtotal2_Internalname ;
      private String lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick ;
      private String edtavTpfb_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal_Jsonclick ;
      private String edtavTpfl_Jsonclick ;
      private String sGXsfl_21_fel_idx="0001" ;
      private String edtavPlanodecontagem_Jsonclick ;
      private String ROClassString ;
      private String edtContagem_Codigo_Jsonclick ;
      private String edtContagem_DataCriacao_Jsonclick ;
      private String cmbContagem_Tipo_Jsonclick ;
      private String edtContagem_SistemaSigla_Jsonclick ;
      private String edtContagem_SistemaCoord_Jsonclick ;
      private String edtContagem_PFB_Jsonclick ;
      private String edtContagem_PFL_Jsonclick ;
      private String edtContagem_QtdItens_Jsonclick ;
      private String Innewwindow_Internalname ;
      private DateTime A197Contagem_DataCriacao ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool n264Contagem_QtdItens ;
      private bool n943Contagem_PFB ;
      private bool n944Contagem_PFL ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n196Contagem_Tipo ;
      private bool n941Contagem_SistemaSigla ;
      private bool n949Contagem_SistemaCoord ;
      private bool n940Contagem_SistemaCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV59PlanoDeContagem_IsBlob ;
      private String AV24Filtro_Coordenacao ;
      private String AV64Planodecontagem_GXI ;
      private String A949Contagem_SistemaCoord ;
      private String AV59PlanoDeContagem ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavSistema_codigo ;
      private GXCombobox cmbavContagem_tipo ;
      private GXCombobox cmbavFiltro_ano ;
      private GXCombobox cmbavFiltro_mes ;
      private GXCombobox cmbContagem_Tipo ;
      private IDataStoreProvider pr_default ;
      private int[] H00GH2_A127Sistema_Codigo ;
      private String[] H00GH2_A129Sistema_Sigla ;
      private int[] H00GH4_A940Contagem_SistemaCod ;
      private bool[] H00GH4_n940Contagem_SistemaCod ;
      private int[] H00GH4_A193Contagem_AreaTrabalhoCod ;
      private decimal[] H00GH4_A944Contagem_PFL ;
      private bool[] H00GH4_n944Contagem_PFL ;
      private decimal[] H00GH4_A943Contagem_PFB ;
      private bool[] H00GH4_n943Contagem_PFB ;
      private String[] H00GH4_A949Contagem_SistemaCoord ;
      private bool[] H00GH4_n949Contagem_SistemaCoord ;
      private String[] H00GH4_A941Contagem_SistemaSigla ;
      private bool[] H00GH4_n941Contagem_SistemaSigla ;
      private String[] H00GH4_A196Contagem_Tipo ;
      private bool[] H00GH4_n196Contagem_Tipo ;
      private DateTime[] H00GH4_A197Contagem_DataCriacao ;
      private int[] H00GH4_A192Contagem_Codigo ;
      private short[] H00GH4_A264Contagem_QtdItens ;
      private bool[] H00GH4_n264Contagem_QtdItens ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV57WWPContext ;
   }

   public class wp_consultacontagens__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00GH4( IGxContext context ,
                                             int AV9Sistema_Codigo ,
                                             String AV24Filtro_Coordenacao ,
                                             long AV26Filtro_Mes ,
                                             String AV60Contagem_Tipo ,
                                             int A940Contagem_SistemaCod ,
                                             String A949Contagem_SistemaCoord ,
                                             DateTime A197Contagem_DataCriacao ,
                                             String A196Contagem_Tipo ,
                                             short AV23Filtro_Ano ,
                                             int AV57WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int A193Contagem_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contagem_SistemaCod] AS Contagem_SistemaCod, T1.[Contagem_AreaTrabalhoCod], T1.[Contagem_PFL], T1.[Contagem_PFB], T2.[Sistema_Coordenacao] AS Contagem_SistemaCoord, T2.[Sistema_Sigla] AS Contagem_SistemaSigla, T1.[Contagem_Tipo], T1.[Contagem_DataCriacao], T1.[Contagem_Codigo], COALESCE( T3.[Contagem_QtdItens], 0) AS Contagem_QtdItens FROM (([Contagem] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Contagem_SistemaCod]) LEFT JOIN (SELECT COUNT(*) AS Contagem_QtdItens, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) GROUP BY [Contagem_Codigo] ) T3 ON T3.[Contagem_Codigo] = T1.[Contagem_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contagem_AreaTrabalhoCod] = @AV57WWPC_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (YEAR(T1.[Contagem_DataCriacao]) = @AV23Filtro_Ano)";
         if ( ! (0==AV9Sistema_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] = @AV9Sistema_Codigo)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Filtro_Coordenacao)) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Coordenacao] = @AV24Filtro_Coordenacao)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV26Filtro_Mes) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[Contagem_DataCriacao]) = @AV26Filtro_Mes)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Contagem_Tipo)) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_Tipo] = @AV60Contagem_Tipo)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contagem_AreaTrabalhoCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00GH4(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (long)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GH2 ;
          prmH00GH2 = new Object[] {
          } ;
          Object[] prmH00GH4 ;
          prmH00GH4 = new Object[] {
          new Object[] {"@AV57WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV9Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24Filtro_Coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV26Filtro_Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV60Contagem_Tipo",SqlDbType.Char,1,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GH2", "SELECT [Sistema_Codigo], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GH2,0,0,true,false )
             ,new CursorDef("H00GH4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GH4,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 25) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((short[]) buf[15])[0] = rslt.getShort(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

}
