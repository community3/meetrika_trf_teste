/*
               File: WWGeral_UnidadeOrganizacional
        Description:  Unidade Organizacional
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:5:56.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwgeral_unidadeorganizacional : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwgeral_unidadeorganizacional( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwgeral_unidadeorganizacional( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         dynavUnidadeorganizacional_vinculada1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         dynavUnidadeorganizacional_vinculada2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         dynavUnidadeorganizacional_vinculada3 = new GXCombobox();
         chkUnidadeOrganizacional_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vUNIDADEORGANIZACIONAL_VINCULADA1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvUNIDADEORGANIZACIONAL_VINCULADA1D02( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vUNIDADEORGANIZACIONAL_VINCULADA2") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvUNIDADEORGANIZACIONAL_VINCULADA2D02( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vUNIDADEORGANIZACIONAL_VINCULADA3") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvUNIDADEORGANIZACIONAL_VINCULADA3D02( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_81 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_81_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_81_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16UnidadeOrganizacional_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16UnidadeOrganizacional_Nome1", AV16UnidadeOrganizacional_Nome1);
               AV37UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37UnidadeOrganizacional_Vinculada1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0)));
               AV18DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               AV19UnidadeOrganizacional_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19UnidadeOrganizacional_Nome2", AV19UnidadeOrganizacional_Nome2);
               AV38UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38UnidadeOrganizacional_Vinculada2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0)));
               AV21DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
               AV22UnidadeOrganizacional_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22UnidadeOrganizacional_Nome3", AV22UnidadeOrganizacional_Nome3);
               AV39UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39UnidadeOrganizacional_Vinculada3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0)));
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV20DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
               AV44TFUnidadeOrganizacional_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFUnidadeOrganizacional_Nome", AV44TFUnidadeOrganizacional_Nome);
               AV45TFUnidadeOrganizacional_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFUnidadeOrganizacional_Nome_Sel", AV45TFUnidadeOrganizacional_Nome_Sel);
               AV48TFTpUo_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFTpUo_Nome", AV48TFTpUo_Nome);
               AV49TFTpUo_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFTpUo_Nome_Sel", AV49TFTpUo_Nome_Sel);
               AV56TFEstado_UF = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFEstado_UF", AV56TFEstado_UF);
               AV57TFEstado_UF_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFEstado_UF_Sel", AV57TFEstado_UF_Sel);
               AV60TFUnidadeOrganizacional_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUnidadeOrganizacional_Ativo_Sel", StringUtil.Str( (decimal)(AV60TFUnidadeOrganizacional_Ativo_Sel), 1, 0));
               AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace", AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace);
               AV50ddo_TpUo_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_TpUo_NomeTitleControlIdToReplace", AV50ddo_TpUo_NomeTitleControlIdToReplace);
               AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace", AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace);
               AV58ddo_Estado_UFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Estado_UFTitleControlIdToReplace", AV58ddo_Estado_UFTitleControlIdToReplace);
               AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace", AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace);
               AV52TFUnidadeOrganizacinal_Arvore = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFUnidadeOrganizacinal_Arvore", AV52TFUnidadeOrganizacinal_Arvore);
               AV53TFUnidadeOrganizacinal_Arvore_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFUnidadeOrganizacinal_Arvore_Sel", AV53TFUnidadeOrganizacinal_Arvore_Sel);
               AV91Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               A611UnidadeOrganizacional_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
               A609TpUo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A609TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeOrganizacional_Nome1, AV37UnidadeOrganizacional_Vinculada1, AV18DynamicFiltersSelector2, AV19UnidadeOrganizacional_Nome2, AV38UnidadeOrganizacional_Vinculada2, AV21DynamicFiltersSelector3, AV22UnidadeOrganizacional_Nome3, AV39UnidadeOrganizacional_Vinculada3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV44TFUnidadeOrganizacional_Nome, AV45TFUnidadeOrganizacional_Nome_Sel, AV48TFTpUo_Nome, AV49TFTpUo_Nome_Sel, AV56TFEstado_UF, AV57TFEstado_UF_Sel, AV60TFUnidadeOrganizacional_Ativo_Sel, AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV50ddo_TpUo_NomeTitleControlIdToReplace, AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace, AV58ddo_Estado_UFTitleControlIdToReplace, AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace, AV52TFUnidadeOrganizacinal_Arvore, AV53TFUnidadeOrganizacinal_Arvore_Sel, AV91Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAD02( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTD02( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205181355681");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwgeral_unidadeorganizacional.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vUNIDADEORGANIZACIONAL_NOME1", StringUtil.RTrim( AV16UnidadeOrganizacional_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vUNIDADEORGANIZACIONAL_VINCULADA1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vUNIDADEORGANIZACIONAL_NOME2", StringUtil.RTrim( AV19UnidadeOrganizacional_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vUNIDADEORGANIZACIONAL_VINCULADA2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV21DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vUNIDADEORGANIZACIONAL_NOME3", StringUtil.RTrim( AV22UnidadeOrganizacional_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vUNIDADEORGANIZACIONAL_VINCULADA3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV20DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUNIDADEORGANIZACIONAL_NOME", StringUtil.RTrim( AV44TFUnidadeOrganizacional_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUNIDADEORGANIZACIONAL_NOME_SEL", StringUtil.RTrim( AV45TFUnidadeOrganizacional_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTPUO_NOME", StringUtil.RTrim( AV48TFTpUo_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTPUO_NOME_SEL", StringUtil.RTrim( AV49TFTpUo_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFESTADO_UF", StringUtil.RTrim( AV56TFEstado_UF));
         GxWebStd.gx_hidden_field( context, "GXH_vTFESTADO_UF_SEL", StringUtil.RTrim( AV57TFEstado_UF_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUNIDADEORGANIZACIONAL_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60TFUnidadeOrganizacional_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_81", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_81), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV62DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV62DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA", AV43UnidadeOrganizacional_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA", AV43UnidadeOrganizacional_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTPUO_NOMETITLEFILTERDATA", AV47TpUo_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTPUO_NOMETITLEFILTERDATA", AV47TpUo_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUNIDADEORGANIZACINAL_ARVORETITLEFILTERDATA", AV51UnidadeOrganizacinal_ArvoreTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUNIDADEORGANIZACINAL_ARVORETITLEFILTERDATA", AV51UnidadeOrganizacinal_ArvoreTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vESTADO_UFTITLEFILTERDATA", AV55Estado_UFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vESTADO_UFTITLEFILTERDATA", AV55Estado_UFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUNIDADEORGANIZACIONAL_ATIVOTITLEFILTERDATA", AV59UnidadeOrganizacional_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUNIDADEORGANIZACIONAL_ATIVOTITLEFILTERDATA", AV59UnidadeOrganizacional_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV91Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "UNIDADEORGANIZACIONAL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "TPUO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A609TpUo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "UNIDADEORGANIZACIONAL_VINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Caption", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Tooltip", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Cls", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Sortedstatus", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Includefilter", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Filtertype", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Datalisttype", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Datalistproc", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Sortasc", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Sortdsc", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Loadingdata", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Cleanfilter", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Noresultsfound", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Caption", StringUtil.RTrim( Ddo_tpuo_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Tooltip", StringUtil.RTrim( Ddo_tpuo_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Cls", StringUtil.RTrim( Ddo_tpuo_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_tpuo_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_tpuo_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_tpuo_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tpuo_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_tpuo_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_tpuo_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_tpuo_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_tpuo_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Filtertype", StringUtil.RTrim( Ddo_tpuo_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_tpuo_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_tpuo_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Datalisttype", StringUtil.RTrim( Ddo_tpuo_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Datalistproc", StringUtil.RTrim( Ddo_tpuo_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tpuo_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Sortasc", StringUtil.RTrim( Ddo_tpuo_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Sortdsc", StringUtil.RTrim( Ddo_tpuo_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Loadingdata", StringUtil.RTrim( Ddo_tpuo_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_tpuo_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_tpuo_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_tpuo_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Caption", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Tooltip", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Cls", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Filteredtext_set", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Selectedvalue_set", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Dropdownoptionstype", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Includesortasc", StringUtil.BoolToStr( Ddo_unidadeorganizacinal_arvore_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Includesortdsc", StringUtil.BoolToStr( Ddo_unidadeorganizacinal_arvore_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Includefilter", StringUtil.BoolToStr( Ddo_unidadeorganizacinal_arvore_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Filtertype", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Filterisrange", StringUtil.BoolToStr( Ddo_unidadeorganizacinal_arvore_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Includedatalist", StringUtil.BoolToStr( Ddo_unidadeorganizacinal_arvore_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Datalisttype", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Datalistproc", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_unidadeorganizacinal_arvore_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Loadingdata", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Cleanfilter", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Noresultsfound", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Searchbuttontext", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Caption", StringUtil.RTrim( Ddo_estado_uf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Tooltip", StringUtil.RTrim( Ddo_estado_uf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Cls", StringUtil.RTrim( Ddo_estado_uf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filteredtext_set", StringUtil.RTrim( Ddo_estado_uf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Selectedvalue_set", StringUtil.RTrim( Ddo_estado_uf_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Dropdownoptionstype", StringUtil.RTrim( Ddo_estado_uf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_estado_uf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includesortasc", StringUtil.BoolToStr( Ddo_estado_uf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includesortdsc", StringUtil.BoolToStr( Ddo_estado_uf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Sortedstatus", StringUtil.RTrim( Ddo_estado_uf_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includefilter", StringUtil.BoolToStr( Ddo_estado_uf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filtertype", StringUtil.RTrim( Ddo_estado_uf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filterisrange", StringUtil.BoolToStr( Ddo_estado_uf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includedatalist", StringUtil.BoolToStr( Ddo_estado_uf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Datalisttype", StringUtil.RTrim( Ddo_estado_uf_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Datalistproc", StringUtil.RTrim( Ddo_estado_uf_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_estado_uf_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Sortasc", StringUtil.RTrim( Ddo_estado_uf_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Sortdsc", StringUtil.RTrim( Ddo_estado_uf_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Loadingdata", StringUtil.RTrim( Ddo_estado_uf_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Cleanfilter", StringUtil.RTrim( Ddo_estado_uf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Noresultsfound", StringUtil.RTrim( Ddo_estado_uf_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Searchbuttontext", StringUtil.RTrim( Ddo_estado_uf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Caption", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Tooltip", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Cls", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_unidadeorganizacional_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_unidadeorganizacional_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_unidadeorganizacional_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_unidadeorganizacional_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Sortasc", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Activeeventkey", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_tpuo_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_tpuo_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TPUO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_tpuo_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Activeeventkey", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Filteredtext_get", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACINAL_ARVORE_Selectedvalue_get", StringUtil.RTrim( Ddo_unidadeorganizacinal_arvore_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Activeeventkey", StringUtil.RTrim( Ddo_estado_uf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filteredtext_get", StringUtil.RTrim( Ddo_estado_uf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Selectedvalue_get", StringUtil.RTrim( Ddo_estado_uf_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_UNIDADEORGANIZACIONAL_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_unidadeorganizacional_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WED02( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTD02( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwgeral_unidadeorganizacional.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWGeral_UnidadeOrganizacional" ;
      }

      public override String GetPgmdesc( )
      {
         return " Unidade Organizacional" ;
      }

      protected void WBD00( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_D02( true) ;
         }
         else
         {
            wb_table1_2_D02( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_D02e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_81_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(94, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_81_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(95, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidadeorganizacional_nome_Internalname, StringUtil.RTrim( AV44TFUnidadeOrganizacional_Nome), StringUtil.RTrim( context.localUtil.Format( AV44TFUnidadeOrganizacional_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidadeorganizacional_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidadeorganizacional_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidadeorganizacional_nome_sel_Internalname, StringUtil.RTrim( AV45TFUnidadeOrganizacional_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV45TFUnidadeOrganizacional_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidadeorganizacional_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidadeorganizacional_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftpuo_nome_Internalname, StringUtil.RTrim( AV48TFTpUo_Nome), StringUtil.RTrim( context.localUtil.Format( AV48TFTpUo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftpuo_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTftpuo_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftpuo_nome_sel_Internalname, StringUtil.RTrim( AV49TFTpUo_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV49TFTpUo_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftpuo_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftpuo_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_81_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfunidadeorganizacinal_arvore_Internalname, StringUtil.RTrim( AV52TFUnidadeOrganizacinal_Arvore), "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", 0, edtavTfunidadeorganizacinal_arvore_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "1000", -1, "", "", -1, true, "", "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_81_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfunidadeorganizacinal_arvore_sel_Internalname, StringUtil.RTrim( AV53TFUnidadeOrganizacinal_Arvore_Sel), "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavTfunidadeorganizacinal_arvore_sel_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "1000", -1, "", "", -1, true, "", "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_uf_Internalname, StringUtil.RTrim( AV56TFEstado_UF), StringUtil.RTrim( context.localUtil.Format( AV56TFEstado_UF, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_uf_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_uf_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_uf_sel_Internalname, StringUtil.RTrim( AV57TFEstado_UF_Sel), StringUtil.RTrim( context.localUtil.Format( AV57TFEstado_UF_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_uf_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_uf_sel_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidadeorganizacional_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60TFUnidadeOrganizacional_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV60TFUnidadeOrganizacional_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidadeorganizacional_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidadeorganizacional_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_UNIDADEORGANIZACIONAL_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_81_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname, AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TPUO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_81_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tpuo_nometitlecontrolidtoreplace_Internalname, AV50ddo_TpUo_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_tpuo_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_UNIDADEORGANIZACINAL_ARVOREContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_81_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_unidadeorganizacinal_arvoretitlecontrolidtoreplace_Internalname, AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavDdo_unidadeorganizacinal_arvoretitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ESTADO_UFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_81_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_estado_uftitlecontrolidtoreplace_Internalname, AV58ddo_Estado_UFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_estado_uftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_UNIDADEORGANIZACIONAL_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_81_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_unidadeorganizacional_ativotitlecontrolidtoreplace_Internalname, AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavDdo_unidadeorganizacional_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_UnidadeOrganizacional.htm");
         }
         wbLoad = true;
      }

      protected void STARTD02( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Unidade Organizacional", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPD00( ) ;
      }

      protected void WSD02( )
      {
         STARTD02( ) ;
         EVTD02( ) ;
      }

      protected void EVTD02( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11D02 */
                              E11D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_UNIDADEORGANIZACIONAL_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12D02 */
                              E12D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TPUO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13D02 */
                              E13D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_UNIDADEORGANIZACINAL_ARVORE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14D02 */
                              E14D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ESTADO_UF.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15D02 */
                              E15D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_UNIDADEORGANIZACIONAL_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16D02 */
                              E16D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17D02 */
                              E17D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18D02 */
                              E18D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19D02 */
                              E19D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20D02 */
                              E20D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21D02 */
                              E21D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22D02 */
                              E22D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23D02 */
                              E23D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24D02 */
                              E24D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25D02 */
                              E25D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26D02 */
                              E26D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27D02 */
                              E27D02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_81_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_81_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_81_idx), 4, 0)), 4, "0");
                              SubsflControlProps_812( ) ;
                              AV25Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV88Update_GXI : context.convertURL( context.PathToRelativeUrl( AV25Update))));
                              AV26Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV89Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV26Delete))));
                              AV28Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Display)) ? AV90Display_GXI : context.convertURL( context.PathToRelativeUrl( AV28Display))));
                              A612UnidadeOrganizacional_Nome = StringUtil.Upper( cgiGet( edtUnidadeOrganizacional_Nome_Internalname));
                              A610TpUo_Nome = StringUtil.Upper( cgiGet( edtTpUo_Nome_Internalname));
                              A1174UnidadeOrganizacinal_Arvore = cgiGet( edtUnidadeOrganizacinal_Arvore_Internalname);
                              A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
                              A629UnidadeOrganizacional_Ativo = StringUtil.StrToBool( cgiGet( chkUnidadeOrganizacional_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28D02 */
                                    E28D02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29D02 */
                                    E29D02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30D02 */
                                    E30D02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Unidadeorganizacional_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEORGANIZACIONAL_NOME1"), AV16UnidadeOrganizacional_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Unidadeorganizacional_vinculada1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vUNIDADEORGANIZACIONAL_VINCULADA1"), ",", ".") != Convert.ToDecimal( AV37UnidadeOrganizacional_Vinculada1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Unidadeorganizacional_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEORGANIZACIONAL_NOME2"), AV19UnidadeOrganizacional_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Unidadeorganizacional_vinculada2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vUNIDADEORGANIZACIONAL_VINCULADA2"), ",", ".") != Convert.ToDecimal( AV38UnidadeOrganizacional_Vinculada2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Unidadeorganizacional_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEORGANIZACIONAL_NOME3"), AV22UnidadeOrganizacional_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Unidadeorganizacional_vinculada3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vUNIDADEORGANIZACIONAL_VINCULADA3"), ",", ".") != Convert.ToDecimal( AV39UnidadeOrganizacional_Vinculada3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfunidadeorganizacional_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEORGANIZACIONAL_NOME"), AV44TFUnidadeOrganizacional_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfunidadeorganizacional_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEORGANIZACIONAL_NOME_SEL"), AV45TFUnidadeOrganizacional_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftpuo_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTPUO_NOME"), AV48TFTpUo_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftpuo_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTPUO_NOME_SEL"), AV49TFTpUo_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfestado_uf Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF"), AV56TFEstado_UF) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfestado_uf_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF_SEL"), AV57TFEstado_UF_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfunidadeorganizacional_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUNIDADEORGANIZACIONAL_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV60TFUnidadeOrganizacional_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WED02( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAD02( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("UNIDADEORGANIZACIONAL_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("UNIDADEORGANIZACIONAL_VINCULADA", "Depend�ncia", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            dynavUnidadeorganizacional_vinculada1.Name = "vUNIDADEORGANIZACIONAL_VINCULADA1";
            dynavUnidadeorganizacional_vinculada1.WebTags = "";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("UNIDADEORGANIZACIONAL_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("UNIDADEORGANIZACIONAL_VINCULADA", "Depend�ncia", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            dynavUnidadeorganizacional_vinculada2.Name = "vUNIDADEORGANIZACIONAL_VINCULADA2";
            dynavUnidadeorganizacional_vinculada2.WebTags = "";
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("UNIDADEORGANIZACIONAL_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("UNIDADEORGANIZACIONAL_VINCULADA", "Depend�ncia", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            }
            dynavUnidadeorganizacional_vinculada3.Name = "vUNIDADEORGANIZACIONAL_VINCULADA3";
            dynavUnidadeorganizacional_vinculada3.WebTags = "";
            GXCCtl = "UNIDADEORGANIZACIONAL_ATIVO_" + sGXsfl_81_idx;
            chkUnidadeOrganizacional_Ativo.Name = GXCCtl;
            chkUnidadeOrganizacional_Ativo.WebTags = "";
            chkUnidadeOrganizacional_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUnidadeOrganizacional_Ativo_Internalname, "TitleCaption", chkUnidadeOrganizacional_Ativo.Caption);
            chkUnidadeOrganizacional_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA1D02( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA1_dataD02( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvUNIDADEORGANIZACIONAL_VINCULADA1_htmlD02( )
      {
         int gxdynajaxvalue ;
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA1_dataD02( ) ;
         gxdynajaxindex = 1;
         dynavUnidadeorganizacional_vinculada1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavUnidadeorganizacional_vinculada1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavUnidadeorganizacional_vinculada1.ItemCount > 0 )
         {
            AV37UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37UnidadeOrganizacional_Vinculada1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0)));
         }
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA1_dataD02( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D02 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00D02_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D02_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA2D02( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA2_dataD02( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvUNIDADEORGANIZACIONAL_VINCULADA2_htmlD02( )
      {
         int gxdynajaxvalue ;
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA2_dataD02( ) ;
         gxdynajaxindex = 1;
         dynavUnidadeorganizacional_vinculada2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavUnidadeorganizacional_vinculada2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavUnidadeorganizacional_vinculada2.ItemCount > 0 )
         {
            AV38UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38UnidadeOrganizacional_Vinculada2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0)));
         }
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA2_dataD02( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D03 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00D03_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D03_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA3D02( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA3_dataD02( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvUNIDADEORGANIZACIONAL_VINCULADA3_htmlD02( )
      {
         int gxdynajaxvalue ;
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA3_dataD02( ) ;
         gxdynajaxindex = 1;
         dynavUnidadeorganizacional_vinculada3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavUnidadeorganizacional_vinculada3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavUnidadeorganizacional_vinculada3.ItemCount > 0 )
         {
            AV39UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39UnidadeOrganizacional_Vinculada3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0)));
         }
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA3_dataD02( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D04 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00D04_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D04_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_812( ) ;
         while ( nGXsfl_81_idx <= nRC_GXsfl_81 )
         {
            sendrow_812( ) ;
            nGXsfl_81_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_81_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_81_idx+1));
            sGXsfl_81_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_81_idx), 4, 0)), 4, "0");
            SubsflControlProps_812( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV16UnidadeOrganizacional_Nome1 ,
                                       int AV37UnidadeOrganizacional_Vinculada1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       String AV19UnidadeOrganizacional_Nome2 ,
                                       int AV38UnidadeOrganizacional_Vinculada2 ,
                                       String AV21DynamicFiltersSelector3 ,
                                       String AV22UnidadeOrganizacional_Nome3 ,
                                       int AV39UnidadeOrganizacional_Vinculada3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV20DynamicFiltersEnabled3 ,
                                       String AV44TFUnidadeOrganizacional_Nome ,
                                       String AV45TFUnidadeOrganizacional_Nome_Sel ,
                                       String AV48TFTpUo_Nome ,
                                       String AV49TFTpUo_Nome_Sel ,
                                       String AV56TFEstado_UF ,
                                       String AV57TFEstado_UF_Sel ,
                                       short AV60TFUnidadeOrganizacional_Ativo_Sel ,
                                       String AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace ,
                                       String AV50ddo_TpUo_NomeTitleControlIdToReplace ,
                                       String AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace ,
                                       String AV58ddo_Estado_UFTitleControlIdToReplace ,
                                       String AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace ,
                                       String AV52TFUnidadeOrganizacinal_Arvore ,
                                       String AV53TFUnidadeOrganizacinal_Arvore_Sel ,
                                       String AV91Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving ,
                                       int A611UnidadeOrganizacional_Codigo ,
                                       int A609TpUo_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFD02( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEORGANIZACIONAL_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "UNIDADEORGANIZACIONAL_NOME", StringUtil.RTrim( A612UnidadeOrganizacional_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEORGANIZACINAL_ARVORE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1174UnidadeOrganizacinal_Arvore, ""))));
         GxWebStd.gx_hidden_field( context, "UNIDADEORGANIZACINAL_ARVORE", StringUtil.RTrim( A1174UnidadeOrganizacinal_Arvore));
         GxWebStd.gx_hidden_field( context, "gxhash_ESTADO_UF", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!"))));
         GxWebStd.gx_hidden_field( context, "ESTADO_UF", StringUtil.RTrim( A23Estado_UF));
         GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEORGANIZACIONAL_ATIVO", GetSecureSignedToken( "", A629UnidadeOrganizacional_Ativo));
         GxWebStd.gx_hidden_field( context, "UNIDADEORGANIZACIONAL_ATIVO", StringUtil.BoolToStr( A629UnidadeOrganizacional_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( dynavUnidadeorganizacional_vinculada1.ItemCount > 0 )
         {
            AV37UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37UnidadeOrganizacional_Vinculada1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( dynavUnidadeorganizacional_vinculada2.ItemCount > 0 )
         {
            AV38UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38UnidadeOrganizacional_Vinculada2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         }
         if ( dynavUnidadeorganizacional_vinculada3.ItemCount > 0 )
         {
            AV39UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39UnidadeOrganizacional_Vinculada3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFD02( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV91Pgmname = "WWGeral_UnidadeOrganizacional";
         context.Gx_err = 0;
      }

      protected void RFD02( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 81;
         /* Execute user event: E29D02 */
         E29D02 ();
         nGXsfl_81_idx = 1;
         sGXsfl_81_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_81_idx), 4, 0)), 4, "0");
         SubsflControlProps_812( ) ;
         nGXsfl_81_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_812( ) ;
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ,
                                                 AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ,
                                                 AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ,
                                                 AV71WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ,
                                                 AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ,
                                                 AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ,
                                                 AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ,
                                                 AV75WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ,
                                                 AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ,
                                                 AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ,
                                                 AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ,
                                                 AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ,
                                                 AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ,
                                                 AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ,
                                                 AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ,
                                                 AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ,
                                                 AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ,
                                                 AV87WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ,
                                                 A612UnidadeOrganizacional_Nome ,
                                                 A613UnidadeOrganizacional_Vinculada ,
                                                 A610TpUo_Nome ,
                                                 A23Estado_UF ,
                                                 A629UnidadeOrganizacional_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ,
                                                 AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ,
                                                 A1174UnidadeOrganizacinal_Arvore },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                                 }
            });
            lV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = StringUtil.PadR( StringUtil.RTrim( AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1), 50, "%");
            lV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = StringUtil.PadR( StringUtil.RTrim( AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2), 50, "%");
            lV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = StringUtil.PadR( StringUtil.RTrim( AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3), 50, "%");
            lV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = StringUtil.PadR( StringUtil.RTrim( AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome), 50, "%");
            lV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = StringUtil.PadR( StringUtil.RTrim( AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome), 50, "%");
            lV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf), 2, "%");
            /* Using cursor H00D05 */
            pr_default.execute(3, new Object[] {lV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1, AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1, lV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2, AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2, lV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3, AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3, lV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome, AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel, lV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome, AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel, lV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf, AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel});
            nGXsfl_81_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(3) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A611UnidadeOrganizacional_Codigo = H00D05_A611UnidadeOrganizacional_Codigo[0];
               A609TpUo_Codigo = H00D05_A609TpUo_Codigo[0];
               A629UnidadeOrganizacional_Ativo = H00D05_A629UnidadeOrganizacional_Ativo[0];
               A23Estado_UF = H00D05_A23Estado_UF[0];
               A610TpUo_Nome = H00D05_A610TpUo_Nome[0];
               A612UnidadeOrganizacional_Nome = H00D05_A612UnidadeOrganizacional_Nome[0];
               A613UnidadeOrganizacional_Vinculada = H00D05_A613UnidadeOrganizacional_Vinculada[0];
               n613UnidadeOrganizacional_Vinculada = H00D05_n613UnidadeOrganizacional_Vinculada[0];
               A610TpUo_Nome = H00D05_A610TpUo_Nome[0];
               if ( ! H00D05_n613UnidadeOrganizacional_Vinculada[0] )
               {
                  GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
                  new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
                  A1174UnidadeOrganizacinal_Arvore = GXt_char1;
               }
               else
               {
                  A1174UnidadeOrganizacinal_Arvore = "";
               }
               if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore)) ) ) || ( StringUtil.Like( A1174UnidadeOrganizacinal_Arvore , StringUtil.PadR( AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore , 1000 , "%"),  ' ' ) ) )
               {
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel)) || ( ( StringUtil.StrCmp(A1174UnidadeOrganizacinal_Arvore, AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel) == 0 ) ) )
                  {
                     /* Execute user event: E30D02 */
                     E30D02 ();
                  }
               }
               pr_default.readNext(3);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(3) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(3);
            wbEnd = 81;
            WBD00( ) ;
         }
         nGXsfl_81_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = AV16UnidadeOrganizacional_Nome1;
         AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 = AV37UnidadeOrganizacional_Vinculada1;
         AV71WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = AV19UnidadeOrganizacional_Nome2;
         AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 = AV38UnidadeOrganizacional_Vinculada2;
         AV75WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = AV22UnidadeOrganizacional_Nome3;
         AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 = AV39UnidadeOrganizacional_Vinculada3;
         AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = AV44TFUnidadeOrganizacional_Nome;
         AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = AV45TFUnidadeOrganizacional_Nome_Sel;
         AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = AV48TFTpUo_Nome;
         AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = AV49TFTpUo_Nome_Sel;
         AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = AV52TFUnidadeOrganizacinal_Arvore;
         AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = AV53TFUnidadeOrganizacinal_Arvore_Sel;
         AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = AV56TFEstado_UF;
         AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = AV57TFEstado_UF_Sel;
         AV87WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel = AV60TFUnidadeOrganizacional_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeOrganizacional_Nome1, AV37UnidadeOrganizacional_Vinculada1, AV18DynamicFiltersSelector2, AV19UnidadeOrganizacional_Nome2, AV38UnidadeOrganizacional_Vinculada2, AV21DynamicFiltersSelector3, AV22UnidadeOrganizacional_Nome3, AV39UnidadeOrganizacional_Vinculada3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV44TFUnidadeOrganizacional_Nome, AV45TFUnidadeOrganizacional_Nome_Sel, AV48TFTpUo_Nome, AV49TFTpUo_Nome_Sel, AV56TFEstado_UF, AV57TFEstado_UF_Sel, AV60TFUnidadeOrganizacional_Ativo_Sel, AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV50ddo_TpUo_NomeTitleControlIdToReplace, AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace, AV58ddo_Estado_UFTitleControlIdToReplace, AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace, AV52TFUnidadeOrganizacinal_Arvore, AV53TFUnidadeOrganizacinal_Arvore_Sel, AV91Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = AV16UnidadeOrganizacional_Nome1;
         AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 = AV37UnidadeOrganizacional_Vinculada1;
         AV71WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = AV19UnidadeOrganizacional_Nome2;
         AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 = AV38UnidadeOrganizacional_Vinculada2;
         AV75WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = AV22UnidadeOrganizacional_Nome3;
         AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 = AV39UnidadeOrganizacional_Vinculada3;
         AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = AV44TFUnidadeOrganizacional_Nome;
         AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = AV45TFUnidadeOrganizacional_Nome_Sel;
         AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = AV48TFTpUo_Nome;
         AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = AV49TFTpUo_Nome_Sel;
         AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = AV52TFUnidadeOrganizacinal_Arvore;
         AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = AV53TFUnidadeOrganizacinal_Arvore_Sel;
         AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = AV56TFEstado_UF;
         AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = AV57TFEstado_UF_Sel;
         AV87WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel = AV60TFUnidadeOrganizacional_Ativo_Sel;
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeOrganizacional_Nome1, AV37UnidadeOrganizacional_Vinculada1, AV18DynamicFiltersSelector2, AV19UnidadeOrganizacional_Nome2, AV38UnidadeOrganizacional_Vinculada2, AV21DynamicFiltersSelector3, AV22UnidadeOrganizacional_Nome3, AV39UnidadeOrganizacional_Vinculada3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV44TFUnidadeOrganizacional_Nome, AV45TFUnidadeOrganizacional_Nome_Sel, AV48TFTpUo_Nome, AV49TFTpUo_Nome_Sel, AV56TFEstado_UF, AV57TFEstado_UF_Sel, AV60TFUnidadeOrganizacional_Ativo_Sel, AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV50ddo_TpUo_NomeTitleControlIdToReplace, AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace, AV58ddo_Estado_UFTitleControlIdToReplace, AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace, AV52TFUnidadeOrganizacinal_Arvore, AV53TFUnidadeOrganizacinal_Arvore_Sel, AV91Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = AV16UnidadeOrganizacional_Nome1;
         AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 = AV37UnidadeOrganizacional_Vinculada1;
         AV71WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = AV19UnidadeOrganizacional_Nome2;
         AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 = AV38UnidadeOrganizacional_Vinculada2;
         AV75WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = AV22UnidadeOrganizacional_Nome3;
         AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 = AV39UnidadeOrganizacional_Vinculada3;
         AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = AV44TFUnidadeOrganizacional_Nome;
         AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = AV45TFUnidadeOrganizacional_Nome_Sel;
         AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = AV48TFTpUo_Nome;
         AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = AV49TFTpUo_Nome_Sel;
         AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = AV52TFUnidadeOrganizacinal_Arvore;
         AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = AV53TFUnidadeOrganizacinal_Arvore_Sel;
         AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = AV56TFEstado_UF;
         AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = AV57TFEstado_UF_Sel;
         AV87WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel = AV60TFUnidadeOrganizacional_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeOrganizacional_Nome1, AV37UnidadeOrganizacional_Vinculada1, AV18DynamicFiltersSelector2, AV19UnidadeOrganizacional_Nome2, AV38UnidadeOrganizacional_Vinculada2, AV21DynamicFiltersSelector3, AV22UnidadeOrganizacional_Nome3, AV39UnidadeOrganizacional_Vinculada3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV44TFUnidadeOrganizacional_Nome, AV45TFUnidadeOrganizacional_Nome_Sel, AV48TFTpUo_Nome, AV49TFTpUo_Nome_Sel, AV56TFEstado_UF, AV57TFEstado_UF_Sel, AV60TFUnidadeOrganizacional_Ativo_Sel, AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV50ddo_TpUo_NomeTitleControlIdToReplace, AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace, AV58ddo_Estado_UFTitleControlIdToReplace, AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace, AV52TFUnidadeOrganizacinal_Arvore, AV53TFUnidadeOrganizacinal_Arvore_Sel, AV91Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = AV16UnidadeOrganizacional_Nome1;
         AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 = AV37UnidadeOrganizacional_Vinculada1;
         AV71WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = AV19UnidadeOrganizacional_Nome2;
         AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 = AV38UnidadeOrganizacional_Vinculada2;
         AV75WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = AV22UnidadeOrganizacional_Nome3;
         AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 = AV39UnidadeOrganizacional_Vinculada3;
         AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = AV44TFUnidadeOrganizacional_Nome;
         AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = AV45TFUnidadeOrganizacional_Nome_Sel;
         AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = AV48TFTpUo_Nome;
         AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = AV49TFTpUo_Nome_Sel;
         AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = AV52TFUnidadeOrganizacinal_Arvore;
         AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = AV53TFUnidadeOrganizacinal_Arvore_Sel;
         AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = AV56TFEstado_UF;
         AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = AV57TFEstado_UF_Sel;
         AV87WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel = AV60TFUnidadeOrganizacional_Ativo_Sel;
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeOrganizacional_Nome1, AV37UnidadeOrganizacional_Vinculada1, AV18DynamicFiltersSelector2, AV19UnidadeOrganizacional_Nome2, AV38UnidadeOrganizacional_Vinculada2, AV21DynamicFiltersSelector3, AV22UnidadeOrganizacional_Nome3, AV39UnidadeOrganizacional_Vinculada3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV44TFUnidadeOrganizacional_Nome, AV45TFUnidadeOrganizacional_Nome_Sel, AV48TFTpUo_Nome, AV49TFTpUo_Nome_Sel, AV56TFEstado_UF, AV57TFEstado_UF_Sel, AV60TFUnidadeOrganizacional_Ativo_Sel, AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV50ddo_TpUo_NomeTitleControlIdToReplace, AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace, AV58ddo_Estado_UFTitleControlIdToReplace, AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace, AV52TFUnidadeOrganizacinal_Arvore, AV53TFUnidadeOrganizacinal_Arvore_Sel, AV91Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = AV16UnidadeOrganizacional_Nome1;
         AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 = AV37UnidadeOrganizacional_Vinculada1;
         AV71WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = AV19UnidadeOrganizacional_Nome2;
         AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 = AV38UnidadeOrganizacional_Vinculada2;
         AV75WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = AV22UnidadeOrganizacional_Nome3;
         AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 = AV39UnidadeOrganizacional_Vinculada3;
         AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = AV44TFUnidadeOrganizacional_Nome;
         AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = AV45TFUnidadeOrganizacional_Nome_Sel;
         AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = AV48TFTpUo_Nome;
         AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = AV49TFTpUo_Nome_Sel;
         AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = AV52TFUnidadeOrganizacinal_Arvore;
         AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = AV53TFUnidadeOrganizacinal_Arvore_Sel;
         AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = AV56TFEstado_UF;
         AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = AV57TFEstado_UF_Sel;
         AV87WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel = AV60TFUnidadeOrganizacional_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeOrganizacional_Nome1, AV37UnidadeOrganizacional_Vinculada1, AV18DynamicFiltersSelector2, AV19UnidadeOrganizacional_Nome2, AV38UnidadeOrganizacional_Vinculada2, AV21DynamicFiltersSelector3, AV22UnidadeOrganizacional_Nome3, AV39UnidadeOrganizacional_Vinculada3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV44TFUnidadeOrganizacional_Nome, AV45TFUnidadeOrganizacional_Nome_Sel, AV48TFTpUo_Nome, AV49TFTpUo_Nome_Sel, AV56TFEstado_UF, AV57TFEstado_UF_Sel, AV60TFUnidadeOrganizacional_Ativo_Sel, AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV50ddo_TpUo_NomeTitleControlIdToReplace, AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace, AV58ddo_Estado_UFTitleControlIdToReplace, AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace, AV52TFUnidadeOrganizacinal_Arvore, AV53TFUnidadeOrganizacinal_Arvore_Sel, AV91Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPD00( )
      {
         /* Before Start, stand alone formulas. */
         AV91Pgmname = "WWGeral_UnidadeOrganizacional";
         context.Gx_err = 0;
         GXVvUNIDADEORGANIZACIONAL_VINCULADA1_htmlD02( ) ;
         GXVvUNIDADEORGANIZACIONAL_VINCULADA2_htmlD02( ) ;
         GXVvUNIDADEORGANIZACIONAL_VINCULADA3_htmlD02( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E28D02 */
         E28D02 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV62DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA"), AV43UnidadeOrganizacional_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTPUO_NOMETITLEFILTERDATA"), AV47TpUo_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUNIDADEORGANIZACINAL_ARVORETITLEFILTERDATA"), AV51UnidadeOrganizacinal_ArvoreTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vESTADO_UFTITLEFILTERDATA"), AV55Estado_UFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUNIDADEORGANIZACIONAL_ATIVOTITLEFILTERDATA"), AV59UnidadeOrganizacional_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV16UnidadeOrganizacional_Nome1 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16UnidadeOrganizacional_Nome1", AV16UnidadeOrganizacional_Nome1);
            dynavUnidadeorganizacional_vinculada1.Name = dynavUnidadeorganizacional_vinculada1_Internalname;
            dynavUnidadeorganizacional_vinculada1.CurrentValue = cgiGet( dynavUnidadeorganizacional_vinculada1_Internalname);
            AV37UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( cgiGet( dynavUnidadeorganizacional_vinculada1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37UnidadeOrganizacional_Vinculada1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            AV19UnidadeOrganizacional_Nome2 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19UnidadeOrganizacional_Nome2", AV19UnidadeOrganizacional_Nome2);
            dynavUnidadeorganizacional_vinculada2.Name = dynavUnidadeorganizacional_vinculada2_Internalname;
            dynavUnidadeorganizacional_vinculada2.CurrentValue = cgiGet( dynavUnidadeorganizacional_vinculada2_Internalname);
            AV38UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( cgiGet( dynavUnidadeorganizacional_vinculada2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38UnidadeOrganizacional_Vinculada2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0)));
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV21DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            AV22UnidadeOrganizacional_Nome3 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22UnidadeOrganizacional_Nome3", AV22UnidadeOrganizacional_Nome3);
            dynavUnidadeorganizacional_vinculada3.Name = dynavUnidadeorganizacional_vinculada3_Internalname;
            dynavUnidadeorganizacional_vinculada3.CurrentValue = cgiGet( dynavUnidadeorganizacional_vinculada3_Internalname);
            AV39UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( cgiGet( dynavUnidadeorganizacional_vinculada3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39UnidadeOrganizacional_Vinculada3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0)));
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV20DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
            AV44TFUnidadeOrganizacional_Nome = StringUtil.Upper( cgiGet( edtavTfunidadeorganizacional_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFUnidadeOrganizacional_Nome", AV44TFUnidadeOrganizacional_Nome);
            AV45TFUnidadeOrganizacional_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfunidadeorganizacional_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFUnidadeOrganizacional_Nome_Sel", AV45TFUnidadeOrganizacional_Nome_Sel);
            AV48TFTpUo_Nome = StringUtil.Upper( cgiGet( edtavTftpuo_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFTpUo_Nome", AV48TFTpUo_Nome);
            AV49TFTpUo_Nome_Sel = StringUtil.Upper( cgiGet( edtavTftpuo_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFTpUo_Nome_Sel", AV49TFTpUo_Nome_Sel);
            AV52TFUnidadeOrganizacinal_Arvore = cgiGet( edtavTfunidadeorganizacinal_arvore_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFUnidadeOrganizacinal_Arvore", AV52TFUnidadeOrganizacinal_Arvore);
            AV53TFUnidadeOrganizacinal_Arvore_Sel = cgiGet( edtavTfunidadeorganizacinal_arvore_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFUnidadeOrganizacinal_Arvore_Sel", AV53TFUnidadeOrganizacinal_Arvore_Sel);
            AV56TFEstado_UF = StringUtil.Upper( cgiGet( edtavTfestado_uf_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFEstado_UF", AV56TFEstado_UF);
            AV57TFEstado_UF_Sel = StringUtil.Upper( cgiGet( edtavTfestado_uf_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFEstado_UF_Sel", AV57TFEstado_UF_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfunidadeorganizacional_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfunidadeorganizacional_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFUNIDADEORGANIZACIONAL_ATIVO_SEL");
               GX_FocusControl = edtavTfunidadeorganizacional_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFUnidadeOrganizacional_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUnidadeOrganizacional_Ativo_Sel", StringUtil.Str( (decimal)(AV60TFUnidadeOrganizacional_Ativo_Sel), 1, 0));
            }
            else
            {
               AV60TFUnidadeOrganizacional_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfunidadeorganizacional_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUnidadeOrganizacional_Ativo_Sel", StringUtil.Str( (decimal)(AV60TFUnidadeOrganizacional_Ativo_Sel), 1, 0));
            }
            AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = cgiGet( edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace", AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace);
            AV50ddo_TpUo_NomeTitleControlIdToReplace = cgiGet( edtavDdo_tpuo_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_TpUo_NomeTitleControlIdToReplace", AV50ddo_TpUo_NomeTitleControlIdToReplace);
            AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace = cgiGet( edtavDdo_unidadeorganizacinal_arvoretitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace", AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace);
            AV58ddo_Estado_UFTitleControlIdToReplace = cgiGet( edtavDdo_estado_uftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Estado_UFTitleControlIdToReplace", AV58ddo_Estado_UFTitleControlIdToReplace);
            AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_unidadeorganizacional_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace", AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_81 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_81"), ",", "."));
            AV64GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV65GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            AV40Codigo = (int)(context.localUtil.CToN( cgiGet( "vCODIGO"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_unidadeorganizacional_nome_Caption = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Caption");
            Ddo_unidadeorganizacional_nome_Tooltip = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Tooltip");
            Ddo_unidadeorganizacional_nome_Cls = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Cls");
            Ddo_unidadeorganizacional_nome_Filteredtext_set = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_set");
            Ddo_unidadeorganizacional_nome_Selectedvalue_set = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_set");
            Ddo_unidadeorganizacional_nome_Dropdownoptionstype = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Dropdownoptionstype");
            Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Titlecontrolidtoreplace");
            Ddo_unidadeorganizacional_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Includesortasc"));
            Ddo_unidadeorganizacional_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Includesortdsc"));
            Ddo_unidadeorganizacional_nome_Sortedstatus = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Sortedstatus");
            Ddo_unidadeorganizacional_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Includefilter"));
            Ddo_unidadeorganizacional_nome_Filtertype = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Filtertype");
            Ddo_unidadeorganizacional_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Filterisrange"));
            Ddo_unidadeorganizacional_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Includedatalist"));
            Ddo_unidadeorganizacional_nome_Datalisttype = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Datalisttype");
            Ddo_unidadeorganizacional_nome_Datalistproc = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Datalistproc");
            Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_unidadeorganizacional_nome_Sortasc = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Sortasc");
            Ddo_unidadeorganizacional_nome_Sortdsc = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Sortdsc");
            Ddo_unidadeorganizacional_nome_Loadingdata = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Loadingdata");
            Ddo_unidadeorganizacional_nome_Cleanfilter = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Cleanfilter");
            Ddo_unidadeorganizacional_nome_Noresultsfound = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Noresultsfound");
            Ddo_unidadeorganizacional_nome_Searchbuttontext = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Searchbuttontext");
            Ddo_tpuo_nome_Caption = cgiGet( "DDO_TPUO_NOME_Caption");
            Ddo_tpuo_nome_Tooltip = cgiGet( "DDO_TPUO_NOME_Tooltip");
            Ddo_tpuo_nome_Cls = cgiGet( "DDO_TPUO_NOME_Cls");
            Ddo_tpuo_nome_Filteredtext_set = cgiGet( "DDO_TPUO_NOME_Filteredtext_set");
            Ddo_tpuo_nome_Selectedvalue_set = cgiGet( "DDO_TPUO_NOME_Selectedvalue_set");
            Ddo_tpuo_nome_Dropdownoptionstype = cgiGet( "DDO_TPUO_NOME_Dropdownoptionstype");
            Ddo_tpuo_nome_Titlecontrolidtoreplace = cgiGet( "DDO_TPUO_NOME_Titlecontrolidtoreplace");
            Ddo_tpuo_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TPUO_NOME_Includesortasc"));
            Ddo_tpuo_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TPUO_NOME_Includesortdsc"));
            Ddo_tpuo_nome_Sortedstatus = cgiGet( "DDO_TPUO_NOME_Sortedstatus");
            Ddo_tpuo_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TPUO_NOME_Includefilter"));
            Ddo_tpuo_nome_Filtertype = cgiGet( "DDO_TPUO_NOME_Filtertype");
            Ddo_tpuo_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TPUO_NOME_Filterisrange"));
            Ddo_tpuo_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TPUO_NOME_Includedatalist"));
            Ddo_tpuo_nome_Datalisttype = cgiGet( "DDO_TPUO_NOME_Datalisttype");
            Ddo_tpuo_nome_Datalistproc = cgiGet( "DDO_TPUO_NOME_Datalistproc");
            Ddo_tpuo_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TPUO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tpuo_nome_Sortasc = cgiGet( "DDO_TPUO_NOME_Sortasc");
            Ddo_tpuo_nome_Sortdsc = cgiGet( "DDO_TPUO_NOME_Sortdsc");
            Ddo_tpuo_nome_Loadingdata = cgiGet( "DDO_TPUO_NOME_Loadingdata");
            Ddo_tpuo_nome_Cleanfilter = cgiGet( "DDO_TPUO_NOME_Cleanfilter");
            Ddo_tpuo_nome_Noresultsfound = cgiGet( "DDO_TPUO_NOME_Noresultsfound");
            Ddo_tpuo_nome_Searchbuttontext = cgiGet( "DDO_TPUO_NOME_Searchbuttontext");
            Ddo_unidadeorganizacinal_arvore_Caption = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Caption");
            Ddo_unidadeorganizacinal_arvore_Tooltip = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Tooltip");
            Ddo_unidadeorganizacinal_arvore_Cls = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Cls");
            Ddo_unidadeorganizacinal_arvore_Filteredtext_set = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Filteredtext_set");
            Ddo_unidadeorganizacinal_arvore_Selectedvalue_set = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Selectedvalue_set");
            Ddo_unidadeorganizacinal_arvore_Dropdownoptionstype = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Dropdownoptionstype");
            Ddo_unidadeorganizacinal_arvore_Titlecontrolidtoreplace = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Titlecontrolidtoreplace");
            Ddo_unidadeorganizacinal_arvore_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Includesortasc"));
            Ddo_unidadeorganizacinal_arvore_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Includesortdsc"));
            Ddo_unidadeorganizacinal_arvore_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Includefilter"));
            Ddo_unidadeorganizacinal_arvore_Filtertype = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Filtertype");
            Ddo_unidadeorganizacinal_arvore_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Filterisrange"));
            Ddo_unidadeorganizacinal_arvore_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Includedatalist"));
            Ddo_unidadeorganizacinal_arvore_Datalisttype = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Datalisttype");
            Ddo_unidadeorganizacinal_arvore_Datalistproc = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Datalistproc");
            Ddo_unidadeorganizacinal_arvore_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_unidadeorganizacinal_arvore_Loadingdata = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Loadingdata");
            Ddo_unidadeorganizacinal_arvore_Cleanfilter = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Cleanfilter");
            Ddo_unidadeorganizacinal_arvore_Noresultsfound = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Noresultsfound");
            Ddo_unidadeorganizacinal_arvore_Searchbuttontext = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Searchbuttontext");
            Ddo_estado_uf_Caption = cgiGet( "DDO_ESTADO_UF_Caption");
            Ddo_estado_uf_Tooltip = cgiGet( "DDO_ESTADO_UF_Tooltip");
            Ddo_estado_uf_Cls = cgiGet( "DDO_ESTADO_UF_Cls");
            Ddo_estado_uf_Filteredtext_set = cgiGet( "DDO_ESTADO_UF_Filteredtext_set");
            Ddo_estado_uf_Selectedvalue_set = cgiGet( "DDO_ESTADO_UF_Selectedvalue_set");
            Ddo_estado_uf_Dropdownoptionstype = cgiGet( "DDO_ESTADO_UF_Dropdownoptionstype");
            Ddo_estado_uf_Titlecontrolidtoreplace = cgiGet( "DDO_ESTADO_UF_Titlecontrolidtoreplace");
            Ddo_estado_uf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includesortasc"));
            Ddo_estado_uf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includesortdsc"));
            Ddo_estado_uf_Sortedstatus = cgiGet( "DDO_ESTADO_UF_Sortedstatus");
            Ddo_estado_uf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includefilter"));
            Ddo_estado_uf_Filtertype = cgiGet( "DDO_ESTADO_UF_Filtertype");
            Ddo_estado_uf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Filterisrange"));
            Ddo_estado_uf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includedatalist"));
            Ddo_estado_uf_Datalisttype = cgiGet( "DDO_ESTADO_UF_Datalisttype");
            Ddo_estado_uf_Datalistproc = cgiGet( "DDO_ESTADO_UF_Datalistproc");
            Ddo_estado_uf_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ESTADO_UF_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_estado_uf_Sortasc = cgiGet( "DDO_ESTADO_UF_Sortasc");
            Ddo_estado_uf_Sortdsc = cgiGet( "DDO_ESTADO_UF_Sortdsc");
            Ddo_estado_uf_Loadingdata = cgiGet( "DDO_ESTADO_UF_Loadingdata");
            Ddo_estado_uf_Cleanfilter = cgiGet( "DDO_ESTADO_UF_Cleanfilter");
            Ddo_estado_uf_Noresultsfound = cgiGet( "DDO_ESTADO_UF_Noresultsfound");
            Ddo_estado_uf_Searchbuttontext = cgiGet( "DDO_ESTADO_UF_Searchbuttontext");
            Ddo_unidadeorganizacional_ativo_Caption = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Caption");
            Ddo_unidadeorganizacional_ativo_Tooltip = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Tooltip");
            Ddo_unidadeorganizacional_ativo_Cls = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Cls");
            Ddo_unidadeorganizacional_ativo_Selectedvalue_set = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Selectedvalue_set");
            Ddo_unidadeorganizacional_ativo_Dropdownoptionstype = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Dropdownoptionstype");
            Ddo_unidadeorganizacional_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Titlecontrolidtoreplace");
            Ddo_unidadeorganizacional_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Includesortasc"));
            Ddo_unidadeorganizacional_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Includesortdsc"));
            Ddo_unidadeorganizacional_ativo_Sortedstatus = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Sortedstatus");
            Ddo_unidadeorganizacional_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Includefilter"));
            Ddo_unidadeorganizacional_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Includedatalist"));
            Ddo_unidadeorganizacional_ativo_Datalisttype = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Datalisttype");
            Ddo_unidadeorganizacional_ativo_Datalistfixedvalues = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Datalistfixedvalues");
            Ddo_unidadeorganizacional_ativo_Sortasc = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Sortasc");
            Ddo_unidadeorganizacional_ativo_Sortdsc = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Sortdsc");
            Ddo_unidadeorganizacional_ativo_Cleanfilter = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Cleanfilter");
            Ddo_unidadeorganizacional_ativo_Searchbuttontext = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_unidadeorganizacional_nome_Activeeventkey = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Activeeventkey");
            Ddo_unidadeorganizacional_nome_Filteredtext_get = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_get");
            Ddo_unidadeorganizacional_nome_Selectedvalue_get = cgiGet( "DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_get");
            Ddo_tpuo_nome_Activeeventkey = cgiGet( "DDO_TPUO_NOME_Activeeventkey");
            Ddo_tpuo_nome_Filteredtext_get = cgiGet( "DDO_TPUO_NOME_Filteredtext_get");
            Ddo_tpuo_nome_Selectedvalue_get = cgiGet( "DDO_TPUO_NOME_Selectedvalue_get");
            Ddo_unidadeorganizacinal_arvore_Activeeventkey = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Activeeventkey");
            Ddo_unidadeorganizacinal_arvore_Filteredtext_get = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Filteredtext_get");
            Ddo_unidadeorganizacinal_arvore_Selectedvalue_get = cgiGet( "DDO_UNIDADEORGANIZACINAL_ARVORE_Selectedvalue_get");
            Ddo_estado_uf_Activeeventkey = cgiGet( "DDO_ESTADO_UF_Activeeventkey");
            Ddo_estado_uf_Filteredtext_get = cgiGet( "DDO_ESTADO_UF_Filteredtext_get");
            Ddo_estado_uf_Selectedvalue_get = cgiGet( "DDO_ESTADO_UF_Selectedvalue_get");
            Ddo_unidadeorganizacional_ativo_Activeeventkey = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Activeeventkey");
            Ddo_unidadeorganizacional_ativo_Selectedvalue_get = cgiGet( "DDO_UNIDADEORGANIZACIONAL_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEORGANIZACIONAL_NOME1"), AV16UnidadeOrganizacional_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vUNIDADEORGANIZACIONAL_VINCULADA1"), ",", ".") != Convert.ToDecimal( AV37UnidadeOrganizacional_Vinculada1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEORGANIZACIONAL_NOME2"), AV19UnidadeOrganizacional_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vUNIDADEORGANIZACIONAL_VINCULADA2"), ",", ".") != Convert.ToDecimal( AV38UnidadeOrganizacional_Vinculada2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUNIDADEORGANIZACIONAL_NOME3"), AV22UnidadeOrganizacional_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vUNIDADEORGANIZACIONAL_VINCULADA3"), ",", ".") != Convert.ToDecimal( AV39UnidadeOrganizacional_Vinculada3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEORGANIZACIONAL_NOME"), AV44TFUnidadeOrganizacional_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUNIDADEORGANIZACIONAL_NOME_SEL"), AV45TFUnidadeOrganizacional_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTPUO_NOME"), AV48TFTpUo_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTPUO_NOME_SEL"), AV49TFTpUo_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF"), AV56TFEstado_UF) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF_SEL"), AV57TFEstado_UF_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUNIDADEORGANIZACIONAL_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV60TFUnidadeOrganizacional_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E28D02 */
         E28D02 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28D02( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV18DynamicFiltersSelector2 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector3 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfunidadeorganizacional_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfunidadeorganizacional_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacional_nome_Visible), 5, 0)));
         edtavTfunidadeorganizacional_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfunidadeorganizacional_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacional_nome_sel_Visible), 5, 0)));
         edtavTftpuo_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftpuo_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftpuo_nome_Visible), 5, 0)));
         edtavTftpuo_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftpuo_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftpuo_nome_sel_Visible), 5, 0)));
         edtavTfunidadeorganizacinal_arvore_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfunidadeorganizacinal_arvore_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacinal_arvore_Visible), 5, 0)));
         edtavTfunidadeorganizacinal_arvore_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfunidadeorganizacinal_arvore_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacinal_arvore_sel_Visible), 5, 0)));
         edtavTfestado_uf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfestado_uf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_uf_Visible), 5, 0)));
         edtavTfestado_uf_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfestado_uf_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_uf_sel_Visible), 5, 0)));
         edtavTfunidadeorganizacional_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfunidadeorganizacional_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacional_ativo_sel_Visible), 5, 0)));
         Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_UnidadeOrganizacional_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_nome_Internalname, "TitleControlIdToReplace", Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace);
         AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace", AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace);
         edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tpuo_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_TpUo_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tpuo_nome_Internalname, "TitleControlIdToReplace", Ddo_tpuo_nome_Titlecontrolidtoreplace);
         AV50ddo_TpUo_NomeTitleControlIdToReplace = Ddo_tpuo_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_TpUo_NomeTitleControlIdToReplace", AV50ddo_TpUo_NomeTitleControlIdToReplace);
         edtavDdo_tpuo_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tpuo_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tpuo_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_unidadeorganizacinal_arvore_Titlecontrolidtoreplace = subGrid_Internalname+"_UnidadeOrganizacinal_Arvore";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacinal_arvore_Internalname, "TitleControlIdToReplace", Ddo_unidadeorganizacinal_arvore_Titlecontrolidtoreplace);
         AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace = Ddo_unidadeorganizacinal_arvore_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace", AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace);
         edtavDdo_unidadeorganizacinal_arvoretitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_unidadeorganizacinal_arvoretitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_unidadeorganizacinal_arvoretitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_estado_uf_Titlecontrolidtoreplace = subGrid_Internalname+"_Estado_UF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "TitleControlIdToReplace", Ddo_estado_uf_Titlecontrolidtoreplace);
         AV58ddo_Estado_UFTitleControlIdToReplace = Ddo_estado_uf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Estado_UFTitleControlIdToReplace", AV58ddo_Estado_UFTitleControlIdToReplace);
         edtavDdo_estado_uftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_estado_uftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_estado_uftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_unidadeorganizacional_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_UnidadeOrganizacional_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_ativo_Internalname, "TitleControlIdToReplace", Ddo_unidadeorganizacional_ativo_Titlecontrolidtoreplace);
         AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace = Ddo_unidadeorganizacional_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace", AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace);
         edtavDdo_unidadeorganizacional_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_unidadeorganizacional_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_unidadeorganizacional_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Unidade Organizacional";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Organizacional", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         cmbavOrderedby.addItem("3", "UF", 0);
         cmbavOrderedby.addItem("4", "Ativa?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = AV62DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2) ;
         AV62DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2;
      }

      protected void E29D02( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV43UnidadeOrganizacional_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47TpUo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51UnidadeOrganizacinal_ArvoreTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55Estado_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59UnidadeOrganizacional_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUnidadeOrganizacional_Nome_Titleformat = 2;
         edtUnidadeOrganizacional_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeOrganizacional_Nome_Internalname, "Title", edtUnidadeOrganizacional_Nome_Title);
         edtTpUo_Nome_Titleformat = 2;
         edtTpUo_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV50ddo_TpUo_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTpUo_Nome_Internalname, "Title", edtTpUo_Nome_Title);
         edtUnidadeOrganizacinal_Arvore_Titleformat = 2;
         edtUnidadeOrganizacinal_Arvore_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "�rvore de depend�ncias", AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeOrganizacinal_Arvore_Internalname, "Title", edtUnidadeOrganizacinal_Arvore_Title);
         edtEstado_UF_Titleformat = 2;
         edtEstado_UF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "UF", AV58ddo_Estado_UFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEstado_UF_Internalname, "Title", edtEstado_UF_Title);
         chkUnidadeOrganizacional_Ativo_Titleformat = 2;
         chkUnidadeOrganizacional_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativa?", AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUnidadeOrganizacional_Ativo_Internalname, "Title", chkUnidadeOrganizacional_Ativo.Title.Text);
         AV64GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64GridCurrentPage), 10, 0)));
         AV65GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65GridPageCount), 10, 0)));
         AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = AV16UnidadeOrganizacional_Nome1;
         AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 = AV37UnidadeOrganizacional_Vinculada1;
         AV71WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = AV19UnidadeOrganizacional_Nome2;
         AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 = AV38UnidadeOrganizacional_Vinculada2;
         AV75WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = AV22UnidadeOrganizacional_Nome3;
         AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 = AV39UnidadeOrganizacional_Vinculada3;
         AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = AV44TFUnidadeOrganizacional_Nome;
         AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = AV45TFUnidadeOrganizacional_Nome_Sel;
         AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = AV48TFTpUo_Nome;
         AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = AV49TFTpUo_Nome_Sel;
         AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = AV52TFUnidadeOrganizacinal_Arvore;
         AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = AV53TFUnidadeOrganizacinal_Arvore_Sel;
         AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = AV56TFEstado_UF;
         AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = AV57TFEstado_UF_Sel;
         AV87WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel = AV60TFUnidadeOrganizacional_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43UnidadeOrganizacional_NomeTitleFilterData", AV43UnidadeOrganizacional_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47TpUo_NomeTitleFilterData", AV47TpUo_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51UnidadeOrganizacinal_ArvoreTitleFilterData", AV51UnidadeOrganizacinal_ArvoreTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV55Estado_UFTitleFilterData", AV55Estado_UFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV59UnidadeOrganizacional_AtivoTitleFilterData", AV59UnidadeOrganizacional_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11D02( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV63PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV63PageToGo) ;
         }
      }

      protected void E12D02( )
      {
         /* Ddo_unidadeorganizacional_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_unidadeorganizacional_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_unidadeorganizacional_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFUnidadeOrganizacional_Nome = Ddo_unidadeorganizacional_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFUnidadeOrganizacional_Nome", AV44TFUnidadeOrganizacional_Nome);
            AV45TFUnidadeOrganizacional_Nome_Sel = Ddo_unidadeorganizacional_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFUnidadeOrganizacional_Nome_Sel", AV45TFUnidadeOrganizacional_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13D02( )
      {
         /* Ddo_tpuo_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tpuo_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tpuo_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tpuo_nome_Internalname, "SortedStatus", Ddo_tpuo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tpuo_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tpuo_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tpuo_nome_Internalname, "SortedStatus", Ddo_tpuo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tpuo_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFTpUo_Nome = Ddo_tpuo_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFTpUo_Nome", AV48TFTpUo_Nome);
            AV49TFTpUo_Nome_Sel = Ddo_tpuo_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFTpUo_Nome_Sel", AV49TFTpUo_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14D02( )
      {
         /* Ddo_unidadeorganizacinal_arvore_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_unidadeorganizacinal_arvore_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFUnidadeOrganizacinal_Arvore = Ddo_unidadeorganizacinal_arvore_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFUnidadeOrganizacinal_Arvore", AV52TFUnidadeOrganizacinal_Arvore);
            AV53TFUnidadeOrganizacinal_Arvore_Sel = Ddo_unidadeorganizacinal_arvore_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFUnidadeOrganizacinal_Arvore_Sel", AV53TFUnidadeOrganizacinal_Arvore_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E15D02( )
      {
         /* Ddo_estado_uf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_uf_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_uf_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV56TFEstado_UF = Ddo_estado_uf_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFEstado_UF", AV56TFEstado_UF);
            AV57TFEstado_UF_Sel = Ddo_estado_uf_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFEstado_UF_Sel", AV57TFEstado_UF_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16D02( )
      {
         /* Ddo_unidadeorganizacional_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_unidadeorganizacional_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_ativo_Internalname, "SortedStatus", Ddo_unidadeorganizacional_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_unidadeorganizacional_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_ativo_Internalname, "SortedStatus", Ddo_unidadeorganizacional_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV60TFUnidadeOrganizacional_Ativo_Sel = (short)(NumberUtil.Val( Ddo_unidadeorganizacional_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUnidadeOrganizacional_Ativo_Sel", StringUtil.Str( (decimal)(AV60TFUnidadeOrganizacional_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E30D02( )
      {
         /* Grid_Load Routine */
         AV25Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV25Update);
         AV88Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("geral_unidadeorganizacional.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A611UnidadeOrganizacional_Codigo);
         AV26Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV26Delete);
         AV89Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("geral_unidadeorganizacional.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A611UnidadeOrganizacional_Codigo);
         AV28Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV28Display);
         AV90Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewgeral_unidadeorganizacional.aspx") + "?" + UrlEncode("" +A611UnidadeOrganizacional_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtUnidadeOrganizacional_Nome_Link = formatLink("viewgeral_unidadeorganizacional.aspx") + "?" + UrlEncode("" +A611UnidadeOrganizacional_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtTpUo_Nome_Link = formatLink("viewgeral_tp_uo.aspx") + "?" + UrlEncode("" +A609TpUo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 81;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_812( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_81_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(81, GridRow);
         }
      }

      protected void E17D02( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E23D02( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18D02( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeOrganizacional_Nome1, AV37UnidadeOrganizacional_Vinculada1, AV18DynamicFiltersSelector2, AV19UnidadeOrganizacional_Nome2, AV38UnidadeOrganizacional_Vinculada2, AV21DynamicFiltersSelector3, AV22UnidadeOrganizacional_Nome3, AV39UnidadeOrganizacional_Vinculada3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV44TFUnidadeOrganizacional_Nome, AV45TFUnidadeOrganizacional_Nome_Sel, AV48TFTpUo_Nome, AV49TFTpUo_Nome_Sel, AV56TFEstado_UF, AV57TFEstado_UF_Sel, AV60TFUnidadeOrganizacional_Ativo_Sel, AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV50ddo_TpUo_NomeTitleControlIdToReplace, AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace, AV58ddo_Estado_UFTitleControlIdToReplace, AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace, AV52TFUnidadeOrganizacinal_Arvore, AV53TFUnidadeOrganizacinal_Arvore_Sel, AV91Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada1_Internalname, "Values", dynavUnidadeorganizacional_vinculada1.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada2_Internalname, "Values", dynavUnidadeorganizacional_vinculada2.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada3_Internalname, "Values", dynavUnidadeorganizacional_vinculada3.ToJavascriptSource());
      }

      protected void E24D02( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25D02( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV20DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19D02( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeOrganizacional_Nome1, AV37UnidadeOrganizacional_Vinculada1, AV18DynamicFiltersSelector2, AV19UnidadeOrganizacional_Nome2, AV38UnidadeOrganizacional_Vinculada2, AV21DynamicFiltersSelector3, AV22UnidadeOrganizacional_Nome3, AV39UnidadeOrganizacional_Vinculada3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV44TFUnidadeOrganizacional_Nome, AV45TFUnidadeOrganizacional_Nome_Sel, AV48TFTpUo_Nome, AV49TFTpUo_Nome_Sel, AV56TFEstado_UF, AV57TFEstado_UF_Sel, AV60TFUnidadeOrganizacional_Ativo_Sel, AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV50ddo_TpUo_NomeTitleControlIdToReplace, AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace, AV58ddo_Estado_UFTitleControlIdToReplace, AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace, AV52TFUnidadeOrganizacinal_Arvore, AV53TFUnidadeOrganizacinal_Arvore_Sel, AV91Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada1_Internalname, "Values", dynavUnidadeorganizacional_vinculada1.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada2_Internalname, "Values", dynavUnidadeorganizacional_vinculada2.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada3_Internalname, "Values", dynavUnidadeorganizacional_vinculada3.ToJavascriptSource());
      }

      protected void E26D02( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20D02( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16UnidadeOrganizacional_Nome1, AV37UnidadeOrganizacional_Vinculada1, AV18DynamicFiltersSelector2, AV19UnidadeOrganizacional_Nome2, AV38UnidadeOrganizacional_Vinculada2, AV21DynamicFiltersSelector3, AV22UnidadeOrganizacional_Nome3, AV39UnidadeOrganizacional_Vinculada3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV44TFUnidadeOrganizacional_Nome, AV45TFUnidadeOrganizacional_Nome_Sel, AV48TFTpUo_Nome, AV49TFTpUo_Nome_Sel, AV56TFEstado_UF, AV57TFEstado_UF_Sel, AV60TFUnidadeOrganizacional_Ativo_Sel, AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV50ddo_TpUo_NomeTitleControlIdToReplace, AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace, AV58ddo_Estado_UFTitleControlIdToReplace, AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace, AV52TFUnidadeOrganizacinal_Arvore, AV53TFUnidadeOrganizacinal_Arvore_Sel, AV91Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada1_Internalname, "Values", dynavUnidadeorganizacional_vinculada1.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada2_Internalname, "Values", dynavUnidadeorganizacional_vinculada2.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada3_Internalname, "Values", dynavUnidadeorganizacional_vinculada3.ToJavascriptSource());
      }

      protected void E27D02( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21D02( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada1_Internalname, "Values", dynavUnidadeorganizacional_vinculada1.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada2_Internalname, "Values", dynavUnidadeorganizacional_vinculada2.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada3_Internalname, "Values", dynavUnidadeorganizacional_vinculada3.ToJavascriptSource());
      }

      protected void E22D02( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("geral_unidadeorganizacional.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_unidadeorganizacional_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
         Ddo_tpuo_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tpuo_nome_Internalname, "SortedStatus", Ddo_tpuo_nome_Sortedstatus);
         Ddo_estado_uf_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
         Ddo_unidadeorganizacional_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_ativo_Internalname, "SortedStatus", Ddo_unidadeorganizacional_ativo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_unidadeorganizacional_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_tpuo_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tpuo_nome_Internalname, "SortedStatus", Ddo_tpuo_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_estado_uf_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_unidadeorganizacional_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_ativo_Internalname, "SortedStatus", Ddo_unidadeorganizacional_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavUnidadeorganizacional_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidadeorganizacional_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome1_Visible), 5, 0)));
         dynavUnidadeorganizacional_vinculada1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidadeorganizacional_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
         {
            dynavUnidadeorganizacional_vinculada1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavUnidadeorganizacional_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidadeorganizacional_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome2_Visible), 5, 0)));
         dynavUnidadeorganizacional_vinculada2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidadeorganizacional_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
         {
            dynavUnidadeorganizacional_vinculada2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavUnidadeorganizacional_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidadeorganizacional_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome3_Visible), 5, 0)));
         dynavUnidadeorganizacional_vinculada3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUnidadeorganizacional_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
         {
            dynavUnidadeorganizacional_vinculada3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV19UnidadeOrganizacional_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19UnidadeOrganizacional_Nome2", AV19UnidadeOrganizacional_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         AV21DynamicFiltersSelector3 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         AV22UnidadeOrganizacional_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22UnidadeOrganizacional_Nome3", AV22UnidadeOrganizacional_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV44TFUnidadeOrganizacional_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFUnidadeOrganizacional_Nome", AV44TFUnidadeOrganizacional_Nome);
         Ddo_unidadeorganizacional_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_nome_Internalname, "FilteredText_set", Ddo_unidadeorganizacional_nome_Filteredtext_set);
         AV45TFUnidadeOrganizacional_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFUnidadeOrganizacional_Nome_Sel", AV45TFUnidadeOrganizacional_Nome_Sel);
         Ddo_unidadeorganizacional_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_nome_Internalname, "SelectedValue_set", Ddo_unidadeorganizacional_nome_Selectedvalue_set);
         AV48TFTpUo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFTpUo_Nome", AV48TFTpUo_Nome);
         Ddo_tpuo_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tpuo_nome_Internalname, "FilteredText_set", Ddo_tpuo_nome_Filteredtext_set);
         AV49TFTpUo_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFTpUo_Nome_Sel", AV49TFTpUo_Nome_Sel);
         Ddo_tpuo_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tpuo_nome_Internalname, "SelectedValue_set", Ddo_tpuo_nome_Selectedvalue_set);
         AV52TFUnidadeOrganizacinal_Arvore = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFUnidadeOrganizacinal_Arvore", AV52TFUnidadeOrganizacinal_Arvore);
         Ddo_unidadeorganizacinal_arvore_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacinal_arvore_Internalname, "FilteredText_set", Ddo_unidadeorganizacinal_arvore_Filteredtext_set);
         AV53TFUnidadeOrganizacinal_Arvore_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFUnidadeOrganizacinal_Arvore_Sel", AV53TFUnidadeOrganizacinal_Arvore_Sel);
         Ddo_unidadeorganizacinal_arvore_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacinal_arvore_Internalname, "SelectedValue_set", Ddo_unidadeorganizacinal_arvore_Selectedvalue_set);
         AV56TFEstado_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFEstado_UF", AV56TFEstado_UF);
         Ddo_estado_uf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "FilteredText_set", Ddo_estado_uf_Filteredtext_set);
         AV57TFEstado_UF_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFEstado_UF_Sel", AV57TFEstado_UF_Sel);
         Ddo_estado_uf_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SelectedValue_set", Ddo_estado_uf_Selectedvalue_set);
         AV60TFUnidadeOrganizacional_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUnidadeOrganizacional_Ativo_Sel", StringUtil.Str( (decimal)(AV60TFUnidadeOrganizacional_Ativo_Sel), 1, 0));
         Ddo_unidadeorganizacional_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_ativo_Internalname, "SelectedValue_set", Ddo_unidadeorganizacional_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16UnidadeOrganizacional_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16UnidadeOrganizacional_Nome1", AV16UnidadeOrganizacional_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get(AV91Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV91Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV27Session.Get(AV91Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV92GXV1 = 1;
         while ( AV92GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV92GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV44TFUnidadeOrganizacional_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFUnidadeOrganizacional_Nome", AV44TFUnidadeOrganizacional_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFUnidadeOrganizacional_Nome)) )
               {
                  Ddo_unidadeorganizacional_nome_Filteredtext_set = AV44TFUnidadeOrganizacional_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_nome_Internalname, "FilteredText_set", Ddo_unidadeorganizacional_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME_SEL") == 0 )
            {
               AV45TFUnidadeOrganizacional_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFUnidadeOrganizacional_Nome_Sel", AV45TFUnidadeOrganizacional_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFUnidadeOrganizacional_Nome_Sel)) )
               {
                  Ddo_unidadeorganizacional_nome_Selectedvalue_set = AV45TFUnidadeOrganizacional_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_nome_Internalname, "SelectedValue_set", Ddo_unidadeorganizacional_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTPUO_NOME") == 0 )
            {
               AV48TFTpUo_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFTpUo_Nome", AV48TFTpUo_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFTpUo_Nome)) )
               {
                  Ddo_tpuo_nome_Filteredtext_set = AV48TFTpUo_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tpuo_nome_Internalname, "FilteredText_set", Ddo_tpuo_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTPUO_NOME_SEL") == 0 )
            {
               AV49TFTpUo_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFTpUo_Nome_Sel", AV49TFTpUo_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFTpUo_Nome_Sel)) )
               {
                  Ddo_tpuo_nome_Selectedvalue_set = AV49TFTpUo_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tpuo_nome_Internalname, "SelectedValue_set", Ddo_tpuo_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACINAL_ARVORE") == 0 )
            {
               AV52TFUnidadeOrganizacinal_Arvore = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFUnidadeOrganizacinal_Arvore", AV52TFUnidadeOrganizacinal_Arvore);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFUnidadeOrganizacinal_Arvore)) )
               {
                  Ddo_unidadeorganizacinal_arvore_Filteredtext_set = AV52TFUnidadeOrganizacinal_Arvore;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacinal_arvore_Internalname, "FilteredText_set", Ddo_unidadeorganizacinal_arvore_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACINAL_ARVORE_SEL") == 0 )
            {
               AV53TFUnidadeOrganizacinal_Arvore_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFUnidadeOrganizacinal_Arvore_Sel", AV53TFUnidadeOrganizacinal_Arvore_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFUnidadeOrganizacinal_Arvore_Sel)) )
               {
                  Ddo_unidadeorganizacinal_arvore_Selectedvalue_set = AV53TFUnidadeOrganizacinal_Arvore_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacinal_arvore_Internalname, "SelectedValue_set", Ddo_unidadeorganizacinal_arvore_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFESTADO_UF") == 0 )
            {
               AV56TFEstado_UF = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFEstado_UF", AV56TFEstado_UF);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFEstado_UF)) )
               {
                  Ddo_estado_uf_Filteredtext_set = AV56TFEstado_UF;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "FilteredText_set", Ddo_estado_uf_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFESTADO_UF_SEL") == 0 )
            {
               AV57TFEstado_UF_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFEstado_UF_Sel", AV57TFEstado_UF_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFEstado_UF_Sel)) )
               {
                  Ddo_estado_uf_Selectedvalue_set = AV57TFEstado_UF_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SelectedValue_set", Ddo_estado_uf_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_ATIVO_SEL") == 0 )
            {
               AV60TFUnidadeOrganizacional_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUnidadeOrganizacional_Ativo_Sel", StringUtil.Str( (decimal)(AV60TFUnidadeOrganizacional_Ativo_Sel), 1, 0));
               if ( ! (0==AV60TFUnidadeOrganizacional_Ativo_Sel) )
               {
                  Ddo_unidadeorganizacional_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV60TFUnidadeOrganizacional_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_unidadeorganizacional_ativo_Internalname, "SelectedValue_set", Ddo_unidadeorganizacional_ativo_Selectedvalue_set);
               }
            }
            AV92GXV1 = (int)(AV92GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV16UnidadeOrganizacional_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16UnidadeOrganizacional_Nome1", AV16UnidadeOrganizacional_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
            {
               AV37UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37UnidadeOrganizacional_Vinculada1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
               {
                  AV19UnidadeOrganizacional_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19UnidadeOrganizacional_Nome2", AV19UnidadeOrganizacional_Nome2);
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
               {
                  AV38UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38UnidadeOrganizacional_Vinculada2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV20DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV21DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
                  {
                     AV22UnidadeOrganizacional_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22UnidadeOrganizacional_Nome3", AV22UnidadeOrganizacional_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
                  {
                     AV39UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39UnidadeOrganizacional_Vinculada3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV27Session.Get(AV91Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFUnidadeOrganizacional_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUNIDADEORGANIZACIONAL_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFUnidadeOrganizacional_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFUnidadeOrganizacional_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUNIDADEORGANIZACIONAL_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFUnidadeOrganizacional_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFTpUo_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTPUO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV48TFTpUo_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFTpUo_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTPUO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV49TFTpUo_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFUnidadeOrganizacinal_Arvore)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUNIDADEORGANIZACINAL_ARVORE";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFUnidadeOrganizacinal_Arvore;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFUnidadeOrganizacinal_Arvore_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUNIDADEORGANIZACINAL_ARVORE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV53TFUnidadeOrganizacinal_Arvore_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFEstado_UF)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFESTADO_UF";
            AV11GridStateFilterValue.gxTpr_Value = AV56TFEstado_UF;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFEstado_UF_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFESTADO_UF_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV57TFEstado_UF_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV60TFUnidadeOrganizacional_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUNIDADEORGANIZACIONAL_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV60TFUnidadeOrganizacional_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV91Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16UnidadeOrganizacional_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV16UnidadeOrganizacional_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ! (0==AV37UnidadeOrganizacional_Vinculada1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19UnidadeOrganizacional_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19UnidadeOrganizacional_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ! (0==AV38UnidadeOrganizacional_Vinculada2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22UnidadeOrganizacional_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22UnidadeOrganizacional_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ! (0==AV39UnidadeOrganizacional_Vinculada3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV91Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Geral_UnidadeOrganizacional";
         AV27Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_D02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_D02( true) ;
         }
         else
         {
            wb_table2_8_D02( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_D02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_75_D02( true) ;
         }
         else
         {
            wb_table3_75_D02( false) ;
         }
         return  ;
      }

      protected void wb_table3_75_D02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_D02e( true) ;
         }
         else
         {
            wb_table1_2_D02e( false) ;
         }
      }

      protected void wb_table3_75_D02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_78_D02( true) ;
         }
         else
         {
            wb_table4_78_D02( false) ;
         }
         return  ;
      }

      protected void wb_table4_78_D02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_75_D02e( true) ;
         }
         else
         {
            wb_table3_75_D02e( false) ;
         }
      }

      protected void wb_table4_78_D02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"81\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUnidadeOrganizacional_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtUnidadeOrganizacional_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUnidadeOrganizacional_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTpUo_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTpUo_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTpUo_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUnidadeOrganizacinal_Arvore_Titleformat == 0 )
               {
                  context.SendWebValue( edtUnidadeOrganizacinal_Arvore_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUnidadeOrganizacinal_Arvore_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtEstado_UF_Titleformat == 0 )
               {
                  context.SendWebValue( edtEstado_UF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtEstado_UF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUnidadeOrganizacional_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkUnidadeOrganizacional_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUnidadeOrganizacional_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV25Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV26Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A612UnidadeOrganizacional_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUnidadeOrganizacional_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUnidadeOrganizacional_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtUnidadeOrganizacional_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A610TpUo_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTpUo_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTpUo_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTpUo_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1174UnidadeOrganizacinal_Arvore));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUnidadeOrganizacinal_Arvore_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUnidadeOrganizacinal_Arvore_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A23Estado_UF));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtEstado_UF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtEstado_UF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A629UnidadeOrganizacional_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUnidadeOrganizacional_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUnidadeOrganizacional_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 81 )
         {
            wbEnd = 0;
            nRC_GXsfl_81 = (short)(nGXsfl_81_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_78_D02e( true) ;
         }
         else
         {
            wb_table4_78_D02e( false) ;
         }
      }

      protected void wb_table2_8_D02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGeral_unidadeorganizacionaltitle_Internalname, "Estrutura Organizacional", "", "", lblGeral_unidadeorganizacionaltitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_D02( true) ;
         }
         else
         {
            wb_table5_13_D02( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_D02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_18_D02( true) ;
         }
         else
         {
            wb_table6_18_D02( false) ;
         }
         return  ;
      }

      protected void wb_table6_18_D02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table7_28_D02( true) ;
         }
         else
         {
            wb_table7_28_D02( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_D02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_D02e( true) ;
         }
         else
         {
            wb_table2_8_D02e( false) ;
         }
      }

      protected void wb_table7_28_D02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_33_D02( true) ;
         }
         else
         {
            wb_table8_33_D02( false) ;
         }
         return  ;
      }

      protected void wb_table8_33_D02e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_D02e( true) ;
         }
         else
         {
            wb_table7_28_D02e( false) ;
         }
      }

      protected void wb_table8_33_D02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome1_Internalname, StringUtil.RTrim( AV16UnidadeOrganizacional_Nome1), StringUtil.RTrim( context.localUtil.Format( AV16UnidadeOrganizacional_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavUnidadeorganizacional_vinculada1, dynavUnidadeorganizacional_vinculada1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0)), 1, dynavUnidadeorganizacional_vinculada1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavUnidadeorganizacional_vinculada1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            dynavUnidadeorganizacional_vinculada1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37UnidadeOrganizacional_Vinculada1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada1_Internalname, "Values", (String)(dynavUnidadeorganizacional_vinculada1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome2_Internalname, StringUtil.RTrim( AV19UnidadeOrganizacional_Nome2), StringUtil.RTrim( context.localUtil.Format( AV19UnidadeOrganizacional_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavUnidadeorganizacional_vinculada2, dynavUnidadeorganizacional_vinculada2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0)), 1, dynavUnidadeorganizacional_vinculada2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavUnidadeorganizacional_vinculada2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            dynavUnidadeorganizacional_vinculada2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38UnidadeOrganizacional_Vinculada2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada2_Internalname, "Values", (String)(dynavUnidadeorganizacional_vinculada2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_UnidadeOrganizacional.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome3_Internalname, StringUtil.RTrim( AV22UnidadeOrganizacional_Nome3), StringUtil.RTrim( context.localUtil.Format( AV22UnidadeOrganizacional_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavUnidadeorganizacional_vinculada3, dynavUnidadeorganizacional_vinculada3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0)), 1, dynavUnidadeorganizacional_vinculada3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavUnidadeorganizacional_vinculada3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_WWGeral_UnidadeOrganizacional.htm");
            dynavUnidadeorganizacional_vinculada3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39UnidadeOrganizacional_Vinculada3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_vinculada3_Internalname, "Values", (String)(dynavUnidadeorganizacional_vinculada3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_33_D02e( true) ;
         }
         else
         {
            wb_table8_33_D02e( false) ;
         }
      }

      protected void wb_table6_18_D02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnselectuo_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(81), 2, 0)+","+"null"+");", "Visualizar", bttBtnselectuo_Jsonclick, 7, "Visualizar estrutura organizacional", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e31d01_client"+"'", TempTags, "", 2, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_18_D02e( true) ;
         }
         else
         {
            wb_table6_18_D02e( false) ;
         }
      }

      protected void wb_table5_13_D02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_D02e( true) ;
         }
         else
         {
            wb_table5_13_D02e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAD02( ) ;
         WSD02( ) ;
         WED02( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020518136651");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwgeral_unidadeorganizacional.js", "?2020518136652");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_812( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_81_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_81_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_81_idx;
         edtUnidadeOrganizacional_Nome_Internalname = "UNIDADEORGANIZACIONAL_NOME_"+sGXsfl_81_idx;
         edtTpUo_Nome_Internalname = "TPUO_NOME_"+sGXsfl_81_idx;
         edtUnidadeOrganizacinal_Arvore_Internalname = "UNIDADEORGANIZACINAL_ARVORE_"+sGXsfl_81_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_81_idx;
         chkUnidadeOrganizacional_Ativo_Internalname = "UNIDADEORGANIZACIONAL_ATIVO_"+sGXsfl_81_idx;
      }

      protected void SubsflControlProps_fel_812( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_81_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_81_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_81_fel_idx;
         edtUnidadeOrganizacional_Nome_Internalname = "UNIDADEORGANIZACIONAL_NOME_"+sGXsfl_81_fel_idx;
         edtTpUo_Nome_Internalname = "TPUO_NOME_"+sGXsfl_81_fel_idx;
         edtUnidadeOrganizacinal_Arvore_Internalname = "UNIDADEORGANIZACINAL_ARVORE_"+sGXsfl_81_fel_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_81_fel_idx;
         chkUnidadeOrganizacional_Ativo_Internalname = "UNIDADEORGANIZACIONAL_ATIVO_"+sGXsfl_81_fel_idx;
      }

      protected void sendrow_812( )
      {
         SubsflControlProps_812( ) ;
         WBD00( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_81_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_81_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_81_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV25Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV88Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV88Update_GXI : context.PathToRelativeUrl( AV25Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV25Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV26Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV89Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV89Delete_GXI : context.PathToRelativeUrl( AV26Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV26Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV90Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Display)) ? AV90Display_GXI : context.PathToRelativeUrl( AV28Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeOrganizacional_Nome_Internalname,StringUtil.RTrim( A612UnidadeOrganizacional_Nome),StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtUnidadeOrganizacional_Nome_Link,(String)"",(String)"",(String)"",(String)edtUnidadeOrganizacional_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)81,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTpUo_Nome_Internalname,StringUtil.RTrim( A610TpUo_Nome),StringUtil.RTrim( context.localUtil.Format( A610TpUo_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtTpUo_Nome_Link,(String)"",(String)"",(String)"",(String)edtTpUo_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)81,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeOrganizacinal_Arvore_Internalname,StringUtil.RTrim( A1174UnidadeOrganizacinal_Arvore),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUnidadeOrganizacinal_Arvore_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1000,(short)0,(short)0,(short)81,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_UF_Internalname,StringUtil.RTrim( A23Estado_UF),StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEstado_UF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)81,(short)1,(short)-1,(short)-1,(bool)true,(String)"UF",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUnidadeOrganizacional_Ativo_Internalname,StringUtil.BoolToStr( A629UnidadeOrganizacional_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEORGANIZACIONAL_NOME"+"_"+sGXsfl_81_idx, GetSecureSignedToken( sGXsfl_81_idx, StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEORGANIZACINAL_ARVORE"+"_"+sGXsfl_81_idx, GetSecureSignedToken( sGXsfl_81_idx, StringUtil.RTrim( context.localUtil.Format( A1174UnidadeOrganizacinal_Arvore, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_ESTADO_UF"+"_"+sGXsfl_81_idx, GetSecureSignedToken( sGXsfl_81_idx, StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_UNIDADEORGANIZACIONAL_ATIVO"+"_"+sGXsfl_81_idx, GetSecureSignedToken( sGXsfl_81_idx, A629UnidadeOrganizacional_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_81_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_81_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_81_idx+1));
            sGXsfl_81_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_81_idx), 4, 0)), 4, "0");
            SubsflControlProps_812( ) ;
         }
         /* End function sendrow_812 */
      }

      protected void init_default_properties( )
      {
         lblGeral_unidadeorganizacionaltitle_Internalname = "GERAL_UNIDADEORGANIZACIONALTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         bttBtnselectuo_Internalname = "BTNSELECTUO";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavUnidadeorganizacional_nome1_Internalname = "vUNIDADEORGANIZACIONAL_NOME1";
         dynavUnidadeorganizacional_vinculada1_Internalname = "vUNIDADEORGANIZACIONAL_VINCULADA1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavUnidadeorganizacional_nome2_Internalname = "vUNIDADEORGANIZACIONAL_NOME2";
         dynavUnidadeorganizacional_vinculada2_Internalname = "vUNIDADEORGANIZACIONAL_VINCULADA2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavUnidadeorganizacional_nome3_Internalname = "vUNIDADEORGANIZACIONAL_NOME3";
         dynavUnidadeorganizacional_vinculada3_Internalname = "vUNIDADEORGANIZACIONAL_VINCULADA3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtUnidadeOrganizacional_Nome_Internalname = "UNIDADEORGANIZACIONAL_NOME";
         edtTpUo_Nome_Internalname = "TPUO_NOME";
         edtUnidadeOrganizacinal_Arvore_Internalname = "UNIDADEORGANIZACINAL_ARVORE";
         edtEstado_UF_Internalname = "ESTADO_UF";
         chkUnidadeOrganizacional_Ativo_Internalname = "UNIDADEORGANIZACIONAL_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfunidadeorganizacional_nome_Internalname = "vTFUNIDADEORGANIZACIONAL_NOME";
         edtavTfunidadeorganizacional_nome_sel_Internalname = "vTFUNIDADEORGANIZACIONAL_NOME_SEL";
         edtavTftpuo_nome_Internalname = "vTFTPUO_NOME";
         edtavTftpuo_nome_sel_Internalname = "vTFTPUO_NOME_SEL";
         edtavTfunidadeorganizacinal_arvore_Internalname = "vTFUNIDADEORGANIZACINAL_ARVORE";
         edtavTfunidadeorganizacinal_arvore_sel_Internalname = "vTFUNIDADEORGANIZACINAL_ARVORE_SEL";
         edtavTfestado_uf_Internalname = "vTFESTADO_UF";
         edtavTfestado_uf_sel_Internalname = "vTFESTADO_UF_SEL";
         edtavTfunidadeorganizacional_ativo_sel_Internalname = "vTFUNIDADEORGANIZACIONAL_ATIVO_SEL";
         Ddo_unidadeorganizacional_nome_Internalname = "DDO_UNIDADEORGANIZACIONAL_NOME";
         edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname = "vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE";
         Ddo_tpuo_nome_Internalname = "DDO_TPUO_NOME";
         edtavDdo_tpuo_nometitlecontrolidtoreplace_Internalname = "vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_unidadeorganizacinal_arvore_Internalname = "DDO_UNIDADEORGANIZACINAL_ARVORE";
         edtavDdo_unidadeorganizacinal_arvoretitlecontrolidtoreplace_Internalname = "vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE";
         Ddo_estado_uf_Internalname = "DDO_ESTADO_UF";
         edtavDdo_estado_uftitlecontrolidtoreplace_Internalname = "vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE";
         Ddo_unidadeorganizacional_ativo_Internalname = "DDO_UNIDADEORGANIZACIONAL_ATIVO";
         edtavDdo_unidadeorganizacional_ativotitlecontrolidtoreplace_Internalname = "vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtEstado_UF_Jsonclick = "";
         edtUnidadeOrganizacinal_Arvore_Jsonclick = "";
         edtTpUo_Nome_Jsonclick = "";
         edtUnidadeOrganizacional_Nome_Jsonclick = "";
         dynavUnidadeorganizacional_vinculada3_Jsonclick = "";
         edtavUnidadeorganizacional_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         dynavUnidadeorganizacional_vinculada2_Jsonclick = "";
         edtavUnidadeorganizacional_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         dynavUnidadeorganizacional_vinculada1_Jsonclick = "";
         edtavUnidadeorganizacional_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtTpUo_Nome_Link = "";
         edtUnidadeOrganizacional_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkUnidadeOrganizacional_Ativo_Titleformat = 0;
         edtEstado_UF_Titleformat = 0;
         edtUnidadeOrganizacinal_Arvore_Titleformat = 0;
         edtTpUo_Nome_Titleformat = 0;
         edtUnidadeOrganizacional_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         dynavUnidadeorganizacional_vinculada3.Visible = 1;
         edtavUnidadeorganizacional_nome3_Visible = 1;
         dynavUnidadeorganizacional_vinculada2.Visible = 1;
         edtavUnidadeorganizacional_nome2_Visible = 1;
         dynavUnidadeorganizacional_vinculada1.Visible = 1;
         edtavUnidadeorganizacional_nome1_Visible = 1;
         chkUnidadeOrganizacional_Ativo.Title.Text = "Ativa?";
         edtEstado_UF_Title = "UF";
         edtUnidadeOrganizacinal_Arvore_Title = "�rvore de depend�ncias";
         edtTpUo_Nome_Title = "Tipo";
         edtUnidadeOrganizacional_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkUnidadeOrganizacional_Ativo.Caption = "";
         edtavDdo_unidadeorganizacional_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_estado_uftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_unidadeorganizacinal_arvoretitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tpuo_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfunidadeorganizacional_ativo_sel_Jsonclick = "";
         edtavTfunidadeorganizacional_ativo_sel_Visible = 1;
         edtavTfestado_uf_sel_Jsonclick = "";
         edtavTfestado_uf_sel_Visible = 1;
         edtavTfestado_uf_Jsonclick = "";
         edtavTfestado_uf_Visible = 1;
         edtavTfunidadeorganizacinal_arvore_sel_Visible = 1;
         edtavTfunidadeorganizacinal_arvore_Visible = 1;
         edtavTftpuo_nome_sel_Jsonclick = "";
         edtavTftpuo_nome_sel_Visible = 1;
         edtavTftpuo_nome_Jsonclick = "";
         edtavTftpuo_nome_Visible = 1;
         edtavTfunidadeorganizacional_nome_sel_Jsonclick = "";
         edtavTfunidadeorganizacional_nome_sel_Visible = 1;
         edtavTfunidadeorganizacional_nome_Jsonclick = "";
         edtavTfunidadeorganizacional_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_unidadeorganizacional_ativo_Searchbuttontext = "Pesquisar";
         Ddo_unidadeorganizacional_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_unidadeorganizacional_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_unidadeorganizacional_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_unidadeorganizacional_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_unidadeorganizacional_ativo_Datalisttype = "FixedValues";
         Ddo_unidadeorganizacional_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_unidadeorganizacional_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_ativo_Titlecontrolidtoreplace = "";
         Ddo_unidadeorganizacional_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_unidadeorganizacional_ativo_Cls = "ColumnSettings";
         Ddo_unidadeorganizacional_ativo_Tooltip = "Op��es";
         Ddo_unidadeorganizacional_ativo_Caption = "";
         Ddo_estado_uf_Searchbuttontext = "Pesquisar";
         Ddo_estado_uf_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_estado_uf_Cleanfilter = "Limpar pesquisa";
         Ddo_estado_uf_Loadingdata = "Carregando dados...";
         Ddo_estado_uf_Sortdsc = "Ordenar de Z � A";
         Ddo_estado_uf_Sortasc = "Ordenar de A � Z";
         Ddo_estado_uf_Datalistupdateminimumcharacters = 0;
         Ddo_estado_uf_Datalistproc = "GetWWGeral_UnidadeOrganizacionalFilterData";
         Ddo_estado_uf_Datalisttype = "Dynamic";
         Ddo_estado_uf_Includedatalist = Convert.ToBoolean( -1);
         Ddo_estado_uf_Filterisrange = Convert.ToBoolean( 0);
         Ddo_estado_uf_Filtertype = "Character";
         Ddo_estado_uf_Includefilter = Convert.ToBoolean( -1);
         Ddo_estado_uf_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_estado_uf_Includesortasc = Convert.ToBoolean( -1);
         Ddo_estado_uf_Titlecontrolidtoreplace = "";
         Ddo_estado_uf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_estado_uf_Cls = "ColumnSettings";
         Ddo_estado_uf_Tooltip = "Op��es";
         Ddo_estado_uf_Caption = "";
         Ddo_unidadeorganizacinal_arvore_Searchbuttontext = "Pesquisar";
         Ddo_unidadeorganizacinal_arvore_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_unidadeorganizacinal_arvore_Cleanfilter = "Limpar pesquisa";
         Ddo_unidadeorganizacinal_arvore_Loadingdata = "Carregando dados...";
         Ddo_unidadeorganizacinal_arvore_Datalistupdateminimumcharacters = 0;
         Ddo_unidadeorganizacinal_arvore_Datalistproc = "GetWWGeral_UnidadeOrganizacionalFilterData";
         Ddo_unidadeorganizacinal_arvore_Datalisttype = "Dynamic";
         Ddo_unidadeorganizacinal_arvore_Includedatalist = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacinal_arvore_Filterisrange = Convert.ToBoolean( 0);
         Ddo_unidadeorganizacinal_arvore_Filtertype = "Character";
         Ddo_unidadeorganizacinal_arvore_Includefilter = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacinal_arvore_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_unidadeorganizacinal_arvore_Includesortasc = Convert.ToBoolean( 0);
         Ddo_unidadeorganizacinal_arvore_Titlecontrolidtoreplace = "";
         Ddo_unidadeorganizacinal_arvore_Dropdownoptionstype = "GridTitleSettings";
         Ddo_unidadeorganizacinal_arvore_Cls = "ColumnSettings";
         Ddo_unidadeorganizacinal_arvore_Tooltip = "Op��es";
         Ddo_unidadeorganizacinal_arvore_Caption = "";
         Ddo_tpuo_nome_Searchbuttontext = "Pesquisar";
         Ddo_tpuo_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tpuo_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_tpuo_nome_Loadingdata = "Carregando dados...";
         Ddo_tpuo_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_tpuo_nome_Sortasc = "Ordenar de A � Z";
         Ddo_tpuo_nome_Datalistupdateminimumcharacters = 0;
         Ddo_tpuo_nome_Datalistproc = "GetWWGeral_UnidadeOrganizacionalFilterData";
         Ddo_tpuo_nome_Datalisttype = "Dynamic";
         Ddo_tpuo_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tpuo_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tpuo_nome_Filtertype = "Character";
         Ddo_tpuo_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_tpuo_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tpuo_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tpuo_nome_Titlecontrolidtoreplace = "";
         Ddo_tpuo_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tpuo_nome_Cls = "ColumnSettings";
         Ddo_tpuo_nome_Tooltip = "Op��es";
         Ddo_tpuo_nome_Caption = "";
         Ddo_unidadeorganizacional_nome_Searchbuttontext = "Pesquisar";
         Ddo_unidadeorganizacional_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_unidadeorganizacional_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_unidadeorganizacional_nome_Loadingdata = "Carregando dados...";
         Ddo_unidadeorganizacional_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_unidadeorganizacional_nome_Sortasc = "Ordenar de A � Z";
         Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters = 0;
         Ddo_unidadeorganizacional_nome_Datalistproc = "GetWWGeral_UnidadeOrganizacionalFilterData";
         Ddo_unidadeorganizacional_nome_Datalisttype = "Dynamic";
         Ddo_unidadeorganizacional_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_unidadeorganizacional_nome_Filtertype = "Character";
         Ddo_unidadeorganizacional_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace = "";
         Ddo_unidadeorganizacional_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_unidadeorganizacional_nome_Cls = "ColumnSettings";
         Ddo_unidadeorganizacional_nome_Tooltip = "Op��es";
         Ddo_unidadeorganizacional_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Unidade Organizacional";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV43UnidadeOrganizacional_NomeTitleFilterData',fld:'vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV47TpUo_NomeTitleFilterData',fld:'vTPUO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV51UnidadeOrganizacinal_ArvoreTitleFilterData',fld:'vUNIDADEORGANIZACINAL_ARVORETITLEFILTERDATA',pic:'',nv:null},{av:'AV55Estado_UFTitleFilterData',fld:'vESTADO_UFTITLEFILTERDATA',pic:'',nv:null},{av:'AV59UnidadeOrganizacional_AtivoTitleFilterData',fld:'vUNIDADEORGANIZACIONAL_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtUnidadeOrganizacional_Nome_Titleformat',ctrl:'UNIDADEORGANIZACIONAL_NOME',prop:'Titleformat'},{av:'edtUnidadeOrganizacional_Nome_Title',ctrl:'UNIDADEORGANIZACIONAL_NOME',prop:'Title'},{av:'edtTpUo_Nome_Titleformat',ctrl:'TPUO_NOME',prop:'Titleformat'},{av:'edtTpUo_Nome_Title',ctrl:'TPUO_NOME',prop:'Title'},{av:'edtUnidadeOrganizacinal_Arvore_Titleformat',ctrl:'UNIDADEORGANIZACINAL_ARVORE',prop:'Titleformat'},{av:'edtUnidadeOrganizacinal_Arvore_Title',ctrl:'UNIDADEORGANIZACINAL_ARVORE',prop:'Title'},{av:'edtEstado_UF_Titleformat',ctrl:'ESTADO_UF',prop:'Titleformat'},{av:'edtEstado_UF_Title',ctrl:'ESTADO_UF',prop:'Title'},{av:'chkUnidadeOrganizacional_Ativo_Titleformat',ctrl:'UNIDADEORGANIZACIONAL_ATIVO',prop:'Titleformat'},{av:'chkUnidadeOrganizacional_Ativo.Title.Text',ctrl:'UNIDADEORGANIZACIONAL_ATIVO',prop:'Title'},{av:'AV64GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV65GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11D02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_UNIDADEORGANIZACIONAL_NOME.ONOPTIONCLICKED","{handler:'E12D02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_unidadeorganizacional_nome_Activeeventkey',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'ActiveEventKey'},{av:'Ddo_unidadeorganizacional_nome_Filteredtext_get',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'FilteredText_get'},{av:'Ddo_unidadeorganizacional_nome_Selectedvalue_get',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_unidadeorganizacional_nome_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SortedStatus'},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tpuo_nome_Sortedstatus',ctrl:'DDO_TPUO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_unidadeorganizacional_ativo_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TPUO_NOME.ONOPTIONCLICKED","{handler:'E13D02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_tpuo_nome_Activeeventkey',ctrl:'DDO_TPUO_NOME',prop:'ActiveEventKey'},{av:'Ddo_tpuo_nome_Filteredtext_get',ctrl:'DDO_TPUO_NOME',prop:'FilteredText_get'},{av:'Ddo_tpuo_nome_Selectedvalue_get',ctrl:'DDO_TPUO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tpuo_nome_Sortedstatus',ctrl:'DDO_TPUO_NOME',prop:'SortedStatus'},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_unidadeorganizacional_nome_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_unidadeorganizacional_ativo_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_UNIDADEORGANIZACINAL_ARVORE.ONOPTIONCLICKED","{handler:'E14D02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_unidadeorganizacinal_arvore_Activeeventkey',ctrl:'DDO_UNIDADEORGANIZACINAL_ARVORE',prop:'ActiveEventKey'},{av:'Ddo_unidadeorganizacinal_arvore_Filteredtext_get',ctrl:'DDO_UNIDADEORGANIZACINAL_ARVORE',prop:'FilteredText_get'},{av:'Ddo_unidadeorganizacinal_arvore_Selectedvalue_get',ctrl:'DDO_UNIDADEORGANIZACINAL_ARVORE',prop:'SelectedValue_get'}],oparms:[{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''}]}");
         setEventMetadata("DDO_ESTADO_UF.ONOPTIONCLICKED","{handler:'E15D02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_estado_uf_Activeeventkey',ctrl:'DDO_ESTADO_UF',prop:'ActiveEventKey'},{av:'Ddo_estado_uf_Filteredtext_get',ctrl:'DDO_ESTADO_UF',prop:'FilteredText_get'},{av:'Ddo_estado_uf_Selectedvalue_get',ctrl:'DDO_ESTADO_UF',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'Ddo_unidadeorganizacional_nome_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SortedStatus'},{av:'Ddo_tpuo_nome_Sortedstatus',ctrl:'DDO_TPUO_NOME',prop:'SortedStatus'},{av:'Ddo_unidadeorganizacional_ativo_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_UNIDADEORGANIZACIONAL_ATIVO.ONOPTIONCLICKED","{handler:'E16D02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_unidadeorganizacional_ativo_Activeeventkey',ctrl:'DDO_UNIDADEORGANIZACIONAL_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_unidadeorganizacional_ativo_Selectedvalue_get',ctrl:'DDO_UNIDADEORGANIZACIONAL_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_unidadeorganizacional_ativo_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_ATIVO',prop:'SortedStatus'},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_unidadeorganizacional_nome_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SortedStatus'},{av:'Ddo_tpuo_nome_Sortedstatus',ctrl:'DDO_TPUO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E30D02',iparms:[{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV25Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV26Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV28Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtUnidadeOrganizacional_Nome_Link',ctrl:'UNIDADEORGANIZACIONAL_NOME',prop:'Link'},{av:'edtTpUo_Nome_Link',ctrl:'TPUO_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17D02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E23D02',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18D02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada2'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada3'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E24D02',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E25D02',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19D02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada2'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada3'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E26D02',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20D02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada2'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada3'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E27D02',iparms:[{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21D02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TpUo_NomeTitleControlIdToReplace',fld:'vDDO_TPUO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACINAL_ARVORETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV44TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'Ddo_unidadeorganizacional_nome_Filteredtext_set',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'FilteredText_set'},{av:'AV45TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_unidadeorganizacional_nome_Selectedvalue_set',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SelectedValue_set'},{av:'AV48TFTpUo_Nome',fld:'vTFTPUO_NOME',pic:'@!',nv:''},{av:'Ddo_tpuo_nome_Filteredtext_set',ctrl:'DDO_TPUO_NOME',prop:'FilteredText_set'},{av:'AV49TFTpUo_Nome_Sel',fld:'vTFTPUO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tpuo_nome_Selectedvalue_set',ctrl:'DDO_TPUO_NOME',prop:'SelectedValue_set'},{av:'AV52TFUnidadeOrganizacinal_Arvore',fld:'vTFUNIDADEORGANIZACINAL_ARVORE',pic:'',nv:''},{av:'Ddo_unidadeorganizacinal_arvore_Filteredtext_set',ctrl:'DDO_UNIDADEORGANIZACINAL_ARVORE',prop:'FilteredText_set'},{av:'AV53TFUnidadeOrganizacinal_Arvore_Sel',fld:'vTFUNIDADEORGANIZACINAL_ARVORE_SEL',pic:'',nv:''},{av:'Ddo_unidadeorganizacinal_arvore_Selectedvalue_set',ctrl:'DDO_UNIDADEORGANIZACINAL_ARVORE',prop:'SelectedValue_set'},{av:'AV56TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'Ddo_estado_uf_Filteredtext_set',ctrl:'DDO_ESTADO_UF',prop:'FilteredText_set'},{av:'AV57TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'Ddo_estado_uf_Selectedvalue_set',ctrl:'DDO_ESTADO_UF',prop:'SelectedValue_set'},{av:'AV60TFUnidadeOrganizacional_Ativo_Sel',fld:'vTFUNIDADEORGANIZACIONAL_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_unidadeorganizacional_ativo_Selectedvalue_set',ctrl:'DDO_UNIDADEORGANIZACIONAL_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada1'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV37UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV38UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV39UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada2'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'dynavUnidadeorganizacional_vinculada3'}]}");
         setEventMetadata("'DOSELECTUO'","{handler:'E31D01',iparms:[{av:'AV40Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV40Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOINSERT'","{handler:'E22D02',iparms:[{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_unidadeorganizacional_nome_Activeeventkey = "";
         Ddo_unidadeorganizacional_nome_Filteredtext_get = "";
         Ddo_unidadeorganizacional_nome_Selectedvalue_get = "";
         Ddo_tpuo_nome_Activeeventkey = "";
         Ddo_tpuo_nome_Filteredtext_get = "";
         Ddo_tpuo_nome_Selectedvalue_get = "";
         Ddo_unidadeorganizacinal_arvore_Activeeventkey = "";
         Ddo_unidadeorganizacinal_arvore_Filteredtext_get = "";
         Ddo_unidadeorganizacinal_arvore_Selectedvalue_get = "";
         Ddo_estado_uf_Activeeventkey = "";
         Ddo_estado_uf_Filteredtext_get = "";
         Ddo_estado_uf_Selectedvalue_get = "";
         Ddo_unidadeorganizacional_ativo_Activeeventkey = "";
         Ddo_unidadeorganizacional_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16UnidadeOrganizacional_Nome1 = "";
         AV18DynamicFiltersSelector2 = "";
         AV19UnidadeOrganizacional_Nome2 = "";
         AV21DynamicFiltersSelector3 = "";
         AV22UnidadeOrganizacional_Nome3 = "";
         AV44TFUnidadeOrganizacional_Nome = "";
         AV45TFUnidadeOrganizacional_Nome_Sel = "";
         AV48TFTpUo_Nome = "";
         AV49TFTpUo_Nome_Sel = "";
         AV56TFEstado_UF = "";
         AV57TFEstado_UF_Sel = "";
         AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = "";
         AV50ddo_TpUo_NomeTitleControlIdToReplace = "";
         AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace = "";
         AV58ddo_Estado_UFTitleControlIdToReplace = "";
         AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace = "";
         AV52TFUnidadeOrganizacinal_Arvore = "";
         AV53TFUnidadeOrganizacinal_Arvore_Sel = "";
         AV91Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV62DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV43UnidadeOrganizacional_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47TpUo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51UnidadeOrganizacinal_ArvoreTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55Estado_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59UnidadeOrganizacional_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_unidadeorganizacional_nome_Filteredtext_set = "";
         Ddo_unidadeorganizacional_nome_Selectedvalue_set = "";
         Ddo_unidadeorganizacional_nome_Sortedstatus = "";
         Ddo_tpuo_nome_Filteredtext_set = "";
         Ddo_tpuo_nome_Selectedvalue_set = "";
         Ddo_tpuo_nome_Sortedstatus = "";
         Ddo_unidadeorganizacinal_arvore_Filteredtext_set = "";
         Ddo_unidadeorganizacinal_arvore_Selectedvalue_set = "";
         Ddo_estado_uf_Filteredtext_set = "";
         Ddo_estado_uf_Selectedvalue_set = "";
         Ddo_estado_uf_Sortedstatus = "";
         Ddo_unidadeorganizacional_ativo_Selectedvalue_set = "";
         Ddo_unidadeorganizacional_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV25Update = "";
         AV88Update_GXI = "";
         AV26Delete = "";
         AV89Delete_GXI = "";
         AV28Display = "";
         AV90Display_GXI = "";
         A612UnidadeOrganizacional_Nome = "";
         A610TpUo_Nome = "";
         A1174UnidadeOrganizacinal_Arvore = "";
         A23Estado_UF = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00D02_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00D02_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00D02_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         H00D03_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00D03_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00D03_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         H00D04_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00D04_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00D04_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = "";
         lV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = "";
         lV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = "";
         lV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = "";
         lV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = "";
         lV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = "";
         AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = "";
         AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = "";
         AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = "";
         AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = "";
         AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = "";
         AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = "";
         AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = "";
         AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = "";
         AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = "";
         AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = "";
         AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = "";
         AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = "";
         AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = "";
         AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = "";
         H00D05_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00D05_A609TpUo_Codigo = new int[1] ;
         H00D05_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         H00D05_A23Estado_UF = new String[] {""} ;
         H00D05_A610TpUo_Nome = new String[] {""} ;
         H00D05_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00D05_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         H00D05_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         GXt_char1 = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV27Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblGeral_unidadeorganizacionaltitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         bttBtnselectuo_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwgeral_unidadeorganizacional__default(),
            new Object[][] {
                new Object[] {
               H00D02_A611UnidadeOrganizacional_Codigo, H00D02_A612UnidadeOrganizacional_Nome, H00D02_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               H00D03_A611UnidadeOrganizacional_Codigo, H00D03_A612UnidadeOrganizacional_Nome, H00D03_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               H00D04_A611UnidadeOrganizacional_Codigo, H00D04_A612UnidadeOrganizacional_Nome, H00D04_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               H00D05_A611UnidadeOrganizacional_Codigo, H00D05_A609TpUo_Codigo, H00D05_A629UnidadeOrganizacional_Ativo, H00D05_A23Estado_UF, H00D05_A610TpUo_Nome, H00D05_A612UnidadeOrganizacional_Nome, H00D05_A613UnidadeOrganizacional_Vinculada, H00D05_n613UnidadeOrganizacional_Vinculada
               }
            }
         );
         AV91Pgmname = "WWGeral_UnidadeOrganizacional";
         /* GeneXus formulas. */
         AV91Pgmname = "WWGeral_UnidadeOrganizacional";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_81 ;
      private short nGXsfl_81_idx=1 ;
      private short AV13OrderedBy ;
      private short AV60TFUnidadeOrganizacional_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_81_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV87WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ;
      private short edtUnidadeOrganizacional_Nome_Titleformat ;
      private short edtTpUo_Nome_Titleformat ;
      private short edtUnidadeOrganizacinal_Arvore_Titleformat ;
      private short edtEstado_UF_Titleformat ;
      private short chkUnidadeOrganizacional_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV37UnidadeOrganizacional_Vinculada1 ;
      private int AV38UnidadeOrganizacional_Vinculada2 ;
      private int AV39UnidadeOrganizacional_Vinculada3 ;
      private int A611UnidadeOrganizacional_Codigo ;
      private int A609TpUo_Codigo ;
      private int AV40Codigo ;
      private int A613UnidadeOrganizacional_Vinculada ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters ;
      private int Ddo_tpuo_nome_Datalistupdateminimumcharacters ;
      private int Ddo_unidadeorganizacinal_arvore_Datalistupdateminimumcharacters ;
      private int Ddo_estado_uf_Datalistupdateminimumcharacters ;
      private int edtavTfunidadeorganizacional_nome_Visible ;
      private int edtavTfunidadeorganizacional_nome_sel_Visible ;
      private int edtavTftpuo_nome_Visible ;
      private int edtavTftpuo_nome_sel_Visible ;
      private int edtavTfunidadeorganizacinal_arvore_Visible ;
      private int edtavTfunidadeorganizacinal_arvore_sel_Visible ;
      private int edtavTfestado_uf_Visible ;
      private int edtavTfestado_uf_sel_Visible ;
      private int edtavTfunidadeorganizacional_ativo_sel_Visible ;
      private int edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tpuo_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_unidadeorganizacinal_arvoretitlecontrolidtoreplace_Visible ;
      private int edtavDdo_estado_uftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_unidadeorganizacional_ativotitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ;
      private int AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ;
      private int AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ;
      private int edtavOrdereddsc_Visible ;
      private int AV63PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavUnidadeorganizacional_nome1_Visible ;
      private int edtavUnidadeorganizacional_nome2_Visible ;
      private int edtavUnidadeorganizacional_nome3_Visible ;
      private int AV92GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV64GridCurrentPage ;
      private long AV65GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_unidadeorganizacional_nome_Activeeventkey ;
      private String Ddo_unidadeorganizacional_nome_Filteredtext_get ;
      private String Ddo_unidadeorganizacional_nome_Selectedvalue_get ;
      private String Ddo_tpuo_nome_Activeeventkey ;
      private String Ddo_tpuo_nome_Filteredtext_get ;
      private String Ddo_tpuo_nome_Selectedvalue_get ;
      private String Ddo_unidadeorganizacinal_arvore_Activeeventkey ;
      private String Ddo_unidadeorganizacinal_arvore_Filteredtext_get ;
      private String Ddo_unidadeorganizacinal_arvore_Selectedvalue_get ;
      private String Ddo_estado_uf_Activeeventkey ;
      private String Ddo_estado_uf_Filteredtext_get ;
      private String Ddo_estado_uf_Selectedvalue_get ;
      private String Ddo_unidadeorganizacional_ativo_Activeeventkey ;
      private String Ddo_unidadeorganizacional_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_81_idx="0001" ;
      private String AV16UnidadeOrganizacional_Nome1 ;
      private String AV19UnidadeOrganizacional_Nome2 ;
      private String AV22UnidadeOrganizacional_Nome3 ;
      private String AV44TFUnidadeOrganizacional_Nome ;
      private String AV45TFUnidadeOrganizacional_Nome_Sel ;
      private String AV48TFTpUo_Nome ;
      private String AV49TFTpUo_Nome_Sel ;
      private String AV56TFEstado_UF ;
      private String AV57TFEstado_UF_Sel ;
      private String AV52TFUnidadeOrganizacinal_Arvore ;
      private String AV53TFUnidadeOrganizacinal_Arvore_Sel ;
      private String AV91Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_unidadeorganizacional_nome_Caption ;
      private String Ddo_unidadeorganizacional_nome_Tooltip ;
      private String Ddo_unidadeorganizacional_nome_Cls ;
      private String Ddo_unidadeorganizacional_nome_Filteredtext_set ;
      private String Ddo_unidadeorganizacional_nome_Selectedvalue_set ;
      private String Ddo_unidadeorganizacional_nome_Dropdownoptionstype ;
      private String Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace ;
      private String Ddo_unidadeorganizacional_nome_Sortedstatus ;
      private String Ddo_unidadeorganizacional_nome_Filtertype ;
      private String Ddo_unidadeorganizacional_nome_Datalisttype ;
      private String Ddo_unidadeorganizacional_nome_Datalistproc ;
      private String Ddo_unidadeorganizacional_nome_Sortasc ;
      private String Ddo_unidadeorganizacional_nome_Sortdsc ;
      private String Ddo_unidadeorganizacional_nome_Loadingdata ;
      private String Ddo_unidadeorganizacional_nome_Cleanfilter ;
      private String Ddo_unidadeorganizacional_nome_Noresultsfound ;
      private String Ddo_unidadeorganizacional_nome_Searchbuttontext ;
      private String Ddo_tpuo_nome_Caption ;
      private String Ddo_tpuo_nome_Tooltip ;
      private String Ddo_tpuo_nome_Cls ;
      private String Ddo_tpuo_nome_Filteredtext_set ;
      private String Ddo_tpuo_nome_Selectedvalue_set ;
      private String Ddo_tpuo_nome_Dropdownoptionstype ;
      private String Ddo_tpuo_nome_Titlecontrolidtoreplace ;
      private String Ddo_tpuo_nome_Sortedstatus ;
      private String Ddo_tpuo_nome_Filtertype ;
      private String Ddo_tpuo_nome_Datalisttype ;
      private String Ddo_tpuo_nome_Datalistproc ;
      private String Ddo_tpuo_nome_Sortasc ;
      private String Ddo_tpuo_nome_Sortdsc ;
      private String Ddo_tpuo_nome_Loadingdata ;
      private String Ddo_tpuo_nome_Cleanfilter ;
      private String Ddo_tpuo_nome_Noresultsfound ;
      private String Ddo_tpuo_nome_Searchbuttontext ;
      private String Ddo_unidadeorganizacinal_arvore_Caption ;
      private String Ddo_unidadeorganizacinal_arvore_Tooltip ;
      private String Ddo_unidadeorganizacinal_arvore_Cls ;
      private String Ddo_unidadeorganizacinal_arvore_Filteredtext_set ;
      private String Ddo_unidadeorganizacinal_arvore_Selectedvalue_set ;
      private String Ddo_unidadeorganizacinal_arvore_Dropdownoptionstype ;
      private String Ddo_unidadeorganizacinal_arvore_Titlecontrolidtoreplace ;
      private String Ddo_unidadeorganizacinal_arvore_Filtertype ;
      private String Ddo_unidadeorganizacinal_arvore_Datalisttype ;
      private String Ddo_unidadeorganizacinal_arvore_Datalistproc ;
      private String Ddo_unidadeorganizacinal_arvore_Loadingdata ;
      private String Ddo_unidadeorganizacinal_arvore_Cleanfilter ;
      private String Ddo_unidadeorganizacinal_arvore_Noresultsfound ;
      private String Ddo_unidadeorganizacinal_arvore_Searchbuttontext ;
      private String Ddo_estado_uf_Caption ;
      private String Ddo_estado_uf_Tooltip ;
      private String Ddo_estado_uf_Cls ;
      private String Ddo_estado_uf_Filteredtext_set ;
      private String Ddo_estado_uf_Selectedvalue_set ;
      private String Ddo_estado_uf_Dropdownoptionstype ;
      private String Ddo_estado_uf_Titlecontrolidtoreplace ;
      private String Ddo_estado_uf_Sortedstatus ;
      private String Ddo_estado_uf_Filtertype ;
      private String Ddo_estado_uf_Datalisttype ;
      private String Ddo_estado_uf_Datalistproc ;
      private String Ddo_estado_uf_Sortasc ;
      private String Ddo_estado_uf_Sortdsc ;
      private String Ddo_estado_uf_Loadingdata ;
      private String Ddo_estado_uf_Cleanfilter ;
      private String Ddo_estado_uf_Noresultsfound ;
      private String Ddo_estado_uf_Searchbuttontext ;
      private String Ddo_unidadeorganizacional_ativo_Caption ;
      private String Ddo_unidadeorganizacional_ativo_Tooltip ;
      private String Ddo_unidadeorganizacional_ativo_Cls ;
      private String Ddo_unidadeorganizacional_ativo_Selectedvalue_set ;
      private String Ddo_unidadeorganizacional_ativo_Dropdownoptionstype ;
      private String Ddo_unidadeorganizacional_ativo_Titlecontrolidtoreplace ;
      private String Ddo_unidadeorganizacional_ativo_Sortedstatus ;
      private String Ddo_unidadeorganizacional_ativo_Datalisttype ;
      private String Ddo_unidadeorganizacional_ativo_Datalistfixedvalues ;
      private String Ddo_unidadeorganizacional_ativo_Sortasc ;
      private String Ddo_unidadeorganizacional_ativo_Sortdsc ;
      private String Ddo_unidadeorganizacional_ativo_Cleanfilter ;
      private String Ddo_unidadeorganizacional_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfunidadeorganizacional_nome_Internalname ;
      private String edtavTfunidadeorganizacional_nome_Jsonclick ;
      private String edtavTfunidadeorganizacional_nome_sel_Internalname ;
      private String edtavTfunidadeorganizacional_nome_sel_Jsonclick ;
      private String edtavTftpuo_nome_Internalname ;
      private String edtavTftpuo_nome_Jsonclick ;
      private String edtavTftpuo_nome_sel_Internalname ;
      private String edtavTftpuo_nome_sel_Jsonclick ;
      private String edtavTfunidadeorganizacinal_arvore_Internalname ;
      private String edtavTfunidadeorganizacinal_arvore_sel_Internalname ;
      private String edtavTfestado_uf_Internalname ;
      private String edtavTfestado_uf_Jsonclick ;
      private String edtavTfestado_uf_sel_Internalname ;
      private String edtavTfestado_uf_sel_Jsonclick ;
      private String edtavTfunidadeorganizacional_ativo_sel_Internalname ;
      private String edtavTfunidadeorganizacional_ativo_sel_Jsonclick ;
      private String edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tpuo_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_unidadeorganizacinal_arvoretitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_estado_uftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_unidadeorganizacional_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String A612UnidadeOrganizacional_Nome ;
      private String edtUnidadeOrganizacional_Nome_Internalname ;
      private String A610TpUo_Nome ;
      private String edtTpUo_Nome_Internalname ;
      private String A1174UnidadeOrganizacinal_Arvore ;
      private String edtUnidadeOrganizacinal_Arvore_Internalname ;
      private String A23Estado_UF ;
      private String edtEstado_UF_Internalname ;
      private String chkUnidadeOrganizacional_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ;
      private String lV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ;
      private String lV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ;
      private String lV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ;
      private String lV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ;
      private String lV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ;
      private String AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ;
      private String AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ;
      private String AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ;
      private String AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ;
      private String AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ;
      private String AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ;
      private String AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ;
      private String AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ;
      private String AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ;
      private String AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ;
      private String AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ;
      private String GXt_char1 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavUnidadeorganizacional_nome1_Internalname ;
      private String dynavUnidadeorganizacional_vinculada1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavUnidadeorganizacional_nome2_Internalname ;
      private String dynavUnidadeorganizacional_vinculada2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavUnidadeorganizacional_nome3_Internalname ;
      private String dynavUnidadeorganizacional_vinculada3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_unidadeorganizacional_nome_Internalname ;
      private String Ddo_tpuo_nome_Internalname ;
      private String Ddo_unidadeorganizacinal_arvore_Internalname ;
      private String Ddo_estado_uf_Internalname ;
      private String Ddo_unidadeorganizacional_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtUnidadeOrganizacional_Nome_Title ;
      private String edtTpUo_Nome_Title ;
      private String edtUnidadeOrganizacinal_Arvore_Title ;
      private String edtEstado_UF_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtUnidadeOrganizacional_Nome_Link ;
      private String edtTpUo_Nome_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblGeral_unidadeorganizacionaltitle_Internalname ;
      private String lblGeral_unidadeorganizacionaltitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavUnidadeorganizacional_nome1_Jsonclick ;
      private String dynavUnidadeorganizacional_vinculada1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavUnidadeorganizacional_nome2_Jsonclick ;
      private String dynavUnidadeorganizacional_vinculada2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavUnidadeorganizacional_nome3_Jsonclick ;
      private String dynavUnidadeorganizacional_vinculada3_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String bttBtnselectuo_Internalname ;
      private String bttBtnselectuo_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_81_fel_idx="0001" ;
      private String ROClassString ;
      private String edtUnidadeOrganizacional_Nome_Jsonclick ;
      private String edtTpUo_Nome_Jsonclick ;
      private String edtUnidadeOrganizacinal_Arvore_Jsonclick ;
      private String edtEstado_UF_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV20DynamicFiltersEnabled3 ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_unidadeorganizacional_nome_Includesortasc ;
      private bool Ddo_unidadeorganizacional_nome_Includesortdsc ;
      private bool Ddo_unidadeorganizacional_nome_Includefilter ;
      private bool Ddo_unidadeorganizacional_nome_Filterisrange ;
      private bool Ddo_unidadeorganizacional_nome_Includedatalist ;
      private bool Ddo_tpuo_nome_Includesortasc ;
      private bool Ddo_tpuo_nome_Includesortdsc ;
      private bool Ddo_tpuo_nome_Includefilter ;
      private bool Ddo_tpuo_nome_Filterisrange ;
      private bool Ddo_tpuo_nome_Includedatalist ;
      private bool Ddo_unidadeorganizacinal_arvore_Includesortasc ;
      private bool Ddo_unidadeorganizacinal_arvore_Includesortdsc ;
      private bool Ddo_unidadeorganizacinal_arvore_Includefilter ;
      private bool Ddo_unidadeorganizacinal_arvore_Filterisrange ;
      private bool Ddo_unidadeorganizacinal_arvore_Includedatalist ;
      private bool Ddo_estado_uf_Includesortasc ;
      private bool Ddo_estado_uf_Includesortdsc ;
      private bool Ddo_estado_uf_Includefilter ;
      private bool Ddo_estado_uf_Filterisrange ;
      private bool Ddo_estado_uf_Includedatalist ;
      private bool Ddo_unidadeorganizacional_ativo_Includesortasc ;
      private bool Ddo_unidadeorganizacional_ativo_Includesortdsc ;
      private bool Ddo_unidadeorganizacional_ativo_Includefilter ;
      private bool Ddo_unidadeorganizacional_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A629UnidadeOrganizacional_Ativo ;
      private bool AV71WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ;
      private bool AV75WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ;
      private bool n613UnidadeOrganizacional_Vinculada ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV25Update_IsBlob ;
      private bool AV26Delete_IsBlob ;
      private bool AV28Display_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV21DynamicFiltersSelector3 ;
      private String AV46ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace ;
      private String AV50ddo_TpUo_NomeTitleControlIdToReplace ;
      private String AV54ddo_UnidadeOrganizacinal_ArvoreTitleControlIdToReplace ;
      private String AV58ddo_Estado_UFTitleControlIdToReplace ;
      private String AV61ddo_UnidadeOrganizacional_AtivoTitleControlIdToReplace ;
      private String AV88Update_GXI ;
      private String AV89Delete_GXI ;
      private String AV90Display_GXI ;
      private String AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ;
      private String AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ;
      private String AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ;
      private String AV25Update ;
      private String AV26Delete ;
      private String AV28Display ;
      private IGxSession AV27Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox dynavUnidadeorganizacional_vinculada1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox dynavUnidadeorganizacional_vinculada2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox dynavUnidadeorganizacional_vinculada3 ;
      private GXCheckbox chkUnidadeOrganizacional_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00D02_A611UnidadeOrganizacional_Codigo ;
      private String[] H00D02_A612UnidadeOrganizacional_Nome ;
      private bool[] H00D02_A629UnidadeOrganizacional_Ativo ;
      private int[] H00D03_A611UnidadeOrganizacional_Codigo ;
      private String[] H00D03_A612UnidadeOrganizacional_Nome ;
      private bool[] H00D03_A629UnidadeOrganizacional_Ativo ;
      private int[] H00D04_A611UnidadeOrganizacional_Codigo ;
      private String[] H00D04_A612UnidadeOrganizacional_Nome ;
      private bool[] H00D04_A629UnidadeOrganizacional_Ativo ;
      private int[] H00D05_A611UnidadeOrganizacional_Codigo ;
      private int[] H00D05_A609TpUo_Codigo ;
      private bool[] H00D05_A629UnidadeOrganizacional_Ativo ;
      private String[] H00D05_A23Estado_UF ;
      private String[] H00D05_A610TpUo_Nome ;
      private String[] H00D05_A612UnidadeOrganizacional_Nome ;
      private int[] H00D05_A613UnidadeOrganizacional_Vinculada ;
      private bool[] H00D05_n613UnidadeOrganizacional_Vinculada ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43UnidadeOrganizacional_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47TpUo_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51UnidadeOrganizacinal_ArvoreTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV55Estado_UFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV59UnidadeOrganizacional_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV62DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 ;
   }

   public class wwgeral_unidadeorganizacional__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00D05( IGxContext context ,
                                             String AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ,
                                             String AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ,
                                             int AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ,
                                             bool AV71WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ,
                                             String AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ,
                                             String AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ,
                                             int AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ,
                                             bool AV75WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ,
                                             String AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ,
                                             String AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ,
                                             int AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ,
                                             String AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ,
                                             String AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ,
                                             String AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ,
                                             String AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ,
                                             String AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ,
                                             String AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ,
                                             short AV87WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             String A610TpUo_Nome ,
                                             String A23Estado_UF ,
                                             bool A629UnidadeOrganizacional_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             String AV84WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ,
                                             String AV83WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ,
                                             String A1174UnidadeOrganizacinal_Arvore )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[UnidadeOrganizacional_Codigo], T1.[TpUo_Codigo], T1.[UnidadeOrganizacional_Ativo], T1.[Estado_UF], T2.[TpUo_Nome], T1.[UnidadeOrganizacional_Nome], T1.[UnidadeOrganizacional_Vinculada] FROM ([Geral_UnidadeOrganizacional] T1 WITH (NOLOCK) INNER JOIN [Geral_tp_uo] T2 WITH (NOLOCK) ON T2.[TpUo_Codigo] = T1.[TpUo_Codigo])";
         if ( ( StringUtil.StrCmp(AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV71WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV71WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV72WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV75WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 + '%')";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV75WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV76WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like @lV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like @lV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] = @AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] = @AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TpUo_Nome] like @lV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TpUo_Nome] like @lV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TpUo_Nome] = @AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TpUo_Nome] = @AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Estado_UF] like @lV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Estado_UF] like @lV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Estado_UF] = @AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Estado_UF] = @AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV87WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Ativo] = 1)";
            }
         }
         if ( AV87WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[UnidadeOrganizacional_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[UnidadeOrganizacional_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[TpUo_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[TpUo_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Estado_UF]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Estado_UF] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[UnidadeOrganizacional_Ativo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[UnidadeOrganizacional_Ativo] DESC";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_H00D05(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (bool)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00D02 ;
          prmH00D02 = new Object[] {
          } ;
          Object[] prmH00D03 ;
          prmH00D03 = new Object[] {
          } ;
          Object[] prmH00D04 ;
          prmH00D04 = new Object[] {
          } ;
          Object[] prmH00D05 ;
          prmH00D05 = new Object[] {
          new Object[] {"@lV69WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV70WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV73WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV74WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV77WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV78WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV79WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV80WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV81WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV82WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV85WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV86WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel",SqlDbType.Char,2,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00D02", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Ativo] = 1 ORDER BY [UnidadeOrganizacional_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D02,0,0,true,false )
             ,new CursorDef("H00D03", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Ativo] = 1 ORDER BY [UnidadeOrganizacional_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D03,0,0,true,false )
             ,new CursorDef("H00D04", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Ativo] = 1 ORDER BY [UnidadeOrganizacional_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D04,0,0,true,false )
             ,new CursorDef("H00D05", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D05,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

}
