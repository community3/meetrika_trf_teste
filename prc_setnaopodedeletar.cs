/*
               File: PRC_SetNaoPodeDeletar
        Description: Set Nao Pode Deletar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:29.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_setnaopodedeletar : GXProcedure
   {
      public prc_setnaopodedeletar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_setnaopodedeletar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_TabelaNome ,
                           int aP1_Codigo ,
                           out bool aP2_PodeDeletar )
      {
         this.AV12TabelaNome = aP0_TabelaNome;
         this.AV13Codigo = aP1_Codigo;
         this.AV9PodeDeletar = false ;
         initialize();
         executePrivate();
         aP2_PodeDeletar=this.AV9PodeDeletar;
      }

      public bool executeUdp( String aP0_TabelaNome ,
                              int aP1_Codigo )
      {
         this.AV12TabelaNome = aP0_TabelaNome;
         this.AV13Codigo = aP1_Codigo;
         this.AV9PodeDeletar = false ;
         initialize();
         executePrivate();
         aP2_PodeDeletar=this.AV9PodeDeletar;
         return AV9PodeDeletar ;
      }

      public void executeSubmit( String aP0_TabelaNome ,
                                 int aP1_Codigo ,
                                 out bool aP2_PodeDeletar )
      {
         prc_setnaopodedeletar objprc_setnaopodedeletar;
         objprc_setnaopodedeletar = new prc_setnaopodedeletar();
         objprc_setnaopodedeletar.AV12TabelaNome = aP0_TabelaNome;
         objprc_setnaopodedeletar.AV13Codigo = aP1_Codigo;
         objprc_setnaopodedeletar.AV9PodeDeletar = false ;
         objprc_setnaopodedeletar.context.SetSubmitInitialConfig(context);
         objprc_setnaopodedeletar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_setnaopodedeletar);
         aP2_PodeDeletar=this.AV9PodeDeletar;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_setnaopodedeletar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9PodeDeletar = true;
         if ( StringUtil.StrCmp(AV12TabelaNome, "FnD") == 0 )
         {
            /* Using cursor P005F2 */
            pr_default.execute(0, new Object[] {AV13Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A368FuncaoDados_Codigo = P005F2_A368FuncaoDados_Codigo[0];
               A172Tabela_Codigo = P005F2_A172Tabela_Codigo[0];
               AV9PodeDeletar = false;
               pr_default.close(0);
               this.cleanup();
               if (true) return;
               pr_default.readNext(0);
            }
            pr_default.close(0);
         }
         else if ( StringUtil.StrCmp(AV12TabelaNome, "FnT") == 0 )
         {
            /* Using cursor P005F3 */
            pr_default.execute(1, new Object[] {AV13Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A165FuncaoAPF_Codigo = P005F3_A165FuncaoAPF_Codigo[0];
               /* Using cursor P005F4 */
               pr_default.execute(2, new Object[] {A165FuncaoAPF_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A364FuncaoAPFAtributos_AtributosCod = P005F4_A364FuncaoAPFAtributos_AtributosCod[0];
                  AV9PodeDeletar = false;
                  pr_default.close(2);
                  this.cleanup();
                  if (true) return;
                  pr_default.readNext(2);
               }
               pr_default.close(2);
               /* Using cursor P005F5 */
               pr_default.execute(3, new Object[] {A165FuncaoAPF_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A406FuncaoAPFEvidencia_Codigo = P005F5_A406FuncaoAPFEvidencia_Codigo[0];
                  AV9PodeDeletar = false;
                  pr_default.close(3);
                  this.cleanup();
                  if (true) return;
                  pr_default.readNext(3);
               }
               pr_default.close(3);
               /* Using cursor P005F6 */
               pr_default.execute(4, new Object[] {A165FuncaoAPF_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A161FuncaoUsuario_Codigo = P005F6_A161FuncaoUsuario_Codigo[0];
                  AV9PodeDeletar = false;
                  pr_default.close(4);
                  this.cleanup();
                  if (true) return;
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         else if ( StringUtil.StrCmp(AV12TabelaNome, "Atr") == 0 )
         {
            /* Using cursor P005F7 */
            pr_default.execute(5, new Object[] {AV13Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A364FuncaoAPFAtributos_AtributosCod = P005F7_A364FuncaoAPFAtributos_AtributosCod[0];
               A165FuncaoAPF_Codigo = P005F7_A165FuncaoAPF_Codigo[0];
               AV9PodeDeletar = false;
               pr_default.close(5);
               this.cleanup();
               if (true) return;
               pr_default.readNext(5);
            }
            pr_default.close(5);
         }
         else if ( StringUtil.StrCmp(AV12TabelaNome, "Tbl") == 0 )
         {
            /* Using cursor P005F8 */
            pr_default.execute(6, new Object[] {AV13Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A172Tabela_Codigo = P005F8_A172Tabela_Codigo[0];
               /* Using cursor P005F9 */
               pr_default.execute(7, new Object[] {A172Tabela_Codigo});
               while ( (pr_default.getStatus(7) != 101) )
               {
                  A356Atributos_TabelaCod = P005F9_A356Atributos_TabelaCod[0];
                  A176Atributos_Codigo = P005F9_A176Atributos_Codigo[0];
                  AV9PodeDeletar = false;
                  pr_default.close(7);
                  this.cleanup();
                  if (true) return;
                  pr_default.readNext(7);
               }
               pr_default.close(7);
               /* Using cursor P005F10 */
               pr_default.execute(8, new Object[] {A172Tabela_Codigo});
               while ( (pr_default.getStatus(8) != 101) )
               {
                  A368FuncaoDados_Codigo = P005F10_A368FuncaoDados_Codigo[0];
                  AV9PodeDeletar = false;
                  pr_default.close(8);
                  this.cleanup();
                  if (true) return;
                  pr_default.readNext(8);
               }
               pr_default.close(8);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(6);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005F2_A368FuncaoDados_Codigo = new int[1] ;
         P005F2_A172Tabela_Codigo = new int[1] ;
         P005F3_A165FuncaoAPF_Codigo = new int[1] ;
         P005F4_A165FuncaoAPF_Codigo = new int[1] ;
         P005F4_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P005F5_A165FuncaoAPF_Codigo = new int[1] ;
         P005F5_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         P005F6_A165FuncaoAPF_Codigo = new int[1] ;
         P005F6_A161FuncaoUsuario_Codigo = new int[1] ;
         P005F7_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P005F7_A165FuncaoAPF_Codigo = new int[1] ;
         P005F8_A172Tabela_Codigo = new int[1] ;
         P005F9_A356Atributos_TabelaCod = new int[1] ;
         P005F9_A176Atributos_Codigo = new int[1] ;
         P005F10_A172Tabela_Codigo = new int[1] ;
         P005F10_A368FuncaoDados_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_setnaopodedeletar__default(),
            new Object[][] {
                new Object[] {
               P005F2_A368FuncaoDados_Codigo, P005F2_A172Tabela_Codigo
               }
               , new Object[] {
               P005F3_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               P005F4_A165FuncaoAPF_Codigo, P005F4_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               P005F5_A165FuncaoAPF_Codigo, P005F5_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               P005F6_A165FuncaoAPF_Codigo, P005F6_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               P005F7_A364FuncaoAPFAtributos_AtributosCod, P005F7_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               P005F8_A172Tabela_Codigo
               }
               , new Object[] {
               P005F9_A356Atributos_TabelaCod, P005F9_A176Atributos_Codigo
               }
               , new Object[] {
               P005F10_A172Tabela_Codigo, P005F10_A368FuncaoDados_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV13Codigo ;
      private int A368FuncaoDados_Codigo ;
      private int A172Tabela_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int A406FuncaoAPFEvidencia_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private int A356Atributos_TabelaCod ;
      private int A176Atributos_Codigo ;
      private String AV12TabelaNome ;
      private String scmdbuf ;
      private bool AV9PodeDeletar ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P005F2_A368FuncaoDados_Codigo ;
      private int[] P005F2_A172Tabela_Codigo ;
      private int[] P005F3_A165FuncaoAPF_Codigo ;
      private int[] P005F4_A165FuncaoAPF_Codigo ;
      private int[] P005F4_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P005F5_A165FuncaoAPF_Codigo ;
      private int[] P005F5_A406FuncaoAPFEvidencia_Codigo ;
      private int[] P005F6_A165FuncaoAPF_Codigo ;
      private int[] P005F6_A161FuncaoUsuario_Codigo ;
      private int[] P005F7_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P005F7_A165FuncaoAPF_Codigo ;
      private int[] P005F8_A172Tabela_Codigo ;
      private int[] P005F9_A356Atributos_TabelaCod ;
      private int[] P005F9_A176Atributos_Codigo ;
      private int[] P005F10_A172Tabela_Codigo ;
      private int[] P005F10_A368FuncaoDados_Codigo ;
      private bool aP2_PodeDeletar ;
   }

   public class prc_setnaopodedeletar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005F2 ;
          prmP005F2 = new Object[] {
          new Object[] {"@AV13Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005F3 ;
          prmP005F3 = new Object[] {
          new Object[] {"@AV13Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005F4 ;
          prmP005F4 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005F5 ;
          prmP005F5 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005F6 ;
          prmP005F6 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005F7 ;
          prmP005F7 = new Object[] {
          new Object[] {"@AV13Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005F8 ;
          prmP005F8 = new Object[] {
          new Object[] {"@AV13Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005F9 ;
          prmP005F9 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005F10 ;
          prmP005F10 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005F2", "SELECT TOP 1 [FuncaoDados_Codigo], [Tabela_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV13Codigo ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005F2,1,0,false,true )
             ,new CursorDef("P005F3", "SELECT [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @AV13Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005F3,1,0,true,true )
             ,new CursorDef("P005F4", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005F4,1,0,false,true )
             ,new CursorDef("P005F5", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005F5,1,0,false,true )
             ,new CursorDef("P005F6", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoUsuario_Codigo] FROM [FuncoesUsuarioFuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005F6,1,0,false,true )
             ,new CursorDef("P005F7", "SELECT TOP 1 [FuncaoAPFAtributos_AtributosCod], [FuncaoAPF_Codigo] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPFAtributos_AtributosCod] = @AV13Codigo ORDER BY [FuncaoAPFAtributos_AtributosCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005F7,1,0,false,true )
             ,new CursorDef("P005F8", "SELECT [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @AV13Codigo ORDER BY [Tabela_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005F8,1,0,true,true )
             ,new CursorDef("P005F9", "SELECT TOP 1 [Atributos_TabelaCod], [Atributos_Codigo] FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_TabelaCod] = @Tabela_Codigo ORDER BY [Atributos_TabelaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005F9,1,0,false,true )
             ,new CursorDef("P005F10", "SELECT TOP 1 [Tabela_Codigo], [FuncaoDados_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_Codigo ORDER BY [Tabela_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005F10,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
