/*
               File: Email_Instancia_Destinatarios_BC
        Description: Email_Instancia_Destinatarios
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:51.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class email_instancia_destinatarios_bc : GXHttpHandler, IGxSilentTrn
   {
      public email_instancia_destinatarios_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public email_instancia_destinatarios_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow46185( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey46185( ) ;
         standaloneModal( ) ;
         AddRow46185( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
               Z1667Email_Instancia_Destinatarios_Guid = (Guid)(A1667Email_Instancia_Destinatarios_Guid);
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_460( )
      {
         BeforeValidate46185( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls46185( ) ;
            }
            else
            {
               CheckExtendedTable46185( ) ;
               if ( AnyError == 0 )
               {
                  ZM46185( 5) ;
               }
               CloseExtendedTableCursors46185( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM46185( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z1675Email_Instancia_Destinatarios_Email = A1675Email_Instancia_Destinatarios_Email;
            Z1676Email_Instancia_Destinatarios_IsEnviado = A1676Email_Instancia_Destinatarios_IsEnviado;
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -4 )
         {
            Z1667Email_Instancia_Destinatarios_Guid = (Guid)(A1667Email_Instancia_Destinatarios_Guid);
            Z1675Email_Instancia_Destinatarios_Email = A1675Email_Instancia_Destinatarios_Email;
            Z1676Email_Instancia_Destinatarios_IsEnviado = A1676Email_Instancia_Destinatarios_IsEnviado;
            Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load46185( )
      {
         /* Using cursor BC00465 */
         pr_default.execute(3, new Object[] {A1666Email_Instancia_Guid, A1667Email_Instancia_Destinatarios_Guid});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound185 = 1;
            A1675Email_Instancia_Destinatarios_Email = BC00465_A1675Email_Instancia_Destinatarios_Email[0];
            A1676Email_Instancia_Destinatarios_IsEnviado = BC00465_A1676Email_Instancia_Destinatarios_IsEnviado[0];
            ZM46185( -4) ;
         }
         pr_default.close(3);
         OnLoadActions46185( ) ;
      }

      protected void OnLoadActions46185( )
      {
      }

      protected void CheckExtendedTable46185( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00464 */
         pr_default.execute(2, new Object[] {A1666Email_Instancia_Guid});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Email_Instancia'.", "ForeignKeyNotFound", 1, "EMAIL_INSTANCIA_GUID");
            AnyError = 1;
         }
         pr_default.close(2);
         if ( ! ( GxRegex.IsMatch(A1675Email_Instancia_Destinatarios_Email,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") ) )
         {
            GX_msglist.addItem("O valor de Email_Instancia_Destinatarios_Email n�o coincide com o padr�o especificado", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors46185( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey46185( )
      {
         /* Using cursor BC00466 */
         pr_default.execute(4, new Object[] {A1666Email_Instancia_Guid, A1667Email_Instancia_Destinatarios_Guid});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound185 = 1;
         }
         else
         {
            RcdFound185 = 0;
         }
         pr_default.close(4);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00463 */
         pr_default.execute(1, new Object[] {A1666Email_Instancia_Guid, A1667Email_Instancia_Destinatarios_Guid});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM46185( 4) ;
            RcdFound185 = 1;
            A1667Email_Instancia_Destinatarios_Guid = (Guid)((Guid)(BC00463_A1667Email_Instancia_Destinatarios_Guid[0]));
            A1675Email_Instancia_Destinatarios_Email = BC00463_A1675Email_Instancia_Destinatarios_Email[0];
            A1676Email_Instancia_Destinatarios_IsEnviado = BC00463_A1676Email_Instancia_Destinatarios_IsEnviado[0];
            A1666Email_Instancia_Guid = (Guid)((Guid)(BC00463_A1666Email_Instancia_Guid[0]));
            Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
            Z1667Email_Instancia_Destinatarios_Guid = (Guid)(A1667Email_Instancia_Destinatarios_Guid);
            sMode185 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load46185( ) ;
            if ( AnyError == 1 )
            {
               RcdFound185 = 0;
               InitializeNonKey46185( ) ;
            }
            Gx_mode = sMode185;
         }
         else
         {
            RcdFound185 = 0;
            InitializeNonKey46185( ) ;
            sMode185 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode185;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey46185( ) ;
         if ( RcdFound185 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_460( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency46185( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00462 */
            pr_default.execute(0, new Object[] {A1666Email_Instancia_Guid, A1667Email_Instancia_Destinatarios_Guid});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Email_Instancia_Destinatarios"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1675Email_Instancia_Destinatarios_Email, BC00462_A1675Email_Instancia_Destinatarios_Email[0]) != 0 ) || ( Z1676Email_Instancia_Destinatarios_IsEnviado != BC00462_A1676Email_Instancia_Destinatarios_IsEnviado[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Email_Instancia_Destinatarios"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert46185( )
      {
         BeforeValidate46185( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable46185( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM46185( 0) ;
            CheckOptimisticConcurrency46185( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm46185( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert46185( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00467 */
                     pr_default.execute(5, new Object[] {A1667Email_Instancia_Destinatarios_Guid, A1675Email_Instancia_Destinatarios_Email, A1676Email_Instancia_Destinatarios_IsEnviado, A1666Email_Instancia_Guid});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("Email_Instancia_Destinatarios") ;
                     if ( (pr_default.getStatus(5) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load46185( ) ;
            }
            EndLevel46185( ) ;
         }
         CloseExtendedTableCursors46185( ) ;
      }

      protected void Update46185( )
      {
         BeforeValidate46185( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable46185( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency46185( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm46185( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate46185( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00468 */
                     pr_default.execute(6, new Object[] {A1675Email_Instancia_Destinatarios_Email, A1676Email_Instancia_Destinatarios_IsEnviado, A1666Email_Instancia_Guid, A1667Email_Instancia_Destinatarios_Guid});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Email_Instancia_Destinatarios") ;
                     if ( (pr_default.getStatus(6) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Email_Instancia_Destinatarios"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate46185( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel46185( ) ;
         }
         CloseExtendedTableCursors46185( ) ;
      }

      protected void DeferredUpdate46185( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate46185( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency46185( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls46185( ) ;
            AfterConfirm46185( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete46185( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC00469 */
                  pr_default.execute(7, new Object[] {A1666Email_Instancia_Guid, A1667Email_Instancia_Destinatarios_Guid});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("Email_Instancia_Destinatarios") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode185 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel46185( ) ;
         Gx_mode = sMode185;
      }

      protected void OnDeleteControls46185( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel46185( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete46185( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart46185( )
      {
         /* Using cursor BC004610 */
         pr_default.execute(8, new Object[] {A1666Email_Instancia_Guid, A1667Email_Instancia_Destinatarios_Guid});
         RcdFound185 = 0;
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound185 = 1;
            A1667Email_Instancia_Destinatarios_Guid = (Guid)((Guid)(BC004610_A1667Email_Instancia_Destinatarios_Guid[0]));
            A1675Email_Instancia_Destinatarios_Email = BC004610_A1675Email_Instancia_Destinatarios_Email[0];
            A1676Email_Instancia_Destinatarios_IsEnviado = BC004610_A1676Email_Instancia_Destinatarios_IsEnviado[0];
            A1666Email_Instancia_Guid = (Guid)((Guid)(BC004610_A1666Email_Instancia_Guid[0]));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext46185( )
      {
         /* Scan next routine */
         pr_default.readNext(8);
         RcdFound185 = 0;
         ScanKeyLoad46185( ) ;
      }

      protected void ScanKeyLoad46185( )
      {
         sMode185 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound185 = 1;
            A1667Email_Instancia_Destinatarios_Guid = (Guid)((Guid)(BC004610_A1667Email_Instancia_Destinatarios_Guid[0]));
            A1675Email_Instancia_Destinatarios_Email = BC004610_A1675Email_Instancia_Destinatarios_Email[0];
            A1676Email_Instancia_Destinatarios_IsEnviado = BC004610_A1676Email_Instancia_Destinatarios_IsEnviado[0];
            A1666Email_Instancia_Guid = (Guid)((Guid)(BC004610_A1666Email_Instancia_Guid[0]));
         }
         Gx_mode = sMode185;
      }

      protected void ScanKeyEnd46185( )
      {
         pr_default.close(8);
      }

      protected void AfterConfirm46185( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert46185( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate46185( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete46185( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete46185( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate46185( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes46185( )
      {
      }

      protected void AddRow46185( )
      {
         VarsToRow185( bcEmail_Instancia_Destinatarios) ;
      }

      protected void ReadRow46185( )
      {
         RowToVars185( bcEmail_Instancia_Destinatarios, 1) ;
      }

      protected void InitializeNonKey46185( )
      {
         A1675Email_Instancia_Destinatarios_Email = "";
         A1676Email_Instancia_Destinatarios_IsEnviado = false;
         Z1675Email_Instancia_Destinatarios_Email = "";
         Z1676Email_Instancia_Destinatarios_IsEnviado = false;
      }

      protected void InitAll46185( )
      {
         A1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         A1667Email_Instancia_Destinatarios_Guid = (Guid)(System.Guid.Empty);
         InitializeNonKey46185( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow185( SdtEmail_Instancia_Destinatarios obj185 )
      {
         obj185.gxTpr_Mode = Gx_mode;
         obj185.gxTpr_Email_instancia_destinatarios_email = A1675Email_Instancia_Destinatarios_Email;
         obj185.gxTpr_Email_instancia_destinatarios_isenviado = A1676Email_Instancia_Destinatarios_IsEnviado;
         obj185.gxTpr_Email_instancia_guid = (Guid)(A1666Email_Instancia_Guid);
         obj185.gxTpr_Email_instancia_destinatarios_guid = (Guid)(A1667Email_Instancia_Destinatarios_Guid);
         obj185.gxTpr_Email_instancia_guid_Z = (Guid)(Z1666Email_Instancia_Guid);
         obj185.gxTpr_Email_instancia_destinatarios_guid_Z = (Guid)(Z1667Email_Instancia_Destinatarios_Guid);
         obj185.gxTpr_Email_instancia_destinatarios_email_Z = Z1675Email_Instancia_Destinatarios_Email;
         obj185.gxTpr_Email_instancia_destinatarios_isenviado_Z = Z1676Email_Instancia_Destinatarios_IsEnviado;
         obj185.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow185( SdtEmail_Instancia_Destinatarios obj185 )
      {
         obj185.gxTpr_Email_instancia_guid = (Guid)(A1666Email_Instancia_Guid);
         obj185.gxTpr_Email_instancia_destinatarios_guid = (Guid)(A1667Email_Instancia_Destinatarios_Guid);
         return  ;
      }

      public void RowToVars185( SdtEmail_Instancia_Destinatarios obj185 ,
                                int forceLoad )
      {
         Gx_mode = obj185.gxTpr_Mode;
         A1675Email_Instancia_Destinatarios_Email = obj185.gxTpr_Email_instancia_destinatarios_email;
         A1676Email_Instancia_Destinatarios_IsEnviado = obj185.gxTpr_Email_instancia_destinatarios_isenviado;
         A1666Email_Instancia_Guid = (Guid)(obj185.gxTpr_Email_instancia_guid);
         A1667Email_Instancia_Destinatarios_Guid = (Guid)(obj185.gxTpr_Email_instancia_destinatarios_guid);
         Z1666Email_Instancia_Guid = (Guid)(obj185.gxTpr_Email_instancia_guid_Z);
         Z1667Email_Instancia_Destinatarios_Guid = (Guid)(obj185.gxTpr_Email_instancia_destinatarios_guid_Z);
         Z1675Email_Instancia_Destinatarios_Email = obj185.gxTpr_Email_instancia_destinatarios_email_Z;
         Z1676Email_Instancia_Destinatarios_IsEnviado = obj185.gxTpr_Email_instancia_destinatarios_isenviado_Z;
         Gx_mode = obj185.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1666Email_Instancia_Guid = (Guid)((Guid)getParm(obj,0));
         A1667Email_Instancia_Destinatarios_Guid = (Guid)((Guid)getParm(obj,1));
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey46185( ) ;
         ScanKeyStart46185( ) ;
         if ( RcdFound185 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004611 */
            pr_default.execute(9, new Object[] {A1666Email_Instancia_Guid});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Email_Instancia'.", "ForeignKeyNotFound", 1, "EMAIL_INSTANCIA_GUID");
               AnyError = 1;
            }
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
            Z1667Email_Instancia_Destinatarios_Guid = (Guid)(A1667Email_Instancia_Destinatarios_Guid);
         }
         ZM46185( -4) ;
         OnLoadActions46185( ) ;
         AddRow46185( ) ;
         ScanKeyEnd46185( ) ;
         if ( RcdFound185 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars185( bcEmail_Instancia_Destinatarios, 0) ;
         ScanKeyStart46185( ) ;
         if ( RcdFound185 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004611 */
            pr_default.execute(9, new Object[] {A1666Email_Instancia_Guid});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Email_Instancia'.", "ForeignKeyNotFound", 1, "EMAIL_INSTANCIA_GUID");
               AnyError = 1;
            }
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
            Z1667Email_Instancia_Destinatarios_Guid = (Guid)(A1667Email_Instancia_Destinatarios_Guid);
         }
         ZM46185( -4) ;
         OnLoadActions46185( ) ;
         AddRow46185( ) ;
         ScanKeyEnd46185( ) ;
         if ( RcdFound185 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars185( bcEmail_Instancia_Destinatarios, 0) ;
         nKeyPressed = 1;
         GetKey46185( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert46185( ) ;
         }
         else
         {
            if ( RcdFound185 == 1 )
            {
               if ( ( A1666Email_Instancia_Guid != Z1666Email_Instancia_Guid ) || ( A1667Email_Instancia_Destinatarios_Guid != Z1667Email_Instancia_Destinatarios_Guid ) )
               {
                  A1666Email_Instancia_Guid = (Guid)(Z1666Email_Instancia_Guid);
                  A1667Email_Instancia_Destinatarios_Guid = (Guid)(Z1667Email_Instancia_Destinatarios_Guid);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update46185( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A1666Email_Instancia_Guid != Z1666Email_Instancia_Guid ) || ( A1667Email_Instancia_Destinatarios_Guid != Z1667Email_Instancia_Destinatarios_Guid ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert46185( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert46185( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow185( bcEmail_Instancia_Destinatarios) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars185( bcEmail_Instancia_Destinatarios, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey46185( ) ;
         if ( RcdFound185 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A1666Email_Instancia_Guid != Z1666Email_Instancia_Guid ) || ( A1667Email_Instancia_Destinatarios_Guid != Z1667Email_Instancia_Destinatarios_Guid ) )
            {
               A1666Email_Instancia_Guid = (Guid)(Z1666Email_Instancia_Guid);
               A1667Email_Instancia_Destinatarios_Guid = (Guid)(Z1667Email_Instancia_Destinatarios_Guid);
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A1666Email_Instancia_Guid != Z1666Email_Instancia_Guid ) || ( A1667Email_Instancia_Destinatarios_Guid != Z1667Email_Instancia_Destinatarios_Guid ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         context.RollbackDataStores( "Email_Instancia_Destinatarios_BC");
         VarsToRow185( bcEmail_Instancia_Destinatarios) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcEmail_Instancia_Destinatarios.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcEmail_Instancia_Destinatarios.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcEmail_Instancia_Destinatarios )
         {
            bcEmail_Instancia_Destinatarios = (SdtEmail_Instancia_Destinatarios)(sdt);
            if ( StringUtil.StrCmp(bcEmail_Instancia_Destinatarios.gxTpr_Mode, "") == 0 )
            {
               bcEmail_Instancia_Destinatarios.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow185( bcEmail_Instancia_Destinatarios) ;
            }
            else
            {
               RowToVars185( bcEmail_Instancia_Destinatarios, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcEmail_Instancia_Destinatarios.gxTpr_Mode, "") == 0 )
            {
               bcEmail_Instancia_Destinatarios.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars185( bcEmail_Instancia_Destinatarios, 1) ;
         return  ;
      }

      public SdtEmail_Instancia_Destinatarios Email_Instancia_Destinatarios_BC
      {
         get {
            return bcEmail_Instancia_Destinatarios ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         A1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         Z1667Email_Instancia_Destinatarios_Guid = (Guid)(System.Guid.Empty);
         A1667Email_Instancia_Destinatarios_Guid = (Guid)(System.Guid.Empty);
         Z1675Email_Instancia_Destinatarios_Email = "";
         A1675Email_Instancia_Destinatarios_Email = "";
         BC00465_A1667Email_Instancia_Destinatarios_Guid = new Guid[] {System.Guid.Empty} ;
         BC00465_A1675Email_Instancia_Destinatarios_Email = new String[] {""} ;
         BC00465_A1676Email_Instancia_Destinatarios_IsEnviado = new bool[] {false} ;
         BC00465_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         BC00464_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         BC00466_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         BC00466_A1667Email_Instancia_Destinatarios_Guid = new Guid[] {System.Guid.Empty} ;
         BC00463_A1667Email_Instancia_Destinatarios_Guid = new Guid[] {System.Guid.Empty} ;
         BC00463_A1675Email_Instancia_Destinatarios_Email = new String[] {""} ;
         BC00463_A1676Email_Instancia_Destinatarios_IsEnviado = new bool[] {false} ;
         BC00463_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         sMode185 = "";
         BC00462_A1667Email_Instancia_Destinatarios_Guid = new Guid[] {System.Guid.Empty} ;
         BC00462_A1675Email_Instancia_Destinatarios_Email = new String[] {""} ;
         BC00462_A1676Email_Instancia_Destinatarios_IsEnviado = new bool[] {false} ;
         BC00462_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         BC004610_A1667Email_Instancia_Destinatarios_Guid = new Guid[] {System.Guid.Empty} ;
         BC004610_A1675Email_Instancia_Destinatarios_Email = new String[] {""} ;
         BC004610_A1676Email_Instancia_Destinatarios_IsEnviado = new bool[] {false} ;
         BC004610_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC004611_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.email_instancia_destinatarios_bc__default(),
            new Object[][] {
                new Object[] {
               BC00462_A1667Email_Instancia_Destinatarios_Guid, BC00462_A1675Email_Instancia_Destinatarios_Email, BC00462_A1676Email_Instancia_Destinatarios_IsEnviado, BC00462_A1666Email_Instancia_Guid
               }
               , new Object[] {
               BC00463_A1667Email_Instancia_Destinatarios_Guid, BC00463_A1675Email_Instancia_Destinatarios_Email, BC00463_A1676Email_Instancia_Destinatarios_IsEnviado, BC00463_A1666Email_Instancia_Guid
               }
               , new Object[] {
               BC00464_A1666Email_Instancia_Guid
               }
               , new Object[] {
               BC00465_A1667Email_Instancia_Destinatarios_Guid, BC00465_A1675Email_Instancia_Destinatarios_Email, BC00465_A1676Email_Instancia_Destinatarios_IsEnviado, BC00465_A1666Email_Instancia_Guid
               }
               , new Object[] {
               BC00466_A1666Email_Instancia_Guid, BC00466_A1667Email_Instancia_Destinatarios_Guid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004610_A1667Email_Instancia_Destinatarios_Guid, BC004610_A1675Email_Instancia_Destinatarios_Email, BC004610_A1676Email_Instancia_Destinatarios_IsEnviado, BC004610_A1666Email_Instancia_Guid
               }
               , new Object[] {
               BC004611_A1666Email_Instancia_Guid
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound185 ;
      private int trnEnded ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode185 ;
      private bool Z1676Email_Instancia_Destinatarios_IsEnviado ;
      private bool A1676Email_Instancia_Destinatarios_IsEnviado ;
      private String Z1675Email_Instancia_Destinatarios_Email ;
      private String A1675Email_Instancia_Destinatarios_Email ;
      private Guid Z1666Email_Instancia_Guid ;
      private Guid A1666Email_Instancia_Guid ;
      private Guid Z1667Email_Instancia_Destinatarios_Guid ;
      private Guid A1667Email_Instancia_Destinatarios_Guid ;
      private SdtEmail_Instancia_Destinatarios bcEmail_Instancia_Destinatarios ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private Guid[] BC00465_A1667Email_Instancia_Destinatarios_Guid ;
      private String[] BC00465_A1675Email_Instancia_Destinatarios_Email ;
      private bool[] BC00465_A1676Email_Instancia_Destinatarios_IsEnviado ;
      private Guid[] BC00465_A1666Email_Instancia_Guid ;
      private Guid[] BC00464_A1666Email_Instancia_Guid ;
      private Guid[] BC00466_A1666Email_Instancia_Guid ;
      private Guid[] BC00466_A1667Email_Instancia_Destinatarios_Guid ;
      private Guid[] BC00463_A1667Email_Instancia_Destinatarios_Guid ;
      private String[] BC00463_A1675Email_Instancia_Destinatarios_Email ;
      private bool[] BC00463_A1676Email_Instancia_Destinatarios_IsEnviado ;
      private Guid[] BC00463_A1666Email_Instancia_Guid ;
      private Guid[] BC00462_A1667Email_Instancia_Destinatarios_Guid ;
      private String[] BC00462_A1675Email_Instancia_Destinatarios_Email ;
      private bool[] BC00462_A1676Email_Instancia_Destinatarios_IsEnviado ;
      private Guid[] BC00462_A1666Email_Instancia_Guid ;
      private Guid[] BC004610_A1667Email_Instancia_Destinatarios_Guid ;
      private String[] BC004610_A1675Email_Instancia_Destinatarios_Email ;
      private bool[] BC004610_A1676Email_Instancia_Destinatarios_IsEnviado ;
      private Guid[] BC004610_A1666Email_Instancia_Guid ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private Guid[] BC004611_A1666Email_Instancia_Guid ;
   }

   public class email_instancia_destinatarios_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00465 ;
          prmBC00465 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Destinatarios_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00464 ;
          prmBC00464 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00466 ;
          prmBC00466 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Destinatarios_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00463 ;
          prmBC00463 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Destinatarios_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00462 ;
          prmBC00462 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Destinatarios_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00467 ;
          prmBC00467 = new Object[] {
          new Object[] {"@Email_Instancia_Destinatarios_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Destinatarios_Email",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Email_Instancia_Destinatarios_IsEnviado",SqlDbType.Bit,4,0} ,
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00468 ;
          prmBC00468 = new Object[] {
          new Object[] {"@Email_Instancia_Destinatarios_Email",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Email_Instancia_Destinatarios_IsEnviado",SqlDbType.Bit,4,0} ,
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Destinatarios_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00469 ;
          prmBC00469 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Destinatarios_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC004610 ;
          prmBC004610 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Destinatarios_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC004611 ;
          prmBC004611 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00462", "SELECT [Email_Instancia_Destinatarios_Guid], [Email_Instancia_Destinatarios_Email], [Email_Instancia_Destinatarios_IsEnviado], [Email_Instancia_Guid] FROM [Email_Instancia_Destinatarios] WITH (UPDLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid AND [Email_Instancia_Destinatarios_Guid] = @Email_Instancia_Destinatarios_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00462,1,0,true,false )
             ,new CursorDef("BC00463", "SELECT [Email_Instancia_Destinatarios_Guid], [Email_Instancia_Destinatarios_Email], [Email_Instancia_Destinatarios_IsEnviado], [Email_Instancia_Guid] FROM [Email_Instancia_Destinatarios] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid AND [Email_Instancia_Destinatarios_Guid] = @Email_Instancia_Destinatarios_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00463,1,0,true,false )
             ,new CursorDef("BC00464", "SELECT [Email_Instancia_Guid] FROM [Email_Instancia] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00464,1,0,true,false )
             ,new CursorDef("BC00465", "SELECT TM1.[Email_Instancia_Destinatarios_Guid], TM1.[Email_Instancia_Destinatarios_Email], TM1.[Email_Instancia_Destinatarios_IsEnviado], TM1.[Email_Instancia_Guid] FROM [Email_Instancia_Destinatarios] TM1 WITH (NOLOCK) WHERE TM1.[Email_Instancia_Guid] = @Email_Instancia_Guid and TM1.[Email_Instancia_Destinatarios_Guid] = @Email_Instancia_Destinatarios_Guid ORDER BY TM1.[Email_Instancia_Guid], TM1.[Email_Instancia_Destinatarios_Guid]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00465,100,0,true,false )
             ,new CursorDef("BC00466", "SELECT [Email_Instancia_Guid], [Email_Instancia_Destinatarios_Guid] FROM [Email_Instancia_Destinatarios] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid AND [Email_Instancia_Destinatarios_Guid] = @Email_Instancia_Destinatarios_Guid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00466,1,0,true,false )
             ,new CursorDef("BC00467", "INSERT INTO [Email_Instancia_Destinatarios]([Email_Instancia_Destinatarios_Guid], [Email_Instancia_Destinatarios_Email], [Email_Instancia_Destinatarios_IsEnviado], [Email_Instancia_Guid]) VALUES(@Email_Instancia_Destinatarios_Guid, @Email_Instancia_Destinatarios_Email, @Email_Instancia_Destinatarios_IsEnviado, @Email_Instancia_Guid)", GxErrorMask.GX_NOMASK,prmBC00467)
             ,new CursorDef("BC00468", "UPDATE [Email_Instancia_Destinatarios] SET [Email_Instancia_Destinatarios_Email]=@Email_Instancia_Destinatarios_Email, [Email_Instancia_Destinatarios_IsEnviado]=@Email_Instancia_Destinatarios_IsEnviado  WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid AND [Email_Instancia_Destinatarios_Guid] = @Email_Instancia_Destinatarios_Guid", GxErrorMask.GX_NOMASK,prmBC00468)
             ,new CursorDef("BC00469", "DELETE FROM [Email_Instancia_Destinatarios]  WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid AND [Email_Instancia_Destinatarios_Guid] = @Email_Instancia_Destinatarios_Guid", GxErrorMask.GX_NOMASK,prmBC00469)
             ,new CursorDef("BC004610", "SELECT TM1.[Email_Instancia_Destinatarios_Guid], TM1.[Email_Instancia_Destinatarios_Email], TM1.[Email_Instancia_Destinatarios_IsEnviado], TM1.[Email_Instancia_Guid] FROM [Email_Instancia_Destinatarios] TM1 WITH (NOLOCK) WHERE TM1.[Email_Instancia_Guid] = @Email_Instancia_Guid and TM1.[Email_Instancia_Destinatarios_Guid] = @Email_Instancia_Destinatarios_Guid ORDER BY TM1.[Email_Instancia_Guid], TM1.[Email_Instancia_Destinatarios_Guid]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004610,100,0,true,false )
             ,new CursorDef("BC004611", "SELECT [Email_Instancia_Guid] FROM [Email_Instancia] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004611,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((Guid[]) buf[3])[0] = rslt.getGuid(4) ;
                return;
             case 1 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((Guid[]) buf[3])[0] = rslt.getGuid(4) ;
                return;
             case 2 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
             case 3 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((Guid[]) buf[3])[0] = rslt.getGuid(4) ;
                return;
             case 4 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
                return;
             case 8 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((Guid[]) buf[3])[0] = rslt.getGuid(4) ;
                return;
             case 9 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (Guid)parms[0]);
                stmt.SetParameter(2, (Guid)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (Guid)parms[0]);
                stmt.SetParameter(2, (Guid)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (Guid)parms[0]);
                stmt.SetParameter(2, (Guid)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (Guid)parms[0]);
                stmt.SetParameter(2, (Guid)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (Guid)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                stmt.SetParameter(4, (Guid)parms[3]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (Guid)parms[2]);
                stmt.SetParameter(4, (Guid)parms[3]);
                return;
             case 7 :
                stmt.SetParameter(1, (Guid)parms[0]);
                stmt.SetParameter(2, (Guid)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (Guid)parms[0]);
                stmt.SetParameter(2, (Guid)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
       }
    }

 }

}
