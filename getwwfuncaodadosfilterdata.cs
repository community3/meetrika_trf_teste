/*
               File: GetWWFuncaoDadosFilterData
        Description: Get WWFuncao Dados Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:33:49.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwfuncaodadosfilterdata : GXProcedure
   {
      public getwwfuncaodadosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwfuncaodadosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwfuncaodadosfilterdata objgetwwfuncaodadosfilterdata;
         objgetwwfuncaodadosfilterdata = new getwwfuncaodadosfilterdata();
         objgetwwfuncaodadosfilterdata.AV26DDOName = aP0_DDOName;
         objgetwwfuncaodadosfilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetwwfuncaodadosfilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetwwfuncaodadosfilterdata.AV30OptionsJson = "" ;
         objgetwwfuncaodadosfilterdata.AV33OptionsDescJson = "" ;
         objgetwwfuncaodadosfilterdata.AV35OptionIndexesJson = "" ;
         objgetwwfuncaodadosfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwfuncaodadosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwfuncaodadosfilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwfuncaodadosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_FUNCAODADOS_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAODADOS_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("WWFuncaoDadosGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWFuncaoDadosGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("WWFuncaoDadosGridState"), "");
         }
         AV59GXV1 = 1;
         while ( AV59GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV59GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "FUNCAODADOS_SISTEMAAREACOD") == 0 )
            {
               AV42FuncaoDados_SistemaAreaCod = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME") == 0 )
            {
               AV10TFFuncaoDados_Nome = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME_SEL") == 0 )
            {
               AV11TFFuncaoDados_Nome_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_TIPO_SEL") == 0 )
            {
               AV12TFFuncaoDados_Tipo_SelsJson = AV40GridStateFilterValue.gxTpr_Value;
               AV13TFFuncaoDados_Tipo_Sels.FromJSonString(AV12TFFuncaoDados_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_COMPLEXIDADE_SEL") == 0 )
            {
               AV14TFFuncaoDados_Complexidade_SelsJson = AV40GridStateFilterValue.gxTpr_Value;
               AV15TFFuncaoDados_Complexidade_Sels.FromJSonString(AV14TFFuncaoDados_Complexidade_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_DER") == 0 )
            {
               AV16TFFuncaoDados_DER = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV17TFFuncaoDados_DER_To = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_RLR") == 0 )
            {
               AV18TFFuncaoDados_RLR = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV19TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_PF") == 0 )
            {
               AV20TFFuncaoDados_PF = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, ".");
               AV21TFFuncaoDados_PF_To = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_ATIVO_SEL") == 0 )
            {
               AV22TFFuncaoDados_Ativo_SelsJson = AV40GridStateFilterValue.gxTpr_Value;
               AV23TFFuncaoDados_Ativo_Sels.FromJSonString(AV22TFFuncaoDados_Ativo_SelsJson);
            }
            AV59GXV1 = (int)(AV59GXV1+1);
         }
         if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(1));
            AV43DynamicFiltersSelector1 = AV41GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV43DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 )
            {
               AV44DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV45FuncaoDados_Nome1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector1, "FUNCAODADOS_SISTEMADES") == 0 )
            {
               AV44DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV46FuncaoDados_SistemaDes1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV47DynamicFiltersEnabled2 = true;
               AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(2));
               AV48DynamicFiltersSelector2 = AV41GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 )
               {
                  AV49DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV50FuncaoDados_Nome2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector2, "FUNCAODADOS_SISTEMADES") == 0 )
               {
                  AV49DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV51FuncaoDados_SistemaDes2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV52DynamicFiltersEnabled3 = true;
                  AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(3));
                  AV53DynamicFiltersSelector3 = AV41GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "FUNCAODADOS_NOME") == 0 )
                  {
                     AV54DynamicFiltersOperator3 = AV41GridStateDynamicFilter.gxTpr_Operator;
                     AV55FuncaoDados_Nome3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "FUNCAODADOS_SISTEMADES") == 0 )
                  {
                     AV54DynamicFiltersOperator3 = AV41GridStateDynamicFilter.gxTpr_Operator;
                     AV56FuncaoDados_SistemaDes3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAODADOS_NOMEOPTIONS' Routine */
         AV10TFFuncaoDados_Nome = AV24SearchTxt;
         AV11TFFuncaoDados_Nome_Sel = "";
         AV61WWFuncaoDadosDS_1_Funcaodados_sistemaareacod = AV42FuncaoDados_SistemaAreaCod;
         AV62WWFuncaoDadosDS_2_Dynamicfiltersselector1 = AV43DynamicFiltersSelector1;
         AV63WWFuncaoDadosDS_3_Dynamicfiltersoperator1 = AV44DynamicFiltersOperator1;
         AV64WWFuncaoDadosDS_4_Funcaodados_nome1 = AV45FuncaoDados_Nome1;
         AV65WWFuncaoDadosDS_5_Funcaodados_sistemades1 = AV46FuncaoDados_SistemaDes1;
         AV66WWFuncaoDadosDS_6_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV67WWFuncaoDadosDS_7_Dynamicfiltersselector2 = AV48DynamicFiltersSelector2;
         AV68WWFuncaoDadosDS_8_Dynamicfiltersoperator2 = AV49DynamicFiltersOperator2;
         AV69WWFuncaoDadosDS_9_Funcaodados_nome2 = AV50FuncaoDados_Nome2;
         AV70WWFuncaoDadosDS_10_Funcaodados_sistemades2 = AV51FuncaoDados_SistemaDes2;
         AV71WWFuncaoDadosDS_11_Dynamicfiltersenabled3 = AV52DynamicFiltersEnabled3;
         AV72WWFuncaoDadosDS_12_Dynamicfiltersselector3 = AV53DynamicFiltersSelector3;
         AV73WWFuncaoDadosDS_13_Dynamicfiltersoperator3 = AV54DynamicFiltersOperator3;
         AV74WWFuncaoDadosDS_14_Funcaodados_nome3 = AV55FuncaoDados_Nome3;
         AV75WWFuncaoDadosDS_15_Funcaodados_sistemades3 = AV56FuncaoDados_SistemaDes3;
         AV76WWFuncaoDadosDS_16_Tffuncaodados_nome = AV10TFFuncaoDados_Nome;
         AV77WWFuncaoDadosDS_17_Tffuncaodados_nome_sel = AV11TFFuncaoDados_Nome_Sel;
         AV78WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels = AV13TFFuncaoDados_Tipo_Sels;
         AV79WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels = AV15TFFuncaoDados_Complexidade_Sels;
         AV80WWFuncaoDadosDS_20_Tffuncaodados_der = AV16TFFuncaoDados_DER;
         AV81WWFuncaoDadosDS_21_Tffuncaodados_der_to = AV17TFFuncaoDados_DER_To;
         AV82WWFuncaoDadosDS_22_Tffuncaodados_rlr = AV18TFFuncaoDados_RLR;
         AV83WWFuncaoDadosDS_23_Tffuncaodados_rlr_to = AV19TFFuncaoDados_RLR_To;
         AV84WWFuncaoDadosDS_24_Tffuncaodados_pf = AV20TFFuncaoDados_PF;
         AV85WWFuncaoDadosDS_25_Tffuncaodados_pf_to = AV21TFFuncaoDados_PF_To;
         AV86WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels = AV23TFFuncaoDados_Ativo_Sels;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A376FuncaoDados_Complexidade ,
                                              AV79WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels ,
                                              A373FuncaoDados_Tipo ,
                                              AV78WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels ,
                                              A394FuncaoDados_Ativo ,
                                              AV86WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels ,
                                              AV61WWFuncaoDadosDS_1_Funcaodados_sistemaareacod ,
                                              AV62WWFuncaoDadosDS_2_Dynamicfiltersselector1 ,
                                              AV63WWFuncaoDadosDS_3_Dynamicfiltersoperator1 ,
                                              AV64WWFuncaoDadosDS_4_Funcaodados_nome1 ,
                                              AV65WWFuncaoDadosDS_5_Funcaodados_sistemades1 ,
                                              AV66WWFuncaoDadosDS_6_Dynamicfiltersenabled2 ,
                                              AV67WWFuncaoDadosDS_7_Dynamicfiltersselector2 ,
                                              AV68WWFuncaoDadosDS_8_Dynamicfiltersoperator2 ,
                                              AV69WWFuncaoDadosDS_9_Funcaodados_nome2 ,
                                              AV70WWFuncaoDadosDS_10_Funcaodados_sistemades2 ,
                                              AV71WWFuncaoDadosDS_11_Dynamicfiltersenabled3 ,
                                              AV72WWFuncaoDadosDS_12_Dynamicfiltersselector3 ,
                                              AV73WWFuncaoDadosDS_13_Dynamicfiltersoperator3 ,
                                              AV74WWFuncaoDadosDS_14_Funcaodados_nome3 ,
                                              AV75WWFuncaoDadosDS_15_Funcaodados_sistemades3 ,
                                              AV77WWFuncaoDadosDS_17_Tffuncaodados_nome_sel ,
                                              AV76WWFuncaoDadosDS_16_Tffuncaodados_nome ,
                                              AV78WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels.Count ,
                                              AV86WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels.Count ,
                                              A372FuncaoDados_SistemaAreaCod ,
                                              A369FuncaoDados_Nome ,
                                              A371FuncaoDados_SistemaDes ,
                                              AV79WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels.Count ,
                                              AV80WWFuncaoDadosDS_20_Tffuncaodados_der ,
                                              A374FuncaoDados_DER ,
                                              AV81WWFuncaoDadosDS_21_Tffuncaodados_der_to ,
                                              AV82WWFuncaoDadosDS_22_Tffuncaodados_rlr ,
                                              A375FuncaoDados_RLR ,
                                              AV83WWFuncaoDadosDS_23_Tffuncaodados_rlr_to ,
                                              AV84WWFuncaoDadosDS_24_Tffuncaodados_pf ,
                                              A377FuncaoDados_PF ,
                                              AV85WWFuncaoDadosDS_25_Tffuncaodados_pf_to },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                              }
         });
         lV64WWFuncaoDadosDS_4_Funcaodados_nome1 = StringUtil.Concat( StringUtil.RTrim( AV64WWFuncaoDadosDS_4_Funcaodados_nome1), "%", "");
         lV64WWFuncaoDadosDS_4_Funcaodados_nome1 = StringUtil.Concat( StringUtil.RTrim( AV64WWFuncaoDadosDS_4_Funcaodados_nome1), "%", "");
         lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1 = StringUtil.Concat( StringUtil.RTrim( AV65WWFuncaoDadosDS_5_Funcaodados_sistemades1), "%", "");
         lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1 = StringUtil.Concat( StringUtil.RTrim( AV65WWFuncaoDadosDS_5_Funcaodados_sistemades1), "%", "");
         lV69WWFuncaoDadosDS_9_Funcaodados_nome2 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncaoDadosDS_9_Funcaodados_nome2), "%", "");
         lV69WWFuncaoDadosDS_9_Funcaodados_nome2 = StringUtil.Concat( StringUtil.RTrim( AV69WWFuncaoDadosDS_9_Funcaodados_nome2), "%", "");
         lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2 = StringUtil.Concat( StringUtil.RTrim( AV70WWFuncaoDadosDS_10_Funcaodados_sistemades2), "%", "");
         lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2 = StringUtil.Concat( StringUtil.RTrim( AV70WWFuncaoDadosDS_10_Funcaodados_sistemades2), "%", "");
         lV74WWFuncaoDadosDS_14_Funcaodados_nome3 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncaoDadosDS_14_Funcaodados_nome3), "%", "");
         lV74WWFuncaoDadosDS_14_Funcaodados_nome3 = StringUtil.Concat( StringUtil.RTrim( AV74WWFuncaoDadosDS_14_Funcaodados_nome3), "%", "");
         lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3 = StringUtil.Concat( StringUtil.RTrim( AV75WWFuncaoDadosDS_15_Funcaodados_sistemades3), "%", "");
         lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3 = StringUtil.Concat( StringUtil.RTrim( AV75WWFuncaoDadosDS_15_Funcaodados_sistemades3), "%", "");
         lV76WWFuncaoDadosDS_16_Tffuncaodados_nome = StringUtil.Concat( StringUtil.RTrim( AV76WWFuncaoDadosDS_16_Tffuncaodados_nome), "%", "");
         /* Using cursor P00LJ2 */
         pr_default.execute(0, new Object[] {AV79WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels.Count, AV61WWFuncaoDadosDS_1_Funcaodados_sistemaareacod, lV64WWFuncaoDadosDS_4_Funcaodados_nome1, lV64WWFuncaoDadosDS_4_Funcaodados_nome1, lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1, lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1, lV69WWFuncaoDadosDS_9_Funcaodados_nome2, lV69WWFuncaoDadosDS_9_Funcaodados_nome2, lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2, lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2, lV74WWFuncaoDadosDS_14_Funcaodados_nome3, lV74WWFuncaoDadosDS_14_Funcaodados_nome3, lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3, lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3, lV76WWFuncaoDadosDS_16_Tffuncaodados_nome, AV77WWFuncaoDadosDS_17_Tffuncaodados_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKLJ2 = false;
            A370FuncaoDados_SistemaCod = P00LJ2_A370FuncaoDados_SistemaCod[0];
            A369FuncaoDados_Nome = P00LJ2_A369FuncaoDados_Nome[0];
            A394FuncaoDados_Ativo = P00LJ2_A394FuncaoDados_Ativo[0];
            A373FuncaoDados_Tipo = P00LJ2_A373FuncaoDados_Tipo[0];
            A371FuncaoDados_SistemaDes = P00LJ2_A371FuncaoDados_SistemaDes[0];
            n371FuncaoDados_SistemaDes = P00LJ2_n371FuncaoDados_SistemaDes[0];
            A372FuncaoDados_SistemaAreaCod = P00LJ2_A372FuncaoDados_SistemaAreaCod[0];
            n372FuncaoDados_SistemaAreaCod = P00LJ2_n372FuncaoDados_SistemaAreaCod[0];
            A755FuncaoDados_Contar = P00LJ2_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = P00LJ2_n755FuncaoDados_Contar[0];
            A368FuncaoDados_Codigo = P00LJ2_A368FuncaoDados_Codigo[0];
            A371FuncaoDados_SistemaDes = P00LJ2_A371FuncaoDados_SistemaDes[0];
            n371FuncaoDados_SistemaDes = P00LJ2_n371FuncaoDados_SistemaDes[0];
            A372FuncaoDados_SistemaAreaCod = P00LJ2_A372FuncaoDados_SistemaAreaCod[0];
            n372FuncaoDados_SistemaAreaCod = P00LJ2_n372FuncaoDados_SistemaAreaCod[0];
            GXt_char1 = A376FuncaoDados_Complexidade;
            new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
            A376FuncaoDados_Complexidade = GXt_char1;
            if ( ( AV79WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels.Count <= 0 ) || ( (AV79WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels.IndexOf(StringUtil.RTrim( A376FuncaoDados_Complexidade))>0) ) )
            {
               GXt_int2 = A374FuncaoDados_DER;
               new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
               A374FuncaoDados_DER = GXt_int2;
               if ( (0==AV80WWFuncaoDadosDS_20_Tffuncaodados_der) || ( ( A374FuncaoDados_DER >= AV80WWFuncaoDadosDS_20_Tffuncaodados_der ) ) )
               {
                  if ( (0==AV81WWFuncaoDadosDS_21_Tffuncaodados_der_to) || ( ( A374FuncaoDados_DER <= AV81WWFuncaoDadosDS_21_Tffuncaodados_der_to ) ) )
                  {
                     GXt_int2 = A375FuncaoDados_RLR;
                     new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
                     A375FuncaoDados_RLR = GXt_int2;
                     if ( (0==AV82WWFuncaoDadosDS_22_Tffuncaodados_rlr) || ( ( A375FuncaoDados_RLR >= AV82WWFuncaoDadosDS_22_Tffuncaodados_rlr ) ) )
                     {
                        if ( (0==AV83WWFuncaoDadosDS_23_Tffuncaodados_rlr_to) || ( ( A375FuncaoDados_RLR <= AV83WWFuncaoDadosDS_23_Tffuncaodados_rlr_to ) ) )
                        {
                           if ( A755FuncaoDados_Contar )
                           {
                              GXt_int2 = (short)(A377FuncaoDados_PF);
                              new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int2) ;
                              A377FuncaoDados_PF = (decimal)(GXt_int2);
                           }
                           else
                           {
                              A377FuncaoDados_PF = 0;
                           }
                           if ( (Convert.ToDecimal(0)==AV84WWFuncaoDadosDS_24_Tffuncaodados_pf) || ( ( A377FuncaoDados_PF >= AV84WWFuncaoDadosDS_24_Tffuncaodados_pf ) ) )
                           {
                              if ( (Convert.ToDecimal(0)==AV85WWFuncaoDadosDS_25_Tffuncaodados_pf_to) || ( ( A377FuncaoDados_PF <= AV85WWFuncaoDadosDS_25_Tffuncaodados_pf_to ) ) )
                              {
                                 AV36count = 0;
                                 while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00LJ2_A369FuncaoDados_Nome[0], A369FuncaoDados_Nome) == 0 ) )
                                 {
                                    BRKLJ2 = false;
                                    A368FuncaoDados_Codigo = P00LJ2_A368FuncaoDados_Codigo[0];
                                    AV36count = (long)(AV36count+1);
                                    BRKLJ2 = true;
                                    pr_default.readNext(0);
                                 }
                                 if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A369FuncaoDados_Nome)) )
                                 {
                                    AV28Option = A369FuncaoDados_Nome;
                                    AV29Options.Add(AV28Option, 0);
                                    AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                                 }
                                 if ( AV29Options.Count == 50 )
                                 {
                                    /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                    if (true) break;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKLJ2 )
            {
               BRKLJ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFuncaoDados_Nome = "";
         AV11TFFuncaoDados_Nome_Sel = "";
         AV12TFFuncaoDados_Tipo_SelsJson = "";
         AV13TFFuncaoDados_Tipo_Sels = new GxSimpleCollection();
         AV14TFFuncaoDados_Complexidade_SelsJson = "";
         AV15TFFuncaoDados_Complexidade_Sels = new GxSimpleCollection();
         AV22TFFuncaoDados_Ativo_SelsJson = "";
         AV23TFFuncaoDados_Ativo_Sels = new GxSimpleCollection();
         AV41GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV43DynamicFiltersSelector1 = "";
         AV45FuncaoDados_Nome1 = "";
         AV46FuncaoDados_SistemaDes1 = "";
         AV48DynamicFiltersSelector2 = "";
         AV50FuncaoDados_Nome2 = "";
         AV51FuncaoDados_SistemaDes2 = "";
         AV53DynamicFiltersSelector3 = "";
         AV55FuncaoDados_Nome3 = "";
         AV56FuncaoDados_SistemaDes3 = "";
         AV62WWFuncaoDadosDS_2_Dynamicfiltersselector1 = "";
         AV64WWFuncaoDadosDS_4_Funcaodados_nome1 = "";
         AV65WWFuncaoDadosDS_5_Funcaodados_sistemades1 = "";
         AV67WWFuncaoDadosDS_7_Dynamicfiltersselector2 = "";
         AV69WWFuncaoDadosDS_9_Funcaodados_nome2 = "";
         AV70WWFuncaoDadosDS_10_Funcaodados_sistemades2 = "";
         AV72WWFuncaoDadosDS_12_Dynamicfiltersselector3 = "";
         AV74WWFuncaoDadosDS_14_Funcaodados_nome3 = "";
         AV75WWFuncaoDadosDS_15_Funcaodados_sistemades3 = "";
         AV76WWFuncaoDadosDS_16_Tffuncaodados_nome = "";
         AV77WWFuncaoDadosDS_17_Tffuncaodados_nome_sel = "";
         AV78WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels = new GxSimpleCollection();
         AV79WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels = new GxSimpleCollection();
         AV86WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV64WWFuncaoDadosDS_4_Funcaodados_nome1 = "";
         lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1 = "";
         lV69WWFuncaoDadosDS_9_Funcaodados_nome2 = "";
         lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2 = "";
         lV74WWFuncaoDadosDS_14_Funcaodados_nome3 = "";
         lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3 = "";
         lV76WWFuncaoDadosDS_16_Tffuncaodados_nome = "";
         A376FuncaoDados_Complexidade = "";
         A373FuncaoDados_Tipo = "";
         A394FuncaoDados_Ativo = "";
         A369FuncaoDados_Nome = "";
         A371FuncaoDados_SistemaDes = "";
         P00LJ2_A370FuncaoDados_SistemaCod = new int[1] ;
         P00LJ2_A369FuncaoDados_Nome = new String[] {""} ;
         P00LJ2_A394FuncaoDados_Ativo = new String[] {""} ;
         P00LJ2_A373FuncaoDados_Tipo = new String[] {""} ;
         P00LJ2_A371FuncaoDados_SistemaDes = new String[] {""} ;
         P00LJ2_n371FuncaoDados_SistemaDes = new bool[] {false} ;
         P00LJ2_A372FuncaoDados_SistemaAreaCod = new int[1] ;
         P00LJ2_n372FuncaoDados_SistemaAreaCod = new bool[] {false} ;
         P00LJ2_A755FuncaoDados_Contar = new bool[] {false} ;
         P00LJ2_n755FuncaoDados_Contar = new bool[] {false} ;
         P00LJ2_A368FuncaoDados_Codigo = new int[1] ;
         GXt_char1 = "";
         AV28Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwfuncaodadosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00LJ2_A370FuncaoDados_SistemaCod, P00LJ2_A369FuncaoDados_Nome, P00LJ2_A394FuncaoDados_Ativo, P00LJ2_A373FuncaoDados_Tipo, P00LJ2_A371FuncaoDados_SistemaDes, P00LJ2_n371FuncaoDados_SistemaDes, P00LJ2_A372FuncaoDados_SistemaAreaCod, P00LJ2_n372FuncaoDados_SistemaAreaCod, P00LJ2_A755FuncaoDados_Contar, P00LJ2_n755FuncaoDados_Contar,
               P00LJ2_A368FuncaoDados_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFFuncaoDados_DER ;
      private short AV17TFFuncaoDados_DER_To ;
      private short AV18TFFuncaoDados_RLR ;
      private short AV19TFFuncaoDados_RLR_To ;
      private short AV44DynamicFiltersOperator1 ;
      private short AV49DynamicFiltersOperator2 ;
      private short AV54DynamicFiltersOperator3 ;
      private short AV63WWFuncaoDadosDS_3_Dynamicfiltersoperator1 ;
      private short AV68WWFuncaoDadosDS_8_Dynamicfiltersoperator2 ;
      private short AV73WWFuncaoDadosDS_13_Dynamicfiltersoperator3 ;
      private short AV80WWFuncaoDadosDS_20_Tffuncaodados_der ;
      private short AV81WWFuncaoDadosDS_21_Tffuncaodados_der_to ;
      private short AV82WWFuncaoDadosDS_22_Tffuncaodados_rlr ;
      private short AV83WWFuncaoDadosDS_23_Tffuncaodados_rlr_to ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short GXt_int2 ;
      private int AV59GXV1 ;
      private int AV42FuncaoDados_SistemaAreaCod ;
      private int AV61WWFuncaoDadosDS_1_Funcaodados_sistemaareacod ;
      private int AV78WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels_Count ;
      private int AV86WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels_Count ;
      private int AV79WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels_Count ;
      private int A372FuncaoDados_SistemaAreaCod ;
      private int A370FuncaoDados_SistemaCod ;
      private int A368FuncaoDados_Codigo ;
      private long AV36count ;
      private decimal AV20TFFuncaoDados_PF ;
      private decimal AV21TFFuncaoDados_PF_To ;
      private decimal AV84WWFuncaoDadosDS_24_Tffuncaodados_pf ;
      private decimal AV85WWFuncaoDadosDS_25_Tffuncaodados_pf_to ;
      private decimal A377FuncaoDados_PF ;
      private String scmdbuf ;
      private String A376FuncaoDados_Complexidade ;
      private String A373FuncaoDados_Tipo ;
      private String A394FuncaoDados_Ativo ;
      private String GXt_char1 ;
      private bool returnInSub ;
      private bool AV47DynamicFiltersEnabled2 ;
      private bool AV52DynamicFiltersEnabled3 ;
      private bool AV66WWFuncaoDadosDS_6_Dynamicfiltersenabled2 ;
      private bool AV71WWFuncaoDadosDS_11_Dynamicfiltersenabled3 ;
      private bool BRKLJ2 ;
      private bool n371FuncaoDados_SistemaDes ;
      private bool n372FuncaoDados_SistemaAreaCod ;
      private bool A755FuncaoDados_Contar ;
      private bool n755FuncaoDados_Contar ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV12TFFuncaoDados_Tipo_SelsJson ;
      private String AV14TFFuncaoDados_Complexidade_SelsJson ;
      private String AV22TFFuncaoDados_Ativo_SelsJson ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV10TFFuncaoDados_Nome ;
      private String AV11TFFuncaoDados_Nome_Sel ;
      private String AV43DynamicFiltersSelector1 ;
      private String AV45FuncaoDados_Nome1 ;
      private String AV46FuncaoDados_SistemaDes1 ;
      private String AV48DynamicFiltersSelector2 ;
      private String AV50FuncaoDados_Nome2 ;
      private String AV51FuncaoDados_SistemaDes2 ;
      private String AV53DynamicFiltersSelector3 ;
      private String AV55FuncaoDados_Nome3 ;
      private String AV56FuncaoDados_SistemaDes3 ;
      private String AV62WWFuncaoDadosDS_2_Dynamicfiltersselector1 ;
      private String AV64WWFuncaoDadosDS_4_Funcaodados_nome1 ;
      private String AV65WWFuncaoDadosDS_5_Funcaodados_sistemades1 ;
      private String AV67WWFuncaoDadosDS_7_Dynamicfiltersselector2 ;
      private String AV69WWFuncaoDadosDS_9_Funcaodados_nome2 ;
      private String AV70WWFuncaoDadosDS_10_Funcaodados_sistemades2 ;
      private String AV72WWFuncaoDadosDS_12_Dynamicfiltersselector3 ;
      private String AV74WWFuncaoDadosDS_14_Funcaodados_nome3 ;
      private String AV75WWFuncaoDadosDS_15_Funcaodados_sistemades3 ;
      private String AV76WWFuncaoDadosDS_16_Tffuncaodados_nome ;
      private String AV77WWFuncaoDadosDS_17_Tffuncaodados_nome_sel ;
      private String lV64WWFuncaoDadosDS_4_Funcaodados_nome1 ;
      private String lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1 ;
      private String lV69WWFuncaoDadosDS_9_Funcaodados_nome2 ;
      private String lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2 ;
      private String lV74WWFuncaoDadosDS_14_Funcaodados_nome3 ;
      private String lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3 ;
      private String lV76WWFuncaoDadosDS_16_Tffuncaodados_nome ;
      private String A369FuncaoDados_Nome ;
      private String A371FuncaoDados_SistemaDes ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00LJ2_A370FuncaoDados_SistemaCod ;
      private String[] P00LJ2_A369FuncaoDados_Nome ;
      private String[] P00LJ2_A394FuncaoDados_Ativo ;
      private String[] P00LJ2_A373FuncaoDados_Tipo ;
      private String[] P00LJ2_A371FuncaoDados_SistemaDes ;
      private bool[] P00LJ2_n371FuncaoDados_SistemaDes ;
      private int[] P00LJ2_A372FuncaoDados_SistemaAreaCod ;
      private bool[] P00LJ2_n372FuncaoDados_SistemaAreaCod ;
      private bool[] P00LJ2_A755FuncaoDados_Contar ;
      private bool[] P00LJ2_n755FuncaoDados_Contar ;
      private int[] P00LJ2_A368FuncaoDados_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFFuncaoDados_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV15TFFuncaoDados_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23TFFuncaoDados_Ativo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV78WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV79WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV86WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV41GridStateDynamicFilter ;
   }

   public class getwwfuncaodadosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00LJ2( IGxContext context ,
                                             String A376FuncaoDados_Complexidade ,
                                             IGxCollection AV79WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels ,
                                             String A373FuncaoDados_Tipo ,
                                             IGxCollection AV78WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels ,
                                             String A394FuncaoDados_Ativo ,
                                             IGxCollection AV86WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels ,
                                             int AV61WWFuncaoDadosDS_1_Funcaodados_sistemaareacod ,
                                             String AV62WWFuncaoDadosDS_2_Dynamicfiltersselector1 ,
                                             short AV63WWFuncaoDadosDS_3_Dynamicfiltersoperator1 ,
                                             String AV64WWFuncaoDadosDS_4_Funcaodados_nome1 ,
                                             String AV65WWFuncaoDadosDS_5_Funcaodados_sistemades1 ,
                                             bool AV66WWFuncaoDadosDS_6_Dynamicfiltersenabled2 ,
                                             String AV67WWFuncaoDadosDS_7_Dynamicfiltersselector2 ,
                                             short AV68WWFuncaoDadosDS_8_Dynamicfiltersoperator2 ,
                                             String AV69WWFuncaoDadosDS_9_Funcaodados_nome2 ,
                                             String AV70WWFuncaoDadosDS_10_Funcaodados_sistemades2 ,
                                             bool AV71WWFuncaoDadosDS_11_Dynamicfiltersenabled3 ,
                                             String AV72WWFuncaoDadosDS_12_Dynamicfiltersselector3 ,
                                             short AV73WWFuncaoDadosDS_13_Dynamicfiltersoperator3 ,
                                             String AV74WWFuncaoDadosDS_14_Funcaodados_nome3 ,
                                             String AV75WWFuncaoDadosDS_15_Funcaodados_sistemades3 ,
                                             String AV77WWFuncaoDadosDS_17_Tffuncaodados_nome_sel ,
                                             String AV76WWFuncaoDadosDS_16_Tffuncaodados_nome ,
                                             int AV78WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels_Count ,
                                             int AV86WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels_Count ,
                                             int A372FuncaoDados_SistemaAreaCod ,
                                             String A369FuncaoDados_Nome ,
                                             String A371FuncaoDados_SistemaDes ,
                                             int AV79WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels_Count ,
                                             short AV80WWFuncaoDadosDS_20_Tffuncaodados_der ,
                                             short A374FuncaoDados_DER ,
                                             short AV81WWFuncaoDadosDS_21_Tffuncaodados_der_to ,
                                             short AV82WWFuncaoDadosDS_22_Tffuncaodados_rlr ,
                                             short A375FuncaoDados_RLR ,
                                             short AV83WWFuncaoDadosDS_23_Tffuncaodados_rlr_to ,
                                             decimal AV84WWFuncaoDadosDS_24_Tffuncaodados_pf ,
                                             decimal A377FuncaoDados_PF ,
                                             decimal AV85WWFuncaoDadosDS_25_Tffuncaodados_pf_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [16] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoDados_SistemaCod] AS FuncaoDados_SistemaCod, T1.[FuncaoDados_Nome], T1.[FuncaoDados_Ativo], T1.[FuncaoDados_Tipo], T2.[Sistema_Nome] AS FuncaoDados_SistemaDes, T2.[Sistema_AreaTrabalhoCod] AS FuncaoDados_SistemaAreaCod, T1.[FuncaoDados_Contar], T1.[FuncaoDados_Codigo] FROM ([FuncaoDados] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[FuncaoDados_SistemaCod])";
         if ( ! (0==AV61WWFuncaoDadosDS_1_Funcaodados_sistemaareacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_AreaTrabalhoCod] = @AV61WWFuncaoDadosDS_1_Funcaodados_sistemaareacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_AreaTrabalhoCod] = @AV61WWFuncaoDadosDS_1_Funcaodados_sistemaareacod)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV62WWFuncaoDadosDS_2_Dynamicfiltersselector1, "FUNCAODADOS_NOME") == 0 ) && ( AV63WWFuncaoDadosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWFuncaoDadosDS_4_Funcaodados_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV64WWFuncaoDadosDS_4_Funcaodados_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like @lV64WWFuncaoDadosDS_4_Funcaodados_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV62WWFuncaoDadosDS_2_Dynamicfiltersselector1, "FUNCAODADOS_NOME") == 0 ) && ( AV63WWFuncaoDadosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWFuncaoDadosDS_4_Funcaodados_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV64WWFuncaoDadosDS_4_Funcaodados_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like '%' + @lV64WWFuncaoDadosDS_4_Funcaodados_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV62WWFuncaoDadosDS_2_Dynamicfiltersselector1, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV63WWFuncaoDadosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWFuncaoDadosDS_5_Funcaodados_sistemades1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV62WWFuncaoDadosDS_2_Dynamicfiltersselector1, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV63WWFuncaoDadosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWFuncaoDadosDS_5_Funcaodados_sistemades1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV66WWFuncaoDadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV67WWFuncaoDadosDS_7_Dynamicfiltersselector2, "FUNCAODADOS_NOME") == 0 ) && ( AV68WWFuncaoDadosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncaoDadosDS_9_Funcaodados_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV69WWFuncaoDadosDS_9_Funcaodados_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like @lV69WWFuncaoDadosDS_9_Funcaodados_nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV66WWFuncaoDadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV67WWFuncaoDadosDS_7_Dynamicfiltersselector2, "FUNCAODADOS_NOME") == 0 ) && ( AV68WWFuncaoDadosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncaoDadosDS_9_Funcaodados_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV69WWFuncaoDadosDS_9_Funcaodados_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like '%' + @lV69WWFuncaoDadosDS_9_Funcaodados_nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV66WWFuncaoDadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV67WWFuncaoDadosDS_7_Dynamicfiltersselector2, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV68WWFuncaoDadosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncaoDadosDS_10_Funcaodados_sistemades2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV66WWFuncaoDadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV67WWFuncaoDadosDS_7_Dynamicfiltersselector2, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV68WWFuncaoDadosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncaoDadosDS_10_Funcaodados_sistemades2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV71WWFuncaoDadosDS_11_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV72WWFuncaoDadosDS_12_Dynamicfiltersselector3, "FUNCAODADOS_NOME") == 0 ) && ( AV73WWFuncaoDadosDS_13_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncaoDadosDS_14_Funcaodados_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV74WWFuncaoDadosDS_14_Funcaodados_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like @lV74WWFuncaoDadosDS_14_Funcaodados_nome3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV71WWFuncaoDadosDS_11_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV72WWFuncaoDadosDS_12_Dynamicfiltersselector3, "FUNCAODADOS_NOME") == 0 ) && ( AV73WWFuncaoDadosDS_13_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFuncaoDadosDS_14_Funcaodados_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV74WWFuncaoDadosDS_14_Funcaodados_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like '%' + @lV74WWFuncaoDadosDS_14_Funcaodados_nome3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV71WWFuncaoDadosDS_11_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV72WWFuncaoDadosDS_12_Dynamicfiltersselector3, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV73WWFuncaoDadosDS_13_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncaoDadosDS_15_Funcaodados_sistemades3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV71WWFuncaoDadosDS_11_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV72WWFuncaoDadosDS_12_Dynamicfiltersselector3, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV73WWFuncaoDadosDS_13_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncaoDadosDS_15_Funcaodados_sistemades3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWFuncaoDadosDS_17_Tffuncaodados_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWFuncaoDadosDS_16_Tffuncaodados_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV76WWFuncaoDadosDS_16_Tffuncaodados_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like @lV76WWFuncaoDadosDS_16_Tffuncaodados_nome)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWFuncaoDadosDS_17_Tffuncaodados_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] = @AV77WWFuncaoDadosDS_17_Tffuncaodados_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] = @AV77WWFuncaoDadosDS_17_Tffuncaodados_nome_sel)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV78WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV78WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels, "T1.[FuncaoDados_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV78WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels, "T1.[FuncaoDados_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV86WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV86WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels, "T1.[FuncaoDados_Ativo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV86WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels, "T1.[FuncaoDados_Ativo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00LJ2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (short)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (short)dynConstraints[33] , (short)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (decimal)dynConstraints[37] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00LJ2 ;
          prmP00LJ2 = new Object[] {
          new Object[] {"@AV79WWFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV61WWFuncaoDadosDS_1_Funcaodados_sistemaareacod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV64WWFuncaoDadosDS_4_Funcaodados_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV64WWFuncaoDadosDS_4_Funcaodados_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV65WWFuncaoDadosDS_5_Funcaodados_sistemades1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV69WWFuncaoDadosDS_9_Funcaodados_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV69WWFuncaoDadosDS_9_Funcaodados_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV70WWFuncaoDadosDS_10_Funcaodados_sistemades2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV74WWFuncaoDadosDS_14_Funcaodados_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV74WWFuncaoDadosDS_14_Funcaodados_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV75WWFuncaoDadosDS_15_Funcaodados_sistemades3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV76WWFuncaoDadosDS_16_Tffuncaodados_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV77WWFuncaoDadosDS_17_Tffuncaodados_nome_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00LJ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LJ2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwfuncaodadosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwfuncaodadosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwfuncaodadosfilterdata") )
          {
             return  ;
          }
          getwwfuncaodadosfilterdata worker = new getwwfuncaodadosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
