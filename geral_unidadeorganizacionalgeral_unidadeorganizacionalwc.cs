/*
               File: Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC
        Description: Geral_Unidade Organizacional Geral_Unidade Organizacional WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:2:23.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class geral_unidadeorganizacionalgeral_unidadeorganizacionalwc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public geral_unidadeorganizacionalgeral_unidadeorganizacionalwc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public geral_unidadeorganizacionalgeral_unidadeorganizacionalwc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_UnidadeOrganizacional_Vinculada )
      {
         this.AV7UnidadeOrganizacional_Vinculada = aP0_UnidadeOrganizacional_Vinculada;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         chkavUnidadeorganizacional_ativo = new GXCheckbox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7UnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UnidadeOrganizacional_Vinculada), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7UnidadeOrganizacional_Vinculada});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_71 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_71_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_71_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17UnidadeOrganizacional_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17UnidadeOrganizacional_Nome1", AV17UnidadeOrganizacional_Nome1);
                  AV19DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
                  AV20UnidadeOrganizacional_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_Nome2", AV20UnidadeOrganizacional_Nome2);
                  AV22DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
                  AV23UnidadeOrganizacional_Nome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome3", AV23UnidadeOrganizacional_Nome3);
                  AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
                  AV21DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
                  AV33TFUnidadeOrganizacional_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFUnidadeOrganizacional_Nome", AV33TFUnidadeOrganizacional_Nome);
                  AV34TFUnidadeOrganizacional_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFUnidadeOrganizacional_Nome_Sel", AV34TFUnidadeOrganizacional_Nome_Sel);
                  AV37TFTpUo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFTpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFTpUo_Codigo), 6, 0)));
                  AV38TFTpUo_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFTpUo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFTpUo_Codigo_To), 6, 0)));
                  AV41TFEstado_UF = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFEstado_UF", AV41TFEstado_UF);
                  AV42TFEstado_UF_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFEstado_UF_Sel", AV42TFEstado_UF_Sel);
                  AV7UnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UnidadeOrganizacional_Vinculada), 6, 0)));
                  AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace", AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace);
                  AV39ddo_TpUo_CodigoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_TpUo_CodigoTitleControlIdToReplace", AV39ddo_TpUo_CodigoTitleControlIdToReplace);
                  AV43ddo_Estado_UFTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43ddo_Estado_UFTitleControlIdToReplace", AV43ddo_Estado_UFTitleControlIdToReplace);
                  AV50Pgmname = GetNextPar( );
                  AV29UnidadeOrganizacional_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29UnidadeOrganizacional_Ativo", AV29UnidadeOrganizacional_Ativo);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV25DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersIgnoreFirst", AV25DynamicFiltersIgnoreFirst);
                  AV24DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17UnidadeOrganizacional_Nome1, AV19DynamicFiltersSelector2, AV20UnidadeOrganizacional_Nome2, AV22DynamicFiltersSelector3, AV23UnidadeOrganizacional_Nome3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV33TFUnidadeOrganizacional_Nome, AV34TFUnidadeOrganizacional_Nome_Sel, AV37TFTpUo_Codigo, AV38TFTpUo_Codigo_To, AV41TFEstado_UF, AV42TFEstado_UF_Sel, AV7UnidadeOrganizacional_Vinculada, AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV39ddo_TpUo_CodigoTitleControlIdToReplace, AV43ddo_Estado_UFTitleControlIdToReplace, AV50Pgmname, AV29UnidadeOrganizacional_Ativo, AV11GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAD32( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV50Pgmname = "Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC";
               context.Gx_err = 0;
               WSD32( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Geral_Unidade Organizacional Geral_Unidade Organizacional WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205211822372");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("geral_unidadeorganizacionalgeral_unidadeorganizacionalwc.aspx") + "?" + UrlEncode("" +AV7UnidadeOrganizacional_Vinculada)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME1", StringUtil.RTrim( AV17UnidadeOrganizacional_Nome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME2", StringUtil.RTrim( AV20UnidadeOrganizacional_Nome2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV22DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME3", StringUtil.RTrim( AV23UnidadeOrganizacional_Nome3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV21DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME", StringUtil.RTrim( AV33TFUnidadeOrganizacional_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME_SEL", StringUtil.RTrim( AV34TFUnidadeOrganizacional_Nome_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFTPUO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFTpUo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFTPUO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFTpUo_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFESTADO_UF", StringUtil.RTrim( AV41TFEstado_UF));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFESTADO_UF_SEL", StringUtil.RTrim( AV42TFEstado_UF_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_71", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_71), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV44DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV44DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA", AV32UnidadeOrganizacional_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA", AV32UnidadeOrganizacional_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTPUO_CODIGOTITLEFILTERDATA", AV36TpUo_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTPUO_CODIGOTITLEFILTERDATA", AV36TpUo_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vESTADO_UFTITLEFILTERDATA", AV40Estado_UFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vESTADO_UFTITLEFILTERDATA", AV40Estado_UFTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7UnidadeOrganizacional_Vinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vUNIDADEORGANIZACIONAL_VINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7UnidadeOrganizacional_Vinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV50Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV25DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV24DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Caption", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Tooltip", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Cls", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortedstatus", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includefilter", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filtertype", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalisttype", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalistproc", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortasc", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortdsc", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Loadingdata", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Cleanfilter", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Noresultsfound", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Caption", StringUtil.RTrim( Ddo_tpuo_codigo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_tpuo_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Cls", StringUtil.RTrim( Ddo_tpuo_codigo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_tpuo_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_tpuo_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_tpuo_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tpuo_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_tpuo_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_tpuo_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_tpuo_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_tpuo_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_tpuo_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_tpuo_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_tpuo_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_tpuo_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_tpuo_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_tpuo_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_tpuo_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_tpuo_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_tpuo_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Caption", StringUtil.RTrim( Ddo_estado_uf_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Tooltip", StringUtil.RTrim( Ddo_estado_uf_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Cls", StringUtil.RTrim( Ddo_estado_uf_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Filteredtext_set", StringUtil.RTrim( Ddo_estado_uf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Selectedvalue_set", StringUtil.RTrim( Ddo_estado_uf_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Dropdownoptionstype", StringUtil.RTrim( Ddo_estado_uf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_estado_uf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Includesortasc", StringUtil.BoolToStr( Ddo_estado_uf_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Includesortdsc", StringUtil.BoolToStr( Ddo_estado_uf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Sortedstatus", StringUtil.RTrim( Ddo_estado_uf_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Includefilter", StringUtil.BoolToStr( Ddo_estado_uf_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Filtertype", StringUtil.RTrim( Ddo_estado_uf_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Filterisrange", StringUtil.BoolToStr( Ddo_estado_uf_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Includedatalist", StringUtil.BoolToStr( Ddo_estado_uf_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Datalisttype", StringUtil.RTrim( Ddo_estado_uf_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Datalistproc", StringUtil.RTrim( Ddo_estado_uf_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_estado_uf_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Sortasc", StringUtil.RTrim( Ddo_estado_uf_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Sortdsc", StringUtil.RTrim( Ddo_estado_uf_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Loadingdata", StringUtil.RTrim( Ddo_estado_uf_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Cleanfilter", StringUtil.RTrim( Ddo_estado_uf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Noresultsfound", StringUtil.RTrim( Ddo_estado_uf_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Searchbuttontext", StringUtil.RTrim( Ddo_estado_uf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Activeeventkey", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_tpuo_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_tpuo_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TPUO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_tpuo_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Activeeventkey", StringUtil.RTrim( Ddo_estado_uf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Filteredtext_get", StringUtil.RTrim( Ddo_estado_uf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Selectedvalue_get", StringUtil.RTrim( Ddo_estado_uf_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormD32( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("geral_unidadeorganizacionalgeral_unidadeorganizacionalwc.js", "?20205211822532");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Geral_Unidade Organizacional Geral_Unidade Organizacional WC" ;
      }

      protected void WBD30( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "geral_unidadeorganizacionalgeral_unidadeorganizacionalwc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_D32( true) ;
         }
         else
         {
            wb_table1_2_D32( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_D32e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUnidadeOrganizacional_Vinculada_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A613UnidadeOrganizacional_Vinculada), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUnidadeOrganizacional_Vinculada_Jsonclick, 0, "Attribute", "", "", "", edtUnidadeOrganizacional_Vinculada_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(81, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV21DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(82, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidadeorganizacional_nome_Internalname, StringUtil.RTrim( AV33TFUnidadeOrganizacional_Nome), StringUtil.RTrim( context.localUtil.Format( AV33TFUnidadeOrganizacional_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,83);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidadeorganizacional_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidadeorganizacional_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidadeorganizacional_nome_sel_Internalname, StringUtil.RTrim( AV34TFUnidadeOrganizacional_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV34TFUnidadeOrganizacional_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,84);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidadeorganizacional_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidadeorganizacional_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftpuo_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFTpUo_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37TFTpUo_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,85);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftpuo_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTftpuo_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftpuo_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFTpUo_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38TFTpUo_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,86);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftpuo_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTftpuo_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_uf_Internalname, StringUtil.RTrim( AV41TFEstado_UF), StringUtil.RTrim( context.localUtil.Format( AV41TFEstado_UF, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,87);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_uf_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_uf_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_uf_sel_Internalname, StringUtil.RTrim( AV42TFEstado_UF_Sel), StringUtil.RTrim( context.localUtil.Format( AV42TFEstado_UF_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,88);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_uf_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_uf_sel_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname, AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", 0, edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_TPUO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tpuo_codigotitlecontrolidtoreplace_Internalname, AV39ddo_TpUo_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavDdo_tpuo_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_ESTADO_UFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_estado_uftitlecontrolidtoreplace_Internalname, AV43ddo_Estado_UFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", 0, edtavDdo_estado_uftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTD32( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Geral_Unidade Organizacional Geral_Unidade Organizacional WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPD30( ) ;
            }
         }
      }

      protected void WSD32( )
      {
         STARTD32( ) ;
         EVTD32( ) ;
      }

      protected void EVTD32( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11D32 */
                                    E11D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_UNIDADEORGANIZACIONAL_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12D32 */
                                    E12D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TPUO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13D32 */
                                    E13D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ESTADO_UF.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14D32 */
                                    E14D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15D32 */
                                    E15D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16D32 */
                                    E16D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17D32 */
                                    E17D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18D32 */
                                    E18D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19D32 */
                                    E19D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20D32 */
                                    E20D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21D32 */
                                    E21D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22D32 */
                                    E22D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E23D32 */
                                    E23D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E24D32 */
                                    E24D32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD30( ) ;
                              }
                              nGXsfl_71_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
                              SubsflControlProps_712( ) ;
                              A611UnidadeOrganizacional_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUnidadeOrganizacional_Codigo_Internalname), ",", "."));
                              A612UnidadeOrganizacional_Nome = StringUtil.Upper( cgiGet( edtUnidadeOrganizacional_Nome_Internalname));
                              A609TpUo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTpUo_Codigo_Internalname), ",", "."));
                              A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25D32 */
                                          E25D32 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E26D32 */
                                          E26D32 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E27D32 */
                                          E27D32 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME1"), AV17UnidadeOrganizacional_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME2"), AV20UnidadeOrganizacional_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV22DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_nome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME3"), AV23UnidadeOrganizacional_Nome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV21DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfunidadeorganizacional_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME"), AV33TFUnidadeOrganizacional_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfunidadeorganizacional_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME_SEL"), AV34TFUnidadeOrganizacional_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tftpuo_codigo Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFTPUO_CODIGO"), ",", ".") != Convert.ToDecimal( AV37TFTpUo_Codigo )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tftpuo_codigo_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFTPUO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV38TFTpUo_Codigo_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfestado_uf Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFESTADO_UF"), AV41TFEstado_UF) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfestado_uf_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFESTADO_UF_SEL"), AV42TFEstado_UF_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPD30( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WED32( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormD32( ) ;
            }
         }
      }

      protected void PAD32( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            chkavUnidadeorganizacional_ativo.Name = "vUNIDADEORGANIZACIONAL_ATIVO";
            chkavUnidadeorganizacional_ativo.WebTags = "";
            chkavUnidadeorganizacional_ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavUnidadeorganizacional_ativo_Internalname, "TitleCaption", chkavUnidadeorganizacional_ativo.Caption);
            chkavUnidadeorganizacional_ativo.CheckedValue = "false";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("UNIDADEORGANIZACIONAL_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("UNIDADEORGANIZACIONAL_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("UNIDADEORGANIZACIONAL_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV22DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV22DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_712( ) ;
         while ( nGXsfl_71_idx <= nRC_GXsfl_71 )
         {
            sendrow_712( ) ;
            nGXsfl_71_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_71_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_71_idx+1));
            sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
            SubsflControlProps_712( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV17UnidadeOrganizacional_Nome1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV20UnidadeOrganizacional_Nome2 ,
                                       String AV22DynamicFiltersSelector3 ,
                                       String AV23UnidadeOrganizacional_Nome3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV21DynamicFiltersEnabled3 ,
                                       String AV33TFUnidadeOrganizacional_Nome ,
                                       String AV34TFUnidadeOrganizacional_Nome_Sel ,
                                       int AV37TFTpUo_Codigo ,
                                       int AV38TFTpUo_Codigo_To ,
                                       String AV41TFEstado_UF ,
                                       String AV42TFEstado_UF_Sel ,
                                       int AV7UnidadeOrganizacional_Vinculada ,
                                       String AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace ,
                                       String AV39ddo_TpUo_CodigoTitleControlIdToReplace ,
                                       String AV43ddo_Estado_UFTitleControlIdToReplace ,
                                       String AV50Pgmname ,
                                       bool AV29UnidadeOrganizacional_Ativo ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV25DynamicFiltersIgnoreFirst ,
                                       bool AV24DynamicFiltersRemoving ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFD32( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"UNIDADEORGANIZACIONAL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"UNIDADEORGANIZACIONAL_NOME", StringUtil.RTrim( A612UnidadeOrganizacional_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TPUO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A609TpUo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TPUO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A609TpUo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ESTADO_UF", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"ESTADO_UF", StringUtil.RTrim( A23Estado_UF));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV22DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV22DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFD32( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV50Pgmname = "Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC";
         context.Gx_err = 0;
      }

      protected void RFD32( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 71;
         /* Execute user event: E26D32 */
         E26D32 ();
         nGXsfl_71_idx = 1;
         sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
         SubsflControlProps_712( ) ;
         nGXsfl_71_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_712( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17UnidadeOrganizacional_Nome1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20UnidadeOrganizacional_Nome2 ,
                                                 AV21DynamicFiltersEnabled3 ,
                                                 AV22DynamicFiltersSelector3 ,
                                                 AV23UnidadeOrganizacional_Nome3 ,
                                                 AV34TFUnidadeOrganizacional_Nome_Sel ,
                                                 AV33TFUnidadeOrganizacional_Nome ,
                                                 AV37TFTpUo_Codigo ,
                                                 AV38TFTpUo_Codigo_To ,
                                                 AV42TFEstado_UF_Sel ,
                                                 AV41TFEstado_UF ,
                                                 A612UnidadeOrganizacional_Nome ,
                                                 A609TpUo_Codigo ,
                                                 A23Estado_UF ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A613UnidadeOrganizacional_Vinculada ,
                                                 AV7UnidadeOrganizacional_Vinculada ,
                                                 A629UnidadeOrganizacional_Ativo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17UnidadeOrganizacional_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17UnidadeOrganizacional_Nome1", AV17UnidadeOrganizacional_Nome1);
            lV20UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV20UnidadeOrganizacional_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_Nome2", AV20UnidadeOrganizacional_Nome2);
            lV23UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV23UnidadeOrganizacional_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome3", AV23UnidadeOrganizacional_Nome3);
            lV33TFUnidadeOrganizacional_Nome = StringUtil.PadR( StringUtil.RTrim( AV33TFUnidadeOrganizacional_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFUnidadeOrganizacional_Nome", AV33TFUnidadeOrganizacional_Nome);
            lV41TFEstado_UF = StringUtil.PadR( StringUtil.RTrim( AV41TFEstado_UF), 2, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFEstado_UF", AV41TFEstado_UF);
            /* Using cursor H00D32 */
            pr_default.execute(0, new Object[] {AV7UnidadeOrganizacional_Vinculada, lV17UnidadeOrganizacional_Nome1, lV20UnidadeOrganizacional_Nome2, lV23UnidadeOrganizacional_Nome3, lV33TFUnidadeOrganizacional_Nome, AV34TFUnidadeOrganizacional_Nome_Sel, AV37TFTpUo_Codigo, AV38TFTpUo_Codigo_To, lV41TFEstado_UF, AV42TFEstado_UF_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_71_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A629UnidadeOrganizacional_Ativo = H00D32_A629UnidadeOrganizacional_Ativo[0];
               A613UnidadeOrganizacional_Vinculada = H00D32_A613UnidadeOrganizacional_Vinculada[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
               n613UnidadeOrganizacional_Vinculada = H00D32_n613UnidadeOrganizacional_Vinculada[0];
               A23Estado_UF = H00D32_A23Estado_UF[0];
               A609TpUo_Codigo = H00D32_A609TpUo_Codigo[0];
               A612UnidadeOrganizacional_Nome = H00D32_A612UnidadeOrganizacional_Nome[0];
               A611UnidadeOrganizacional_Codigo = H00D32_A611UnidadeOrganizacional_Codigo[0];
               /* Execute user event: E27D32 */
               E27D32 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 71;
            WBD30( ) ;
         }
         nGXsfl_71_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17UnidadeOrganizacional_Nome1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20UnidadeOrganizacional_Nome2 ,
                                              AV21DynamicFiltersEnabled3 ,
                                              AV22DynamicFiltersSelector3 ,
                                              AV23UnidadeOrganizacional_Nome3 ,
                                              AV34TFUnidadeOrganizacional_Nome_Sel ,
                                              AV33TFUnidadeOrganizacional_Nome ,
                                              AV37TFTpUo_Codigo ,
                                              AV38TFTpUo_Codigo_To ,
                                              AV42TFEstado_UF_Sel ,
                                              AV41TFEstado_UF ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A609TpUo_Codigo ,
                                              A23Estado_UF ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              AV7UnidadeOrganizacional_Vinculada ,
                                              A629UnidadeOrganizacional_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV17UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17UnidadeOrganizacional_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17UnidadeOrganizacional_Nome1", AV17UnidadeOrganizacional_Nome1);
         lV20UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV20UnidadeOrganizacional_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_Nome2", AV20UnidadeOrganizacional_Nome2);
         lV23UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV23UnidadeOrganizacional_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome3", AV23UnidadeOrganizacional_Nome3);
         lV33TFUnidadeOrganizacional_Nome = StringUtil.PadR( StringUtil.RTrim( AV33TFUnidadeOrganizacional_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFUnidadeOrganizacional_Nome", AV33TFUnidadeOrganizacional_Nome);
         lV41TFEstado_UF = StringUtil.PadR( StringUtil.RTrim( AV41TFEstado_UF), 2, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFEstado_UF", AV41TFEstado_UF);
         /* Using cursor H00D33 */
         pr_default.execute(1, new Object[] {AV7UnidadeOrganizacional_Vinculada, lV17UnidadeOrganizacional_Nome1, lV20UnidadeOrganizacional_Nome2, lV23UnidadeOrganizacional_Nome3, lV33TFUnidadeOrganizacional_Nome, AV34TFUnidadeOrganizacional_Nome_Sel, AV37TFTpUo_Codigo, AV38TFTpUo_Codigo_To, lV41TFEstado_UF, AV42TFEstado_UF_Sel});
         GRID_nRecordCount = H00D33_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17UnidadeOrganizacional_Nome1, AV19DynamicFiltersSelector2, AV20UnidadeOrganizacional_Nome2, AV22DynamicFiltersSelector3, AV23UnidadeOrganizacional_Nome3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV33TFUnidadeOrganizacional_Nome, AV34TFUnidadeOrganizacional_Nome_Sel, AV37TFTpUo_Codigo, AV38TFTpUo_Codigo_To, AV41TFEstado_UF, AV42TFEstado_UF_Sel, AV7UnidadeOrganizacional_Vinculada, AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV39ddo_TpUo_CodigoTitleControlIdToReplace, AV43ddo_Estado_UFTitleControlIdToReplace, AV50Pgmname, AV29UnidadeOrganizacional_Ativo, AV11GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17UnidadeOrganizacional_Nome1, AV19DynamicFiltersSelector2, AV20UnidadeOrganizacional_Nome2, AV22DynamicFiltersSelector3, AV23UnidadeOrganizacional_Nome3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV33TFUnidadeOrganizacional_Nome, AV34TFUnidadeOrganizacional_Nome_Sel, AV37TFTpUo_Codigo, AV38TFTpUo_Codigo_To, AV41TFEstado_UF, AV42TFEstado_UF_Sel, AV7UnidadeOrganizacional_Vinculada, AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV39ddo_TpUo_CodigoTitleControlIdToReplace, AV43ddo_Estado_UFTitleControlIdToReplace, AV50Pgmname, AV29UnidadeOrganizacional_Ativo, AV11GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17UnidadeOrganizacional_Nome1, AV19DynamicFiltersSelector2, AV20UnidadeOrganizacional_Nome2, AV22DynamicFiltersSelector3, AV23UnidadeOrganizacional_Nome3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV33TFUnidadeOrganizacional_Nome, AV34TFUnidadeOrganizacional_Nome_Sel, AV37TFTpUo_Codigo, AV38TFTpUo_Codigo_To, AV41TFEstado_UF, AV42TFEstado_UF_Sel, AV7UnidadeOrganizacional_Vinculada, AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV39ddo_TpUo_CodigoTitleControlIdToReplace, AV43ddo_Estado_UFTitleControlIdToReplace, AV50Pgmname, AV29UnidadeOrganizacional_Ativo, AV11GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17UnidadeOrganizacional_Nome1, AV19DynamicFiltersSelector2, AV20UnidadeOrganizacional_Nome2, AV22DynamicFiltersSelector3, AV23UnidadeOrganizacional_Nome3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV33TFUnidadeOrganizacional_Nome, AV34TFUnidadeOrganizacional_Nome_Sel, AV37TFTpUo_Codigo, AV38TFTpUo_Codigo_To, AV41TFEstado_UF, AV42TFEstado_UF_Sel, AV7UnidadeOrganizacional_Vinculada, AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV39ddo_TpUo_CodigoTitleControlIdToReplace, AV43ddo_Estado_UFTitleControlIdToReplace, AV50Pgmname, AV29UnidadeOrganizacional_Ativo, AV11GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17UnidadeOrganizacional_Nome1, AV19DynamicFiltersSelector2, AV20UnidadeOrganizacional_Nome2, AV22DynamicFiltersSelector3, AV23UnidadeOrganizacional_Nome3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV33TFUnidadeOrganizacional_Nome, AV34TFUnidadeOrganizacional_Nome_Sel, AV37TFTpUo_Codigo, AV38TFTpUo_Codigo_To, AV41TFEstado_UF, AV42TFEstado_UF_Sel, AV7UnidadeOrganizacional_Vinculada, AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV39ddo_TpUo_CodigoTitleControlIdToReplace, AV43ddo_Estado_UFTitleControlIdToReplace, AV50Pgmname, AV29UnidadeOrganizacional_Ativo, AV11GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPD30( )
      {
         /* Before Start, stand alone formulas. */
         AV50Pgmname = "Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25D32 */
         E25D32 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV44DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA"), AV32UnidadeOrganizacional_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vTPUO_CODIGOTITLEFILTERDATA"), AV36TpUo_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vESTADO_UFTITLEFILTERDATA"), AV40Estado_UFTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            AV29UnidadeOrganizacional_Ativo = StringUtil.StrToBool( cgiGet( chkavUnidadeorganizacional_ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29UnidadeOrganizacional_Ativo", AV29UnidadeOrganizacional_Ativo);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV17UnidadeOrganizacional_Nome1 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17UnidadeOrganizacional_Nome1", AV17UnidadeOrganizacional_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV20UnidadeOrganizacional_Nome2 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_Nome2", AV20UnidadeOrganizacional_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV22DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
            AV23UnidadeOrganizacional_Nome3 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome3", AV23UnidadeOrganizacional_Nome3);
            A613UnidadeOrganizacional_Vinculada = (int)(context.localUtil.CToN( cgiGet( edtUnidadeOrganizacional_Vinculada_Internalname), ",", "."));
            n613UnidadeOrganizacional_Vinculada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV21DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
            AV33TFUnidadeOrganizacional_Nome = StringUtil.Upper( cgiGet( edtavTfunidadeorganizacional_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFUnidadeOrganizacional_Nome", AV33TFUnidadeOrganizacional_Nome);
            AV34TFUnidadeOrganizacional_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfunidadeorganizacional_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFUnidadeOrganizacional_Nome_Sel", AV34TFUnidadeOrganizacional_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftpuo_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftpuo_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTPUO_CODIGO");
               GX_FocusControl = edtavTftpuo_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFTpUo_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFTpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFTpUo_Codigo), 6, 0)));
            }
            else
            {
               AV37TFTpUo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTftpuo_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFTpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFTpUo_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftpuo_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftpuo_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTPUO_CODIGO_TO");
               GX_FocusControl = edtavTftpuo_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFTpUo_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFTpUo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFTpUo_Codigo_To), 6, 0)));
            }
            else
            {
               AV38TFTpUo_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTftpuo_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFTpUo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFTpUo_Codigo_To), 6, 0)));
            }
            AV41TFEstado_UF = StringUtil.Upper( cgiGet( edtavTfestado_uf_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFEstado_UF", AV41TFEstado_UF);
            AV42TFEstado_UF_Sel = StringUtil.Upper( cgiGet( edtavTfestado_uf_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFEstado_UF_Sel", AV42TFEstado_UF_Sel);
            AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = cgiGet( edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace", AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace);
            AV39ddo_TpUo_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_tpuo_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_TpUo_CodigoTitleControlIdToReplace", AV39ddo_TpUo_CodigoTitleControlIdToReplace);
            AV43ddo_Estado_UFTitleControlIdToReplace = cgiGet( edtavDdo_estado_uftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43ddo_Estado_UFTitleControlIdToReplace", AV43ddo_Estado_UFTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_71 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_71"), ",", "."));
            AV46GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV47GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7UnidadeOrganizacional_Vinculada = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7UnidadeOrganizacional_Vinculada"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_unidadeorganizacional_nome_Caption = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Caption");
            Ddo_unidadeorganizacional_nome_Tooltip = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Tooltip");
            Ddo_unidadeorganizacional_nome_Cls = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Cls");
            Ddo_unidadeorganizacional_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_set");
            Ddo_unidadeorganizacional_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_set");
            Ddo_unidadeorganizacional_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Dropdownoptionstype");
            Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Titlecontrolidtoreplace");
            Ddo_unidadeorganizacional_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includesortasc"));
            Ddo_unidadeorganizacional_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includesortdsc"));
            Ddo_unidadeorganizacional_nome_Sortedstatus = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortedstatus");
            Ddo_unidadeorganizacional_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includefilter"));
            Ddo_unidadeorganizacional_nome_Filtertype = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filtertype");
            Ddo_unidadeorganizacional_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filterisrange"));
            Ddo_unidadeorganizacional_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includedatalist"));
            Ddo_unidadeorganizacional_nome_Datalisttype = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalisttype");
            Ddo_unidadeorganizacional_nome_Datalistproc = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalistproc");
            Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_unidadeorganizacional_nome_Sortasc = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortasc");
            Ddo_unidadeorganizacional_nome_Sortdsc = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortdsc");
            Ddo_unidadeorganizacional_nome_Loadingdata = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Loadingdata");
            Ddo_unidadeorganizacional_nome_Cleanfilter = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Cleanfilter");
            Ddo_unidadeorganizacional_nome_Noresultsfound = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Noresultsfound");
            Ddo_unidadeorganizacional_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Searchbuttontext");
            Ddo_tpuo_codigo_Caption = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Caption");
            Ddo_tpuo_codigo_Tooltip = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Tooltip");
            Ddo_tpuo_codigo_Cls = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Cls");
            Ddo_tpuo_codigo_Filteredtext_set = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Filteredtext_set");
            Ddo_tpuo_codigo_Filteredtextto_set = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Filteredtextto_set");
            Ddo_tpuo_codigo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Dropdownoptionstype");
            Ddo_tpuo_codigo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Titlecontrolidtoreplace");
            Ddo_tpuo_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TPUO_CODIGO_Includesortasc"));
            Ddo_tpuo_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TPUO_CODIGO_Includesortdsc"));
            Ddo_tpuo_codigo_Sortedstatus = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Sortedstatus");
            Ddo_tpuo_codigo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TPUO_CODIGO_Includefilter"));
            Ddo_tpuo_codigo_Filtertype = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Filtertype");
            Ddo_tpuo_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TPUO_CODIGO_Filterisrange"));
            Ddo_tpuo_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TPUO_CODIGO_Includedatalist"));
            Ddo_tpuo_codigo_Sortasc = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Sortasc");
            Ddo_tpuo_codigo_Sortdsc = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Sortdsc");
            Ddo_tpuo_codigo_Cleanfilter = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Cleanfilter");
            Ddo_tpuo_codigo_Rangefilterfrom = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Rangefilterfrom");
            Ddo_tpuo_codigo_Rangefilterto = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Rangefilterto");
            Ddo_tpuo_codigo_Searchbuttontext = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Searchbuttontext");
            Ddo_estado_uf_Caption = cgiGet( sPrefix+"DDO_ESTADO_UF_Caption");
            Ddo_estado_uf_Tooltip = cgiGet( sPrefix+"DDO_ESTADO_UF_Tooltip");
            Ddo_estado_uf_Cls = cgiGet( sPrefix+"DDO_ESTADO_UF_Cls");
            Ddo_estado_uf_Filteredtext_set = cgiGet( sPrefix+"DDO_ESTADO_UF_Filteredtext_set");
            Ddo_estado_uf_Selectedvalue_set = cgiGet( sPrefix+"DDO_ESTADO_UF_Selectedvalue_set");
            Ddo_estado_uf_Dropdownoptionstype = cgiGet( sPrefix+"DDO_ESTADO_UF_Dropdownoptionstype");
            Ddo_estado_uf_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_ESTADO_UF_Titlecontrolidtoreplace");
            Ddo_estado_uf_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ESTADO_UF_Includesortasc"));
            Ddo_estado_uf_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ESTADO_UF_Includesortdsc"));
            Ddo_estado_uf_Sortedstatus = cgiGet( sPrefix+"DDO_ESTADO_UF_Sortedstatus");
            Ddo_estado_uf_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ESTADO_UF_Includefilter"));
            Ddo_estado_uf_Filtertype = cgiGet( sPrefix+"DDO_ESTADO_UF_Filtertype");
            Ddo_estado_uf_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ESTADO_UF_Filterisrange"));
            Ddo_estado_uf_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ESTADO_UF_Includedatalist"));
            Ddo_estado_uf_Datalisttype = cgiGet( sPrefix+"DDO_ESTADO_UF_Datalisttype");
            Ddo_estado_uf_Datalistproc = cgiGet( sPrefix+"DDO_ESTADO_UF_Datalistproc");
            Ddo_estado_uf_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_ESTADO_UF_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_estado_uf_Sortasc = cgiGet( sPrefix+"DDO_ESTADO_UF_Sortasc");
            Ddo_estado_uf_Sortdsc = cgiGet( sPrefix+"DDO_ESTADO_UF_Sortdsc");
            Ddo_estado_uf_Loadingdata = cgiGet( sPrefix+"DDO_ESTADO_UF_Loadingdata");
            Ddo_estado_uf_Cleanfilter = cgiGet( sPrefix+"DDO_ESTADO_UF_Cleanfilter");
            Ddo_estado_uf_Noresultsfound = cgiGet( sPrefix+"DDO_ESTADO_UF_Noresultsfound");
            Ddo_estado_uf_Searchbuttontext = cgiGet( sPrefix+"DDO_ESTADO_UF_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_unidadeorganizacional_nome_Activeeventkey = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Activeeventkey");
            Ddo_unidadeorganizacional_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_get");
            Ddo_unidadeorganizacional_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_get");
            Ddo_tpuo_codigo_Activeeventkey = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Activeeventkey");
            Ddo_tpuo_codigo_Filteredtext_get = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Filteredtext_get");
            Ddo_tpuo_codigo_Filteredtextto_get = cgiGet( sPrefix+"DDO_TPUO_CODIGO_Filteredtextto_get");
            Ddo_estado_uf_Activeeventkey = cgiGet( sPrefix+"DDO_ESTADO_UF_Activeeventkey");
            Ddo_estado_uf_Filteredtext_get = cgiGet( sPrefix+"DDO_ESTADO_UF_Filteredtext_get");
            Ddo_estado_uf_Selectedvalue_get = cgiGet( sPrefix+"DDO_ESTADO_UF_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME1"), AV17UnidadeOrganizacional_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME2"), AV20UnidadeOrganizacional_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV22DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME3"), AV23UnidadeOrganizacional_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV21DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME"), AV33TFUnidadeOrganizacional_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME_SEL"), AV34TFUnidadeOrganizacional_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFTPUO_CODIGO"), ",", ".") != Convert.ToDecimal( AV37TFTpUo_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFTPUO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV38TFTpUo_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFESTADO_UF"), AV41TFEstado_UF) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFESTADO_UF_SEL"), AV42TFEstado_UF_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25D32 */
         E25D32 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25D32( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersSelector3 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfunidadeorganizacional_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfunidadeorganizacional_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacional_nome_Visible), 5, 0)));
         edtavTfunidadeorganizacional_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfunidadeorganizacional_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacional_nome_sel_Visible), 5, 0)));
         edtavTftpuo_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTftpuo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftpuo_codigo_Visible), 5, 0)));
         edtavTftpuo_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTftpuo_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftpuo_codigo_to_Visible), 5, 0)));
         edtavTfestado_uf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfestado_uf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_uf_Visible), 5, 0)));
         edtavTfestado_uf_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfestado_uf_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_uf_sel_Visible), 5, 0)));
         Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_UnidadeOrganizacional_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "TitleControlIdToReplace", Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace);
         AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace", AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace);
         edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tpuo_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_TpUo_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tpuo_codigo_Internalname, "TitleControlIdToReplace", Ddo_tpuo_codigo_Titlecontrolidtoreplace);
         AV39ddo_TpUo_CodigoTitleControlIdToReplace = Ddo_tpuo_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_TpUo_CodigoTitleControlIdToReplace", AV39ddo_TpUo_CodigoTitleControlIdToReplace);
         edtavDdo_tpuo_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_tpuo_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tpuo_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_estado_uf_Titlecontrolidtoreplace = subGrid_Internalname+"_Estado_UF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "TitleControlIdToReplace", Ddo_estado_uf_Titlecontrolidtoreplace);
         AV43ddo_Estado_UFTitleControlIdToReplace = Ddo_estado_uf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43ddo_Estado_UFTitleControlIdToReplace", AV43ddo_Estado_UFTitleControlIdToReplace);
         edtavDdo_estado_uftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_estado_uftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_estado_uftitlecontrolidtoreplace_Visible), 5, 0)));
         edtUnidadeOrganizacional_Vinculada_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUnidadeOrganizacional_Vinculada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeOrganizacional_Vinculada_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Organizacional", 0);
         cmbavOrderedby.addItem("2", "Tipo UO", 0);
         cmbavOrderedby.addItem("3", "UF", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV44DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV44DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26D32( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV32UnidadeOrganizacional_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36TpUo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40Estado_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUnidadeOrganizacional_Nome_Titleformat = 2;
         edtUnidadeOrganizacional_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUnidadeOrganizacional_Nome_Internalname, "Title", edtUnidadeOrganizacional_Nome_Title);
         edtTpUo_Codigo_Titleformat = 2;
         edtTpUo_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo UO", AV39ddo_TpUo_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTpUo_Codigo_Internalname, "Title", edtTpUo_Codigo_Title);
         edtEstado_UF_Titleformat = 2;
         edtEstado_UF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "UF", AV43ddo_Estado_UFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtEstado_UF_Internalname, "Title", edtEstado_UF_Title);
         AV46GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46GridCurrentPage), 10, 0)));
         AV47GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV32UnidadeOrganizacional_NomeTitleFilterData", AV32UnidadeOrganizacional_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV36TpUo_CodigoTitleFilterData", AV36TpUo_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV40Estado_UFTitleFilterData", AV40Estado_UFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11D32( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV45PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV45PageToGo) ;
         }
      }

      protected void E12D32( )
      {
         /* Ddo_unidadeorganizacional_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_unidadeorganizacional_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_unidadeorganizacional_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV33TFUnidadeOrganizacional_Nome = Ddo_unidadeorganizacional_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFUnidadeOrganizacional_Nome", AV33TFUnidadeOrganizacional_Nome);
            AV34TFUnidadeOrganizacional_Nome_Sel = Ddo_unidadeorganizacional_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFUnidadeOrganizacional_Nome_Sel", AV34TFUnidadeOrganizacional_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13D32( )
      {
         /* Ddo_tpuo_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tpuo_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tpuo_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tpuo_codigo_Internalname, "SortedStatus", Ddo_tpuo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_tpuo_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tpuo_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tpuo_codigo_Internalname, "SortedStatus", Ddo_tpuo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_tpuo_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFTpUo_Codigo = (int)(NumberUtil.Val( Ddo_tpuo_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFTpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFTpUo_Codigo), 6, 0)));
            AV38TFTpUo_Codigo_To = (int)(NumberUtil.Val( Ddo_tpuo_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFTpUo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFTpUo_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14D32( )
      {
         /* Ddo_estado_uf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_estado_uf_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_estado_uf_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFEstado_UF = Ddo_estado_uf_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFEstado_UF", AV41TFEstado_UF);
            AV42TFEstado_UF_Sel = Ddo_estado_uf_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFEstado_UF_Sel", AV42TFEstado_UF_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27D32( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 71;
         }
         sendrow_712( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_71_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(71, GridRow);
         }
      }

      protected void E15D32( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E20D32( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16D32( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV24DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         AV25DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersIgnoreFirst", AV25DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         AV25DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersIgnoreFirst", AV25DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17UnidadeOrganizacional_Nome1, AV19DynamicFiltersSelector2, AV20UnidadeOrganizacional_Nome2, AV22DynamicFiltersSelector3, AV23UnidadeOrganizacional_Nome3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV33TFUnidadeOrganizacional_Nome, AV34TFUnidadeOrganizacional_Nome_Sel, AV37TFTpUo_Codigo, AV38TFTpUo_Codigo_To, AV41TFEstado_UF, AV42TFEstado_UF_Sel, AV7UnidadeOrganizacional_Vinculada, AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV39ddo_TpUo_CodigoTitleControlIdToReplace, AV43ddo_Estado_UFTitleControlIdToReplace, AV50Pgmname, AV29UnidadeOrganizacional_Ativo, AV11GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21D32( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22D32( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV21DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17D32( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV24DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17UnidadeOrganizacional_Nome1, AV19DynamicFiltersSelector2, AV20UnidadeOrganizacional_Nome2, AV22DynamicFiltersSelector3, AV23UnidadeOrganizacional_Nome3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV33TFUnidadeOrganizacional_Nome, AV34TFUnidadeOrganizacional_Nome_Sel, AV37TFTpUo_Codigo, AV38TFTpUo_Codigo_To, AV41TFEstado_UF, AV42TFEstado_UF_Sel, AV7UnidadeOrganizacional_Vinculada, AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV39ddo_TpUo_CodigoTitleControlIdToReplace, AV43ddo_Estado_UFTitleControlIdToReplace, AV50Pgmname, AV29UnidadeOrganizacional_Ativo, AV11GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23D32( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18D32( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV24DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         AV21DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17UnidadeOrganizacional_Nome1, AV19DynamicFiltersSelector2, AV20UnidadeOrganizacional_Nome2, AV22DynamicFiltersSelector3, AV23UnidadeOrganizacional_Nome3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV33TFUnidadeOrganizacional_Nome, AV34TFUnidadeOrganizacional_Nome_Sel, AV37TFTpUo_Codigo, AV38TFTpUo_Codigo_To, AV41TFEstado_UF, AV42TFEstado_UF_Sel, AV7UnidadeOrganizacional_Vinculada, AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV39ddo_TpUo_CodigoTitleControlIdToReplace, AV43ddo_Estado_UFTitleControlIdToReplace, AV50Pgmname, AV29UnidadeOrganizacional_Ativo, AV11GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24D32( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19D32( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_unidadeorganizacional_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
         Ddo_tpuo_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tpuo_codigo_Internalname, "SortedStatus", Ddo_tpuo_codigo_Sortedstatus);
         Ddo_estado_uf_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_unidadeorganizacional_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_tpuo_codigo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tpuo_codigo_Internalname, "SortedStatus", Ddo_tpuo_codigo_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_estado_uf_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavUnidadeorganizacional_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavUnidadeorganizacional_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavUnidadeorganizacional_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20UnidadeOrganizacional_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_Nome2", AV20UnidadeOrganizacional_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         AV22DynamicFiltersSelector3 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         AV23UnidadeOrganizacional_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome3", AV23UnidadeOrganizacional_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV29UnidadeOrganizacional_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29UnidadeOrganizacional_Ativo", AV29UnidadeOrganizacional_Ativo);
         AV33TFUnidadeOrganizacional_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFUnidadeOrganizacional_Nome", AV33TFUnidadeOrganizacional_Nome);
         Ddo_unidadeorganizacional_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "FilteredText_set", Ddo_unidadeorganizacional_nome_Filteredtext_set);
         AV34TFUnidadeOrganizacional_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFUnidadeOrganizacional_Nome_Sel", AV34TFUnidadeOrganizacional_Nome_Sel);
         Ddo_unidadeorganizacional_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SelectedValue_set", Ddo_unidadeorganizacional_nome_Selectedvalue_set);
         AV37TFTpUo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFTpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFTpUo_Codigo), 6, 0)));
         Ddo_tpuo_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tpuo_codigo_Internalname, "FilteredText_set", Ddo_tpuo_codigo_Filteredtext_set);
         AV38TFTpUo_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFTpUo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFTpUo_Codigo_To), 6, 0)));
         Ddo_tpuo_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tpuo_codigo_Internalname, "FilteredTextTo_set", Ddo_tpuo_codigo_Filteredtextto_set);
         AV41TFEstado_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFEstado_UF", AV41TFEstado_UF);
         Ddo_estado_uf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "FilteredText_set", Ddo_estado_uf_Filteredtext_set);
         AV42TFEstado_UF_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFEstado_UF_Sel", AV42TFEstado_UF_Sel);
         Ddo_estado_uf_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SelectedValue_set", Ddo_estado_uf_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17UnidadeOrganizacional_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17UnidadeOrganizacional_Nome1", AV17UnidadeOrganizacional_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV28Session.Get(AV50Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV50Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV28Session.Get(AV50Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV51GXV1 = 1;
         while ( AV51GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV51GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "UNIDADEORGANIZACIONAL_ATIVO") == 0 )
            {
               AV29UnidadeOrganizacional_Ativo = BooleanUtil.Val( AV12GridStateFilterValue.gxTpr_Value);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29UnidadeOrganizacional_Ativo", AV29UnidadeOrganizacional_Ativo);
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV33TFUnidadeOrganizacional_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFUnidadeOrganizacional_Nome", AV33TFUnidadeOrganizacional_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFUnidadeOrganizacional_Nome)) )
               {
                  Ddo_unidadeorganizacional_nome_Filteredtext_set = AV33TFUnidadeOrganizacional_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "FilteredText_set", Ddo_unidadeorganizacional_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME_SEL") == 0 )
            {
               AV34TFUnidadeOrganizacional_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFUnidadeOrganizacional_Nome_Sel", AV34TFUnidadeOrganizacional_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFUnidadeOrganizacional_Nome_Sel)) )
               {
                  Ddo_unidadeorganizacional_nome_Selectedvalue_set = AV34TFUnidadeOrganizacional_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SelectedValue_set", Ddo_unidadeorganizacional_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFTPUO_CODIGO") == 0 )
            {
               AV37TFTpUo_Codigo = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFTpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFTpUo_Codigo), 6, 0)));
               AV38TFTpUo_Codigo_To = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFTpUo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFTpUo_Codigo_To), 6, 0)));
               if ( ! (0==AV37TFTpUo_Codigo) )
               {
                  Ddo_tpuo_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV37TFTpUo_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tpuo_codigo_Internalname, "FilteredText_set", Ddo_tpuo_codigo_Filteredtext_set);
               }
               if ( ! (0==AV38TFTpUo_Codigo_To) )
               {
                  Ddo_tpuo_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV38TFTpUo_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tpuo_codigo_Internalname, "FilteredTextTo_set", Ddo_tpuo_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFESTADO_UF") == 0 )
            {
               AV41TFEstado_UF = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFEstado_UF", AV41TFEstado_UF);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFEstado_UF)) )
               {
                  Ddo_estado_uf_Filteredtext_set = AV41TFEstado_UF;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "FilteredText_set", Ddo_estado_uf_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFESTADO_UF_SEL") == 0 )
            {
               AV42TFEstado_UF_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFEstado_UF_Sel", AV42TFEstado_UF_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFEstado_UF_Sel)) )
               {
                  Ddo_estado_uf_Selectedvalue_set = AV42TFEstado_UF_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SelectedValue_set", Ddo_estado_uf_Selectedvalue_set);
               }
            }
            AV51GXV1 = (int)(AV51GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV17UnidadeOrganizacional_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17UnidadeOrganizacional_Nome1", AV17UnidadeOrganizacional_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
               {
                  AV20UnidadeOrganizacional_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_Nome2", AV20UnidadeOrganizacional_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV21DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV22DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
                  {
                     AV23UnidadeOrganizacional_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome3", AV23UnidadeOrganizacional_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV24DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV28Session.Get(AV50Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! (false==AV29UnidadeOrganizacional_Ativo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "UNIDADEORGANIZACIONAL_ATIVO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.BoolToStr( AV29UnidadeOrganizacional_Ativo);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFUnidadeOrganizacional_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUNIDADEORGANIZACIONAL_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV33TFUnidadeOrganizacional_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFUnidadeOrganizacional_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUNIDADEORGANIZACIONAL_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV34TFUnidadeOrganizacional_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV37TFTpUo_Codigo) && (0==AV38TFTpUo_Codigo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFTPUO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV37TFTpUo_Codigo), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV38TFTpUo_Codigo_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFEstado_UF)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFESTADO_UF";
            AV12GridStateFilterValue.gxTpr_Value = AV41TFEstado_UF;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFEstado_UF_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFESTADO_UF_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV42TFEstado_UF_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7UnidadeOrganizacional_Vinculada) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&UNIDADEORGANIZACIONAL_VINCULADA";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7UnidadeOrganizacional_Vinculada), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV50Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV25DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17UnidadeOrganizacional_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV17UnidadeOrganizacional_Nome1;
            }
            if ( AV24DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV20UnidadeOrganizacional_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV20UnidadeOrganizacional_Nome2;
            }
            if ( AV24DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV21DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV22DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23UnidadeOrganizacional_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV23UnidadeOrganizacional_Nome3;
            }
            if ( AV24DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV50Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Geral_UnidadeOrganizacional";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "UnidadeOrganizacional_Vinculada";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7UnidadeOrganizacional_Vinculada), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV28Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_D32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_D32( true) ;
         }
         else
         {
            wb_table2_8_D32( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_D32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_68_D32( true) ;
         }
         else
         {
            wb_table3_68_D32( false) ;
         }
         return  ;
      }

      protected void wb_table3_68_D32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_D32e( true) ;
         }
         else
         {
            wb_table1_2_D32e( false) ;
         }
      }

      protected void wb_table3_68_D32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"71\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Organizacional_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUnidadeOrganizacional_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtUnidadeOrganizacional_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUnidadeOrganizacional_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTpUo_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtTpUo_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTpUo_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtEstado_UF_Titleformat == 0 )
               {
                  context.SendWebValue( edtEstado_UF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtEstado_UF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A612UnidadeOrganizacional_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUnidadeOrganizacional_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUnidadeOrganizacional_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A609TpUo_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTpUo_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTpUo_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A23Estado_UF));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtEstado_UF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtEstado_UF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 71 )
         {
            wbEnd = 0;
            nRC_GXsfl_71 = (short)(nGXsfl_71_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_68_D32e( true) ;
         }
         else
         {
            wb_table3_68_D32e( false) ;
         }
      }

      protected void wb_table2_8_D32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_D32( true) ;
         }
         else
         {
            wb_table4_11_D32( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_D32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "", true, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_20_D32( true) ;
         }
         else
         {
            wb_table5_20_D32( false) ;
         }
         return  ;
      }

      protected void wb_table5_20_D32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_D32e( true) ;
         }
         else
         {
            wb_table2_8_D32e( false) ;
         }
      }

      protected void wb_table5_20_D32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextunidadeorganizacional_ativo_Internalname, "Ativa?", "", "", lblFiltertextunidadeorganizacional_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavUnidadeorganizacional_ativo_Internalname, StringUtil.BoolToStr( AV29UnidadeOrganizacional_Ativo), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(27, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_29_D32( true) ;
         }
         else
         {
            wb_table6_29_D32( false) ;
         }
         return  ;
      }

      protected void wb_table6_29_D32e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_20_D32e( true) ;
         }
         else
         {
            wb_table5_20_D32e( false) ;
         }
      }

      protected void wb_table6_29_D32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome1_Internalname, StringUtil.RTrim( AV17UnidadeOrganizacional_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17UnidadeOrganizacional_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome2_Internalname, StringUtil.RTrim( AV20UnidadeOrganizacional_Nome2), StringUtil.RTrim( context.localUtil.Format( AV20UnidadeOrganizacional_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV22DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome3_Internalname, StringUtil.RTrim( AV23UnidadeOrganizacional_Nome3), StringUtil.RTrim( context.localUtil.Format( AV23UnidadeOrganizacional_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,62);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_29_D32e( true) ;
         }
         else
         {
            wb_table6_29_D32e( false) ;
         }
      }

      protected void wb_table4_11_D32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_D32e( true) ;
         }
         else
         {
            wb_table4_11_D32e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7UnidadeOrganizacional_Vinculada = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UnidadeOrganizacional_Vinculada), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAD32( ) ;
         WSD32( ) ;
         WED32( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7UnidadeOrganizacional_Vinculada = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAD32( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "geral_unidadeorganizacionalgeral_unidadeorganizacionalwc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAD32( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7UnidadeOrganizacional_Vinculada = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UnidadeOrganizacional_Vinculada), 6, 0)));
         }
         wcpOAV7UnidadeOrganizacional_Vinculada = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7UnidadeOrganizacional_Vinculada"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7UnidadeOrganizacional_Vinculada != wcpOAV7UnidadeOrganizacional_Vinculada ) ) )
         {
            setjustcreated();
         }
         wcpOAV7UnidadeOrganizacional_Vinculada = AV7UnidadeOrganizacional_Vinculada;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7UnidadeOrganizacional_Vinculada = cgiGet( sPrefix+"AV7UnidadeOrganizacional_Vinculada_CTRL");
         if ( StringUtil.Len( sCtrlAV7UnidadeOrganizacional_Vinculada) > 0 )
         {
            AV7UnidadeOrganizacional_Vinculada = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7UnidadeOrganizacional_Vinculada), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UnidadeOrganizacional_Vinculada), 6, 0)));
         }
         else
         {
            AV7UnidadeOrganizacional_Vinculada = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7UnidadeOrganizacional_Vinculada_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAD32( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSD32( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSD32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7UnidadeOrganizacional_Vinculada_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7UnidadeOrganizacional_Vinculada), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7UnidadeOrganizacional_Vinculada)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7UnidadeOrganizacional_Vinculada_CTRL", StringUtil.RTrim( sCtrlAV7UnidadeOrganizacional_Vinculada));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WED32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205211823048");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("geral_unidadeorganizacionalgeral_unidadeorganizacionalwc.js", "?20205211823048");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_712( )
      {
         edtUnidadeOrganizacional_Codigo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_CODIGO_"+sGXsfl_71_idx;
         edtUnidadeOrganizacional_Nome_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_NOME_"+sGXsfl_71_idx;
         edtTpUo_Codigo_Internalname = sPrefix+"TPUO_CODIGO_"+sGXsfl_71_idx;
         edtEstado_UF_Internalname = sPrefix+"ESTADO_UF_"+sGXsfl_71_idx;
      }

      protected void SubsflControlProps_fel_712( )
      {
         edtUnidadeOrganizacional_Codigo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_CODIGO_"+sGXsfl_71_fel_idx;
         edtUnidadeOrganizacional_Nome_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_NOME_"+sGXsfl_71_fel_idx;
         edtTpUo_Codigo_Internalname = sPrefix+"TPUO_CODIGO_"+sGXsfl_71_fel_idx;
         edtEstado_UF_Internalname = sPrefix+"ESTADO_UF_"+sGXsfl_71_fel_idx;
      }

      protected void sendrow_712( )
      {
         SubsflControlProps_712( ) ;
         WBD30( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_71_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_71_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_71_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeOrganizacional_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUnidadeOrganizacional_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)71,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeOrganizacional_Nome_Internalname,StringUtil.RTrim( A612UnidadeOrganizacional_Nome),StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUnidadeOrganizacional_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)71,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTpUo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A609TpUo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A609TpUo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTpUo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)71,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_UF_Internalname,StringUtil.RTrim( A23Estado_UF),StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEstado_UF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)71,(short)1,(short)-1,(short)-1,(bool)true,(String)"UF",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_CODIGO"+"_"+sGXsfl_71_idx, GetSecureSignedToken( sPrefix+sGXsfl_71_idx, context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_NOME"+"_"+sGXsfl_71_idx, GetSecureSignedToken( sPrefix+sGXsfl_71_idx, StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TPUO_CODIGO"+"_"+sGXsfl_71_idx, GetSecureSignedToken( sPrefix+sGXsfl_71_idx, context.localUtil.Format( (decimal)(A609TpUo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ESTADO_UF"+"_"+sGXsfl_71_idx, GetSecureSignedToken( sPrefix+sGXsfl_71_idx, StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_71_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_71_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_71_idx+1));
            sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
            SubsflControlProps_712( ) ;
         }
         /* End function sendrow_712 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblFiltertextunidadeorganizacional_ativo_Internalname = sPrefix+"FILTERTEXTUNIDADEORGANIZACIONAL_ATIVO";
         chkavUnidadeorganizacional_ativo_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_ATIVO";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavUnidadeorganizacional_nome1_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_NOME1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         edtavUnidadeorganizacional_nome2_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_NOME2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         edtavUnidadeorganizacional_nome3_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_NOME3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtUnidadeOrganizacional_Codigo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_CODIGO";
         edtUnidadeOrganizacional_Nome_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_NOME";
         edtTpUo_Codigo_Internalname = sPrefix+"TPUO_CODIGO";
         edtEstado_UF_Internalname = sPrefix+"ESTADO_UF";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtUnidadeOrganizacional_Vinculada_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_VINCULADA";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         edtavTfunidadeorganizacional_nome_Internalname = sPrefix+"vTFUNIDADEORGANIZACIONAL_NOME";
         edtavTfunidadeorganizacional_nome_sel_Internalname = sPrefix+"vTFUNIDADEORGANIZACIONAL_NOME_SEL";
         edtavTftpuo_codigo_Internalname = sPrefix+"vTFTPUO_CODIGO";
         edtavTftpuo_codigo_to_Internalname = sPrefix+"vTFTPUO_CODIGO_TO";
         edtavTfestado_uf_Internalname = sPrefix+"vTFESTADO_UF";
         edtavTfestado_uf_sel_Internalname = sPrefix+"vTFESTADO_UF_SEL";
         Ddo_unidadeorganizacional_nome_Internalname = sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME";
         edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE";
         Ddo_tpuo_codigo_Internalname = sPrefix+"DDO_TPUO_CODIGO";
         edtavDdo_tpuo_codigotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_TPUO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_estado_uf_Internalname = sPrefix+"DDO_ESTADO_UF";
         edtavDdo_estado_uftitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtEstado_UF_Jsonclick = "";
         edtTpUo_Codigo_Jsonclick = "";
         edtUnidadeOrganizacional_Nome_Jsonclick = "";
         edtUnidadeOrganizacional_Codigo_Jsonclick = "";
         edtavUnidadeorganizacional_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavUnidadeorganizacional_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavUnidadeorganizacional_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtEstado_UF_Titleformat = 0;
         edtTpUo_Codigo_Titleformat = 0;
         edtUnidadeOrganizacional_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavUnidadeorganizacional_nome3_Visible = 1;
         edtavUnidadeorganizacional_nome2_Visible = 1;
         edtavUnidadeorganizacional_nome1_Visible = 1;
         edtEstado_UF_Title = "UF";
         edtTpUo_Codigo_Title = "Tipo UO";
         edtUnidadeOrganizacional_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavUnidadeorganizacional_ativo.Caption = "";
         edtavDdo_estado_uftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tpuo_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfestado_uf_sel_Jsonclick = "";
         edtavTfestado_uf_sel_Visible = 1;
         edtavTfestado_uf_Jsonclick = "";
         edtavTfestado_uf_Visible = 1;
         edtavTftpuo_codigo_to_Jsonclick = "";
         edtavTftpuo_codigo_to_Visible = 1;
         edtavTftpuo_codigo_Jsonclick = "";
         edtavTftpuo_codigo_Visible = 1;
         edtavTfunidadeorganizacional_nome_sel_Jsonclick = "";
         edtavTfunidadeorganizacional_nome_sel_Visible = 1;
         edtavTfunidadeorganizacional_nome_Jsonclick = "";
         edtavTfunidadeorganizacional_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtUnidadeOrganizacional_Vinculada_Jsonclick = "";
         edtUnidadeOrganizacional_Vinculada_Visible = 1;
         Ddo_estado_uf_Searchbuttontext = "Pesquisar";
         Ddo_estado_uf_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_estado_uf_Cleanfilter = "Limpar pesquisa";
         Ddo_estado_uf_Loadingdata = "Carregando dados...";
         Ddo_estado_uf_Sortdsc = "Ordenar de Z � A";
         Ddo_estado_uf_Sortasc = "Ordenar de A � Z";
         Ddo_estado_uf_Datalistupdateminimumcharacters = 0;
         Ddo_estado_uf_Datalistproc = "GetGeral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWCFilterData";
         Ddo_estado_uf_Datalisttype = "Dynamic";
         Ddo_estado_uf_Includedatalist = Convert.ToBoolean( -1);
         Ddo_estado_uf_Filterisrange = Convert.ToBoolean( 0);
         Ddo_estado_uf_Filtertype = "Character";
         Ddo_estado_uf_Includefilter = Convert.ToBoolean( -1);
         Ddo_estado_uf_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_estado_uf_Includesortasc = Convert.ToBoolean( -1);
         Ddo_estado_uf_Titlecontrolidtoreplace = "";
         Ddo_estado_uf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_estado_uf_Cls = "ColumnSettings";
         Ddo_estado_uf_Tooltip = "Op��es";
         Ddo_estado_uf_Caption = "";
         Ddo_tpuo_codigo_Searchbuttontext = "Pesquisar";
         Ddo_tpuo_codigo_Rangefilterto = "At�";
         Ddo_tpuo_codigo_Rangefilterfrom = "Desde";
         Ddo_tpuo_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_tpuo_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_tpuo_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_tpuo_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_tpuo_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_tpuo_codigo_Filtertype = "Numeric";
         Ddo_tpuo_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_tpuo_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tpuo_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tpuo_codigo_Titlecontrolidtoreplace = "";
         Ddo_tpuo_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tpuo_codigo_Cls = "ColumnSettings";
         Ddo_tpuo_codigo_Tooltip = "Op��es";
         Ddo_tpuo_codigo_Caption = "";
         Ddo_unidadeorganizacional_nome_Searchbuttontext = "Pesquisar";
         Ddo_unidadeorganizacional_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_unidadeorganizacional_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_unidadeorganizacional_nome_Loadingdata = "Carregando dados...";
         Ddo_unidadeorganizacional_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_unidadeorganizacional_nome_Sortasc = "Ordenar de A � Z";
         Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters = 0;
         Ddo_unidadeorganizacional_nome_Datalistproc = "GetGeral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWCFilterData";
         Ddo_unidadeorganizacional_nome_Datalisttype = "Dynamic";
         Ddo_unidadeorganizacional_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_unidadeorganizacional_nome_Filtertype = "Character";
         Ddo_unidadeorganizacional_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace = "";
         Ddo_unidadeorganizacional_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_unidadeorganizacional_nome_Cls = "ColumnSettings";
         Ddo_unidadeorganizacional_nome_Tooltip = "Op��es";
         Ddo_unidadeorganizacional_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TpUo_CodigoTitleControlIdToReplace',fld:'vDDO_TPUO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV29UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV7UnidadeOrganizacional_Vinculada',fld:'vUNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''}],oparms:[{av:'AV32UnidadeOrganizacional_NomeTitleFilterData',fld:'vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV36TpUo_CodigoTitleFilterData',fld:'vTPUO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV40Estado_UFTitleFilterData',fld:'vESTADO_UFTITLEFILTERDATA',pic:'',nv:null},{av:'edtUnidadeOrganizacional_Nome_Titleformat',ctrl:'UNIDADEORGANIZACIONAL_NOME',prop:'Titleformat'},{av:'edtUnidadeOrganizacional_Nome_Title',ctrl:'UNIDADEORGANIZACIONAL_NOME',prop:'Title'},{av:'edtTpUo_Codigo_Titleformat',ctrl:'TPUO_CODIGO',prop:'Titleformat'},{av:'edtTpUo_Codigo_Title',ctrl:'TPUO_CODIGO',prop:'Title'},{av:'edtEstado_UF_Titleformat',ctrl:'ESTADO_UF',prop:'Titleformat'},{av:'edtEstado_UF_Title',ctrl:'ESTADO_UF',prop:'Title'},{av:'AV46GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV47GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11D32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV7UnidadeOrganizacional_Vinculada',fld:'vUNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TpUo_CodigoTitleControlIdToReplace',fld:'vDDO_TPUO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_UNIDADEORGANIZACIONAL_NOME.ONOPTIONCLICKED","{handler:'E12D32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV7UnidadeOrganizacional_Vinculada',fld:'vUNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TpUo_CodigoTitleControlIdToReplace',fld:'vDDO_TPUO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Ddo_unidadeorganizacional_nome_Activeeventkey',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'ActiveEventKey'},{av:'Ddo_unidadeorganizacional_nome_Filteredtext_get',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'FilteredText_get'},{av:'Ddo_unidadeorganizacional_nome_Selectedvalue_get',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_unidadeorganizacional_nome_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SortedStatus'},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tpuo_codigo_Sortedstatus',ctrl:'DDO_TPUO_CODIGO',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TPUO_CODIGO.ONOPTIONCLICKED","{handler:'E13D32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV7UnidadeOrganizacional_Vinculada',fld:'vUNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TpUo_CodigoTitleControlIdToReplace',fld:'vDDO_TPUO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Ddo_tpuo_codigo_Activeeventkey',ctrl:'DDO_TPUO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_tpuo_codigo_Filteredtext_get',ctrl:'DDO_TPUO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_tpuo_codigo_Filteredtextto_get',ctrl:'DDO_TPUO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tpuo_codigo_Sortedstatus',ctrl:'DDO_TPUO_CODIGO',prop:'SortedStatus'},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_unidadeorganizacional_nome_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ESTADO_UF.ONOPTIONCLICKED","{handler:'E14D32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV7UnidadeOrganizacional_Vinculada',fld:'vUNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TpUo_CodigoTitleControlIdToReplace',fld:'vDDO_TPUO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Ddo_estado_uf_Activeeventkey',ctrl:'DDO_ESTADO_UF',prop:'ActiveEventKey'},{av:'Ddo_estado_uf_Filteredtext_get',ctrl:'DDO_ESTADO_UF',prop:'FilteredText_get'},{av:'Ddo_estado_uf_Selectedvalue_get',ctrl:'DDO_ESTADO_UF',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'Ddo_unidadeorganizacional_nome_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SortedStatus'},{av:'Ddo_tpuo_codigo_Sortedstatus',ctrl:'DDO_TPUO_CODIGO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27D32',iparms:[],oparms:[]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15D32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV7UnidadeOrganizacional_Vinculada',fld:'vUNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TpUo_CodigoTitleControlIdToReplace',fld:'vDDO_TPUO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20D32',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16D32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV7UnidadeOrganizacional_Vinculada',fld:'vUNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TpUo_CodigoTitleControlIdToReplace',fld:'vDDO_TPUO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21D32',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22D32',iparms:[],oparms:[{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17D32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV7UnidadeOrganizacional_Vinculada',fld:'vUNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TpUo_CodigoTitleControlIdToReplace',fld:'vDDO_TPUO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23D32',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18D32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV7UnidadeOrganizacional_Vinculada',fld:'vUNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TpUo_CodigoTitleControlIdToReplace',fld:'vDDO_TPUO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24D32',iparms:[{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19D32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV7UnidadeOrganizacional_Vinculada',fld:'vUNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TpUo_CodigoTitleControlIdToReplace',fld:'vDDO_TPUO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV29UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV33TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'Ddo_unidadeorganizacional_nome_Filteredtext_set',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'FilteredText_set'},{av:'AV34TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_unidadeorganizacional_nome_Selectedvalue_set',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SelectedValue_set'},{av:'AV37TFTpUo_Codigo',fld:'vTFTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_tpuo_codigo_Filteredtext_set',ctrl:'DDO_TPUO_CODIGO',prop:'FilteredText_set'},{av:'AV38TFTpUo_Codigo_To',fld:'vTFTPUO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_tpuo_codigo_Filteredtextto_set',ctrl:'DDO_TPUO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV41TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'Ddo_estado_uf_Filteredtext_set',ctrl:'DDO_ESTADO_UF',prop:'FilteredText_set'},{av:'AV42TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'Ddo_estado_uf_Selectedvalue_set',ctrl:'DDO_ESTADO_UF',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_unidadeorganizacional_nome_Activeeventkey = "";
         Ddo_unidadeorganizacional_nome_Filteredtext_get = "";
         Ddo_unidadeorganizacional_nome_Selectedvalue_get = "";
         Ddo_tpuo_codigo_Activeeventkey = "";
         Ddo_tpuo_codigo_Filteredtext_get = "";
         Ddo_tpuo_codigo_Filteredtextto_get = "";
         Ddo_estado_uf_Activeeventkey = "";
         Ddo_estado_uf_Filteredtext_get = "";
         Ddo_estado_uf_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV17UnidadeOrganizacional_Nome1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV20UnidadeOrganizacional_Nome2 = "";
         AV22DynamicFiltersSelector3 = "";
         AV23UnidadeOrganizacional_Nome3 = "";
         AV33TFUnidadeOrganizacional_Nome = "";
         AV34TFUnidadeOrganizacional_Nome_Sel = "";
         AV41TFEstado_UF = "";
         AV42TFEstado_UF_Sel = "";
         AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = "";
         AV39ddo_TpUo_CodigoTitleControlIdToReplace = "";
         AV43ddo_Estado_UFTitleControlIdToReplace = "";
         AV50Pgmname = "";
         AV29UnidadeOrganizacional_Ativo = true;
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV44DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV32UnidadeOrganizacional_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36TpUo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40Estado_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_unidadeorganizacional_nome_Filteredtext_set = "";
         Ddo_unidadeorganizacional_nome_Selectedvalue_set = "";
         Ddo_unidadeorganizacional_nome_Sortedstatus = "";
         Ddo_tpuo_codigo_Filteredtext_set = "";
         Ddo_tpuo_codigo_Filteredtextto_set = "";
         Ddo_tpuo_codigo_Sortedstatus = "";
         Ddo_estado_uf_Filteredtext_set = "";
         Ddo_estado_uf_Selectedvalue_set = "";
         Ddo_estado_uf_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A612UnidadeOrganizacional_Nome = "";
         A23Estado_UF = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17UnidadeOrganizacional_Nome1 = "";
         lV20UnidadeOrganizacional_Nome2 = "";
         lV23UnidadeOrganizacional_Nome3 = "";
         lV33TFUnidadeOrganizacional_Nome = "";
         lV41TFEstado_UF = "";
         H00D32_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         H00D32_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         H00D32_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         H00D32_A23Estado_UF = new String[] {""} ;
         H00D32_A609TpUo_Codigo = new int[1] ;
         H00D32_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00D32_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00D33_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV28Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextunidadeorganizacional_ativo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7UnidadeOrganizacional_Vinculada = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.geral_unidadeorganizacionalgeral_unidadeorganizacionalwc__default(),
            new Object[][] {
                new Object[] {
               H00D32_A629UnidadeOrganizacional_Ativo, H00D32_A613UnidadeOrganizacional_Vinculada, H00D32_n613UnidadeOrganizacional_Vinculada, H00D32_A23Estado_UF, H00D32_A609TpUo_Codigo, H00D32_A612UnidadeOrganizacional_Nome, H00D32_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               H00D33_AGRID_nRecordCount
               }
            }
         );
         AV50Pgmname = "Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC";
         /* GeneXus formulas. */
         AV50Pgmname = "Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_71 ;
      private short nGXsfl_71_idx=1 ;
      private short AV14OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_71_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtUnidadeOrganizacional_Nome_Titleformat ;
      private short edtTpUo_Codigo_Titleformat ;
      private short edtEstado_UF_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7UnidadeOrganizacional_Vinculada ;
      private int wcpOAV7UnidadeOrganizacional_Vinculada ;
      private int subGrid_Rows ;
      private int AV37TFTpUo_Codigo ;
      private int AV38TFTpUo_Codigo_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters ;
      private int Ddo_estado_uf_Datalistupdateminimumcharacters ;
      private int A613UnidadeOrganizacional_Vinculada ;
      private int edtUnidadeOrganizacional_Vinculada_Visible ;
      private int edtavTfunidadeorganizacional_nome_Visible ;
      private int edtavTfunidadeorganizacional_nome_sel_Visible ;
      private int edtavTftpuo_codigo_Visible ;
      private int edtavTftpuo_codigo_to_Visible ;
      private int edtavTfestado_uf_Visible ;
      private int edtavTfestado_uf_sel_Visible ;
      private int edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tpuo_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_estado_uftitlecontrolidtoreplace_Visible ;
      private int A611UnidadeOrganizacional_Codigo ;
      private int A609TpUo_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV45PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavUnidadeorganizacional_nome1_Visible ;
      private int edtavUnidadeorganizacional_nome2_Visible ;
      private int edtavUnidadeorganizacional_nome3_Visible ;
      private int AV51GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV46GridCurrentPage ;
      private long AV47GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_unidadeorganizacional_nome_Activeeventkey ;
      private String Ddo_unidadeorganizacional_nome_Filteredtext_get ;
      private String Ddo_unidadeorganizacional_nome_Selectedvalue_get ;
      private String Ddo_tpuo_codigo_Activeeventkey ;
      private String Ddo_tpuo_codigo_Filteredtext_get ;
      private String Ddo_tpuo_codigo_Filteredtextto_get ;
      private String Ddo_estado_uf_Activeeventkey ;
      private String Ddo_estado_uf_Filteredtext_get ;
      private String Ddo_estado_uf_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_71_idx="0001" ;
      private String AV17UnidadeOrganizacional_Nome1 ;
      private String AV20UnidadeOrganizacional_Nome2 ;
      private String AV23UnidadeOrganizacional_Nome3 ;
      private String AV33TFUnidadeOrganizacional_Nome ;
      private String AV34TFUnidadeOrganizacional_Nome_Sel ;
      private String AV41TFEstado_UF ;
      private String AV42TFEstado_UF_Sel ;
      private String AV50Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_unidadeorganizacional_nome_Caption ;
      private String Ddo_unidadeorganizacional_nome_Tooltip ;
      private String Ddo_unidadeorganizacional_nome_Cls ;
      private String Ddo_unidadeorganizacional_nome_Filteredtext_set ;
      private String Ddo_unidadeorganizacional_nome_Selectedvalue_set ;
      private String Ddo_unidadeorganizacional_nome_Dropdownoptionstype ;
      private String Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace ;
      private String Ddo_unidadeorganizacional_nome_Sortedstatus ;
      private String Ddo_unidadeorganizacional_nome_Filtertype ;
      private String Ddo_unidadeorganizacional_nome_Datalisttype ;
      private String Ddo_unidadeorganizacional_nome_Datalistproc ;
      private String Ddo_unidadeorganizacional_nome_Sortasc ;
      private String Ddo_unidadeorganizacional_nome_Sortdsc ;
      private String Ddo_unidadeorganizacional_nome_Loadingdata ;
      private String Ddo_unidadeorganizacional_nome_Cleanfilter ;
      private String Ddo_unidadeorganizacional_nome_Noresultsfound ;
      private String Ddo_unidadeorganizacional_nome_Searchbuttontext ;
      private String Ddo_tpuo_codigo_Caption ;
      private String Ddo_tpuo_codigo_Tooltip ;
      private String Ddo_tpuo_codigo_Cls ;
      private String Ddo_tpuo_codigo_Filteredtext_set ;
      private String Ddo_tpuo_codigo_Filteredtextto_set ;
      private String Ddo_tpuo_codigo_Dropdownoptionstype ;
      private String Ddo_tpuo_codigo_Titlecontrolidtoreplace ;
      private String Ddo_tpuo_codigo_Sortedstatus ;
      private String Ddo_tpuo_codigo_Filtertype ;
      private String Ddo_tpuo_codigo_Sortasc ;
      private String Ddo_tpuo_codigo_Sortdsc ;
      private String Ddo_tpuo_codigo_Cleanfilter ;
      private String Ddo_tpuo_codigo_Rangefilterfrom ;
      private String Ddo_tpuo_codigo_Rangefilterto ;
      private String Ddo_tpuo_codigo_Searchbuttontext ;
      private String Ddo_estado_uf_Caption ;
      private String Ddo_estado_uf_Tooltip ;
      private String Ddo_estado_uf_Cls ;
      private String Ddo_estado_uf_Filteredtext_set ;
      private String Ddo_estado_uf_Selectedvalue_set ;
      private String Ddo_estado_uf_Dropdownoptionstype ;
      private String Ddo_estado_uf_Titlecontrolidtoreplace ;
      private String Ddo_estado_uf_Sortedstatus ;
      private String Ddo_estado_uf_Filtertype ;
      private String Ddo_estado_uf_Datalisttype ;
      private String Ddo_estado_uf_Datalistproc ;
      private String Ddo_estado_uf_Sortasc ;
      private String Ddo_estado_uf_Sortdsc ;
      private String Ddo_estado_uf_Loadingdata ;
      private String Ddo_estado_uf_Cleanfilter ;
      private String Ddo_estado_uf_Noresultsfound ;
      private String Ddo_estado_uf_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtUnidadeOrganizacional_Vinculada_Internalname ;
      private String edtUnidadeOrganizacional_Vinculada_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfunidadeorganizacional_nome_Internalname ;
      private String edtavTfunidadeorganizacional_nome_Jsonclick ;
      private String edtavTfunidadeorganizacional_nome_sel_Internalname ;
      private String edtavTfunidadeorganizacional_nome_sel_Jsonclick ;
      private String edtavTftpuo_codigo_Internalname ;
      private String edtavTftpuo_codigo_Jsonclick ;
      private String edtavTftpuo_codigo_to_Internalname ;
      private String edtavTftpuo_codigo_to_Jsonclick ;
      private String edtavTfestado_uf_Internalname ;
      private String edtavTfestado_uf_Jsonclick ;
      private String edtavTfestado_uf_sel_Internalname ;
      private String edtavTfestado_uf_sel_Jsonclick ;
      private String edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tpuo_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_estado_uftitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtUnidadeOrganizacional_Codigo_Internalname ;
      private String A612UnidadeOrganizacional_Nome ;
      private String edtUnidadeOrganizacional_Nome_Internalname ;
      private String edtTpUo_Codigo_Internalname ;
      private String A23Estado_UF ;
      private String edtEstado_UF_Internalname ;
      private String chkavUnidadeorganizacional_ativo_Internalname ;
      private String scmdbuf ;
      private String lV17UnidadeOrganizacional_Nome1 ;
      private String lV20UnidadeOrganizacional_Nome2 ;
      private String lV23UnidadeOrganizacional_Nome3 ;
      private String lV33TFUnidadeOrganizacional_Nome ;
      private String lV41TFEstado_UF ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavUnidadeorganizacional_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavUnidadeorganizacional_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavUnidadeorganizacional_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_unidadeorganizacional_nome_Internalname ;
      private String Ddo_tpuo_codigo_Internalname ;
      private String Ddo_estado_uf_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtUnidadeOrganizacional_Nome_Title ;
      private String edtTpUo_Codigo_Title ;
      private String edtEstado_UF_Title ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextunidadeorganizacional_ativo_Internalname ;
      private String lblFiltertextunidadeorganizacional_ativo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavUnidadeorganizacional_nome1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavUnidadeorganizacional_nome2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavUnidadeorganizacional_nome3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sCtrlAV7UnidadeOrganizacional_Vinculada ;
      private String sGXsfl_71_fel_idx="0001" ;
      private String ROClassString ;
      private String edtUnidadeOrganizacional_Codigo_Jsonclick ;
      private String edtUnidadeOrganizacional_Nome_Jsonclick ;
      private String edtTpUo_Codigo_Jsonclick ;
      private String edtEstado_UF_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV21DynamicFiltersEnabled3 ;
      private bool AV29UnidadeOrganizacional_Ativo ;
      private bool AV25DynamicFiltersIgnoreFirst ;
      private bool AV24DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_unidadeorganizacional_nome_Includesortasc ;
      private bool Ddo_unidadeorganizacional_nome_Includesortdsc ;
      private bool Ddo_unidadeorganizacional_nome_Includefilter ;
      private bool Ddo_unidadeorganizacional_nome_Filterisrange ;
      private bool Ddo_unidadeorganizacional_nome_Includedatalist ;
      private bool Ddo_tpuo_codigo_Includesortasc ;
      private bool Ddo_tpuo_codigo_Includesortdsc ;
      private bool Ddo_tpuo_codigo_Includefilter ;
      private bool Ddo_tpuo_codigo_Filterisrange ;
      private bool Ddo_tpuo_codigo_Includedatalist ;
      private bool Ddo_estado_uf_Includesortasc ;
      private bool Ddo_estado_uf_Includesortdsc ;
      private bool Ddo_estado_uf_Includefilter ;
      private bool Ddo_estado_uf_Filterisrange ;
      private bool Ddo_estado_uf_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A629UnidadeOrganizacional_Ativo ;
      private bool n613UnidadeOrganizacional_Vinculada ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV22DynamicFiltersSelector3 ;
      private String AV35ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace ;
      private String AV39ddo_TpUo_CodigoTitleControlIdToReplace ;
      private String AV43ddo_Estado_UFTitleControlIdToReplace ;
      private IGxSession AV28Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCheckbox chkavUnidadeorganizacional_ativo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00D32_A629UnidadeOrganizacional_Ativo ;
      private int[] H00D32_A613UnidadeOrganizacional_Vinculada ;
      private bool[] H00D32_n613UnidadeOrganizacional_Vinculada ;
      private String[] H00D32_A23Estado_UF ;
      private int[] H00D32_A609TpUo_Codigo ;
      private String[] H00D32_A612UnidadeOrganizacional_Nome ;
      private int[] H00D32_A611UnidadeOrganizacional_Codigo ;
      private long[] H00D33_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV32UnidadeOrganizacional_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36TpUo_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40Estado_UFTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV44DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class geral_unidadeorganizacionalgeral_unidadeorganizacionalwc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00D32( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV17UnidadeOrganizacional_Nome1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             String AV20UnidadeOrganizacional_Nome2 ,
                                             bool AV21DynamicFiltersEnabled3 ,
                                             String AV22DynamicFiltersSelector3 ,
                                             String AV23UnidadeOrganizacional_Nome3 ,
                                             String AV34TFUnidadeOrganizacional_Nome_Sel ,
                                             String AV33TFUnidadeOrganizacional_Nome ,
                                             int AV37TFTpUo_Codigo ,
                                             int AV38TFTpUo_Codigo_To ,
                                             String AV42TFEstado_UF_Sel ,
                                             String AV41TFEstado_UF ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             int A609TpUo_Codigo ,
                                             String A23Estado_UF ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             int AV7UnidadeOrganizacional_Vinculada ,
                                             bool A629UnidadeOrganizacional_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [15] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [UnidadeOrganizacional_Ativo], [UnidadeOrganizacional_Vinculada], [Estado_UF], [TpUo_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Codigo]";
         sFromString = " FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([UnidadeOrganizacional_Vinculada] = @AV7UnidadeOrganizacional_Vinculada)";
         sWhereString = sWhereString + " and ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV17UnidadeOrganizacional_Nome1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV20UnidadeOrganizacional_Nome2 + '%')";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV21DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV23UnidadeOrganizacional_Nome3 + '%')";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV34TFUnidadeOrganizacional_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFUnidadeOrganizacional_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV33TFUnidadeOrganizacional_Nome)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFUnidadeOrganizacional_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] = @AV34TFUnidadeOrganizacional_Nome_Sel)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV37TFTpUo_Codigo) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] >= @AV37TFTpUo_Codigo)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV38TFTpUo_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] <= @AV38TFTpUo_Codigo_To)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV42TFEstado_UF_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFEstado_UF)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] like @lV41TFEstado_UF)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFEstado_UF_Sel)) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV42TFEstado_UF_Sel)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeOrganizacional_Vinculada], [UnidadeOrganizacional_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeOrganizacional_Vinculada] DESC, [UnidadeOrganizacional_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeOrganizacional_Vinculada], [TpUo_Codigo]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeOrganizacional_Vinculada] DESC, [TpUo_Codigo] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeOrganizacional_Vinculada], [Estado_UF]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeOrganizacional_Vinculada] DESC, [Estado_UF] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeOrganizacional_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00D33( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV17UnidadeOrganizacional_Nome1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             String AV20UnidadeOrganizacional_Nome2 ,
                                             bool AV21DynamicFiltersEnabled3 ,
                                             String AV22DynamicFiltersSelector3 ,
                                             String AV23UnidadeOrganizacional_Nome3 ,
                                             String AV34TFUnidadeOrganizacional_Nome_Sel ,
                                             String AV33TFUnidadeOrganizacional_Nome ,
                                             int AV37TFTpUo_Codigo ,
                                             int AV38TFTpUo_Codigo_To ,
                                             String AV42TFEstado_UF_Sel ,
                                             String AV41TFEstado_UF ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             int A609TpUo_Codigo ,
                                             String A23Estado_UF ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             int AV7UnidadeOrganizacional_Vinculada ,
                                             bool A629UnidadeOrganizacional_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [10] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([UnidadeOrganizacional_Vinculada] = @AV7UnidadeOrganizacional_Vinculada)";
         scmdbuf = scmdbuf + " and ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV17UnidadeOrganizacional_Nome1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV20UnidadeOrganizacional_Nome2 + '%')";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV21DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV23UnidadeOrganizacional_Nome3 + '%')";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV34TFUnidadeOrganizacional_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFUnidadeOrganizacional_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV33TFUnidadeOrganizacional_Nome)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFUnidadeOrganizacional_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] = @AV34TFUnidadeOrganizacional_Nome_Sel)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV37TFTpUo_Codigo) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] >= @AV37TFTpUo_Codigo)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV38TFTpUo_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] <= @AV38TFTpUo_Codigo_To)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV42TFEstado_UF_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFEstado_UF)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] like @lV41TFEstado_UF)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFEstado_UF_Sel)) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV42TFEstado_UF_Sel)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00D32(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H00D33(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00D32 ;
          prmH00D32 = new Object[] {
          new Object[] {"@AV7UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV33TFUnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV34TFUnidadeOrganizacional_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV37TFTpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV38TFTpUo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41TFEstado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@AV42TFEstado_UF_Sel",SqlDbType.Char,2,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00D33 ;
          prmH00D33 = new Object[] {
          new Object[] {"@AV7UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV33TFUnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV34TFUnidadeOrganizacional_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV37TFTpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV38TFTpUo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41TFEstado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@AV42TFEstado_UF_Sel",SqlDbType.Char,2,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00D32", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D32,11,0,true,false )
             ,new CursorDef("H00D33", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D33,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

}
