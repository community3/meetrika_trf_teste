/*
               File: PRC_FaturarLiquidarLote
        Description: Faturar Liquidar Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:12.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_faturarliquidarlote : GXProcedure
   {
      public prc_faturarliquidarlote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_faturarliquidarlote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Acao ,
                           int aP1_ContagemResultado_LoteAceiteCod )
      {
         this.AV9Acao = aP0_Acao;
         this.AV87ContagemResultado_LoteAceiteCod = aP1_ContagemResultado_LoteAceiteCod;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_Acao ,
                                 int aP1_ContagemResultado_LoteAceiteCod )
      {
         prc_faturarliquidarlote objprc_faturarliquidarlote;
         objprc_faturarliquidarlote = new prc_faturarliquidarlote();
         objprc_faturarliquidarlote.AV9Acao = aP0_Acao;
         objprc_faturarliquidarlote.AV87ContagemResultado_LoteAceiteCod = aP1_ContagemResultado_LoteAceiteCod;
         objprc_faturarliquidarlote.context.SetSubmitInitialConfig(context);
         objprc_faturarliquidarlote.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_faturarliquidarlote);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_faturarliquidarlote)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV9Acao, "P") == 0 )
         {
            AV90StatusDemanda = "P";
         }
         else if ( StringUtil.StrCmp(AV9Acao, "UP") == 0 )
         {
            AV90StatusDemanda = "O";
         }
         else if ( StringUtil.StrCmp(AV9Acao, "L") == 0 )
         {
            AV90StatusDemanda = "L";
         }
         else if ( StringUtil.StrCmp(AV9Acao, "UL") == 0 )
         {
            AV90StatusDemanda = "P";
         }
         /* Execute user subroutine: 'ALTERARSTATUS' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'ALTERARSTATUS' Routine */
         /* Using cursor P004Y2 */
         pr_default.execute(0, new Object[] {AV87ContagemResultado_LoteAceiteCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = P004Y2_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P004Y2_n597ContagemResultado_LoteAceiteCod[0];
            A484ContagemResultado_StatusDmn = P004Y2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P004Y2_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = P004Y2_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = AV90StatusDemanda;
            n484ContagemResultado_StatusDmn = false;
            BatchSize = 200;
            pr_default.initializeBatch( 1, BatchSize, this, "Executebatchp004y3");
            /* Using cursor P004Y3 */
            pr_default.addRecord(1, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            if ( pr_default.recordCount(1) == pr_default.getBatchSize(1) )
            {
               Executebatchp004y3( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(0);
         }
         if ( pr_default.getBatchSize(1) > 0 )
         {
            Executebatchp004y3( ) ;
         }
         pr_default.close(0);
      }

      protected void Executebatchp004y3( )
      {
         /* Using cursor P004Y3 */
         pr_default.executeBatch(1);
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_FaturarLiquidarLote");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         AV90StatusDemanda = "";
         scmdbuf = "";
         P004Y2_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P004Y2_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P004Y2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004Y2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004Y2_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         P004Y3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004Y3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004Y3_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_faturarliquidarlote__default(),
            new Object[][] {
                new Object[] {
               P004Y2_A597ContagemResultado_LoteAceiteCod, P004Y2_n597ContagemResultado_LoteAceiteCod, P004Y2_A484ContagemResultado_StatusDmn, P004Y2_n484ContagemResultado_StatusDmn, P004Y2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV87ContagemResultado_LoteAceiteCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A456ContagemResultado_Codigo ;
      private int BatchSize ;
      private String AV9Acao ;
      private String AV90StatusDemanda ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private bool returnInSub ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P004Y2_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P004Y2_n597ContagemResultado_LoteAceiteCod ;
      private String[] P004Y2_A484ContagemResultado_StatusDmn ;
      private bool[] P004Y2_n484ContagemResultado_StatusDmn ;
      private int[] P004Y2_A456ContagemResultado_Codigo ;
      private String[] P004Y3_A484ContagemResultado_StatusDmn ;
      private bool[] P004Y3_n484ContagemResultado_StatusDmn ;
      private int[] P004Y3_A456ContagemResultado_Codigo ;
   }

   public class prc_faturarliquidarlote__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new BatchUpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004Y2 ;
          prmP004Y2 = new Object[] {
          new Object[] {"@AV87ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004Y3 ;
          prmP004Y3 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004Y2", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_StatusDmn], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV87ContagemResultado_LoteAceiteCod ORDER BY [ContagemResultado_LoteAceiteCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004Y2,1,0,true,false )
             ,new CursorDef("P004Y3", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004Y3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
