/*
               File: AreaTrabalho_Sistema
        Description: Area Trabalho_Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:4:12.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class areatrabalho_sistema : GXProcedure
   {
      public areatrabalho_sistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public areatrabalho_sistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           out IGxCollection aP1_Gxm2rootcol )
      {
         this.AV5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_AreaTrabalho_Sistema", "GxEv3Up14_MeetrikaVs3", "SdtSDT_AreaTrabalho_Sistema", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_AreaTrabalho_Codigo )
      {
         this.AV5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_AreaTrabalho_Sistema", "GxEv3Up14_MeetrikaVs3", "SdtSDT_AreaTrabalho_Sistema", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 out IGxCollection aP1_Gxm2rootcol )
      {
         areatrabalho_sistema objareatrabalho_sistema;
         objareatrabalho_sistema = new areatrabalho_sistema();
         objareatrabalho_sistema.AV5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objareatrabalho_sistema.Gxm2rootcol = new GxObjectCollection( context, "SDT_AreaTrabalho_Sistema", "GxEv3Up14_MeetrikaVs3", "SdtSDT_AreaTrabalho_Sistema", "GeneXus.Programs") ;
         objareatrabalho_sistema.context.SetSubmitInitialConfig(context);
         objareatrabalho_sistema.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objareatrabalho_sistema);
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((areatrabalho_sistema)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000D2 */
         pr_default.execute(0, new Object[] {AV5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A135Sistema_AreaTrabalhoCod = P000D2_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P000D2_A127Sistema_Codigo[0];
            A416Sistema_Nome = P000D2_A416Sistema_Nome[0];
            A128Sistema_Descricao = P000D2_A128Sistema_Descricao[0];
            n128Sistema_Descricao = P000D2_n128Sistema_Descricao[0];
            A129Sistema_Sigla = P000D2_A129Sistema_Sigla[0];
            Gxm1sdt_areatrabalho_sistema = new SdtSDT_AreaTrabalho_Sistema(context);
            Gxm2rootcol.Add(Gxm1sdt_areatrabalho_sistema, 0);
            Gxm1sdt_areatrabalho_sistema.gxTpr_External_id = A127Sistema_Codigo;
            Gxm1sdt_areatrabalho_sistema.gxTpr_Name = A416Sistema_Nome;
            Gxm1sdt_areatrabalho_sistema.gxTpr_Description = A128Sistema_Descricao;
            Gxm1sdt_areatrabalho_sistema.gxTpr_Acronym = A129Sistema_Sigla;
            Gxm1sdt_areatrabalho_sistema.gxTpr_Cost = 100;
            Gxm1sdt_areatrabalho_sistema.gxTpr_Deadline = DateTimeUtil.ServerNow( context, "DEFAULT");
            Gxm1sdt_areatrabalho_sistema.gxTpr_Created_at = DateTimeUtil.ServerNow( context, "DEFAULT");
            Gxm1sdt_areatrabalho_sistema.gxTpr_Updated_at = DateTimeUtil.ServerNow( context, "DEFAULT");
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000D2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P000D2_A127Sistema_Codigo = new int[1] ;
         P000D2_A416Sistema_Nome = new String[] {""} ;
         P000D2_A128Sistema_Descricao = new String[] {""} ;
         P000D2_n128Sistema_Descricao = new bool[] {false} ;
         P000D2_A129Sistema_Sigla = new String[] {""} ;
         A416Sistema_Nome = "";
         A128Sistema_Descricao = "";
         A129Sistema_Sigla = "";
         Gxm1sdt_areatrabalho_sistema = new SdtSDT_AreaTrabalho_Sistema(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.areatrabalho_sistema__default(),
            new Object[][] {
                new Object[] {
               P000D2_A135Sistema_AreaTrabalhoCod, P000D2_A127Sistema_Codigo, P000D2_A416Sistema_Nome, P000D2_A128Sistema_Descricao, P000D2_n128Sistema_Descricao, P000D2_A129Sistema_Sigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV5AreaTrabalho_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private String scmdbuf ;
      private String A129Sistema_Sigla ;
      private bool n128Sistema_Descricao ;
      private String A128Sistema_Descricao ;
      private String A416Sistema_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000D2_A135Sistema_AreaTrabalhoCod ;
      private int[] P000D2_A127Sistema_Codigo ;
      private String[] P000D2_A416Sistema_Nome ;
      private String[] P000D2_A128Sistema_Descricao ;
      private bool[] P000D2_n128Sistema_Descricao ;
      private String[] P000D2_A129Sistema_Sigla ;
      private IGxCollection aP1_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_AreaTrabalho_Sistema ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_AreaTrabalho_Sistema Gxm1sdt_areatrabalho_sistema ;
   }

   public class areatrabalho_sistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000D2 ;
          prmP000D2 = new Object[] {
          new Object[] {"@AV5AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000D2", "SELECT [Sistema_AreaTrabalhoCod], [Sistema_Codigo], [Sistema_Nome], [Sistema_Descricao], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_AreaTrabalhoCod] = @AV5AreaTrabalho_Codigo ORDER BY [Sistema_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000D2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 25) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.areatrabalho_sistema_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class areatrabalho_sistema_services : GxRestService
 {
    [OperationContract]
    [WebInvoke(Method =  "GET" ,
    	BodyStyle =  WebMessageBodyStyle.Bare  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/?AreaTrabalho_Codigo={AreaTrabalho_Codigo}")]
    public GxGenericCollection<SdtSDT_AreaTrabalho_Sistema_RESTInterface> execute( int AreaTrabalho_Codigo )
    {
       GxGenericCollection<SdtSDT_AreaTrabalho_Sistema_RESTInterface> Gxm2rootcol = new GxGenericCollection<SdtSDT_AreaTrabalho_Sistema_RESTInterface>() ;
       try
       {
          if ( ! ProcessHeaders("areatrabalho_sistema") )
          {
             return null ;
          }
          areatrabalho_sistema worker = new areatrabalho_sistema(context) ;
          worker.IsMain = RunAsMain ;
          IGxCollection gxrGxm2rootcol = new GxObjectCollection() ;
          worker.execute(AreaTrabalho_Codigo,out gxrGxm2rootcol );
          worker.cleanup( );
          Gxm2rootcol = new GxGenericCollection<SdtSDT_AreaTrabalho_Sistema_RESTInterface>(gxrGxm2rootcol) ;
          return Gxm2rootcol ;
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
       return null ;
    }

 }

}
