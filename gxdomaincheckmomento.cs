/*
               File: CheckMomento
        Description: CheckMomento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 21:56:34.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomaincheckmomento
   {
      private static Hashtable domain = new Hashtable();
      static gxdomaincheckmomento ()
      {
         domain[(short)1] = "Ao Iniciar uma Captura";
         domain[(short)2] = "Ao Terminar uma Captura";
         domain[(short)3] = "Ao Iniciar um Cancelamento";
         domain[(short)4] = "Ao Terminar um Cancelamento";
         domain[(short)5] = "Ao Iniciar uma Rejei��o";
         domain[(short)6] = "Ao Terminar uma Rejei��o";
         domain[(short)7] = "Ao Enviar para o Backlog";
         domain[(short)8] = "Ao Sair do Backlog";
         domain[(short)9] = "Ao Iniciar uma An�lise";
         domain[(short)10] = "Ao Terminar uma An�lise";
         domain[(short)11] = "Ao Iniciar uma Execu��o";
         domain[(short)12] = "Ao Terminar uma Execu��o";
         domain[(short)13] = "Ao Acatar uma Diverg�ncia";
         domain[(short)14] = "Ao Homologar uma demanda";
         domain[(short)15] = "Ao Liquidar uma demanda";
         domain[(short)16] = "Ao dar o Aceite de uma Demanda";
      }

      public static string getDescription( IGxContext context ,
                                           short key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (short key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
