/*
               File: ExportCapaLote
        Description: Export Capa Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:6.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportcapalote : GXProcedure
   {
      public exportcapalote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public exportcapalote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_LoteAceiteCod ,
                           out String aP1_Filename ,
                           out String aP2_ErrorMessage )
      {
         this.AV10ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         this.AV20Filename = "" ;
         this.AV18ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP1_Filename=this.AV20Filename;
         aP2_ErrorMessage=this.AV18ErrorMessage;
      }

      public String executeUdp( int aP0_ContagemResultado_LoteAceiteCod ,
                                out String aP1_Filename )
      {
         this.AV10ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         this.AV20Filename = "" ;
         this.AV18ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP1_Filename=this.AV20Filename;
         aP2_ErrorMessage=this.AV18ErrorMessage;
         return AV18ErrorMessage ;
      }

      public void executeSubmit( int aP0_ContagemResultado_LoteAceiteCod ,
                                 out String aP1_Filename ,
                                 out String aP2_ErrorMessage )
      {
         exportcapalote objexportcapalote;
         objexportcapalote = new exportcapalote();
         objexportcapalote.AV10ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         objexportcapalote.AV20Filename = "" ;
         objexportcapalote.AV18ErrorMessage = "" ;
         objexportcapalote.context.SetSubmitInitialConfig(context);
         objexportcapalote.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportcapalote);
         aP1_Filename=this.AV20Filename;
         aP2_ErrorMessage=this.AV18ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportcapalote)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV91WWPContext) ;
         /* Execute user subroutine: 'OPENDOCUMENT' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV9CellRow = 1;
         AV21FirstColumn = 1;
         /* Execute user subroutine: 'WRITEMAINTITLE' */
         S131 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEFILTERS' */
         S141 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITECOLUMNTITLES' */
         S151 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEDATA' */
         S161 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'CLOSEDOCUMENT' */
         S171 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENDOCUMENT' Routine */
         AV20Filename = "PublicTempStorage\\ExportCapaLote-" + ".xlsx";
         AV19ExcelDocument.Open(AV20Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV19ExcelDocument.Clear();
      }

      protected void S131( )
      {
         /* 'WRITEMAINTITLE' Routine */
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn, 1, 1).Bold = 1;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn, 1, 1).Color = 11;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn, 1, 1).Text = "Consulta de Lotes";
         AV9CellRow = (int)(AV9CellRow+2);
      }

      protected void S141( )
      {
         /* 'WRITEFILTERS' Routine */
         /* Using cursor P00BP3 */
         pr_default.execute(0, new Object[] {AV10ContagemResultado_LoteAceiteCod});
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000Lote_Numero = P00BP3_A40000Lote_Numero[0];
         }
         else
         {
            A40000Lote_Numero = "";
         }
         pr_default.close(0);
         /* Using cursor P00BP6 */
         pr_default.execute(1, new Object[] {AV10ContagemResultado_LoteAceiteCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            A40001GXC2 = P00BP6_A40001GXC2[0];
            A40002GXC3 = P00BP6_A40002GXC3[0];
         }
         else
         {
            A40001GXC2 = DateTime.MinValue;
            A40002GXC3 = DateTime.MinValue;
         }
         pr_default.close(1);
         AV60Lote_Numero = A40000Lote_Numero;
         AV12DataInicio = A40001GXC2;
         AV11DataFim = A40002GXC3;
         AV9CellRow = (int)(AV9CellRow+1);
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Bold = 1;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Color = 3;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Text = "Lote";
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+1, 1, 1).Italic = 1;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+1, 1, 1).Text = AV60Lote_Numero;
         AV9CellRow = (int)(AV9CellRow+1);
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Bold = 1;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Color = 3;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Text = "Per�odo";
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+1, 1, 1).Italic = 1;
         GXt_dtime1 = DateTimeUtil.ResetTime( AV12DataInicio ) ;
         AV19ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+1, 1, 1).Date = GXt_dtime1;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+2, 1, 1).Italic = 1;
         GXt_dtime1 = DateTimeUtil.ResetTime( AV11DataFim ) ;
         AV19ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+2, 1, 1).Date = GXt_dtime1;
         AV9CellRow = (int)(AV9CellRow+2);
      }

      protected void S151( )
      {
         /* 'WRITECOLUMNTITLES' Routine */
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Bold = 1;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Color = 11;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Text = "N�mero OS/TA";
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+1, 1, 1).Bold = 1;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+1, 1, 1).Color = 11;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+1, 1, 1).Text = "Coordena��o";
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+2, 1, 1).Bold = 1;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+2, 1, 1).Color = 11;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+2, 1, 1).Text = "Sistema";
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+3, 1, 1).Bold = 1;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+3, 1, 1).Color = 11;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+3, 1, 1).Text = "Quantidade";
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+4, 1, 1).Bold = 1;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+4, 1, 1).Color = 11;
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+4, 1, 1).Text = "Valor";
      }

      protected void S161( )
      {
         /* 'WRITEDATA' Routine */
         /* Using cursor P00BP7 */
         pr_default.execute(2, new Object[] {AV10ContagemResultado_LoteAceiteCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKBP2 = false;
            A489ContagemResultado_SistemaCod = P00BP7_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00BP7_n489ContagemResultado_SistemaCod[0];
            A597ContagemResultado_LoteAceiteCod = P00BP7_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00BP7_n597ContagemResultado_LoteAceiteCod[0];
            A1452ContagemResultado_SS = P00BP7_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P00BP7_n1452ContagemResultado_SS[0];
            A1051ContagemResultado_GlsValor = P00BP7_A1051ContagemResultado_GlsValor[0];
            n1051ContagemResultado_GlsValor = P00BP7_n1051ContagemResultado_GlsValor[0];
            A512ContagemResultado_ValorPF = P00BP7_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00BP7_n512ContagemResultado_ValorPF[0];
            A528ContagemResultado_LoteAceite = P00BP7_A528ContagemResultado_LoteAceite[0];
            n528ContagemResultado_LoteAceite = P00BP7_n528ContagemResultado_LoteAceite[0];
            A515ContagemResultado_SistemaCoord = P00BP7_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P00BP7_n515ContagemResultado_SistemaCoord[0];
            A509ContagemrResultado_SistemaSigla = P00BP7_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00BP7_n509ContagemrResultado_SistemaSigla[0];
            A456ContagemResultado_Codigo = P00BP7_A456ContagemResultado_Codigo[0];
            A515ContagemResultado_SistemaCoord = P00BP7_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P00BP7_n515ContagemResultado_SistemaCoord[0];
            A509ContagemrResultado_SistemaSigla = P00BP7_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00BP7_n509ContagemrResultado_SistemaSigla[0];
            A528ContagemResultado_LoteAceite = P00BP7_A528ContagemResultado_LoteAceite[0];
            n528ContagemResultado_LoteAceite = P00BP7_n528ContagemResultado_LoteAceite[0];
            GXt_decimal2 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
            A574ContagemResultado_PFFinal = GXt_decimal2;
            AV9CellRow = (int)(AV9CellRow+1);
            AV92ValorPF = A512ContagemResultado_ValorPF;
            AV87PFFinal = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00BP7_A597ContagemResultado_LoteAceiteCod[0] == A597ContagemResultado_LoteAceiteCod ) && ( P00BP7_A1452ContagemResultado_SS[0] == A1452ContagemResultado_SS ) )
            {
               BRKBP2 = false;
               A1051ContagemResultado_GlsValor = P00BP7_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = P00BP7_n1051ContagemResultado_GlsValor[0];
               A456ContagemResultado_Codigo = P00BP7_A456ContagemResultado_Codigo[0];
               GXt_decimal2 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
               A574ContagemResultado_PFFinal = GXt_decimal2;
               AV22GlsValor = (decimal)(AV22GlsValor+A1051ContagemResultado_GlsValor);
               AV87PFFinal = (decimal)(AV87PFFinal+A574ContagemResultado_PFFinal);
               BRKBP2 = true;
               pr_default.readNext(2);
            }
            AV8Valor = (decimal)(AV87PFFinal*AV92ValorPF);
            AV8Valor = NumberUtil.Round( AV8Valor, 3);
            AV88ValorTotal = (decimal)(AV88ValorTotal+AV8Valor);
            AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Text = StringUtil.Substring( A528ContagemResultado_LoteAceite, StringUtil.Len( A528ContagemResultado_LoteAceite)-3, 4)+"/"+StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)), 4, "0");
            AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+1, 1, 1).Text = A515ContagemResultado_SistemaCoord;
            AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+2, 1, 1).Text = A509ContagemrResultado_SistemaSigla;
            AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+3, 1, 1).Number = (double)(AV87PFFinal);
            AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+4, 1, 1).Number = (double)(AV8Valor);
            if ( ! BRKBP2 )
            {
               BRKBP2 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
         if ( ( AV22GlsValor > Convert.ToDecimal( 0 )) )
         {
            AV9CellRow = (int)(AV9CellRow+1);
            AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Text = "GLOSAS";
            AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+4, 1, 1).Number = (double)(AV22GlsValor*-1);
            AV88ValorTotal = (decimal)(AV88ValorTotal-AV22GlsValor);
         }
         AV9CellRow = (int)(AV9CellRow+1);
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+0, 1, 1).Text = "TOTAL";
         AV19ExcelDocument.get_Cells(AV9CellRow, AV21FirstColumn+4, 1, 1).Number = (double)(NumberUtil.Round( AV88ValorTotal, 2));
      }

      protected void S171( )
      {
         /* 'CLOSEDOCUMENT' Routine */
         AV19ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV19ExcelDocument.Close();
      }

      protected void S121( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV19ExcelDocument.ErrCode != 0 )
         {
            AV20Filename = "";
            AV18ErrorMessage = AV19ExcelDocument.ErrDescription;
            AV19ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV91WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV19ExcelDocument = new ExcelDocumentI();
         scmdbuf = "";
         P00BP3_A40000Lote_Numero = new String[] {""} ;
         A40000Lote_Numero = "";
         P00BP6_A40001GXC2 = new DateTime[] {DateTime.MinValue} ;
         P00BP6_A40002GXC3 = new DateTime[] {DateTime.MinValue} ;
         A40001GXC2 = DateTime.MinValue;
         A40002GXC3 = DateTime.MinValue;
         AV60Lote_Numero = "";
         AV12DataInicio = DateTime.MinValue;
         AV11DataFim = DateTime.MinValue;
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         P00BP7_A489ContagemResultado_SistemaCod = new int[1] ;
         P00BP7_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00BP7_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00BP7_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00BP7_A1452ContagemResultado_SS = new int[1] ;
         P00BP7_n1452ContagemResultado_SS = new bool[] {false} ;
         P00BP7_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P00BP7_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P00BP7_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00BP7_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00BP7_A528ContagemResultado_LoteAceite = new String[] {""} ;
         P00BP7_n528ContagemResultado_LoteAceite = new bool[] {false} ;
         P00BP7_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P00BP7_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P00BP7_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00BP7_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00BP7_A456ContagemResultado_Codigo = new int[1] ;
         A528ContagemResultado_LoteAceite = "";
         A515ContagemResultado_SistemaCoord = "";
         A509ContagemrResultado_SistemaSigla = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.exportcapalote__default(),
            new Object[][] {
                new Object[] {
               P00BP3_A40000Lote_Numero
               }
               , new Object[] {
               P00BP6_A40001GXC2, P00BP6_A40002GXC3
               }
               , new Object[] {
               P00BP7_A489ContagemResultado_SistemaCod, P00BP7_n489ContagemResultado_SistemaCod, P00BP7_A597ContagemResultado_LoteAceiteCod, P00BP7_n597ContagemResultado_LoteAceiteCod, P00BP7_A1452ContagemResultado_SS, P00BP7_n1452ContagemResultado_SS, P00BP7_A1051ContagemResultado_GlsValor, P00BP7_n1051ContagemResultado_GlsValor, P00BP7_A512ContagemResultado_ValorPF, P00BP7_n512ContagemResultado_ValorPF,
               P00BP7_A528ContagemResultado_LoteAceite, P00BP7_n528ContagemResultado_LoteAceite, P00BP7_A515ContagemResultado_SistemaCoord, P00BP7_n515ContagemResultado_SistemaCoord, P00BP7_A509ContagemrResultado_SistemaSigla, P00BP7_n509ContagemrResultado_SistemaSigla, P00BP7_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV10ContagemResultado_LoteAceiteCod ;
      private int AV9CellRow ;
      private int AV21FirstColumn ;
      private int A489ContagemResultado_SistemaCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A1452ContagemResultado_SS ;
      private int A456ContagemResultado_Codigo ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal AV92ValorPF ;
      private decimal AV87PFFinal ;
      private decimal GXt_decimal2 ;
      private decimal AV22GlsValor ;
      private decimal AV8Valor ;
      private decimal AV88ValorTotal ;
      private String scmdbuf ;
      private String A40000Lote_Numero ;
      private String AV60Lote_Numero ;
      private String A528ContagemResultado_LoteAceite ;
      private String A509ContagemrResultado_SistemaSigla ;
      private DateTime GXt_dtime1 ;
      private DateTime A40001GXC2 ;
      private DateTime A40002GXC3 ;
      private DateTime AV12DataInicio ;
      private DateTime AV11DataFim ;
      private bool returnInSub ;
      private bool BRKBP2 ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1452ContagemResultado_SS ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n528ContagemResultado_LoteAceite ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private String AV18ErrorMessage ;
      private String AV20Filename ;
      private String A515ContagemResultado_SistemaCoord ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00BP3_A40000Lote_Numero ;
      private DateTime[] P00BP6_A40001GXC2 ;
      private DateTime[] P00BP6_A40002GXC3 ;
      private int[] P00BP7_A489ContagemResultado_SistemaCod ;
      private bool[] P00BP7_n489ContagemResultado_SistemaCod ;
      private int[] P00BP7_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00BP7_n597ContagemResultado_LoteAceiteCod ;
      private int[] P00BP7_A1452ContagemResultado_SS ;
      private bool[] P00BP7_n1452ContagemResultado_SS ;
      private decimal[] P00BP7_A1051ContagemResultado_GlsValor ;
      private bool[] P00BP7_n1051ContagemResultado_GlsValor ;
      private decimal[] P00BP7_A512ContagemResultado_ValorPF ;
      private bool[] P00BP7_n512ContagemResultado_ValorPF ;
      private String[] P00BP7_A528ContagemResultado_LoteAceite ;
      private bool[] P00BP7_n528ContagemResultado_LoteAceite ;
      private String[] P00BP7_A515ContagemResultado_SistemaCoord ;
      private bool[] P00BP7_n515ContagemResultado_SistemaCoord ;
      private String[] P00BP7_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00BP7_n509ContagemrResultado_SistemaSigla ;
      private int[] P00BP7_A456ContagemResultado_Codigo ;
      private String aP1_Filename ;
      private String aP2_ErrorMessage ;
      private ExcelDocumentI AV19ExcelDocument ;
      private wwpbaseobjects.SdtWWPContext AV91WWPContext ;
   }

   public class exportcapalote__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BP3 ;
          prmP00BP3 = new Object[] {
          new Object[] {"@AV10ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BP6 ;
          prmP00BP6 = new Object[] {
          new Object[] {"@AV10ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BP7 ;
          prmP00BP7 = new Object[] {
          new Object[] {"@AV10ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BP3", "SELECT COALESCE( T1.[Lote_Numero], '') AS Lote_Numero FROM (SELECT [Lote_Numero] AS Lote_Numero, [Lote_Codigo] FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @AV10ContagemResultado_LoteAceiteCod ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BP3,1,0,true,false )
             ,new CursorDef("P00BP6", "SELECT COALESCE( T1.[GXC2], convert( DATETIME, '17530101', 112 )) AS GXC2, COALESCE( T1.[GXC3], convert( DATETIME, '17530101', 112 )) AS GXC3 FROM (SELECT MIN(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC2, MAX(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC3 FROM ([ContagemResultado] T2 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) WHERE T2.[ContagemResultado_LoteAceiteCod] = @AV10ContagemResultado_LoteAceiteCod ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BP6,1,0,true,false )
             ,new CursorDef("P00BP7", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, T1.[ContagemResultado_SS], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_ValorPF], T3.[Lote_Numero] AS ContagemResultado_LoteAceite, T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Lote] T3 WITH (NOLOCK) ON T3.[Lote_Codigo] = T1.[ContagemResultado_LoteAceiteCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV10ContagemResultado_LoteAceiteCod ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BP7,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 25) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
