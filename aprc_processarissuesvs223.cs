/*
               File: PRC_ProcessarIssuesVs223
        Description: Processar Issues Vs223
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:28:24.27
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_processarissuesvs223 : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV13Host = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV14Url = GetNextPar( );
                  AV15Key = GetNextPar( );
                  AV19Contratada = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV45ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV22UserId = (int)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_processarissuesvs223( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_processarissuesvs223( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Host ,
                           String aP1_Url ,
                           String aP2_Key ,
                           int aP3_Contratada ,
                           int aP4_ContratoServicos_Codigo ,
                           ref int aP5_UserId )
      {
         this.AV13Host = aP0_Host;
         this.AV14Url = aP1_Url;
         this.AV15Key = aP2_Key;
         this.AV19Contratada = aP3_Contratada;
         this.AV45ContratoServicos_Codigo = aP4_ContratoServicos_Codigo;
         this.AV22UserId = aP5_UserId;
         initialize();
         executePrivate();
         aP5_UserId=this.AV22UserId;
      }

      public int executeUdp( String aP0_Host ,
                             String aP1_Url ,
                             String aP2_Key ,
                             int aP3_Contratada ,
                             int aP4_ContratoServicos_Codigo )
      {
         this.AV13Host = aP0_Host;
         this.AV14Url = aP1_Url;
         this.AV15Key = aP2_Key;
         this.AV19Contratada = aP3_Contratada;
         this.AV45ContratoServicos_Codigo = aP4_ContratoServicos_Codigo;
         this.AV22UserId = aP5_UserId;
         initialize();
         executePrivate();
         aP5_UserId=this.AV22UserId;
         return AV22UserId ;
      }

      public void executeSubmit( String aP0_Host ,
                                 String aP1_Url ,
                                 String aP2_Key ,
                                 int aP3_Contratada ,
                                 int aP4_ContratoServicos_Codigo ,
                                 ref int aP5_UserId )
      {
         aprc_processarissuesvs223 objaprc_processarissuesvs223;
         objaprc_processarissuesvs223 = new aprc_processarissuesvs223();
         objaprc_processarissuesvs223.AV13Host = aP0_Host;
         objaprc_processarissuesvs223.AV14Url = aP1_Url;
         objaprc_processarissuesvs223.AV15Key = aP2_Key;
         objaprc_processarissuesvs223.AV19Contratada = aP3_Contratada;
         objaprc_processarissuesvs223.AV45ContratoServicos_Codigo = aP4_ContratoServicos_Codigo;
         objaprc_processarissuesvs223.AV22UserId = aP5_UserId;
         objaprc_processarissuesvs223.context.SetSubmitInitialConfig(context);
         objaprc_processarissuesvs223.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_processarissuesvs223);
         aP5_UserId=this.AV22UserId;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_processarissuesvs223)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV60ServerNow = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV8SDT_Issues.FromXml(AV11WebSession.Get("Issues"), "");
            AV69Sistemas.FromXml(AV11WebSession.Get("Sistemas"), "SDT_CodigosCollection");
            AV11WebSession.Remove("Issues");
            AV11WebSession.Remove("Sistemas");
            /* Using cursor P009G2 */
            pr_default.execute(0, new Object[] {AV19Contratada});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A29Contratante_Codigo = P009G2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P009G2_n29Contratante_Codigo[0];
               A39Contratada_Codigo = P009G2_A39Contratada_Codigo[0];
               A52Contratada_AreaTrabalhoCod = P009G2_A52Contratada_AreaTrabalhoCod[0];
               A593Contratante_OSAutomatica = P009G2_A593Contratante_OSAutomatica[0];
               A29Contratante_Codigo = P009G2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P009G2_n29Contratante_Codigo[0];
               A593Contratante_OSAutomatica = P009G2_A593Contratante_OSAutomatica[0];
               AV28AreaTrabalho = A52Contratada_AreaTrabalhoCod;
               AV29OSAutomatica = A593Contratante_OSAutomatica;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            /* Using cursor P009G3 */
            pr_default.execute(1, new Object[] {AV45ContratoServicos_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A74Contrato_Codigo = P009G3_A74Contrato_Codigo[0];
               A160ContratoServicos_Codigo = P009G3_A160ContratoServicos_Codigo[0];
               A1454ContratoServicos_PrazoTpDias = P009G3_A1454ContratoServicos_PrazoTpDias[0];
               n1454ContratoServicos_PrazoTpDias = P009G3_n1454ContratoServicos_PrazoTpDias[0];
               A557Servico_VlrUnidadeContratada = P009G3_A557Servico_VlrUnidadeContratada[0];
               A116Contrato_ValorUnidadeContratacao = P009G3_A116Contrato_ValorUnidadeContratacao[0];
               A1152ContratoServicos_PrazoAnalise = P009G3_A1152ContratoServicos_PrazoAnalise[0];
               n1152ContratoServicos_PrazoAnalise = P009G3_n1152ContratoServicos_PrazoAnalise[0];
               A913ContratoServicos_PrazoTipo = P009G3_A913ContratoServicos_PrazoTipo[0];
               n913ContratoServicos_PrazoTipo = P009G3_n913ContratoServicos_PrazoTipo[0];
               A116Contrato_ValorUnidadeContratacao = P009G3_A116Contrato_ValorUnidadeContratacao[0];
               A913ContratoServicos_PrazoTipo = P009G3_A913ContratoServicos_PrazoTipo[0];
               n913ContratoServicos_PrazoTipo = P009G3_n913ContratoServicos_PrazoTipo[0];
               AV59TipoDias = A1454ContratoServicos_PrazoTpDias;
               if ( ( A557Servico_VlrUnidadeContratada > Convert.ToDecimal( 0 )) )
               {
                  AV32ValorPF = A557Servico_VlrUnidadeContratada;
               }
               else
               {
                  AV32ValorPF = A116Contrato_ValorUnidadeContratacao;
               }
               AV33PrazoTipo = A913ContratoServicos_PrazoTipo;
               AV34DiasAnalise = A1152ContratoServicos_PrazoAnalise;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            AV74GXV1 = 1;
            while ( AV74GXV1 <= AV8SDT_Issues.gxTpr_Issues.Count )
            {
               AV17IssueItem = ((SdtSDT_RedmineIssues_issue)AV8SDT_Issues.gxTpr_Issues.Item(AV74GXV1));
               /* Execute user subroutine: 'GETISSUE' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'NEWOS' */
               S121 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               AV74GXV1 = (int)(AV74GXV1+1);
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H9G0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'GETISSUE' Routine */
         AV10httpclient.Host = StringUtil.Trim( AV13Host);
         AV10httpclient.BaseURL = StringUtil.Trim( AV14Url);
         AV9Execute = "issues/" + StringUtil.Trim( StringUtil.Str( (decimal)(AV17IssueItem.gxTpr_Id), 4, 0)) + ".xml?";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15Key)) )
         {
            AV9Execute = AV9Execute + "&key=" + StringUtil.Trim( AV15Key);
         }
         AV10httpclient.Execute("GET", AV9Execute);
         AV18SDT_Issue.FromXml(AV10httpclient.ToString(), "");
      }

      protected void S121( )
      {
         /* 'NEWOS' Routine */
         AV26PFBFS = 0;
         AV27PFLFS = 0;
         AV23Demanda = StringUtil.Trim( StringUtil.Str( (decimal)(AV17IssueItem.gxTpr_Id), 4, 0));
         AV36DataDmn = context.localUtil.CToD( AV17IssueItem.gxTpr_Start_date, 2);
         AV43Sdt_ContagemResultadoEvidencias.Clear();
         AV75GXV2 = 1;
         while ( AV75GXV2 <= AV18SDT_Issue.gxTpr_Attachments.gxTpr_Attachments.Count )
         {
            AV37Attachment = ((SdtSDT_Redmineissue_attachments_attachment)AV18SDT_Issue.gxTpr_Attachments.gxTpr_Attachments.Item(AV75GXV2));
            AV38Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link = AV37Attachment.gxTpr_Content_url;
            AV38Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_data = AV60ServerNow;
            AV43Sdt_ContagemResultadoEvidencias.Add(AV38Sdt_ArquivoEvidencia, 0);
            AV38Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
            AV75GXV2 = (int)(AV75GXV2+1);
         }
         AV76GXV3 = 1;
         while ( AV76GXV3 <= AV18SDT_Issue.gxTpr_Journals.gxTpr_Journals.Count )
         {
            AV61Journal = ((SdtSDT_Redmineissue_journals_journal)AV18SDT_Issue.gxTpr_Journals.gxTpr_Journals.Item(AV76GXV3));
            AV47i = (short)(StringUtil.StringSearch( StringUtil.Lower( AV61Journal.gxTpr_Notes), "http", 1));
            AV62l = (short)(StringUtil.StringSearch( AV61Journal.gxTpr_Notes, " ", AV47i+1)-AV47i-1);
            AV63Link = StringUtil.Substring( AV61Journal.gxTpr_Notes, AV47i, AV62l);
            AV38Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link = AV63Link;
            AV38Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_data = AV60ServerNow;
            AV43Sdt_ContagemResultadoEvidencias.Add(AV38Sdt_ArquivoEvidencia, 0);
            AV38Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
            AV76GXV3 = (int)(AV76GXV3+1);
         }
         AV77GXV4 = 1;
         while ( AV77GXV4 <= AV69Sistemas.Count )
         {
            AV66Item = ((SdtSDT_Codigos)AV69Sistemas.Item(AV77GXV4));
            if ( AV66Item.gxTpr_Codigo == AV17IssueItem.gxTpr_Id )
            {
               AV55Sistema_Codigo = (int)(NumberUtil.Val( AV66Item.gxTpr_Descricao, "."));
               if (true) break;
            }
            AV77GXV4 = (int)(AV77GXV4+1);
         }
         AV78GXLvl86 = 0;
         /* Using cursor P009G4 */
         pr_default.execute(2, new Object[] {AV55Sistema_Codigo, AV19Contratada, AV23Demanda, AV45ContratoServicos_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A489ContagemResultado_SistemaCod = P009G4_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P009G4_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P009G4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P009G4_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = P009G4_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P009G4_n457ContagemResultado_Demanda[0];
            A490ContagemResultado_ContratadaCod = P009G4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P009G4_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = P009G4_A456ContagemResultado_Codigo[0];
            AV78GXLvl86 = 1;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(2);
         }
         pr_default.close(2);
         if ( AV78GXLvl86 == 0 )
         {
            if ( AV43Sdt_ContagemResultadoEvidencias.Count > 0 )
            {
               AV11WebSession.Set("ArquivosEvd", AV43Sdt_ContagemResultadoEvidencias.ToXml(false, true, "SDT_ContagemResultadoEvidencias", "GxEv3Up14_MeetrikaVs3"));
            }
            new prc_novaos(context ).execute(  AV19Contratada,  0,  AV36DataDmn, ref  AV23Demanda, ref  AV25DemandaFM,  StringUtil.Trim( AV18SDT_Issue.gxTpr_Subject),  StringUtil.Trim( AV18SDT_Issue.gxTpr_Description),  "",  0,  AV22UserId,  AV64Servico_Codigo,  AV32ValorPF,  AV55Sistema_Codigo,  0,  0, ref  AV53Prazo_Entrega,  AV29OSAutomatica,  AV26PFBFS,  AV27PFLFS,  AV45ContratoServicos_Codigo,  AV35PrazoAnalise,  AV33PrazoTipo,  AV17IssueItem.gxTpr_Id,  AV17IssueItem.gxTpr_Project.gxTpr_Id,  context.localUtil.CToT( AV17IssueItem.gxTpr_Updated_on, 2),  0,  0,  AV56Prioridade_Codigo,  AV58ContagemResultado_CntSrvPrrPrz,  AV57ContagemResultado_CntSrvPrrCst,  0,  1,  (decimal)(0)) ;
            AV67Linha = "N� OS " + AV25DemandaFM + "| OS Ref. " + AV23Demanda + " Sistema " + StringUtil.Trim( StringUtil.Str( (decimal)(AV55Sistema_Codigo), 6, 0)) + ", processada.";
            H9G0( false, 15) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV67Linha, "")), 42, Gx_line+0, 564, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+15);
         }
      }

      protected void H9G0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV60ServerNow = (DateTime)(DateTime.MinValue);
         AV8SDT_Issues = new SdtSDT_RedmineIssues(context);
         AV11WebSession = context.GetSession();
         AV69Sistemas = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs");
         scmdbuf = "";
         P009G2_A29Contratante_Codigo = new int[1] ;
         P009G2_n29Contratante_Codigo = new bool[] {false} ;
         P009G2_A39Contratada_Codigo = new int[1] ;
         P009G2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P009G2_A593Contratante_OSAutomatica = new bool[] {false} ;
         P009G3_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P009G3_A74Contrato_Codigo = new int[1] ;
         P009G3_A160ContratoServicos_Codigo = new int[1] ;
         P009G3_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P009G3_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         P009G3_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         P009G3_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P009G3_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P009G3_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         P009G3_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         P009G3_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         A1454ContratoServicos_PrazoTpDias = "";
         A913ContratoServicos_PrazoTipo = "";
         AV59TipoDias = "";
         AV33PrazoTipo = "";
         AV17IssueItem = new SdtSDT_RedmineIssues_issue(context);
         AV10httpclient = new GxHttpClient( context);
         AV9Execute = "";
         AV18SDT_Issue = new SdtSDT_Redmineissue(context);
         AV23Demanda = "";
         AV36DataDmn = DateTime.MinValue;
         AV43Sdt_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         AV37Attachment = new SdtSDT_Redmineissue_attachments_attachment(context);
         AV38Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         AV61Journal = new SdtSDT_Redmineissue_journals_journal(context);
         AV63Link = "";
         AV66Item = new SdtSDT_Codigos(context);
         P009G4_A489ContagemResultado_SistemaCod = new int[1] ;
         P009G4_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P009G4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P009G4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P009G4_A457ContagemResultado_Demanda = new String[] {""} ;
         P009G4_n457ContagemResultado_Demanda = new bool[] {false} ;
         P009G4_A490ContagemResultado_ContratadaCod = new int[1] ;
         P009G4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P009G4_A456ContagemResultado_Codigo = new int[1] ;
         A457ContagemResultado_Demanda = "";
         AV25DemandaFM = "";
         AV53Prazo_Entrega = (DateTime)(DateTime.MinValue);
         AV35PrazoAnalise = (DateTime)(DateTime.MinValue);
         AV67Linha = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_processarissuesvs223__default(),
            new Object[][] {
                new Object[] {
               P009G2_A29Contratante_Codigo, P009G2_n29Contratante_Codigo, P009G2_A39Contratada_Codigo, P009G2_A52Contratada_AreaTrabalhoCod, P009G2_A593Contratante_OSAutomatica
               }
               , new Object[] {
               P009G3_A903ContratoServicosPrazo_CntSrvCod, P009G3_A74Contrato_Codigo, P009G3_A160ContratoServicos_Codigo, P009G3_A1454ContratoServicos_PrazoTpDias, P009G3_n1454ContratoServicos_PrazoTpDias, P009G3_A557Servico_VlrUnidadeContratada, P009G3_A116Contrato_ValorUnidadeContratacao, P009G3_A1152ContratoServicos_PrazoAnalise, P009G3_n1152ContratoServicos_PrazoAnalise, P009G3_A913ContratoServicos_PrazoTipo,
               P009G3_n913ContratoServicos_PrazoTipo
               }
               , new Object[] {
               P009G4_A489ContagemResultado_SistemaCod, P009G4_n489ContagemResultado_SistemaCod, P009G4_A1553ContagemResultado_CntSrvCod, P009G4_n1553ContagemResultado_CntSrvCod, P009G4_A457ContagemResultado_Demanda, P009G4_n457ContagemResultado_Demanda, P009G4_A490ContagemResultado_ContratadaCod, P009G4_n490ContagemResultado_ContratadaCod, P009G4_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short AV34DiasAnalise ;
      private short AV47i ;
      private short AV62l ;
      private short AV78GXLvl86 ;
      private int AV19Contratada ;
      private int AV45ContratoServicos_Codigo ;
      private int AV22UserId ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A29Contratante_Codigo ;
      private int A39Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int AV28AreaTrabalho ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int AV74GXV1 ;
      private int AV75GXV2 ;
      private int AV76GXV3 ;
      private int AV77GXV4 ;
      private int AV55Sistema_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV64Servico_Codigo ;
      private int AV56Prioridade_Codigo ;
      private int Gx_OldLine ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal AV32ValorPF ;
      private decimal AV26PFBFS ;
      private decimal AV27PFLFS ;
      private decimal AV58ContagemResultado_CntSrvPrrPrz ;
      private decimal AV57ContagemResultado_CntSrvPrrCst ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV13Host ;
      private String AV14Url ;
      private String AV15Key ;
      private String scmdbuf ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String A913ContratoServicos_PrazoTipo ;
      private String AV59TipoDias ;
      private String AV33PrazoTipo ;
      private String AV9Execute ;
      private String AV63Link ;
      private String AV67Linha ;
      private DateTime AV60ServerNow ;
      private DateTime AV53Prazo_Entrega ;
      private DateTime AV35PrazoAnalise ;
      private DateTime AV36DataDmn ;
      private bool entryPointCalled ;
      private bool n29Contratante_Codigo ;
      private bool A593Contratante_OSAutomatica ;
      private bool AV29OSAutomatica ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n913ContratoServicos_PrazoTipo ;
      private bool returnInSub ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n490ContagemResultado_ContratadaCod ;
      private String AV23Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String AV25DemandaFM ;
      private IGxSession AV11WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP5_UserId ;
      private IDataStoreProvider pr_default ;
      private int[] P009G2_A29Contratante_Codigo ;
      private bool[] P009G2_n29Contratante_Codigo ;
      private int[] P009G2_A39Contratada_Codigo ;
      private int[] P009G2_A52Contratada_AreaTrabalhoCod ;
      private bool[] P009G2_A593Contratante_OSAutomatica ;
      private int[] P009G3_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] P009G3_A74Contrato_Codigo ;
      private int[] P009G3_A160ContratoServicos_Codigo ;
      private String[] P009G3_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P009G3_n1454ContratoServicos_PrazoTpDias ;
      private decimal[] P009G3_A557Servico_VlrUnidadeContratada ;
      private decimal[] P009G3_A116Contrato_ValorUnidadeContratacao ;
      private short[] P009G3_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P009G3_n1152ContratoServicos_PrazoAnalise ;
      private String[] P009G3_A913ContratoServicos_PrazoTipo ;
      private bool[] P009G3_n913ContratoServicos_PrazoTipo ;
      private int[] P009G4_A489ContagemResultado_SistemaCod ;
      private bool[] P009G4_n489ContagemResultado_SistemaCod ;
      private int[] P009G4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P009G4_n1553ContagemResultado_CntSrvCod ;
      private String[] P009G4_A457ContagemResultado_Demanda ;
      private bool[] P009G4_n457ContagemResultado_Demanda ;
      private int[] P009G4_A490ContagemResultado_ContratadaCod ;
      private bool[] P009G4_n490ContagemResultado_ContratadaCod ;
      private int[] P009G4_A456ContagemResultado_Codigo ;
      private GxHttpClient AV10httpclient ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV69Sistemas ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV43Sdt_ContagemResultadoEvidencias ;
      private SdtSDT_Redmineissue AV18SDT_Issue ;
      private SdtSDT_Redmineissue_attachments_attachment AV37Attachment ;
      private SdtSDT_Redmineissue_journals_journal AV61Journal ;
      private SdtSDT_RedmineIssues AV8SDT_Issues ;
      private SdtSDT_RedmineIssues_issue AV17IssueItem ;
      private SdtSDT_Codigos AV66Item ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV38Sdt_ArquivoEvidencia ;
   }

   public class aprc_processarissuesvs223__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009G2 ;
          prmP009G2 = new Object[] {
          new Object[] {"@AV19Contratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009G3 ;
          prmP009G3 = new Object[] {
          new Object[] {"@AV45ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009G4 ;
          prmP009G4 = new Object[] {
          new Object[] {"@AV55Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19Contratada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV45ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009G2", "SELECT TOP 1 T2.[Contratante_Codigo], T1.[Contratada_Codigo], T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T3.[Contratante_OSAutomatica] FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T3 WITH (NOLOCK) ON T3.[Contratante_Codigo] = T2.[Contratante_Codigo]) WHERE T1.[Contratada_Codigo] = @AV19Contratada ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009G2,1,0,false,true )
             ,new CursorDef("P009G3", "SELECT TOP 1 T3.[ContratoServicosPrazo_CntSrvCod], T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[ContratoServicos_PrazoTpDias], T1.[Servico_VlrUnidadeContratada], T2.[Contrato_ValorUnidadeContratacao], T1.[ContratoServicos_PrazoAnalise], COALESCE( T3.[ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN [ContratoServicosPrazo] T3 WITH (NOLOCK) ON T3.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicos_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV45ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009G3,1,0,false,true )
             ,new CursorDef("P009G4", "SELECT TOP 1 [ContagemResultado_SistemaCod], [ContagemResultado_CntSrvCod], [ContagemResultado_Demanda], [ContagemResultado_ContratadaCod], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_SistemaCod] = @AV55Sistema_Codigo) AND ([ContagemResultado_ContratadaCod] = @AV19Contratada) AND ([ContagemResultado_Demanda] = @AV23Demanda) AND ([ContagemResultado_CntSrvCod] = @AV45ContratoServicos_Codigo) ORDER BY [ContagemResultado_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009G4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[7])[0] = rslt.getShort(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
       }
    }

 }

}
