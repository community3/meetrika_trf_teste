/*
               File: type_SdtContratoServicosArtefatos
        Description: Artefato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:55:39.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratoServicosArtefatos" )]
   [XmlType(TypeName =  "ContratoServicosArtefatos" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtContratoServicosArtefatos : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoServicosArtefatos( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratoServicosArtefatos_Artefatos_descricao = "";
         gxTv_SdtContratoServicosArtefatos_Mode = "";
         gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z = "";
      }

      public SdtContratoServicosArtefatos( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV160ContratoServicos_Codigo ,
                        int AV1749Artefatos_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV160ContratoServicos_Codigo,(int)AV1749Artefatos_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContratoServicos_Codigo", typeof(int)}, new Object[]{"Artefatos_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContratoServicosArtefatos");
         metadata.Set("BT", "ContratoServicosArtefatos");
         metadata.Set("PK", "[ \"ContratoServicos_Codigo\",\"Artefatos_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Artefatos_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"ContratoServicos_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Artefatos_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Artefatos_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosartefato_obrigatorio_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosartefato_obrigatorio_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratoServicosArtefatos deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratoServicosArtefatos)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratoServicosArtefatos obj ;
         obj = this;
         obj.gxTpr_Contratoservicos_codigo = deserialized.gxTpr_Contratoservicos_codigo;
         obj.gxTpr_Artefatos_codigo = deserialized.gxTpr_Artefatos_codigo;
         obj.gxTpr_Artefatos_descricao = deserialized.gxTpr_Artefatos_descricao;
         obj.gxTpr_Contratoservicosartefato_obrigatorio = deserialized.gxTpr_Contratoservicosartefato_obrigatorio;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratoservicos_codigo_Z = deserialized.gxTpr_Contratoservicos_codigo_Z;
         obj.gxTpr_Artefatos_codigo_Z = deserialized.gxTpr_Artefatos_codigo_Z;
         obj.gxTpr_Artefatos_descricao_Z = deserialized.gxTpr_Artefatos_descricao_Z;
         obj.gxTpr_Contratoservicosartefato_obrigatorio_Z = deserialized.gxTpr_Contratoservicosartefato_obrigatorio_Z;
         obj.gxTpr_Contratoservicosartefato_obrigatorio_N = deserialized.gxTpr_Contratoservicosartefato_obrigatorio_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Codigo") )
               {
                  gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Codigo") )
               {
                  gxTv_SdtContratoServicosArtefatos_Artefatos_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Descricao") )
               {
                  gxTv_SdtContratoServicosArtefatos_Artefatos_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosArtefato_Obrigatorio") )
               {
                  gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratoServicosArtefatos_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratoServicosArtefatos_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Codigo_Z") )
               {
                  gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Codigo_Z") )
               {
                  gxTv_SdtContratoServicosArtefatos_Artefatos_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Descricao_Z") )
               {
                  gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosArtefato_Obrigatorio_Z") )
               {
                  gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosArtefato_Obrigatorio_N") )
               {
                  gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratoServicosArtefatos";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratoServicos_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Artefatos_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosArtefatos_Artefatos_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Artefatos_Descricao", StringUtil.RTrim( gxTv_SdtContratoServicosArtefatos_Artefatos_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoServicosArtefato_Obrigatorio", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratoServicosArtefatos_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosArtefatos_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicos_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Artefatos_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosArtefatos_Artefatos_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Artefatos_Descricao_Z", StringUtil.RTrim( gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicosArtefato_Obrigatorio_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoServicosArtefato_Obrigatorio_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratoServicos_Codigo", gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo, false);
         AddObjectProperty("Artefatos_Codigo", gxTv_SdtContratoServicosArtefatos_Artefatos_codigo, false);
         AddObjectProperty("Artefatos_Descricao", gxTv_SdtContratoServicosArtefatos_Artefatos_descricao, false);
         AddObjectProperty("ContratoServicosArtefato_Obrigatorio", gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratoServicosArtefatos_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratoServicosArtefatos_Initialized, false);
            AddObjectProperty("ContratoServicos_Codigo_Z", gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo_Z, false);
            AddObjectProperty("Artefatos_Codigo_Z", gxTv_SdtContratoServicosArtefatos_Artefatos_codigo_Z, false);
            AddObjectProperty("Artefatos_Descricao_Z", gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z, false);
            AddObjectProperty("ContratoServicosArtefato_Obrigatorio_Z", gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_Z, false);
            AddObjectProperty("ContratoServicosArtefato_Obrigatorio_N", gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Codigo" )]
      [  XmlElement( ElementName = "ContratoServicos_Codigo"   )]
      public int gxTpr_Contratoservicos_codigo
      {
         get {
            return gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo ;
         }

         set {
            if ( gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo != value )
            {
               gxTv_SdtContratoServicosArtefatos_Mode = "INS";
               this.gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosArtefatos_Artefatos_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z_SetNull( );
               this.gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_Z_SetNull( );
            }
            gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Artefatos_Codigo" )]
      [  XmlElement( ElementName = "Artefatos_Codigo"   )]
      public int gxTpr_Artefatos_codigo
      {
         get {
            return gxTv_SdtContratoServicosArtefatos_Artefatos_codigo ;
         }

         set {
            if ( gxTv_SdtContratoServicosArtefatos_Artefatos_codigo != value )
            {
               gxTv_SdtContratoServicosArtefatos_Mode = "INS";
               this.gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosArtefatos_Artefatos_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z_SetNull( );
               this.gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_Z_SetNull( );
            }
            gxTv_SdtContratoServicosArtefatos_Artefatos_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Artefatos_Descricao" )]
      [  XmlElement( ElementName = "Artefatos_Descricao"   )]
      public String gxTpr_Artefatos_descricao
      {
         get {
            return gxTv_SdtContratoServicosArtefatos_Artefatos_descricao ;
         }

         set {
            gxTv_SdtContratoServicosArtefatos_Artefatos_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosArtefato_Obrigatorio" )]
      [  XmlElement( ElementName = "ContratoServicosArtefato_Obrigatorio"   )]
      public bool gxTpr_Contratoservicosartefato_obrigatorio
      {
         get {
            return gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio ;
         }

         set {
            gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_N = 0;
            gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio = value;
         }

      }

      public void gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_SetNull( )
      {
         gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_N = 1;
         gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratoServicosArtefatos_Mode ;
         }

         set {
            gxTv_SdtContratoServicosArtefatos_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosArtefatos_Mode_SetNull( )
      {
         gxTv_SdtContratoServicosArtefatos_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosArtefatos_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratoServicosArtefatos_Initialized ;
         }

         set {
            gxTv_SdtContratoServicosArtefatos_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosArtefatos_Initialized_SetNull( )
      {
         gxTv_SdtContratoServicosArtefatos_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosArtefatos_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Codigo_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_Codigo_Z"   )]
      public int gxTpr_Contratoservicos_codigo_Z
      {
         get {
            return gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo_Z ;
         }

         set {
            gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo_Z_SetNull( )
      {
         gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Artefatos_Codigo_Z" )]
      [  XmlElement( ElementName = "Artefatos_Codigo_Z"   )]
      public int gxTpr_Artefatos_codigo_Z
      {
         get {
            return gxTv_SdtContratoServicosArtefatos_Artefatos_codigo_Z ;
         }

         set {
            gxTv_SdtContratoServicosArtefatos_Artefatos_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicosArtefatos_Artefatos_codigo_Z_SetNull( )
      {
         gxTv_SdtContratoServicosArtefatos_Artefatos_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosArtefatos_Artefatos_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Artefatos_Descricao_Z" )]
      [  XmlElement( ElementName = "Artefatos_Descricao_Z"   )]
      public String gxTpr_Artefatos_descricao_Z
      {
         get {
            return gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z ;
         }

         set {
            gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z_SetNull( )
      {
         gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosArtefato_Obrigatorio_Z" )]
      [  XmlElement( ElementName = "ContratoServicosArtefato_Obrigatorio_Z"   )]
      public bool gxTpr_Contratoservicosartefato_obrigatorio_Z
      {
         get {
            return gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_Z ;
         }

         set {
            gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_Z = value;
         }

      }

      public void gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_Z_SetNull( )
      {
         gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosArtefato_Obrigatorio_N" )]
      [  XmlElement( ElementName = "ContratoServicosArtefato_Obrigatorio_N"   )]
      public short gxTpr_Contratoservicosartefato_obrigatorio_N
      {
         get {
            return gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_N ;
         }

         set {
            gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_N_SetNull( )
      {
         gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratoServicosArtefatos_Artefatos_descricao = "";
         gxTv_SdtContratoServicosArtefatos_Mode = "";
         gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratoservicosartefatos", "GeneXus.Programs.contratoservicosartefatos_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratoServicosArtefatos_Initialized ;
      private short gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo ;
      private int gxTv_SdtContratoServicosArtefatos_Artefatos_codigo ;
      private int gxTv_SdtContratoServicosArtefatos_Contratoservicos_codigo_Z ;
      private int gxTv_SdtContratoServicosArtefatos_Artefatos_codigo_Z ;
      private String gxTv_SdtContratoServicosArtefatos_Mode ;
      private String sTagName ;
      private bool gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio ;
      private bool gxTv_SdtContratoServicosArtefatos_Contratoservicosartefato_obrigatorio_Z ;
      private String gxTv_SdtContratoServicosArtefatos_Artefatos_descricao ;
      private String gxTv_SdtContratoServicosArtefatos_Artefatos_descricao_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContratoServicosArtefatos", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContratoServicosArtefatos_RESTInterface : GxGenericCollectionItem<SdtContratoServicosArtefatos>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoServicosArtefatos_RESTInterface( ) : base()
      {
      }

      public SdtContratoServicosArtefatos_RESTInterface( SdtContratoServicosArtefatos psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratoServicos_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoservicos_codigo
      {
         get {
            return sdt.gxTpr_Contratoservicos_codigo ;
         }

         set {
            sdt.gxTpr_Contratoservicos_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Artefatos_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Artefatos_codigo
      {
         get {
            return sdt.gxTpr_Artefatos_codigo ;
         }

         set {
            sdt.gxTpr_Artefatos_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Artefatos_Descricao" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Artefatos_descricao
      {
         get {
            return sdt.gxTpr_Artefatos_descricao ;
         }

         set {
            sdt.gxTpr_Artefatos_descricao = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicosArtefato_Obrigatorio" , Order = 3 )]
      [GxSeudo()]
      public bool gxTpr_Contratoservicosartefato_obrigatorio
      {
         get {
            return sdt.gxTpr_Contratoservicosartefato_obrigatorio ;
         }

         set {
            sdt.gxTpr_Contratoservicosartefato_obrigatorio = value;
         }

      }

      public SdtContratoServicosArtefatos sdt
      {
         get {
            return (SdtContratoServicosArtefatos)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratoServicosArtefatos() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 11 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
