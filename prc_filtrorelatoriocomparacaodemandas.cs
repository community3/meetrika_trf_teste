/*
               File: PRC_FiltroRelatorioComparacaoDemandas
        Description: PRC_Filtro Relatorio Comparacao Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:39.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_filtrorelatoriocomparacaodemandas : GXProcedure
   {
      public prc_filtrorelatoriocomparacaodemandas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_filtrorelatoriocomparacaodemandas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           out bool aP1_IsCarrega )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV11IsCarrega = false ;
         initialize();
         executePrivate();
         aP1_IsCarrega=this.AV11IsCarrega;
      }

      public bool executeUdp( int aP0_ContagemResultado_Codigo )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV11IsCarrega = false ;
         initialize();
         executePrivate();
         aP1_IsCarrega=this.AV11IsCarrega;
         return AV11IsCarrega ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 out bool aP1_IsCarrega )
      {
         prc_filtrorelatoriocomparacaodemandas objprc_filtrorelatoriocomparacaodemandas;
         objprc_filtrorelatoriocomparacaodemandas = new prc_filtrorelatoriocomparacaodemandas();
         objprc_filtrorelatoriocomparacaodemandas.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_filtrorelatoriocomparacaodemandas.AV11IsCarrega = false ;
         objprc_filtrorelatoriocomparacaodemandas.context.SetSubmitInitialConfig(context);
         objprc_filtrorelatoriocomparacaodemandas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_filtrorelatoriocomparacaodemandas);
         aP1_IsCarrega=this.AV11IsCarrega;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_filtrorelatoriocomparacaodemandas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11IsCarrega = false;
         AV9ContagemResultado_OSVinculada = 0;
         /* Using cursor P00YO3 */
         pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P00YO3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YO3_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = P00YO3_A456ContagemResultado_Codigo[0];
            A1326ContagemResultado_ContratadaTipoFab = P00YO3_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00YO3_n1326ContagemResultado_ContratadaTipoFab[0];
            A602ContagemResultado_OSVinculada = P00YO3_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00YO3_n602ContagemResultado_OSVinculada[0];
            A684ContagemResultado_PFBFSUltima = P00YO3_A684ContagemResultado_PFBFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00YO3_A682ContagemResultado_PFBFMUltima[0];
            A1326ContagemResultado_ContratadaTipoFab = P00YO3_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00YO3_n1326ContagemResultado_ContratadaTipoFab[0];
            A684ContagemResultado_PFBFSUltima = P00YO3_A684ContagemResultado_PFBFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00YO3_A682ContagemResultado_PFBFMUltima[0];
            AV10Esforco1 = ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M")==0) ? A682ContagemResultado_PFBFMUltima : ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S")==0) ? A684ContagemResultado_PFBFSUltima : (decimal)(0)));
            AV9ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00YO5 */
         pr_default.execute(1, new Object[] {AV9ContagemResultado_OSVinculada});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P00YO5_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YO5_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = P00YO5_A456ContagemResultado_Codigo[0];
            A1326ContagemResultado_ContratadaTipoFab = P00YO5_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00YO5_n1326ContagemResultado_ContratadaTipoFab[0];
            A684ContagemResultado_PFBFSUltima = P00YO5_A684ContagemResultado_PFBFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00YO5_A682ContagemResultado_PFBFMUltima[0];
            A1326ContagemResultado_ContratadaTipoFab = P00YO5_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00YO5_n1326ContagemResultado_ContratadaTipoFab[0];
            A684ContagemResultado_PFBFSUltima = P00YO5_A684ContagemResultado_PFBFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00YO5_A682ContagemResultado_PFBFMUltima[0];
            AV12Esforco2 = ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M")==0) ? A682ContagemResultado_PFBFMUltima : ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S")==0) ? A684ContagemResultado_PFBFSUltima : (decimal)(0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( ! (Convert.ToDecimal(0)==AV10Esforco1) && ! (Convert.ToDecimal(0)==AV12Esforco2) )
         {
            AV11IsCarrega = true;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00YO3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YO3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YO3_A456ContagemResultado_Codigo = new int[1] ;
         P00YO3_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00YO3_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00YO3_A602ContagemResultado_OSVinculada = new int[1] ;
         P00YO3_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00YO3_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00YO3_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         A1326ContagemResultado_ContratadaTipoFab = "";
         P00YO5_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YO5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YO5_A456ContagemResultado_Codigo = new int[1] ;
         P00YO5_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00YO5_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00YO5_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00YO5_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_filtrorelatoriocomparacaodemandas__default(),
            new Object[][] {
                new Object[] {
               P00YO3_A490ContagemResultado_ContratadaCod, P00YO3_n490ContagemResultado_ContratadaCod, P00YO3_A456ContagemResultado_Codigo, P00YO3_A1326ContagemResultado_ContratadaTipoFab, P00YO3_n1326ContagemResultado_ContratadaTipoFab, P00YO3_A602ContagemResultado_OSVinculada, P00YO3_n602ContagemResultado_OSVinculada, P00YO3_A684ContagemResultado_PFBFSUltima, P00YO3_A682ContagemResultado_PFBFMUltima
               }
               , new Object[] {
               P00YO5_A490ContagemResultado_ContratadaCod, P00YO5_n490ContagemResultado_ContratadaCod, P00YO5_A456ContagemResultado_Codigo, P00YO5_A1326ContagemResultado_ContratadaTipoFab, P00YO5_n1326ContagemResultado_ContratadaTipoFab, P00YO5_A684ContagemResultado_PFBFSUltima, P00YO5_A682ContagemResultado_PFBFMUltima
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8ContagemResultado_Codigo ;
      private int AV9ContagemResultado_OSVinculada ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal AV10Esforco1 ;
      private decimal AV12Esforco2 ;
      private String scmdbuf ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private bool AV11IsCarrega ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n602ContagemResultado_OSVinculada ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00YO3_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YO3_n490ContagemResultado_ContratadaCod ;
      private int[] P00YO3_A456ContagemResultado_Codigo ;
      private String[] P00YO3_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00YO3_n1326ContagemResultado_ContratadaTipoFab ;
      private int[] P00YO3_A602ContagemResultado_OSVinculada ;
      private bool[] P00YO3_n602ContagemResultado_OSVinculada ;
      private decimal[] P00YO3_A684ContagemResultado_PFBFSUltima ;
      private decimal[] P00YO3_A682ContagemResultado_PFBFMUltima ;
      private int[] P00YO5_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YO5_n490ContagemResultado_ContratadaCod ;
      private int[] P00YO5_A456ContagemResultado_Codigo ;
      private String[] P00YO5_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00YO5_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] P00YO5_A684ContagemResultado_PFBFSUltima ;
      private decimal[] P00YO5_A682ContagemResultado_PFBFMUltima ;
      private bool aP1_IsCarrega ;
   }

   public class prc_filtrorelatoriocomparacaodemandas__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00YO3 ;
          prmP00YO3 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YO5 ;
          prmP00YO5 = new Object[] {
          new Object[] {"@AV9ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00YO3", "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_OSVinculada], COALESCE( T3.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T3.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YO3,1,0,false,true )
             ,new CursorDef("P00YO5", "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, COALESCE( T3.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T3.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV9ContagemResultado_OSVinculada ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YO5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
