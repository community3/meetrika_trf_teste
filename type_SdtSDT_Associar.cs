/*
               File: type_SdtSDT_Associar
        Description: SDT_Associar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:59.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Associar" )]
   [XmlType(TypeName =  "SDT_Associar" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_Associar : GxUserType
   {
      public SdtSDT_Associar( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Associar_Descricao = "";
      }

      public SdtSDT_Associar( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Associar deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Associar)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Associar obj ;
         obj = this;
         obj.gxTpr_Key = deserialized.gxTpr_Key;
         obj.gxTpr_Descricao = deserialized.gxTpr_Descricao;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Key") )
               {
                  gxTv_SdtSDT_Associar_Key = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Descricao") )
               {
                  gxTv_SdtSDT_Associar_Descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Associar";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Key", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Associar_Key), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Descricao", StringUtil.RTrim( gxTv_SdtSDT_Associar_Descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Key", gxTv_SdtSDT_Associar_Key, false);
         AddObjectProperty("Descricao", gxTv_SdtSDT_Associar_Descricao, false);
         return  ;
      }

      [  SoapElement( ElementName = "Key" )]
      [  XmlElement( ElementName = "Key"   )]
      public int gxTpr_Key
      {
         get {
            return gxTv_SdtSDT_Associar_Key ;
         }

         set {
            gxTv_SdtSDT_Associar_Key = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Descricao" )]
      [  XmlElement( ElementName = "Descricao"   )]
      public String gxTpr_Descricao
      {
         get {
            return gxTv_SdtSDT_Associar_Descricao ;
         }

         set {
            gxTv_SdtSDT_Associar_Descricao = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Associar_Descricao = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_Associar_Key ;
      protected String sTagName ;
      protected String gxTv_SdtSDT_Associar_Descricao ;
   }

   [DataContract(Name = @"SDT_Associar", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_Associar_RESTInterface : GxGenericCollectionItem<SdtSDT_Associar>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Associar_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Associar_RESTInterface( SdtSDT_Associar psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Key" , Order = 0 )]
      public String gxTpr_Key
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Key), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Key = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Descricao" , Order = 1 )]
      public String gxTpr_Descricao
      {
         get {
            return sdt.gxTpr_Descricao ;
         }

         set {
            sdt.gxTpr_Descricao = (String)(value);
         }

      }

      public SdtSDT_Associar sdt
      {
         get {
            return (SdtSDT_Associar)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Associar() ;
         }
      }

   }

}
