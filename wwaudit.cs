/*
               File: WWAudit
        Description:  Audit
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:42:3.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwaudit : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwaudit( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwaudit( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavAudittablename = new GXCombobox();
         cmbavAuditaction = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_48 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_48_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_48_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV17AuditTableName = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AuditTableName", AV17AuditTableName);
               AV9AuditDate = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AuditDate", context.localUtil.TToC( AV9AuditDate, 8, 5, 0, 3, "/", ":", " "));
               AV10AuditDate_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AuditDate_To", context.localUtil.TToC( AV10AuditDate_To, 8, 5, 0, 3, "/", ":", " "));
               AV5AuditAction = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AuditAction", AV5AuditAction);
               AV43Usuario_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Usuario_PessoaNom", AV43Usuario_PessoaNom);
               AV37OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
               AV38OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38OrderedDsc", AV38OrderedDsc);
               AV49TFAuditDate = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFAuditDate", context.localUtil.TToC( AV49TFAuditDate, 8, 5, 0, 3, "/", ":", " "));
               AV50TFAuditDate_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFAuditDate_To", context.localUtil.TToC( AV50TFAuditDate_To, 8, 5, 0, 3, "/", ":", " "));
               AV55TFAuditDescription = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFAuditDescription", AV55TFAuditDescription);
               AV56TFAuditDescription_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFAuditDescription_Sel", AV56TFAuditDescription_Sel);
               AV59TFUsuario_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFUsuario_PessoaNom", AV59TFUsuario_PessoaNom);
               AV60TFUsuario_PessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUsuario_PessoaNom_Sel", AV60TFUsuario_PessoaNom_Sel);
               AV63TFAuditAction = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFAuditAction", AV63TFAuditAction);
               AV64TFAuditAction_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFAuditAction_Sel", AV64TFAuditAction_Sel);
               AV53ddo_AuditDateTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_AuditDateTitleControlIdToReplace", AV53ddo_AuditDateTitleControlIdToReplace);
               AV57ddo_AuditDescriptionTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_AuditDescriptionTitleControlIdToReplace", AV57ddo_AuditDescriptionTitleControlIdToReplace);
               AV61ddo_Usuario_PessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Usuario_PessoaNomTitleControlIdToReplace", AV61ddo_Usuario_PessoaNomTitleControlIdToReplace);
               AV65ddo_AuditActionTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_AuditActionTitleControlIdToReplace", AV65ddo_AuditActionTitleControlIdToReplace);
               AV72Pgmname = GetNextPar( );
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1Usuario_Codigo = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV17AuditTableName, AV9AuditDate, AV10AuditDate_To, AV5AuditAction, AV43Usuario_PessoaNom, AV37OrderedBy, AV38OrderedDsc, AV49TFAuditDate, AV50TFAuditDate_To, AV55TFAuditDescription, AV56TFAuditDescription_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFAuditAction, AV64TFAuditAction_Sel, AV53ddo_AuditDateTitleControlIdToReplace, AV57ddo_AuditDescriptionTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_AuditActionTitleControlIdToReplace, AV72Pgmname, A1Usuario_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAKO2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTKO2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020529942354");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwaudit.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vAUDITTABLENAME", StringUtil.RTrim( AV17AuditTableName));
         GxWebStd.gx_hidden_field( context, "GXH_vAUDITDATE", context.localUtil.TToC( AV9AuditDate, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vAUDITDATE_TO", context.localUtil.TToC( AV10AuditDate_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vAUDITACTION", StringUtil.RTrim( AV5AuditAction));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM", StringUtil.RTrim( AV43Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV38OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUDITDATE", context.localUtil.TToC( AV49TFAuditDate, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUDITDATE_TO", context.localUtil.TToC( AV50TFAuditDate_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUDITDESCRIPTION", AV55TFAuditDescription);
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUDITDESCRIPTION_SEL", AV56TFAuditDescription_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_PESSOANOM", StringUtil.RTrim( AV59TFUsuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_PESSOANOM_SEL", StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUDITACTION", StringUtil.RTrim( AV63TFAuditAction));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUDITACTION_SEL", StringUtil.RTrim( AV64TFAuditAction_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_48", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_48), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV66DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV66DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITDATETITLEFILTERDATA", AV48AuditDateTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITDATETITLEFILTERDATA", AV48AuditDateTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITDESCRIPTIONTITLEFILTERDATA", AV54AuditDescriptionTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITDESCRIPTIONTITLEFILTERDATA", AV54AuditDescriptionTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIO_PESSOANOMTITLEFILTERDATA", AV58Usuario_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIO_PESSOANOMTITLEFILTERDATA", AV58Usuario_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITACTIONTITLEFILTERDATA", AV62AuditActionTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITACTIONTITLEFILTERDATA", AV62AuditActionTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV72Pgmname));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Caption", StringUtil.RTrim( Ddo_auditdate_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Tooltip", StringUtil.RTrim( Ddo_auditdate_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Cls", StringUtil.RTrim( Ddo_auditdate_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Filteredtext_set", StringUtil.RTrim( Ddo_auditdate_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Filteredtextto_set", StringUtil.RTrim( Ddo_auditdate_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Dropdownoptionstype", StringUtil.RTrim( Ddo_auditdate_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_auditdate_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Includesortasc", StringUtil.BoolToStr( Ddo_auditdate_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Includesortdsc", StringUtil.BoolToStr( Ddo_auditdate_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Sortedstatus", StringUtil.RTrim( Ddo_auditdate_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Includefilter", StringUtil.BoolToStr( Ddo_auditdate_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Filtertype", StringUtil.RTrim( Ddo_auditdate_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Filterisrange", StringUtil.BoolToStr( Ddo_auditdate_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Includedatalist", StringUtil.BoolToStr( Ddo_auditdate_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Sortasc", StringUtil.RTrim( Ddo_auditdate_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Sortdsc", StringUtil.RTrim( Ddo_auditdate_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Cleanfilter", StringUtil.RTrim( Ddo_auditdate_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Rangefilterfrom", StringUtil.RTrim( Ddo_auditdate_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Rangefilterto", StringUtil.RTrim( Ddo_auditdate_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Searchbuttontext", StringUtil.RTrim( Ddo_auditdate_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Caption", StringUtil.RTrim( Ddo_auditdescription_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Tooltip", StringUtil.RTrim( Ddo_auditdescription_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Cls", StringUtil.RTrim( Ddo_auditdescription_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Filteredtext_set", StringUtil.RTrim( Ddo_auditdescription_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Selectedvalue_set", StringUtil.RTrim( Ddo_auditdescription_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Dropdownoptionstype", StringUtil.RTrim( Ddo_auditdescription_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_auditdescription_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Includesortasc", StringUtil.BoolToStr( Ddo_auditdescription_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Includesortdsc", StringUtil.BoolToStr( Ddo_auditdescription_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Sortedstatus", StringUtil.RTrim( Ddo_auditdescription_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Includefilter", StringUtil.BoolToStr( Ddo_auditdescription_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Filtertype", StringUtil.RTrim( Ddo_auditdescription_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Filterisrange", StringUtil.BoolToStr( Ddo_auditdescription_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Includedatalist", StringUtil.BoolToStr( Ddo_auditdescription_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Datalisttype", StringUtil.RTrim( Ddo_auditdescription_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Datalistproc", StringUtil.RTrim( Ddo_auditdescription_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_auditdescription_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Sortasc", StringUtil.RTrim( Ddo_auditdescription_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Sortdsc", StringUtil.RTrim( Ddo_auditdescription_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Loadingdata", StringUtil.RTrim( Ddo_auditdescription_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Cleanfilter", StringUtil.RTrim( Ddo_auditdescription_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Noresultsfound", StringUtil.RTrim( Ddo_auditdescription_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Searchbuttontext", StringUtil.RTrim( Ddo_auditdescription_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Caption", StringUtil.RTrim( Ddo_usuario_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_usuario_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Cls", StringUtil.RTrim( Ddo_usuario_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_usuario_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_usuario_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuario_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuario_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_usuario_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_usuario_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_usuario_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuario_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_usuario_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_usuario_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_usuario_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_usuario_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Caption", StringUtil.RTrim( Ddo_auditaction_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Tooltip", StringUtil.RTrim( Ddo_auditaction_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Cls", StringUtil.RTrim( Ddo_auditaction_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Filteredtext_set", StringUtil.RTrim( Ddo_auditaction_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Selectedvalue_set", StringUtil.RTrim( Ddo_auditaction_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Dropdownoptionstype", StringUtil.RTrim( Ddo_auditaction_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_auditaction_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Includesortasc", StringUtil.BoolToStr( Ddo_auditaction_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Includesortdsc", StringUtil.BoolToStr( Ddo_auditaction_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Sortedstatus", StringUtil.RTrim( Ddo_auditaction_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Includefilter", StringUtil.BoolToStr( Ddo_auditaction_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Filtertype", StringUtil.RTrim( Ddo_auditaction_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Filterisrange", StringUtil.BoolToStr( Ddo_auditaction_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Includedatalist", StringUtil.BoolToStr( Ddo_auditaction_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Datalisttype", StringUtil.RTrim( Ddo_auditaction_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Datalistproc", StringUtil.RTrim( Ddo_auditaction_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_auditaction_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Sortasc", StringUtil.RTrim( Ddo_auditaction_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Sortdsc", StringUtil.RTrim( Ddo_auditaction_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Loadingdata", StringUtil.RTrim( Ddo_auditaction_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Cleanfilter", StringUtil.RTrim( Ddo_auditaction_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Noresultsfound", StringUtil.RTrim( Ddo_auditaction_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Searchbuttontext", StringUtil.RTrim( Ddo_auditaction_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Activeeventkey", StringUtil.RTrim( Ddo_auditdate_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Filteredtext_get", StringUtil.RTrim( Ddo_auditdate_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDATE_Filteredtextto_get", StringUtil.RTrim( Ddo_auditdate_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Activeeventkey", StringUtil.RTrim( Ddo_auditdescription_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Filteredtext_get", StringUtil.RTrim( Ddo_auditdescription_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITDESCRIPTION_Selectedvalue_get", StringUtil.RTrim( Ddo_auditdescription_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_usuario_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_usuario_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_usuario_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Activeeventkey", StringUtil.RTrim( Ddo_auditaction_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Filteredtext_get", StringUtil.RTrim( Ddo_auditaction_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUDITACTION_Selectedvalue_get", StringUtil.RTrim( Ddo_auditaction_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEKO2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTKO2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwaudit.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWAudit" ;
      }

      public override String GetPgmdesc( )
      {
         return " Audit" ;
      }

      protected void WBKO0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_KO2( true) ;
         }
         else
         {
            wb_table1_2_KO2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KO2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAudit.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV38OrderedDsc), StringUtil.BoolToStr( AV38OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWAudit.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_48_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfauditdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfauditdate_Internalname, context.localUtil.TToC( AV49TFAuditDate, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV49TFAuditDate, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfauditdate_Jsonclick, 0, "Attribute", "", "", "", edtavTfauditdate_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAudit.htm");
            GxWebStd.gx_bitmap( context, edtavTfauditdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfauditdate_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAudit.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_48_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfauditdate_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfauditdate_to_Internalname, context.localUtil.TToC( AV50TFAuditDate_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV50TFAuditDate_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfauditdate_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfauditdate_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAudit.htm");
            GxWebStd.gx_bitmap( context, edtavTfauditdate_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfauditdate_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAudit.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_auditdateauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_48_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_auditdateauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_auditdateauxdate_Internalname, context.localUtil.Format(AV51DDO_AuditDateAuxDate, "99/99/99"), context.localUtil.Format( AV51DDO_AuditDateAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_auditdateauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAudit.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_auditdateauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAudit.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_48_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_auditdateauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_auditdateauxdateto_Internalname, context.localUtil.Format(AV52DDO_AuditDateAuxDateTo, "99/99/99"), context.localUtil.Format( AV52DDO_AuditDateAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_auditdateauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAudit.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_auditdateauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAudit.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_48_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfauditdescription_Internalname, AV55TFAuditDescription, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", 0, edtavTfauditdescription_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWAudit.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_48_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfauditdescription_sel_Internalname, AV56TFAuditDescription_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", 0, edtavTfauditdescription_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWAudit.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_pessoanom_Internalname, StringUtil.RTrim( AV59TFUsuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV59TFUsuario_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfusuario_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAudit.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_pessoanom_sel_Internalname, StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV60TFUsuario_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfusuario_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAudit.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfauditaction_Internalname, StringUtil.RTrim( AV63TFAuditAction), StringUtil.RTrim( context.localUtil.Format( AV63TFAuditAction, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfauditaction_Jsonclick, 0, "Attribute", "", "", "", edtavTfauditaction_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAudit.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfauditaction_sel_Internalname, StringUtil.RTrim( AV64TFAuditAction_Sel), StringUtil.RTrim( context.localUtil.Format( AV64TFAuditAction_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfauditaction_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfauditaction_sel_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAudit.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AUDITDATEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_48_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_auditdatetitlecontrolidtoreplace_Internalname, AV53ddo_AuditDateTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", 0, edtavDdo_auditdatetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAudit.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AUDITDESCRIPTIONContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_48_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_auditdescriptiontitlecontrolidtoreplace_Internalname, AV57ddo_AuditDescriptionTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", 0, edtavDdo_auditdescriptiontitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAudit.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIO_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_48_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", 0, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAudit.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AUDITACTIONContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_48_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_auditactiontitlecontrolidtoreplace_Internalname, AV65ddo_AuditActionTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", 0, edtavDdo_auditactiontitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAudit.htm");
         }
         wbLoad = true;
      }

      protected void STARTKO2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Audit", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKO0( ) ;
      }

      protected void WSKO2( )
      {
         STARTKO2( ) ;
         EVTKO2( ) ;
      }

      protected void EVTKO2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11KO2 */
                              E11KO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUDITDATE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12KO2 */
                              E12KO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUDITDESCRIPTION.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13KO2 */
                              E13KO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIO_PESSOANOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14KO2 */
                              E14KO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUDITACTION.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15KO2 */
                              E15KO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_48_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
                              SubsflControlProps_482( ) ;
                              A1430AuditId = (long)(context.localUtil.CToN( cgiGet( edtAuditId_Internalname), ",", "."));
                              A1431AuditDate = context.localUtil.CToT( cgiGet( edtAuditDate_Internalname), 0);
                              n1431AuditDate = false;
                              A1432AuditTableName = StringUtil.Upper( cgiGet( edtAuditTableName_Internalname));
                              n1432AuditTableName = false;
                              A1434AuditDescription = cgiGet( edtAuditDescription_Internalname);
                              n1434AuditDescription = false;
                              A1433AuditShortDescription = cgiGet( edtAuditShortDescription_Internalname);
                              n1433AuditShortDescription = false;
                              A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", "."));
                              n1Usuario_Codigo = false;
                              A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
                              n58Usuario_PessoaNom = false;
                              A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
                              n2Usuario_Nome = false;
                              A1435AuditAction = cgiGet( edtAuditAction_Internalname);
                              n1435AuditAction = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E16KO2 */
                                    E16KO2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E17KO2 */
                                    E17KO2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E18KO2 */
                                    E18KO2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Audittablename Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vAUDITTABLENAME"), AV17AuditTableName) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Auditdate Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAUDITDATE"), 0) != AV9AuditDate )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Auditdate_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAUDITDATE_TO"), 0) != AV10AuditDate_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Auditaction Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vAUDITACTION"), AV5AuditAction) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_pessoanom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM"), AV43Usuario_PessoaNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV37OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV38OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfauditdate Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUDITDATE"), 0) != AV49TFAuditDate )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfauditdate_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUDITDATE_TO"), 0) != AV50TFAuditDate_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfauditdescription Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUDITDESCRIPTION"), AV55TFAuditDescription) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfauditdescription_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUDITDESCRIPTION_SEL"), AV56TFAuditDescription_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_pessoanom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM"), AV59TFUsuario_PessoaNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_pessoanom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM_SEL"), AV60TFUsuario_PessoaNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfauditaction Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUDITACTION"), AV63TFAuditAction) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfauditaction_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUDITACTION_SEL"), AV64TFAuditAction_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKO2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAKO2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavAudittablename.Name = "vAUDITTABLENAME";
            cmbavAudittablename.WebTags = "";
            cmbavAudittablename.addItem("", "(Nenhum)", 0);
            cmbavAudittablename.addItem("Usuario", "Usu�rio", 0);
            cmbavAudittablename.addItem("ContagemResultado", "Contagem Resultado", 0);
            cmbavAudittablename.addItem("Contratata", "Contratatas", 0);
            cmbavAudittablename.addItem("Contratante", "Contratantes", 0);
            cmbavAudittablename.addItem("AreaTrabalho", "�reas de Trabalho", 0);
            cmbavAudittablename.addItem("ContratoServicos", "Contrato Servi�os", 0);
            cmbavAudittablename.addItem("Servico", "Servi�o", 0);
            cmbavAudittablename.addItem("Sistema", "Sistemas", 0);
            cmbavAudittablename.addItem("Lote", "Lote", 0);
            if ( cmbavAudittablename.ItemCount > 0 )
            {
               AV17AuditTableName = cmbavAudittablename.getValidValue(AV17AuditTableName);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AuditTableName", AV17AuditTableName);
            }
            cmbavAuditaction.Name = "vAUDITACTION";
            cmbavAuditaction.WebTags = "";
            cmbavAuditaction.addItem("", "(Nenhum)", 0);
            cmbavAuditaction.addItem("INS", "Insert", 0);
            cmbavAuditaction.addItem("UPD", "Update", 0);
            cmbavAuditaction.addItem("DLT", "Delete", 0);
            if ( cmbavAuditaction.ItemCount > 0 )
            {
               AV5AuditAction = cmbavAuditaction.getValidValue(AV5AuditAction);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AuditAction", AV5AuditAction);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavAudittablename_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_482( ) ;
         while ( nGXsfl_48_idx <= nRC_GXsfl_48 )
         {
            sendrow_482( ) ;
            nGXsfl_48_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_48_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_48_idx+1));
            sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
            SubsflControlProps_482( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV17AuditTableName ,
                                       DateTime AV9AuditDate ,
                                       DateTime AV10AuditDate_To ,
                                       String AV5AuditAction ,
                                       String AV43Usuario_PessoaNom ,
                                       short AV37OrderedBy ,
                                       bool AV38OrderedDsc ,
                                       DateTime AV49TFAuditDate ,
                                       DateTime AV50TFAuditDate_To ,
                                       String AV55TFAuditDescription ,
                                       String AV56TFAuditDescription_Sel ,
                                       String AV59TFUsuario_PessoaNom ,
                                       String AV60TFUsuario_PessoaNom_Sel ,
                                       String AV63TFAuditAction ,
                                       String AV64TFAuditAction_Sel ,
                                       String AV53ddo_AuditDateTitleControlIdToReplace ,
                                       String AV57ddo_AuditDescriptionTitleControlIdToReplace ,
                                       String AV61ddo_Usuario_PessoaNomTitleControlIdToReplace ,
                                       String AV65ddo_AuditActionTitleControlIdToReplace ,
                                       String AV72Pgmname ,
                                       int A1Usuario_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFKO2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_AUDITID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1430AuditId), "ZZZZZZZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AUDITID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1430AuditId), 18, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AUDITDATE", GetSecureSignedToken( "", context.localUtil.Format( A1431AuditDate, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "AUDITDATE", context.localUtil.TToC( A1431AuditDate, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_AUDITTABLENAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1432AuditTableName, "@!"))));
         GxWebStd.gx_hidden_field( context, "AUDITTABLENAME", StringUtil.RTrim( A1432AuditTableName));
         GxWebStd.gx_hidden_field( context, "gxhash_AUDITDESCRIPTION", GetSecureSignedToken( "", A1434AuditDescription));
         GxWebStd.gx_hidden_field( context, "AUDITDESCRIPTION", A1434AuditDescription);
         GxWebStd.gx_hidden_field( context, "gxhash_AUDITSHORTDESCRIPTION", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1433AuditShortDescription, ""))));
         GxWebStd.gx_hidden_field( context, "AUDITSHORTDESCRIPTION", A1433AuditShortDescription);
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AUDITACTION", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1435AuditAction, ""))));
         GxWebStd.gx_hidden_field( context, "AUDITACTION", StringUtil.RTrim( A1435AuditAction));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavAudittablename.ItemCount > 0 )
         {
            AV17AuditTableName = cmbavAudittablename.getValidValue(AV17AuditTableName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AuditTableName", AV17AuditTableName);
         }
         if ( cmbavAuditaction.ItemCount > 0 )
         {
            AV5AuditAction = cmbavAuditaction.getValidValue(AV5AuditAction);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AuditAction", AV5AuditAction);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKO2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV72Pgmname = "WWAudit";
         context.Gx_err = 0;
      }

      protected void RFKO2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 48;
         /* Execute user event: E17KO2 */
         E17KO2 ();
         nGXsfl_48_idx = 1;
         sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
         SubsflControlProps_482( ) ;
         nGXsfl_48_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_482( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV9AuditDate ,
                                                 AV10AuditDate_To ,
                                                 AV5AuditAction ,
                                                 AV43Usuario_PessoaNom ,
                                                 AV49TFAuditDate ,
                                                 AV50TFAuditDate_To ,
                                                 AV56TFAuditDescription_Sel ,
                                                 AV55TFAuditDescription ,
                                                 AV60TFUsuario_PessoaNom_Sel ,
                                                 AV59TFUsuario_PessoaNom ,
                                                 AV64TFAuditAction_Sel ,
                                                 AV63TFAuditAction ,
                                                 A1431AuditDate ,
                                                 A1435AuditAction ,
                                                 A58Usuario_PessoaNom ,
                                                 A1434AuditDescription ,
                                                 AV37OrderedBy ,
                                                 AV38OrderedDsc ,
                                                 AV17AuditTableName ,
                                                 A1432AuditTableName },
                                                 new int[] {
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                 }
            });
            lV43Usuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV43Usuario_PessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Usuario_PessoaNom", AV43Usuario_PessoaNom);
            lV55TFAuditDescription = StringUtil.Concat( StringUtil.RTrim( AV55TFAuditDescription), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFAuditDescription", AV55TFAuditDescription);
            lV59TFUsuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV59TFUsuario_PessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFUsuario_PessoaNom", AV59TFUsuario_PessoaNom);
            lV63TFAuditAction = StringUtil.PadR( StringUtil.RTrim( AV63TFAuditAction), 3, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFAuditAction", AV63TFAuditAction);
            /* Using cursor H00KO2 */
            pr_default.execute(0, new Object[] {AV17AuditTableName, AV9AuditDate, AV10AuditDate_To, AV5AuditAction, lV43Usuario_PessoaNom, AV49TFAuditDate, AV50TFAuditDate_To, lV55TFAuditDescription, AV56TFAuditDescription_Sel, lV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, lV63TFAuditAction, AV64TFAuditAction_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_48_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A57Usuario_PessoaCod = H00KO2_A57Usuario_PessoaCod[0];
               A1432AuditTableName = H00KO2_A1432AuditTableName[0];
               n1432AuditTableName = H00KO2_n1432AuditTableName[0];
               A1435AuditAction = H00KO2_A1435AuditAction[0];
               n1435AuditAction = H00KO2_n1435AuditAction[0];
               A2Usuario_Nome = H00KO2_A2Usuario_Nome[0];
               n2Usuario_Nome = H00KO2_n2Usuario_Nome[0];
               A58Usuario_PessoaNom = H00KO2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00KO2_n58Usuario_PessoaNom[0];
               A1Usuario_Codigo = H00KO2_A1Usuario_Codigo[0];
               n1Usuario_Codigo = H00KO2_n1Usuario_Codigo[0];
               A1433AuditShortDescription = H00KO2_A1433AuditShortDescription[0];
               n1433AuditShortDescription = H00KO2_n1433AuditShortDescription[0];
               A1434AuditDescription = H00KO2_A1434AuditDescription[0];
               n1434AuditDescription = H00KO2_n1434AuditDescription[0];
               A1431AuditDate = H00KO2_A1431AuditDate[0];
               n1431AuditDate = H00KO2_n1431AuditDate[0];
               A1430AuditId = H00KO2_A1430AuditId[0];
               A57Usuario_PessoaCod = H00KO2_A57Usuario_PessoaCod[0];
               A2Usuario_Nome = H00KO2_A2Usuario_Nome[0];
               n2Usuario_Nome = H00KO2_n2Usuario_Nome[0];
               A58Usuario_PessoaNom = H00KO2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00KO2_n58Usuario_PessoaNom[0];
               /* Execute user event: E18KO2 */
               E18KO2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 48;
            WBKO0( ) ;
         }
         nGXsfl_48_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV9AuditDate ,
                                              AV10AuditDate_To ,
                                              AV5AuditAction ,
                                              AV43Usuario_PessoaNom ,
                                              AV49TFAuditDate ,
                                              AV50TFAuditDate_To ,
                                              AV56TFAuditDescription_Sel ,
                                              AV55TFAuditDescription ,
                                              AV60TFUsuario_PessoaNom_Sel ,
                                              AV59TFUsuario_PessoaNom ,
                                              AV64TFAuditAction_Sel ,
                                              AV63TFAuditAction ,
                                              A1431AuditDate ,
                                              A1435AuditAction ,
                                              A58Usuario_PessoaNom ,
                                              A1434AuditDescription ,
                                              AV37OrderedBy ,
                                              AV38OrderedDsc ,
                                              AV17AuditTableName ,
                                              A1432AuditTableName },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV43Usuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV43Usuario_PessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Usuario_PessoaNom", AV43Usuario_PessoaNom);
         lV55TFAuditDescription = StringUtil.Concat( StringUtil.RTrim( AV55TFAuditDescription), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFAuditDescription", AV55TFAuditDescription);
         lV59TFUsuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV59TFUsuario_PessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFUsuario_PessoaNom", AV59TFUsuario_PessoaNom);
         lV63TFAuditAction = StringUtil.PadR( StringUtil.RTrim( AV63TFAuditAction), 3, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFAuditAction", AV63TFAuditAction);
         /* Using cursor H00KO3 */
         pr_default.execute(1, new Object[] {AV17AuditTableName, AV9AuditDate, AV10AuditDate_To, AV5AuditAction, lV43Usuario_PessoaNom, AV49TFAuditDate, AV50TFAuditDate_To, lV55TFAuditDescription, AV56TFAuditDescription_Sel, lV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, lV63TFAuditAction, AV64TFAuditAction_Sel});
         GRID_nRecordCount = H00KO3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV17AuditTableName, AV9AuditDate, AV10AuditDate_To, AV5AuditAction, AV43Usuario_PessoaNom, AV37OrderedBy, AV38OrderedDsc, AV49TFAuditDate, AV50TFAuditDate_To, AV55TFAuditDescription, AV56TFAuditDescription_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFAuditAction, AV64TFAuditAction_Sel, AV53ddo_AuditDateTitleControlIdToReplace, AV57ddo_AuditDescriptionTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_AuditActionTitleControlIdToReplace, AV72Pgmname, A1Usuario_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV17AuditTableName, AV9AuditDate, AV10AuditDate_To, AV5AuditAction, AV43Usuario_PessoaNom, AV37OrderedBy, AV38OrderedDsc, AV49TFAuditDate, AV50TFAuditDate_To, AV55TFAuditDescription, AV56TFAuditDescription_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFAuditAction, AV64TFAuditAction_Sel, AV53ddo_AuditDateTitleControlIdToReplace, AV57ddo_AuditDescriptionTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_AuditActionTitleControlIdToReplace, AV72Pgmname, A1Usuario_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV17AuditTableName, AV9AuditDate, AV10AuditDate_To, AV5AuditAction, AV43Usuario_PessoaNom, AV37OrderedBy, AV38OrderedDsc, AV49TFAuditDate, AV50TFAuditDate_To, AV55TFAuditDescription, AV56TFAuditDescription_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFAuditAction, AV64TFAuditAction_Sel, AV53ddo_AuditDateTitleControlIdToReplace, AV57ddo_AuditDescriptionTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_AuditActionTitleControlIdToReplace, AV72Pgmname, A1Usuario_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV17AuditTableName, AV9AuditDate, AV10AuditDate_To, AV5AuditAction, AV43Usuario_PessoaNom, AV37OrderedBy, AV38OrderedDsc, AV49TFAuditDate, AV50TFAuditDate_To, AV55TFAuditDescription, AV56TFAuditDescription_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFAuditAction, AV64TFAuditAction_Sel, AV53ddo_AuditDateTitleControlIdToReplace, AV57ddo_AuditDescriptionTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_AuditActionTitleControlIdToReplace, AV72Pgmname, A1Usuario_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV17AuditTableName, AV9AuditDate, AV10AuditDate_To, AV5AuditAction, AV43Usuario_PessoaNom, AV37OrderedBy, AV38OrderedDsc, AV49TFAuditDate, AV50TFAuditDate_To, AV55TFAuditDescription, AV56TFAuditDescription_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFAuditAction, AV64TFAuditAction_Sel, AV53ddo_AuditDateTitleControlIdToReplace, AV57ddo_AuditDescriptionTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_AuditActionTitleControlIdToReplace, AV72Pgmname, A1Usuario_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPKO0( )
      {
         /* Before Start, stand alone formulas. */
         AV72Pgmname = "WWAudit";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E16KO2 */
         E16KO2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV66DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vAUDITDATETITLEFILTERDATA"), AV48AuditDateTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAUDITDESCRIPTIONTITLEFILTERDATA"), AV54AuditDescriptionTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIO_PESSOANOMTITLEFILTERDATA"), AV58Usuario_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAUDITACTIONTITLEFILTERDATA"), AV62AuditActionTitleFilterData);
            /* Read variables values. */
            cmbavAudittablename.Name = cmbavAudittablename_Internalname;
            cmbavAudittablename.CurrentValue = cgiGet( cmbavAudittablename_Internalname);
            AV17AuditTableName = cgiGet( cmbavAudittablename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AuditTableName", AV17AuditTableName);
            if ( context.localUtil.VCDateTime( cgiGet( edtavAuditdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Audit Date"}), 1, "vAUDITDATE");
               GX_FocusControl = edtavAuditdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9AuditDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AuditDate", context.localUtil.TToC( AV9AuditDate, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV9AuditDate = context.localUtil.CToT( cgiGet( edtavAuditdate_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AuditDate", context.localUtil.TToC( AV9AuditDate, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAuditdate_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Audit Date_To"}), 1, "vAUDITDATE_TO");
               GX_FocusControl = edtavAuditdate_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV10AuditDate_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AuditDate_To", context.localUtil.TToC( AV10AuditDate_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV10AuditDate_To = context.localUtil.CToT( cgiGet( edtavAuditdate_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AuditDate_To", context.localUtil.TToC( AV10AuditDate_To, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavAuditaction.Name = cmbavAuditaction_Internalname;
            cmbavAuditaction.CurrentValue = cgiGet( cmbavAuditaction_Internalname);
            AV5AuditAction = cgiGet( cmbavAuditaction_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AuditAction", AV5AuditAction);
            AV43Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Usuario_PessoaNom", AV43Usuario_PessoaNom);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
            }
            else
            {
               AV37OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
            }
            AV38OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38OrderedDsc", AV38OrderedDsc);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfauditdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFAudit Date"}), 1, "vTFAUDITDATE");
               GX_FocusControl = edtavTfauditdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFAuditDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFAuditDate", context.localUtil.TToC( AV49TFAuditDate, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV49TFAuditDate = context.localUtil.CToT( cgiGet( edtavTfauditdate_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFAuditDate", context.localUtil.TToC( AV49TFAuditDate, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfauditdate_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFAudit Date_To"}), 1, "vTFAUDITDATE_TO");
               GX_FocusControl = edtavTfauditdate_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFAuditDate_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFAuditDate_To", context.localUtil.TToC( AV50TFAuditDate_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV50TFAuditDate_To = context.localUtil.CToT( cgiGet( edtavTfauditdate_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFAuditDate_To", context.localUtil.TToC( AV50TFAuditDate_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_auditdateauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Audit Date Aux Date"}), 1, "vDDO_AUDITDATEAUXDATE");
               GX_FocusControl = edtavDdo_auditdateauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51DDO_AuditDateAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_AuditDateAuxDate", context.localUtil.Format(AV51DDO_AuditDateAuxDate, "99/99/99"));
            }
            else
            {
               AV51DDO_AuditDateAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_auditdateauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_AuditDateAuxDate", context.localUtil.Format(AV51DDO_AuditDateAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_auditdateauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Audit Date Aux Date To"}), 1, "vDDO_AUDITDATEAUXDATETO");
               GX_FocusControl = edtavDdo_auditdateauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52DDO_AuditDateAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DDO_AuditDateAuxDateTo", context.localUtil.Format(AV52DDO_AuditDateAuxDateTo, "99/99/99"));
            }
            else
            {
               AV52DDO_AuditDateAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_auditdateauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DDO_AuditDateAuxDateTo", context.localUtil.Format(AV52DDO_AuditDateAuxDateTo, "99/99/99"));
            }
            AV55TFAuditDescription = cgiGet( edtavTfauditdescription_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFAuditDescription", AV55TFAuditDescription);
            AV56TFAuditDescription_Sel = cgiGet( edtavTfauditdescription_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFAuditDescription_Sel", AV56TFAuditDescription_Sel);
            AV59TFUsuario_PessoaNom = StringUtil.Upper( cgiGet( edtavTfusuario_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFUsuario_PessoaNom", AV59TFUsuario_PessoaNom);
            AV60TFUsuario_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfusuario_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUsuario_PessoaNom_Sel", AV60TFUsuario_PessoaNom_Sel);
            AV63TFAuditAction = cgiGet( edtavTfauditaction_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFAuditAction", AV63TFAuditAction);
            AV64TFAuditAction_Sel = cgiGet( edtavTfauditaction_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFAuditAction_Sel", AV64TFAuditAction_Sel);
            AV53ddo_AuditDateTitleControlIdToReplace = cgiGet( edtavDdo_auditdatetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_AuditDateTitleControlIdToReplace", AV53ddo_AuditDateTitleControlIdToReplace);
            AV57ddo_AuditDescriptionTitleControlIdToReplace = cgiGet( edtavDdo_auditdescriptiontitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_AuditDescriptionTitleControlIdToReplace", AV57ddo_AuditDescriptionTitleControlIdToReplace);
            AV61ddo_Usuario_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Usuario_PessoaNomTitleControlIdToReplace", AV61ddo_Usuario_PessoaNomTitleControlIdToReplace);
            AV65ddo_AuditActionTitleControlIdToReplace = cgiGet( edtavDdo_auditactiontitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_AuditActionTitleControlIdToReplace", AV65ddo_AuditActionTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_48 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_48"), ",", "."));
            AV68GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV69GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_auditdate_Caption = cgiGet( "DDO_AUDITDATE_Caption");
            Ddo_auditdate_Tooltip = cgiGet( "DDO_AUDITDATE_Tooltip");
            Ddo_auditdate_Cls = cgiGet( "DDO_AUDITDATE_Cls");
            Ddo_auditdate_Filteredtext_set = cgiGet( "DDO_AUDITDATE_Filteredtext_set");
            Ddo_auditdate_Filteredtextto_set = cgiGet( "DDO_AUDITDATE_Filteredtextto_set");
            Ddo_auditdate_Dropdownoptionstype = cgiGet( "DDO_AUDITDATE_Dropdownoptionstype");
            Ddo_auditdate_Titlecontrolidtoreplace = cgiGet( "DDO_AUDITDATE_Titlecontrolidtoreplace");
            Ddo_auditdate_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AUDITDATE_Includesortasc"));
            Ddo_auditdate_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AUDITDATE_Includesortdsc"));
            Ddo_auditdate_Sortedstatus = cgiGet( "DDO_AUDITDATE_Sortedstatus");
            Ddo_auditdate_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AUDITDATE_Includefilter"));
            Ddo_auditdate_Filtertype = cgiGet( "DDO_AUDITDATE_Filtertype");
            Ddo_auditdate_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AUDITDATE_Filterisrange"));
            Ddo_auditdate_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AUDITDATE_Includedatalist"));
            Ddo_auditdate_Sortasc = cgiGet( "DDO_AUDITDATE_Sortasc");
            Ddo_auditdate_Sortdsc = cgiGet( "DDO_AUDITDATE_Sortdsc");
            Ddo_auditdate_Cleanfilter = cgiGet( "DDO_AUDITDATE_Cleanfilter");
            Ddo_auditdate_Rangefilterfrom = cgiGet( "DDO_AUDITDATE_Rangefilterfrom");
            Ddo_auditdate_Rangefilterto = cgiGet( "DDO_AUDITDATE_Rangefilterto");
            Ddo_auditdate_Searchbuttontext = cgiGet( "DDO_AUDITDATE_Searchbuttontext");
            Ddo_auditdescription_Caption = cgiGet( "DDO_AUDITDESCRIPTION_Caption");
            Ddo_auditdescription_Tooltip = cgiGet( "DDO_AUDITDESCRIPTION_Tooltip");
            Ddo_auditdescription_Cls = cgiGet( "DDO_AUDITDESCRIPTION_Cls");
            Ddo_auditdescription_Filteredtext_set = cgiGet( "DDO_AUDITDESCRIPTION_Filteredtext_set");
            Ddo_auditdescription_Selectedvalue_set = cgiGet( "DDO_AUDITDESCRIPTION_Selectedvalue_set");
            Ddo_auditdescription_Dropdownoptionstype = cgiGet( "DDO_AUDITDESCRIPTION_Dropdownoptionstype");
            Ddo_auditdescription_Titlecontrolidtoreplace = cgiGet( "DDO_AUDITDESCRIPTION_Titlecontrolidtoreplace");
            Ddo_auditdescription_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AUDITDESCRIPTION_Includesortasc"));
            Ddo_auditdescription_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AUDITDESCRIPTION_Includesortdsc"));
            Ddo_auditdescription_Sortedstatus = cgiGet( "DDO_AUDITDESCRIPTION_Sortedstatus");
            Ddo_auditdescription_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AUDITDESCRIPTION_Includefilter"));
            Ddo_auditdescription_Filtertype = cgiGet( "DDO_AUDITDESCRIPTION_Filtertype");
            Ddo_auditdescription_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AUDITDESCRIPTION_Filterisrange"));
            Ddo_auditdescription_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AUDITDESCRIPTION_Includedatalist"));
            Ddo_auditdescription_Datalisttype = cgiGet( "DDO_AUDITDESCRIPTION_Datalisttype");
            Ddo_auditdescription_Datalistproc = cgiGet( "DDO_AUDITDESCRIPTION_Datalistproc");
            Ddo_auditdescription_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_AUDITDESCRIPTION_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_auditdescription_Sortasc = cgiGet( "DDO_AUDITDESCRIPTION_Sortasc");
            Ddo_auditdescription_Sortdsc = cgiGet( "DDO_AUDITDESCRIPTION_Sortdsc");
            Ddo_auditdescription_Loadingdata = cgiGet( "DDO_AUDITDESCRIPTION_Loadingdata");
            Ddo_auditdescription_Cleanfilter = cgiGet( "DDO_AUDITDESCRIPTION_Cleanfilter");
            Ddo_auditdescription_Noresultsfound = cgiGet( "DDO_AUDITDESCRIPTION_Noresultsfound");
            Ddo_auditdescription_Searchbuttontext = cgiGet( "DDO_AUDITDESCRIPTION_Searchbuttontext");
            Ddo_usuario_pessoanom_Caption = cgiGet( "DDO_USUARIO_PESSOANOM_Caption");
            Ddo_usuario_pessoanom_Tooltip = cgiGet( "DDO_USUARIO_PESSOANOM_Tooltip");
            Ddo_usuario_pessoanom_Cls = cgiGet( "DDO_USUARIO_PESSOANOM_Cls");
            Ddo_usuario_pessoanom_Filteredtext_set = cgiGet( "DDO_USUARIO_PESSOANOM_Filteredtext_set");
            Ddo_usuario_pessoanom_Selectedvalue_set = cgiGet( "DDO_USUARIO_PESSOANOM_Selectedvalue_set");
            Ddo_usuario_pessoanom_Dropdownoptionstype = cgiGet( "DDO_USUARIO_PESSOANOM_Dropdownoptionstype");
            Ddo_usuario_pessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIO_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_usuario_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includesortasc"));
            Ddo_usuario_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includesortdsc"));
            Ddo_usuario_pessoanom_Sortedstatus = cgiGet( "DDO_USUARIO_PESSOANOM_Sortedstatus");
            Ddo_usuario_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includefilter"));
            Ddo_usuario_pessoanom_Filtertype = cgiGet( "DDO_USUARIO_PESSOANOM_Filtertype");
            Ddo_usuario_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Filterisrange"));
            Ddo_usuario_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includedatalist"));
            Ddo_usuario_pessoanom_Datalisttype = cgiGet( "DDO_USUARIO_PESSOANOM_Datalisttype");
            Ddo_usuario_pessoanom_Datalistproc = cgiGet( "DDO_USUARIO_PESSOANOM_Datalistproc");
            Ddo_usuario_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_USUARIO_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuario_pessoanom_Sortasc = cgiGet( "DDO_USUARIO_PESSOANOM_Sortasc");
            Ddo_usuario_pessoanom_Sortdsc = cgiGet( "DDO_USUARIO_PESSOANOM_Sortdsc");
            Ddo_usuario_pessoanom_Loadingdata = cgiGet( "DDO_USUARIO_PESSOANOM_Loadingdata");
            Ddo_usuario_pessoanom_Cleanfilter = cgiGet( "DDO_USUARIO_PESSOANOM_Cleanfilter");
            Ddo_usuario_pessoanom_Noresultsfound = cgiGet( "DDO_USUARIO_PESSOANOM_Noresultsfound");
            Ddo_usuario_pessoanom_Searchbuttontext = cgiGet( "DDO_USUARIO_PESSOANOM_Searchbuttontext");
            Ddo_auditaction_Caption = cgiGet( "DDO_AUDITACTION_Caption");
            Ddo_auditaction_Tooltip = cgiGet( "DDO_AUDITACTION_Tooltip");
            Ddo_auditaction_Cls = cgiGet( "DDO_AUDITACTION_Cls");
            Ddo_auditaction_Filteredtext_set = cgiGet( "DDO_AUDITACTION_Filteredtext_set");
            Ddo_auditaction_Selectedvalue_set = cgiGet( "DDO_AUDITACTION_Selectedvalue_set");
            Ddo_auditaction_Dropdownoptionstype = cgiGet( "DDO_AUDITACTION_Dropdownoptionstype");
            Ddo_auditaction_Titlecontrolidtoreplace = cgiGet( "DDO_AUDITACTION_Titlecontrolidtoreplace");
            Ddo_auditaction_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AUDITACTION_Includesortasc"));
            Ddo_auditaction_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AUDITACTION_Includesortdsc"));
            Ddo_auditaction_Sortedstatus = cgiGet( "DDO_AUDITACTION_Sortedstatus");
            Ddo_auditaction_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AUDITACTION_Includefilter"));
            Ddo_auditaction_Filtertype = cgiGet( "DDO_AUDITACTION_Filtertype");
            Ddo_auditaction_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AUDITACTION_Filterisrange"));
            Ddo_auditaction_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AUDITACTION_Includedatalist"));
            Ddo_auditaction_Datalisttype = cgiGet( "DDO_AUDITACTION_Datalisttype");
            Ddo_auditaction_Datalistproc = cgiGet( "DDO_AUDITACTION_Datalistproc");
            Ddo_auditaction_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_AUDITACTION_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_auditaction_Sortasc = cgiGet( "DDO_AUDITACTION_Sortasc");
            Ddo_auditaction_Sortdsc = cgiGet( "DDO_AUDITACTION_Sortdsc");
            Ddo_auditaction_Loadingdata = cgiGet( "DDO_AUDITACTION_Loadingdata");
            Ddo_auditaction_Cleanfilter = cgiGet( "DDO_AUDITACTION_Cleanfilter");
            Ddo_auditaction_Noresultsfound = cgiGet( "DDO_AUDITACTION_Noresultsfound");
            Ddo_auditaction_Searchbuttontext = cgiGet( "DDO_AUDITACTION_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_auditdate_Activeeventkey = cgiGet( "DDO_AUDITDATE_Activeeventkey");
            Ddo_auditdate_Filteredtext_get = cgiGet( "DDO_AUDITDATE_Filteredtext_get");
            Ddo_auditdate_Filteredtextto_get = cgiGet( "DDO_AUDITDATE_Filteredtextto_get");
            Ddo_auditdescription_Activeeventkey = cgiGet( "DDO_AUDITDESCRIPTION_Activeeventkey");
            Ddo_auditdescription_Filteredtext_get = cgiGet( "DDO_AUDITDESCRIPTION_Filteredtext_get");
            Ddo_auditdescription_Selectedvalue_get = cgiGet( "DDO_AUDITDESCRIPTION_Selectedvalue_get");
            Ddo_usuario_pessoanom_Activeeventkey = cgiGet( "DDO_USUARIO_PESSOANOM_Activeeventkey");
            Ddo_usuario_pessoanom_Filteredtext_get = cgiGet( "DDO_USUARIO_PESSOANOM_Filteredtext_get");
            Ddo_usuario_pessoanom_Selectedvalue_get = cgiGet( "DDO_USUARIO_PESSOANOM_Selectedvalue_get");
            Ddo_auditaction_Activeeventkey = cgiGet( "DDO_AUDITACTION_Activeeventkey");
            Ddo_auditaction_Filteredtext_get = cgiGet( "DDO_AUDITACTION_Filteredtext_get");
            Ddo_auditaction_Selectedvalue_get = cgiGet( "DDO_AUDITACTION_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAUDITTABLENAME"), AV17AuditTableName) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAUDITDATE"), 0) != AV9AuditDate )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAUDITDATE_TO"), 0) != AV10AuditDate_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAUDITACTION"), AV5AuditAction) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM"), AV43Usuario_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV37OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV38OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUDITDATE"), 0) != AV49TFAuditDate )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUDITDATE_TO"), 0) != AV50TFAuditDate_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUDITDESCRIPTION"), AV55TFAuditDescription) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUDITDESCRIPTION_SEL"), AV56TFAuditDescription_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM"), AV59TFUsuario_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM_SEL"), AV60TFUsuario_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUDITACTION"), AV63TFAuditAction) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUDITACTION_SEL"), AV64TFAuditAction_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E16KO2 */
         E16KO2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16KO2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfauditdate_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfauditdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfauditdate_Visible), 5, 0)));
         edtavTfauditdate_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfauditdate_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfauditdate_to_Visible), 5, 0)));
         edtavTfauditdescription_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfauditdescription_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfauditdescription_Visible), 5, 0)));
         edtavTfauditdescription_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfauditdescription_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfauditdescription_sel_Visible), 5, 0)));
         edtavTfusuario_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_pessoanom_Visible), 5, 0)));
         edtavTfusuario_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_pessoanom_sel_Visible), 5, 0)));
         edtavTfauditaction_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfauditaction_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfauditaction_Visible), 5, 0)));
         edtavTfauditaction_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfauditaction_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfauditaction_sel_Visible), 5, 0)));
         Ddo_auditdate_Titlecontrolidtoreplace = subGrid_Internalname+"_AuditDate";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdate_Internalname, "TitleControlIdToReplace", Ddo_auditdate_Titlecontrolidtoreplace);
         AV53ddo_AuditDateTitleControlIdToReplace = Ddo_auditdate_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_AuditDateTitleControlIdToReplace", AV53ddo_AuditDateTitleControlIdToReplace);
         edtavDdo_auditdatetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_auditdatetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_auditdatetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_auditdescription_Titlecontrolidtoreplace = subGrid_Internalname+"_AuditDescription";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdescription_Internalname, "TitleControlIdToReplace", Ddo_auditdescription_Titlecontrolidtoreplace);
         AV57ddo_AuditDescriptionTitleControlIdToReplace = Ddo_auditdescription_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_AuditDescriptionTitleControlIdToReplace", AV57ddo_AuditDescriptionTitleControlIdToReplace);
         edtavDdo_auditdescriptiontitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_auditdescriptiontitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_auditdescriptiontitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_usuario_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Usuario_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_usuario_pessoanom_Titlecontrolidtoreplace);
         AV61ddo_Usuario_PessoaNomTitleControlIdToReplace = Ddo_usuario_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Usuario_PessoaNomTitleControlIdToReplace", AV61ddo_Usuario_PessoaNomTitleControlIdToReplace);
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_auditaction_Titlecontrolidtoreplace = subGrid_Internalname+"_AuditAction";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditaction_Internalname, "TitleControlIdToReplace", Ddo_auditaction_Titlecontrolidtoreplace);
         AV65ddo_AuditActionTitleControlIdToReplace = Ddo_auditaction_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_AuditActionTitleControlIdToReplace", AV65ddo_AuditActionTitleControlIdToReplace);
         edtavDdo_auditactiontitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_auditactiontitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_auditactiontitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Audit";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV37OrderedBy < 1 )
         {
            AV37OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV66DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV66DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         subGrid_Rows = 100;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
      }

      protected void E17KO2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV48AuditDateTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54AuditDescriptionTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58Usuario_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62AuditActionTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV47WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtAuditDate_Titleformat = 2;
         edtAuditDate_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data Hora", AV53ddo_AuditDateTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAuditDate_Internalname, "Title", edtAuditDate_Title);
         edtAuditDescription_Titleformat = 2;
         edtAuditDescription_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV57ddo_AuditDescriptionTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAuditDescription_Internalname, "Title", edtAuditDescription_Title);
         edtUsuario_PessoaNom_Titleformat = 2;
         edtUsuario_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usu�rio", AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Title", edtUsuario_PessoaNom_Title);
         edtAuditAction_Titleformat = 2;
         edtAuditAction_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "A��o", AV65ddo_AuditActionTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAuditAction_Internalname, "Title", edtAuditAction_Title);
         AV68GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68GridCurrentPage), 10, 0)));
         AV69GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48AuditDateTitleFilterData", AV48AuditDateTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54AuditDescriptionTitleFilterData", AV54AuditDescriptionTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58Usuario_PessoaNomTitleFilterData", AV58Usuario_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62AuditActionTitleFilterData", AV62AuditActionTitleFilterData);
      }

      protected void E11KO2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV67PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV67PageToGo) ;
         }
      }

      protected void E12KO2( )
      {
         /* Ddo_auditdate_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_auditdate_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV37OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
            AV38OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38OrderedDsc", AV38OrderedDsc);
            Ddo_auditdate_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdate_Internalname, "SortedStatus", Ddo_auditdate_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_auditdate_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV37OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
            AV38OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38OrderedDsc", AV38OrderedDsc);
            Ddo_auditdate_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdate_Internalname, "SortedStatus", Ddo_auditdate_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_auditdate_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFAuditDate = context.localUtil.CToT( Ddo_auditdate_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFAuditDate", context.localUtil.TToC( AV49TFAuditDate, 8, 5, 0, 3, "/", ":", " "));
            AV50TFAuditDate_To = context.localUtil.CToT( Ddo_auditdate_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFAuditDate_To", context.localUtil.TToC( AV50TFAuditDate_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV50TFAuditDate_To) )
            {
               AV50TFAuditDate_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV50TFAuditDate_To)), (short)(DateTimeUtil.Month( AV50TFAuditDate_To)), (short)(DateTimeUtil.Day( AV50TFAuditDate_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFAuditDate_To", context.localUtil.TToC( AV50TFAuditDate_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E13KO2( )
      {
         /* Ddo_auditdescription_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_auditdescription_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV37OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
            AV38OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38OrderedDsc", AV38OrderedDsc);
            Ddo_auditdescription_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdescription_Internalname, "SortedStatus", Ddo_auditdescription_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_auditdescription_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV37OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
            AV38OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38OrderedDsc", AV38OrderedDsc);
            Ddo_auditdescription_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdescription_Internalname, "SortedStatus", Ddo_auditdescription_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_auditdescription_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFAuditDescription = Ddo_auditdescription_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFAuditDescription", AV55TFAuditDescription);
            AV56TFAuditDescription_Sel = Ddo_auditdescription_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFAuditDescription_Sel", AV56TFAuditDescription_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E14KO2( )
      {
         /* Ddo_usuario_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV37OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
            AV38OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38OrderedDsc", AV38OrderedDsc);
            Ddo_usuario_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV37OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
            AV38OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38OrderedDsc", AV38OrderedDsc);
            Ddo_usuario_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFUsuario_PessoaNom = Ddo_usuario_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFUsuario_PessoaNom", AV59TFUsuario_PessoaNom);
            AV60TFUsuario_PessoaNom_Sel = Ddo_usuario_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUsuario_PessoaNom_Sel", AV60TFUsuario_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E15KO2( )
      {
         /* Ddo_auditaction_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_auditaction_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV37OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
            AV38OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38OrderedDsc", AV38OrderedDsc);
            Ddo_auditaction_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditaction_Internalname, "SortedStatus", Ddo_auditaction_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_auditaction_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV37OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
            AV38OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38OrderedDsc", AV38OrderedDsc);
            Ddo_auditaction_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditaction_Internalname, "SortedStatus", Ddo_auditaction_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_auditaction_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFAuditAction = Ddo_auditaction_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFAuditAction", AV63TFAuditAction);
            AV64TFAuditAction_Sel = Ddo_auditaction_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFAuditAction_Sel", AV64TFAuditAction_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E18KO2( )
      {
         /* Grid_Load Routine */
         edtUsuario_PessoaNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A1Usuario_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 48;
         }
         sendrow_482( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_48_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(48, GridRow);
         }
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_auditdate_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdate_Internalname, "SortedStatus", Ddo_auditdate_Sortedstatus);
         Ddo_auditdescription_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdescription_Internalname, "SortedStatus", Ddo_auditdescription_Sortedstatus);
         Ddo_usuario_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
         Ddo_auditaction_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditaction_Internalname, "SortedStatus", Ddo_auditaction_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV37OrderedBy == 1 )
         {
            Ddo_auditdate_Sortedstatus = (AV38OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdate_Internalname, "SortedStatus", Ddo_auditdate_Sortedstatus);
         }
         else if ( AV37OrderedBy == 2 )
         {
            Ddo_auditdescription_Sortedstatus = (AV38OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdescription_Internalname, "SortedStatus", Ddo_auditdescription_Sortedstatus);
         }
         else if ( AV37OrderedBy == 3 )
         {
            Ddo_usuario_pessoanom_Sortedstatus = (AV38OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
         }
         else if ( AV37OrderedBy == 4 )
         {
            Ddo_auditaction_Sortedstatus = (AV38OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditaction_Internalname, "SortedStatus", Ddo_auditaction_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV39Session.Get(AV72Pgmname+"GridState"), "") == 0 )
         {
            AV32GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV72Pgmname+"GridState"), "");
         }
         else
         {
            AV32GridState.FromXml(AV39Session.Get(AV72Pgmname+"GridState"), "");
         }
         AV37OrderedBy = AV32GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37OrderedBy), 4, 0)));
         AV38OrderedDsc = AV32GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38OrderedDsc", AV38OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV73GXV1 = 1;
         while ( AV73GXV1 <= AV32GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV32GridState.gxTpr_Filtervalues.Item(AV73GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "AUDITTABLENAME") == 0 )
            {
               AV17AuditTableName = AV34GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AuditTableName", AV17AuditTableName);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "AUDITDATE") == 0 )
            {
               AV9AuditDate = context.localUtil.CToT( AV34GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AuditDate", context.localUtil.TToC( AV9AuditDate, 8, 5, 0, 3, "/", ":", " "));
               AV10AuditDate_To = context.localUtil.CToT( AV34GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AuditDate_To", context.localUtil.TToC( AV10AuditDate_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "AUDITACTION") == 0 )
            {
               AV5AuditAction = AV34GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AuditAction", AV5AuditAction);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "USUARIO_PESSOANOM") == 0 )
            {
               AV43Usuario_PessoaNom = AV34GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Usuario_PessoaNom", AV43Usuario_PessoaNom);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAUDITDATE") == 0 )
            {
               AV49TFAuditDate = context.localUtil.CToT( AV34GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFAuditDate", context.localUtil.TToC( AV49TFAuditDate, 8, 5, 0, 3, "/", ":", " "));
               AV50TFAuditDate_To = context.localUtil.CToT( AV34GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFAuditDate_To", context.localUtil.TToC( AV50TFAuditDate_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV49TFAuditDate) )
               {
                  AV51DDO_AuditDateAuxDate = DateTimeUtil.ResetTime(AV49TFAuditDate);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_AuditDateAuxDate", context.localUtil.Format(AV51DDO_AuditDateAuxDate, "99/99/99"));
                  Ddo_auditdate_Filteredtext_set = context.localUtil.DToC( AV51DDO_AuditDateAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdate_Internalname, "FilteredText_set", Ddo_auditdate_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV50TFAuditDate_To) )
               {
                  AV52DDO_AuditDateAuxDateTo = DateTimeUtil.ResetTime(AV50TFAuditDate_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DDO_AuditDateAuxDateTo", context.localUtil.Format(AV52DDO_AuditDateAuxDateTo, "99/99/99"));
                  Ddo_auditdate_Filteredtextto_set = context.localUtil.DToC( AV52DDO_AuditDateAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdate_Internalname, "FilteredTextTo_set", Ddo_auditdate_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAUDITDESCRIPTION") == 0 )
            {
               AV55TFAuditDescription = AV34GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFAuditDescription", AV55TFAuditDescription);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFAuditDescription)) )
               {
                  Ddo_auditdescription_Filteredtext_set = AV55TFAuditDescription;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdescription_Internalname, "FilteredText_set", Ddo_auditdescription_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAUDITDESCRIPTION_SEL") == 0 )
            {
               AV56TFAuditDescription_Sel = AV34GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFAuditDescription_Sel", AV56TFAuditDescription_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFAuditDescription_Sel)) )
               {
                  Ddo_auditdescription_Selectedvalue_set = AV56TFAuditDescription_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditdescription_Internalname, "SelectedValue_set", Ddo_auditdescription_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM") == 0 )
            {
               AV59TFUsuario_PessoaNom = AV34GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFUsuario_PessoaNom", AV59TFUsuario_PessoaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFUsuario_PessoaNom)) )
               {
                  Ddo_usuario_pessoanom_Filteredtext_set = AV59TFUsuario_PessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "FilteredText_set", Ddo_usuario_pessoanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM_SEL") == 0 )
            {
               AV60TFUsuario_PessoaNom_Sel = AV34GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUsuario_PessoaNom_Sel", AV60TFUsuario_PessoaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel)) )
               {
                  Ddo_usuario_pessoanom_Selectedvalue_set = AV60TFUsuario_PessoaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SelectedValue_set", Ddo_usuario_pessoanom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAUDITACTION") == 0 )
            {
               AV63TFAuditAction = AV34GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFAuditAction", AV63TFAuditAction);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFAuditAction)) )
               {
                  Ddo_auditaction_Filteredtext_set = AV63TFAuditAction;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditaction_Internalname, "FilteredText_set", Ddo_auditaction_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAUDITACTION_SEL") == 0 )
            {
               AV64TFAuditAction_Sel = AV34GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFAuditAction_Sel", AV64TFAuditAction_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFAuditAction_Sel)) )
               {
                  Ddo_auditaction_Selectedvalue_set = AV64TFAuditAction_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_auditaction_Internalname, "SelectedValue_set", Ddo_auditaction_Selectedvalue_set);
               }
            }
            AV73GXV1 = (int)(AV73GXV1+1);
         }
         subgrid_gotopage( AV32GridState.gxTpr_Currentpage) ;
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV32GridState.FromXml(AV39Session.Get(AV72Pgmname+"GridState"), "");
         AV32GridState.gxTpr_Orderedby = AV37OrderedBy;
         AV32GridState.gxTpr_Ordereddsc = AV38OrderedDsc;
         AV32GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17AuditTableName)) )
         {
            AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV34GridStateFilterValue.gxTpr_Name = "AUDITTABLENAME";
            AV34GridStateFilterValue.gxTpr_Value = AV17AuditTableName;
            AV32GridState.gxTpr_Filtervalues.Add(AV34GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV9AuditDate) && (DateTime.MinValue==AV10AuditDate_To) ) )
         {
            AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV34GridStateFilterValue.gxTpr_Name = "AUDITDATE";
            AV34GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV9AuditDate, 8, 5, 0, 3, "/", ":", " ");
            AV34GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV10AuditDate_To, 8, 5, 0, 3, "/", ":", " ");
            AV32GridState.gxTpr_Filtervalues.Add(AV34GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5AuditAction)) )
         {
            AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV34GridStateFilterValue.gxTpr_Name = "AUDITACTION";
            AV34GridStateFilterValue.gxTpr_Value = AV5AuditAction;
            AV32GridState.gxTpr_Filtervalues.Add(AV34GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Usuario_PessoaNom)) )
         {
            AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV34GridStateFilterValue.gxTpr_Name = "USUARIO_PESSOANOM";
            AV34GridStateFilterValue.gxTpr_Value = AV43Usuario_PessoaNom;
            AV32GridState.gxTpr_Filtervalues.Add(AV34GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV49TFAuditDate) && (DateTime.MinValue==AV50TFAuditDate_To) ) )
         {
            AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV34GridStateFilterValue.gxTpr_Name = "TFAUDITDATE";
            AV34GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV49TFAuditDate, 8, 5, 0, 3, "/", ":", " ");
            AV34GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV50TFAuditDate_To, 8, 5, 0, 3, "/", ":", " ");
            AV32GridState.gxTpr_Filtervalues.Add(AV34GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFAuditDescription)) )
         {
            AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV34GridStateFilterValue.gxTpr_Name = "TFAUDITDESCRIPTION";
            AV34GridStateFilterValue.gxTpr_Value = AV55TFAuditDescription;
            AV32GridState.gxTpr_Filtervalues.Add(AV34GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFAuditDescription_Sel)) )
         {
            AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV34GridStateFilterValue.gxTpr_Name = "TFAUDITDESCRIPTION_SEL";
            AV34GridStateFilterValue.gxTpr_Value = AV56TFAuditDescription_Sel;
            AV32GridState.gxTpr_Filtervalues.Add(AV34GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFUsuario_PessoaNom)) )
         {
            AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV34GridStateFilterValue.gxTpr_Name = "TFUSUARIO_PESSOANOM";
            AV34GridStateFilterValue.gxTpr_Value = AV59TFUsuario_PessoaNom;
            AV32GridState.gxTpr_Filtervalues.Add(AV34GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel)) )
         {
            AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV34GridStateFilterValue.gxTpr_Name = "TFUSUARIO_PESSOANOM_SEL";
            AV34GridStateFilterValue.gxTpr_Value = AV60TFUsuario_PessoaNom_Sel;
            AV32GridState.gxTpr_Filtervalues.Add(AV34GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFAuditAction)) )
         {
            AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV34GridStateFilterValue.gxTpr_Name = "TFAUDITACTION";
            AV34GridStateFilterValue.gxTpr_Value = AV63TFAuditAction;
            AV32GridState.gxTpr_Filtervalues.Add(AV34GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFAuditAction_Sel)) )
         {
            AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV34GridStateFilterValue.gxTpr_Name = "TFAUDITACTION_SEL";
            AV34GridStateFilterValue.gxTpr_Value = AV64TFAuditAction_Sel;
            AV32GridState.gxTpr_Filtervalues.Add(AV34GridStateFilterValue, 0);
         }
         AV32GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV72Pgmname+"GridState",  AV32GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV40TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV40TrnContext.gxTpr_Callerobject = AV72Pgmname;
         AV40TrnContext.gxTpr_Callerondelete = true;
         AV40TrnContext.gxTpr_Callerurl = AV35HTTPRequest.ScriptName+"?"+AV35HTTPRequest.QueryString;
         AV40TrnContext.gxTpr_Transactionname = "Audit";
         AV39Session.Set("TrnContext", AV40TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_KO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_KO2( true) ;
         }
         else
         {
            wb_table2_8_KO2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_KO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_42_KO2( true) ;
         }
         else
         {
            wb_table3_42_KO2( false) ;
         }
         return  ;
      }

      protected void wb_table3_42_KO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KO2e( true) ;
         }
         else
         {
            wb_table1_2_KO2e( false) ;
         }
      }

      protected void wb_table3_42_KO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_45_KO2( true) ;
         }
         else
         {
            wb_table4_45_KO2( false) ;
         }
         return  ;
      }

      protected void wb_table4_45_KO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_42_KO2e( true) ;
         }
         else
         {
            wb_table3_42_KO2e( false) ;
         }
      }

      protected void wb_table4_45_KO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"48\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Auditoria") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAuditDate_Titleformat == 0 )
               {
                  context.SendWebValue( edtAuditDate_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAuditDate_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "da Tabela") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAuditDescription_Titleformat == 0 )
               {
                  context.SendWebValue( edtAuditDescription_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAuditDescription_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Compacta") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Usu�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAuditAction_Titleformat == 0 )
               {
                  context.SendWebValue( edtAuditAction_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAuditAction_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1430AuditId), 18, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1431AuditDate, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAuditDate_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAuditDate_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1432AuditTableName));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1434AuditDescription);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAuditDescription_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAuditDescription_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1433AuditShortDescription);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A58Usuario_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtUsuario_PessoaNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2Usuario_Nome));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1435AuditAction));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAuditAction_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAuditAction_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 48 )
         {
            wbEnd = 0;
            nRC_GXsfl_48 = (short)(nGXsfl_48_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_45_KO2e( true) ;
         }
         else
         {
            wb_table4_45_KO2e( false) ;
         }
      }

      protected void wb_table2_8_KO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAudittitle_Internalname, "Auditoria", "", "", lblAudittitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWAudit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_KO2( true) ;
         }
         else
         {
            wb_table5_14_KO2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_KO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_KO2e( true) ;
         }
         else
         {
            wb_table2_8_KO2e( false) ;
         }
      }

      protected void wb_table5_14_KO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTeste_Internalname, tblTeste_Internalname, "", "Table", 0, "", "", 2, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextaudittablename_Internalname, "Tabela", "", "", lblFiltertextaudittablename_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WWAudit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_48_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAudittablename, cmbavAudittablename_Internalname, StringUtil.RTrim( AV17AuditTableName), 1, cmbavAudittablename_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WWAudit.htm");
            cmbavAudittablename.CurrentValue = StringUtil.RTrim( AV17AuditTableName);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAudittablename_Internalname, "Values", (String)(cmbavAudittablename.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextauditdate_Internalname, "Data Hora", "", "", lblFiltertextauditdate_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WWAudit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_KO2( true) ;
         }
         else
         {
            wb_table6_23_KO2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_KO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextauditaction_Internalname, "A��o", "", "", lblFiltertextauditaction_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WWAudit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_48_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAuditaction, cmbavAuditaction_Internalname, StringUtil.RTrim( AV5AuditAction), 1, cmbavAuditaction_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_WWAudit.htm");
            cmbavAuditaction.CurrentValue = StringUtil.RTrim( AV5AuditAction);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAuditaction_Internalname, "Values", (String)(cmbavAuditaction.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextusuario_pessoanom_Internalname, "Usu�rio", "", "", lblFiltertextusuario_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WWAudit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom_Internalname, StringUtil.RTrim( AV43Usuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV43Usuario_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAudit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_KO2e( true) ;
         }
         else
         {
            wb_table5_14_KO2e( false) ;
         }
      }

      protected void wb_table6_23_KO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedauditdate_Internalname, tblTablemergedauditdate_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_48_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAuditdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAuditdate_Internalname, context.localUtil.TToC( AV9AuditDate, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV9AuditDate, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAuditdate_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAudit.htm");
            GxWebStd.gx_bitmap( context, edtavAuditdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAudit.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAuditdate_rangemiddletext_Internalname, "at�", "", "", lblAuditdate_rangemiddletext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAudit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_48_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAuditdate_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAuditdate_to_Internalname, context.localUtil.TToC( AV10AuditDate_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV10AuditDate_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAuditdate_to_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAudit.htm");
            GxWebStd.gx_bitmap( context, edtavAuditdate_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAudit.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_KO2e( true) ;
         }
         else
         {
            wb_table6_23_KO2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKO2( ) ;
         WSKO2( ) ;
         WEKO2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020529942852");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwaudit.js", "?2020529942853");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_482( )
      {
         edtAuditId_Internalname = "AUDITID_"+sGXsfl_48_idx;
         edtAuditDate_Internalname = "AUDITDATE_"+sGXsfl_48_idx;
         edtAuditTableName_Internalname = "AUDITTABLENAME_"+sGXsfl_48_idx;
         edtAuditDescription_Internalname = "AUDITDESCRIPTION_"+sGXsfl_48_idx;
         edtAuditShortDescription_Internalname = "AUDITSHORTDESCRIPTION_"+sGXsfl_48_idx;
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_48_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_48_idx;
         edtUsuario_Nome_Internalname = "USUARIO_NOME_"+sGXsfl_48_idx;
         edtAuditAction_Internalname = "AUDITACTION_"+sGXsfl_48_idx;
      }

      protected void SubsflControlProps_fel_482( )
      {
         edtAuditId_Internalname = "AUDITID_"+sGXsfl_48_fel_idx;
         edtAuditDate_Internalname = "AUDITDATE_"+sGXsfl_48_fel_idx;
         edtAuditTableName_Internalname = "AUDITTABLENAME_"+sGXsfl_48_fel_idx;
         edtAuditDescription_Internalname = "AUDITDESCRIPTION_"+sGXsfl_48_fel_idx;
         edtAuditShortDescription_Internalname = "AUDITSHORTDESCRIPTION_"+sGXsfl_48_fel_idx;
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_48_fel_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_48_fel_idx;
         edtUsuario_Nome_Internalname = "USUARIO_NOME_"+sGXsfl_48_fel_idx;
         edtAuditAction_Internalname = "AUDITACTION_"+sGXsfl_48_fel_idx;
      }

      protected void sendrow_482( )
      {
         SubsflControlProps_482( ) ;
         WBKO0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_48_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_48_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_48_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAuditId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1430AuditId), 18, 0, ",", "")),context.localUtil.Format( (decimal)(A1430AuditId), "ZZZZZZZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAuditId_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)0,(bool)true,(String)"Id",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAuditDate_Internalname,context.localUtil.TToC( A1431AuditDate, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1431AuditDate, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAuditDate_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAuditTableName_Internalname,StringUtil.RTrim( A1432AuditTableName),StringUtil.RTrim( context.localUtil.Format( A1432AuditTableName, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAuditTableName_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAuditDescription_Internalname,(String)A1434AuditDescription,(String)A1434AuditDescription,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAuditDescription_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)48,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAuditShortDescription_Internalname,(String)A1433AuditShortDescription,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAuditShortDescription_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)400,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_PessoaNom_Internalname,StringUtil.RTrim( A58Usuario_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtUsuario_PessoaNom_Link,(String)"",(String)"",(String)"",(String)edtUsuario_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Nome_Internalname,StringUtil.RTrim( A2Usuario_Nome),StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAuditAction_Internalname,StringUtil.RTrim( A1435AuditAction),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAuditAction_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_AUDITID"+"_"+sGXsfl_48_idx, GetSecureSignedToken( sGXsfl_48_idx, context.localUtil.Format( (decimal)(A1430AuditId), "ZZZZZZZZZZZZZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AUDITDATE"+"_"+sGXsfl_48_idx, GetSecureSignedToken( sGXsfl_48_idx, context.localUtil.Format( A1431AuditDate, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_AUDITTABLENAME"+"_"+sGXsfl_48_idx, GetSecureSignedToken( sGXsfl_48_idx, StringUtil.RTrim( context.localUtil.Format( A1432AuditTableName, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_AUDITDESCRIPTION"+"_"+sGXsfl_48_idx, GetSecureSignedToken( sGXsfl_48_idx, A1434AuditDescription));
            GxWebStd.gx_hidden_field( context, "gxhash_AUDITSHORTDESCRIPTION"+"_"+sGXsfl_48_idx, GetSecureSignedToken( sGXsfl_48_idx, StringUtil.RTrim( context.localUtil.Format( A1433AuditShortDescription, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CODIGO"+"_"+sGXsfl_48_idx, GetSecureSignedToken( sGXsfl_48_idx, context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AUDITACTION"+"_"+sGXsfl_48_idx, GetSecureSignedToken( sGXsfl_48_idx, StringUtil.RTrim( context.localUtil.Format( A1435AuditAction, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_48_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_48_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_48_idx+1));
            sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
            SubsflControlProps_482( ) ;
         }
         /* End function sendrow_482 */
      }

      protected void init_default_properties( )
      {
         lblAudittitle_Internalname = "AUDITTITLE";
         lblFiltertextaudittablename_Internalname = "FILTERTEXTAUDITTABLENAME";
         cmbavAudittablename_Internalname = "vAUDITTABLENAME";
         lblFiltertextauditdate_Internalname = "FILTERTEXTAUDITDATE";
         edtavAuditdate_Internalname = "vAUDITDATE";
         lblAuditdate_rangemiddletext_Internalname = "AUDITDATE_RANGEMIDDLETEXT";
         edtavAuditdate_to_Internalname = "vAUDITDATE_TO";
         tblTablemergedauditdate_Internalname = "TABLEMERGEDAUDITDATE";
         lblFiltertextauditaction_Internalname = "FILTERTEXTAUDITACTION";
         cmbavAuditaction_Internalname = "vAUDITACTION";
         lblFiltertextusuario_pessoanom_Internalname = "FILTERTEXTUSUARIO_PESSOANOM";
         edtavUsuario_pessoanom_Internalname = "vUSUARIO_PESSOANOM";
         tblTeste_Internalname = "TESTE";
         tblTableheader_Internalname = "TABLEHEADER";
         edtAuditId_Internalname = "AUDITID";
         edtAuditDate_Internalname = "AUDITDATE";
         edtAuditTableName_Internalname = "AUDITTABLENAME";
         edtAuditDescription_Internalname = "AUDITDESCRIPTION";
         edtAuditShortDescription_Internalname = "AUDITSHORTDESCRIPTION";
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO";
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM";
         edtUsuario_Nome_Internalname = "USUARIO_NOME";
         edtAuditAction_Internalname = "AUDITACTION";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         edtavTfauditdate_Internalname = "vTFAUDITDATE";
         edtavTfauditdate_to_Internalname = "vTFAUDITDATE_TO";
         edtavDdo_auditdateauxdate_Internalname = "vDDO_AUDITDATEAUXDATE";
         edtavDdo_auditdateauxdateto_Internalname = "vDDO_AUDITDATEAUXDATETO";
         divDdo_auditdateauxdates_Internalname = "DDO_AUDITDATEAUXDATES";
         edtavTfauditdescription_Internalname = "vTFAUDITDESCRIPTION";
         edtavTfauditdescription_sel_Internalname = "vTFAUDITDESCRIPTION_SEL";
         edtavTfusuario_pessoanom_Internalname = "vTFUSUARIO_PESSOANOM";
         edtavTfusuario_pessoanom_sel_Internalname = "vTFUSUARIO_PESSOANOM_SEL";
         edtavTfauditaction_Internalname = "vTFAUDITACTION";
         edtavTfauditaction_sel_Internalname = "vTFAUDITACTION_SEL";
         Ddo_auditdate_Internalname = "DDO_AUDITDATE";
         edtavDdo_auditdatetitlecontrolidtoreplace_Internalname = "vDDO_AUDITDATETITLECONTROLIDTOREPLACE";
         Ddo_auditdescription_Internalname = "DDO_AUDITDESCRIPTION";
         edtavDdo_auditdescriptiontitlecontrolidtoreplace_Internalname = "vDDO_AUDITDESCRIPTIONTITLECONTROLIDTOREPLACE";
         Ddo_usuario_pessoanom_Internalname = "DDO_USUARIO_PESSOANOM";
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname = "vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_auditaction_Internalname = "DDO_AUDITACTION";
         edtavDdo_auditactiontitlecontrolidtoreplace_Internalname = "vDDO_AUDITACTIONTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtAuditAction_Jsonclick = "";
         edtUsuario_Nome_Jsonclick = "";
         edtUsuario_PessoaNom_Jsonclick = "";
         edtUsuario_Codigo_Jsonclick = "";
         edtAuditShortDescription_Jsonclick = "";
         edtAuditDescription_Jsonclick = "";
         edtAuditTableName_Jsonclick = "";
         edtAuditDate_Jsonclick = "";
         edtAuditId_Jsonclick = "";
         edtavAuditdate_to_Jsonclick = "";
         edtavAuditdate_Jsonclick = "";
         edtavUsuario_pessoanom_Jsonclick = "";
         cmbavAuditaction_Jsonclick = "";
         cmbavAudittablename_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtUsuario_PessoaNom_Link = "";
         edtAuditAction_Titleformat = 0;
         edtUsuario_PessoaNom_Titleformat = 0;
         edtAuditDescription_Titleformat = 0;
         edtAuditDate_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtAuditAction_Title = "A��o";
         edtUsuario_PessoaNom_Title = "Usu�rio";
         edtAuditDescription_Title = "Descri��o";
         edtAuditDate_Title = "Data Hora";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_auditactiontitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_auditdescriptiontitlecontrolidtoreplace_Visible = 1;
         edtavDdo_auditdatetitlecontrolidtoreplace_Visible = 1;
         edtavTfauditaction_sel_Jsonclick = "";
         edtavTfauditaction_sel_Visible = 1;
         edtavTfauditaction_Jsonclick = "";
         edtavTfauditaction_Visible = 1;
         edtavTfusuario_pessoanom_sel_Jsonclick = "";
         edtavTfusuario_pessoanom_sel_Visible = 1;
         edtavTfusuario_pessoanom_Jsonclick = "";
         edtavTfusuario_pessoanom_Visible = 1;
         edtavTfauditdescription_sel_Visible = 1;
         edtavTfauditdescription_Visible = 1;
         edtavDdo_auditdateauxdateto_Jsonclick = "";
         edtavDdo_auditdateauxdate_Jsonclick = "";
         edtavTfauditdate_to_Jsonclick = "";
         edtavTfauditdate_to_Visible = 1;
         edtavTfauditdate_Jsonclick = "";
         edtavTfauditdate_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Ddo_auditaction_Searchbuttontext = "Pesquisar";
         Ddo_auditaction_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_auditaction_Cleanfilter = "Limpar pesquisa";
         Ddo_auditaction_Loadingdata = "Carregando dados...";
         Ddo_auditaction_Sortdsc = "Ordenar de Z � A";
         Ddo_auditaction_Sortasc = "Ordenar de A � Z";
         Ddo_auditaction_Datalistupdateminimumcharacters = 0;
         Ddo_auditaction_Datalistproc = "GetWWAuditFilterData";
         Ddo_auditaction_Datalisttype = "Dynamic";
         Ddo_auditaction_Includedatalist = Convert.ToBoolean( -1);
         Ddo_auditaction_Filterisrange = Convert.ToBoolean( 0);
         Ddo_auditaction_Filtertype = "Character";
         Ddo_auditaction_Includefilter = Convert.ToBoolean( -1);
         Ddo_auditaction_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_auditaction_Includesortasc = Convert.ToBoolean( -1);
         Ddo_auditaction_Titlecontrolidtoreplace = "";
         Ddo_auditaction_Dropdownoptionstype = "GridTitleSettings";
         Ddo_auditaction_Cls = "ColumnSettings";
         Ddo_auditaction_Tooltip = "Op��es";
         Ddo_auditaction_Caption = "";
         Ddo_usuario_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_usuario_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuario_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_usuario_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_usuario_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_usuario_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_usuario_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_usuario_pessoanom_Datalistproc = "GetWWAuditFilterData";
         Ddo_usuario_pessoanom_Datalisttype = "Dynamic";
         Ddo_usuario_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuario_pessoanom_Filtertype = "Character";
         Ddo_usuario_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_usuario_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuario_pessoanom_Cls = "ColumnSettings";
         Ddo_usuario_pessoanom_Tooltip = "Op��es";
         Ddo_usuario_pessoanom_Caption = "";
         Ddo_auditdescription_Searchbuttontext = "Pesquisar";
         Ddo_auditdescription_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_auditdescription_Cleanfilter = "Limpar pesquisa";
         Ddo_auditdescription_Loadingdata = "Carregando dados...";
         Ddo_auditdescription_Sortdsc = "Ordenar de Z � A";
         Ddo_auditdescription_Sortasc = "Ordenar de A � Z";
         Ddo_auditdescription_Datalistupdateminimumcharacters = 0;
         Ddo_auditdescription_Datalistproc = "GetWWAuditFilterData";
         Ddo_auditdescription_Datalisttype = "Dynamic";
         Ddo_auditdescription_Includedatalist = Convert.ToBoolean( -1);
         Ddo_auditdescription_Filterisrange = Convert.ToBoolean( 0);
         Ddo_auditdescription_Filtertype = "Character";
         Ddo_auditdescription_Includefilter = Convert.ToBoolean( -1);
         Ddo_auditdescription_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_auditdescription_Includesortasc = Convert.ToBoolean( -1);
         Ddo_auditdescription_Titlecontrolidtoreplace = "";
         Ddo_auditdescription_Dropdownoptionstype = "GridTitleSettings";
         Ddo_auditdescription_Cls = "ColumnSettings";
         Ddo_auditdescription_Tooltip = "Op��es";
         Ddo_auditdescription_Caption = "";
         Ddo_auditdate_Searchbuttontext = "Pesquisar";
         Ddo_auditdate_Rangefilterto = "At�";
         Ddo_auditdate_Rangefilterfrom = "Desde";
         Ddo_auditdate_Cleanfilter = "Limpar pesquisa";
         Ddo_auditdate_Sortdsc = "Ordenar de Z � A";
         Ddo_auditdate_Sortasc = "Ordenar de A � Z";
         Ddo_auditdate_Includedatalist = Convert.ToBoolean( 0);
         Ddo_auditdate_Filterisrange = Convert.ToBoolean( -1);
         Ddo_auditdate_Filtertype = "Date";
         Ddo_auditdate_Includefilter = Convert.ToBoolean( -1);
         Ddo_auditdate_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_auditdate_Includesortasc = Convert.ToBoolean( -1);
         Ddo_auditdate_Titlecontrolidtoreplace = "";
         Ddo_auditdate_Dropdownoptionstype = "GridTitleSettings";
         Ddo_auditdate_Cls = "ColumnSettings";
         Ddo_auditdate_Tooltip = "Op��es";
         Ddo_auditdate_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Audit";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV53ddo_AuditDateTitleControlIdToReplace',fld:'vDDO_AUDITDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_AuditDescriptionTitleControlIdToReplace',fld:'vDDO_AUDITDESCRIPTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_AuditActionTitleControlIdToReplace',fld:'vDDO_AUDITACTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV37OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV38OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17AuditTableName',fld:'vAUDITTABLENAME',pic:'@!',nv:''},{av:'AV9AuditDate',fld:'vAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV10AuditDate_To',fld:'vAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV5AuditAction',fld:'vAUDITACTION',pic:'',nv:''},{av:'AV43Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV49TFAuditDate',fld:'vTFAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV50TFAuditDate_To',fld:'vTFAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFAuditDescription',fld:'vTFAUDITDESCRIPTION',pic:'',nv:''},{av:'AV56TFAuditDescription_Sel',fld:'vTFAUDITDESCRIPTION_SEL',pic:'',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFAuditAction',fld:'vTFAUDITACTION',pic:'',nv:''},{av:'AV64TFAuditAction_Sel',fld:'vTFAUDITACTION_SEL',pic:'',nv:''}],oparms:[{av:'AV48AuditDateTitleFilterData',fld:'vAUDITDATETITLEFILTERDATA',pic:'',nv:null},{av:'AV54AuditDescriptionTitleFilterData',fld:'vAUDITDESCRIPTIONTITLEFILTERDATA',pic:'',nv:null},{av:'AV58Usuario_PessoaNomTitleFilterData',fld:'vUSUARIO_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV62AuditActionTitleFilterData',fld:'vAUDITACTIONTITLEFILTERDATA',pic:'',nv:null},{av:'edtAuditDate_Titleformat',ctrl:'AUDITDATE',prop:'Titleformat'},{av:'edtAuditDate_Title',ctrl:'AUDITDATE',prop:'Title'},{av:'edtAuditDescription_Titleformat',ctrl:'AUDITDESCRIPTION',prop:'Titleformat'},{av:'edtAuditDescription_Title',ctrl:'AUDITDESCRIPTION',prop:'Title'},{av:'edtUsuario_PessoaNom_Titleformat',ctrl:'USUARIO_PESSOANOM',prop:'Titleformat'},{av:'edtUsuario_PessoaNom_Title',ctrl:'USUARIO_PESSOANOM',prop:'Title'},{av:'edtAuditAction_Titleformat',ctrl:'AUDITACTION',prop:'Titleformat'},{av:'edtAuditAction_Title',ctrl:'AUDITACTION',prop:'Title'},{av:'AV68GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV69GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11KO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17AuditTableName',fld:'vAUDITTABLENAME',pic:'@!',nv:''},{av:'AV9AuditDate',fld:'vAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV10AuditDate_To',fld:'vAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV5AuditAction',fld:'vAUDITACTION',pic:'',nv:''},{av:'AV43Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV37OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV38OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV49TFAuditDate',fld:'vTFAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV50TFAuditDate_To',fld:'vTFAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFAuditDescription',fld:'vTFAUDITDESCRIPTION',pic:'',nv:''},{av:'AV56TFAuditDescription_Sel',fld:'vTFAUDITDESCRIPTION_SEL',pic:'',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFAuditAction',fld:'vTFAUDITACTION',pic:'',nv:''},{av:'AV64TFAuditAction_Sel',fld:'vTFAUDITACTION_SEL',pic:'',nv:''},{av:'AV53ddo_AuditDateTitleControlIdToReplace',fld:'vDDO_AUDITDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_AuditDescriptionTitleControlIdToReplace',fld:'vDDO_AUDITDESCRIPTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_AuditActionTitleControlIdToReplace',fld:'vDDO_AUDITACTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_AUDITDATE.ONOPTIONCLICKED","{handler:'E12KO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17AuditTableName',fld:'vAUDITTABLENAME',pic:'@!',nv:''},{av:'AV9AuditDate',fld:'vAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV10AuditDate_To',fld:'vAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV5AuditAction',fld:'vAUDITACTION',pic:'',nv:''},{av:'AV43Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV37OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV38OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV49TFAuditDate',fld:'vTFAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV50TFAuditDate_To',fld:'vTFAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFAuditDescription',fld:'vTFAUDITDESCRIPTION',pic:'',nv:''},{av:'AV56TFAuditDescription_Sel',fld:'vTFAUDITDESCRIPTION_SEL',pic:'',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFAuditAction',fld:'vTFAUDITACTION',pic:'',nv:''},{av:'AV64TFAuditAction_Sel',fld:'vTFAUDITACTION_SEL',pic:'',nv:''},{av:'AV53ddo_AuditDateTitleControlIdToReplace',fld:'vDDO_AUDITDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_AuditDescriptionTitleControlIdToReplace',fld:'vDDO_AUDITDESCRIPTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_AuditActionTitleControlIdToReplace',fld:'vDDO_AUDITACTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_auditdate_Activeeventkey',ctrl:'DDO_AUDITDATE',prop:'ActiveEventKey'},{av:'Ddo_auditdate_Filteredtext_get',ctrl:'DDO_AUDITDATE',prop:'FilteredText_get'},{av:'Ddo_auditdate_Filteredtextto_get',ctrl:'DDO_AUDITDATE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV37OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV38OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_auditdate_Sortedstatus',ctrl:'DDO_AUDITDATE',prop:'SortedStatus'},{av:'AV49TFAuditDate',fld:'vTFAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV50TFAuditDate_To',fld:'vTFAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_auditdescription_Sortedstatus',ctrl:'DDO_AUDITDESCRIPTION',prop:'SortedStatus'},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_auditaction_Sortedstatus',ctrl:'DDO_AUDITACTION',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUDITDESCRIPTION.ONOPTIONCLICKED","{handler:'E13KO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17AuditTableName',fld:'vAUDITTABLENAME',pic:'@!',nv:''},{av:'AV9AuditDate',fld:'vAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV10AuditDate_To',fld:'vAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV5AuditAction',fld:'vAUDITACTION',pic:'',nv:''},{av:'AV43Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV37OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV38OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV49TFAuditDate',fld:'vTFAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV50TFAuditDate_To',fld:'vTFAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFAuditDescription',fld:'vTFAUDITDESCRIPTION',pic:'',nv:''},{av:'AV56TFAuditDescription_Sel',fld:'vTFAUDITDESCRIPTION_SEL',pic:'',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFAuditAction',fld:'vTFAUDITACTION',pic:'',nv:''},{av:'AV64TFAuditAction_Sel',fld:'vTFAUDITACTION_SEL',pic:'',nv:''},{av:'AV53ddo_AuditDateTitleControlIdToReplace',fld:'vDDO_AUDITDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_AuditDescriptionTitleControlIdToReplace',fld:'vDDO_AUDITDESCRIPTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_AuditActionTitleControlIdToReplace',fld:'vDDO_AUDITACTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_auditdescription_Activeeventkey',ctrl:'DDO_AUDITDESCRIPTION',prop:'ActiveEventKey'},{av:'Ddo_auditdescription_Filteredtext_get',ctrl:'DDO_AUDITDESCRIPTION',prop:'FilteredText_get'},{av:'Ddo_auditdescription_Selectedvalue_get',ctrl:'DDO_AUDITDESCRIPTION',prop:'SelectedValue_get'}],oparms:[{av:'AV37OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV38OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_auditdescription_Sortedstatus',ctrl:'DDO_AUDITDESCRIPTION',prop:'SortedStatus'},{av:'AV55TFAuditDescription',fld:'vTFAUDITDESCRIPTION',pic:'',nv:''},{av:'AV56TFAuditDescription_Sel',fld:'vTFAUDITDESCRIPTION_SEL',pic:'',nv:''},{av:'Ddo_auditdate_Sortedstatus',ctrl:'DDO_AUDITDATE',prop:'SortedStatus'},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_auditaction_Sortedstatus',ctrl:'DDO_AUDITACTION',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_USUARIO_PESSOANOM.ONOPTIONCLICKED","{handler:'E14KO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17AuditTableName',fld:'vAUDITTABLENAME',pic:'@!',nv:''},{av:'AV9AuditDate',fld:'vAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV10AuditDate_To',fld:'vAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV5AuditAction',fld:'vAUDITACTION',pic:'',nv:''},{av:'AV43Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV37OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV38OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV49TFAuditDate',fld:'vTFAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV50TFAuditDate_To',fld:'vTFAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFAuditDescription',fld:'vTFAUDITDESCRIPTION',pic:'',nv:''},{av:'AV56TFAuditDescription_Sel',fld:'vTFAUDITDESCRIPTION_SEL',pic:'',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFAuditAction',fld:'vTFAUDITACTION',pic:'',nv:''},{av:'AV64TFAuditAction_Sel',fld:'vTFAUDITACTION_SEL',pic:'',nv:''},{av:'AV53ddo_AuditDateTitleControlIdToReplace',fld:'vDDO_AUDITDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_AuditDescriptionTitleControlIdToReplace',fld:'vDDO_AUDITDESCRIPTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_AuditActionTitleControlIdToReplace',fld:'vDDO_AUDITACTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_usuario_pessoanom_Activeeventkey',ctrl:'DDO_USUARIO_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_usuario_pessoanom_Filteredtext_get',ctrl:'DDO_USUARIO_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_usuario_pessoanom_Selectedvalue_get',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV37OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV38OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_auditdate_Sortedstatus',ctrl:'DDO_AUDITDATE',prop:'SortedStatus'},{av:'Ddo_auditdescription_Sortedstatus',ctrl:'DDO_AUDITDESCRIPTION',prop:'SortedStatus'},{av:'Ddo_auditaction_Sortedstatus',ctrl:'DDO_AUDITACTION',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUDITACTION.ONOPTIONCLICKED","{handler:'E15KO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV17AuditTableName',fld:'vAUDITTABLENAME',pic:'@!',nv:''},{av:'AV9AuditDate',fld:'vAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV10AuditDate_To',fld:'vAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV5AuditAction',fld:'vAUDITACTION',pic:'',nv:''},{av:'AV43Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV37OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV38OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV49TFAuditDate',fld:'vTFAUDITDATE',pic:'99/99/99 99:99',nv:''},{av:'AV50TFAuditDate_To',fld:'vTFAUDITDATE_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFAuditDescription',fld:'vTFAUDITDESCRIPTION',pic:'',nv:''},{av:'AV56TFAuditDescription_Sel',fld:'vTFAUDITDESCRIPTION_SEL',pic:'',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFAuditAction',fld:'vTFAUDITACTION',pic:'',nv:''},{av:'AV64TFAuditAction_Sel',fld:'vTFAUDITACTION_SEL',pic:'',nv:''},{av:'AV53ddo_AuditDateTitleControlIdToReplace',fld:'vDDO_AUDITDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_AuditDescriptionTitleControlIdToReplace',fld:'vDDO_AUDITDESCRIPTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_AuditActionTitleControlIdToReplace',fld:'vDDO_AUDITACTIONTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_auditaction_Activeeventkey',ctrl:'DDO_AUDITACTION',prop:'ActiveEventKey'},{av:'Ddo_auditaction_Filteredtext_get',ctrl:'DDO_AUDITACTION',prop:'FilteredText_get'},{av:'Ddo_auditaction_Selectedvalue_get',ctrl:'DDO_AUDITACTION',prop:'SelectedValue_get'}],oparms:[{av:'AV37OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV38OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_auditaction_Sortedstatus',ctrl:'DDO_AUDITACTION',prop:'SortedStatus'},{av:'AV63TFAuditAction',fld:'vTFAUDITACTION',pic:'',nv:''},{av:'AV64TFAuditAction_Sel',fld:'vTFAUDITACTION_SEL',pic:'',nv:''},{av:'Ddo_auditdate_Sortedstatus',ctrl:'DDO_AUDITDATE',prop:'SortedStatus'},{av:'Ddo_auditdescription_Sortedstatus',ctrl:'DDO_AUDITDESCRIPTION',prop:'SortedStatus'},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E18KO2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtUsuario_PessoaNom_Link',ctrl:'USUARIO_PESSOANOM',prop:'Link'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_auditdate_Activeeventkey = "";
         Ddo_auditdate_Filteredtext_get = "";
         Ddo_auditdate_Filteredtextto_get = "";
         Ddo_auditdescription_Activeeventkey = "";
         Ddo_auditdescription_Filteredtext_get = "";
         Ddo_auditdescription_Selectedvalue_get = "";
         Ddo_usuario_pessoanom_Activeeventkey = "";
         Ddo_usuario_pessoanom_Filteredtext_get = "";
         Ddo_usuario_pessoanom_Selectedvalue_get = "";
         Ddo_auditaction_Activeeventkey = "";
         Ddo_auditaction_Filteredtext_get = "";
         Ddo_auditaction_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV17AuditTableName = "";
         AV9AuditDate = (DateTime)(DateTime.MinValue);
         AV10AuditDate_To = (DateTime)(DateTime.MinValue);
         AV5AuditAction = "";
         AV43Usuario_PessoaNom = "";
         AV49TFAuditDate = (DateTime)(DateTime.MinValue);
         AV50TFAuditDate_To = (DateTime)(DateTime.MinValue);
         AV55TFAuditDescription = "";
         AV56TFAuditDescription_Sel = "";
         AV59TFUsuario_PessoaNom = "";
         AV60TFUsuario_PessoaNom_Sel = "";
         AV63TFAuditAction = "";
         AV64TFAuditAction_Sel = "";
         AV53ddo_AuditDateTitleControlIdToReplace = "";
         AV57ddo_AuditDescriptionTitleControlIdToReplace = "";
         AV61ddo_Usuario_PessoaNomTitleControlIdToReplace = "";
         AV65ddo_AuditActionTitleControlIdToReplace = "";
         AV72Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV66DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV48AuditDateTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54AuditDescriptionTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58Usuario_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62AuditActionTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_auditdate_Filteredtext_set = "";
         Ddo_auditdate_Filteredtextto_set = "";
         Ddo_auditdate_Sortedstatus = "";
         Ddo_auditdescription_Filteredtext_set = "";
         Ddo_auditdescription_Selectedvalue_set = "";
         Ddo_auditdescription_Sortedstatus = "";
         Ddo_usuario_pessoanom_Filteredtext_set = "";
         Ddo_usuario_pessoanom_Selectedvalue_set = "";
         Ddo_usuario_pessoanom_Sortedstatus = "";
         Ddo_auditaction_Filteredtext_set = "";
         Ddo_auditaction_Selectedvalue_set = "";
         Ddo_auditaction_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         AV51DDO_AuditDateAuxDate = DateTime.MinValue;
         AV52DDO_AuditDateAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A1431AuditDate = (DateTime)(DateTime.MinValue);
         A1432AuditTableName = "";
         A1434AuditDescription = "";
         A1433AuditShortDescription = "";
         A58Usuario_PessoaNom = "";
         A2Usuario_Nome = "";
         A1435AuditAction = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV43Usuario_PessoaNom = "";
         lV55TFAuditDescription = "";
         lV59TFUsuario_PessoaNom = "";
         lV63TFAuditAction = "";
         H00KO2_A57Usuario_PessoaCod = new int[1] ;
         H00KO2_A1432AuditTableName = new String[] {""} ;
         H00KO2_n1432AuditTableName = new bool[] {false} ;
         H00KO2_A1435AuditAction = new String[] {""} ;
         H00KO2_n1435AuditAction = new bool[] {false} ;
         H00KO2_A2Usuario_Nome = new String[] {""} ;
         H00KO2_n2Usuario_Nome = new bool[] {false} ;
         H00KO2_A58Usuario_PessoaNom = new String[] {""} ;
         H00KO2_n58Usuario_PessoaNom = new bool[] {false} ;
         H00KO2_A1Usuario_Codigo = new int[1] ;
         H00KO2_n1Usuario_Codigo = new bool[] {false} ;
         H00KO2_A1433AuditShortDescription = new String[] {""} ;
         H00KO2_n1433AuditShortDescription = new bool[] {false} ;
         H00KO2_A1434AuditDescription = new String[] {""} ;
         H00KO2_n1434AuditDescription = new bool[] {false} ;
         H00KO2_A1431AuditDate = new DateTime[] {DateTime.MinValue} ;
         H00KO2_n1431AuditDate = new bool[] {false} ;
         H00KO2_A1430AuditId = new long[1] ;
         H00KO3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV47WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV39Session = context.GetSession();
         AV32GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV40TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV35HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblAudittitle_Jsonclick = "";
         lblFiltertextaudittablename_Jsonclick = "";
         lblFiltertextauditdate_Jsonclick = "";
         lblFiltertextauditaction_Jsonclick = "";
         lblFiltertextusuario_pessoanom_Jsonclick = "";
         lblAuditdate_rangemiddletext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwaudit__default(),
            new Object[][] {
                new Object[] {
               H00KO2_A57Usuario_PessoaCod, H00KO2_A1432AuditTableName, H00KO2_n1432AuditTableName, H00KO2_A1435AuditAction, H00KO2_n1435AuditAction, H00KO2_A2Usuario_Nome, H00KO2_n2Usuario_Nome, H00KO2_A58Usuario_PessoaNom, H00KO2_n58Usuario_PessoaNom, H00KO2_A1Usuario_Codigo,
               H00KO2_n1Usuario_Codigo, H00KO2_A1433AuditShortDescription, H00KO2_n1433AuditShortDescription, H00KO2_A1434AuditDescription, H00KO2_n1434AuditDescription, H00KO2_A1431AuditDate, H00KO2_n1431AuditDate, H00KO2_A1430AuditId
               }
               , new Object[] {
               H00KO3_AGRID_nRecordCount
               }
            }
         );
         AV72Pgmname = "WWAudit";
         /* GeneXus formulas. */
         AV72Pgmname = "WWAudit";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_48 ;
      private short nGXsfl_48_idx=1 ;
      private short AV37OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_48_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtAuditDate_Titleformat ;
      private short edtAuditDescription_Titleformat ;
      private short edtUsuario_PessoaNom_Titleformat ;
      private short edtAuditAction_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A1Usuario_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_auditdescription_Datalistupdateminimumcharacters ;
      private int Ddo_usuario_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_auditaction_Datalistupdateminimumcharacters ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfauditdate_Visible ;
      private int edtavTfauditdate_to_Visible ;
      private int edtavTfauditdescription_Visible ;
      private int edtavTfauditdescription_sel_Visible ;
      private int edtavTfusuario_pessoanom_Visible ;
      private int edtavTfusuario_pessoanom_sel_Visible ;
      private int edtavTfauditaction_Visible ;
      private int edtavTfauditaction_sel_Visible ;
      private int edtavDdo_auditdatetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_auditdescriptiontitlecontrolidtoreplace_Visible ;
      private int edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_auditactiontitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A57Usuario_PessoaCod ;
      private int AV67PageToGo ;
      private int AV73GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV68GridCurrentPage ;
      private long AV69GridPageCount ;
      private long A1430AuditId ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_auditdate_Activeeventkey ;
      private String Ddo_auditdate_Filteredtext_get ;
      private String Ddo_auditdate_Filteredtextto_get ;
      private String Ddo_auditdescription_Activeeventkey ;
      private String Ddo_auditdescription_Filteredtext_get ;
      private String Ddo_auditdescription_Selectedvalue_get ;
      private String Ddo_usuario_pessoanom_Activeeventkey ;
      private String Ddo_usuario_pessoanom_Filteredtext_get ;
      private String Ddo_usuario_pessoanom_Selectedvalue_get ;
      private String Ddo_auditaction_Activeeventkey ;
      private String Ddo_auditaction_Filteredtext_get ;
      private String Ddo_auditaction_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_48_idx="0001" ;
      private String AV17AuditTableName ;
      private String AV5AuditAction ;
      private String AV43Usuario_PessoaNom ;
      private String AV59TFUsuario_PessoaNom ;
      private String AV60TFUsuario_PessoaNom_Sel ;
      private String AV63TFAuditAction ;
      private String AV64TFAuditAction_Sel ;
      private String AV72Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_auditdate_Caption ;
      private String Ddo_auditdate_Tooltip ;
      private String Ddo_auditdate_Cls ;
      private String Ddo_auditdate_Filteredtext_set ;
      private String Ddo_auditdate_Filteredtextto_set ;
      private String Ddo_auditdate_Dropdownoptionstype ;
      private String Ddo_auditdate_Titlecontrolidtoreplace ;
      private String Ddo_auditdate_Sortedstatus ;
      private String Ddo_auditdate_Filtertype ;
      private String Ddo_auditdate_Sortasc ;
      private String Ddo_auditdate_Sortdsc ;
      private String Ddo_auditdate_Cleanfilter ;
      private String Ddo_auditdate_Rangefilterfrom ;
      private String Ddo_auditdate_Rangefilterto ;
      private String Ddo_auditdate_Searchbuttontext ;
      private String Ddo_auditdescription_Caption ;
      private String Ddo_auditdescription_Tooltip ;
      private String Ddo_auditdescription_Cls ;
      private String Ddo_auditdescription_Filteredtext_set ;
      private String Ddo_auditdescription_Selectedvalue_set ;
      private String Ddo_auditdescription_Dropdownoptionstype ;
      private String Ddo_auditdescription_Titlecontrolidtoreplace ;
      private String Ddo_auditdescription_Sortedstatus ;
      private String Ddo_auditdescription_Filtertype ;
      private String Ddo_auditdescription_Datalisttype ;
      private String Ddo_auditdescription_Datalistproc ;
      private String Ddo_auditdescription_Sortasc ;
      private String Ddo_auditdescription_Sortdsc ;
      private String Ddo_auditdescription_Loadingdata ;
      private String Ddo_auditdescription_Cleanfilter ;
      private String Ddo_auditdescription_Noresultsfound ;
      private String Ddo_auditdescription_Searchbuttontext ;
      private String Ddo_usuario_pessoanom_Caption ;
      private String Ddo_usuario_pessoanom_Tooltip ;
      private String Ddo_usuario_pessoanom_Cls ;
      private String Ddo_usuario_pessoanom_Filteredtext_set ;
      private String Ddo_usuario_pessoanom_Selectedvalue_set ;
      private String Ddo_usuario_pessoanom_Dropdownoptionstype ;
      private String Ddo_usuario_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_usuario_pessoanom_Sortedstatus ;
      private String Ddo_usuario_pessoanom_Filtertype ;
      private String Ddo_usuario_pessoanom_Datalisttype ;
      private String Ddo_usuario_pessoanom_Datalistproc ;
      private String Ddo_usuario_pessoanom_Sortasc ;
      private String Ddo_usuario_pessoanom_Sortdsc ;
      private String Ddo_usuario_pessoanom_Loadingdata ;
      private String Ddo_usuario_pessoanom_Cleanfilter ;
      private String Ddo_usuario_pessoanom_Noresultsfound ;
      private String Ddo_usuario_pessoanom_Searchbuttontext ;
      private String Ddo_auditaction_Caption ;
      private String Ddo_auditaction_Tooltip ;
      private String Ddo_auditaction_Cls ;
      private String Ddo_auditaction_Filteredtext_set ;
      private String Ddo_auditaction_Selectedvalue_set ;
      private String Ddo_auditaction_Dropdownoptionstype ;
      private String Ddo_auditaction_Titlecontrolidtoreplace ;
      private String Ddo_auditaction_Sortedstatus ;
      private String Ddo_auditaction_Filtertype ;
      private String Ddo_auditaction_Datalisttype ;
      private String Ddo_auditaction_Datalistproc ;
      private String Ddo_auditaction_Sortasc ;
      private String Ddo_auditaction_Sortdsc ;
      private String Ddo_auditaction_Loadingdata ;
      private String Ddo_auditaction_Cleanfilter ;
      private String Ddo_auditaction_Noresultsfound ;
      private String Ddo_auditaction_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfauditdate_Internalname ;
      private String edtavTfauditdate_Jsonclick ;
      private String edtavTfauditdate_to_Internalname ;
      private String edtavTfauditdate_to_Jsonclick ;
      private String divDdo_auditdateauxdates_Internalname ;
      private String edtavDdo_auditdateauxdate_Internalname ;
      private String edtavDdo_auditdateauxdate_Jsonclick ;
      private String edtavDdo_auditdateauxdateto_Internalname ;
      private String edtavDdo_auditdateauxdateto_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTfauditdescription_Internalname ;
      private String edtavTfauditdescription_sel_Internalname ;
      private String edtavTfusuario_pessoanom_Internalname ;
      private String edtavTfusuario_pessoanom_Jsonclick ;
      private String edtavTfusuario_pessoanom_sel_Internalname ;
      private String edtavTfusuario_pessoanom_sel_Jsonclick ;
      private String edtavTfauditaction_Internalname ;
      private String edtavTfauditaction_Jsonclick ;
      private String edtavTfauditaction_sel_Internalname ;
      private String edtavTfauditaction_sel_Jsonclick ;
      private String edtavDdo_auditdatetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_auditdescriptiontitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_auditactiontitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtAuditId_Internalname ;
      private String edtAuditDate_Internalname ;
      private String A1432AuditTableName ;
      private String edtAuditTableName_Internalname ;
      private String edtAuditDescription_Internalname ;
      private String edtAuditShortDescription_Internalname ;
      private String edtUsuario_Codigo_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String A2Usuario_Nome ;
      private String edtUsuario_Nome_Internalname ;
      private String A1435AuditAction ;
      private String edtAuditAction_Internalname ;
      private String cmbavAudittablename_Internalname ;
      private String scmdbuf ;
      private String lV43Usuario_PessoaNom ;
      private String lV59TFUsuario_PessoaNom ;
      private String lV63TFAuditAction ;
      private String edtavAuditdate_Internalname ;
      private String edtavAuditdate_to_Internalname ;
      private String cmbavAuditaction_Internalname ;
      private String edtavUsuario_pessoanom_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_auditdate_Internalname ;
      private String Ddo_auditdescription_Internalname ;
      private String Ddo_usuario_pessoanom_Internalname ;
      private String Ddo_auditaction_Internalname ;
      private String edtAuditDate_Title ;
      private String edtAuditDescription_Title ;
      private String edtUsuario_PessoaNom_Title ;
      private String edtAuditAction_Title ;
      private String edtUsuario_PessoaNom_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblAudittitle_Internalname ;
      private String lblAudittitle_Jsonclick ;
      private String tblTeste_Internalname ;
      private String lblFiltertextaudittablename_Internalname ;
      private String lblFiltertextaudittablename_Jsonclick ;
      private String cmbavAudittablename_Jsonclick ;
      private String lblFiltertextauditdate_Internalname ;
      private String lblFiltertextauditdate_Jsonclick ;
      private String lblFiltertextauditaction_Internalname ;
      private String lblFiltertextauditaction_Jsonclick ;
      private String cmbavAuditaction_Jsonclick ;
      private String lblFiltertextusuario_pessoanom_Internalname ;
      private String lblFiltertextusuario_pessoanom_Jsonclick ;
      private String edtavUsuario_pessoanom_Jsonclick ;
      private String tblTablemergedauditdate_Internalname ;
      private String edtavAuditdate_Jsonclick ;
      private String lblAuditdate_rangemiddletext_Internalname ;
      private String lblAuditdate_rangemiddletext_Jsonclick ;
      private String edtavAuditdate_to_Jsonclick ;
      private String sGXsfl_48_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAuditId_Jsonclick ;
      private String edtAuditDate_Jsonclick ;
      private String edtAuditTableName_Jsonclick ;
      private String edtAuditDescription_Jsonclick ;
      private String edtAuditShortDescription_Jsonclick ;
      private String edtUsuario_Codigo_Jsonclick ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private String edtUsuario_Nome_Jsonclick ;
      private String edtAuditAction_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV9AuditDate ;
      private DateTime AV10AuditDate_To ;
      private DateTime AV49TFAuditDate ;
      private DateTime AV50TFAuditDate_To ;
      private DateTime A1431AuditDate ;
      private DateTime AV51DDO_AuditDateAuxDate ;
      private DateTime AV52DDO_AuditDateAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV38OrderedDsc ;
      private bool n1Usuario_Codigo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_auditdate_Includesortasc ;
      private bool Ddo_auditdate_Includesortdsc ;
      private bool Ddo_auditdate_Includefilter ;
      private bool Ddo_auditdate_Filterisrange ;
      private bool Ddo_auditdate_Includedatalist ;
      private bool Ddo_auditdescription_Includesortasc ;
      private bool Ddo_auditdescription_Includesortdsc ;
      private bool Ddo_auditdescription_Includefilter ;
      private bool Ddo_auditdescription_Filterisrange ;
      private bool Ddo_auditdescription_Includedatalist ;
      private bool Ddo_usuario_pessoanom_Includesortasc ;
      private bool Ddo_usuario_pessoanom_Includesortdsc ;
      private bool Ddo_usuario_pessoanom_Includefilter ;
      private bool Ddo_usuario_pessoanom_Filterisrange ;
      private bool Ddo_usuario_pessoanom_Includedatalist ;
      private bool Ddo_auditaction_Includesortasc ;
      private bool Ddo_auditaction_Includesortdsc ;
      private bool Ddo_auditaction_Includefilter ;
      private bool Ddo_auditaction_Filterisrange ;
      private bool Ddo_auditaction_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1431AuditDate ;
      private bool n1432AuditTableName ;
      private bool n1434AuditDescription ;
      private bool n1433AuditShortDescription ;
      private bool n58Usuario_PessoaNom ;
      private bool n2Usuario_Nome ;
      private bool n1435AuditAction ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String A1434AuditDescription ;
      private String AV55TFAuditDescription ;
      private String AV56TFAuditDescription_Sel ;
      private String AV53ddo_AuditDateTitleControlIdToReplace ;
      private String AV57ddo_AuditDescriptionTitleControlIdToReplace ;
      private String AV61ddo_Usuario_PessoaNomTitleControlIdToReplace ;
      private String AV65ddo_AuditActionTitleControlIdToReplace ;
      private String A1433AuditShortDescription ;
      private String lV55TFAuditDescription ;
      private IGxSession AV39Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavAudittablename ;
      private GXCombobox cmbavAuditaction ;
      private IDataStoreProvider pr_default ;
      private int[] H00KO2_A57Usuario_PessoaCod ;
      private String[] H00KO2_A1432AuditTableName ;
      private bool[] H00KO2_n1432AuditTableName ;
      private String[] H00KO2_A1435AuditAction ;
      private bool[] H00KO2_n1435AuditAction ;
      private String[] H00KO2_A2Usuario_Nome ;
      private bool[] H00KO2_n2Usuario_Nome ;
      private String[] H00KO2_A58Usuario_PessoaNom ;
      private bool[] H00KO2_n58Usuario_PessoaNom ;
      private int[] H00KO2_A1Usuario_Codigo ;
      private bool[] H00KO2_n1Usuario_Codigo ;
      private String[] H00KO2_A1433AuditShortDescription ;
      private bool[] H00KO2_n1433AuditShortDescription ;
      private String[] H00KO2_A1434AuditDescription ;
      private bool[] H00KO2_n1434AuditDescription ;
      private DateTime[] H00KO2_A1431AuditDate ;
      private bool[] H00KO2_n1431AuditDate ;
      private long[] H00KO2_A1430AuditId ;
      private long[] H00KO3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV35HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48AuditDateTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54AuditDescriptionTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58Usuario_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62AuditActionTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPGridState AV32GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPTransactionContext AV40TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV47WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV66DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwaudit__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00KO2( IGxContext context ,
                                             DateTime AV9AuditDate ,
                                             DateTime AV10AuditDate_To ,
                                             String AV5AuditAction ,
                                             String AV43Usuario_PessoaNom ,
                                             DateTime AV49TFAuditDate ,
                                             DateTime AV50TFAuditDate_To ,
                                             String AV56TFAuditDescription_Sel ,
                                             String AV55TFAuditDescription ,
                                             String AV60TFUsuario_PessoaNom_Sel ,
                                             String AV59TFUsuario_PessoaNom ,
                                             String AV64TFAuditAction_Sel ,
                                             String AV63TFAuditAction ,
                                             DateTime A1431AuditDate ,
                                             String A1435AuditAction ,
                                             String A58Usuario_PessoaNom ,
                                             String A1434AuditDescription ,
                                             short AV37OrderedBy ,
                                             bool AV38OrderedDsc ,
                                             String AV17AuditTableName ,
                                             String A1432AuditTableName )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [18] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[AuditTableName], T1.[AuditAction], T2.[Usuario_Nome], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Codigo], T1.[AuditShortDescription], T1.[AuditDescription], T1.[AuditDate], T1.[AuditId]";
         sFromString = " FROM (([Audit] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[AuditTableName] = @AV17AuditTableName)";
         if ( ! (DateTime.MinValue==AV9AuditDate) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] >= @AV9AuditDate)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV10AuditDate_To) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] <= @AV10AuditDate_To)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5AuditAction)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] = @AV5AuditAction)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Usuario_PessoaNom)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV43Usuario_PessoaNom)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV49TFAuditDate) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] >= @AV49TFAuditDate)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV50TFAuditDate_To) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] <= @AV50TFAuditDate_To)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56TFAuditDescription_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFAuditDescription)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDescription] like @lV55TFAuditDescription)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFAuditDescription_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDescription] = @AV56TFAuditDescription_Sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFUsuario_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV59TFUsuario_PessoaNom)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV60TFUsuario_PessoaNom_Sel)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64TFAuditAction_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFAuditAction)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] like @lV63TFAuditAction)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFAuditAction_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] = @AV64TFAuditAction_Sel)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ( AV37OrderedBy == 1 ) && ! AV38OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AuditDate]";
         }
         else if ( ( AV37OrderedBy == 1 ) && ( AV38OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AuditDate] DESC";
         }
         else if ( ( AV37OrderedBy == 2 ) && ! AV38OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AuditDescription]";
         }
         else if ( ( AV37OrderedBy == 2 ) && ( AV38OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AuditDescription] DESC";
         }
         else if ( ( AV37OrderedBy == 3 ) && ! AV38OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome]";
         }
         else if ( ( AV37OrderedBy == 3 ) && ( AV38OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome] DESC";
         }
         else if ( ( AV37OrderedBy == 4 ) && ! AV38OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AuditAction]";
         }
         else if ( ( AV37OrderedBy == 4 ) && ( AV38OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AuditAction] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AuditId]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00KO3( IGxContext context ,
                                             DateTime AV9AuditDate ,
                                             DateTime AV10AuditDate_To ,
                                             String AV5AuditAction ,
                                             String AV43Usuario_PessoaNom ,
                                             DateTime AV49TFAuditDate ,
                                             DateTime AV50TFAuditDate_To ,
                                             String AV56TFAuditDescription_Sel ,
                                             String AV55TFAuditDescription ,
                                             String AV60TFUsuario_PessoaNom_Sel ,
                                             String AV59TFUsuario_PessoaNom ,
                                             String AV64TFAuditAction_Sel ,
                                             String AV63TFAuditAction ,
                                             DateTime A1431AuditDate ,
                                             String A1435AuditAction ,
                                             String A58Usuario_PessoaNom ,
                                             String A1434AuditDescription ,
                                             short AV37OrderedBy ,
                                             bool AV38OrderedDsc ,
                                             String AV17AuditTableName ,
                                             String A1432AuditTableName )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [13] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([Audit] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[AuditTableName] = @AV17AuditTableName)";
         if ( ! (DateTime.MinValue==AV9AuditDate) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] >= @AV9AuditDate)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV10AuditDate_To) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] <= @AV10AuditDate_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5AuditAction)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] = @AV5AuditAction)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Usuario_PessoaNom)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV43Usuario_PessoaNom)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV49TFAuditDate) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] >= @AV49TFAuditDate)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV50TFAuditDate_To) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] <= @AV50TFAuditDate_To)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56TFAuditDescription_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFAuditDescription)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDescription] like @lV55TFAuditDescription)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFAuditDescription_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDescription] = @AV56TFAuditDescription_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFUsuario_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV59TFUsuario_PessoaNom)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV60TFUsuario_PessoaNom_Sel)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64TFAuditAction_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFAuditAction)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] like @lV63TFAuditAction)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFAuditAction_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] = @AV64TFAuditAction_Sel)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV37OrderedBy == 1 ) && ! AV38OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV37OrderedBy == 1 ) && ( AV38OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV37OrderedBy == 2 ) && ! AV38OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV37OrderedBy == 2 ) && ( AV38OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV37OrderedBy == 3 ) && ! AV38OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV37OrderedBy == 3 ) && ( AV38OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV37OrderedBy == 4 ) && ! AV38OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV37OrderedBy == 4 ) && ( AV38OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00KO2(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] );
               case 1 :
                     return conditional_H00KO3(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KO2 ;
          prmH00KO2 = new Object[] {
          new Object[] {"@AV17AuditTableName",SqlDbType.Char,50,0} ,
          new Object[] {"@AV9AuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV10AuditDate_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV5AuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@lV43Usuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV49TFAuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV50TFAuditDate_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV55TFAuditDescription",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV56TFAuditDescription_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV59TFUsuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV60TFUsuario_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV63TFAuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@AV64TFAuditAction_Sel",SqlDbType.Char,3,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00KO3 ;
          prmH00KO3 = new Object[] {
          new Object[] {"@AV17AuditTableName",SqlDbType.Char,50,0} ,
          new Object[] {"@AV9AuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV10AuditDate_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV5AuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@lV43Usuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV49TFAuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV50TFAuditDate_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV55TFAuditDescription",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV56TFAuditDescription_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV59TFUsuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV60TFUsuario_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV63TFAuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@AV64TFAuditAction_Sel",SqlDbType.Char,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KO2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KO2,11,0,true,false )
             ,new CursorDef("H00KO3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KO3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[15])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((long[]) buf[17])[0] = rslt.getLong(10) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

}
