/*
               File: WP_AssociarContrato_Sistema
        Description: Associar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 20:35:33.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associarcontrato_sistema : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associarcontrato_sistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associarcontrato_sistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoSistemas_CntCod )
      {
         this.AV11ContratoSistemas_CntCod = aP0_ContratoSistemas_CntCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         lstavNotassociatedrecords = new GXListbox();
         lstavAssociatedrecords = new GXListbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV11ContratoSistemas_CntCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoSistemas_CntCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSISTEMAS_CNTCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratoSistemas_CntCod), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAMY2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSMY2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEMY2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Associar") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205320353329");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associarcontrato_sistema.aspx") + "?" + UrlEncode("" +AV11ContratoSistemas_CntCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDKEYLIST", AV7AddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDKEYLIST", AV7AddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDDSCLIST", AV5AddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDDSCLIST", AV5AddedDscList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDKEYLIST", AV21NotAddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDKEYLIST", AV21NotAddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDDSCLIST", AV19NotAddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDDSCLIST", AV19NotAddedDscList);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATOSISTEMAS_CNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1725ContratoSistemas_CntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSISTEMAS_CNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11ContratoSistemas_CntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSISTEMAS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSISTEMAS", AV10ContratoSistemas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSISTEMAS", AV10ContratoSistemas);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMEROATA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMEROATA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMEROATA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSISTEMAS_CNTCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratoSistemas_CntCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSISTEMAS_CNTCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratoSistemas_CntCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormMY2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociarContrato_Sistema" ;
      }

      public override String GetPgmdesc( )
      {
         return "Associar" ;
      }

      protected void WBMY0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_MY2( true) ;
         }
         else
         {
            wb_table1_2_MY2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MY2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddedkeylistxml_Internalname, AV8AddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavAddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContrato_Sistema.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddedkeylistxml_Internalname, AV22NotAddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, edtavNotaddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContrato_Sistema.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddeddsclistxml_Internalname, AV6AddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", 0, edtavAddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContrato_Sistema.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddeddsclistxml_Internalname, AV20NotAddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, edtavNotaddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContrato_Sistema.htm");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_AssociarContrato_Sistema.htm");
         }
         wbLoad = true;
      }

      protected void STARTMY2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Associar", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMY0( ) ;
      }

      protected void WSMY2( )
      {
         STARTMY2( ) ;
         EVTMY2( ) ;
      }

      protected void EVTMY2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11MY2 */
                           E11MY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12MY2 */
                           E12MY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13MY2 */
                                 E13MY2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14MY2 */
                           E14MY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15MY2 */
                           E15MY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16MY2 */
                           E16MY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17MY2 */
                           E17MY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18MY2 */
                           E18MY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19MY2 */
                           E19MY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20MY2 */
                           E20MY2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18MY2 */
                           E18MY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19MY2 */
                           E19MY2 ();
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEMY2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormMY2( ) ;
            }
         }
      }

      protected void PAMY2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            lstavNotassociatedrecords.Name = "vNOTASSOCIATEDRECORDS";
            lstavNotassociatedrecords.WebTags = "";
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Nenhum", 0);
            if ( lstavNotassociatedrecords.ItemCount > 0 )
            {
               AV23NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0)));
            }
            lstavAssociatedrecords.Name = "vASSOCIATEDRECORDS";
            lstavAssociatedrecords.WebTags = "";
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Nenhum", 0);
            if ( lstavAssociatedrecords.ItemCount > 0 )
            {
               AV9AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = lstavNotassociatedrecords_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( lstavNotassociatedrecords.ItemCount > 0 )
         {
            AV23NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0)));
         }
         if ( lstavAssociatedrecords.ItemCount > 0 )
         {
            AV9AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMY2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFMY2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12MY2 */
         E12MY2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00MY2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A78Contrato_NumeroAta = H00MY2_A78Contrato_NumeroAta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATO_NUMEROATA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, ""))));
               n78Contrato_NumeroAta = H00MY2_n78Contrato_NumeroAta[0];
               /* Execute user event: E20MY2 */
               E20MY2 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            WBMY0( ) ;
         }
      }

      protected void STRUPMY0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11MY2 */
         E11MY2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
            n78Contrato_NumeroAta = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATO_NUMEROATA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, ""))));
            lstavNotassociatedrecords.CurrentValue = cgiGet( lstavNotassociatedrecords_Internalname);
            AV23NotAssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavNotassociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0)));
            lstavAssociatedrecords.CurrentValue = cgiGet( lstavAssociatedrecords_Internalname);
            AV9AssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavAssociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)));
            AV8AddedKeyListXml = cgiGet( edtavAddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV22NotAddedKeyListXml = cgiGet( edtavNotaddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NotAddedKeyListXml", AV22NotAddedKeyListXml);
            AV6AddedDscListXml = cgiGet( edtavAddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
            AV20NotAddedDscListXml = cgiGet( edtavNotaddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20NotAddedDscListXml", AV20NotAddedDscListXml);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11MY2 */
         E11MY2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11MY2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV27WWPContext) ;
         if ( StringUtil.StrCmp(AV14HTTPRequest.Method, "GET") == 0 )
         {
            AV31GXLvl6 = 0;
            /* Using cursor H00MY3 */
            pr_default.execute(1, new Object[] {AV11ContratoSistemas_CntCod});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A74Contrato_Codigo = H00MY3_A74Contrato_Codigo[0];
               A75Contrato_AreaTrabalhoCod = H00MY3_A75Contrato_AreaTrabalhoCod[0];
               A92Contrato_Ativo = H00MY3_A92Contrato_Ativo[0];
               AV31GXLvl6 = 1;
               AV28AreaTrabalho = A75Contrato_AreaTrabalhoCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28AreaTrabalho), 6, 0)));
               lblTbinativo_Visible = (!A92Contrato_Ativo ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbinativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbinativo_Visible), 5, 0)));
               bttBtn_confirm_Visible = (A92Contrato_Ativo&&(AV27WWPContext.gxTpr_Insert||AV27WWPContext.gxTpr_Update) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            if ( AV31GXLvl6 == 0 )
            {
               GX_msglist.addItem("Contrato n�o encontrado.");
            }
            /* Using cursor H00MY4 */
            pr_default.execute(2, new Object[] {AV11ContratoSistemas_CntCod});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1725ContratoSistemas_CntCod = H00MY4_A1725ContratoSistemas_CntCod[0];
               A1726ContratoSistemas_SistemaCod = H00MY4_A1726ContratoSistemas_SistemaCod[0];
               AV25SistemasAtendidos.Add(A1726ContratoSistemas_SistemaCod, 0);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 A127Sistema_Codigo ,
                                                 AV25SistemasAtendidos ,
                                                 A130Sistema_Ativo ,
                                                 A135Sistema_AreaTrabalhoCod ,
                                                 AV28AreaTrabalho ,
                                                 A699Sistema_Tipo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00MY5 */
            pr_default.execute(3, new Object[] {AV28AreaTrabalho});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A135Sistema_AreaTrabalhoCod = H00MY5_A135Sistema_AreaTrabalhoCod[0];
               A699Sistema_Tipo = H00MY5_A699Sistema_Tipo[0];
               n699Sistema_Tipo = H00MY5_n699Sistema_Tipo[0];
               A127Sistema_Codigo = H00MY5_A127Sistema_Codigo[0];
               A130Sistema_Ativo = H00MY5_A130Sistema_Ativo[0];
               A129Sistema_Sigla = H00MY5_A129Sistema_Sigla[0];
               AV13Exist = false;
               /* Using cursor H00MY6 */
               pr_default.execute(4, new Object[] {AV11ContratoSistemas_CntCod, A127Sistema_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A1725ContratoSistemas_CntCod = H00MY6_A1725ContratoSistemas_CntCod[0];
                  AV13Exist = true;
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               AV12Description = A129Sistema_Sigla;
               if ( AV13Exist )
               {
                  AV7AddedKeyList.Add(A127Sistema_Codigo, 0);
                  AV5AddedDscList.Add(AV12Description, 0);
               }
               else
               {
                  AV21NotAddedKeyList.Add(A127Sistema_Codigo, 0);
                  AV19NotAddedDscList.Add(AV12Description, 0);
               }
               pr_default.readNext(3);
            }
            pr_default.close(3);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavAddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddedkeylistxml_Visible), 5, 0)));
         edtavNotaddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddedkeylistxml_Visible), 5, 0)));
         edtavAddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddeddsclistxml_Visible), 5, 0)));
         edtavNotaddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddeddsclistxml_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void E12MY2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV27WWPContext) ;
         imgImageassociateselected_Visible = (AV27WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV27WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV27WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV27WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19NotAddedDscList", AV19NotAddedDscList);
      }

      public void GXEnter( )
      {
         /* Execute user event: E13MY2 */
         E13MY2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13MY2( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV15i = 1;
         AV26Success = true;
         AV35GXV1 = 1;
         while ( AV35GXV1 <= AV7AddedKeyList.Count )
         {
            AV24Sistema_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV35GXV1));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Sistema_Codigo), 6, 0)));
            if ( AV26Success )
            {
               AV13Exist = false;
               /* Using cursor H00MY7 */
               pr_default.execute(5, new Object[] {AV11ContratoSistemas_CntCod, AV24Sistema_Codigo});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A1726ContratoSistemas_SistemaCod = H00MY7_A1726ContratoSistemas_SistemaCod[0];
                  A1725ContratoSistemas_CntCod = H00MY7_A1725ContratoSistemas_CntCod[0];
                  AV13Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(5);
               if ( ! AV13Exist )
               {
                  AV10ContratoSistemas = new SdtContratoSistemas(context);
                  AV10ContratoSistemas.gxTpr_Contratosistemas_cntcod = AV11ContratoSistemas_CntCod;
                  AV10ContratoSistemas.gxTpr_Contratosistemas_sistemacod = AV24Sistema_Codigo;
                  AV10ContratoSistemas.Save();
                  if ( ! AV10ContratoSistemas.Success() )
                  {
                     AV26Success = false;
                  }
               }
            }
            AV15i = (int)(AV15i+1);
            AV35GXV1 = (int)(AV35GXV1+1);
         }
         AV15i = 1;
         AV37GXV2 = 1;
         while ( AV37GXV2 <= AV21NotAddedKeyList.Count )
         {
            AV24Sistema_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV37GXV2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Sistema_Codigo), 6, 0)));
            if ( AV26Success )
            {
               AV13Exist = false;
               /* Using cursor H00MY8 */
               pr_default.execute(6, new Object[] {AV11ContratoSistemas_CntCod, AV24Sistema_Codigo});
               while ( (pr_default.getStatus(6) != 101) )
               {
                  A1726ContratoSistemas_SistemaCod = H00MY8_A1726ContratoSistemas_SistemaCod[0];
                  A1725ContratoSistemas_CntCod = H00MY8_A1725ContratoSistemas_CntCod[0];
                  AV13Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(6);
               if ( AV13Exist )
               {
                  AV10ContratoSistemas = new SdtContratoSistemas(context);
                  AV10ContratoSistemas.Load(AV11ContratoSistemas_CntCod, AV24Sistema_Codigo);
                  if ( AV10ContratoSistemas.Success() )
                  {
                     AV10ContratoSistemas.Delete();
                  }
                  if ( ! AV10ContratoSistemas.Success() )
                  {
                     AV26Success = false;
                  }
               }
            }
            AV15i = (int)(AV15i+1);
            AV37GXV2 = (int)(AV37GXV2+1);
         }
         if ( AV26Success )
         {
            context.CommitDataStores( "WP_AssociarContrato_Sistema");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10ContratoSistemas", AV10ContratoSistemas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19NotAddedDscList", AV19NotAddedDscList);
      }

      protected void E14MY2( )
      {
         /* 'Disassociate Selected' Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19NotAddedDscList", AV19NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E15MY2( )
      {
         /* 'Associate selected' Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19NotAddedDscList", AV19NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E16MY2( )
      {
         /* 'Associate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19NotAddedDscList", AV19NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E17MY2( )
      {
         /* 'Disassociate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21NotAddedKeyList = (IGxCollection)(AV7AddedKeyList.Clone());
         AV19NotAddedDscList = (IGxCollection)(AV5AddedDscList.Clone());
         AV5AddedDscList.Clear();
         AV7AddedKeyList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19NotAddedDscList", AV19NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E18MY2( )
      {
         /* Associatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19NotAddedDscList", AV19NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E19MY2( )
      {
         /* Notassociatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21NotAddedKeyList", AV21NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19NotAddedDscList", AV19NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void S122( )
      {
         /* 'UPDATEASSOCIATIONVARIABLES' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         lstavAssociatedrecords.removeAllItems();
         lstavNotassociatedrecords.removeAllItems();
         AV15i = 1;
         AV39GXV3 = 1;
         while ( AV39GXV3 <= AV7AddedKeyList.Count )
         {
            AV24Sistema_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV39GXV3));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Sistema_Codigo), 6, 0)));
            AV12Description = ((String)AV5AddedDscList.Item(AV15i));
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV24Sistema_Codigo), 6, 0)), StringUtil.Trim( AV12Description), 0);
            AV15i = (int)(AV15i+1);
            AV39GXV3 = (int)(AV39GXV3+1);
         }
         AV15i = 1;
         AV40GXV4 = 1;
         while ( AV40GXV4 <= AV21NotAddedKeyList.Count )
         {
            AV24Sistema_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV40GXV4));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Sistema_Codigo), 6, 0)));
            AV12Description = ((String)AV19NotAddedDscList.Item(AV15i));
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV24Sistema_Codigo), 6, 0)), StringUtil.Trim( AV12Description), 0);
            AV15i = (int)(AV15i+1);
            AV40GXV4 = (int)(AV40GXV4+1);
         }
      }

      protected void S142( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV42GXV6 = 1;
         AV41GXV5 = AV10ContratoSistemas.GetMessages();
         while ( AV42GXV6 <= AV41GXV5.Count )
         {
            AV18Message = ((SdtMessages_Message)AV41GXV5.Item(AV42GXV6));
            if ( AV18Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV18Message.gxTpr_Description);
            }
            AV42GXV6 = (int)(AV42GXV6+1);
         }
      }

      protected void S132( )
      {
         /* 'LOADLISTS' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8AddedKeyListXml)) )
         {
            AV5AddedDscList.FromXml(AV6AddedDscListXml, "Collection");
            AV7AddedKeyList.FromXml(AV8AddedKeyListXml, "Collection");
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22NotAddedKeyListXml)) )
         {
            AV21NotAddedKeyList.FromXml(AV22NotAddedKeyListXml, "Collection");
            AV19NotAddedDscList.FromXml(AV20NotAddedDscListXml, "Collection");
         }
      }

      protected void S112( )
      {
         /* 'SAVELISTS' Routine */
         if ( AV7AddedKeyList.Count > 0 )
         {
            AV8AddedKeyListXml = AV7AddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV6AddedDscListXml = AV5AddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
         }
         else
         {
            AV8AddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV6AddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
         }
         if ( AV21NotAddedKeyList.Count > 0 )
         {
            AV22NotAddedKeyListXml = AV21NotAddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NotAddedKeyListXml", AV22NotAddedKeyListXml);
            AV20NotAddedDscListXml = AV19NotAddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20NotAddedDscListXml", AV20NotAddedDscListXml);
         }
         else
         {
            AV22NotAddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22NotAddedKeyListXml", AV22NotAddedKeyListXml);
            AV20NotAddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20NotAddedDscListXml", AV20NotAddedDscListXml);
         }
      }

      protected void S172( )
      {
         /* 'ASSOCIATEALL' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV15i = 1;
         AV16InsertIndex = 1;
         AV43GXV7 = 1;
         while ( AV43GXV7 <= AV21NotAddedKeyList.Count )
         {
            AV24Sistema_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV43GXV7));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Sistema_Codigo), 6, 0)));
            AV12Description = ((String)AV19NotAddedDscList.Item(AV15i));
            while ( ( AV16InsertIndex <= AV5AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV5AddedDscList.Item(AV16InsertIndex)), AV12Description) < 0 ) )
            {
               AV16InsertIndex = (int)(AV16InsertIndex+1);
            }
            AV7AddedKeyList.Add(AV24Sistema_Codigo, AV16InsertIndex);
            AV5AddedDscList.Add(AV12Description, AV16InsertIndex);
            AV15i = (int)(AV15i+1);
            AV43GXV7 = (int)(AV43GXV7+1);
         }
         AV21NotAddedKeyList.Clear();
         AV19NotAddedDscList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV15i = 1;
         AV44GXV8 = 1;
         while ( AV44GXV8 <= AV21NotAddedKeyList.Count )
         {
            AV24Sistema_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV44GXV8));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Sistema_Codigo), 6, 0)));
            if ( AV24Sistema_Codigo == AV23NotAssociatedRecords )
            {
               if (true) break;
            }
            AV15i = (int)(AV15i+1);
            AV44GXV8 = (int)(AV44GXV8+1);
         }
         if ( AV15i <= AV21NotAddedKeyList.Count )
         {
            AV12Description = ((String)AV19NotAddedDscList.Item(AV15i));
            AV16InsertIndex = 1;
            while ( ( AV16InsertIndex <= AV5AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV5AddedDscList.Item(AV16InsertIndex)), AV12Description) < 0 ) )
            {
               AV16InsertIndex = (int)(AV16InsertIndex+1);
            }
            AV7AddedKeyList.Add(AV23NotAssociatedRecords, AV16InsertIndex);
            AV5AddedDscList.Add(AV12Description, AV16InsertIndex);
            AV21NotAddedKeyList.RemoveItem(AV15i);
            AV19NotAddedDscList.RemoveItem(AV15i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV15i = 1;
         AV45GXV9 = 1;
         while ( AV45GXV9 <= AV7AddedKeyList.Count )
         {
            AV24Sistema_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV45GXV9));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Sistema_Codigo), 6, 0)));
            if ( AV24Sistema_Codigo == AV9AssociatedRecords )
            {
               if (true) break;
            }
            AV15i = (int)(AV15i+1);
            AV45GXV9 = (int)(AV45GXV9+1);
         }
         if ( AV15i <= AV7AddedKeyList.Count )
         {
            AV12Description = ((String)AV5AddedDscList.Item(AV15i));
            AV16InsertIndex = 1;
            while ( ( AV16InsertIndex <= AV19NotAddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV19NotAddedDscList.Item(AV16InsertIndex)), AV12Description) < 0 ) )
            {
               AV16InsertIndex = (int)(AV16InsertIndex+1);
            }
            AV21NotAddedKeyList.Add(AV9AssociatedRecords, AV16InsertIndex);
            AV19NotAddedDscList.Add(AV12Description, AV16InsertIndex);
            AV7AddedKeyList.RemoveItem(AV15i);
            AV5AddedDscList.RemoveItem(AV15i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E20MY2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_MY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_MY2( true) ;
         }
         else
         {
            wb_table2_8_MY2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_MY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_18_MY2( true) ;
         }
         else
         {
            wb_table3_18_MY2( false) ;
         }
         return  ;
      }

      protected void wb_table3_18_MY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_50_MY2( true) ;
         }
         else
         {
            wb_table4_50_MY2( false) ;
         }
         return  ;
      }

      protected void wb_table4_50_MY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MY2e( true) ;
         }
         else
         {
            wb_table1_2_MY2e( false) ;
         }
      }

      protected void wb_table4_50_MY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_50_MY2e( true) ;
         }
         else
         {
            wb_table4_50_MY2e( false) ;
         }
      }

      protected void wb_table3_18_MY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_21_MY2( true) ;
         }
         else
         {
            wb_table5_21_MY2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_MY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_18_MY2e( true) ;
         }
         else
         {
            wb_table3_18_MY2e( false) ;
         }
      }

      protected void wb_table5_21_MY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, "Registros N�o Associados", "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, "Registros Associados", "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavNotassociatedrecords, lstavNotassociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0)), 2, lstavNotassociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "px", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_WP_AssociarContrato_Sistema.htm");
            lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23NotAssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", (String)(lstavNotassociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_33_MY2( true) ;
         }
         else
         {
            wb_table6_33_MY2( false) ;
         }
         return  ;
      }

      protected void wb_table6_33_MY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavAssociatedrecords, lstavAssociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)), 2, lstavAssociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "px", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_WP_AssociarContrato_Sistema.htm");
            lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", (String)(lstavAssociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_MY2e( true) ;
         }
         else
         {
            wb_table5_21_MY2e( false) ;
         }
      }

      protected void wb_table6_33_MY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtableassociationbuttons_Internalname, tblUnnamedtableassociationbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_33_MY2e( true) ;
         }
         else
         {
            wb_table6_33_MY2e( false) ;
         }
      }

      protected void wb_table2_8_MY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Sistemas ao Contrato :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbinativo_Internalname, " (Inativo)", "", "", lblTbinativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", lblTbinativo_Visible, 1, 0, "HLP_WP_AssociarContrato_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_MY2e( true) ;
         }
         else
         {
            wb_table2_8_MY2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV11ContratoSistemas_CntCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoSistemas_CntCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSISTEMAS_CNTCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratoSistemas_CntCod), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMY2( ) ;
         WSMY2( ) ;
         WEMY2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020532035340");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associarcontrato_sistema.js", "?2020532035340");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA";
         lblTbinativo_Internalname = "TBINATIVO";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         lstavNotassociatedrecords_Internalname = "vNOTASSOCIATEDRECORDS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblUnnamedtableassociationbuttons_Internalname = "UNNAMEDTABLEASSOCIATIONBUTTONS";
         lstavAssociatedrecords_Internalname = "vASSOCIATEDRECORDS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavAddedkeylistxml_Internalname = "vADDEDKEYLISTXML";
         edtavNotaddedkeylistxml_Internalname = "vNOTADDEDKEYLISTXML";
         edtavAddeddsclistxml_Internalname = "vADDEDDSCLISTXML";
         edtavNotaddeddsclistxml_Internalname = "vNOTADDEDDSCLISTXML";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         lblTbinativo_Visible = 1;
         edtContrato_NumeroAta_Jsonclick = "";
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         lstavAssociatedrecords_Jsonclick = "";
         lstavNotassociatedrecords_Jsonclick = "";
         bttBtn_confirm_Visible = 1;
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         edtavNotaddeddsclistxml_Visible = 1;
         edtavAddeddsclistxml_Visible = 1;
         edtavNotaddedkeylistxml_Visible = 1;
         edtavAddedkeylistxml_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'imgImageassociateselected_Visible',ctrl:'IMAGEASSOCIATESELECTED',prop:'Visible'},{av:'imgImageassociateall_Visible',ctrl:'IMAGEASSOCIATEALL',prop:'Visible'},{av:'imgImagedisassociateselected_Visible',ctrl:'IMAGEDISASSOCIATESELECTED',prop:'Visible'},{av:'imgImagedisassociateall_Visible',ctrl:'IMAGEDISASSOCIATEALL',prop:'Visible'},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV23NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("ENTER","{handler:'E13MY2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'A1725ContratoSistemas_CntCod',fld:'CONTRATOSISTEMAS_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV11ContratoSistemas_CntCod',fld:'vCONTRATOSISTEMAS_CNTCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1726ContratoSistemas_SistemaCod',fld:'CONTRATOSISTEMAS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV10ContratoSistemas',fld:'vCONTRATOSISTEMAS',pic:'',nv:null}],oparms:[{av:'AV24Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV10ContratoSistemas',fld:'vCONTRATOSISTEMAS',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E14MY2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV24Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV23NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E15MY2',iparms:[{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV24Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV23NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E16MY2',iparms:[{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV24Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV23NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E17MY2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV24Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV23NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VASSOCIATEDRECORDS.DBLCLICK","{handler:'E18MY2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV24Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV23NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VNOTASSOCIATEDRECORDS.DBLCLICK","{handler:'E19MY2',iparms:[{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV24Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV21NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV19NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV22NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV20NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV23NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV7AddedKeyList = new GxSimpleCollection();
         AV5AddedDscList = new GxSimpleCollection();
         AV21NotAddedKeyList = new GxSimpleCollection();
         AV19NotAddedDscList = new GxSimpleCollection();
         AV10ContratoSistemas = new SdtContratoSistemas(context);
         A78Contrato_NumeroAta = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV8AddedKeyListXml = "";
         AV22NotAddedKeyListXml = "";
         AV6AddedDscListXml = "";
         AV20NotAddedDscListXml = "";
         lblTbjava_Jsonclick = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00MY2_A74Contrato_Codigo = new int[1] ;
         H00MY2_A78Contrato_NumeroAta = new String[] {""} ;
         H00MY2_n78Contrato_NumeroAta = new bool[] {false} ;
         AV27WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV14HTTPRequest = new GxHttpRequest( context);
         H00MY3_A74Contrato_Codigo = new int[1] ;
         H00MY3_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00MY3_A92Contrato_Ativo = new bool[] {false} ;
         H00MY4_A1725ContratoSistemas_CntCod = new int[1] ;
         H00MY4_A1726ContratoSistemas_SistemaCod = new int[1] ;
         AV25SistemasAtendidos = new GxSimpleCollection();
         A699Sistema_Tipo = "";
         H00MY5_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00MY5_A699Sistema_Tipo = new String[] {""} ;
         H00MY5_n699Sistema_Tipo = new bool[] {false} ;
         H00MY5_A127Sistema_Codigo = new int[1] ;
         H00MY5_A130Sistema_Ativo = new bool[] {false} ;
         H00MY5_A129Sistema_Sigla = new String[] {""} ;
         A129Sistema_Sigla = "";
         H00MY6_A1726ContratoSistemas_SistemaCod = new int[1] ;
         H00MY6_A1725ContratoSistemas_CntCod = new int[1] ;
         AV12Description = "";
         H00MY7_A1726ContratoSistemas_SistemaCod = new int[1] ;
         H00MY7_A1725ContratoSistemas_CntCod = new int[1] ;
         H00MY8_A1726ContratoSistemas_SistemaCod = new int[1] ;
         H00MY8_A1725ContratoSistemas_CntCod = new int[1] ;
         AV41GXV5 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV18Message = new SdtMessages_Message(context);
         sStyleString = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         lblTbinativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associarcontrato_sistema__default(),
            new Object[][] {
                new Object[] {
               H00MY2_A74Contrato_Codigo, H00MY2_A78Contrato_NumeroAta, H00MY2_n78Contrato_NumeroAta
               }
               , new Object[] {
               H00MY3_A74Contrato_Codigo, H00MY3_A75Contrato_AreaTrabalhoCod, H00MY3_A92Contrato_Ativo
               }
               , new Object[] {
               H00MY4_A1725ContratoSistemas_CntCod, H00MY4_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               H00MY5_A135Sistema_AreaTrabalhoCod, H00MY5_A699Sistema_Tipo, H00MY5_n699Sistema_Tipo, H00MY5_A127Sistema_Codigo, H00MY5_A130Sistema_Ativo, H00MY5_A129Sistema_Sigla
               }
               , new Object[] {
               H00MY6_A1726ContratoSistemas_SistemaCod, H00MY6_A1725ContratoSistemas_CntCod
               }
               , new Object[] {
               H00MY7_A1726ContratoSistemas_SistemaCod, H00MY7_A1725ContratoSistemas_CntCod
               }
               , new Object[] {
               H00MY8_A1726ContratoSistemas_SistemaCod, H00MY8_A1725ContratoSistemas_CntCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV31GXLvl6 ;
      private short nGXWrapped ;
      private int AV11ContratoSistemas_CntCod ;
      private int wcpOAV11ContratoSistemas_CntCod ;
      private int A1725ContratoSistemas_CntCod ;
      private int A1726ContratoSistemas_SistemaCod ;
      private int edtavAddedkeylistxml_Visible ;
      private int edtavNotaddedkeylistxml_Visible ;
      private int edtavAddeddsclistxml_Visible ;
      private int edtavNotaddeddsclistxml_Visible ;
      private int lblTbjava_Visible ;
      private int AV23NotAssociatedRecords ;
      private int AV9AssociatedRecords ;
      private int A74Contrato_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int AV28AreaTrabalho ;
      private int lblTbinativo_Visible ;
      private int bttBtn_confirm_Visible ;
      private int A127Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int AV15i ;
      private int AV35GXV1 ;
      private int AV24Sistema_Codigo ;
      private int AV37GXV2 ;
      private int AV39GXV3 ;
      private int AV40GXV4 ;
      private int AV42GXV6 ;
      private int AV16InsertIndex ;
      private int AV43GXV7 ;
      private int AV44GXV8 ;
      private int AV45GXV9 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A78Contrato_NumeroAta ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String edtavAddedkeylistxml_Internalname ;
      private String edtavNotaddedkeylistxml_Internalname ;
      private String edtavAddeddsclistxml_Internalname ;
      private String edtavNotaddeddsclistxml_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String lstavNotassociatedrecords_Internalname ;
      private String scmdbuf ;
      private String edtContrato_NumeroAta_Internalname ;
      private String lstavAssociatedrecords_Internalname ;
      private String lblTbinativo_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String A699Sistema_Tipo ;
      private String A129Sistema_Sigla ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String lstavNotassociatedrecords_Jsonclick ;
      private String lstavAssociatedrecords_Jsonclick ;
      private String tblUnnamedtableassociationbuttons_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTbinativo_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n78Contrato_NumeroAta ;
      private bool returnInSub ;
      private bool A92Contrato_Ativo ;
      private bool A130Sistema_Ativo ;
      private bool n699Sistema_Tipo ;
      private bool AV13Exist ;
      private bool AV26Success ;
      private String AV8AddedKeyListXml ;
      private String AV22NotAddedKeyListXml ;
      private String AV6AddedDscListXml ;
      private String AV20NotAddedDscListXml ;
      private String AV12Description ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXListbox lstavNotassociatedrecords ;
      private GXListbox lstavAssociatedrecords ;
      private IDataStoreProvider pr_default ;
      private int[] H00MY2_A74Contrato_Codigo ;
      private String[] H00MY2_A78Contrato_NumeroAta ;
      private bool[] H00MY2_n78Contrato_NumeroAta ;
      private int[] H00MY3_A74Contrato_Codigo ;
      private int[] H00MY3_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00MY3_A92Contrato_Ativo ;
      private int[] H00MY4_A1725ContratoSistemas_CntCod ;
      private int[] H00MY4_A1726ContratoSistemas_SistemaCod ;
      private int[] H00MY5_A135Sistema_AreaTrabalhoCod ;
      private String[] H00MY5_A699Sistema_Tipo ;
      private bool[] H00MY5_n699Sistema_Tipo ;
      private int[] H00MY5_A127Sistema_Codigo ;
      private bool[] H00MY5_A130Sistema_Ativo ;
      private String[] H00MY5_A129Sistema_Sigla ;
      private int[] H00MY6_A1726ContratoSistemas_SistemaCod ;
      private int[] H00MY6_A1725ContratoSistemas_CntCod ;
      private int[] H00MY7_A1726ContratoSistemas_SistemaCod ;
      private int[] H00MY7_A1725ContratoSistemas_CntCod ;
      private int[] H00MY8_A1726ContratoSistemas_SistemaCod ;
      private int[] H00MY8_A1725ContratoSistemas_CntCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV14HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV7AddedKeyList ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV21NotAddedKeyList ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV25SistemasAtendidos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV5AddedDscList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19NotAddedDscList ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV41GXV5 ;
      private SdtContratoSistemas AV10ContratoSistemas ;
      private SdtMessages_Message AV18Message ;
      private wwpbaseobjects.SdtWWPContext AV27WWPContext ;
   }

   public class wp_associarcontrato_sistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00MY5( IGxContext context ,
                                             int A127Sistema_Codigo ,
                                             IGxCollection AV25SistemasAtendidos ,
                                             bool A130Sistema_Ativo ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             int AV28AreaTrabalho ,
                                             String A699Sistema_Tipo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [1] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Sistema_AreaTrabalhoCod], [Sistema_Tipo], [Sistema_Codigo], [Sistema_Ativo], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Sistema_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25SistemasAtendidos, "[Sistema_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and ([Sistema_AreaTrabalhoCod] = @AV28AreaTrabalho)";
         scmdbuf = scmdbuf + " and ([Sistema_Tipo] = 'A')";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Tipo], [Sistema_Ativo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_H00MY5(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MY2 ;
          prmH00MY2 = new Object[] {
          } ;
          Object[] prmH00MY3 ;
          prmH00MY3 = new Object[] {
          new Object[] {"@AV11ContratoSistemas_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MY4 ;
          prmH00MY4 = new Object[] {
          new Object[] {"@AV11ContratoSistemas_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MY6 ;
          prmH00MY6 = new Object[] {
          new Object[] {"@AV11ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MY7 ;
          prmH00MY7 = new Object[] {
          new Object[] {"@AV11ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MY8 ;
          prmH00MY8 = new Object[] {
          new Object[] {"@AV11ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MY5 ;
          prmH00MY5 = new Object[] {
          new Object[] {"@AV28AreaTrabalho",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MY2", "SELECT [Contrato_Codigo], [Contrato_NumeroAta] FROM [Contrato] WITH (NOLOCK) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MY2,100,0,true,false )
             ,new CursorDef("H00MY3", "SELECT TOP 1 [Contrato_Codigo], [Contrato_AreaTrabalhoCod], [Contrato_Ativo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV11ContratoSistemas_CntCod ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MY3,1,0,false,true )
             ,new CursorDef("H00MY4", "SELECT [ContratoSistemas_CntCod], [ContratoSistemas_SistemaCod] FROM [ContratoSistemas] WITH (NOLOCK) WHERE [ContratoSistemas_CntCod] <> @AV11ContratoSistemas_CntCod ORDER BY [ContratoSistemas_CntCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MY4,100,0,false,false )
             ,new CursorDef("H00MY5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MY5,100,0,true,false )
             ,new CursorDef("H00MY6", "SELECT [ContratoSistemas_SistemaCod], [ContratoSistemas_CntCod] FROM [ContratoSistemas] WITH (NOLOCK) WHERE ([ContratoSistemas_CntCod] = @AV11ContratoSistemas_CntCod) AND (@Sistema_Codigo = @Sistema_Codigo) ORDER BY [ContratoSistemas_CntCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MY6,100,0,false,false )
             ,new CursorDef("H00MY7", "SELECT [ContratoSistemas_SistemaCod], [ContratoSistemas_CntCod] FROM [ContratoSistemas] WITH (NOLOCK) WHERE [ContratoSistemas_CntCod] = @AV11ContratoSistemas_CntCod and [ContratoSistemas_SistemaCod] = @AV24Sistema_Codigo ORDER BY [ContratoSistemas_CntCod], [ContratoSistemas_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MY7,1,0,false,true )
             ,new CursorDef("H00MY8", "SELECT [ContratoSistemas_SistemaCod], [ContratoSistemas_CntCod] FROM [ContratoSistemas] WITH (NOLOCK) WHERE [ContratoSistemas_CntCod] = @AV11ContratoSistemas_CntCod and [ContratoSistemas_SistemaCod] = @AV24Sistema_Codigo ORDER BY [ContratoSistemas_CntCod], [ContratoSistemas_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MY8,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 25) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
