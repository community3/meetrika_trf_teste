/*
               File: PromptReferenciaTecnica
        Description: Selecione Refer�ncia T�cnica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:14:11.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptreferenciatecnica : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptreferenciatecnica( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptreferenciatecnica( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutReferenciaTecnica_Codigo ,
                           ref String aP1_InOutReferenciaTecnica_Nome )
      {
         this.AV7InOutReferenciaTecnica_Codigo = aP0_InOutReferenciaTecnica_Codigo;
         this.AV8InOutReferenciaTecnica_Nome = aP1_InOutReferenciaTecnica_Nome;
         executePrivate();
         aP0_InOutReferenciaTecnica_Codigo=this.AV7InOutReferenciaTecnica_Codigo;
         aP1_InOutReferenciaTecnica_Nome=this.AV8InOutReferenciaTecnica_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbReferenciaTecnica_Unidade = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_83 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_83_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_83_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ReferenciaTecnica_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaTecnica_Nome1", AV17ReferenciaTecnica_Nome1);
               AV18Guia_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Guia_Nome1", AV18Guia_Nome1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV22ReferenciaTecnica_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ReferenciaTecnica_Nome2", AV22ReferenciaTecnica_Nome2);
               AV23Guia_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Guia_Nome2", AV23Guia_Nome2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV27ReferenciaTecnica_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ReferenciaTecnica_Nome3", AV27ReferenciaTecnica_Nome3);
               AV28Guia_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Guia_Nome3", AV28Guia_Nome3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV55TFReferenciaTecnica_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFReferenciaTecnica_Codigo), 6, 0)));
               AV56TFReferenciaTecnica_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFReferenciaTecnica_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFReferenciaTecnica_Codigo_To), 6, 0)));
               AV59TFReferenciaTecnica_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFReferenciaTecnica_Nome", AV59TFReferenciaTecnica_Nome);
               AV60TFReferenciaTecnica_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFReferenciaTecnica_Nome_Sel", AV60TFReferenciaTecnica_Nome_Sel);
               AV63TFReferenciaTecnica_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFReferenciaTecnica_Descricao", AV63TFReferenciaTecnica_Descricao);
               AV64TFReferenciaTecnica_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFReferenciaTecnica_Descricao_Sel", AV64TFReferenciaTecnica_Descricao_Sel);
               AV71TFReferenciaTecnica_Valor = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( AV71TFReferenciaTecnica_Valor, 18, 5)));
               AV72TFReferenciaTecnica_Valor_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFReferenciaTecnica_Valor_To", StringUtil.LTrim( StringUtil.Str( AV72TFReferenciaTecnica_Valor_To, 18, 5)));
               AV75TFGuia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFGuia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFGuia_Codigo), 6, 0)));
               AV76TFGuia_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFGuia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFGuia_Codigo_To), 6, 0)));
               AV79TFGuia_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFGuia_Nome", AV79TFGuia_Nome);
               AV80TFGuia_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFGuia_Nome_Sel", AV80TFGuia_Nome_Sel);
               AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace", AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace);
               AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace", AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace);
               AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace", AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace);
               AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace", AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace);
               AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace", AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace);
               AV77ddo_Guia_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Guia_CodigoTitleControlIdToReplace", AV77ddo_Guia_CodigoTitleControlIdToReplace);
               AV81ddo_Guia_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Guia_NomeTitleControlIdToReplace", AV81ddo_Guia_NomeTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV68TFReferenciaTecnica_Unidade_Sels);
               AV89Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ReferenciaTecnica_Nome1, AV18Guia_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ReferenciaTecnica_Nome2, AV23Guia_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ReferenciaTecnica_Nome3, AV28Guia_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, AV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, AV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, AV79TFGuia_Nome, AV80TFGuia_Nome_Sel, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, AV77ddo_Guia_CodigoTitleControlIdToReplace, AV81ddo_Guia_NomeTitleControlIdToReplace, AV68TFReferenciaTecnica_Unidade_Sels, AV89Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutReferenciaTecnica_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutReferenciaTecnica_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutReferenciaTecnica_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutReferenciaTecnica_Nome", AV8InOutReferenciaTecnica_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA5B2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV89Pgmname = "PromptReferenciaTecnica";
               context.Gx_err = 0;
               WS5B2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE5B2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823141226");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptreferenciatecnica.aspx") + "?" + UrlEncode("" +AV7InOutReferenciaTecnica_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutReferenciaTecnica_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vREFERENCIATECNICA_NOME1", StringUtil.RTrim( AV17ReferenciaTecnica_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vGUIA_NOME1", StringUtil.RTrim( AV18Guia_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vREFERENCIATECNICA_NOME2", StringUtil.RTrim( AV22ReferenciaTecnica_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vGUIA_NOME2", StringUtil.RTrim( AV23Guia_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vREFERENCIATECNICA_NOME3", StringUtil.RTrim( AV27ReferenciaTecnica_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vGUIA_NOME3", StringUtil.RTrim( AV28Guia_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIATECNICA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFReferenciaTecnica_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIATECNICA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFReferenciaTecnica_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIATECNICA_NOME", StringUtil.RTrim( AV59TFReferenciaTecnica_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIATECNICA_NOME_SEL", StringUtil.RTrim( AV60TFReferenciaTecnica_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIATECNICA_DESCRICAO", AV63TFReferenciaTecnica_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIATECNICA_DESCRICAO_SEL", AV64TFReferenciaTecnica_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIATECNICA_VALOR", StringUtil.LTrim( StringUtil.NToC( AV71TFReferenciaTecnica_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIATECNICA_VALOR_TO", StringUtil.LTrim( StringUtil.NToC( AV72TFReferenciaTecnica_Valor_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75TFGuia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76TFGuia_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_NOME", StringUtil.RTrim( AV79TFGuia_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGUIA_NOME_SEL", StringUtil.RTrim( AV80TFGuia_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_83", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_83), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV85GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV82DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV82DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREFERENCIATECNICA_CODIGOTITLEFILTERDATA", AV54ReferenciaTecnica_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREFERENCIATECNICA_CODIGOTITLEFILTERDATA", AV54ReferenciaTecnica_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREFERENCIATECNICA_NOMETITLEFILTERDATA", AV58ReferenciaTecnica_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREFERENCIATECNICA_NOMETITLEFILTERDATA", AV58ReferenciaTecnica_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREFERENCIATECNICA_DESCRICAOTITLEFILTERDATA", AV62ReferenciaTecnica_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREFERENCIATECNICA_DESCRICAOTITLEFILTERDATA", AV62ReferenciaTecnica_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREFERENCIATECNICA_UNIDADETITLEFILTERDATA", AV66ReferenciaTecnica_UnidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREFERENCIATECNICA_UNIDADETITLEFILTERDATA", AV66ReferenciaTecnica_UnidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREFERENCIATECNICA_VALORTITLEFILTERDATA", AV70ReferenciaTecnica_ValorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREFERENCIATECNICA_VALORTITLEFILTERDATA", AV70ReferenciaTecnica_ValorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGUIA_CODIGOTITLEFILTERDATA", AV74Guia_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGUIA_CODIGOTITLEFILTERDATA", AV74Guia_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGUIA_NOMETITLEFILTERDATA", AV78Guia_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGUIA_NOMETITLEFILTERDATA", AV78Guia_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFREFERENCIATECNICA_UNIDADE_SELS", AV68TFReferenciaTecnica_Unidade_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFREFERENCIATECNICA_UNIDADE_SELS", AV68TFReferenciaTecnica_Unidade_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV89Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTREFERENCIATECNICA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutReferenciaTecnica_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTREFERENCIATECNICA_NOME", StringUtil.RTrim( AV8InOutReferenciaTecnica_Nome));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Caption", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Cls", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_referenciatecnica_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_referenciatecnica_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_referenciatecnica_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_referenciatecnica_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_referenciatecnica_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_referenciatecnica_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Caption", StringUtil.RTrim( Ddo_referenciatecnica_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Tooltip", StringUtil.RTrim( Ddo_referenciatecnica_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Cls", StringUtil.RTrim( Ddo_referenciatecnica_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_referenciatecnica_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_referenciatecnica_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_referenciatecnica_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_referenciatecnica_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_referenciatecnica_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_referenciatecnica_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Sortedstatus", StringUtil.RTrim( Ddo_referenciatecnica_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Includefilter", StringUtil.BoolToStr( Ddo_referenciatecnica_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Filtertype", StringUtil.RTrim( Ddo_referenciatecnica_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_referenciatecnica_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_referenciatecnica_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Datalisttype", StringUtil.RTrim( Ddo_referenciatecnica_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_referenciatecnica_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Datalistproc", StringUtil.RTrim( Ddo_referenciatecnica_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_referenciatecnica_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Sortasc", StringUtil.RTrim( Ddo_referenciatecnica_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Sortdsc", StringUtil.RTrim( Ddo_referenciatecnica_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Loadingdata", StringUtil.RTrim( Ddo_referenciatecnica_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Cleanfilter", StringUtil.RTrim( Ddo_referenciatecnica_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_referenciatecnica_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Rangefilterto", StringUtil.RTrim( Ddo_referenciatecnica_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Noresultsfound", StringUtil.RTrim( Ddo_referenciatecnica_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_referenciatecnica_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Caption", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Cls", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_referenciatecnica_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_referenciatecnica_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_referenciatecnica_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_referenciatecnica_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_referenciatecnica_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_referenciatecnica_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Rangefilterfrom", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Rangefilterto", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Caption", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Tooltip", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Cls", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_referenciatecnica_unidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_referenciatecnica_unidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Sortedstatus", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Includefilter", StringUtil.BoolToStr( Ddo_referenciatecnica_unidade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_referenciatecnica_unidade_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_referenciatecnica_unidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Datalisttype", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_referenciatecnica_unidade_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Datalistfixedvalues", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_referenciatecnica_unidade_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Sortasc", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Sortdsc", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Loadingdata", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Cleanfilter", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Rangefilterfrom", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Rangefilterto", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Noresultsfound", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Caption", StringUtil.RTrim( Ddo_referenciatecnica_valor_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Tooltip", StringUtil.RTrim( Ddo_referenciatecnica_valor_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Cls", StringUtil.RTrim( Ddo_referenciatecnica_valor_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Filteredtext_set", StringUtil.RTrim( Ddo_referenciatecnica_valor_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Filteredtextto_set", StringUtil.RTrim( Ddo_referenciatecnica_valor_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_referenciatecnica_valor_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_referenciatecnica_valor_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Includesortasc", StringUtil.BoolToStr( Ddo_referenciatecnica_valor_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Includesortdsc", StringUtil.BoolToStr( Ddo_referenciatecnica_valor_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Sortedstatus", StringUtil.RTrim( Ddo_referenciatecnica_valor_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Includefilter", StringUtil.BoolToStr( Ddo_referenciatecnica_valor_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Filtertype", StringUtil.RTrim( Ddo_referenciatecnica_valor_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Filterisrange", StringUtil.BoolToStr( Ddo_referenciatecnica_valor_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Includedatalist", StringUtil.BoolToStr( Ddo_referenciatecnica_valor_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Datalistfixedvalues", StringUtil.RTrim( Ddo_referenciatecnica_valor_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_referenciatecnica_valor_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Sortasc", StringUtil.RTrim( Ddo_referenciatecnica_valor_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Sortdsc", StringUtil.RTrim( Ddo_referenciatecnica_valor_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Loadingdata", StringUtil.RTrim( Ddo_referenciatecnica_valor_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Cleanfilter", StringUtil.RTrim( Ddo_referenciatecnica_valor_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Rangefilterfrom", StringUtil.RTrim( Ddo_referenciatecnica_valor_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Rangefilterto", StringUtil.RTrim( Ddo_referenciatecnica_valor_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Noresultsfound", StringUtil.RTrim( Ddo_referenciatecnica_valor_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Searchbuttontext", StringUtil.RTrim( Ddo_referenciatecnica_valor_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Caption", StringUtil.RTrim( Ddo_guia_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_guia_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Cls", StringUtil.RTrim( Ddo_guia_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_guia_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_guia_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_guia_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_guia_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_guia_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_guia_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_guia_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_guia_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_guia_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_guia_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_guia_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_guia_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_guia_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_guia_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_guia_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_guia_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_guia_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_guia_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_guia_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_guia_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_guia_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Caption", StringUtil.RTrim( Ddo_guia_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Tooltip", StringUtil.RTrim( Ddo_guia_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Cls", StringUtil.RTrim( Ddo_guia_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_guia_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_guia_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_guia_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_guia_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_guia_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_guia_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Sortedstatus", StringUtil.RTrim( Ddo_guia_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Includefilter", StringUtil.BoolToStr( Ddo_guia_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Filtertype", StringUtil.RTrim( Ddo_guia_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_guia_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_guia_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Datalisttype", StringUtil.RTrim( Ddo_guia_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_guia_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Datalistproc", StringUtil.RTrim( Ddo_guia_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_guia_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Sortasc", StringUtil.RTrim( Ddo_guia_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Sortdsc", StringUtil.RTrim( Ddo_guia_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Loadingdata", StringUtil.RTrim( Ddo_guia_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Cleanfilter", StringUtil.RTrim( Ddo_guia_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_guia_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Rangefilterto", StringUtil.RTrim( Ddo_guia_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Noresultsfound", StringUtil.RTrim( Ddo_guia_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_guia_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_referenciatecnica_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Activeeventkey", StringUtil.RTrim( Ddo_referenciatecnica_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_referenciatecnica_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_referenciatecnica_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_referenciatecnica_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Activeeventkey", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_UNIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_referenciatecnica_unidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Activeeventkey", StringUtil.RTrim( Ddo_referenciatecnica_valor_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Filteredtext_get", StringUtil.RTrim( Ddo_referenciatecnica_valor_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIATECNICA_VALOR_Filteredtextto_get", StringUtil.RTrim( Ddo_referenciatecnica_valor_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_guia_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_guia_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_guia_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Activeeventkey", StringUtil.RTrim( Ddo_guia_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_guia_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GUIA_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_guia_nome_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm5B2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptReferenciaTecnica" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Refer�ncia T�cnica" ;
      }

      protected void WB5B0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_5B2( true) ;
         }
         else
         {
            wb_table1_2_5B2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_5B2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(96, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(97, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciatecnica_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFReferenciaTecnica_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFReferenciaTecnica_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciatecnica_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciatecnica_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciatecnica_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFReferenciaTecnica_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56TFReferenciaTecnica_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciatecnica_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciatecnica_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciatecnica_nome_Internalname, StringUtil.RTrim( AV59TFReferenciaTecnica_Nome), StringUtil.RTrim( context.localUtil.Format( AV59TFReferenciaTecnica_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciatecnica_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciatecnica_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciatecnica_nome_sel_Internalname, StringUtil.RTrim( AV60TFReferenciaTecnica_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV60TFReferenciaTecnica_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciatecnica_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciatecnica_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaTecnica.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfreferenciatecnica_descricao_Internalname, AV63TFReferenciaTecnica_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", 0, edtavTfreferenciatecnica_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptReferenciaTecnica.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfreferenciatecnica_descricao_sel_Internalname, AV64TFReferenciaTecnica_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", 0, edtavTfreferenciatecnica_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciatecnica_valor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV71TFReferenciaTecnica_Valor, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV71TFReferenciaTecnica_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciatecnica_valor_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciatecnica_valor_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciatecnica_valor_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV72TFReferenciaTecnica_Valor_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV72TFReferenciaTecnica_Valor_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciatecnica_valor_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciatecnica_valor_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75TFGuia_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV75TFGuia_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76TFGuia_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV76TFGuia_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_nome_Internalname, StringUtil.RTrim( AV79TFGuia_Nome), StringUtil.RTrim( context.localUtil.Format( AV79TFGuia_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfguia_nome_sel_Internalname, StringUtil.RTrim( AV80TFGuia_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV80TFGuia_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfguia_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfguia_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaTecnica.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REFERENCIATECNICA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_referenciatecnica_codigotitlecontrolidtoreplace_Internalname, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", 0, edtavDdo_referenciatecnica_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaTecnica.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REFERENCIATECNICA_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_referenciatecnica_nometitlecontrolidtoreplace_Internalname, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", 0, edtavDdo_referenciatecnica_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaTecnica.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REFERENCIATECNICA_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_referenciatecnica_descricaotitlecontrolidtoreplace_Internalname, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavDdo_referenciatecnica_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaTecnica.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REFERENCIATECNICA_UNIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_referenciatecnica_unidadetitlecontrolidtoreplace_Internalname, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavDdo_referenciatecnica_unidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaTecnica.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REFERENCIATECNICA_VALORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_referenciatecnica_valortitlecontrolidtoreplace_Internalname, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_referenciatecnica_valortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaTecnica.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GUIA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_guia_codigotitlecontrolidtoreplace_Internalname, AV77ddo_Guia_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavDdo_guia_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaTecnica.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GUIA_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_guia_nometitlecontrolidtoreplace_Internalname, AV81ddo_Guia_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavDdo_guia_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaTecnica.htm");
         }
         wbLoad = true;
      }

      protected void START5B2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Refer�ncia T�cnica", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP5B0( ) ;
      }

      protected void WS5B2( )
      {
         START5B2( ) ;
         EVT5B2( ) ;
      }

      protected void EVT5B2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E115B2 */
                           E115B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_REFERENCIATECNICA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E125B2 */
                           E125B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_REFERENCIATECNICA_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E135B2 */
                           E135B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_REFERENCIATECNICA_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E145B2 */
                           E145B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_REFERENCIATECNICA_UNIDADE.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E155B2 */
                           E155B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_REFERENCIATECNICA_VALOR.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E165B2 */
                           E165B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_GUIA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E175B2 */
                           E175B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_GUIA_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E185B2 */
                           E185B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E195B2 */
                           E195B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E205B2 */
                           E205B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E215B2 */
                           E215B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E225B2 */
                           E225B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E235B2 */
                           E235B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E245B2 */
                           E245B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E255B2 */
                           E255B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E265B2 */
                           E265B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E275B2 */
                           E275B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E285B2 */
                           E285B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_83_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
                           SubsflControlProps_832( ) ;
                           AV31Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV88Select_GXI : context.convertURL( context.PathToRelativeUrl( AV31Select))));
                           A97ReferenciaTecnica_Codigo = (int)(context.localUtil.CToN( cgiGet( edtReferenciaTecnica_Codigo_Internalname), ",", "."));
                           A98ReferenciaTecnica_Nome = StringUtil.Upper( cgiGet( edtReferenciaTecnica_Nome_Internalname));
                           A99ReferenciaTecnica_Descricao = cgiGet( edtReferenciaTecnica_Descricao_Internalname);
                           cmbReferenciaTecnica_Unidade.Name = cmbReferenciaTecnica_Unidade_Internalname;
                           cmbReferenciaTecnica_Unidade.CurrentValue = cgiGet( cmbReferenciaTecnica_Unidade_Internalname);
                           A114ReferenciaTecnica_Unidade = (short)(NumberUtil.Val( cgiGet( cmbReferenciaTecnica_Unidade_Internalname), "."));
                           A100ReferenciaTecnica_Valor = context.localUtil.CToN( cgiGet( edtReferenciaTecnica_Valor_Internalname), ",", ".");
                           A93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", "."));
                           A94Guia_Nome = StringUtil.Upper( cgiGet( edtGuia_Nome_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E295B2 */
                                 E295B2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E305B2 */
                                 E305B2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E315B2 */
                                 E315B2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Referenciatecnica_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIATECNICA_NOME1"), AV17ReferenciaTecnica_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Guia_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME1"), AV18Guia_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Referenciatecnica_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIATECNICA_NOME2"), AV22ReferenciaTecnica_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Guia_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME2"), AV23Guia_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Referenciatecnica_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIATECNICA_NOME3"), AV27ReferenciaTecnica_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Guia_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME3"), AV28Guia_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciatecnica_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIATECNICA_CODIGO"), ",", ".") != Convert.ToDecimal( AV55TFReferenciaTecnica_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciatecnica_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIATECNICA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV56TFReferenciaTecnica_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciatecnica_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIATECNICA_NOME"), AV59TFReferenciaTecnica_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciatecnica_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIATECNICA_NOME_SEL"), AV60TFReferenciaTecnica_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciatecnica_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIATECNICA_DESCRICAO"), AV63TFReferenciaTecnica_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciatecnica_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIATECNICA_DESCRICAO_SEL"), AV64TFReferenciaTecnica_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciatecnica_valor Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIATECNICA_VALOR"), ",", ".") != AV71TFReferenciaTecnica_Valor )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciatecnica_valor_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIATECNICA_VALOR_TO"), ",", ".") != AV72TFReferenciaTecnica_Valor_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfguia_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV75TFGuia_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfguia_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV76TFGuia_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfguia_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_NOME"), AV79TFGuia_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfguia_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_NOME_SEL"), AV80TFGuia_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E325B2 */
                                       E325B2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE5B2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm5B2( ) ;
            }
         }
      }

      protected void PA5B2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("REFERENCIATECNICA_NOME", "da Refer�ncia", 0);
            cmbavDynamicfiltersselector1.addItem("GUIA_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("REFERENCIATECNICA_NOME", "da Refer�ncia", 0);
            cmbavDynamicfiltersselector2.addItem("GUIA_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("REFERENCIATECNICA_NOME", "da Refer�ncia", 0);
            cmbavDynamicfiltersselector3.addItem("GUIA_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "REFERENCIATECNICA_UNIDADE_" + sGXsfl_83_idx;
            cmbReferenciaTecnica_Unidade.Name = GXCCtl;
            cmbReferenciaTecnica_Unidade.WebTags = "";
            cmbReferenciaTecnica_Unidade.addItem("1", "Percentual", 0);
            cmbReferenciaTecnica_Unidade.addItem("2", "Ponto Fun��o", 0);
            if ( cmbReferenciaTecnica_Unidade.ItemCount > 0 )
            {
               A114ReferenciaTecnica_Unidade = (short)(NumberUtil.Val( cmbReferenciaTecnica_Unidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0))), "."));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_832( ) ;
         while ( nGXsfl_83_idx <= nRC_GXsfl_83 )
         {
            sendrow_832( ) ;
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17ReferenciaTecnica_Nome1 ,
                                       String AV18Guia_Nome1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22ReferenciaTecnica_Nome2 ,
                                       String AV23Guia_Nome2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       String AV27ReferenciaTecnica_Nome3 ,
                                       String AV28Guia_Nome3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       int AV55TFReferenciaTecnica_Codigo ,
                                       int AV56TFReferenciaTecnica_Codigo_To ,
                                       String AV59TFReferenciaTecnica_Nome ,
                                       String AV60TFReferenciaTecnica_Nome_Sel ,
                                       String AV63TFReferenciaTecnica_Descricao ,
                                       String AV64TFReferenciaTecnica_Descricao_Sel ,
                                       decimal AV71TFReferenciaTecnica_Valor ,
                                       decimal AV72TFReferenciaTecnica_Valor_To ,
                                       int AV75TFGuia_Codigo ,
                                       int AV76TFGuia_Codigo_To ,
                                       String AV79TFGuia_Nome ,
                                       String AV80TFGuia_Nome_Sel ,
                                       String AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace ,
                                       String AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace ,
                                       String AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace ,
                                       String AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace ,
                                       String AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace ,
                                       String AV77ddo_Guia_CodigoTitleControlIdToReplace ,
                                       String AV81ddo_Guia_NomeTitleControlIdToReplace ,
                                       IGxCollection AV68TFReferenciaTecnica_Unidade_Sels ,
                                       String AV89Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF5B2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIATECNICA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A97ReferenciaTecnica_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "REFERENCIATECNICA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIATECNICA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A98ReferenciaTecnica_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "REFERENCIATECNICA_NOME", StringUtil.RTrim( A98ReferenciaTecnica_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIATECNICA_DESCRICAO", GetSecureSignedToken( "", A99ReferenciaTecnica_Descricao));
         GxWebStd.gx_hidden_field( context, "REFERENCIATECNICA_DESCRICAO", A99ReferenciaTecnica_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIATECNICA_UNIDADE", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A114ReferenciaTecnica_Unidade), "Z9")));
         GxWebStd.gx_hidden_field( context, "REFERENCIATECNICA_UNIDADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIATECNICA_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A100ReferenciaTecnica_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "REFERENCIATECNICA_VALOR", StringUtil.LTrim( StringUtil.NToC( A100ReferenciaTecnica_Valor, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_GUIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GUIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A93Guia_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF5B2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV89Pgmname = "PromptReferenciaTecnica";
         context.Gx_err = 0;
      }

      protected void RF5B2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 83;
         /* Execute user event: E305B2 */
         E305B2 ();
         nGXsfl_83_idx = 1;
         sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
         SubsflControlProps_832( ) ;
         nGXsfl_83_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_832( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A114ReferenciaTecnica_Unidade ,
                                                 AV68TFReferenciaTecnica_Unidade_Sels ,
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17ReferenciaTecnica_Nome1 ,
                                                 AV18Guia_Nome1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV22ReferenciaTecnica_Nome2 ,
                                                 AV23Guia_Nome2 ,
                                                 AV24DynamicFiltersEnabled3 ,
                                                 AV25DynamicFiltersSelector3 ,
                                                 AV26DynamicFiltersOperator3 ,
                                                 AV27ReferenciaTecnica_Nome3 ,
                                                 AV28Guia_Nome3 ,
                                                 AV55TFReferenciaTecnica_Codigo ,
                                                 AV56TFReferenciaTecnica_Codigo_To ,
                                                 AV60TFReferenciaTecnica_Nome_Sel ,
                                                 AV59TFReferenciaTecnica_Nome ,
                                                 AV64TFReferenciaTecnica_Descricao_Sel ,
                                                 AV63TFReferenciaTecnica_Descricao ,
                                                 AV68TFReferenciaTecnica_Unidade_Sels.Count ,
                                                 AV71TFReferenciaTecnica_Valor ,
                                                 AV72TFReferenciaTecnica_Valor_To ,
                                                 AV75TFGuia_Codigo ,
                                                 AV76TFGuia_Codigo_To ,
                                                 AV80TFGuia_Nome_Sel ,
                                                 AV79TFGuia_Nome ,
                                                 A98ReferenciaTecnica_Nome ,
                                                 A94Guia_Nome ,
                                                 A97ReferenciaTecnica_Codigo ,
                                                 A99ReferenciaTecnica_Descricao ,
                                                 A100ReferenciaTecnica_Valor ,
                                                 A93Guia_Codigo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17ReferenciaTecnica_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17ReferenciaTecnica_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaTecnica_Nome1", AV17ReferenciaTecnica_Nome1);
            lV17ReferenciaTecnica_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17ReferenciaTecnica_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaTecnica_Nome1", AV17ReferenciaTecnica_Nome1);
            lV18Guia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Guia_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Guia_Nome1", AV18Guia_Nome1);
            lV18Guia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Guia_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Guia_Nome1", AV18Guia_Nome1);
            lV22ReferenciaTecnica_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22ReferenciaTecnica_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ReferenciaTecnica_Nome2", AV22ReferenciaTecnica_Nome2);
            lV22ReferenciaTecnica_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22ReferenciaTecnica_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ReferenciaTecnica_Nome2", AV22ReferenciaTecnica_Nome2);
            lV23Guia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23Guia_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Guia_Nome2", AV23Guia_Nome2);
            lV23Guia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23Guia_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Guia_Nome2", AV23Guia_Nome2);
            lV27ReferenciaTecnica_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27ReferenciaTecnica_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ReferenciaTecnica_Nome3", AV27ReferenciaTecnica_Nome3);
            lV27ReferenciaTecnica_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27ReferenciaTecnica_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ReferenciaTecnica_Nome3", AV27ReferenciaTecnica_Nome3);
            lV28Guia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV28Guia_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Guia_Nome3", AV28Guia_Nome3);
            lV28Guia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV28Guia_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Guia_Nome3", AV28Guia_Nome3);
            lV59TFReferenciaTecnica_Nome = StringUtil.PadR( StringUtil.RTrim( AV59TFReferenciaTecnica_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFReferenciaTecnica_Nome", AV59TFReferenciaTecnica_Nome);
            lV63TFReferenciaTecnica_Descricao = StringUtil.Concat( StringUtil.RTrim( AV63TFReferenciaTecnica_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFReferenciaTecnica_Descricao", AV63TFReferenciaTecnica_Descricao);
            lV79TFGuia_Nome = StringUtil.PadR( StringUtil.RTrim( AV79TFGuia_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFGuia_Nome", AV79TFGuia_Nome);
            /* Using cursor H005B2 */
            pr_default.execute(0, new Object[] {lV17ReferenciaTecnica_Nome1, lV17ReferenciaTecnica_Nome1, lV18Guia_Nome1, lV18Guia_Nome1, lV22ReferenciaTecnica_Nome2, lV22ReferenciaTecnica_Nome2, lV23Guia_Nome2, lV23Guia_Nome2, lV27ReferenciaTecnica_Nome3, lV27ReferenciaTecnica_Nome3, lV28Guia_Nome3, lV28Guia_Nome3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, lV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, lV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, lV79TFGuia_Nome, AV80TFGuia_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_83_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A94Guia_Nome = H005B2_A94Guia_Nome[0];
               A93Guia_Codigo = H005B2_A93Guia_Codigo[0];
               A100ReferenciaTecnica_Valor = H005B2_A100ReferenciaTecnica_Valor[0];
               A114ReferenciaTecnica_Unidade = H005B2_A114ReferenciaTecnica_Unidade[0];
               A99ReferenciaTecnica_Descricao = H005B2_A99ReferenciaTecnica_Descricao[0];
               A98ReferenciaTecnica_Nome = H005B2_A98ReferenciaTecnica_Nome[0];
               A97ReferenciaTecnica_Codigo = H005B2_A97ReferenciaTecnica_Codigo[0];
               A94Guia_Nome = H005B2_A94Guia_Nome[0];
               /* Execute user event: E315B2 */
               E315B2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 83;
            WB5B0( ) ;
         }
         nGXsfl_83_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A114ReferenciaTecnica_Unidade ,
                                              AV68TFReferenciaTecnica_Unidade_Sels ,
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17ReferenciaTecnica_Nome1 ,
                                              AV18Guia_Nome1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV22ReferenciaTecnica_Nome2 ,
                                              AV23Guia_Nome2 ,
                                              AV24DynamicFiltersEnabled3 ,
                                              AV25DynamicFiltersSelector3 ,
                                              AV26DynamicFiltersOperator3 ,
                                              AV27ReferenciaTecnica_Nome3 ,
                                              AV28Guia_Nome3 ,
                                              AV55TFReferenciaTecnica_Codigo ,
                                              AV56TFReferenciaTecnica_Codigo_To ,
                                              AV60TFReferenciaTecnica_Nome_Sel ,
                                              AV59TFReferenciaTecnica_Nome ,
                                              AV64TFReferenciaTecnica_Descricao_Sel ,
                                              AV63TFReferenciaTecnica_Descricao ,
                                              AV68TFReferenciaTecnica_Unidade_Sels.Count ,
                                              AV71TFReferenciaTecnica_Valor ,
                                              AV72TFReferenciaTecnica_Valor_To ,
                                              AV75TFGuia_Codigo ,
                                              AV76TFGuia_Codigo_To ,
                                              AV80TFGuia_Nome_Sel ,
                                              AV79TFGuia_Nome ,
                                              A98ReferenciaTecnica_Nome ,
                                              A94Guia_Nome ,
                                              A97ReferenciaTecnica_Codigo ,
                                              A99ReferenciaTecnica_Descricao ,
                                              A100ReferenciaTecnica_Valor ,
                                              A93Guia_Codigo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV17ReferenciaTecnica_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17ReferenciaTecnica_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaTecnica_Nome1", AV17ReferenciaTecnica_Nome1);
         lV17ReferenciaTecnica_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17ReferenciaTecnica_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaTecnica_Nome1", AV17ReferenciaTecnica_Nome1);
         lV18Guia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Guia_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Guia_Nome1", AV18Guia_Nome1);
         lV18Guia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Guia_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Guia_Nome1", AV18Guia_Nome1);
         lV22ReferenciaTecnica_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22ReferenciaTecnica_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ReferenciaTecnica_Nome2", AV22ReferenciaTecnica_Nome2);
         lV22ReferenciaTecnica_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22ReferenciaTecnica_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ReferenciaTecnica_Nome2", AV22ReferenciaTecnica_Nome2);
         lV23Guia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23Guia_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Guia_Nome2", AV23Guia_Nome2);
         lV23Guia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23Guia_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Guia_Nome2", AV23Guia_Nome2);
         lV27ReferenciaTecnica_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27ReferenciaTecnica_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ReferenciaTecnica_Nome3", AV27ReferenciaTecnica_Nome3);
         lV27ReferenciaTecnica_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27ReferenciaTecnica_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ReferenciaTecnica_Nome3", AV27ReferenciaTecnica_Nome3);
         lV28Guia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV28Guia_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Guia_Nome3", AV28Guia_Nome3);
         lV28Guia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV28Guia_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Guia_Nome3", AV28Guia_Nome3);
         lV59TFReferenciaTecnica_Nome = StringUtil.PadR( StringUtil.RTrim( AV59TFReferenciaTecnica_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFReferenciaTecnica_Nome", AV59TFReferenciaTecnica_Nome);
         lV63TFReferenciaTecnica_Descricao = StringUtil.Concat( StringUtil.RTrim( AV63TFReferenciaTecnica_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFReferenciaTecnica_Descricao", AV63TFReferenciaTecnica_Descricao);
         lV79TFGuia_Nome = StringUtil.PadR( StringUtil.RTrim( AV79TFGuia_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFGuia_Nome", AV79TFGuia_Nome);
         /* Using cursor H005B3 */
         pr_default.execute(1, new Object[] {lV17ReferenciaTecnica_Nome1, lV17ReferenciaTecnica_Nome1, lV18Guia_Nome1, lV18Guia_Nome1, lV22ReferenciaTecnica_Nome2, lV22ReferenciaTecnica_Nome2, lV23Guia_Nome2, lV23Guia_Nome2, lV27ReferenciaTecnica_Nome3, lV27ReferenciaTecnica_Nome3, lV28Guia_Nome3, lV28Guia_Nome3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, lV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, lV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, lV79TFGuia_Nome, AV80TFGuia_Nome_Sel});
         GRID_nRecordCount = H005B3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ReferenciaTecnica_Nome1, AV18Guia_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ReferenciaTecnica_Nome2, AV23Guia_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ReferenciaTecnica_Nome3, AV28Guia_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, AV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, AV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, AV79TFGuia_Nome, AV80TFGuia_Nome_Sel, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, AV77ddo_Guia_CodigoTitleControlIdToReplace, AV81ddo_Guia_NomeTitleControlIdToReplace, AV68TFReferenciaTecnica_Unidade_Sels, AV89Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ReferenciaTecnica_Nome1, AV18Guia_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ReferenciaTecnica_Nome2, AV23Guia_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ReferenciaTecnica_Nome3, AV28Guia_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, AV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, AV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, AV79TFGuia_Nome, AV80TFGuia_Nome_Sel, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, AV77ddo_Guia_CodigoTitleControlIdToReplace, AV81ddo_Guia_NomeTitleControlIdToReplace, AV68TFReferenciaTecnica_Unidade_Sels, AV89Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ReferenciaTecnica_Nome1, AV18Guia_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ReferenciaTecnica_Nome2, AV23Guia_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ReferenciaTecnica_Nome3, AV28Guia_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, AV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, AV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, AV79TFGuia_Nome, AV80TFGuia_Nome_Sel, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, AV77ddo_Guia_CodigoTitleControlIdToReplace, AV81ddo_Guia_NomeTitleControlIdToReplace, AV68TFReferenciaTecnica_Unidade_Sels, AV89Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ReferenciaTecnica_Nome1, AV18Guia_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ReferenciaTecnica_Nome2, AV23Guia_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ReferenciaTecnica_Nome3, AV28Guia_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, AV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, AV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, AV79TFGuia_Nome, AV80TFGuia_Nome_Sel, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, AV77ddo_Guia_CodigoTitleControlIdToReplace, AV81ddo_Guia_NomeTitleControlIdToReplace, AV68TFReferenciaTecnica_Unidade_Sels, AV89Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ReferenciaTecnica_Nome1, AV18Guia_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ReferenciaTecnica_Nome2, AV23Guia_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ReferenciaTecnica_Nome3, AV28Guia_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, AV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, AV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, AV79TFGuia_Nome, AV80TFGuia_Nome_Sel, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, AV77ddo_Guia_CodigoTitleControlIdToReplace, AV81ddo_Guia_NomeTitleControlIdToReplace, AV68TFReferenciaTecnica_Unidade_Sels, AV89Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP5B0( )
      {
         /* Before Start, stand alone formulas. */
         AV89Pgmname = "PromptReferenciaTecnica";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E295B2 */
         E295B2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV82DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vREFERENCIATECNICA_CODIGOTITLEFILTERDATA"), AV54ReferenciaTecnica_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREFERENCIATECNICA_NOMETITLEFILTERDATA"), AV58ReferenciaTecnica_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREFERENCIATECNICA_DESCRICAOTITLEFILTERDATA"), AV62ReferenciaTecnica_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREFERENCIATECNICA_UNIDADETITLEFILTERDATA"), AV66ReferenciaTecnica_UnidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREFERENCIATECNICA_VALORTITLEFILTERDATA"), AV70ReferenciaTecnica_ValorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vGUIA_CODIGOTITLEFILTERDATA"), AV74Guia_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vGUIA_NOMETITLEFILTERDATA"), AV78Guia_NomeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17ReferenciaTecnica_Nome1 = StringUtil.Upper( cgiGet( edtavReferenciatecnica_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaTecnica_Nome1", AV17ReferenciaTecnica_Nome1);
            AV18Guia_Nome1 = StringUtil.Upper( cgiGet( edtavGuia_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Guia_Nome1", AV18Guia_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22ReferenciaTecnica_Nome2 = StringUtil.Upper( cgiGet( edtavReferenciatecnica_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ReferenciaTecnica_Nome2", AV22ReferenciaTecnica_Nome2);
            AV23Guia_Nome2 = StringUtil.Upper( cgiGet( edtavGuia_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Guia_Nome2", AV23Guia_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            AV27ReferenciaTecnica_Nome3 = StringUtil.Upper( cgiGet( edtavReferenciatecnica_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ReferenciaTecnica_Nome3", AV27ReferenciaTecnica_Nome3);
            AV28Guia_Nome3 = StringUtil.Upper( cgiGet( edtavGuia_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Guia_Nome3", AV28Guia_Nome3);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREFERENCIATECNICA_CODIGO");
               GX_FocusControl = edtavTfreferenciatecnica_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFReferenciaTecnica_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFReferenciaTecnica_Codigo), 6, 0)));
            }
            else
            {
               AV55TFReferenciaTecnica_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFReferenciaTecnica_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREFERENCIATECNICA_CODIGO_TO");
               GX_FocusControl = edtavTfreferenciatecnica_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFReferenciaTecnica_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFReferenciaTecnica_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFReferenciaTecnica_Codigo_To), 6, 0)));
            }
            else
            {
               AV56TFReferenciaTecnica_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFReferenciaTecnica_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFReferenciaTecnica_Codigo_To), 6, 0)));
            }
            AV59TFReferenciaTecnica_Nome = StringUtil.Upper( cgiGet( edtavTfreferenciatecnica_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFReferenciaTecnica_Nome", AV59TFReferenciaTecnica_Nome);
            AV60TFReferenciaTecnica_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfreferenciatecnica_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFReferenciaTecnica_Nome_Sel", AV60TFReferenciaTecnica_Nome_Sel);
            AV63TFReferenciaTecnica_Descricao = cgiGet( edtavTfreferenciatecnica_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFReferenciaTecnica_Descricao", AV63TFReferenciaTecnica_Descricao);
            AV64TFReferenciaTecnica_Descricao_Sel = cgiGet( edtavTfreferenciatecnica_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFReferenciaTecnica_Descricao_Sel", AV64TFReferenciaTecnica_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREFERENCIATECNICA_VALOR");
               GX_FocusControl = edtavTfreferenciatecnica_valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71TFReferenciaTecnica_Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( AV71TFReferenciaTecnica_Valor, 18, 5)));
            }
            else
            {
               AV71TFReferenciaTecnica_Valor = context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( AV71TFReferenciaTecnica_Valor, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_valor_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_valor_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREFERENCIATECNICA_VALOR_TO");
               GX_FocusControl = edtavTfreferenciatecnica_valor_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72TFReferenciaTecnica_Valor_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFReferenciaTecnica_Valor_To", StringUtil.LTrim( StringUtil.Str( AV72TFReferenciaTecnica_Valor_To, 18, 5)));
            }
            else
            {
               AV72TFReferenciaTecnica_Valor_To = context.localUtil.CToN( cgiGet( edtavTfreferenciatecnica_valor_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFReferenciaTecnica_Valor_To", StringUtil.LTrim( StringUtil.Str( AV72TFReferenciaTecnica_Valor_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfguia_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfguia_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFGUIA_CODIGO");
               GX_FocusControl = edtavTfguia_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV75TFGuia_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFGuia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFGuia_Codigo), 6, 0)));
            }
            else
            {
               AV75TFGuia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfguia_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFGuia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFGuia_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfguia_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfguia_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFGUIA_CODIGO_TO");
               GX_FocusControl = edtavTfguia_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV76TFGuia_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFGuia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFGuia_Codigo_To), 6, 0)));
            }
            else
            {
               AV76TFGuia_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfguia_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFGuia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFGuia_Codigo_To), 6, 0)));
            }
            AV79TFGuia_Nome = StringUtil.Upper( cgiGet( edtavTfguia_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFGuia_Nome", AV79TFGuia_Nome);
            AV80TFGuia_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfguia_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFGuia_Nome_Sel", AV80TFGuia_Nome_Sel);
            AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_referenciatecnica_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace", AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace);
            AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace = cgiGet( edtavDdo_referenciatecnica_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace", AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace);
            AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_referenciatecnica_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace", AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace);
            AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace = cgiGet( edtavDdo_referenciatecnica_unidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace", AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace);
            AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace = cgiGet( edtavDdo_referenciatecnica_valortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace", AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace);
            AV77ddo_Guia_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_guia_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Guia_CodigoTitleControlIdToReplace", AV77ddo_Guia_CodigoTitleControlIdToReplace);
            AV81ddo_Guia_NomeTitleControlIdToReplace = cgiGet( edtavDdo_guia_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Guia_NomeTitleControlIdToReplace", AV81ddo_Guia_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_83 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_83"), ",", "."));
            AV84GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV85GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_referenciatecnica_codigo_Caption = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Caption");
            Ddo_referenciatecnica_codigo_Tooltip = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Tooltip");
            Ddo_referenciatecnica_codigo_Cls = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Cls");
            Ddo_referenciatecnica_codigo_Filteredtext_set = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Filteredtext_set");
            Ddo_referenciatecnica_codigo_Filteredtextto_set = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Filteredtextto_set");
            Ddo_referenciatecnica_codigo_Dropdownoptionstype = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Dropdownoptionstype");
            Ddo_referenciatecnica_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Titlecontrolidtoreplace");
            Ddo_referenciatecnica_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Includesortasc"));
            Ddo_referenciatecnica_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Includesortdsc"));
            Ddo_referenciatecnica_codigo_Sortedstatus = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Sortedstatus");
            Ddo_referenciatecnica_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Includefilter"));
            Ddo_referenciatecnica_codigo_Filtertype = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Filtertype");
            Ddo_referenciatecnica_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Filterisrange"));
            Ddo_referenciatecnica_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Includedatalist"));
            Ddo_referenciatecnica_codigo_Datalistfixedvalues = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Datalistfixedvalues");
            Ddo_referenciatecnica_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_referenciatecnica_codigo_Sortasc = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Sortasc");
            Ddo_referenciatecnica_codigo_Sortdsc = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Sortdsc");
            Ddo_referenciatecnica_codigo_Loadingdata = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Loadingdata");
            Ddo_referenciatecnica_codigo_Cleanfilter = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Cleanfilter");
            Ddo_referenciatecnica_codigo_Rangefilterfrom = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Rangefilterfrom");
            Ddo_referenciatecnica_codigo_Rangefilterto = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Rangefilterto");
            Ddo_referenciatecnica_codigo_Noresultsfound = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Noresultsfound");
            Ddo_referenciatecnica_codigo_Searchbuttontext = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Searchbuttontext");
            Ddo_referenciatecnica_nome_Caption = cgiGet( "DDO_REFERENCIATECNICA_NOME_Caption");
            Ddo_referenciatecnica_nome_Tooltip = cgiGet( "DDO_REFERENCIATECNICA_NOME_Tooltip");
            Ddo_referenciatecnica_nome_Cls = cgiGet( "DDO_REFERENCIATECNICA_NOME_Cls");
            Ddo_referenciatecnica_nome_Filteredtext_set = cgiGet( "DDO_REFERENCIATECNICA_NOME_Filteredtext_set");
            Ddo_referenciatecnica_nome_Selectedvalue_set = cgiGet( "DDO_REFERENCIATECNICA_NOME_Selectedvalue_set");
            Ddo_referenciatecnica_nome_Dropdownoptionstype = cgiGet( "DDO_REFERENCIATECNICA_NOME_Dropdownoptionstype");
            Ddo_referenciatecnica_nome_Titlecontrolidtoreplace = cgiGet( "DDO_REFERENCIATECNICA_NOME_Titlecontrolidtoreplace");
            Ddo_referenciatecnica_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_NOME_Includesortasc"));
            Ddo_referenciatecnica_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_NOME_Includesortdsc"));
            Ddo_referenciatecnica_nome_Sortedstatus = cgiGet( "DDO_REFERENCIATECNICA_NOME_Sortedstatus");
            Ddo_referenciatecnica_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_NOME_Includefilter"));
            Ddo_referenciatecnica_nome_Filtertype = cgiGet( "DDO_REFERENCIATECNICA_NOME_Filtertype");
            Ddo_referenciatecnica_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_NOME_Filterisrange"));
            Ddo_referenciatecnica_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_NOME_Includedatalist"));
            Ddo_referenciatecnica_nome_Datalisttype = cgiGet( "DDO_REFERENCIATECNICA_NOME_Datalisttype");
            Ddo_referenciatecnica_nome_Datalistfixedvalues = cgiGet( "DDO_REFERENCIATECNICA_NOME_Datalistfixedvalues");
            Ddo_referenciatecnica_nome_Datalistproc = cgiGet( "DDO_REFERENCIATECNICA_NOME_Datalistproc");
            Ddo_referenciatecnica_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REFERENCIATECNICA_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_referenciatecnica_nome_Sortasc = cgiGet( "DDO_REFERENCIATECNICA_NOME_Sortasc");
            Ddo_referenciatecnica_nome_Sortdsc = cgiGet( "DDO_REFERENCIATECNICA_NOME_Sortdsc");
            Ddo_referenciatecnica_nome_Loadingdata = cgiGet( "DDO_REFERENCIATECNICA_NOME_Loadingdata");
            Ddo_referenciatecnica_nome_Cleanfilter = cgiGet( "DDO_REFERENCIATECNICA_NOME_Cleanfilter");
            Ddo_referenciatecnica_nome_Rangefilterfrom = cgiGet( "DDO_REFERENCIATECNICA_NOME_Rangefilterfrom");
            Ddo_referenciatecnica_nome_Rangefilterto = cgiGet( "DDO_REFERENCIATECNICA_NOME_Rangefilterto");
            Ddo_referenciatecnica_nome_Noresultsfound = cgiGet( "DDO_REFERENCIATECNICA_NOME_Noresultsfound");
            Ddo_referenciatecnica_nome_Searchbuttontext = cgiGet( "DDO_REFERENCIATECNICA_NOME_Searchbuttontext");
            Ddo_referenciatecnica_descricao_Caption = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Caption");
            Ddo_referenciatecnica_descricao_Tooltip = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Tooltip");
            Ddo_referenciatecnica_descricao_Cls = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Cls");
            Ddo_referenciatecnica_descricao_Filteredtext_set = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Filteredtext_set");
            Ddo_referenciatecnica_descricao_Selectedvalue_set = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Selectedvalue_set");
            Ddo_referenciatecnica_descricao_Dropdownoptionstype = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Dropdownoptionstype");
            Ddo_referenciatecnica_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_referenciatecnica_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Includesortasc"));
            Ddo_referenciatecnica_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Includesortdsc"));
            Ddo_referenciatecnica_descricao_Sortedstatus = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Sortedstatus");
            Ddo_referenciatecnica_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Includefilter"));
            Ddo_referenciatecnica_descricao_Filtertype = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Filtertype");
            Ddo_referenciatecnica_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Filterisrange"));
            Ddo_referenciatecnica_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Includedatalist"));
            Ddo_referenciatecnica_descricao_Datalisttype = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Datalisttype");
            Ddo_referenciatecnica_descricao_Datalistfixedvalues = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Datalistfixedvalues");
            Ddo_referenciatecnica_descricao_Datalistproc = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Datalistproc");
            Ddo_referenciatecnica_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_referenciatecnica_descricao_Sortasc = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Sortasc");
            Ddo_referenciatecnica_descricao_Sortdsc = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Sortdsc");
            Ddo_referenciatecnica_descricao_Loadingdata = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Loadingdata");
            Ddo_referenciatecnica_descricao_Cleanfilter = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Cleanfilter");
            Ddo_referenciatecnica_descricao_Rangefilterfrom = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Rangefilterfrom");
            Ddo_referenciatecnica_descricao_Rangefilterto = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Rangefilterto");
            Ddo_referenciatecnica_descricao_Noresultsfound = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Noresultsfound");
            Ddo_referenciatecnica_descricao_Searchbuttontext = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Searchbuttontext");
            Ddo_referenciatecnica_unidade_Caption = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Caption");
            Ddo_referenciatecnica_unidade_Tooltip = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Tooltip");
            Ddo_referenciatecnica_unidade_Cls = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Cls");
            Ddo_referenciatecnica_unidade_Selectedvalue_set = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Selectedvalue_set");
            Ddo_referenciatecnica_unidade_Dropdownoptionstype = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Dropdownoptionstype");
            Ddo_referenciatecnica_unidade_Titlecontrolidtoreplace = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Titlecontrolidtoreplace");
            Ddo_referenciatecnica_unidade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Includesortasc"));
            Ddo_referenciatecnica_unidade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Includesortdsc"));
            Ddo_referenciatecnica_unidade_Sortedstatus = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Sortedstatus");
            Ddo_referenciatecnica_unidade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Includefilter"));
            Ddo_referenciatecnica_unidade_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Filterisrange"));
            Ddo_referenciatecnica_unidade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Includedatalist"));
            Ddo_referenciatecnica_unidade_Datalisttype = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Datalisttype");
            Ddo_referenciatecnica_unidade_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Allowmultipleselection"));
            Ddo_referenciatecnica_unidade_Datalistfixedvalues = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Datalistfixedvalues");
            Ddo_referenciatecnica_unidade_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_referenciatecnica_unidade_Sortasc = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Sortasc");
            Ddo_referenciatecnica_unidade_Sortdsc = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Sortdsc");
            Ddo_referenciatecnica_unidade_Loadingdata = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Loadingdata");
            Ddo_referenciatecnica_unidade_Cleanfilter = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Cleanfilter");
            Ddo_referenciatecnica_unidade_Rangefilterfrom = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Rangefilterfrom");
            Ddo_referenciatecnica_unidade_Rangefilterto = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Rangefilterto");
            Ddo_referenciatecnica_unidade_Noresultsfound = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Noresultsfound");
            Ddo_referenciatecnica_unidade_Searchbuttontext = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Searchbuttontext");
            Ddo_referenciatecnica_valor_Caption = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Caption");
            Ddo_referenciatecnica_valor_Tooltip = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Tooltip");
            Ddo_referenciatecnica_valor_Cls = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Cls");
            Ddo_referenciatecnica_valor_Filteredtext_set = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Filteredtext_set");
            Ddo_referenciatecnica_valor_Filteredtextto_set = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Filteredtextto_set");
            Ddo_referenciatecnica_valor_Dropdownoptionstype = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Dropdownoptionstype");
            Ddo_referenciatecnica_valor_Titlecontrolidtoreplace = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Titlecontrolidtoreplace");
            Ddo_referenciatecnica_valor_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_VALOR_Includesortasc"));
            Ddo_referenciatecnica_valor_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_VALOR_Includesortdsc"));
            Ddo_referenciatecnica_valor_Sortedstatus = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Sortedstatus");
            Ddo_referenciatecnica_valor_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_VALOR_Includefilter"));
            Ddo_referenciatecnica_valor_Filtertype = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Filtertype");
            Ddo_referenciatecnica_valor_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_VALOR_Filterisrange"));
            Ddo_referenciatecnica_valor_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIATECNICA_VALOR_Includedatalist"));
            Ddo_referenciatecnica_valor_Datalistfixedvalues = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Datalistfixedvalues");
            Ddo_referenciatecnica_valor_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REFERENCIATECNICA_VALOR_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_referenciatecnica_valor_Sortasc = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Sortasc");
            Ddo_referenciatecnica_valor_Sortdsc = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Sortdsc");
            Ddo_referenciatecnica_valor_Loadingdata = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Loadingdata");
            Ddo_referenciatecnica_valor_Cleanfilter = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Cleanfilter");
            Ddo_referenciatecnica_valor_Rangefilterfrom = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Rangefilterfrom");
            Ddo_referenciatecnica_valor_Rangefilterto = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Rangefilterto");
            Ddo_referenciatecnica_valor_Noresultsfound = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Noresultsfound");
            Ddo_referenciatecnica_valor_Searchbuttontext = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Searchbuttontext");
            Ddo_guia_codigo_Caption = cgiGet( "DDO_GUIA_CODIGO_Caption");
            Ddo_guia_codigo_Tooltip = cgiGet( "DDO_GUIA_CODIGO_Tooltip");
            Ddo_guia_codigo_Cls = cgiGet( "DDO_GUIA_CODIGO_Cls");
            Ddo_guia_codigo_Filteredtext_set = cgiGet( "DDO_GUIA_CODIGO_Filteredtext_set");
            Ddo_guia_codigo_Filteredtextto_set = cgiGet( "DDO_GUIA_CODIGO_Filteredtextto_set");
            Ddo_guia_codigo_Dropdownoptionstype = cgiGet( "DDO_GUIA_CODIGO_Dropdownoptionstype");
            Ddo_guia_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_GUIA_CODIGO_Titlecontrolidtoreplace");
            Ddo_guia_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_CODIGO_Includesortasc"));
            Ddo_guia_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_CODIGO_Includesortdsc"));
            Ddo_guia_codigo_Sortedstatus = cgiGet( "DDO_GUIA_CODIGO_Sortedstatus");
            Ddo_guia_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GUIA_CODIGO_Includefilter"));
            Ddo_guia_codigo_Filtertype = cgiGet( "DDO_GUIA_CODIGO_Filtertype");
            Ddo_guia_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GUIA_CODIGO_Filterisrange"));
            Ddo_guia_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GUIA_CODIGO_Includedatalist"));
            Ddo_guia_codigo_Datalistfixedvalues = cgiGet( "DDO_GUIA_CODIGO_Datalistfixedvalues");
            Ddo_guia_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_GUIA_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_guia_codigo_Sortasc = cgiGet( "DDO_GUIA_CODIGO_Sortasc");
            Ddo_guia_codigo_Sortdsc = cgiGet( "DDO_GUIA_CODIGO_Sortdsc");
            Ddo_guia_codigo_Loadingdata = cgiGet( "DDO_GUIA_CODIGO_Loadingdata");
            Ddo_guia_codigo_Cleanfilter = cgiGet( "DDO_GUIA_CODIGO_Cleanfilter");
            Ddo_guia_codigo_Rangefilterfrom = cgiGet( "DDO_GUIA_CODIGO_Rangefilterfrom");
            Ddo_guia_codigo_Rangefilterto = cgiGet( "DDO_GUIA_CODIGO_Rangefilterto");
            Ddo_guia_codigo_Noresultsfound = cgiGet( "DDO_GUIA_CODIGO_Noresultsfound");
            Ddo_guia_codigo_Searchbuttontext = cgiGet( "DDO_GUIA_CODIGO_Searchbuttontext");
            Ddo_guia_nome_Caption = cgiGet( "DDO_GUIA_NOME_Caption");
            Ddo_guia_nome_Tooltip = cgiGet( "DDO_GUIA_NOME_Tooltip");
            Ddo_guia_nome_Cls = cgiGet( "DDO_GUIA_NOME_Cls");
            Ddo_guia_nome_Filteredtext_set = cgiGet( "DDO_GUIA_NOME_Filteredtext_set");
            Ddo_guia_nome_Selectedvalue_set = cgiGet( "DDO_GUIA_NOME_Selectedvalue_set");
            Ddo_guia_nome_Dropdownoptionstype = cgiGet( "DDO_GUIA_NOME_Dropdownoptionstype");
            Ddo_guia_nome_Titlecontrolidtoreplace = cgiGet( "DDO_GUIA_NOME_Titlecontrolidtoreplace");
            Ddo_guia_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_NOME_Includesortasc"));
            Ddo_guia_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GUIA_NOME_Includesortdsc"));
            Ddo_guia_nome_Sortedstatus = cgiGet( "DDO_GUIA_NOME_Sortedstatus");
            Ddo_guia_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GUIA_NOME_Includefilter"));
            Ddo_guia_nome_Filtertype = cgiGet( "DDO_GUIA_NOME_Filtertype");
            Ddo_guia_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GUIA_NOME_Filterisrange"));
            Ddo_guia_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GUIA_NOME_Includedatalist"));
            Ddo_guia_nome_Datalisttype = cgiGet( "DDO_GUIA_NOME_Datalisttype");
            Ddo_guia_nome_Datalistfixedvalues = cgiGet( "DDO_GUIA_NOME_Datalistfixedvalues");
            Ddo_guia_nome_Datalistproc = cgiGet( "DDO_GUIA_NOME_Datalistproc");
            Ddo_guia_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_GUIA_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_guia_nome_Sortasc = cgiGet( "DDO_GUIA_NOME_Sortasc");
            Ddo_guia_nome_Sortdsc = cgiGet( "DDO_GUIA_NOME_Sortdsc");
            Ddo_guia_nome_Loadingdata = cgiGet( "DDO_GUIA_NOME_Loadingdata");
            Ddo_guia_nome_Cleanfilter = cgiGet( "DDO_GUIA_NOME_Cleanfilter");
            Ddo_guia_nome_Rangefilterfrom = cgiGet( "DDO_GUIA_NOME_Rangefilterfrom");
            Ddo_guia_nome_Rangefilterto = cgiGet( "DDO_GUIA_NOME_Rangefilterto");
            Ddo_guia_nome_Noresultsfound = cgiGet( "DDO_GUIA_NOME_Noresultsfound");
            Ddo_guia_nome_Searchbuttontext = cgiGet( "DDO_GUIA_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_referenciatecnica_codigo_Activeeventkey = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Activeeventkey");
            Ddo_referenciatecnica_codigo_Filteredtext_get = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Filteredtext_get");
            Ddo_referenciatecnica_codigo_Filteredtextto_get = cgiGet( "DDO_REFERENCIATECNICA_CODIGO_Filteredtextto_get");
            Ddo_referenciatecnica_nome_Activeeventkey = cgiGet( "DDO_REFERENCIATECNICA_NOME_Activeeventkey");
            Ddo_referenciatecnica_nome_Filteredtext_get = cgiGet( "DDO_REFERENCIATECNICA_NOME_Filteredtext_get");
            Ddo_referenciatecnica_nome_Selectedvalue_get = cgiGet( "DDO_REFERENCIATECNICA_NOME_Selectedvalue_get");
            Ddo_referenciatecnica_descricao_Activeeventkey = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Activeeventkey");
            Ddo_referenciatecnica_descricao_Filteredtext_get = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Filteredtext_get");
            Ddo_referenciatecnica_descricao_Selectedvalue_get = cgiGet( "DDO_REFERENCIATECNICA_DESCRICAO_Selectedvalue_get");
            Ddo_referenciatecnica_unidade_Activeeventkey = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Activeeventkey");
            Ddo_referenciatecnica_unidade_Selectedvalue_get = cgiGet( "DDO_REFERENCIATECNICA_UNIDADE_Selectedvalue_get");
            Ddo_referenciatecnica_valor_Activeeventkey = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Activeeventkey");
            Ddo_referenciatecnica_valor_Filteredtext_get = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Filteredtext_get");
            Ddo_referenciatecnica_valor_Filteredtextto_get = cgiGet( "DDO_REFERENCIATECNICA_VALOR_Filteredtextto_get");
            Ddo_guia_codigo_Activeeventkey = cgiGet( "DDO_GUIA_CODIGO_Activeeventkey");
            Ddo_guia_codigo_Filteredtext_get = cgiGet( "DDO_GUIA_CODIGO_Filteredtext_get");
            Ddo_guia_codigo_Filteredtextto_get = cgiGet( "DDO_GUIA_CODIGO_Filteredtextto_get");
            Ddo_guia_nome_Activeeventkey = cgiGet( "DDO_GUIA_NOME_Activeeventkey");
            Ddo_guia_nome_Filteredtext_get = cgiGet( "DDO_GUIA_NOME_Filteredtext_get");
            Ddo_guia_nome_Selectedvalue_get = cgiGet( "DDO_GUIA_NOME_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIATECNICA_NOME1"), AV17ReferenciaTecnica_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME1"), AV18Guia_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIATECNICA_NOME2"), AV22ReferenciaTecnica_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME2"), AV23Guia_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIATECNICA_NOME3"), AV27ReferenciaTecnica_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGUIA_NOME3"), AV28Guia_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIATECNICA_CODIGO"), ",", ".") != Convert.ToDecimal( AV55TFReferenciaTecnica_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIATECNICA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV56TFReferenciaTecnica_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIATECNICA_NOME"), AV59TFReferenciaTecnica_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIATECNICA_NOME_SEL"), AV60TFReferenciaTecnica_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIATECNICA_DESCRICAO"), AV63TFReferenciaTecnica_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIATECNICA_DESCRICAO_SEL"), AV64TFReferenciaTecnica_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIATECNICA_VALOR"), ",", ".") != AV71TFReferenciaTecnica_Valor )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIATECNICA_VALOR_TO"), ",", ".") != AV72TFReferenciaTecnica_Valor_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV75TFGuia_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGUIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV76TFGuia_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_NOME"), AV79TFGuia_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGUIA_NOME_SEL"), AV80TFGuia_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E295B2 */
         E295B2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E295B2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "REFERENCIATECNICA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "REFERENCIATECNICA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "REFERENCIATECNICA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfreferenciatecnica_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciatecnica_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciatecnica_codigo_Visible), 5, 0)));
         edtavTfreferenciatecnica_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciatecnica_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciatecnica_codigo_to_Visible), 5, 0)));
         edtavTfreferenciatecnica_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciatecnica_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciatecnica_nome_Visible), 5, 0)));
         edtavTfreferenciatecnica_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciatecnica_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciatecnica_nome_sel_Visible), 5, 0)));
         edtavTfreferenciatecnica_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciatecnica_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciatecnica_descricao_Visible), 5, 0)));
         edtavTfreferenciatecnica_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciatecnica_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciatecnica_descricao_sel_Visible), 5, 0)));
         edtavTfreferenciatecnica_valor_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciatecnica_valor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciatecnica_valor_Visible), 5, 0)));
         edtavTfreferenciatecnica_valor_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciatecnica_valor_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciatecnica_valor_to_Visible), 5, 0)));
         edtavTfguia_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_codigo_Visible), 5, 0)));
         edtavTfguia_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_codigo_to_Visible), 5, 0)));
         edtavTfguia_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_nome_Visible), 5, 0)));
         edtavTfguia_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfguia_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfguia_nome_sel_Visible), 5, 0)));
         Ddo_referenciatecnica_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ReferenciaTecnica_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_codigo_Internalname, "TitleControlIdToReplace", Ddo_referenciatecnica_codigo_Titlecontrolidtoreplace);
         AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace = Ddo_referenciatecnica_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace", AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace);
         edtavDdo_referenciatecnica_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_referenciatecnica_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_referenciatecnica_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_referenciatecnica_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_ReferenciaTecnica_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_nome_Internalname, "TitleControlIdToReplace", Ddo_referenciatecnica_nome_Titlecontrolidtoreplace);
         AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace = Ddo_referenciatecnica_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace", AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace);
         edtavDdo_referenciatecnica_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_referenciatecnica_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_referenciatecnica_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_referenciatecnica_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ReferenciaTecnica_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_descricao_Internalname, "TitleControlIdToReplace", Ddo_referenciatecnica_descricao_Titlecontrolidtoreplace);
         AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace = Ddo_referenciatecnica_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace", AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace);
         edtavDdo_referenciatecnica_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_referenciatecnica_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_referenciatecnica_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_referenciatecnica_unidade_Titlecontrolidtoreplace = subGrid_Internalname+"_ReferenciaTecnica_Unidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_unidade_Internalname, "TitleControlIdToReplace", Ddo_referenciatecnica_unidade_Titlecontrolidtoreplace);
         AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace = Ddo_referenciatecnica_unidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace", AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace);
         edtavDdo_referenciatecnica_unidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_referenciatecnica_unidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_referenciatecnica_unidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_referenciatecnica_valor_Titlecontrolidtoreplace = subGrid_Internalname+"_ReferenciaTecnica_Valor";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_valor_Internalname, "TitleControlIdToReplace", Ddo_referenciatecnica_valor_Titlecontrolidtoreplace);
         AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace = Ddo_referenciatecnica_valor_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace", AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace);
         edtavDdo_referenciatecnica_valortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_referenciatecnica_valortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_referenciatecnica_valortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_guia_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Guia_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "TitleControlIdToReplace", Ddo_guia_codigo_Titlecontrolidtoreplace);
         AV77ddo_Guia_CodigoTitleControlIdToReplace = Ddo_guia_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Guia_CodigoTitleControlIdToReplace", AV77ddo_Guia_CodigoTitleControlIdToReplace);
         edtavDdo_guia_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_guia_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_guia_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_guia_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Guia_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "TitleControlIdToReplace", Ddo_guia_nome_Titlecontrolidtoreplace);
         AV81ddo_Guia_NomeTitleControlIdToReplace = Ddo_guia_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Guia_NomeTitleControlIdToReplace", AV81ddo_Guia_NomeTitleControlIdToReplace);
         edtavDdo_guia_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_guia_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_guia_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione uma  Refer�ncia T�cnica";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "da Refer�ncia", 0);
         cmbavOrderedby.addItem("2", "C�digo", 0);
         cmbavOrderedby.addItem("3", "Descri��o", 0);
         cmbavOrderedby.addItem("4", "Unidade", 0);
         cmbavOrderedby.addItem("5", "Valor", 0);
         cmbavOrderedby.addItem("6", "C�digo", 0);
         cmbavOrderedby.addItem("7", "Nome", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV82DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV82DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E305B2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV54ReferenciaTecnica_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ReferenciaTecnica_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ReferenciaTecnica_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66ReferenciaTecnica_UnidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70ReferenciaTecnica_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74Guia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78Guia_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GUIA_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV24DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "GUIA_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtReferenciaTecnica_Codigo_Titleformat = 2;
         edtReferenciaTecnica_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaTecnica_Codigo_Internalname, "Title", edtReferenciaTecnica_Codigo_Title);
         edtReferenciaTecnica_Nome_Titleformat = 2;
         edtReferenciaTecnica_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "da Refer�ncia", AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaTecnica_Nome_Internalname, "Title", edtReferenciaTecnica_Nome_Title);
         edtReferenciaTecnica_Descricao_Titleformat = 2;
         edtReferenciaTecnica_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaTecnica_Descricao_Internalname, "Title", edtReferenciaTecnica_Descricao_Title);
         cmbReferenciaTecnica_Unidade_Titleformat = 2;
         cmbReferenciaTecnica_Unidade.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Unidade", AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbReferenciaTecnica_Unidade_Internalname, "Title", cmbReferenciaTecnica_Unidade.Title.Text);
         edtReferenciaTecnica_Valor_Titleformat = 2;
         edtReferenciaTecnica_Valor_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor", AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaTecnica_Valor_Internalname, "Title", edtReferenciaTecnica_Valor_Title);
         edtGuia_Codigo_Titleformat = 2;
         edtGuia_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV77ddo_Guia_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Codigo_Internalname, "Title", edtGuia_Codigo_Title);
         edtGuia_Nome_Titleformat = 2;
         edtGuia_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV81ddo_Guia_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Nome_Internalname, "Title", edtGuia_Nome_Title);
         AV84GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84GridCurrentPage), 10, 0)));
         AV85GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV85GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54ReferenciaTecnica_CodigoTitleFilterData", AV54ReferenciaTecnica_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58ReferenciaTecnica_NomeTitleFilterData", AV58ReferenciaTecnica_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62ReferenciaTecnica_DescricaoTitleFilterData", AV62ReferenciaTecnica_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66ReferenciaTecnica_UnidadeTitleFilterData", AV66ReferenciaTecnica_UnidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70ReferenciaTecnica_ValorTitleFilterData", AV70ReferenciaTecnica_ValorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV74Guia_CodigoTitleFilterData", AV74Guia_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV78Guia_NomeTitleFilterData", AV78Guia_NomeTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E115B2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV83PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV83PageToGo) ;
         }
      }

      protected void E125B2( )
      {
         /* Ddo_referenciatecnica_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_referenciatecnica_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciatecnica_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_codigo_Internalname, "SortedStatus", Ddo_referenciatecnica_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciatecnica_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciatecnica_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_codigo_Internalname, "SortedStatus", Ddo_referenciatecnica_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciatecnica_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFReferenciaTecnica_Codigo = (int)(NumberUtil.Val( Ddo_referenciatecnica_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFReferenciaTecnica_Codigo), 6, 0)));
            AV56TFReferenciaTecnica_Codigo_To = (int)(NumberUtil.Val( Ddo_referenciatecnica_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFReferenciaTecnica_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFReferenciaTecnica_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E135B2( )
      {
         /* Ddo_referenciatecnica_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_referenciatecnica_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciatecnica_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_nome_Internalname, "SortedStatus", Ddo_referenciatecnica_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciatecnica_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciatecnica_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_nome_Internalname, "SortedStatus", Ddo_referenciatecnica_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciatecnica_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFReferenciaTecnica_Nome = Ddo_referenciatecnica_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFReferenciaTecnica_Nome", AV59TFReferenciaTecnica_Nome);
            AV60TFReferenciaTecnica_Nome_Sel = Ddo_referenciatecnica_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFReferenciaTecnica_Nome_Sel", AV60TFReferenciaTecnica_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E145B2( )
      {
         /* Ddo_referenciatecnica_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_referenciatecnica_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciatecnica_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_descricao_Internalname, "SortedStatus", Ddo_referenciatecnica_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciatecnica_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciatecnica_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_descricao_Internalname, "SortedStatus", Ddo_referenciatecnica_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciatecnica_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFReferenciaTecnica_Descricao = Ddo_referenciatecnica_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFReferenciaTecnica_Descricao", AV63TFReferenciaTecnica_Descricao);
            AV64TFReferenciaTecnica_Descricao_Sel = Ddo_referenciatecnica_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFReferenciaTecnica_Descricao_Sel", AV64TFReferenciaTecnica_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E155B2( )
      {
         /* Ddo_referenciatecnica_unidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_referenciatecnica_unidade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciatecnica_unidade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_unidade_Internalname, "SortedStatus", Ddo_referenciatecnica_unidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciatecnica_unidade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciatecnica_unidade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_unidade_Internalname, "SortedStatus", Ddo_referenciatecnica_unidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciatecnica_unidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFReferenciaTecnica_Unidade_SelsJson = Ddo_referenciatecnica_unidade_Selectedvalue_get;
            AV68TFReferenciaTecnica_Unidade_Sels.FromJSonString(StringUtil.StringReplace( AV67TFReferenciaTecnica_Unidade_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68TFReferenciaTecnica_Unidade_Sels", AV68TFReferenciaTecnica_Unidade_Sels);
      }

      protected void E165B2( )
      {
         /* Ddo_referenciatecnica_valor_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_referenciatecnica_valor_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciatecnica_valor_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_valor_Internalname, "SortedStatus", Ddo_referenciatecnica_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciatecnica_valor_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciatecnica_valor_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_valor_Internalname, "SortedStatus", Ddo_referenciatecnica_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciatecnica_valor_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV71TFReferenciaTecnica_Valor = NumberUtil.Val( Ddo_referenciatecnica_valor_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( AV71TFReferenciaTecnica_Valor, 18, 5)));
            AV72TFReferenciaTecnica_Valor_To = NumberUtil.Val( Ddo_referenciatecnica_valor_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFReferenciaTecnica_Valor_To", StringUtil.LTrim( StringUtil.Str( AV72TFReferenciaTecnica_Valor_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E175B2( )
      {
         /* Ddo_guia_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_guia_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "SortedStatus", Ddo_guia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "SortedStatus", Ddo_guia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV75TFGuia_Codigo = (int)(NumberUtil.Val( Ddo_guia_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFGuia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFGuia_Codigo), 6, 0)));
            AV76TFGuia_Codigo_To = (int)(NumberUtil.Val( Ddo_guia_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFGuia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFGuia_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E185B2( )
      {
         /* Ddo_guia_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_guia_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "SortedStatus", Ddo_guia_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_guia_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "SortedStatus", Ddo_guia_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_guia_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV79TFGuia_Nome = Ddo_guia_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFGuia_Nome", AV79TFGuia_Nome);
            AV80TFGuia_Nome_Sel = Ddo_guia_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFGuia_Nome_Sel", AV80TFGuia_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E315B2( )
      {
         /* Grid_Load Routine */
         AV31Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV31Select);
         AV88Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 83;
         }
         sendrow_832( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_83_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(83, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E325B2 */
         E325B2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E325B2( )
      {
         /* Enter Routine */
         AV7InOutReferenciaTecnica_Codigo = A97ReferenciaTecnica_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutReferenciaTecnica_Codigo), 6, 0)));
         AV8InOutReferenciaTecnica_Nome = A98ReferenciaTecnica_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutReferenciaTecnica_Nome", AV8InOutReferenciaTecnica_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutReferenciaTecnica_Codigo,(String)AV8InOutReferenciaTecnica_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E195B2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E245B2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ReferenciaTecnica_Nome1, AV18Guia_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ReferenciaTecnica_Nome2, AV23Guia_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ReferenciaTecnica_Nome3, AV28Guia_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, AV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, AV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, AV79TFGuia_Nome, AV80TFGuia_Nome_Sel, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, AV77ddo_Guia_CodigoTitleControlIdToReplace, AV81ddo_Guia_NomeTitleControlIdToReplace, AV68TFReferenciaTecnica_Unidade_Sels, AV89Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
      }

      protected void E205B2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ReferenciaTecnica_Nome1, AV18Guia_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ReferenciaTecnica_Nome2, AV23Guia_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ReferenciaTecnica_Nome3, AV28Guia_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, AV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, AV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, AV79TFGuia_Nome, AV80TFGuia_Nome_Sel, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, AV77ddo_Guia_CodigoTitleControlIdToReplace, AV81ddo_Guia_NomeTitleControlIdToReplace, AV68TFReferenciaTecnica_Unidade_Sels, AV89Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E255B2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E265B2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ReferenciaTecnica_Nome1, AV18Guia_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ReferenciaTecnica_Nome2, AV23Guia_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ReferenciaTecnica_Nome3, AV28Guia_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, AV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, AV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, AV79TFGuia_Nome, AV80TFGuia_Nome_Sel, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, AV77ddo_Guia_CodigoTitleControlIdToReplace, AV81ddo_Guia_NomeTitleControlIdToReplace, AV68TFReferenciaTecnica_Unidade_Sels, AV89Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
      }

      protected void E215B2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ReferenciaTecnica_Nome1, AV18Guia_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ReferenciaTecnica_Nome2, AV23Guia_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ReferenciaTecnica_Nome3, AV28Guia_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, AV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, AV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, AV79TFGuia_Nome, AV80TFGuia_Nome_Sel, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, AV77ddo_Guia_CodigoTitleControlIdToReplace, AV81ddo_Guia_NomeTitleControlIdToReplace, AV68TFReferenciaTecnica_Unidade_Sels, AV89Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E275B2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E225B2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ReferenciaTecnica_Nome1, AV18Guia_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ReferenciaTecnica_Nome2, AV23Guia_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ReferenciaTecnica_Nome3, AV28Guia_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV55TFReferenciaTecnica_Codigo, AV56TFReferenciaTecnica_Codigo_To, AV59TFReferenciaTecnica_Nome, AV60TFReferenciaTecnica_Nome_Sel, AV63TFReferenciaTecnica_Descricao, AV64TFReferenciaTecnica_Descricao_Sel, AV71TFReferenciaTecnica_Valor, AV72TFReferenciaTecnica_Valor_To, AV75TFGuia_Codigo, AV76TFGuia_Codigo_To, AV79TFGuia_Nome, AV80TFGuia_Nome_Sel, AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace, AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace, AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace, AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace, AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace, AV77ddo_Guia_CodigoTitleControlIdToReplace, AV81ddo_Guia_NomeTitleControlIdToReplace, AV68TFReferenciaTecnica_Unidade_Sels, AV89Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E285B2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E235B2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68TFReferenciaTecnica_Unidade_Sels", AV68TFReferenciaTecnica_Unidade_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_referenciatecnica_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_codigo_Internalname, "SortedStatus", Ddo_referenciatecnica_codigo_Sortedstatus);
         Ddo_referenciatecnica_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_nome_Internalname, "SortedStatus", Ddo_referenciatecnica_nome_Sortedstatus);
         Ddo_referenciatecnica_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_descricao_Internalname, "SortedStatus", Ddo_referenciatecnica_descricao_Sortedstatus);
         Ddo_referenciatecnica_unidade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_unidade_Internalname, "SortedStatus", Ddo_referenciatecnica_unidade_Sortedstatus);
         Ddo_referenciatecnica_valor_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_valor_Internalname, "SortedStatus", Ddo_referenciatecnica_valor_Sortedstatus);
         Ddo_guia_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "SortedStatus", Ddo_guia_codigo_Sortedstatus);
         Ddo_guia_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "SortedStatus", Ddo_guia_nome_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_referenciatecnica_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_codigo_Internalname, "SortedStatus", Ddo_referenciatecnica_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_referenciatecnica_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_nome_Internalname, "SortedStatus", Ddo_referenciatecnica_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_referenciatecnica_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_descricao_Internalname, "SortedStatus", Ddo_referenciatecnica_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_referenciatecnica_unidade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_unidade_Internalname, "SortedStatus", Ddo_referenciatecnica_unidade_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_referenciatecnica_valor_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_valor_Internalname, "SortedStatus", Ddo_referenciatecnica_valor_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_guia_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "SortedStatus", Ddo_guia_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_guia_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "SortedStatus", Ddo_guia_nome_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavReferenciatecnica_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciatecnica_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciatecnica_nome1_Visible), 5, 0)));
         edtavGuia_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 )
         {
            edtavReferenciatecnica_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciatecnica_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciatecnica_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 )
         {
            edtavGuia_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavReferenciatecnica_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciatecnica_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciatecnica_nome2_Visible), 5, 0)));
         edtavGuia_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 )
         {
            edtavReferenciatecnica_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciatecnica_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciatecnica_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GUIA_NOME") == 0 )
         {
            edtavGuia_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavReferenciatecnica_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciatecnica_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciatecnica_nome3_Visible), 5, 0)));
         edtavGuia_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 )
         {
            edtavReferenciatecnica_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciatecnica_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciatecnica_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "GUIA_NOME") == 0 )
         {
            edtavGuia_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuia_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuia_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "REFERENCIATECNICA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22ReferenciaTecnica_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ReferenciaTecnica_Nome2", AV22ReferenciaTecnica_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "REFERENCIATECNICA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         AV27ReferenciaTecnica_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ReferenciaTecnica_Nome3", AV27ReferenciaTecnica_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV55TFReferenciaTecnica_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFReferenciaTecnica_Codigo), 6, 0)));
         Ddo_referenciatecnica_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_codigo_Internalname, "FilteredText_set", Ddo_referenciatecnica_codigo_Filteredtext_set);
         AV56TFReferenciaTecnica_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFReferenciaTecnica_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFReferenciaTecnica_Codigo_To), 6, 0)));
         Ddo_referenciatecnica_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_codigo_Internalname, "FilteredTextTo_set", Ddo_referenciatecnica_codigo_Filteredtextto_set);
         AV59TFReferenciaTecnica_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFReferenciaTecnica_Nome", AV59TFReferenciaTecnica_Nome);
         Ddo_referenciatecnica_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_nome_Internalname, "FilteredText_set", Ddo_referenciatecnica_nome_Filteredtext_set);
         AV60TFReferenciaTecnica_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFReferenciaTecnica_Nome_Sel", AV60TFReferenciaTecnica_Nome_Sel);
         Ddo_referenciatecnica_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_nome_Internalname, "SelectedValue_set", Ddo_referenciatecnica_nome_Selectedvalue_set);
         AV63TFReferenciaTecnica_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFReferenciaTecnica_Descricao", AV63TFReferenciaTecnica_Descricao);
         Ddo_referenciatecnica_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_descricao_Internalname, "FilteredText_set", Ddo_referenciatecnica_descricao_Filteredtext_set);
         AV64TFReferenciaTecnica_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFReferenciaTecnica_Descricao_Sel", AV64TFReferenciaTecnica_Descricao_Sel);
         Ddo_referenciatecnica_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_descricao_Internalname, "SelectedValue_set", Ddo_referenciatecnica_descricao_Selectedvalue_set);
         AV68TFReferenciaTecnica_Unidade_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_referenciatecnica_unidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_unidade_Internalname, "SelectedValue_set", Ddo_referenciatecnica_unidade_Selectedvalue_set);
         AV71TFReferenciaTecnica_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( AV71TFReferenciaTecnica_Valor, 18, 5)));
         Ddo_referenciatecnica_valor_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_valor_Internalname, "FilteredText_set", Ddo_referenciatecnica_valor_Filteredtext_set);
         AV72TFReferenciaTecnica_Valor_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFReferenciaTecnica_Valor_To", StringUtil.LTrim( StringUtil.Str( AV72TFReferenciaTecnica_Valor_To, 18, 5)));
         Ddo_referenciatecnica_valor_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciatecnica_valor_Internalname, "FilteredTextTo_set", Ddo_referenciatecnica_valor_Filteredtextto_set);
         AV75TFGuia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFGuia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75TFGuia_Codigo), 6, 0)));
         Ddo_guia_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "FilteredText_set", Ddo_guia_codigo_Filteredtext_set);
         AV76TFGuia_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFGuia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFGuia_Codigo_To), 6, 0)));
         Ddo_guia_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_codigo_Internalname, "FilteredTextTo_set", Ddo_guia_codigo_Filteredtextto_set);
         AV79TFGuia_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFGuia_Nome", AV79TFGuia_Nome);
         Ddo_guia_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "FilteredText_set", Ddo_guia_nome_Filteredtext_set);
         AV80TFGuia_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFGuia_Nome_Sel", AV80TFGuia_Nome_Sel);
         Ddo_guia_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_guia_nome_Internalname, "SelectedValue_set", Ddo_guia_nome_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "REFERENCIATECNICA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17ReferenciaTecnica_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaTecnica_Nome1", AV17ReferenciaTecnica_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ReferenciaTecnica_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaTecnica_Nome1", AV17ReferenciaTecnica_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18Guia_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Guia_Nome1", AV18Guia_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22ReferenciaTecnica_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ReferenciaTecnica_Nome2", AV22ReferenciaTecnica_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GUIA_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV23Guia_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Guia_Nome2", AV23Guia_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV27ReferenciaTecnica_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ReferenciaTecnica_Nome3", AV27ReferenciaTecnica_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "GUIA_NOME") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV28Guia_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Guia_Nome3", AV28Guia_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV55TFReferenciaTecnica_Codigo) && (0==AV56TFReferenciaTecnica_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIATECNICA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV55TFReferenciaTecnica_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV56TFReferenciaTecnica_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFReferenciaTecnica_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIATECNICA_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV59TFReferenciaTecnica_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFReferenciaTecnica_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIATECNICA_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV60TFReferenciaTecnica_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFReferenciaTecnica_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIATECNICA_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV63TFReferenciaTecnica_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFReferenciaTecnica_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIATECNICA_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV64TFReferenciaTecnica_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV68TFReferenciaTecnica_Unidade_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIATECNICA_UNIDADE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV68TFReferenciaTecnica_Unidade_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV71TFReferenciaTecnica_Valor) && (Convert.ToDecimal(0)==AV72TFReferenciaTecnica_Valor_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIATECNICA_VALOR";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV71TFReferenciaTecnica_Valor, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV72TFReferenciaTecnica_Valor_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV75TFGuia_Codigo) && (0==AV76TFGuia_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGUIA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV75TFGuia_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV76TFGuia_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFGuia_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGUIA_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV79TFGuia_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFGuia_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGUIA_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV80TFGuia_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV89Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ReferenciaTecnica_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ReferenciaTecnica_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Guia_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Guia_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ReferenciaTecnica_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22ReferenciaTecnica_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Guia_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23Guia_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27ReferenciaTecnica_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV27ReferenciaTecnica_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Guia_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV28Guia_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_5B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_5B2( true) ;
         }
         else
         {
            wb_table2_5_5B2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_5B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_77_5B2( true) ;
         }
         else
         {
            wb_table3_77_5B2( false) ;
         }
         return  ;
      }

      protected void wb_table3_77_5B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_5B2e( true) ;
         }
         else
         {
            wb_table1_2_5B2e( false) ;
         }
      }

      protected void wb_table3_77_5B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_80_5B2( true) ;
         }
         else
         {
            wb_table4_80_5B2( false) ;
         }
         return  ;
      }

      protected void wb_table4_80_5B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_77_5B2e( true) ;
         }
         else
         {
            wb_table3_77_5B2e( false) ;
         }
      }

      protected void wb_table4_80_5B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"83\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtReferenciaTecnica_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtReferenciaTecnica_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtReferenciaTecnica_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtReferenciaTecnica_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtReferenciaTecnica_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtReferenciaTecnica_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtReferenciaTecnica_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtReferenciaTecnica_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtReferenciaTecnica_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbReferenciaTecnica_Unidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbReferenciaTecnica_Unidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbReferenciaTecnica_Unidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtReferenciaTecnica_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtReferenciaTecnica_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtReferenciaTecnica_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGuia_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtGuia_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGuia_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGuia_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtGuia_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGuia_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtReferenciaTecnica_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaTecnica_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A98ReferenciaTecnica_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtReferenciaTecnica_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaTecnica_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A99ReferenciaTecnica_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtReferenciaTecnica_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaTecnica_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbReferenciaTecnica_Unidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbReferenciaTecnica_Unidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A100ReferenciaTecnica_Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtReferenciaTecnica_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaTecnica_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A93Guia_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGuia_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGuia_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A94Guia_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGuia_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGuia_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 83 )
         {
            wbEnd = 0;
            nRC_GXsfl_83 = (short)(nGXsfl_83_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_80_5B2e( true) ;
         }
         else
         {
            wb_table4_80_5B2e( false) ;
         }
      }

      protected void wb_table2_5_5B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptReferenciaTecnica.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_5B2( true) ;
         }
         else
         {
            wb_table5_14_5B2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_5B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_5B2e( true) ;
         }
         else
         {
            wb_table2_5_5B2e( false) ;
         }
      }

      protected void wb_table5_14_5B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_5B2( true) ;
         }
         else
         {
            wb_table6_19_5B2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_5B2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_5B2e( true) ;
         }
         else
         {
            wb_table5_14_5B2e( false) ;
         }
      }

      protected void wb_table6_19_5B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptReferenciaTecnica.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_5B2( true) ;
         }
         else
         {
            wb_table7_28_5B2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_5B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaTecnica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_PromptReferenciaTecnica.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_46_5B2( true) ;
         }
         else
         {
            wb_table8_46_5B2( false) ;
         }
         return  ;
      }

      protected void wb_table8_46_5B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaTecnica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_PromptReferenciaTecnica.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_64_5B2( true) ;
         }
         else
         {
            wb_table9_64_5B2( false) ;
         }
         return  ;
      }

      protected void wb_table9_64_5B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_5B2e( true) ;
         }
         else
         {
            wb_table6_19_5B2e( false) ;
         }
      }

      protected void wb_table9_64_5B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_PromptReferenciaTecnica.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavReferenciatecnica_nome3_Internalname, StringUtil.RTrim( AV27ReferenciaTecnica_Nome3), StringUtil.RTrim( context.localUtil.Format( AV27ReferenciaTecnica_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavReferenciatecnica_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavReferenciatecnica_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuia_nome3_Internalname, StringUtil.RTrim( AV28Guia_Nome3), StringUtil.RTrim( context.localUtil.Format( AV28Guia_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuia_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGuia_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_64_5B2e( true) ;
         }
         else
         {
            wb_table9_64_5B2e( false) ;
         }
      }

      protected void wb_table8_46_5B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_PromptReferenciaTecnica.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavReferenciatecnica_nome2_Internalname, StringUtil.RTrim( AV22ReferenciaTecnica_Nome2), StringUtil.RTrim( context.localUtil.Format( AV22ReferenciaTecnica_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavReferenciatecnica_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavReferenciatecnica_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuia_nome2_Internalname, StringUtil.RTrim( AV23Guia_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23Guia_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuia_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGuia_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_46_5B2e( true) ;
         }
         else
         {
            wb_table8_46_5B2e( false) ;
         }
      }

      protected void wb_table7_28_5B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptReferenciaTecnica.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavReferenciatecnica_nome1_Internalname, StringUtil.RTrim( AV17ReferenciaTecnica_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17ReferenciaTecnica_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavReferenciatecnica_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavReferenciatecnica_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaTecnica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuia_nome1_Internalname, StringUtil.RTrim( AV18Guia_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Guia_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuia_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGuia_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaTecnica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_5B2e( true) ;
         }
         else
         {
            wb_table7_28_5B2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutReferenciaTecnica_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutReferenciaTecnica_Codigo), 6, 0)));
         AV8InOutReferenciaTecnica_Nome = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutReferenciaTecnica_Nome", AV8InOutReferenciaTecnica_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA5B2( ) ;
         WS5B2( ) ;
         WE5B2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823142351");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptreferenciatecnica.js", "?202042823142351");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_832( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_83_idx;
         edtReferenciaTecnica_Codigo_Internalname = "REFERENCIATECNICA_CODIGO_"+sGXsfl_83_idx;
         edtReferenciaTecnica_Nome_Internalname = "REFERENCIATECNICA_NOME_"+sGXsfl_83_idx;
         edtReferenciaTecnica_Descricao_Internalname = "REFERENCIATECNICA_DESCRICAO_"+sGXsfl_83_idx;
         cmbReferenciaTecnica_Unidade_Internalname = "REFERENCIATECNICA_UNIDADE_"+sGXsfl_83_idx;
         edtReferenciaTecnica_Valor_Internalname = "REFERENCIATECNICA_VALOR_"+sGXsfl_83_idx;
         edtGuia_Codigo_Internalname = "GUIA_CODIGO_"+sGXsfl_83_idx;
         edtGuia_Nome_Internalname = "GUIA_NOME_"+sGXsfl_83_idx;
      }

      protected void SubsflControlProps_fel_832( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_83_fel_idx;
         edtReferenciaTecnica_Codigo_Internalname = "REFERENCIATECNICA_CODIGO_"+sGXsfl_83_fel_idx;
         edtReferenciaTecnica_Nome_Internalname = "REFERENCIATECNICA_NOME_"+sGXsfl_83_fel_idx;
         edtReferenciaTecnica_Descricao_Internalname = "REFERENCIATECNICA_DESCRICAO_"+sGXsfl_83_fel_idx;
         cmbReferenciaTecnica_Unidade_Internalname = "REFERENCIATECNICA_UNIDADE_"+sGXsfl_83_fel_idx;
         edtReferenciaTecnica_Valor_Internalname = "REFERENCIATECNICA_VALOR_"+sGXsfl_83_fel_idx;
         edtGuia_Codigo_Internalname = "GUIA_CODIGO_"+sGXsfl_83_fel_idx;
         edtGuia_Nome_Internalname = "GUIA_NOME_"+sGXsfl_83_fel_idx;
      }

      protected void sendrow_832( )
      {
         SubsflControlProps_832( ) ;
         WB5B0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_83_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_83_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_83_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 84,'',false,'',83)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV88Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV88Select_GXI : context.PathToRelativeUrl( AV31Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_83_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaTecnica_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A97ReferenciaTecnica_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaTecnica_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaTecnica_Nome_Internalname,StringUtil.RTrim( A98ReferenciaTecnica_Nome),StringUtil.RTrim( context.localUtil.Format( A98ReferenciaTecnica_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaTecnica_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaTecnica_Descricao_Internalname,(String)A99ReferenciaTecnica_Descricao,(String)A99ReferenciaTecnica_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaTecnica_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)83,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_83_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "REFERENCIATECNICA_UNIDADE_" + sGXsfl_83_idx;
               cmbReferenciaTecnica_Unidade.Name = GXCCtl;
               cmbReferenciaTecnica_Unidade.WebTags = "";
               cmbReferenciaTecnica_Unidade.addItem("1", "Percentual", 0);
               cmbReferenciaTecnica_Unidade.addItem("2", "Ponto Fun��o", 0);
               if ( cmbReferenciaTecnica_Unidade.ItemCount > 0 )
               {
                  A114ReferenciaTecnica_Unidade = (short)(NumberUtil.Val( cmbReferenciaTecnica_Unidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbReferenciaTecnica_Unidade,(String)cmbReferenciaTecnica_Unidade_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)),(short)1,(String)cmbReferenciaTecnica_Unidade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbReferenciaTecnica_Unidade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbReferenciaTecnica_Unidade_Internalname, "Values", (String)(cmbReferenciaTecnica_Unidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaTecnica_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A100ReferenciaTecnica_Valor, 18, 5, ",", "")),context.localUtil.Format( A100ReferenciaTecnica_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaTecnica_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGuia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A93Guia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGuia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGuia_Nome_Internalname,StringUtil.RTrim( A94Guia_Nome),StringUtil.RTrim( context.localUtil.Format( A94Guia_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGuia_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIATECNICA_CODIGO"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( (decimal)(A97ReferenciaTecnica_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIATECNICA_NOME"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, StringUtil.RTrim( context.localUtil.Format( A98ReferenciaTecnica_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIATECNICA_DESCRICAO"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, A99ReferenciaTecnica_Descricao));
            GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIATECNICA_UNIDADE"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( (decimal)(A114ReferenciaTecnica_Unidade), "Z9")));
            GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIATECNICA_VALOR"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( A100ReferenciaTecnica_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_GUIA_CODIGO"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         /* End function sendrow_832 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavReferenciatecnica_nome1_Internalname = "vREFERENCIATECNICA_NOME1";
         edtavGuia_nome1_Internalname = "vGUIA_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavReferenciatecnica_nome2_Internalname = "vREFERENCIATECNICA_NOME2";
         edtavGuia_nome2_Internalname = "vGUIA_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavReferenciatecnica_nome3_Internalname = "vREFERENCIATECNICA_NOME3";
         edtavGuia_nome3_Internalname = "vGUIA_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtReferenciaTecnica_Codigo_Internalname = "REFERENCIATECNICA_CODIGO";
         edtReferenciaTecnica_Nome_Internalname = "REFERENCIATECNICA_NOME";
         edtReferenciaTecnica_Descricao_Internalname = "REFERENCIATECNICA_DESCRICAO";
         cmbReferenciaTecnica_Unidade_Internalname = "REFERENCIATECNICA_UNIDADE";
         edtReferenciaTecnica_Valor_Internalname = "REFERENCIATECNICA_VALOR";
         edtGuia_Codigo_Internalname = "GUIA_CODIGO";
         edtGuia_Nome_Internalname = "GUIA_NOME";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfreferenciatecnica_codigo_Internalname = "vTFREFERENCIATECNICA_CODIGO";
         edtavTfreferenciatecnica_codigo_to_Internalname = "vTFREFERENCIATECNICA_CODIGO_TO";
         edtavTfreferenciatecnica_nome_Internalname = "vTFREFERENCIATECNICA_NOME";
         edtavTfreferenciatecnica_nome_sel_Internalname = "vTFREFERENCIATECNICA_NOME_SEL";
         edtavTfreferenciatecnica_descricao_Internalname = "vTFREFERENCIATECNICA_DESCRICAO";
         edtavTfreferenciatecnica_descricao_sel_Internalname = "vTFREFERENCIATECNICA_DESCRICAO_SEL";
         edtavTfreferenciatecnica_valor_Internalname = "vTFREFERENCIATECNICA_VALOR";
         edtavTfreferenciatecnica_valor_to_Internalname = "vTFREFERENCIATECNICA_VALOR_TO";
         edtavTfguia_codigo_Internalname = "vTFGUIA_CODIGO";
         edtavTfguia_codigo_to_Internalname = "vTFGUIA_CODIGO_TO";
         edtavTfguia_nome_Internalname = "vTFGUIA_NOME";
         edtavTfguia_nome_sel_Internalname = "vTFGUIA_NOME_SEL";
         Ddo_referenciatecnica_codigo_Internalname = "DDO_REFERENCIATECNICA_CODIGO";
         edtavDdo_referenciatecnica_codigotitlecontrolidtoreplace_Internalname = "vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_referenciatecnica_nome_Internalname = "DDO_REFERENCIATECNICA_NOME";
         edtavDdo_referenciatecnica_nometitlecontrolidtoreplace_Internalname = "vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE";
         Ddo_referenciatecnica_descricao_Internalname = "DDO_REFERENCIATECNICA_DESCRICAO";
         edtavDdo_referenciatecnica_descricaotitlecontrolidtoreplace_Internalname = "vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_referenciatecnica_unidade_Internalname = "DDO_REFERENCIATECNICA_UNIDADE";
         edtavDdo_referenciatecnica_unidadetitlecontrolidtoreplace_Internalname = "vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE";
         Ddo_referenciatecnica_valor_Internalname = "DDO_REFERENCIATECNICA_VALOR";
         edtavDdo_referenciatecnica_valortitlecontrolidtoreplace_Internalname = "vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE";
         Ddo_guia_codigo_Internalname = "DDO_GUIA_CODIGO";
         edtavDdo_guia_codigotitlecontrolidtoreplace_Internalname = "vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_guia_nome_Internalname = "DDO_GUIA_NOME";
         edtavDdo_guia_nometitlecontrolidtoreplace_Internalname = "vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtGuia_Nome_Jsonclick = "";
         edtGuia_Codigo_Jsonclick = "";
         edtReferenciaTecnica_Valor_Jsonclick = "";
         cmbReferenciaTecnica_Unidade_Jsonclick = "";
         edtReferenciaTecnica_Descricao_Jsonclick = "";
         edtReferenciaTecnica_Nome_Jsonclick = "";
         edtReferenciaTecnica_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavGuia_nome1_Jsonclick = "";
         edtavReferenciatecnica_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavGuia_nome2_Jsonclick = "";
         edtavReferenciatecnica_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavGuia_nome3_Jsonclick = "";
         edtavReferenciatecnica_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtGuia_Nome_Titleformat = 0;
         edtGuia_Codigo_Titleformat = 0;
         edtReferenciaTecnica_Valor_Titleformat = 0;
         cmbReferenciaTecnica_Unidade_Titleformat = 0;
         edtReferenciaTecnica_Descricao_Titleformat = 0;
         edtReferenciaTecnica_Nome_Titleformat = 0;
         edtReferenciaTecnica_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavGuia_nome3_Visible = 1;
         edtavReferenciatecnica_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavGuia_nome2_Visible = 1;
         edtavReferenciatecnica_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavGuia_nome1_Visible = 1;
         edtavReferenciatecnica_nome1_Visible = 1;
         edtGuia_Nome_Title = "Nome";
         edtGuia_Codigo_Title = "C�digo";
         edtReferenciaTecnica_Valor_Title = "Valor";
         cmbReferenciaTecnica_Unidade.Title.Text = "Unidade";
         edtReferenciaTecnica_Descricao_Title = "Descri��o";
         edtReferenciaTecnica_Nome_Title = "da Refer�ncia";
         edtReferenciaTecnica_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_guia_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_guia_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_referenciatecnica_valortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_referenciatecnica_unidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_referenciatecnica_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_referenciatecnica_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_referenciatecnica_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfguia_nome_sel_Jsonclick = "";
         edtavTfguia_nome_sel_Visible = 1;
         edtavTfguia_nome_Jsonclick = "";
         edtavTfguia_nome_Visible = 1;
         edtavTfguia_codigo_to_Jsonclick = "";
         edtavTfguia_codigo_to_Visible = 1;
         edtavTfguia_codigo_Jsonclick = "";
         edtavTfguia_codigo_Visible = 1;
         edtavTfreferenciatecnica_valor_to_Jsonclick = "";
         edtavTfreferenciatecnica_valor_to_Visible = 1;
         edtavTfreferenciatecnica_valor_Jsonclick = "";
         edtavTfreferenciatecnica_valor_Visible = 1;
         edtavTfreferenciatecnica_descricao_sel_Visible = 1;
         edtavTfreferenciatecnica_descricao_Visible = 1;
         edtavTfreferenciatecnica_nome_sel_Jsonclick = "";
         edtavTfreferenciatecnica_nome_sel_Visible = 1;
         edtavTfreferenciatecnica_nome_Jsonclick = "";
         edtavTfreferenciatecnica_nome_Visible = 1;
         edtavTfreferenciatecnica_codigo_to_Jsonclick = "";
         edtavTfreferenciatecnica_codigo_to_Visible = 1;
         edtavTfreferenciatecnica_codigo_Jsonclick = "";
         edtavTfreferenciatecnica_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_guia_nome_Searchbuttontext = "Pesquisar";
         Ddo_guia_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_guia_nome_Rangefilterto = "At�";
         Ddo_guia_nome_Rangefilterfrom = "Desde";
         Ddo_guia_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_guia_nome_Loadingdata = "Carregando dados...";
         Ddo_guia_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_guia_nome_Sortasc = "Ordenar de A � Z";
         Ddo_guia_nome_Datalistupdateminimumcharacters = 0;
         Ddo_guia_nome_Datalistproc = "GetPromptReferenciaTecnicaFilterData";
         Ddo_guia_nome_Datalistfixedvalues = "";
         Ddo_guia_nome_Datalisttype = "Dynamic";
         Ddo_guia_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_guia_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_guia_nome_Filtertype = "Character";
         Ddo_guia_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_guia_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_guia_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_guia_nome_Titlecontrolidtoreplace = "";
         Ddo_guia_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_guia_nome_Cls = "ColumnSettings";
         Ddo_guia_nome_Tooltip = "Op��es";
         Ddo_guia_nome_Caption = "";
         Ddo_guia_codigo_Searchbuttontext = "Pesquisar";
         Ddo_guia_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_guia_codigo_Rangefilterto = "At�";
         Ddo_guia_codigo_Rangefilterfrom = "Desde";
         Ddo_guia_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_guia_codigo_Loadingdata = "Carregando dados...";
         Ddo_guia_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_guia_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_guia_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_guia_codigo_Datalistfixedvalues = "";
         Ddo_guia_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_guia_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_guia_codigo_Filtertype = "Numeric";
         Ddo_guia_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_guia_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_guia_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_guia_codigo_Titlecontrolidtoreplace = "";
         Ddo_guia_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_guia_codigo_Cls = "ColumnSettings";
         Ddo_guia_codigo_Tooltip = "Op��es";
         Ddo_guia_codigo_Caption = "";
         Ddo_referenciatecnica_valor_Searchbuttontext = "Pesquisar";
         Ddo_referenciatecnica_valor_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_referenciatecnica_valor_Rangefilterto = "At�";
         Ddo_referenciatecnica_valor_Rangefilterfrom = "Desde";
         Ddo_referenciatecnica_valor_Cleanfilter = "Limpar pesquisa";
         Ddo_referenciatecnica_valor_Loadingdata = "Carregando dados...";
         Ddo_referenciatecnica_valor_Sortdsc = "Ordenar de Z � A";
         Ddo_referenciatecnica_valor_Sortasc = "Ordenar de A � Z";
         Ddo_referenciatecnica_valor_Datalistupdateminimumcharacters = 0;
         Ddo_referenciatecnica_valor_Datalistfixedvalues = "";
         Ddo_referenciatecnica_valor_Includedatalist = Convert.ToBoolean( 0);
         Ddo_referenciatecnica_valor_Filterisrange = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_valor_Filtertype = "Numeric";
         Ddo_referenciatecnica_valor_Includefilter = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_valor_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_valor_Includesortasc = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_valor_Titlecontrolidtoreplace = "";
         Ddo_referenciatecnica_valor_Dropdownoptionstype = "GridTitleSettings";
         Ddo_referenciatecnica_valor_Cls = "ColumnSettings";
         Ddo_referenciatecnica_valor_Tooltip = "Op��es";
         Ddo_referenciatecnica_valor_Caption = "";
         Ddo_referenciatecnica_unidade_Searchbuttontext = "Filtrar Selecionados";
         Ddo_referenciatecnica_unidade_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_referenciatecnica_unidade_Rangefilterto = "At�";
         Ddo_referenciatecnica_unidade_Rangefilterfrom = "Desde";
         Ddo_referenciatecnica_unidade_Cleanfilter = "Limpar pesquisa";
         Ddo_referenciatecnica_unidade_Loadingdata = "Carregando dados...";
         Ddo_referenciatecnica_unidade_Sortdsc = "Ordenar de Z � A";
         Ddo_referenciatecnica_unidade_Sortasc = "Ordenar de A � Z";
         Ddo_referenciatecnica_unidade_Datalistupdateminimumcharacters = 0;
         Ddo_referenciatecnica_unidade_Datalistfixedvalues = "1:Percentual,2:Ponto Fun��o";
         Ddo_referenciatecnica_unidade_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_unidade_Datalisttype = "FixedValues";
         Ddo_referenciatecnica_unidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_unidade_Filterisrange = Convert.ToBoolean( 0);
         Ddo_referenciatecnica_unidade_Includefilter = Convert.ToBoolean( 0);
         Ddo_referenciatecnica_unidade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_unidade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_unidade_Titlecontrolidtoreplace = "";
         Ddo_referenciatecnica_unidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_referenciatecnica_unidade_Cls = "ColumnSettings";
         Ddo_referenciatecnica_unidade_Tooltip = "Op��es";
         Ddo_referenciatecnica_unidade_Caption = "";
         Ddo_referenciatecnica_descricao_Searchbuttontext = "Pesquisar";
         Ddo_referenciatecnica_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_referenciatecnica_descricao_Rangefilterto = "At�";
         Ddo_referenciatecnica_descricao_Rangefilterfrom = "Desde";
         Ddo_referenciatecnica_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_referenciatecnica_descricao_Loadingdata = "Carregando dados...";
         Ddo_referenciatecnica_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_referenciatecnica_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_referenciatecnica_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_referenciatecnica_descricao_Datalistproc = "GetPromptReferenciaTecnicaFilterData";
         Ddo_referenciatecnica_descricao_Datalistfixedvalues = "";
         Ddo_referenciatecnica_descricao_Datalisttype = "Dynamic";
         Ddo_referenciatecnica_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_referenciatecnica_descricao_Filtertype = "Character";
         Ddo_referenciatecnica_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_descricao_Titlecontrolidtoreplace = "";
         Ddo_referenciatecnica_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_referenciatecnica_descricao_Cls = "ColumnSettings";
         Ddo_referenciatecnica_descricao_Tooltip = "Op��es";
         Ddo_referenciatecnica_descricao_Caption = "";
         Ddo_referenciatecnica_nome_Searchbuttontext = "Pesquisar";
         Ddo_referenciatecnica_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_referenciatecnica_nome_Rangefilterto = "At�";
         Ddo_referenciatecnica_nome_Rangefilterfrom = "Desde";
         Ddo_referenciatecnica_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_referenciatecnica_nome_Loadingdata = "Carregando dados...";
         Ddo_referenciatecnica_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_referenciatecnica_nome_Sortasc = "Ordenar de A � Z";
         Ddo_referenciatecnica_nome_Datalistupdateminimumcharacters = 0;
         Ddo_referenciatecnica_nome_Datalistproc = "GetPromptReferenciaTecnicaFilterData";
         Ddo_referenciatecnica_nome_Datalistfixedvalues = "";
         Ddo_referenciatecnica_nome_Datalisttype = "Dynamic";
         Ddo_referenciatecnica_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_referenciatecnica_nome_Filtertype = "Character";
         Ddo_referenciatecnica_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_nome_Titlecontrolidtoreplace = "";
         Ddo_referenciatecnica_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_referenciatecnica_nome_Cls = "ColumnSettings";
         Ddo_referenciatecnica_nome_Tooltip = "Op��es";
         Ddo_referenciatecnica_nome_Caption = "";
         Ddo_referenciatecnica_codigo_Searchbuttontext = "Pesquisar";
         Ddo_referenciatecnica_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_referenciatecnica_codigo_Rangefilterto = "At�";
         Ddo_referenciatecnica_codigo_Rangefilterfrom = "Desde";
         Ddo_referenciatecnica_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_referenciatecnica_codigo_Loadingdata = "Carregando dados...";
         Ddo_referenciatecnica_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_referenciatecnica_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_referenciatecnica_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_referenciatecnica_codigo_Datalistfixedvalues = "";
         Ddo_referenciatecnica_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_referenciatecnica_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_codigo_Filtertype = "Numeric";
         Ddo_referenciatecnica_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_referenciatecnica_codigo_Titlecontrolidtoreplace = "";
         Ddo_referenciatecnica_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_referenciatecnica_codigo_Cls = "ColumnSettings";
         Ddo_referenciatecnica_codigo_Tooltip = "Op��es";
         Ddo_referenciatecnica_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Refer�ncia T�cnica";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''}],oparms:[{av:'AV54ReferenciaTecnica_CodigoTitleFilterData',fld:'vREFERENCIATECNICA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58ReferenciaTecnica_NomeTitleFilterData',fld:'vREFERENCIATECNICA_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV62ReferenciaTecnica_DescricaoTitleFilterData',fld:'vREFERENCIATECNICA_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV66ReferenciaTecnica_UnidadeTitleFilterData',fld:'vREFERENCIATECNICA_UNIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV70ReferenciaTecnica_ValorTitleFilterData',fld:'vREFERENCIATECNICA_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'AV74Guia_CodigoTitleFilterData',fld:'vGUIA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV78Guia_NomeTitleFilterData',fld:'vGUIA_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtReferenciaTecnica_Codigo_Titleformat',ctrl:'REFERENCIATECNICA_CODIGO',prop:'Titleformat'},{av:'edtReferenciaTecnica_Codigo_Title',ctrl:'REFERENCIATECNICA_CODIGO',prop:'Title'},{av:'edtReferenciaTecnica_Nome_Titleformat',ctrl:'REFERENCIATECNICA_NOME',prop:'Titleformat'},{av:'edtReferenciaTecnica_Nome_Title',ctrl:'REFERENCIATECNICA_NOME',prop:'Title'},{av:'edtReferenciaTecnica_Descricao_Titleformat',ctrl:'REFERENCIATECNICA_DESCRICAO',prop:'Titleformat'},{av:'edtReferenciaTecnica_Descricao_Title',ctrl:'REFERENCIATECNICA_DESCRICAO',prop:'Title'},{av:'cmbReferenciaTecnica_Unidade'},{av:'edtReferenciaTecnica_Valor_Titleformat',ctrl:'REFERENCIATECNICA_VALOR',prop:'Titleformat'},{av:'edtReferenciaTecnica_Valor_Title',ctrl:'REFERENCIATECNICA_VALOR',prop:'Title'},{av:'edtGuia_Codigo_Titleformat',ctrl:'GUIA_CODIGO',prop:'Titleformat'},{av:'edtGuia_Codigo_Title',ctrl:'GUIA_CODIGO',prop:'Title'},{av:'edtGuia_Nome_Titleformat',ctrl:'GUIA_NOME',prop:'Titleformat'},{av:'edtGuia_Nome_Title',ctrl:'GUIA_NOME',prop:'Title'},{av:'AV84GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV85GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E115B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_REFERENCIATECNICA_CODIGO.ONOPTIONCLICKED","{handler:'E125B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_referenciatecnica_codigo_Activeeventkey',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_referenciatecnica_codigo_Filteredtext_get',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_referenciatecnica_codigo_Filteredtextto_get',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_referenciatecnica_codigo_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'SortedStatus'},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_referenciatecnica_nome_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_descricao_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_unidade_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_UNIDADE',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_valor_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'SortedStatus'},{av:'Ddo_guia_codigo_Sortedstatus',ctrl:'DDO_GUIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_guia_nome_Sortedstatus',ctrl:'DDO_GUIA_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REFERENCIATECNICA_NOME.ONOPTIONCLICKED","{handler:'E135B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_referenciatecnica_nome_Activeeventkey',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'ActiveEventKey'},{av:'Ddo_referenciatecnica_nome_Filteredtext_get',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'FilteredText_get'},{av:'Ddo_referenciatecnica_nome_Selectedvalue_get',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_referenciatecnica_nome_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'SortedStatus'},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_referenciatecnica_codigo_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_descricao_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_unidade_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_UNIDADE',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_valor_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'SortedStatus'},{av:'Ddo_guia_codigo_Sortedstatus',ctrl:'DDO_GUIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_guia_nome_Sortedstatus',ctrl:'DDO_GUIA_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REFERENCIATECNICA_DESCRICAO.ONOPTIONCLICKED","{handler:'E145B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_referenciatecnica_descricao_Activeeventkey',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_referenciatecnica_descricao_Filteredtext_get',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_referenciatecnica_descricao_Selectedvalue_get',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_referenciatecnica_descricao_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'SortedStatus'},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_referenciatecnica_codigo_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_nome_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_unidade_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_UNIDADE',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_valor_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'SortedStatus'},{av:'Ddo_guia_codigo_Sortedstatus',ctrl:'DDO_GUIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_guia_nome_Sortedstatus',ctrl:'DDO_GUIA_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REFERENCIATECNICA_UNIDADE.ONOPTIONCLICKED","{handler:'E155B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_referenciatecnica_unidade_Activeeventkey',ctrl:'DDO_REFERENCIATECNICA_UNIDADE',prop:'ActiveEventKey'},{av:'Ddo_referenciatecnica_unidade_Selectedvalue_get',ctrl:'DDO_REFERENCIATECNICA_UNIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_referenciatecnica_unidade_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_UNIDADE',prop:'SortedStatus'},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'Ddo_referenciatecnica_codigo_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_nome_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_descricao_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_valor_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'SortedStatus'},{av:'Ddo_guia_codigo_Sortedstatus',ctrl:'DDO_GUIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_guia_nome_Sortedstatus',ctrl:'DDO_GUIA_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REFERENCIATECNICA_VALOR.ONOPTIONCLICKED","{handler:'E165B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_referenciatecnica_valor_Activeeventkey',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'ActiveEventKey'},{av:'Ddo_referenciatecnica_valor_Filteredtext_get',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'FilteredText_get'},{av:'Ddo_referenciatecnica_valor_Filteredtextto_get',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_referenciatecnica_valor_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'SortedStatus'},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_referenciatecnica_codigo_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_nome_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_descricao_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_unidade_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_UNIDADE',prop:'SortedStatus'},{av:'Ddo_guia_codigo_Sortedstatus',ctrl:'DDO_GUIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_guia_nome_Sortedstatus',ctrl:'DDO_GUIA_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_GUIA_CODIGO.ONOPTIONCLICKED","{handler:'E175B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_guia_codigo_Activeeventkey',ctrl:'DDO_GUIA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_guia_codigo_Filteredtext_get',ctrl:'DDO_GUIA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_guia_codigo_Filteredtextto_get',ctrl:'DDO_GUIA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_guia_codigo_Sortedstatus',ctrl:'DDO_GUIA_CODIGO',prop:'SortedStatus'},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_referenciatecnica_codigo_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_nome_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_descricao_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_unidade_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_UNIDADE',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_valor_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'SortedStatus'},{av:'Ddo_guia_nome_Sortedstatus',ctrl:'DDO_GUIA_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_GUIA_NOME.ONOPTIONCLICKED","{handler:'E185B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_guia_nome_Activeeventkey',ctrl:'DDO_GUIA_NOME',prop:'ActiveEventKey'},{av:'Ddo_guia_nome_Filteredtext_get',ctrl:'DDO_GUIA_NOME',prop:'FilteredText_get'},{av:'Ddo_guia_nome_Selectedvalue_get',ctrl:'DDO_GUIA_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_guia_nome_Sortedstatus',ctrl:'DDO_GUIA_NOME',prop:'SortedStatus'},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_referenciatecnica_codigo_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_nome_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_descricao_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_unidade_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_UNIDADE',prop:'SortedStatus'},{av:'Ddo_referenciatecnica_valor_Sortedstatus',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'SortedStatus'},{av:'Ddo_guia_codigo_Sortedstatus',ctrl:'DDO_GUIA_CODIGO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E315B2',iparms:[],oparms:[{av:'AV31Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E325B2',iparms:[{av:'A97ReferenciaTecnica_Codigo',fld:'REFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A98ReferenciaTecnica_Nome',fld:'REFERENCIATECNICA_NOME',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutReferenciaTecnica_Codigo',fld:'vINOUTREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutReferenciaTecnica_Nome',fld:'vINOUTREFERENCIATECNICA_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E195B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E245B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E205B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'edtavReferenciatecnica_nome2_Visible',ctrl:'vREFERENCIATECNICA_NOME2',prop:'Visible'},{av:'edtavGuia_nome2_Visible',ctrl:'vGUIA_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavReferenciatecnica_nome3_Visible',ctrl:'vREFERENCIATECNICA_NOME3',prop:'Visible'},{av:'edtavGuia_nome3_Visible',ctrl:'vGUIA_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavReferenciatecnica_nome1_Visible',ctrl:'vREFERENCIATECNICA_NOME1',prop:'Visible'},{av:'edtavGuia_nome1_Visible',ctrl:'vGUIA_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E255B2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavReferenciatecnica_nome1_Visible',ctrl:'vREFERENCIATECNICA_NOME1',prop:'Visible'},{av:'edtavGuia_nome1_Visible',ctrl:'vGUIA_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E265B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E215B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'edtavReferenciatecnica_nome2_Visible',ctrl:'vREFERENCIATECNICA_NOME2',prop:'Visible'},{av:'edtavGuia_nome2_Visible',ctrl:'vGUIA_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavReferenciatecnica_nome3_Visible',ctrl:'vREFERENCIATECNICA_NOME3',prop:'Visible'},{av:'edtavGuia_nome3_Visible',ctrl:'vGUIA_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavReferenciatecnica_nome1_Visible',ctrl:'vREFERENCIATECNICA_NOME1',prop:'Visible'},{av:'edtavGuia_nome1_Visible',ctrl:'vGUIA_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E275B2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavReferenciatecnica_nome2_Visible',ctrl:'vREFERENCIATECNICA_NOME2',prop:'Visible'},{av:'edtavGuia_nome2_Visible',ctrl:'vGUIA_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E225B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'edtavReferenciatecnica_nome2_Visible',ctrl:'vREFERENCIATECNICA_NOME2',prop:'Visible'},{av:'edtavGuia_nome2_Visible',ctrl:'vGUIA_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavReferenciatecnica_nome3_Visible',ctrl:'vREFERENCIATECNICA_NOME3',prop:'Visible'},{av:'edtavGuia_nome3_Visible',ctrl:'vGUIA_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavReferenciatecnica_nome1_Visible',ctrl:'vREFERENCIATECNICA_NOME1',prop:'Visible'},{av:'edtavGuia_nome1_Visible',ctrl:'vGUIA_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E285B2',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavReferenciatecnica_nome3_Visible',ctrl:'vREFERENCIATECNICA_NOME3',prop:'Visible'},{av:'edtavGuia_nome3_Visible',ctrl:'vGUIA_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E235B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_UNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace',fld:'vDDO_REFERENCIATECNICA_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Guia_CodigoTitleControlIdToReplace',fld:'vDDO_GUIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Guia_NomeTitleControlIdToReplace',fld:'vDDO_GUIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'AV89Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV55TFReferenciaTecnica_Codigo',fld:'vTFREFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_referenciatecnica_codigo_Filteredtext_set',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'FilteredText_set'},{av:'AV56TFReferenciaTecnica_Codigo_To',fld:'vTFREFERENCIATECNICA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_referenciatecnica_codigo_Filteredtextto_set',ctrl:'DDO_REFERENCIATECNICA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV59TFReferenciaTecnica_Nome',fld:'vTFREFERENCIATECNICA_NOME',pic:'@!',nv:''},{av:'Ddo_referenciatecnica_nome_Filteredtext_set',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'FilteredText_set'},{av:'AV60TFReferenciaTecnica_Nome_Sel',fld:'vTFREFERENCIATECNICA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_referenciatecnica_nome_Selectedvalue_set',ctrl:'DDO_REFERENCIATECNICA_NOME',prop:'SelectedValue_set'},{av:'AV63TFReferenciaTecnica_Descricao',fld:'vTFREFERENCIATECNICA_DESCRICAO',pic:'',nv:''},{av:'Ddo_referenciatecnica_descricao_Filteredtext_set',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'FilteredText_set'},{av:'AV64TFReferenciaTecnica_Descricao_Sel',fld:'vTFREFERENCIATECNICA_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_referenciatecnica_descricao_Selectedvalue_set',ctrl:'DDO_REFERENCIATECNICA_DESCRICAO',prop:'SelectedValue_set'},{av:'AV68TFReferenciaTecnica_Unidade_Sels',fld:'vTFREFERENCIATECNICA_UNIDADE_SELS',pic:'',nv:null},{av:'Ddo_referenciatecnica_unidade_Selectedvalue_set',ctrl:'DDO_REFERENCIATECNICA_UNIDADE',prop:'SelectedValue_set'},{av:'AV71TFReferenciaTecnica_Valor',fld:'vTFREFERENCIATECNICA_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_referenciatecnica_valor_Filteredtext_set',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'FilteredText_set'},{av:'AV72TFReferenciaTecnica_Valor_To',fld:'vTFREFERENCIATECNICA_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_referenciatecnica_valor_Filteredtextto_set',ctrl:'DDO_REFERENCIATECNICA_VALOR',prop:'FilteredTextTo_set'},{av:'AV75TFGuia_Codigo',fld:'vTFGUIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_guia_codigo_Filteredtext_set',ctrl:'DDO_GUIA_CODIGO',prop:'FilteredText_set'},{av:'AV76TFGuia_Codigo_To',fld:'vTFGUIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_guia_codigo_Filteredtextto_set',ctrl:'DDO_GUIA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV79TFGuia_Nome',fld:'vTFGUIA_NOME',pic:'@!',nv:''},{av:'Ddo_guia_nome_Filteredtext_set',ctrl:'DDO_GUIA_NOME',prop:'FilteredText_set'},{av:'AV80TFGuia_Nome_Sel',fld:'vTFGUIA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_guia_nome_Selectedvalue_set',ctrl:'DDO_GUIA_NOME',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ReferenciaTecnica_Nome1',fld:'vREFERENCIATECNICA_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavReferenciatecnica_nome1_Visible',ctrl:'vREFERENCIATECNICA_NOME1',prop:'Visible'},{av:'edtavGuia_nome1_Visible',ctrl:'vGUIA_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ReferenciaTecnica_Nome2',fld:'vREFERENCIATECNICA_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ReferenciaTecnica_Nome3',fld:'vREFERENCIATECNICA_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18Guia_Nome1',fld:'vGUIA_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Guia_Nome2',fld:'vGUIA_NOME2',pic:'@!',nv:''},{av:'AV28Guia_Nome3',fld:'vGUIA_NOME3',pic:'@!',nv:''},{av:'edtavReferenciatecnica_nome2_Visible',ctrl:'vREFERENCIATECNICA_NOME2',prop:'Visible'},{av:'edtavGuia_nome2_Visible',ctrl:'vGUIA_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavReferenciatecnica_nome3_Visible',ctrl:'vREFERENCIATECNICA_NOME3',prop:'Visible'},{av:'edtavGuia_nome3_Visible',ctrl:'vGUIA_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutReferenciaTecnica_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_referenciatecnica_codigo_Activeeventkey = "";
         Ddo_referenciatecnica_codigo_Filteredtext_get = "";
         Ddo_referenciatecnica_codigo_Filteredtextto_get = "";
         Ddo_referenciatecnica_nome_Activeeventkey = "";
         Ddo_referenciatecnica_nome_Filteredtext_get = "";
         Ddo_referenciatecnica_nome_Selectedvalue_get = "";
         Ddo_referenciatecnica_descricao_Activeeventkey = "";
         Ddo_referenciatecnica_descricao_Filteredtext_get = "";
         Ddo_referenciatecnica_descricao_Selectedvalue_get = "";
         Ddo_referenciatecnica_unidade_Activeeventkey = "";
         Ddo_referenciatecnica_unidade_Selectedvalue_get = "";
         Ddo_referenciatecnica_valor_Activeeventkey = "";
         Ddo_referenciatecnica_valor_Filteredtext_get = "";
         Ddo_referenciatecnica_valor_Filteredtextto_get = "";
         Ddo_guia_codigo_Activeeventkey = "";
         Ddo_guia_codigo_Filteredtext_get = "";
         Ddo_guia_codigo_Filteredtextto_get = "";
         Ddo_guia_nome_Activeeventkey = "";
         Ddo_guia_nome_Filteredtext_get = "";
         Ddo_guia_nome_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ReferenciaTecnica_Nome1 = "";
         AV18Guia_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22ReferenciaTecnica_Nome2 = "";
         AV23Guia_Nome2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV27ReferenciaTecnica_Nome3 = "";
         AV28Guia_Nome3 = "";
         AV59TFReferenciaTecnica_Nome = "";
         AV60TFReferenciaTecnica_Nome_Sel = "";
         AV63TFReferenciaTecnica_Descricao = "";
         AV64TFReferenciaTecnica_Descricao_Sel = "";
         AV79TFGuia_Nome = "";
         AV80TFGuia_Nome_Sel = "";
         AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace = "";
         AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace = "";
         AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace = "";
         AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace = "";
         AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace = "";
         AV77ddo_Guia_CodigoTitleControlIdToReplace = "";
         AV81ddo_Guia_NomeTitleControlIdToReplace = "";
         AV68TFReferenciaTecnica_Unidade_Sels = new GxSimpleCollection();
         AV89Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV82DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV54ReferenciaTecnica_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ReferenciaTecnica_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ReferenciaTecnica_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66ReferenciaTecnica_UnidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70ReferenciaTecnica_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74Guia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78Guia_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_referenciatecnica_codigo_Filteredtext_set = "";
         Ddo_referenciatecnica_codigo_Filteredtextto_set = "";
         Ddo_referenciatecnica_codigo_Sortedstatus = "";
         Ddo_referenciatecnica_nome_Filteredtext_set = "";
         Ddo_referenciatecnica_nome_Selectedvalue_set = "";
         Ddo_referenciatecnica_nome_Sortedstatus = "";
         Ddo_referenciatecnica_descricao_Filteredtext_set = "";
         Ddo_referenciatecnica_descricao_Selectedvalue_set = "";
         Ddo_referenciatecnica_descricao_Sortedstatus = "";
         Ddo_referenciatecnica_unidade_Selectedvalue_set = "";
         Ddo_referenciatecnica_unidade_Sortedstatus = "";
         Ddo_referenciatecnica_valor_Filteredtext_set = "";
         Ddo_referenciatecnica_valor_Filteredtextto_set = "";
         Ddo_referenciatecnica_valor_Sortedstatus = "";
         Ddo_guia_codigo_Filteredtext_set = "";
         Ddo_guia_codigo_Filteredtextto_set = "";
         Ddo_guia_codigo_Sortedstatus = "";
         Ddo_guia_nome_Filteredtext_set = "";
         Ddo_guia_nome_Selectedvalue_set = "";
         Ddo_guia_nome_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Select = "";
         AV88Select_GXI = "";
         A98ReferenciaTecnica_Nome = "";
         A99ReferenciaTecnica_Descricao = "";
         A94Guia_Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17ReferenciaTecnica_Nome1 = "";
         lV18Guia_Nome1 = "";
         lV22ReferenciaTecnica_Nome2 = "";
         lV23Guia_Nome2 = "";
         lV27ReferenciaTecnica_Nome3 = "";
         lV28Guia_Nome3 = "";
         lV59TFReferenciaTecnica_Nome = "";
         lV63TFReferenciaTecnica_Descricao = "";
         lV79TFGuia_Nome = "";
         H005B2_A94Guia_Nome = new String[] {""} ;
         H005B2_A93Guia_Codigo = new int[1] ;
         H005B2_A100ReferenciaTecnica_Valor = new decimal[1] ;
         H005B2_A114ReferenciaTecnica_Unidade = new short[1] ;
         H005B2_A99ReferenciaTecnica_Descricao = new String[] {""} ;
         H005B2_A98ReferenciaTecnica_Nome = new String[] {""} ;
         H005B2_A97ReferenciaTecnica_Codigo = new int[1] ;
         H005B3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV67TFReferenciaTecnica_Unidade_SelsJson = "";
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptreferenciatecnica__default(),
            new Object[][] {
                new Object[] {
               H005B2_A94Guia_Nome, H005B2_A93Guia_Codigo, H005B2_A100ReferenciaTecnica_Valor, H005B2_A114ReferenciaTecnica_Unidade, H005B2_A99ReferenciaTecnica_Descricao, H005B2_A98ReferenciaTecnica_Nome, H005B2_A97ReferenciaTecnica_Codigo
               }
               , new Object[] {
               H005B3_AGRID_nRecordCount
               }
            }
         );
         AV89Pgmname = "PromptReferenciaTecnica";
         /* GeneXus formulas. */
         AV89Pgmname = "PromptReferenciaTecnica";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_83 ;
      private short nGXsfl_83_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A114ReferenciaTecnica_Unidade ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_83_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtReferenciaTecnica_Codigo_Titleformat ;
      private short edtReferenciaTecnica_Nome_Titleformat ;
      private short edtReferenciaTecnica_Descricao_Titleformat ;
      private short cmbReferenciaTecnica_Unidade_Titleformat ;
      private short edtReferenciaTecnica_Valor_Titleformat ;
      private short edtGuia_Codigo_Titleformat ;
      private short edtGuia_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutReferenciaTecnica_Codigo ;
      private int wcpOAV7InOutReferenciaTecnica_Codigo ;
      private int subGrid_Rows ;
      private int AV55TFReferenciaTecnica_Codigo ;
      private int AV56TFReferenciaTecnica_Codigo_To ;
      private int AV75TFGuia_Codigo ;
      private int AV76TFGuia_Codigo_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_referenciatecnica_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_referenciatecnica_nome_Datalistupdateminimumcharacters ;
      private int Ddo_referenciatecnica_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_referenciatecnica_unidade_Datalistupdateminimumcharacters ;
      private int Ddo_referenciatecnica_valor_Datalistupdateminimumcharacters ;
      private int Ddo_guia_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_guia_nome_Datalistupdateminimumcharacters ;
      private int edtavTfreferenciatecnica_codigo_Visible ;
      private int edtavTfreferenciatecnica_codigo_to_Visible ;
      private int edtavTfreferenciatecnica_nome_Visible ;
      private int edtavTfreferenciatecnica_nome_sel_Visible ;
      private int edtavTfreferenciatecnica_descricao_Visible ;
      private int edtavTfreferenciatecnica_descricao_sel_Visible ;
      private int edtavTfreferenciatecnica_valor_Visible ;
      private int edtavTfreferenciatecnica_valor_to_Visible ;
      private int edtavTfguia_codigo_Visible ;
      private int edtavTfguia_codigo_to_Visible ;
      private int edtavTfguia_nome_Visible ;
      private int edtavTfguia_nome_sel_Visible ;
      private int edtavDdo_referenciatecnica_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_referenciatecnica_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_referenciatecnica_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_referenciatecnica_unidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_referenciatecnica_valortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_guia_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_guia_nometitlecontrolidtoreplace_Visible ;
      private int A97ReferenciaTecnica_Codigo ;
      private int A93Guia_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV68TFReferenciaTecnica_Unidade_Sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int AV83PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavReferenciatecnica_nome1_Visible ;
      private int edtavGuia_nome1_Visible ;
      private int edtavReferenciatecnica_nome2_Visible ;
      private int edtavGuia_nome2_Visible ;
      private int edtavReferenciatecnica_nome3_Visible ;
      private int edtavGuia_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV84GridCurrentPage ;
      private long AV85GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV71TFReferenciaTecnica_Valor ;
      private decimal AV72TFReferenciaTecnica_Valor_To ;
      private decimal A100ReferenciaTecnica_Valor ;
      private String AV8InOutReferenciaTecnica_Nome ;
      private String wcpOAV8InOutReferenciaTecnica_Nome ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_referenciatecnica_codigo_Activeeventkey ;
      private String Ddo_referenciatecnica_codigo_Filteredtext_get ;
      private String Ddo_referenciatecnica_codigo_Filteredtextto_get ;
      private String Ddo_referenciatecnica_nome_Activeeventkey ;
      private String Ddo_referenciatecnica_nome_Filteredtext_get ;
      private String Ddo_referenciatecnica_nome_Selectedvalue_get ;
      private String Ddo_referenciatecnica_descricao_Activeeventkey ;
      private String Ddo_referenciatecnica_descricao_Filteredtext_get ;
      private String Ddo_referenciatecnica_descricao_Selectedvalue_get ;
      private String Ddo_referenciatecnica_unidade_Activeeventkey ;
      private String Ddo_referenciatecnica_unidade_Selectedvalue_get ;
      private String Ddo_referenciatecnica_valor_Activeeventkey ;
      private String Ddo_referenciatecnica_valor_Filteredtext_get ;
      private String Ddo_referenciatecnica_valor_Filteredtextto_get ;
      private String Ddo_guia_codigo_Activeeventkey ;
      private String Ddo_guia_codigo_Filteredtext_get ;
      private String Ddo_guia_codigo_Filteredtextto_get ;
      private String Ddo_guia_nome_Activeeventkey ;
      private String Ddo_guia_nome_Filteredtext_get ;
      private String Ddo_guia_nome_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_83_idx="0001" ;
      private String AV17ReferenciaTecnica_Nome1 ;
      private String AV18Guia_Nome1 ;
      private String AV22ReferenciaTecnica_Nome2 ;
      private String AV23Guia_Nome2 ;
      private String AV27ReferenciaTecnica_Nome3 ;
      private String AV28Guia_Nome3 ;
      private String AV59TFReferenciaTecnica_Nome ;
      private String AV60TFReferenciaTecnica_Nome_Sel ;
      private String AV79TFGuia_Nome ;
      private String AV80TFGuia_Nome_Sel ;
      private String AV89Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_referenciatecnica_codigo_Caption ;
      private String Ddo_referenciatecnica_codigo_Tooltip ;
      private String Ddo_referenciatecnica_codigo_Cls ;
      private String Ddo_referenciatecnica_codigo_Filteredtext_set ;
      private String Ddo_referenciatecnica_codigo_Filteredtextto_set ;
      private String Ddo_referenciatecnica_codigo_Dropdownoptionstype ;
      private String Ddo_referenciatecnica_codigo_Titlecontrolidtoreplace ;
      private String Ddo_referenciatecnica_codigo_Sortedstatus ;
      private String Ddo_referenciatecnica_codigo_Filtertype ;
      private String Ddo_referenciatecnica_codigo_Datalistfixedvalues ;
      private String Ddo_referenciatecnica_codigo_Sortasc ;
      private String Ddo_referenciatecnica_codigo_Sortdsc ;
      private String Ddo_referenciatecnica_codigo_Loadingdata ;
      private String Ddo_referenciatecnica_codigo_Cleanfilter ;
      private String Ddo_referenciatecnica_codigo_Rangefilterfrom ;
      private String Ddo_referenciatecnica_codigo_Rangefilterto ;
      private String Ddo_referenciatecnica_codigo_Noresultsfound ;
      private String Ddo_referenciatecnica_codigo_Searchbuttontext ;
      private String Ddo_referenciatecnica_nome_Caption ;
      private String Ddo_referenciatecnica_nome_Tooltip ;
      private String Ddo_referenciatecnica_nome_Cls ;
      private String Ddo_referenciatecnica_nome_Filteredtext_set ;
      private String Ddo_referenciatecnica_nome_Selectedvalue_set ;
      private String Ddo_referenciatecnica_nome_Dropdownoptionstype ;
      private String Ddo_referenciatecnica_nome_Titlecontrolidtoreplace ;
      private String Ddo_referenciatecnica_nome_Sortedstatus ;
      private String Ddo_referenciatecnica_nome_Filtertype ;
      private String Ddo_referenciatecnica_nome_Datalisttype ;
      private String Ddo_referenciatecnica_nome_Datalistfixedvalues ;
      private String Ddo_referenciatecnica_nome_Datalistproc ;
      private String Ddo_referenciatecnica_nome_Sortasc ;
      private String Ddo_referenciatecnica_nome_Sortdsc ;
      private String Ddo_referenciatecnica_nome_Loadingdata ;
      private String Ddo_referenciatecnica_nome_Cleanfilter ;
      private String Ddo_referenciatecnica_nome_Rangefilterfrom ;
      private String Ddo_referenciatecnica_nome_Rangefilterto ;
      private String Ddo_referenciatecnica_nome_Noresultsfound ;
      private String Ddo_referenciatecnica_nome_Searchbuttontext ;
      private String Ddo_referenciatecnica_descricao_Caption ;
      private String Ddo_referenciatecnica_descricao_Tooltip ;
      private String Ddo_referenciatecnica_descricao_Cls ;
      private String Ddo_referenciatecnica_descricao_Filteredtext_set ;
      private String Ddo_referenciatecnica_descricao_Selectedvalue_set ;
      private String Ddo_referenciatecnica_descricao_Dropdownoptionstype ;
      private String Ddo_referenciatecnica_descricao_Titlecontrolidtoreplace ;
      private String Ddo_referenciatecnica_descricao_Sortedstatus ;
      private String Ddo_referenciatecnica_descricao_Filtertype ;
      private String Ddo_referenciatecnica_descricao_Datalisttype ;
      private String Ddo_referenciatecnica_descricao_Datalistfixedvalues ;
      private String Ddo_referenciatecnica_descricao_Datalistproc ;
      private String Ddo_referenciatecnica_descricao_Sortasc ;
      private String Ddo_referenciatecnica_descricao_Sortdsc ;
      private String Ddo_referenciatecnica_descricao_Loadingdata ;
      private String Ddo_referenciatecnica_descricao_Cleanfilter ;
      private String Ddo_referenciatecnica_descricao_Rangefilterfrom ;
      private String Ddo_referenciatecnica_descricao_Rangefilterto ;
      private String Ddo_referenciatecnica_descricao_Noresultsfound ;
      private String Ddo_referenciatecnica_descricao_Searchbuttontext ;
      private String Ddo_referenciatecnica_unidade_Caption ;
      private String Ddo_referenciatecnica_unidade_Tooltip ;
      private String Ddo_referenciatecnica_unidade_Cls ;
      private String Ddo_referenciatecnica_unidade_Selectedvalue_set ;
      private String Ddo_referenciatecnica_unidade_Dropdownoptionstype ;
      private String Ddo_referenciatecnica_unidade_Titlecontrolidtoreplace ;
      private String Ddo_referenciatecnica_unidade_Sortedstatus ;
      private String Ddo_referenciatecnica_unidade_Datalisttype ;
      private String Ddo_referenciatecnica_unidade_Datalistfixedvalues ;
      private String Ddo_referenciatecnica_unidade_Sortasc ;
      private String Ddo_referenciatecnica_unidade_Sortdsc ;
      private String Ddo_referenciatecnica_unidade_Loadingdata ;
      private String Ddo_referenciatecnica_unidade_Cleanfilter ;
      private String Ddo_referenciatecnica_unidade_Rangefilterfrom ;
      private String Ddo_referenciatecnica_unidade_Rangefilterto ;
      private String Ddo_referenciatecnica_unidade_Noresultsfound ;
      private String Ddo_referenciatecnica_unidade_Searchbuttontext ;
      private String Ddo_referenciatecnica_valor_Caption ;
      private String Ddo_referenciatecnica_valor_Tooltip ;
      private String Ddo_referenciatecnica_valor_Cls ;
      private String Ddo_referenciatecnica_valor_Filteredtext_set ;
      private String Ddo_referenciatecnica_valor_Filteredtextto_set ;
      private String Ddo_referenciatecnica_valor_Dropdownoptionstype ;
      private String Ddo_referenciatecnica_valor_Titlecontrolidtoreplace ;
      private String Ddo_referenciatecnica_valor_Sortedstatus ;
      private String Ddo_referenciatecnica_valor_Filtertype ;
      private String Ddo_referenciatecnica_valor_Datalistfixedvalues ;
      private String Ddo_referenciatecnica_valor_Sortasc ;
      private String Ddo_referenciatecnica_valor_Sortdsc ;
      private String Ddo_referenciatecnica_valor_Loadingdata ;
      private String Ddo_referenciatecnica_valor_Cleanfilter ;
      private String Ddo_referenciatecnica_valor_Rangefilterfrom ;
      private String Ddo_referenciatecnica_valor_Rangefilterto ;
      private String Ddo_referenciatecnica_valor_Noresultsfound ;
      private String Ddo_referenciatecnica_valor_Searchbuttontext ;
      private String Ddo_guia_codigo_Caption ;
      private String Ddo_guia_codigo_Tooltip ;
      private String Ddo_guia_codigo_Cls ;
      private String Ddo_guia_codigo_Filteredtext_set ;
      private String Ddo_guia_codigo_Filteredtextto_set ;
      private String Ddo_guia_codigo_Dropdownoptionstype ;
      private String Ddo_guia_codigo_Titlecontrolidtoreplace ;
      private String Ddo_guia_codigo_Sortedstatus ;
      private String Ddo_guia_codigo_Filtertype ;
      private String Ddo_guia_codigo_Datalistfixedvalues ;
      private String Ddo_guia_codigo_Sortasc ;
      private String Ddo_guia_codigo_Sortdsc ;
      private String Ddo_guia_codigo_Loadingdata ;
      private String Ddo_guia_codigo_Cleanfilter ;
      private String Ddo_guia_codigo_Rangefilterfrom ;
      private String Ddo_guia_codigo_Rangefilterto ;
      private String Ddo_guia_codigo_Noresultsfound ;
      private String Ddo_guia_codigo_Searchbuttontext ;
      private String Ddo_guia_nome_Caption ;
      private String Ddo_guia_nome_Tooltip ;
      private String Ddo_guia_nome_Cls ;
      private String Ddo_guia_nome_Filteredtext_set ;
      private String Ddo_guia_nome_Selectedvalue_set ;
      private String Ddo_guia_nome_Dropdownoptionstype ;
      private String Ddo_guia_nome_Titlecontrolidtoreplace ;
      private String Ddo_guia_nome_Sortedstatus ;
      private String Ddo_guia_nome_Filtertype ;
      private String Ddo_guia_nome_Datalisttype ;
      private String Ddo_guia_nome_Datalistfixedvalues ;
      private String Ddo_guia_nome_Datalistproc ;
      private String Ddo_guia_nome_Sortasc ;
      private String Ddo_guia_nome_Sortdsc ;
      private String Ddo_guia_nome_Loadingdata ;
      private String Ddo_guia_nome_Cleanfilter ;
      private String Ddo_guia_nome_Rangefilterfrom ;
      private String Ddo_guia_nome_Rangefilterto ;
      private String Ddo_guia_nome_Noresultsfound ;
      private String Ddo_guia_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfreferenciatecnica_codigo_Internalname ;
      private String edtavTfreferenciatecnica_codigo_Jsonclick ;
      private String edtavTfreferenciatecnica_codigo_to_Internalname ;
      private String edtavTfreferenciatecnica_codigo_to_Jsonclick ;
      private String edtavTfreferenciatecnica_nome_Internalname ;
      private String edtavTfreferenciatecnica_nome_Jsonclick ;
      private String edtavTfreferenciatecnica_nome_sel_Internalname ;
      private String edtavTfreferenciatecnica_nome_sel_Jsonclick ;
      private String edtavTfreferenciatecnica_descricao_Internalname ;
      private String edtavTfreferenciatecnica_descricao_sel_Internalname ;
      private String edtavTfreferenciatecnica_valor_Internalname ;
      private String edtavTfreferenciatecnica_valor_Jsonclick ;
      private String edtavTfreferenciatecnica_valor_to_Internalname ;
      private String edtavTfreferenciatecnica_valor_to_Jsonclick ;
      private String edtavTfguia_codigo_Internalname ;
      private String edtavTfguia_codigo_Jsonclick ;
      private String edtavTfguia_codigo_to_Internalname ;
      private String edtavTfguia_codigo_to_Jsonclick ;
      private String edtavTfguia_nome_Internalname ;
      private String edtavTfguia_nome_Jsonclick ;
      private String edtavTfguia_nome_sel_Internalname ;
      private String edtavTfguia_nome_sel_Jsonclick ;
      private String edtavDdo_referenciatecnica_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_referenciatecnica_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_referenciatecnica_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_referenciatecnica_unidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_referenciatecnica_valortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_guia_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_guia_nometitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtReferenciaTecnica_Codigo_Internalname ;
      private String A98ReferenciaTecnica_Nome ;
      private String edtReferenciaTecnica_Nome_Internalname ;
      private String edtReferenciaTecnica_Descricao_Internalname ;
      private String cmbReferenciaTecnica_Unidade_Internalname ;
      private String edtReferenciaTecnica_Valor_Internalname ;
      private String edtGuia_Codigo_Internalname ;
      private String A94Guia_Nome ;
      private String edtGuia_Nome_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV17ReferenciaTecnica_Nome1 ;
      private String lV18Guia_Nome1 ;
      private String lV22ReferenciaTecnica_Nome2 ;
      private String lV23Guia_Nome2 ;
      private String lV27ReferenciaTecnica_Nome3 ;
      private String lV28Guia_Nome3 ;
      private String lV59TFReferenciaTecnica_Nome ;
      private String lV79TFGuia_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavReferenciatecnica_nome1_Internalname ;
      private String edtavGuia_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavReferenciatecnica_nome2_Internalname ;
      private String edtavGuia_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavReferenciatecnica_nome3_Internalname ;
      private String edtavGuia_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_referenciatecnica_codigo_Internalname ;
      private String Ddo_referenciatecnica_nome_Internalname ;
      private String Ddo_referenciatecnica_descricao_Internalname ;
      private String Ddo_referenciatecnica_unidade_Internalname ;
      private String Ddo_referenciatecnica_valor_Internalname ;
      private String Ddo_guia_codigo_Internalname ;
      private String Ddo_guia_nome_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtReferenciaTecnica_Codigo_Title ;
      private String edtReferenciaTecnica_Nome_Title ;
      private String edtReferenciaTecnica_Descricao_Title ;
      private String edtReferenciaTecnica_Valor_Title ;
      private String edtGuia_Codigo_Title ;
      private String edtGuia_Nome_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavReferenciatecnica_nome3_Jsonclick ;
      private String edtavGuia_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavReferenciatecnica_nome2_Jsonclick ;
      private String edtavGuia_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavReferenciatecnica_nome1_Jsonclick ;
      private String edtavGuia_nome1_Jsonclick ;
      private String sGXsfl_83_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtReferenciaTecnica_Codigo_Jsonclick ;
      private String edtReferenciaTecnica_Nome_Jsonclick ;
      private String edtReferenciaTecnica_Descricao_Jsonclick ;
      private String cmbReferenciaTecnica_Unidade_Jsonclick ;
      private String edtReferenciaTecnica_Valor_Jsonclick ;
      private String edtGuia_Codigo_Jsonclick ;
      private String edtGuia_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_referenciatecnica_codigo_Includesortasc ;
      private bool Ddo_referenciatecnica_codigo_Includesortdsc ;
      private bool Ddo_referenciatecnica_codigo_Includefilter ;
      private bool Ddo_referenciatecnica_codigo_Filterisrange ;
      private bool Ddo_referenciatecnica_codigo_Includedatalist ;
      private bool Ddo_referenciatecnica_nome_Includesortasc ;
      private bool Ddo_referenciatecnica_nome_Includesortdsc ;
      private bool Ddo_referenciatecnica_nome_Includefilter ;
      private bool Ddo_referenciatecnica_nome_Filterisrange ;
      private bool Ddo_referenciatecnica_nome_Includedatalist ;
      private bool Ddo_referenciatecnica_descricao_Includesortasc ;
      private bool Ddo_referenciatecnica_descricao_Includesortdsc ;
      private bool Ddo_referenciatecnica_descricao_Includefilter ;
      private bool Ddo_referenciatecnica_descricao_Filterisrange ;
      private bool Ddo_referenciatecnica_descricao_Includedatalist ;
      private bool Ddo_referenciatecnica_unidade_Includesortasc ;
      private bool Ddo_referenciatecnica_unidade_Includesortdsc ;
      private bool Ddo_referenciatecnica_unidade_Includefilter ;
      private bool Ddo_referenciatecnica_unidade_Filterisrange ;
      private bool Ddo_referenciatecnica_unidade_Includedatalist ;
      private bool Ddo_referenciatecnica_unidade_Allowmultipleselection ;
      private bool Ddo_referenciatecnica_valor_Includesortasc ;
      private bool Ddo_referenciatecnica_valor_Includesortdsc ;
      private bool Ddo_referenciatecnica_valor_Includefilter ;
      private bool Ddo_referenciatecnica_valor_Filterisrange ;
      private bool Ddo_referenciatecnica_valor_Includedatalist ;
      private bool Ddo_guia_codigo_Includesortasc ;
      private bool Ddo_guia_codigo_Includesortdsc ;
      private bool Ddo_guia_codigo_Includefilter ;
      private bool Ddo_guia_codigo_Filterisrange ;
      private bool Ddo_guia_codigo_Includedatalist ;
      private bool Ddo_guia_nome_Includesortasc ;
      private bool Ddo_guia_nome_Includesortdsc ;
      private bool Ddo_guia_nome_Includefilter ;
      private bool Ddo_guia_nome_Filterisrange ;
      private bool Ddo_guia_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Select_IsBlob ;
      private String A99ReferenciaTecnica_Descricao ;
      private String AV67TFReferenciaTecnica_Unidade_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV63TFReferenciaTecnica_Descricao ;
      private String AV64TFReferenciaTecnica_Descricao_Sel ;
      private String AV57ddo_ReferenciaTecnica_CodigoTitleControlIdToReplace ;
      private String AV61ddo_ReferenciaTecnica_NomeTitleControlIdToReplace ;
      private String AV65ddo_ReferenciaTecnica_DescricaoTitleControlIdToReplace ;
      private String AV69ddo_ReferenciaTecnica_UnidadeTitleControlIdToReplace ;
      private String AV73ddo_ReferenciaTecnica_ValorTitleControlIdToReplace ;
      private String AV77ddo_Guia_CodigoTitleControlIdToReplace ;
      private String AV81ddo_Guia_NomeTitleControlIdToReplace ;
      private String AV88Select_GXI ;
      private String lV63TFReferenciaTecnica_Descricao ;
      private String AV31Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutReferenciaTecnica_Codigo ;
      private String aP1_InOutReferenciaTecnica_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbReferenciaTecnica_Unidade ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H005B2_A94Guia_Nome ;
      private int[] H005B2_A93Guia_Codigo ;
      private decimal[] H005B2_A100ReferenciaTecnica_Valor ;
      private short[] H005B2_A114ReferenciaTecnica_Unidade ;
      private String[] H005B2_A99ReferenciaTecnica_Descricao ;
      private String[] H005B2_A98ReferenciaTecnica_Nome ;
      private int[] H005B2_A97ReferenciaTecnica_Codigo ;
      private long[] H005B3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV68TFReferenciaTecnica_Unidade_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54ReferenciaTecnica_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58ReferenciaTecnica_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62ReferenciaTecnica_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66ReferenciaTecnica_UnidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV70ReferenciaTecnica_ValorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV74Guia_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV78Guia_NomeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV82DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptreferenciatecnica__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H005B2( IGxContext context ,
                                             short A114ReferenciaTecnica_Unidade ,
                                             IGxCollection AV68TFReferenciaTecnica_Unidade_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17ReferenciaTecnica_Nome1 ,
                                             String AV18Guia_Nome1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22ReferenciaTecnica_Nome2 ,
                                             String AV23Guia_Nome2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             short AV26DynamicFiltersOperator3 ,
                                             String AV27ReferenciaTecnica_Nome3 ,
                                             String AV28Guia_Nome3 ,
                                             int AV55TFReferenciaTecnica_Codigo ,
                                             int AV56TFReferenciaTecnica_Codigo_To ,
                                             String AV60TFReferenciaTecnica_Nome_Sel ,
                                             String AV59TFReferenciaTecnica_Nome ,
                                             String AV64TFReferenciaTecnica_Descricao_Sel ,
                                             String AV63TFReferenciaTecnica_Descricao ,
                                             int AV68TFReferenciaTecnica_Unidade_Sels_Count ,
                                             decimal AV71TFReferenciaTecnica_Valor ,
                                             decimal AV72TFReferenciaTecnica_Valor_To ,
                                             int AV75TFGuia_Codigo ,
                                             int AV76TFGuia_Codigo_To ,
                                             String AV80TFGuia_Nome_Sel ,
                                             String AV79TFGuia_Nome ,
                                             String A98ReferenciaTecnica_Nome ,
                                             String A94Guia_Nome ,
                                             int A97ReferenciaTecnica_Codigo ,
                                             String A99ReferenciaTecnica_Descricao ,
                                             decimal A100ReferenciaTecnica_Valor ,
                                             int A93Guia_Codigo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [29] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Guia_Nome], T1.[Guia_Codigo], T1.[ReferenciaTecnica_Valor], T1.[ReferenciaTecnica_Unidade], T1.[ReferenciaTecnica_Descricao], T1.[ReferenciaTecnica_Nome], T1.[ReferenciaTecnica_Codigo]";
         sFromString = " FROM ([ReferenciaTecnica] T1 WITH (NOLOCK) INNER JOIN [Guia] T2 WITH (NOLOCK) ON T2.[Guia_Codigo] = T1.[Guia_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ReferenciaTecnica_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV17ReferenciaTecnica_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV17ReferenciaTecnica_Nome1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ReferenciaTecnica_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV17ReferenciaTecnica_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV17ReferenciaTecnica_Nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Guia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV18Guia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV18Guia_Nome1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Guia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV18Guia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV18Guia_Nome1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ReferenciaTecnica_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV22ReferenciaTecnica_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV22ReferenciaTecnica_Nome2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ReferenciaTecnica_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV22ReferenciaTecnica_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV22ReferenciaTecnica_Nome2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Guia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV23Guia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV23Guia_Nome2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Guia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV23Guia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV23Guia_Nome2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27ReferenciaTecnica_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV27ReferenciaTecnica_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV27ReferenciaTecnica_Nome3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27ReferenciaTecnica_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV27ReferenciaTecnica_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV27ReferenciaTecnica_Nome3)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Guia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV28Guia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV28Guia_Nome3)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Guia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV28Guia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV28Guia_Nome3)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV55TFReferenciaTecnica_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] >= @AV55TFReferenciaTecnica_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] >= @AV55TFReferenciaTecnica_Codigo)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV56TFReferenciaTecnica_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] <= @AV56TFReferenciaTecnica_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] <= @AV56TFReferenciaTecnica_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFReferenciaTecnica_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFReferenciaTecnica_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV59TFReferenciaTecnica_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV59TFReferenciaTecnica_Nome)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFReferenciaTecnica_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] = @AV60TFReferenciaTecnica_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] = @AV60TFReferenciaTecnica_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64TFReferenciaTecnica_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFReferenciaTecnica_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] like @lV63TFReferenciaTecnica_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] like @lV63TFReferenciaTecnica_Descricao)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFReferenciaTecnica_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] = @AV64TFReferenciaTecnica_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] = @AV64TFReferenciaTecnica_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV68TFReferenciaTecnica_Unidade_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV68TFReferenciaTecnica_Unidade_Sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV68TFReferenciaTecnica_Unidade_Sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
         }
         if ( ! (Convert.ToDecimal(0)==AV71TFReferenciaTecnica_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] >= @AV71TFReferenciaTecnica_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] >= @AV71TFReferenciaTecnica_Valor)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV72TFReferenciaTecnica_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] <= @AV72TFReferenciaTecnica_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] <= @AV72TFReferenciaTecnica_Valor_To)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (0==AV75TFGuia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] >= @AV75TFGuia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] >= @AV75TFGuia_Codigo)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (0==AV76TFGuia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] <= @AV76TFGuia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] <= @AV76TFGuia_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV80TFGuia_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFGuia_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV79TFGuia_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV79TFGuia_Nome)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFGuia_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] = @AV80TFGuia_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] = @AV80TFGuia_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaTecnica_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaTecnica_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaTecnica_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaTecnica_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaTecnica_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaTecnica_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaTecnica_Unidade]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaTecnica_Unidade] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaTecnica_Valor]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaTecnica_Valor] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Guia_Codigo]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Guia_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Guia_Nome]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Guia_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaTecnica_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H005B3( IGxContext context ,
                                             short A114ReferenciaTecnica_Unidade ,
                                             IGxCollection AV68TFReferenciaTecnica_Unidade_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17ReferenciaTecnica_Nome1 ,
                                             String AV18Guia_Nome1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22ReferenciaTecnica_Nome2 ,
                                             String AV23Guia_Nome2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             short AV26DynamicFiltersOperator3 ,
                                             String AV27ReferenciaTecnica_Nome3 ,
                                             String AV28Guia_Nome3 ,
                                             int AV55TFReferenciaTecnica_Codigo ,
                                             int AV56TFReferenciaTecnica_Codigo_To ,
                                             String AV60TFReferenciaTecnica_Nome_Sel ,
                                             String AV59TFReferenciaTecnica_Nome ,
                                             String AV64TFReferenciaTecnica_Descricao_Sel ,
                                             String AV63TFReferenciaTecnica_Descricao ,
                                             int AV68TFReferenciaTecnica_Unidade_Sels_Count ,
                                             decimal AV71TFReferenciaTecnica_Valor ,
                                             decimal AV72TFReferenciaTecnica_Valor_To ,
                                             int AV75TFGuia_Codigo ,
                                             int AV76TFGuia_Codigo_To ,
                                             String AV80TFGuia_Nome_Sel ,
                                             String AV79TFGuia_Nome ,
                                             String A98ReferenciaTecnica_Nome ,
                                             String A94Guia_Nome ,
                                             int A97ReferenciaTecnica_Codigo ,
                                             String A99ReferenciaTecnica_Descricao ,
                                             decimal A100ReferenciaTecnica_Valor ,
                                             int A93Guia_Codigo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [24] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ReferenciaTecnica] T1 WITH (NOLOCK) INNER JOIN [Guia] T2 WITH (NOLOCK) ON T2.[Guia_Codigo] = T1.[Guia_Codigo])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ReferenciaTecnica_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV17ReferenciaTecnica_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV17ReferenciaTecnica_Nome1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ReferenciaTecnica_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV17ReferenciaTecnica_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV17ReferenciaTecnica_Nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Guia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV18Guia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV18Guia_Nome1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "GUIA_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Guia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV18Guia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV18Guia_Nome1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ReferenciaTecnica_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV22ReferenciaTecnica_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV22ReferenciaTecnica_Nome2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ReferenciaTecnica_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV22ReferenciaTecnica_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV22ReferenciaTecnica_Nome2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Guia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV23Guia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV23Guia_Nome2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "GUIA_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Guia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV23Guia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV23Guia_Nome2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27ReferenciaTecnica_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV27ReferenciaTecnica_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV27ReferenciaTecnica_Nome3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27ReferenciaTecnica_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV27ReferenciaTecnica_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV27ReferenciaTecnica_Nome3)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Guia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV28Guia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV28Guia_Nome3)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "GUIA_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Guia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV28Guia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV28Guia_Nome3)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV55TFReferenciaTecnica_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] >= @AV55TFReferenciaTecnica_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] >= @AV55TFReferenciaTecnica_Codigo)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV56TFReferenciaTecnica_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] <= @AV56TFReferenciaTecnica_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] <= @AV56TFReferenciaTecnica_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFReferenciaTecnica_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFReferenciaTecnica_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV59TFReferenciaTecnica_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV59TFReferenciaTecnica_Nome)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFReferenciaTecnica_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] = @AV60TFReferenciaTecnica_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] = @AV60TFReferenciaTecnica_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64TFReferenciaTecnica_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFReferenciaTecnica_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] like @lV63TFReferenciaTecnica_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] like @lV63TFReferenciaTecnica_Descricao)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFReferenciaTecnica_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] = @AV64TFReferenciaTecnica_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] = @AV64TFReferenciaTecnica_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV68TFReferenciaTecnica_Unidade_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV68TFReferenciaTecnica_Unidade_Sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV68TFReferenciaTecnica_Unidade_Sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
         }
         if ( ! (Convert.ToDecimal(0)==AV71TFReferenciaTecnica_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] >= @AV71TFReferenciaTecnica_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] >= @AV71TFReferenciaTecnica_Valor)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV72TFReferenciaTecnica_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] <= @AV72TFReferenciaTecnica_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] <= @AV72TFReferenciaTecnica_Valor_To)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (0==AV75TFGuia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] >= @AV75TFGuia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] >= @AV75TFGuia_Codigo)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (0==AV76TFGuia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Guia_Codigo] <= @AV76TFGuia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Guia_Codigo] <= @AV76TFGuia_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV80TFGuia_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFGuia_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV79TFGuia_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV79TFGuia_Nome)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFGuia_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] = @AV80TFGuia_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] = @AV80TFGuia_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H005B2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (int)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
               case 1 :
                     return conditional_H005B3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (int)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH005B2 ;
          prmH005B2 = new Object[] {
          new Object[] {"@lV17ReferenciaTecnica_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV17ReferenciaTecnica_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Guia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Guia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22ReferenciaTecnica_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22ReferenciaTecnica_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23Guia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23Guia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27ReferenciaTecnica_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27ReferenciaTecnica_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28Guia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28Guia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55TFReferenciaTecnica_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV56TFReferenciaTecnica_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59TFReferenciaTecnica_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60TFReferenciaTecnica_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63TFReferenciaTecnica_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV64TFReferenciaTecnica_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV71TFReferenciaTecnica_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV72TFReferenciaTecnica_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV75TFGuia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV76TFGuia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV79TFGuia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV80TFGuia_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH005B3 ;
          prmH005B3 = new Object[] {
          new Object[] {"@lV17ReferenciaTecnica_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV17ReferenciaTecnica_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Guia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Guia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22ReferenciaTecnica_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22ReferenciaTecnica_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23Guia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23Guia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27ReferenciaTecnica_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27ReferenciaTecnica_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28Guia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28Guia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55TFReferenciaTecnica_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV56TFReferenciaTecnica_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59TFReferenciaTecnica_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60TFReferenciaTecnica_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63TFReferenciaTecnica_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV64TFReferenciaTecnica_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV71TFReferenciaTecnica_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV72TFReferenciaTecnica_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV75TFGuia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV76TFGuia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV79TFGuia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV80TFGuia_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H005B2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005B2,11,0,true,false )
             ,new CursorDef("H005B3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005B3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
       }
    }

 }

}
