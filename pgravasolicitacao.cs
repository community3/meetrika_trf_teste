/*
               File: PGravaSolicitacao
        Description: PGrava Solicitacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:1:36.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class pgravasolicitacao : GXProcedure
   {
      public pgravasolicitacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public pgravasolicitacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_codigo ,
                           int aP1_Servico_Codigo ,
                           int aP2_Sistema_Codigo ,
                           String aP3_Solicitacoes_Objetivo ,
                           String aP4_Solicitacoes_Novo_Projeto ,
                           int aP5_UserName ,
                           IGxCollection aP6_SDTSolicitacao ,
                           out String aP7_vrMsg )
      {
         this.AV14Contratada_codigo = aP0_Contratada_codigo;
         this.AV13Servico_Codigo = aP1_Servico_Codigo;
         this.AV12Sistema_Codigo = aP2_Sistema_Codigo;
         this.AV11Solicitacoes_Objetivo = aP3_Solicitacoes_Objetivo;
         this.AV10Solicitacoes_Novo_Projeto = aP4_Solicitacoes_Novo_Projeto;
         this.AV9UserName = aP5_UserName;
         this.AV8SDTSolicitacao = aP6_SDTSolicitacao;
         this.AV21vrMsg = "" ;
         initialize();
         executePrivate();
         aP7_vrMsg=this.AV21vrMsg;
      }

      public String executeUdp( int aP0_Contratada_codigo ,
                                int aP1_Servico_Codigo ,
                                int aP2_Sistema_Codigo ,
                                String aP3_Solicitacoes_Objetivo ,
                                String aP4_Solicitacoes_Novo_Projeto ,
                                int aP5_UserName ,
                                IGxCollection aP6_SDTSolicitacao )
      {
         this.AV14Contratada_codigo = aP0_Contratada_codigo;
         this.AV13Servico_Codigo = aP1_Servico_Codigo;
         this.AV12Sistema_Codigo = aP2_Sistema_Codigo;
         this.AV11Solicitacoes_Objetivo = aP3_Solicitacoes_Objetivo;
         this.AV10Solicitacoes_Novo_Projeto = aP4_Solicitacoes_Novo_Projeto;
         this.AV9UserName = aP5_UserName;
         this.AV8SDTSolicitacao = aP6_SDTSolicitacao;
         this.AV21vrMsg = "" ;
         initialize();
         executePrivate();
         aP7_vrMsg=this.AV21vrMsg;
         return AV21vrMsg ;
      }

      public void executeSubmit( int aP0_Contratada_codigo ,
                                 int aP1_Servico_Codigo ,
                                 int aP2_Sistema_Codigo ,
                                 String aP3_Solicitacoes_Objetivo ,
                                 String aP4_Solicitacoes_Novo_Projeto ,
                                 int aP5_UserName ,
                                 IGxCollection aP6_SDTSolicitacao ,
                                 out String aP7_vrMsg )
      {
         pgravasolicitacao objpgravasolicitacao;
         objpgravasolicitacao = new pgravasolicitacao();
         objpgravasolicitacao.AV14Contratada_codigo = aP0_Contratada_codigo;
         objpgravasolicitacao.AV13Servico_Codigo = aP1_Servico_Codigo;
         objpgravasolicitacao.AV12Sistema_Codigo = aP2_Sistema_Codigo;
         objpgravasolicitacao.AV11Solicitacoes_Objetivo = aP3_Solicitacoes_Objetivo;
         objpgravasolicitacao.AV10Solicitacoes_Novo_Projeto = aP4_Solicitacoes_Novo_Projeto;
         objpgravasolicitacao.AV9UserName = aP5_UserName;
         objpgravasolicitacao.AV8SDTSolicitacao = aP6_SDTSolicitacao;
         objpgravasolicitacao.AV21vrMsg = "" ;
         objpgravasolicitacao.context.SetSubmitInitialConfig(context);
         objpgravasolicitacao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objpgravasolicitacao);
         aP7_vrMsg=this.AV21vrMsg;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((pgravasolicitacao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Solicitacao = new SdtSolicitacoes(context);
         AV17Solicitacao.gxTpr_Contratada_codigo = AV14Contratada_codigo;
         AV17Solicitacao.gxTpr_Servico_codigo = AV13Servico_Codigo;
         AV17Solicitacao.gxTpr_Sistema_codigo = AV12Sistema_Codigo;
         AV17Solicitacao.gxTpr_Solicitacoes_data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV17Solicitacao.gxTpr_Solicitacoes_data_ult = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV17Solicitacao.gxTpr_Solicitacoes_novo_projeto = AV10Solicitacoes_Novo_Projeto;
         AV17Solicitacao.gxTpr_Solicitacoes_objetivo = AV11Solicitacoes_Objetivo;
         AV17Solicitacao.gxTpr_Solicitacoes_status = "A";
         AV17Solicitacao.gxTpr_Solicitacoes_usuario = AV9UserName;
         AV17Solicitacao.gxTpr_Solicitacoes_usuario_ult = AV9UserName;
         AV17Solicitacao.Save();
         if ( ! AV17Solicitacao.Fail() )
         {
            context.CommitDataStores( "PGravaSolicitacao");
            AV16Solicitacoes_Codigo = AV17Solicitacao.gxTpr_Solicitacoes_codigo;
            AV24GXV1 = 1;
            while ( AV24GXV1 <= AV8SDTSolicitacao.Count )
            {
               AV15SDTSolicitacaoItem = ((SdtSDTSolicitacao_SDTSolicitacaoItem)AV8SDTSolicitacao.Item(AV24GXV1));
               AV18SolicitacoesItens = new SdtSolicitacoesItens(context);
               AV18SolicitacoesItens.gxTpr_Solicitacoes_codigo = AV16Solicitacoes_Codigo;
               AV18SolicitacoesItens.gxTpr_Funcaousuario_codigo = AV15SDTSolicitacaoItem.gxTpr_Funcaousuario_codigo;
               AV18SolicitacoesItens.gxTpr_Solicitacoesitens_arquivo = AV15SDTSolicitacaoItem.gxTpr_Solicitacoesitens_arquivo;
               AV18SolicitacoesItens.gxTpr_Solicitacoesitens_descricao = AV15SDTSolicitacaoItem.gxTpr_Solicitacoesitens_descricao;
               AV18SolicitacoesItens.Save();
               if ( ! AV18SolicitacoesItens.Fail() )
               {
                  context.CommitDataStores( "PGravaSolicitacao");
               }
               else
               {
                  /* Execute user subroutine: 'ERROS_ITENS' */
                  S111 ();
                  if ( returnInSub )
                  {
                     this.cleanup();
                     if (true) return;
                  }
               }
               AV24GXV1 = (int)(AV24GXV1+1);
            }
         }
         else
         {
            /* Execute user subroutine: 'ERROS-SOLICITACAO' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'ERROS_ITENS' Routine */
         AV19Messages = AV18SolicitacoesItens.GetMessages();
         AV25GXV2 = 1;
         while ( AV25GXV2 <= AV19Messages.Count )
         {
            AV20Message = ((SdtMessages_Message)AV19Messages.Item(AV25GXV2));
            AV21vrMsg = "Erro ao incluir Pessoa: " + AV20Message.gxTpr_Description;
            GX_msglist.addItem(AV21vrMsg);
            AV25GXV2 = (int)(AV25GXV2+1);
         }
      }

      protected void S121( )
      {
         /* 'ERROS-SOLICITACAO' Routine */
         AV19Messages = AV17Solicitacao.GetMessages();
         AV26GXV3 = 1;
         while ( AV26GXV3 <= AV19Messages.Count )
         {
            AV20Message = ((SdtMessages_Message)AV19Messages.Item(AV26GXV3));
            AV21vrMsg = "Erro ao incluir a Contratada: " + AV20Message.gxTpr_Description;
            GX_msglist.addItem(AV21vrMsg);
            AV26GXV3 = (int)(AV26GXV3+1);
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Solicitacao = new SdtSolicitacoes(context);
         AV15SDTSolicitacaoItem = new SdtSDTSolicitacao_SDTSolicitacaoItem(context);
         AV18SolicitacoesItens = new SdtSolicitacoesItens(context);
         AV19Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV20Message = new SdtMessages_Message(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.pgravasolicitacao__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV14Contratada_codigo ;
      private int AV13Servico_Codigo ;
      private int AV12Sistema_Codigo ;
      private int AV9UserName ;
      private int AV16Solicitacoes_Codigo ;
      private int AV24GXV1 ;
      private int AV25GXV2 ;
      private int AV26GXV3 ;
      private String AV10Solicitacoes_Novo_Projeto ;
      private String AV21vrMsg ;
      private bool returnInSub ;
      private String AV11Solicitacoes_Objetivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP7_vrMsg ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtSDTSolicitacao_SDTSolicitacaoItem ))]
      private IGxCollection AV8SDTSolicitacao ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV19Messages ;
      private SdtSDTSolicitacao_SDTSolicitacaoItem AV15SDTSolicitacaoItem ;
      private SdtSolicitacoes AV17Solicitacao ;
      private SdtMessages_Message AV20Message ;
      private SdtSolicitacoesItens AV18SolicitacoesItens ;
   }

   public class pgravasolicitacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
