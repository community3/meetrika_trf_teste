/*
               File: PromptGeral_Grupo_Funcao
        Description: Selecione Grupo de Func�es
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/15/2020 0:4:24.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptgeral_grupo_funcao : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptgeral_grupo_funcao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptgeral_grupo_funcao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutGrupoFuncao_Codigo ,
                           ref String aP1_InOutGrupoFuncao_Nome )
      {
         this.AV7InOutGrupoFuncao_Codigo = aP0_InOutGrupoFuncao_Codigo;
         this.AV8InOutGrupoFuncao_Nome = aP1_InOutGrupoFuncao_Nome;
         executePrivate();
         aP0_InOutGrupoFuncao_Codigo=this.AV7InOutGrupoFuncao_Codigo;
         aP1_InOutGrupoFuncao_Nome=this.AV8InOutGrupoFuncao_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavGrupofuncao_areatrabalhocod = new GXCombobox();
         chkavGrupofuncao_ativo = new GXCheckbox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_69 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_69_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_69_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV14DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
               AV15GrupoFuncao_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GrupoFuncao_Nome1", AV15GrupoFuncao_Nome1);
               AV17DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
               AV18GrupoFuncao_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GrupoFuncao_Nome2", AV18GrupoFuncao_Nome2);
               AV20DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector3", AV20DynamicFiltersSelector3);
               AV21GrupoFuncao_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GrupoFuncao_Nome3", AV21GrupoFuncao_Nome3);
               AV16DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersEnabled2", AV16DynamicFiltersEnabled2);
               AV19DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled3", AV19DynamicFiltersEnabled3);
               AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
               AV28TFGrupoFuncao_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFGrupoFuncao_Nome", AV28TFGrupoFuncao_Nome);
               AV29TFGrupoFuncao_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFGrupoFuncao_Nome_Sel", AV29TFGrupoFuncao_Nome_Sel);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace", AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace);
               AV36GrupoFuncao_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0)));
               AV26GrupoFuncao_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26GrupoFuncao_Ativo", AV26GrupoFuncao_Ativo);
               AV40Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV23DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersIgnoreFirst", AV23DynamicFiltersIgnoreFirst);
               AV22DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersRemoving", AV22DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV15GrupoFuncao_Nome1, AV17DynamicFiltersSelector2, AV18GrupoFuncao_Nome2, AV20DynamicFiltersSelector3, AV21GrupoFuncao_Nome3, AV16DynamicFiltersEnabled2, AV19DynamicFiltersEnabled3, AV13OrderedDsc, AV28TFGrupoFuncao_Nome, AV29TFGrupoFuncao_Nome_Sel, AV6WWPContext, AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace, AV36GrupoFuncao_AreaTrabalhoCod, AV26GrupoFuncao_Ativo, AV40Pgmname, AV10GridState, AV23DynamicFiltersIgnoreFirst, AV22DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutGrupoFuncao_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutGrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutGrupoFuncao_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutGrupoFuncao_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutGrupoFuncao_Nome", AV8InOutGrupoFuncao_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PACN2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV40Pgmname = "PromptGeral_Grupo_Funcao";
               context.Gx_err = 0;
               WSCN2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WECN2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020515042474");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptgeral_grupo_funcao.aspx") + "?" + UrlEncode("" +AV7InOutGrupoFuncao_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutGrupoFuncao_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV14DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vGRUPOFUNCAO_NOME1", AV15GrupoFuncao_Nome1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV17DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vGRUPOFUNCAO_NOME2", AV18GrupoFuncao_Nome2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV20DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vGRUPOFUNCAO_NOME3", AV21GrupoFuncao_Nome3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV16DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV19DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGRUPOFUNCAO_NOME", AV28TFGrupoFuncao_Nome);
         GxWebStd.gx_hidden_field( context, "GXH_vTFGRUPOFUNCAO_NOME_SEL", AV29TFGrupoFuncao_Nome_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_69", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_69), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV31DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV31DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRUPOFUNCAO_NOMETITLEFILTERDATA", AV27GrupoFuncao_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRUPOFUNCAO_NOMETITLEFILTERDATA", AV27GrupoFuncao_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV40Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV23DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV22DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTGRUPOFUNCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutGrupoFuncao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTGRUPOFUNCAO_NOME", AV8InOutGrupoFuncao_Nome);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Caption", StringUtil.RTrim( Ddo_grupofuncao_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Tooltip", StringUtil.RTrim( Ddo_grupofuncao_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Cls", StringUtil.RTrim( Ddo_grupofuncao_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_grupofuncao_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_grupofuncao_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_grupofuncao_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_grupofuncao_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_grupofuncao_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_grupofuncao_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_grupofuncao_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_grupofuncao_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Filtertype", StringUtil.RTrim( Ddo_grupofuncao_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_grupofuncao_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_grupofuncao_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Datalisttype", StringUtil.RTrim( Ddo_grupofuncao_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_grupofuncao_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Datalistproc", StringUtil.RTrim( Ddo_grupofuncao_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_grupofuncao_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Sortasc", StringUtil.RTrim( Ddo_grupofuncao_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Sortdsc", StringUtil.RTrim( Ddo_grupofuncao_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Loadingdata", StringUtil.RTrim( Ddo_grupofuncao_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_grupofuncao_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_grupofuncao_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Rangefilterto", StringUtil.RTrim( Ddo_grupofuncao_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_grupofuncao_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_grupofuncao_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_grupofuncao_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_grupofuncao_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_grupofuncao_nome_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormCN2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptGeral_Grupo_Funcao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Grupo de Func�es" ;
      }

      protected void WBCN0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_CN2( true) ;
         }
         else
         {
            wb_table1_2_CN2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_CN2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV16DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(77, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(78, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptGeral_Grupo_Funcao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfgrupofuncao_nome_Internalname, AV28TFGrupoFuncao_Nome, StringUtil.RTrim( context.localUtil.Format( AV28TFGrupoFuncao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfgrupofuncao_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfgrupofuncao_nome_Visible, 1, 0, "text", "", 380, "px", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptGeral_Grupo_Funcao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfgrupofuncao_nome_sel_Internalname, AV29TFGrupoFuncao_Nome_Sel, StringUtil.RTrim( context.localUtil.Format( AV29TFGrupoFuncao_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfgrupofuncao_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfgrupofuncao_nome_sel_Visible, 1, 0, "text", "", 380, "px", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptGeral_Grupo_Funcao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GRUPOFUNCAO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_grupofuncao_nometitlecontrolidtoreplace_Internalname, AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", 0, edtavDdo_grupofuncao_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptGeral_Grupo_Funcao.htm");
         }
         wbLoad = true;
      }

      protected void STARTCN2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Grupo de Func�es", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPCN0( ) ;
      }

      protected void WSCN2( )
      {
         STARTCN2( ) ;
         EVTCN2( ) ;
      }

      protected void EVTCN2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11CN2 */
                           E11CN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_GRUPOFUNCAO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12CN2 */
                           E12CN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13CN2 */
                           E13CN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14CN2 */
                           E14CN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15CN2 */
                           E15CN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16CN2 */
                           E16CN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17CN2 */
                           E17CN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18CN2 */
                           E18CN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19CN2 */
                           E19CN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20CN2 */
                           E20CN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21CN2 */
                           E21CN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_69_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
                           SubsflControlProps_692( ) ;
                           AV24Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV24Select)) ? AV39Select_GXI : context.convertURL( context.PathToRelativeUrl( AV24Select))));
                           A619GrupoFuncao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGrupoFuncao_Codigo_Internalname), ",", "."));
                           A620GrupoFuncao_Nome = StringUtil.Upper( cgiGet( edtGrupoFuncao_Nome_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E22CN2 */
                                 E22CN2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23CN2 */
                                 E23CN2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24CN2 */
                                 E24CN2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV14DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Grupofuncao_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vGRUPOFUNCAO_NOME1"), AV15GrupoFuncao_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV17DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Grupofuncao_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vGRUPOFUNCAO_NOME2"), AV18GrupoFuncao_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV20DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Grupofuncao_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vGRUPOFUNCAO_NOME3"), AV21GrupoFuncao_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV16DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV19DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfgrupofuncao_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGRUPOFUNCAO_NOME"), AV28TFGrupoFuncao_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfgrupofuncao_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGRUPOFUNCAO_NOME_SEL"), AV29TFGrupoFuncao_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E25CN2 */
                                       E25CN2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WECN2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormCN2( ) ;
            }
         }
      }

      protected void PACN2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavGrupofuncao_areatrabalhocod.Name = "vGRUPOFUNCAO_AREATRABALHOCOD";
            dynavGrupofuncao_areatrabalhocod.WebTags = "";
            dynavGrupofuncao_areatrabalhocod.removeAllItems();
            /* Using cursor H00CN2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavGrupofuncao_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00CN2_A5AreaTrabalho_Codigo[0]), 6, 0)), H00CN2_A6AreaTrabalho_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavGrupofuncao_areatrabalhocod.ItemCount > 0 )
            {
               AV36GrupoFuncao_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavGrupofuncao_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0)));
            }
            chkavGrupofuncao_ativo.Name = "vGRUPOFUNCAO_ATIVO";
            chkavGrupofuncao_ativo.WebTags = "";
            chkavGrupofuncao_ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavGrupofuncao_ativo_Internalname, "TitleCaption", chkavGrupofuncao_ativo.Caption);
            chkavGrupofuncao_ativo.CheckedValue = "false";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("GRUPOFUNCAO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV14DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV14DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("GRUPOFUNCAO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV17DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV17DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("GRUPOFUNCAO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV20DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector3", AV20DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavGrupofuncao_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvGRUPOFUNCAO_AREATRABALHOCODCN1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvGRUPOFUNCAO_AREATRABALHOCOD_dataCN1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvGRUPOFUNCAO_AREATRABALHOCOD_htmlCN1( )
      {
         int gxdynajaxvalue ;
         GXDLVvGRUPOFUNCAO_AREATRABALHOCOD_dataCN1( ) ;
         gxdynajaxindex = 1;
         dynavGrupofuncao_areatrabalhocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavGrupofuncao_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavGrupofuncao_areatrabalhocod.ItemCount > 0 )
         {
            AV36GrupoFuncao_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavGrupofuncao_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void GXDLVvGRUPOFUNCAO_AREATRABALHOCOD_dataCN1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00CN3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CN3_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00CN3_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_692( ) ;
         while ( nGXsfl_69_idx <= nRC_GXsfl_69 )
         {
            sendrow_692( ) ;
            nGXsfl_69_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_69_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_69_idx+1));
            sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
            SubsflControlProps_692( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV14DynamicFiltersSelector1 ,
                                       String AV15GrupoFuncao_Nome1 ,
                                       String AV17DynamicFiltersSelector2 ,
                                       String AV18GrupoFuncao_Nome2 ,
                                       String AV20DynamicFiltersSelector3 ,
                                       String AV21GrupoFuncao_Nome3 ,
                                       bool AV16DynamicFiltersEnabled2 ,
                                       bool AV19DynamicFiltersEnabled3 ,
                                       bool AV13OrderedDsc ,
                                       String AV28TFGrupoFuncao_Nome ,
                                       String AV29TFGrupoFuncao_Nome_Sel ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace ,
                                       int AV36GrupoFuncao_AreaTrabalhoCod ,
                                       bool AV26GrupoFuncao_Ativo ,
                                       String AV40Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV23DynamicFiltersIgnoreFirst ,
                                       bool AV22DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFCN2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_GRUPOFUNCAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GRUPOFUNCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A619GrupoFuncao_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_GRUPOFUNCAO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A620GrupoFuncao_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "GRUPOFUNCAO_NOME", A620GrupoFuncao_Nome);
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavGrupofuncao_areatrabalhocod.ItemCount > 0 )
         {
            AV36GrupoFuncao_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavGrupofuncao_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV14DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV14DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV17DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV17DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV20DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector3", AV20DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFCN2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV40Pgmname = "PromptGeral_Grupo_Funcao";
         context.Gx_err = 0;
      }

      protected void RFCN2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 69;
         /* Execute user event: E23CN2 */
         E23CN2 ();
         nGXsfl_69_idx = 1;
         sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
         SubsflControlProps_692( ) ;
         nGXsfl_69_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_692( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV14DynamicFiltersSelector1 ,
                                                 AV15GrupoFuncao_Nome1 ,
                                                 AV16DynamicFiltersEnabled2 ,
                                                 AV17DynamicFiltersSelector2 ,
                                                 AV18GrupoFuncao_Nome2 ,
                                                 AV19DynamicFiltersEnabled3 ,
                                                 AV20DynamicFiltersSelector3 ,
                                                 AV21GrupoFuncao_Nome3 ,
                                                 AV29TFGrupoFuncao_Nome_Sel ,
                                                 AV28TFGrupoFuncao_Nome ,
                                                 A620GrupoFuncao_Nome ,
                                                 AV13OrderedDsc ,
                                                 A2179GrupoFuncao_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                                 A625GrupoFuncao_Ativo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            lV15GrupoFuncao_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV15GrupoFuncao_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GrupoFuncao_Nome1", AV15GrupoFuncao_Nome1);
            lV18GrupoFuncao_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV18GrupoFuncao_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GrupoFuncao_Nome2", AV18GrupoFuncao_Nome2);
            lV21GrupoFuncao_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV21GrupoFuncao_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GrupoFuncao_Nome3", AV21GrupoFuncao_Nome3);
            lV28TFGrupoFuncao_Nome = StringUtil.Concat( StringUtil.RTrim( AV28TFGrupoFuncao_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFGrupoFuncao_Nome", AV28TFGrupoFuncao_Nome);
            /* Using cursor H00CN4 */
            pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV15GrupoFuncao_Nome1, lV18GrupoFuncao_Nome2, lV21GrupoFuncao_Nome3, lV28TFGrupoFuncao_Nome, AV29TFGrupoFuncao_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_69_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2179GrupoFuncao_AreaTrabalhoCod = H00CN4_A2179GrupoFuncao_AreaTrabalhoCod[0];
               n2179GrupoFuncao_AreaTrabalhoCod = H00CN4_n2179GrupoFuncao_AreaTrabalhoCod[0];
               A625GrupoFuncao_Ativo = H00CN4_A625GrupoFuncao_Ativo[0];
               A620GrupoFuncao_Nome = H00CN4_A620GrupoFuncao_Nome[0];
               A619GrupoFuncao_Codigo = H00CN4_A619GrupoFuncao_Codigo[0];
               /* Execute user event: E24CN2 */
               E24CN2 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 69;
            WBCN0( ) ;
         }
         nGXsfl_69_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV14DynamicFiltersSelector1 ,
                                              AV15GrupoFuncao_Nome1 ,
                                              AV16DynamicFiltersEnabled2 ,
                                              AV17DynamicFiltersSelector2 ,
                                              AV18GrupoFuncao_Nome2 ,
                                              AV19DynamicFiltersEnabled3 ,
                                              AV20DynamicFiltersSelector3 ,
                                              AV21GrupoFuncao_Nome3 ,
                                              AV29TFGrupoFuncao_Nome_Sel ,
                                              AV28TFGrupoFuncao_Nome ,
                                              A620GrupoFuncao_Nome ,
                                              AV13OrderedDsc ,
                                              A2179GrupoFuncao_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A625GrupoFuncao_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV15GrupoFuncao_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV15GrupoFuncao_Nome1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GrupoFuncao_Nome1", AV15GrupoFuncao_Nome1);
         lV18GrupoFuncao_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV18GrupoFuncao_Nome2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GrupoFuncao_Nome2", AV18GrupoFuncao_Nome2);
         lV21GrupoFuncao_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV21GrupoFuncao_Nome3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GrupoFuncao_Nome3", AV21GrupoFuncao_Nome3);
         lV28TFGrupoFuncao_Nome = StringUtil.Concat( StringUtil.RTrim( AV28TFGrupoFuncao_Nome), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFGrupoFuncao_Nome", AV28TFGrupoFuncao_Nome);
         /* Using cursor H00CN5 */
         pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV15GrupoFuncao_Nome1, lV18GrupoFuncao_Nome2, lV21GrupoFuncao_Nome3, lV28TFGrupoFuncao_Nome, AV29TFGrupoFuncao_Nome_Sel});
         GRID_nRecordCount = H00CN5_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV15GrupoFuncao_Nome1, AV17DynamicFiltersSelector2, AV18GrupoFuncao_Nome2, AV20DynamicFiltersSelector3, AV21GrupoFuncao_Nome3, AV16DynamicFiltersEnabled2, AV19DynamicFiltersEnabled3, AV13OrderedDsc, AV28TFGrupoFuncao_Nome, AV29TFGrupoFuncao_Nome_Sel, AV6WWPContext, AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace, AV36GrupoFuncao_AreaTrabalhoCod, AV26GrupoFuncao_Ativo, AV40Pgmname, AV10GridState, AV23DynamicFiltersIgnoreFirst, AV22DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV15GrupoFuncao_Nome1, AV17DynamicFiltersSelector2, AV18GrupoFuncao_Nome2, AV20DynamicFiltersSelector3, AV21GrupoFuncao_Nome3, AV16DynamicFiltersEnabled2, AV19DynamicFiltersEnabled3, AV13OrderedDsc, AV28TFGrupoFuncao_Nome, AV29TFGrupoFuncao_Nome_Sel, AV6WWPContext, AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace, AV36GrupoFuncao_AreaTrabalhoCod, AV26GrupoFuncao_Ativo, AV40Pgmname, AV10GridState, AV23DynamicFiltersIgnoreFirst, AV22DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV15GrupoFuncao_Nome1, AV17DynamicFiltersSelector2, AV18GrupoFuncao_Nome2, AV20DynamicFiltersSelector3, AV21GrupoFuncao_Nome3, AV16DynamicFiltersEnabled2, AV19DynamicFiltersEnabled3, AV13OrderedDsc, AV28TFGrupoFuncao_Nome, AV29TFGrupoFuncao_Nome_Sel, AV6WWPContext, AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace, AV36GrupoFuncao_AreaTrabalhoCod, AV26GrupoFuncao_Ativo, AV40Pgmname, AV10GridState, AV23DynamicFiltersIgnoreFirst, AV22DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV15GrupoFuncao_Nome1, AV17DynamicFiltersSelector2, AV18GrupoFuncao_Nome2, AV20DynamicFiltersSelector3, AV21GrupoFuncao_Nome3, AV16DynamicFiltersEnabled2, AV19DynamicFiltersEnabled3, AV13OrderedDsc, AV28TFGrupoFuncao_Nome, AV29TFGrupoFuncao_Nome_Sel, AV6WWPContext, AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace, AV36GrupoFuncao_AreaTrabalhoCod, AV26GrupoFuncao_Ativo, AV40Pgmname, AV10GridState, AV23DynamicFiltersIgnoreFirst, AV22DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV15GrupoFuncao_Nome1, AV17DynamicFiltersSelector2, AV18GrupoFuncao_Nome2, AV20DynamicFiltersSelector3, AV21GrupoFuncao_Nome3, AV16DynamicFiltersEnabled2, AV19DynamicFiltersEnabled3, AV13OrderedDsc, AV28TFGrupoFuncao_Nome, AV29TFGrupoFuncao_Nome_Sel, AV6WWPContext, AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace, AV36GrupoFuncao_AreaTrabalhoCod, AV26GrupoFuncao_Ativo, AV40Pgmname, AV10GridState, AV23DynamicFiltersIgnoreFirst, AV22DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPCN0( )
      {
         /* Before Start, stand alone formulas. */
         AV40Pgmname = "PromptGeral_Grupo_Funcao";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22CN2 */
         E22CN2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV31DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vGRUPOFUNCAO_NOMETITLEFILTERDATA"), AV27GrupoFuncao_NomeTitleFilterData);
            /* Read variables values. */
            dynavGrupofuncao_areatrabalhocod.Name = dynavGrupofuncao_areatrabalhocod_Internalname;
            dynavGrupofuncao_areatrabalhocod.CurrentValue = cgiGet( dynavGrupofuncao_areatrabalhocod_Internalname);
            AV36GrupoFuncao_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynavGrupofuncao_areatrabalhocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0)));
            AV26GrupoFuncao_Ativo = StringUtil.StrToBool( cgiGet( chkavGrupofuncao_ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26GrupoFuncao_Ativo", AV26GrupoFuncao_Ativo);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV14DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            AV15GrupoFuncao_Nome1 = StringUtil.Upper( cgiGet( edtavGrupofuncao_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GrupoFuncao_Nome1", AV15GrupoFuncao_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV17DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
            AV18GrupoFuncao_Nome2 = StringUtil.Upper( cgiGet( edtavGrupofuncao_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GrupoFuncao_Nome2", AV18GrupoFuncao_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV20DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector3", AV20DynamicFiltersSelector3);
            AV21GrupoFuncao_Nome3 = StringUtil.Upper( cgiGet( edtavGrupofuncao_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GrupoFuncao_Nome3", AV21GrupoFuncao_Nome3);
            AV16DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersEnabled2", AV16DynamicFiltersEnabled2);
            AV19DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled3", AV19DynamicFiltersEnabled3);
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            AV28TFGrupoFuncao_Nome = StringUtil.Upper( cgiGet( edtavTfgrupofuncao_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFGrupoFuncao_Nome", AV28TFGrupoFuncao_Nome);
            AV29TFGrupoFuncao_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfgrupofuncao_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFGrupoFuncao_Nome_Sel", AV29TFGrupoFuncao_Nome_Sel);
            AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace = cgiGet( edtavDdo_grupofuncao_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace", AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_69 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_69"), ",", "."));
            AV33GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV34GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_grupofuncao_nome_Caption = cgiGet( "DDO_GRUPOFUNCAO_NOME_Caption");
            Ddo_grupofuncao_nome_Tooltip = cgiGet( "DDO_GRUPOFUNCAO_NOME_Tooltip");
            Ddo_grupofuncao_nome_Cls = cgiGet( "DDO_GRUPOFUNCAO_NOME_Cls");
            Ddo_grupofuncao_nome_Filteredtext_set = cgiGet( "DDO_GRUPOFUNCAO_NOME_Filteredtext_set");
            Ddo_grupofuncao_nome_Selectedvalue_set = cgiGet( "DDO_GRUPOFUNCAO_NOME_Selectedvalue_set");
            Ddo_grupofuncao_nome_Dropdownoptionstype = cgiGet( "DDO_GRUPOFUNCAO_NOME_Dropdownoptionstype");
            Ddo_grupofuncao_nome_Titlecontrolidtoreplace = cgiGet( "DDO_GRUPOFUNCAO_NOME_Titlecontrolidtoreplace");
            Ddo_grupofuncao_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GRUPOFUNCAO_NOME_Includesortasc"));
            Ddo_grupofuncao_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GRUPOFUNCAO_NOME_Includesortdsc"));
            Ddo_grupofuncao_nome_Sortedstatus = cgiGet( "DDO_GRUPOFUNCAO_NOME_Sortedstatus");
            Ddo_grupofuncao_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GRUPOFUNCAO_NOME_Includefilter"));
            Ddo_grupofuncao_nome_Filtertype = cgiGet( "DDO_GRUPOFUNCAO_NOME_Filtertype");
            Ddo_grupofuncao_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GRUPOFUNCAO_NOME_Filterisrange"));
            Ddo_grupofuncao_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GRUPOFUNCAO_NOME_Includedatalist"));
            Ddo_grupofuncao_nome_Datalisttype = cgiGet( "DDO_GRUPOFUNCAO_NOME_Datalisttype");
            Ddo_grupofuncao_nome_Datalistfixedvalues = cgiGet( "DDO_GRUPOFUNCAO_NOME_Datalistfixedvalues");
            Ddo_grupofuncao_nome_Datalistproc = cgiGet( "DDO_GRUPOFUNCAO_NOME_Datalistproc");
            Ddo_grupofuncao_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_GRUPOFUNCAO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_grupofuncao_nome_Sortasc = cgiGet( "DDO_GRUPOFUNCAO_NOME_Sortasc");
            Ddo_grupofuncao_nome_Sortdsc = cgiGet( "DDO_GRUPOFUNCAO_NOME_Sortdsc");
            Ddo_grupofuncao_nome_Loadingdata = cgiGet( "DDO_GRUPOFUNCAO_NOME_Loadingdata");
            Ddo_grupofuncao_nome_Cleanfilter = cgiGet( "DDO_GRUPOFUNCAO_NOME_Cleanfilter");
            Ddo_grupofuncao_nome_Rangefilterfrom = cgiGet( "DDO_GRUPOFUNCAO_NOME_Rangefilterfrom");
            Ddo_grupofuncao_nome_Rangefilterto = cgiGet( "DDO_GRUPOFUNCAO_NOME_Rangefilterto");
            Ddo_grupofuncao_nome_Noresultsfound = cgiGet( "DDO_GRUPOFUNCAO_NOME_Noresultsfound");
            Ddo_grupofuncao_nome_Searchbuttontext = cgiGet( "DDO_GRUPOFUNCAO_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_grupofuncao_nome_Activeeventkey = cgiGet( "DDO_GRUPOFUNCAO_NOME_Activeeventkey");
            Ddo_grupofuncao_nome_Filteredtext_get = cgiGet( "DDO_GRUPOFUNCAO_NOME_Filteredtext_get");
            Ddo_grupofuncao_nome_Selectedvalue_get = cgiGet( "DDO_GRUPOFUNCAO_NOME_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV14DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGRUPOFUNCAO_NOME1"), AV15GrupoFuncao_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV17DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGRUPOFUNCAO_NOME2"), AV18GrupoFuncao_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV20DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vGRUPOFUNCAO_NOME3"), AV21GrupoFuncao_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV16DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV19DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGRUPOFUNCAO_NOME"), AV28TFGrupoFuncao_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFGRUPOFUNCAO_NOME_SEL"), AV29TFGrupoFuncao_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22CN2 */
         E22CN2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22CN2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV14DynamicFiltersSelector1 = "GRUPOFUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV17DynamicFiltersSelector2 = "GRUPOFUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector3 = "GRUPOFUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector3", AV20DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfgrupofuncao_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfgrupofuncao_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfgrupofuncao_nome_Visible), 5, 0)));
         edtavTfgrupofuncao_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfgrupofuncao_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfgrupofuncao_nome_sel_Visible), 5, 0)));
         Ddo_grupofuncao_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_GrupoFuncao_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_nome_Internalname, "TitleControlIdToReplace", Ddo_grupofuncao_nome_Titlecontrolidtoreplace);
         AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace = Ddo_grupofuncao_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace", AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace);
         edtavDdo_grupofuncao_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_grupofuncao_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_grupofuncao_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Grupo de Func�es";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV31DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV31DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E23CN2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV27GrupoFuncao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtGrupoFuncao_Nome_Titleformat = 2;
         edtGrupoFuncao_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoFuncao_Nome_Internalname, "Title", edtGrupoFuncao_Nome_Title);
         AV33GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GridCurrentPage), 10, 0)));
         AV34GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27GrupoFuncao_NomeTitleFilterData", AV27GrupoFuncao_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11CN2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV32PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV32PageToGo) ;
         }
      }

      protected void E12CN2( )
      {
         /* Ddo_grupofuncao_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_grupofuncao_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_grupofuncao_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_nome_Internalname, "SortedStatus", Ddo_grupofuncao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_grupofuncao_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_grupofuncao_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_nome_Internalname, "SortedStatus", Ddo_grupofuncao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_grupofuncao_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV28TFGrupoFuncao_Nome = Ddo_grupofuncao_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFGrupoFuncao_Nome", AV28TFGrupoFuncao_Nome);
            AV29TFGrupoFuncao_Nome_Sel = Ddo_grupofuncao_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFGrupoFuncao_Nome_Sel", AV29TFGrupoFuncao_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E24CN2( )
      {
         /* Grid_Load Routine */
         AV24Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV24Select);
         AV39Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 69;
         }
         sendrow_692( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_69_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(69, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E25CN2 */
         E25CN2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25CN2( )
      {
         /* Enter Routine */
         AV7InOutGrupoFuncao_Codigo = A619GrupoFuncao_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutGrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutGrupoFuncao_Codigo), 6, 0)));
         AV8InOutGrupoFuncao_Nome = A620GrupoFuncao_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutGrupoFuncao_Nome", AV8InOutGrupoFuncao_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutGrupoFuncao_Codigo,(String)AV8InOutGrupoFuncao_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E17CN2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV16DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersEnabled2", AV16DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E13CN2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV22DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersRemoving", AV22DynamicFiltersRemoving);
         AV23DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersIgnoreFirst", AV23DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersRemoving", AV22DynamicFiltersRemoving);
         AV23DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersIgnoreFirst", AV23DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV15GrupoFuncao_Nome1, AV17DynamicFiltersSelector2, AV18GrupoFuncao_Nome2, AV20DynamicFiltersSelector3, AV21GrupoFuncao_Nome3, AV16DynamicFiltersEnabled2, AV19DynamicFiltersEnabled3, AV13OrderedDsc, AV28TFGrupoFuncao_Nome, AV29TFGrupoFuncao_Nome_Sel, AV6WWPContext, AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace, AV36GrupoFuncao_AreaTrabalhoCod, AV26GrupoFuncao_Ativo, AV40Pgmname, AV10GridState, AV23DynamicFiltersIgnoreFirst, AV22DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E18CN2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19CN2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV19DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled3", AV19DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E14CN2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV22DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersRemoving", AV22DynamicFiltersRemoving);
         AV16DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersEnabled2", AV16DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersRemoving", AV22DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV15GrupoFuncao_Nome1, AV17DynamicFiltersSelector2, AV18GrupoFuncao_Nome2, AV20DynamicFiltersSelector3, AV21GrupoFuncao_Nome3, AV16DynamicFiltersEnabled2, AV19DynamicFiltersEnabled3, AV13OrderedDsc, AV28TFGrupoFuncao_Nome, AV29TFGrupoFuncao_Nome_Sel, AV6WWPContext, AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace, AV36GrupoFuncao_AreaTrabalhoCod, AV26GrupoFuncao_Ativo, AV40Pgmname, AV10GridState, AV23DynamicFiltersIgnoreFirst, AV22DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E20CN2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15CN2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV22DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersRemoving", AV22DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled3", AV19DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersRemoving", AV22DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14DynamicFiltersSelector1, AV15GrupoFuncao_Nome1, AV17DynamicFiltersSelector2, AV18GrupoFuncao_Nome2, AV20DynamicFiltersSelector3, AV21GrupoFuncao_Nome3, AV16DynamicFiltersEnabled2, AV19DynamicFiltersEnabled3, AV13OrderedDsc, AV28TFGrupoFuncao_Nome, AV29TFGrupoFuncao_Nome_Sel, AV6WWPContext, AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace, AV36GrupoFuncao_AreaTrabalhoCod, AV26GrupoFuncao_Ativo, AV40Pgmname, AV10GridState, AV23DynamicFiltersIgnoreFirst, AV22DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21CN2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16CN2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavGrupofuncao_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupofuncao_areatrabalhocod_Internalname, "Values", dynavGrupofuncao_areatrabalhocod.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S192( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_grupofuncao_nome_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_nome_Internalname, "SortedStatus", Ddo_grupofuncao_nome_Sortedstatus);
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavGrupofuncao_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGrupofuncao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGrupofuncao_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "GRUPOFUNCAO_NOME") == 0 )
         {
            edtavGrupofuncao_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGrupofuncao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGrupofuncao_nome1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavGrupofuncao_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGrupofuncao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGrupofuncao_nome2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "GRUPOFUNCAO_NOME") == 0 )
         {
            edtavGrupofuncao_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGrupofuncao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGrupofuncao_nome2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavGrupofuncao_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGrupofuncao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGrupofuncao_nome3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector3, "GRUPOFUNCAO_NOME") == 0 )
         {
            edtavGrupofuncao_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGrupofuncao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGrupofuncao_nome3_Visible), 5, 0)));
         }
      }

      protected void S172( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV16DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersEnabled2", AV16DynamicFiltersEnabled2);
         AV17DynamicFiltersSelector2 = "GRUPOFUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
         AV18GrupoFuncao_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GrupoFuncao_Nome2", AV18GrupoFuncao_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled3", AV19DynamicFiltersEnabled3);
         AV20DynamicFiltersSelector3 = "GRUPOFUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector3", AV20DynamicFiltersSelector3);
         AV21GrupoFuncao_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GrupoFuncao_Nome3", AV21GrupoFuncao_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S182( )
      {
         /* 'CLEANFILTERS' Routine */
         AV36GrupoFuncao_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36GrupoFuncao_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0)));
         AV26GrupoFuncao_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26GrupoFuncao_Ativo", AV26GrupoFuncao_Ativo);
         AV28TFGrupoFuncao_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFGrupoFuncao_Nome", AV28TFGrupoFuncao_Nome);
         Ddo_grupofuncao_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_nome_Internalname, "FilteredText_set", Ddo_grupofuncao_nome_Filteredtext_set);
         AV29TFGrupoFuncao_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFGrupoFuncao_Nome_Sel", AV29TFGrupoFuncao_Nome_Sel);
         Ddo_grupofuncao_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_nome_Internalname, "SelectedValue_set", Ddo_grupofuncao_nome_Selectedvalue_set);
         AV14DynamicFiltersSelector1 = "GRUPOFUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         AV15GrupoFuncao_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GrupoFuncao_Nome1", AV15GrupoFuncao_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV14DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "GRUPOFUNCAO_NOME") == 0 )
            {
               AV15GrupoFuncao_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GrupoFuncao_Nome1", AV15GrupoFuncao_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV16DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersEnabled2", AV16DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV17DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "GRUPOFUNCAO_NOME") == 0 )
               {
                  AV18GrupoFuncao_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GrupoFuncao_Nome2", AV18GrupoFuncao_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV19DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled3", AV19DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV20DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector3", AV20DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV20DynamicFiltersSelector3, "GRUPOFUNCAO_NOME") == 0 )
                  {
                     AV21GrupoFuncao_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GrupoFuncao_Nome3", AV21GrupoFuncao_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV22DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV36GrupoFuncao_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "GRUPOFUNCAO_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (false==AV26GrupoFuncao_Ativo) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "GRUPOFUNCAO_ATIVO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.BoolToStr( AV26GrupoFuncao_Ativo);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFGrupoFuncao_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGRUPOFUNCAO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV28TFGrupoFuncao_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFGrupoFuncao_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGRUPOFUNCAO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV29TFGrupoFuncao_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV40Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S162( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV23DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV14DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "GRUPOFUNCAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV15GrupoFuncao_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV15GrupoFuncao_Nome1;
            }
            if ( AV22DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV16DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV17DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "GRUPOFUNCAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18GrupoFuncao_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18GrupoFuncao_Nome2;
            }
            if ( AV22DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector3, "GRUPOFUNCAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21GrupoFuncao_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21GrupoFuncao_Nome3;
            }
            if ( AV22DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_CN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_CN2( true) ;
         }
         else
         {
            wb_table2_5_CN2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_CN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_63_CN2( true) ;
         }
         else
         {
            wb_table3_63_CN2( false) ;
         }
         return  ;
      }

      protected void wb_table3_63_CN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_CN2e( true) ;
         }
         else
         {
            wb_table1_2_CN2e( false) ;
         }
      }

      protected void wb_table3_63_CN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_66_CN2( true) ;
         }
         else
         {
            wb_table4_66_CN2( false) ;
         }
         return  ;
      }

      protected void wb_table4_66_CN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_63_CN2e( true) ;
         }
         else
         {
            wb_table3_63_CN2e( false) ;
         }
      }

      protected void wb_table4_66_CN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"69\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de Fun��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGrupoFuncao_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtGrupoFuncao_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGrupoFuncao_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV24Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A619GrupoFuncao_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A620GrupoFuncao_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGrupoFuncao_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGrupoFuncao_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 69 )
         {
            wbEnd = 0;
            nRC_GXsfl_69 = (short)(nGXsfl_69_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_66_CN2e( true) ;
         }
         else
         {
            wb_table4_66_CN2e( false) ;
         }
      }

      protected void wb_table2_5_CN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_8_CN2( true) ;
         }
         else
         {
            wb_table5_8_CN2( false) ;
         }
         return  ;
      }

      protected void wb_table5_8_CN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_CN2e( true) ;
         }
         else
         {
            wb_table2_5_CN2e( false) ;
         }
      }

      protected void wb_table5_8_CN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextgrupofuncao_areatrabalhocod_Internalname, "�rea de Trabalhlho", "", "", lblFiltertextgrupofuncao_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavGrupofuncao_areatrabalhocod, dynavGrupofuncao_areatrabalhocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0)), 1, dynavGrupofuncao_areatrabalhocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "", true, "HLP_PromptGeral_Grupo_Funcao.htm");
            dynavGrupofuncao_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36GrupoFuncao_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavGrupofuncao_areatrabalhocod_Internalname, "Values", (String)(dynavGrupofuncao_areatrabalhocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextgrupofuncao_ativo_Internalname, "Ativo?", "", "", lblFiltertextgrupofuncao_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavGrupofuncao_ativo_Internalname, StringUtil.BoolToStr( AV26GrupoFuncao_Ativo), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(21, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_23_CN2( true) ;
         }
         else
         {
            wb_table6_23_CN2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_CN2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_8_CN2e( true) ;
         }
         else
         {
            wb_table5_8_CN2e( false) ;
         }
      }

      protected void wb_table6_23_CN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV14DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_PromptGeral_Grupo_Funcao.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGrupofuncao_nome1_Internalname, AV15GrupoFuncao_Nome1, StringUtil.RTrim( context.localUtil.Format( AV15GrupoFuncao_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGrupofuncao_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGrupofuncao_nome1_Visible, 1, 0, "text", "", 380, "px", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGeral_Grupo_Funcao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV17DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_PromptGeral_Grupo_Funcao.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGrupofuncao_nome2_Internalname, AV18GrupoFuncao_Nome2, StringUtil.RTrim( context.localUtil.Format( AV18GrupoFuncao_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGrupofuncao_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGrupofuncao_nome2_Visible, 1, 0, "text", "", 380, "px", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGeral_Grupo_Funcao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_PromptGeral_Grupo_Funcao.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGrupofuncao_nome3_Internalname, AV21GrupoFuncao_Nome3, StringUtil.RTrim( context.localUtil.Format( AV21GrupoFuncao_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGrupofuncao_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavGrupofuncao_nome3_Visible, 1, 0, "text", "", 380, "px", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptGeral_Grupo_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_CN2e( true) ;
         }
         else
         {
            wb_table6_23_CN2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutGrupoFuncao_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutGrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutGrupoFuncao_Codigo), 6, 0)));
         AV8InOutGrupoFuncao_Nome = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutGrupoFuncao_Nome", AV8InOutGrupoFuncao_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PACN2( ) ;
         WSCN2( ) ;
         WECN2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020515042712");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptgeral_grupo_funcao.js", "?2020515042712");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_692( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_69_idx;
         edtGrupoFuncao_Codigo_Internalname = "GRUPOFUNCAO_CODIGO_"+sGXsfl_69_idx;
         edtGrupoFuncao_Nome_Internalname = "GRUPOFUNCAO_NOME_"+sGXsfl_69_idx;
      }

      protected void SubsflControlProps_fel_692( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_69_fel_idx;
         edtGrupoFuncao_Codigo_Internalname = "GRUPOFUNCAO_CODIGO_"+sGXsfl_69_fel_idx;
         edtGrupoFuncao_Nome_Internalname = "GRUPOFUNCAO_NOME_"+sGXsfl_69_fel_idx;
      }

      protected void sendrow_692( )
      {
         SubsflControlProps_692( ) ;
         WBCN0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_69_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_69_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_69_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 70,'',false,'',69)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV24Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV24Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV39Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV24Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV24Select)) ? AV39Select_GXI : context.PathToRelativeUrl( AV24Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_69_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV24Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGrupoFuncao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A619GrupoFuncao_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGrupoFuncao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGrupoFuncao_Nome_Internalname,(String)A620GrupoFuncao_Nome,StringUtil.RTrim( context.localUtil.Format( A620GrupoFuncao_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGrupoFuncao_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)80,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_GRUPOFUNCAO_CODIGO"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_GRUPOFUNCAO_NOME"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, StringUtil.RTrim( context.localUtil.Format( A620GrupoFuncao_Nome, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_69_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_69_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_69_idx+1));
            sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
            SubsflControlProps_692( ) ;
         }
         /* End function sendrow_692 */
      }

      protected void init_default_properties( )
      {
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextgrupofuncao_areatrabalhocod_Internalname = "FILTERTEXTGRUPOFUNCAO_AREATRABALHOCOD";
         dynavGrupofuncao_areatrabalhocod_Internalname = "vGRUPOFUNCAO_AREATRABALHOCOD";
         lblFiltertextgrupofuncao_ativo_Internalname = "FILTERTEXTGRUPOFUNCAO_ATIVO";
         chkavGrupofuncao_ativo_Internalname = "vGRUPOFUNCAO_ATIVO";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavGrupofuncao_nome1_Internalname = "vGRUPOFUNCAO_NOME1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavGrupofuncao_nome2_Internalname = "vGRUPOFUNCAO_NOME2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavGrupofuncao_nome3_Internalname = "vGRUPOFUNCAO_NOME3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtGrupoFuncao_Codigo_Internalname = "GRUPOFUNCAO_CODIGO";
         edtGrupoFuncao_Nome_Internalname = "GRUPOFUNCAO_NOME";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         edtavTfgrupofuncao_nome_Internalname = "vTFGRUPOFUNCAO_NOME";
         edtavTfgrupofuncao_nome_sel_Internalname = "vTFGRUPOFUNCAO_NOME_SEL";
         Ddo_grupofuncao_nome_Internalname = "DDO_GRUPOFUNCAO_NOME";
         edtavDdo_grupofuncao_nometitlecontrolidtoreplace_Internalname = "vDDO_GRUPOFUNCAO_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtGrupoFuncao_Nome_Jsonclick = "";
         edtGrupoFuncao_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavGrupofuncao_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavGrupofuncao_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavGrupofuncao_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavGrupofuncao_areatrabalhocod_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtGrupoFuncao_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavGrupofuncao_nome3_Visible = 1;
         edtavGrupofuncao_nome2_Visible = 1;
         edtavGrupofuncao_nome1_Visible = 1;
         edtGrupoFuncao_Nome_Title = "Nome";
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavGrupofuncao_ativo.Caption = "";
         edtavDdo_grupofuncao_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfgrupofuncao_nome_sel_Jsonclick = "";
         edtavTfgrupofuncao_nome_sel_Visible = 1;
         edtavTfgrupofuncao_nome_Jsonclick = "";
         edtavTfgrupofuncao_nome_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_grupofuncao_nome_Searchbuttontext = "Pesquisar";
         Ddo_grupofuncao_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_grupofuncao_nome_Rangefilterto = "At�";
         Ddo_grupofuncao_nome_Rangefilterfrom = "Desde";
         Ddo_grupofuncao_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_grupofuncao_nome_Loadingdata = "Carregando dados...";
         Ddo_grupofuncao_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_grupofuncao_nome_Sortasc = "Ordenar de A � Z";
         Ddo_grupofuncao_nome_Datalistupdateminimumcharacters = 0;
         Ddo_grupofuncao_nome_Datalistproc = "GetPromptGeral_Grupo_FuncaoFilterData";
         Ddo_grupofuncao_nome_Datalistfixedvalues = "";
         Ddo_grupofuncao_nome_Datalisttype = "Dynamic";
         Ddo_grupofuncao_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_grupofuncao_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_grupofuncao_nome_Filtertype = "Character";
         Ddo_grupofuncao_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_grupofuncao_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_grupofuncao_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_grupofuncao_nome_Titlecontrolidtoreplace = "";
         Ddo_grupofuncao_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_grupofuncao_nome_Cls = "ColumnSettings";
         Ddo_grupofuncao_nome_Tooltip = "Op��es";
         Ddo_grupofuncao_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Grupo de Func�es";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36GrupoFuncao_AreaTrabalhoCod',fld:'vGRUPOFUNCAO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26GrupoFuncao_Ativo',fld:'vGRUPOFUNCAO_ATIVO',pic:'',nv:false},{av:'AV28TFGrupoFuncao_Nome',fld:'vTFGRUPOFUNCAO_NOME',pic:'@!',nv:''},{av:'AV29TFGrupoFuncao_Nome_Sel',fld:'vTFGRUPOFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15GrupoFuncao_Nome1',fld:'vGRUPOFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV18GrupoFuncao_Nome2',fld:'vGRUPOFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV21GrupoFuncao_Nome3',fld:'vGRUPOFUNCAO_NOME3',pic:'@!',nv:''}],oparms:[{av:'AV27GrupoFuncao_NomeTitleFilterData',fld:'vGRUPOFUNCAO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtGrupoFuncao_Nome_Titleformat',ctrl:'GRUPOFUNCAO_NOME',prop:'Titleformat'},{av:'edtGrupoFuncao_Nome_Title',ctrl:'GRUPOFUNCAO_NOME',prop:'Title'},{av:'AV33GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV34GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11CN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15GrupoFuncao_Nome1',fld:'vGRUPOFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV18GrupoFuncao_Nome2',fld:'vGRUPOFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV21GrupoFuncao_Nome3',fld:'vGRUPOFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFGrupoFuncao_Nome',fld:'vTFGRUPOFUNCAO_NOME',pic:'@!',nv:''},{av:'AV29TFGrupoFuncao_Nome_Sel',fld:'vTFGRUPOFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36GrupoFuncao_AreaTrabalhoCod',fld:'vGRUPOFUNCAO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26GrupoFuncao_Ativo',fld:'vGRUPOFUNCAO_ATIVO',pic:'',nv:false},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV22DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_GRUPOFUNCAO_NOME.ONOPTIONCLICKED","{handler:'E12CN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15GrupoFuncao_Nome1',fld:'vGRUPOFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV18GrupoFuncao_Nome2',fld:'vGRUPOFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV21GrupoFuncao_Nome3',fld:'vGRUPOFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFGrupoFuncao_Nome',fld:'vTFGRUPOFUNCAO_NOME',pic:'@!',nv:''},{av:'AV29TFGrupoFuncao_Nome_Sel',fld:'vTFGRUPOFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36GrupoFuncao_AreaTrabalhoCod',fld:'vGRUPOFUNCAO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26GrupoFuncao_Ativo',fld:'vGRUPOFUNCAO_ATIVO',pic:'',nv:false},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV22DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_grupofuncao_nome_Activeeventkey',ctrl:'DDO_GRUPOFUNCAO_NOME',prop:'ActiveEventKey'},{av:'Ddo_grupofuncao_nome_Filteredtext_get',ctrl:'DDO_GRUPOFUNCAO_NOME',prop:'FilteredText_get'},{av:'Ddo_grupofuncao_nome_Selectedvalue_get',ctrl:'DDO_GRUPOFUNCAO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_grupofuncao_nome_Sortedstatus',ctrl:'DDO_GRUPOFUNCAO_NOME',prop:'SortedStatus'},{av:'AV28TFGrupoFuncao_Nome',fld:'vTFGRUPOFUNCAO_NOME',pic:'@!',nv:''},{av:'AV29TFGrupoFuncao_Nome_Sel',fld:'vTFGRUPOFUNCAO_NOME_SEL',pic:'@!',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E24CN2',iparms:[],oparms:[{av:'AV24Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E25CN2',iparms:[{av:'A619GrupoFuncao_Codigo',fld:'GRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A620GrupoFuncao_Nome',fld:'GRUPOFUNCAO_NOME',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutGrupoFuncao_Codigo',fld:'vINOUTGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutGrupoFuncao_Nome',fld:'vINOUTGRUPOFUNCAO_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17CN2',iparms:[],oparms:[{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13CN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15GrupoFuncao_Nome1',fld:'vGRUPOFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV18GrupoFuncao_Nome2',fld:'vGRUPOFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV21GrupoFuncao_Nome3',fld:'vGRUPOFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFGrupoFuncao_Nome',fld:'vTFGRUPOFUNCAO_NOME',pic:'@!',nv:''},{av:'AV29TFGrupoFuncao_Nome_Sel',fld:'vTFGRUPOFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36GrupoFuncao_AreaTrabalhoCod',fld:'vGRUPOFUNCAO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26GrupoFuncao_Ativo',fld:'vGRUPOFUNCAO_ATIVO',pic:'',nv:false},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV22DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV22DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV18GrupoFuncao_Nome2',fld:'vGRUPOFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV21GrupoFuncao_Nome3',fld:'vGRUPOFUNCAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15GrupoFuncao_Nome1',fld:'vGRUPOFUNCAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavGrupofuncao_nome2_Visible',ctrl:'vGRUPOFUNCAO_NOME2',prop:'Visible'},{av:'edtavGrupofuncao_nome3_Visible',ctrl:'vGRUPOFUNCAO_NOME3',prop:'Visible'},{av:'edtavGrupofuncao_nome1_Visible',ctrl:'vGRUPOFUNCAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18CN2',iparms:[{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavGrupofuncao_nome1_Visible',ctrl:'vGRUPOFUNCAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19CN2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14CN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15GrupoFuncao_Nome1',fld:'vGRUPOFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV18GrupoFuncao_Nome2',fld:'vGRUPOFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV21GrupoFuncao_Nome3',fld:'vGRUPOFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFGrupoFuncao_Nome',fld:'vTFGRUPOFUNCAO_NOME',pic:'@!',nv:''},{av:'AV29TFGrupoFuncao_Nome_Sel',fld:'vTFGRUPOFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36GrupoFuncao_AreaTrabalhoCod',fld:'vGRUPOFUNCAO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26GrupoFuncao_Ativo',fld:'vGRUPOFUNCAO_ATIVO',pic:'',nv:false},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV22DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV22DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV18GrupoFuncao_Nome2',fld:'vGRUPOFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV21GrupoFuncao_Nome3',fld:'vGRUPOFUNCAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15GrupoFuncao_Nome1',fld:'vGRUPOFUNCAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavGrupofuncao_nome2_Visible',ctrl:'vGRUPOFUNCAO_NOME2',prop:'Visible'},{av:'edtavGrupofuncao_nome3_Visible',ctrl:'vGRUPOFUNCAO_NOME3',prop:'Visible'},{av:'edtavGrupofuncao_nome1_Visible',ctrl:'vGRUPOFUNCAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20CN2',iparms:[{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavGrupofuncao_nome2_Visible',ctrl:'vGRUPOFUNCAO_NOME2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15CN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15GrupoFuncao_Nome1',fld:'vGRUPOFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV18GrupoFuncao_Nome2',fld:'vGRUPOFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV21GrupoFuncao_Nome3',fld:'vGRUPOFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFGrupoFuncao_Nome',fld:'vTFGRUPOFUNCAO_NOME',pic:'@!',nv:''},{av:'AV29TFGrupoFuncao_Nome_Sel',fld:'vTFGRUPOFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36GrupoFuncao_AreaTrabalhoCod',fld:'vGRUPOFUNCAO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26GrupoFuncao_Ativo',fld:'vGRUPOFUNCAO_ATIVO',pic:'',nv:false},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV22DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV22DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV18GrupoFuncao_Nome2',fld:'vGRUPOFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV21GrupoFuncao_Nome3',fld:'vGRUPOFUNCAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15GrupoFuncao_Nome1',fld:'vGRUPOFUNCAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavGrupofuncao_nome2_Visible',ctrl:'vGRUPOFUNCAO_NOME2',prop:'Visible'},{av:'edtavGrupofuncao_nome3_Visible',ctrl:'vGRUPOFUNCAO_NOME3',prop:'Visible'},{av:'edtavGrupofuncao_nome1_Visible',ctrl:'vGRUPOFUNCAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21CN2',iparms:[{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavGrupofuncao_nome3_Visible',ctrl:'vGRUPOFUNCAO_NOME3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16CN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15GrupoFuncao_Nome1',fld:'vGRUPOFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV18GrupoFuncao_Nome2',fld:'vGRUPOFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV21GrupoFuncao_Nome3',fld:'vGRUPOFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28TFGrupoFuncao_Nome',fld:'vTFGRUPOFUNCAO_NOME',pic:'@!',nv:''},{av:'AV29TFGrupoFuncao_Nome_Sel',fld:'vTFGRUPOFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36GrupoFuncao_AreaTrabalhoCod',fld:'vGRUPOFUNCAO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26GrupoFuncao_Ativo',fld:'vGRUPOFUNCAO_ATIVO',pic:'',nv:false},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV22DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV36GrupoFuncao_AreaTrabalhoCod',fld:'vGRUPOFUNCAO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26GrupoFuncao_Ativo',fld:'vGRUPOFUNCAO_ATIVO',pic:'',nv:false},{av:'AV28TFGrupoFuncao_Nome',fld:'vTFGRUPOFUNCAO_NOME',pic:'@!',nv:''},{av:'Ddo_grupofuncao_nome_Filteredtext_set',ctrl:'DDO_GRUPOFUNCAO_NOME',prop:'FilteredText_set'},{av:'AV29TFGrupoFuncao_Nome_Sel',fld:'vTFGRUPOFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_grupofuncao_nome_Selectedvalue_set',ctrl:'DDO_GRUPOFUNCAO_NOME',prop:'SelectedValue_set'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15GrupoFuncao_Nome1',fld:'vGRUPOFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavGrupofuncao_nome1_Visible',ctrl:'vGRUPOFUNCAO_NOME1',prop:'Visible'},{av:'AV16DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV18GrupoFuncao_Nome2',fld:'vGRUPOFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV20DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV21GrupoFuncao_Nome3',fld:'vGRUPOFUNCAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavGrupofuncao_nome2_Visible',ctrl:'vGRUPOFUNCAO_NOME2',prop:'Visible'},{av:'edtavGrupofuncao_nome3_Visible',ctrl:'vGRUPOFUNCAO_NOME3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutGrupoFuncao_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_grupofuncao_nome_Activeeventkey = "";
         Ddo_grupofuncao_nome_Filteredtext_get = "";
         Ddo_grupofuncao_nome_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV14DynamicFiltersSelector1 = "";
         AV15GrupoFuncao_Nome1 = "";
         AV17DynamicFiltersSelector2 = "";
         AV18GrupoFuncao_Nome2 = "";
         AV20DynamicFiltersSelector3 = "";
         AV21GrupoFuncao_Nome3 = "";
         AV28TFGrupoFuncao_Nome = "";
         AV29TFGrupoFuncao_Nome_Sel = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace = "";
         AV26GrupoFuncao_Ativo = true;
         AV40Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV31DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV27GrupoFuncao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_grupofuncao_nome_Filteredtext_set = "";
         Ddo_grupofuncao_nome_Selectedvalue_set = "";
         Ddo_grupofuncao_nome_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV24Select = "";
         AV39Select_GXI = "";
         A620GrupoFuncao_Nome = "";
         scmdbuf = "";
         H00CN2_A5AreaTrabalho_Codigo = new int[1] ;
         H00CN2_A6AreaTrabalho_Descricao = new String[] {""} ;
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00CN3_A5AreaTrabalho_Codigo = new int[1] ;
         H00CN3_A6AreaTrabalho_Descricao = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV15GrupoFuncao_Nome1 = "";
         lV18GrupoFuncao_Nome2 = "";
         lV21GrupoFuncao_Nome3 = "";
         lV28TFGrupoFuncao_Nome = "";
         H00CN4_A2179GrupoFuncao_AreaTrabalhoCod = new int[1] ;
         H00CN4_n2179GrupoFuncao_AreaTrabalhoCod = new bool[] {false} ;
         H00CN4_A625GrupoFuncao_Ativo = new bool[] {false} ;
         H00CN4_A620GrupoFuncao_Nome = new String[] {""} ;
         H00CN4_A619GrupoFuncao_Codigo = new int[1] ;
         H00CN5_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextgrupofuncao_areatrabalhocod_Jsonclick = "";
         lblFiltertextgrupofuncao_ativo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptgeral_grupo_funcao__default(),
            new Object[][] {
                new Object[] {
               H00CN2_A5AreaTrabalho_Codigo, H00CN2_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00CN3_A5AreaTrabalho_Codigo, H00CN3_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00CN4_A2179GrupoFuncao_AreaTrabalhoCod, H00CN4_n2179GrupoFuncao_AreaTrabalhoCod, H00CN4_A625GrupoFuncao_Ativo, H00CN4_A620GrupoFuncao_Nome, H00CN4_A619GrupoFuncao_Codigo
               }
               , new Object[] {
               H00CN5_AGRID_nRecordCount
               }
            }
         );
         AV40Pgmname = "PromptGeral_Grupo_Funcao";
         /* GeneXus formulas. */
         AV40Pgmname = "PromptGeral_Grupo_Funcao";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_69 ;
      private short nGXsfl_69_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_69_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtGrupoFuncao_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutGrupoFuncao_Codigo ;
      private int wcpOAV7InOutGrupoFuncao_Codigo ;
      private int subGrid_Rows ;
      private int AV36GrupoFuncao_AreaTrabalhoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_grupofuncao_nome_Datalistupdateminimumcharacters ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfgrupofuncao_nome_Visible ;
      private int edtavTfgrupofuncao_nome_sel_Visible ;
      private int edtavDdo_grupofuncao_nometitlecontrolidtoreplace_Visible ;
      private int A619GrupoFuncao_Codigo ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A2179GrupoFuncao_AreaTrabalhoCod ;
      private int AV32PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavGrupofuncao_nome1_Visible ;
      private int edtavGrupofuncao_nome2_Visible ;
      private int edtavGrupofuncao_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV33GridCurrentPage ;
      private long AV34GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_grupofuncao_nome_Activeeventkey ;
      private String Ddo_grupofuncao_nome_Filteredtext_get ;
      private String Ddo_grupofuncao_nome_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_69_idx="0001" ;
      private String AV40Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_grupofuncao_nome_Caption ;
      private String Ddo_grupofuncao_nome_Tooltip ;
      private String Ddo_grupofuncao_nome_Cls ;
      private String Ddo_grupofuncao_nome_Filteredtext_set ;
      private String Ddo_grupofuncao_nome_Selectedvalue_set ;
      private String Ddo_grupofuncao_nome_Dropdownoptionstype ;
      private String Ddo_grupofuncao_nome_Titlecontrolidtoreplace ;
      private String Ddo_grupofuncao_nome_Sortedstatus ;
      private String Ddo_grupofuncao_nome_Filtertype ;
      private String Ddo_grupofuncao_nome_Datalisttype ;
      private String Ddo_grupofuncao_nome_Datalistfixedvalues ;
      private String Ddo_grupofuncao_nome_Datalistproc ;
      private String Ddo_grupofuncao_nome_Sortasc ;
      private String Ddo_grupofuncao_nome_Sortdsc ;
      private String Ddo_grupofuncao_nome_Loadingdata ;
      private String Ddo_grupofuncao_nome_Cleanfilter ;
      private String Ddo_grupofuncao_nome_Rangefilterfrom ;
      private String Ddo_grupofuncao_nome_Rangefilterto ;
      private String Ddo_grupofuncao_nome_Noresultsfound ;
      private String Ddo_grupofuncao_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfgrupofuncao_nome_Internalname ;
      private String edtavTfgrupofuncao_nome_Jsonclick ;
      private String edtavTfgrupofuncao_nome_sel_Internalname ;
      private String edtavTfgrupofuncao_nome_sel_Jsonclick ;
      private String edtavDdo_grupofuncao_nometitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtGrupoFuncao_Codigo_Internalname ;
      private String edtGrupoFuncao_Nome_Internalname ;
      private String scmdbuf ;
      private String chkavGrupofuncao_ativo_Internalname ;
      private String dynavGrupofuncao_areatrabalhocod_Internalname ;
      private String gxwrpcisep ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavGrupofuncao_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavGrupofuncao_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavGrupofuncao_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_grupofuncao_nome_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtGrupoFuncao_Nome_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextgrupofuncao_areatrabalhocod_Internalname ;
      private String lblFiltertextgrupofuncao_areatrabalhocod_Jsonclick ;
      private String dynavGrupofuncao_areatrabalhocod_Jsonclick ;
      private String lblFiltertextgrupofuncao_ativo_Internalname ;
      private String lblFiltertextgrupofuncao_ativo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavGrupofuncao_nome1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavGrupofuncao_nome2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavGrupofuncao_nome3_Jsonclick ;
      private String sGXsfl_69_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtGrupoFuncao_Codigo_Jsonclick ;
      private String edtGrupoFuncao_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV16DynamicFiltersEnabled2 ;
      private bool AV19DynamicFiltersEnabled3 ;
      private bool AV13OrderedDsc ;
      private bool AV26GrupoFuncao_Ativo ;
      private bool AV23DynamicFiltersIgnoreFirst ;
      private bool AV22DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_grupofuncao_nome_Includesortasc ;
      private bool Ddo_grupofuncao_nome_Includesortdsc ;
      private bool Ddo_grupofuncao_nome_Includefilter ;
      private bool Ddo_grupofuncao_nome_Filterisrange ;
      private bool Ddo_grupofuncao_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A625GrupoFuncao_Ativo ;
      private bool n2179GrupoFuncao_AreaTrabalhoCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV24Select_IsBlob ;
      private String AV8InOutGrupoFuncao_Nome ;
      private String wcpOAV8InOutGrupoFuncao_Nome ;
      private String AV14DynamicFiltersSelector1 ;
      private String AV15GrupoFuncao_Nome1 ;
      private String AV17DynamicFiltersSelector2 ;
      private String AV18GrupoFuncao_Nome2 ;
      private String AV20DynamicFiltersSelector3 ;
      private String AV21GrupoFuncao_Nome3 ;
      private String AV28TFGrupoFuncao_Nome ;
      private String AV29TFGrupoFuncao_Nome_Sel ;
      private String AV30ddo_GrupoFuncao_NomeTitleControlIdToReplace ;
      private String AV39Select_GXI ;
      private String A620GrupoFuncao_Nome ;
      private String lV15GrupoFuncao_Nome1 ;
      private String lV18GrupoFuncao_Nome2 ;
      private String lV21GrupoFuncao_Nome3 ;
      private String lV28TFGrupoFuncao_Nome ;
      private String AV24Select ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutGrupoFuncao_Codigo ;
      private String aP1_InOutGrupoFuncao_Nome ;
      private GXCombobox dynavGrupofuncao_areatrabalhocod ;
      private GXCheckbox chkavGrupofuncao_ativo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00CN2_A5AreaTrabalho_Codigo ;
      private String[] H00CN2_A6AreaTrabalho_Descricao ;
      private int[] H00CN3_A5AreaTrabalho_Codigo ;
      private String[] H00CN3_A6AreaTrabalho_Descricao ;
      private int[] H00CN4_A2179GrupoFuncao_AreaTrabalhoCod ;
      private bool[] H00CN4_n2179GrupoFuncao_AreaTrabalhoCod ;
      private bool[] H00CN4_A625GrupoFuncao_Ativo ;
      private String[] H00CN4_A620GrupoFuncao_Nome ;
      private int[] H00CN4_A619GrupoFuncao_Codigo ;
      private long[] H00CN5_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV27GrupoFuncao_NomeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV31DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptgeral_grupo_funcao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00CN4( IGxContext context ,
                                             String AV14DynamicFiltersSelector1 ,
                                             String AV15GrupoFuncao_Nome1 ,
                                             bool AV16DynamicFiltersEnabled2 ,
                                             String AV17DynamicFiltersSelector2 ,
                                             String AV18GrupoFuncao_Nome2 ,
                                             bool AV19DynamicFiltersEnabled3 ,
                                             String AV20DynamicFiltersSelector3 ,
                                             String AV21GrupoFuncao_Nome3 ,
                                             String AV29TFGrupoFuncao_Nome_Sel ,
                                             String AV28TFGrupoFuncao_Nome ,
                                             String A620GrupoFuncao_Nome ,
                                             bool AV13OrderedDsc ,
                                             int A2179GrupoFuncao_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                             bool A625GrupoFuncao_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [11] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [GrupoFuncao_AreaTrabalhoCod], [GrupoFuncao_Ativo], [GrupoFuncao_Nome], [GrupoFuncao_Codigo]";
         sFromString = " FROM [Geral_Grupo_Funcao] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([GrupoFuncao_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         sWhereString = sWhereString + " and ([GrupoFuncao_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "GRUPOFUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15GrupoFuncao_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like '%' + @lV15GrupoFuncao_Nome1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV16DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "GRUPOFUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18GrupoFuncao_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like '%' + @lV18GrupoFuncao_Nome2 + '%')";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector3, "GRUPOFUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21GrupoFuncao_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like '%' + @lV21GrupoFuncao_Nome3 + '%')";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV29TFGrupoFuncao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFGrupoFuncao_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like @lV28TFGrupoFuncao_Nome)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFGrupoFuncao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] = @AV29TFGrupoFuncao_Nome_Sel)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [GrupoFuncao_Nome]";
         }
         else if ( AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [GrupoFuncao_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [GrupoFuncao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00CN5( IGxContext context ,
                                             String AV14DynamicFiltersSelector1 ,
                                             String AV15GrupoFuncao_Nome1 ,
                                             bool AV16DynamicFiltersEnabled2 ,
                                             String AV17DynamicFiltersSelector2 ,
                                             String AV18GrupoFuncao_Nome2 ,
                                             bool AV19DynamicFiltersEnabled3 ,
                                             String AV20DynamicFiltersSelector3 ,
                                             String AV21GrupoFuncao_Nome3 ,
                                             String AV29TFGrupoFuncao_Nome_Sel ,
                                             String AV28TFGrupoFuncao_Nome ,
                                             String A620GrupoFuncao_Nome ,
                                             bool AV13OrderedDsc ,
                                             int A2179GrupoFuncao_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                             bool A625GrupoFuncao_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [6] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Geral_Grupo_Funcao] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([GrupoFuncao_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and ([GrupoFuncao_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "GRUPOFUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15GrupoFuncao_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like '%' + @lV15GrupoFuncao_Nome1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV16DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "GRUPOFUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18GrupoFuncao_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like '%' + @lV18GrupoFuncao_Nome2 + '%')";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector3, "GRUPOFUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21GrupoFuncao_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like '%' + @lV21GrupoFuncao_Nome3 + '%')";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV29TFGrupoFuncao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFGrupoFuncao_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] like @lV28TFGrupoFuncao_Nome)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFGrupoFuncao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([GrupoFuncao_Nome] = @AV29TFGrupoFuncao_Nome_Sel)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00CN4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (bool)dynConstraints[14] );
               case 3 :
                     return conditional_H00CN5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (bool)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00CN2 ;
          prmH00CN2 = new Object[] {
          } ;
          Object[] prmH00CN3 ;
          prmH00CN3 = new Object[] {
          } ;
          Object[] prmH00CN4 ;
          prmH00CN4 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV15GrupoFuncao_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV18GrupoFuncao_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV21GrupoFuncao_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV28TFGrupoFuncao_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV29TFGrupoFuncao_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00CN5 ;
          prmH00CN5 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV15GrupoFuncao_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV18GrupoFuncao_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV21GrupoFuncao_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV28TFGrupoFuncao_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV29TFGrupoFuncao_Nome_Sel",SqlDbType.VarChar,80,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00CN2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CN2,0,0,true,false )
             ,new CursorDef("H00CN3", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CN3,0,0,true,false )
             ,new CursorDef("H00CN4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CN4,11,0,true,false )
             ,new CursorDef("H00CN5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CN5,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

}
