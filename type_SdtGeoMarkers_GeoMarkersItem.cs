/*
               File: type_SdtGeoMarkers_GeoMarkersItem
        Description: GeoMarkers
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:56.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GeoMarkers.GeoMarkersItem" )]
   [XmlType(TypeName =  "GeoMarkers.GeoMarkersItem" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtGeoMarkers_GeoMarkersItem : GxUserType
   {
      public SdtGeoMarkers_GeoMarkersItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtGeoMarkers_GeoMarkersItem_Mvaltext = "";
         gxTv_SdtGeoMarkers_GeoMarkersItem_Mcity = "";
      }

      public SdtGeoMarkers_GeoMarkersItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGeoMarkers_GeoMarkersItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGeoMarkers_GeoMarkersItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGeoMarkers_GeoMarkersItem obj ;
         obj = this;
         obj.gxTpr_Mvalnum = deserialized.gxTpr_Mvalnum;
         obj.gxTpr_Mvaltext = deserialized.gxTpr_Mvaltext;
         obj.gxTpr_Mcity = deserialized.gxTpr_Mcity;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "MValnum") )
               {
                  gxTv_SdtGeoMarkers_GeoMarkersItem_Mvalnum = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "MValtext") )
               {
                  gxTv_SdtGeoMarkers_GeoMarkersItem_Mvaltext = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "MCity") )
               {
                  gxTv_SdtGeoMarkers_GeoMarkersItem_Mcity = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GeoMarkers.GeoMarkersItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("MValnum", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGeoMarkers_GeoMarkersItem_Mvalnum), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("MValtext", StringUtil.RTrim( gxTv_SdtGeoMarkers_GeoMarkersItem_Mvaltext));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("MCity", StringUtil.RTrim( gxTv_SdtGeoMarkers_GeoMarkersItem_Mcity));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("MValnum", gxTv_SdtGeoMarkers_GeoMarkersItem_Mvalnum, false);
         AddObjectProperty("MValtext", gxTv_SdtGeoMarkers_GeoMarkersItem_Mvaltext, false);
         AddObjectProperty("MCity", gxTv_SdtGeoMarkers_GeoMarkersItem_Mcity, false);
         return  ;
      }

      [  SoapElement( ElementName = "MValnum" )]
      [  XmlElement( ElementName = "MValnum"   )]
      public int gxTpr_Mvalnum
      {
         get {
            return gxTv_SdtGeoMarkers_GeoMarkersItem_Mvalnum ;
         }

         set {
            gxTv_SdtGeoMarkers_GeoMarkersItem_Mvalnum = (int)(value);
         }

      }

      [  SoapElement( ElementName = "MValtext" )]
      [  XmlElement( ElementName = "MValtext"   )]
      public String gxTpr_Mvaltext
      {
         get {
            return gxTv_SdtGeoMarkers_GeoMarkersItem_Mvaltext ;
         }

         set {
            gxTv_SdtGeoMarkers_GeoMarkersItem_Mvaltext = (String)(value);
         }

      }

      [  SoapElement( ElementName = "MCity" )]
      [  XmlElement( ElementName = "MCity"   )]
      public String gxTpr_Mcity
      {
         get {
            return gxTv_SdtGeoMarkers_GeoMarkersItem_Mcity ;
         }

         set {
            gxTv_SdtGeoMarkers_GeoMarkersItem_Mcity = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtGeoMarkers_GeoMarkersItem_Mvaltext = "";
         gxTv_SdtGeoMarkers_GeoMarkersItem_Mcity = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtGeoMarkers_GeoMarkersItem_Mvalnum ;
      protected String gxTv_SdtGeoMarkers_GeoMarkersItem_Mvaltext ;
      protected String gxTv_SdtGeoMarkers_GeoMarkersItem_Mcity ;
      protected String sTagName ;
   }

   [DataContract(Name = @"GeoMarkers.GeoMarkersItem", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtGeoMarkers_GeoMarkersItem_RESTInterface : GxGenericCollectionItem<SdtGeoMarkers_GeoMarkersItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGeoMarkers_GeoMarkersItem_RESTInterface( ) : base()
      {
      }

      public SdtGeoMarkers_GeoMarkersItem_RESTInterface( SdtGeoMarkers_GeoMarkersItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "MValnum" , Order = 0 )]
      public Nullable<int> gxTpr_Mvalnum
      {
         get {
            return sdt.gxTpr_Mvalnum ;
         }

         set {
            sdt.gxTpr_Mvalnum = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "MValtext" , Order = 1 )]
      public String gxTpr_Mvaltext
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Mvaltext) ;
         }

         set {
            sdt.gxTpr_Mvaltext = (String)(value);
         }

      }

      [DataMember( Name = "MCity" , Order = 2 )]
      public String gxTpr_Mcity
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Mcity) ;
         }

         set {
            sdt.gxTpr_Mcity = (String)(value);
         }

      }

      public SdtGeoMarkers_GeoMarkersItem sdt
      {
         get {
            return (SdtGeoMarkers_GeoMarkersItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGeoMarkers_GeoMarkersItem() ;
         }
      }

   }

}
