/*
               File: WP_testeCombo
        Description: Nova Ordem de Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/3/2020 2:34:5.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_testecombo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_testecombo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_testecombo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavContagemresultado_requisitadoporcontratante = new GXCheckbox();
         dynavContagemresultado_owner = new GXCombobox();
         cmbavContagemresultado_cntcod = new GXCombobox();
         dynavContagemresultado_servicogrupo = new GXCombobox();
         dynavContagemresultado_servico = new GXCombobox();
         cmbavContratoservicosprazo_complexidade = new GXCombobox();
         cmbavContratoservicosprioridade_codigo = new GXCombobox();
         dynavContagemresultado_contratadacod = new GXCombobox();
         cmbavContagemresultado_responsavel = new GXCombobox();
         dynavContagemresultado_contratadaorigemcod = new GXCombobox();
         dynavContagemresultado_contadorfscod = new GXCombobox();
         cmbavContratoservicos_localexec = new GXCombobox();
         dynavContagemresultado_sistemacod = new GXCombobox();
         dynavModulo_codigo = new GXCombobox();
         dynavContagemresultado_fncusrcod = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_OWNER") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_OWNERT62( AV10WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
            {
               AV18ContagemResultado_CntCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SERVICOGRUPOT62( AV18ContagemResultado_CntCod, AV10WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV8ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContagemResultado_ServicoGrupo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SERVICOT62( AV8ContagemResultado_ServicoGrupo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV18ContagemResultado_CntCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTRATADACODT62( AV18ContagemResultado_CntCod) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTRATADAORIGEMCODT62( AV10WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTADORFSCOD") == 0 )
            {
               AV45ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45ContagemResultado_ContratadaOrigemCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTADORFSCODT62( AV45ContagemResultado_ContratadaOrigemCod) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV9ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SISTEMACODT62( AV9ContagemResultado_Servico) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vMODULO_CODIGO") == 0 )
            {
               AV50ContagemResultado_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvMODULO_CODIGOT62( AV50ContagemResultado_SistemaCod) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_FNCUSRCOD") == 0 )
            {
               AV50ContagemResultado_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_FNCUSRCODT62( AV50ContagemResultado_SistemaCod) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAT62( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTT62( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202043234564");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_testecombo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV59Pgmname));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOSERVICOS_ATIVO", A638ContratoServicos_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATO_ATIVO", A92Contrato_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADA_ATIVO", A43Contratada_Ativo);
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, "SERVICO_NOME", StringUtil.RTrim( A608Servico_Nome));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_UNDCNTNOME", StringUtil.RTrim( A2132ContratoServicos_UndCntNome));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_QNTUNTCNS", StringUtil.LTrim( StringUtil.NToC( A1340ContratoServicos_QntUntCns, 9, 4, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_ISORIGEMREFERENCIA", A2092Servico_IsOrigemReferencia);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOSERVICOS_SOLICITAGESTORSISTEMA", A2094ContratoServicos_SolicitaGestorSistema);
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_MOMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_DIASBAIXA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_DIASMEDIA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_DIASALTA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRIORIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRIORIDADE_NOME", StringUtil.RTrim( A1337ContratoServicosPrioridade_Nome));
         GxWebStd.gx_boolean_hidden_field( context, "vSERVICO_ISORIGEMREFERENCIA", AV55Servico_IsOrigemReferencia);
         GxWebStd.gx_boolean_hidden_field( context, "vSERVICO_ISSOLICITAGESTORSISTEMA", AV56Servico_IsSolicitaGestorSistema);
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADAUSUARIO_USUARIOATIVO", A1394ContratadaUsuario_UsuarioAtivo);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_DEFERIAS", A1908Usuario_DeFerias);
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOPESSOANOM", StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV10WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV10WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_CARGOUONOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV12Usuario_CargoUONom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_REQUISITANTE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15ContagemResultado_Requisitante, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( "", AV16ContagemResultado_DataDmn));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_PRAZOANALISE", GetSecureSignedToken( "", AV48ContagemResultado_PrazoAnalise));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Width", StringUtil.RTrim( Gxuitabspanel_tabmain_Width));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Cls", StringUtil.RTrim( Gxuitabspanel_tabmain_Cls));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autowidth));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autoheight));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tabmain_Designtimetabs));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "UCCONFIRMPANEL_Title", StringUtil.RTrim( Ucconfirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "UCCONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Ucconfirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "UCCONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Ucconfirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "UCCONFIRMPANEL_Confirmtype", StringUtil.RTrim( Ucconfirmpanel_Confirmtype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_testeCombo";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(AV16ContagemResultado_DataDmn, "99/99/99");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_testecombo:[SendSecurityCheck value for]"+"ContagemResultado_DataDmn:"+context.localUtil.Format(AV16ContagemResultado_DataDmn, "99/99/99"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WET62( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTT62( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_testecombo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_testeCombo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Nova Ordem de Servi�o" ;
      }

      protected void WBT60( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_T62( true) ;
         }
         else
         {
            wb_table1_2_T62( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_T62e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTT62( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Nova Ordem de Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPT60( ) ;
      }

      protected void WST62( )
      {
         STARTT62( ) ;
         EVTT62( ) ;
      }

      protected void EVTT62( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11T62 */
                              E11T62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12T62 */
                                    E12T62 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13T62 */
                              E13T62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_SERVICOGRUPO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14T62 */
                              E14T62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_SERVICO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15T62 */
                              E15T62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATOSERVICOSPRAZO_COMPLEXIDADE.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16T62 */
                              E16T62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATOSERVICOSPRIORIDADE_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17T62 */
                              E17T62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_CONTRATADACOD.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18T62 */
                              E18T62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_SISTEMACOD.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19T62 */
                              E19T62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20T62 */
                              E20T62 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WET62( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAT62( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            chkavContagemresultado_requisitadoporcontratante.Name = "vCONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE";
            chkavContagemresultado_requisitadoporcontratante.WebTags = "";
            chkavContagemresultado_requisitadoporcontratante.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContagemresultado_requisitadoporcontratante_Internalname, "TitleCaption", chkavContagemresultado_requisitadoporcontratante.Caption);
            chkavContagemresultado_requisitadoporcontratante.CheckedValue = "false";
            dynavContagemresultado_owner.Name = "vCONTAGEMRESULTADO_OWNER";
            dynavContagemresultado_owner.WebTags = "";
            cmbavContagemresultado_cntcod.Name = "vCONTAGEMRESULTADO_CNTCOD";
            cmbavContagemresultado_cntcod.WebTags = "";
            if ( cmbavContagemresultado_cntcod.ItemCount > 0 )
            {
               AV18ContagemResultado_CntCod = (int)(NumberUtil.Val( cmbavContagemresultado_cntcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0)));
            }
            dynavContagemresultado_servicogrupo.Name = "vCONTAGEMRESULTADO_SERVICOGRUPO";
            dynavContagemresultado_servicogrupo.WebTags = "";
            dynavContagemresultado_servico.Name = "vCONTAGEMRESULTADO_SERVICO";
            dynavContagemresultado_servico.WebTags = "";
            cmbavContratoservicosprazo_complexidade.Name = "vCONTRATOSERVICOSPRAZO_COMPLEXIDADE";
            cmbavContratoservicosprazo_complexidade.WebTags = "";
            if ( cmbavContratoservicosprazo_complexidade.ItemCount > 0 )
            {
               AV34ContratoServicosPrazo_Complexidade = (short)(NumberUtil.Val( cmbavContratoservicosprazo_complexidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34ContratoServicosPrazo_Complexidade), 3, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContratoServicosPrazo_Complexidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34ContratoServicosPrazo_Complexidade), 3, 0)));
            }
            cmbavContratoservicosprioridade_codigo.Name = "vCONTRATOSERVICOSPRIORIDADE_CODIGO";
            cmbavContratoservicosprioridade_codigo.WebTags = "";
            if ( cmbavContratoservicosprioridade_codigo.ItemCount > 0 )
            {
               AV35ContratoServicosPrioridade_Codigo = (int)(NumberUtil.Val( cmbavContratoservicosprioridade_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContratoServicosPrioridade_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContratoServicosPrioridade_Codigo), 6, 0)));
            }
            dynavContagemresultado_contratadacod.Name = "vCONTAGEMRESULTADO_CONTRATADACOD";
            dynavContagemresultado_contratadacod.WebTags = "";
            cmbavContagemresultado_responsavel.Name = "vCONTAGEMRESULTADO_RESPONSAVEL";
            cmbavContagemresultado_responsavel.WebTags = "";
            if ( cmbavContagemresultado_responsavel.ItemCount > 0 )
            {
               AV37ContagemResultado_Responsavel = (int)(NumberUtil.Val( cmbavContagemresultado_responsavel.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_Responsavel), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContagemResultado_Responsavel), 6, 0)));
            }
            dynavContagemresultado_contratadaorigemcod.Name = "vCONTAGEMRESULTADO_CONTRATADAORIGEMCOD";
            dynavContagemresultado_contratadaorigemcod.WebTags = "";
            dynavContagemresultado_contadorfscod.Name = "vCONTAGEMRESULTADO_CONTADORFSCOD";
            dynavContagemresultado_contadorfscod.WebTags = "";
            cmbavContratoservicos_localexec.Name = "vCONTRATOSERVICOS_LOCALEXEC";
            cmbavContratoservicos_localexec.WebTags = "";
            cmbavContratoservicos_localexec.addItem("A", "Contratada", 0);
            cmbavContratoservicos_localexec.addItem("E", "Contratante", 0);
            cmbavContratoservicos_localexec.addItem("O", "Opcional", 0);
            if ( cmbavContratoservicos_localexec.ItemCount > 0 )
            {
               AV47ContratoServicos_LocalExec = cmbavContratoservicos_localexec.getValidValue(AV47ContratoServicos_LocalExec);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContratoServicos_LocalExec", AV47ContratoServicos_LocalExec);
            }
            dynavContagemresultado_sistemacod.Name = "vCONTAGEMRESULTADO_SISTEMACOD";
            dynavContagemresultado_sistemacod.WebTags = "";
            dynavModulo_codigo.Name = "vMODULO_CODIGO";
            dynavModulo_codigo.WebTags = "";
            dynavContagemresultado_fncusrcod.Name = "vCONTAGEMRESULTADO_FNCUSRCOD";
            dynavContagemresultado_fncusrcod.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContagemresultado_demandafm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTAGEMRESULTADO_SERVICOGRUPO_htmlT62( AV18ContagemResultado_CntCod, AV10WWPContext) ;
         GXVvCONTAGEMRESULTADO_SERVICOGRUPO_htmlT62( AV18ContagemResultado_CntCod, AV10WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlT62( AV18ContagemResultado_CntCod) ;
         GXVvCONTAGEMRESULTADO_SERVICO_htmlT62( AV8ContagemResultado_ServicoGrupo) ;
         GXVvCONTAGEMRESULTADO_SISTEMACOD_htmlT62( AV9ContagemResultado_Servico) ;
         GXVvCONTAGEMRESULTADO_CONTADORFSCOD_htmlT62( AV45ContagemResultado_ContratadaOrigemCod) ;
         GXVvMODULO_CODIGO_htmlT62( AV50ContagemResultado_SistemaCod) ;
         GXVvCONTAGEMRESULTADO_FNCUSRCOD_htmlT62( AV50ContagemResultado_SistemaCod) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTAGEMRESULTADO_OWNERT62( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_OWNER_dataT62( AV10WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_OWNER_htmlT62( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_OWNER_dataT62( AV10WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_owner.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_owner.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_owner.ItemCount > 0 )
         {
            AV14ContagemResultado_Owner = (int)(NumberUtil.Val( dynavContagemresultado_owner.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14ContagemResultado_Owner), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ContagemResultado_Owner), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_OWNER_dataT62( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00T63 */
         pr_default.execute(0, new Object[] {AV10WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T63_A60ContratanteUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T63_A62ContratanteUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICOGRUPOT62( int AV18ContagemResultado_CntCod ,
                                                              wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SERVICOGRUPO_dataT62( AV18ContagemResultado_CntCod, AV10WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERVICOGRUPO_htmlT62( int AV18ContagemResultado_CntCod ,
                                                                 wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SERVICOGRUPO_dataT62( AV18ContagemResultado_CntCod, AV10WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_servicogrupo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_servicogrupo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_servicogrupo.ItemCount > 0 )
         {
            AV8ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8ContagemResultado_ServicoGrupo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContagemResultado_ServicoGrupo), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICOGRUPO_dataT62( int AV18ContagemResultado_CntCod ,
                                                                   wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00T64 */
         pr_default.execute(1, new Object[] {AV18ContagemResultado_CntCod, AV10WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T64_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00T64_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICOT62( int AV8ContagemResultado_ServicoGrupo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataT62( AV8ContagemResultado_ServicoGrupo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERVICO_htmlT62( int AV8ContagemResultado_ServicoGrupo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataT62( AV8ContagemResultado_ServicoGrupo) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_servico.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_servico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV9ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICO_dataT62( int AV8ContagemResultado_ServicoGrupo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00T65 */
         pr_default.execute(2, new Object[] {AV8ContagemResultado_ServicoGrupo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T65_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T65_A608Servico_Nome[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACODT62( int AV18ContagemResultado_CntCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataT62( AV18ContagemResultado_CntCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlT62( int AV18ContagemResultado_CntCod )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataT62( AV18ContagemResultado_CntCod) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contratadacod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contratadacod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV36ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36ContagemResultado_ContratadaCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataT62( int AV18ContagemResultado_CntCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00T66 */
         pr_default.execute(3, new Object[] {AV18ContagemResultado_CntCod});
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T66_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T66_A41Contratada_PessoaNom[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADAORIGEMCODT62( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTRATADAORIGEMCOD_dataT62( AV10WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTRATADAORIGEMCOD_htmlT62( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTRATADAORIGEMCOD_dataT62( AV10WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contratadaorigemcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contratadaorigemcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contratadaorigemcod.ItemCount > 0 )
         {
            AV45ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadaorigemcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV45ContagemResultado_ContratadaOrigemCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45ContagemResultado_ContratadaOrigemCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADAORIGEMCOD_dataT62( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00T67 */
         pr_default.execute(4, new Object[] {AV10WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T67_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T67_A41Contratada_PessoaNom[0]));
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFSCODT62( int AV45ContagemResultado_ContratadaOrigemCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTADORFSCOD_dataT62( AV45ContagemResultado_ContratadaOrigemCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTADORFSCOD_htmlT62( int AV45ContagemResultado_ContratadaOrigemCod )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTADORFSCOD_dataT62( AV45ContagemResultado_ContratadaOrigemCod) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contadorfscod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contadorfscod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contadorfscod.ItemCount > 0 )
         {
            AV46ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfscod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV46ContagemResultado_ContadorFSCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46ContagemResultado_ContadorFSCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFSCOD_dataT62( int AV45ContagemResultado_ContratadaOrigemCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00T68 */
         pr_default.execute(5, new Object[] {AV45ContagemResultado_ContratadaOrigemCod});
         while ( (pr_default.getStatus(5) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T68_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T68_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACODT62( int AV9ContagemResultado_Servico )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD_dataT62( AV9ContagemResultado_Servico) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SISTEMACOD_htmlT62( int AV9ContagemResultado_Servico )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD_dataT62( AV9ContagemResultado_Servico) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_sistemacod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_sistemacod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_sistemacod.ItemCount > 0 )
         {
            AV50ContagemResultado_SistemaCod = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD_dataT62( int AV9ContagemResultado_Servico )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00T69 */
         pr_default.execute(6, new Object[] {AV9ContagemResultado_Servico});
         while ( (pr_default.getStatus(6) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T69_A1063ContratoServicosSistemas_SistemaCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T69_A1064ContratoServicosSistemas_SistemaSigla[0]));
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void GXDLVvMODULO_CODIGOT62( int AV50ContagemResultado_SistemaCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvMODULO_CODIGO_dataT62( AV50ContagemResultado_SistemaCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvMODULO_CODIGO_htmlT62( int AV50ContagemResultado_SistemaCod )
      {
         int gxdynajaxvalue ;
         GXDLVvMODULO_CODIGO_dataT62( AV50ContagemResultado_SistemaCod) ;
         gxdynajaxindex = 1;
         dynavModulo_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavModulo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavModulo_codigo.ItemCount > 0 )
         {
            AV51Modulo_Codigo = (int)(NumberUtil.Val( dynavModulo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV51Modulo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51Modulo_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvMODULO_CODIGO_dataT62( int AV50ContagemResultado_SistemaCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00T610 */
         pr_default.execute(7, new Object[] {AV50ContagemResultado_SistemaCod});
         while ( (pr_default.getStatus(7) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T610_A146Modulo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T610_A145Modulo_Sigla[0]));
            pr_default.readNext(7);
         }
         pr_default.close(7);
      }

      protected void GXDLVvCONTAGEMRESULTADO_FNCUSRCODT62( int AV50ContagemResultado_SistemaCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_FNCUSRCOD_dataT62( AV50ContagemResultado_SistemaCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_FNCUSRCOD_htmlT62( int AV50ContagemResultado_SistemaCod )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_FNCUSRCOD_dataT62( AV50ContagemResultado_SistemaCod) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_fncusrcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_fncusrcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_fncusrcod.ItemCount > 0 )
         {
            AV52ContagemResultado_FncUsrCod = (int)(NumberUtil.Val( dynavContagemresultado_fncusrcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_FncUsrCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52ContagemResultado_FncUsrCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_FNCUSRCOD_dataT62( int AV50ContagemResultado_SistemaCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00T611 */
         pr_default.execute(8, new Object[] {AV50ContagemResultado_SistemaCod});
         while ( (pr_default.getStatus(8) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T611_A161FuncaoUsuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00T611_A162FuncaoUsuario_Nome[0]);
            pr_default.readNext(8);
         }
         pr_default.close(8);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContagemresultado_owner.ItemCount > 0 )
         {
            AV14ContagemResultado_Owner = (int)(NumberUtil.Val( dynavContagemresultado_owner.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14ContagemResultado_Owner), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ContagemResultado_Owner), 6, 0)));
         }
         if ( cmbavContagemresultado_cntcod.ItemCount > 0 )
         {
            AV18ContagemResultado_CntCod = (int)(NumberUtil.Val( cmbavContagemresultado_cntcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0)));
         }
         if ( dynavContagemresultado_servicogrupo.ItemCount > 0 )
         {
            AV8ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8ContagemResultado_ServicoGrupo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContagemResultado_ServicoGrupo), 6, 0)));
         }
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV9ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0)));
         }
         if ( cmbavContratoservicosprazo_complexidade.ItemCount > 0 )
         {
            AV34ContratoServicosPrazo_Complexidade = (short)(NumberUtil.Val( cmbavContratoservicosprazo_complexidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34ContratoServicosPrazo_Complexidade), 3, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContratoServicosPrazo_Complexidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34ContratoServicosPrazo_Complexidade), 3, 0)));
         }
         if ( cmbavContratoservicosprioridade_codigo.ItemCount > 0 )
         {
            AV35ContratoServicosPrioridade_Codigo = (int)(NumberUtil.Val( cmbavContratoservicosprioridade_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContratoServicosPrioridade_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContratoServicosPrioridade_Codigo), 6, 0)));
         }
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV36ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36ContagemResultado_ContratadaCod), 6, 0)));
         }
         if ( cmbavContagemresultado_responsavel.ItemCount > 0 )
         {
            AV37ContagemResultado_Responsavel = (int)(NumberUtil.Val( cmbavContagemresultado_responsavel.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_Responsavel), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContagemResultado_Responsavel), 6, 0)));
         }
         if ( dynavContagemresultado_contratadaorigemcod.ItemCount > 0 )
         {
            AV45ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadaorigemcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV45ContagemResultado_ContratadaOrigemCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45ContagemResultado_ContratadaOrigemCod), 6, 0)));
         }
         if ( dynavContagemresultado_contadorfscod.ItemCount > 0 )
         {
            AV46ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfscod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV46ContagemResultado_ContadorFSCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46ContagemResultado_ContadorFSCod), 6, 0)));
         }
         if ( cmbavContratoservicos_localexec.ItemCount > 0 )
         {
            AV47ContratoServicos_LocalExec = cmbavContratoservicos_localexec.getValidValue(AV47ContratoServicos_LocalExec);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContratoServicos_LocalExec", AV47ContratoServicos_LocalExec);
         }
         if ( dynavContagemresultado_sistemacod.ItemCount > 0 )
         {
            AV50ContagemResultado_SistemaCod = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0)));
         }
         if ( dynavModulo_codigo.ItemCount > 0 )
         {
            AV51Modulo_Codigo = (int)(NumberUtil.Val( dynavModulo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV51Modulo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51Modulo_Codigo), 6, 0)));
         }
         if ( dynavContagemresultado_fncusrcod.ItemCount > 0 )
         {
            AV52ContagemResultado_FncUsrCod = (int)(NumberUtil.Val( dynavContagemresultado_fncusrcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_FncUsrCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52ContagemResultado_FncUsrCod), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFT62( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV59Pgmname = "WP_testeCombo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         context.Gx_err = 0;
         edtavUsuario_cargouonom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargouonom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargouonom_Enabled), 5, 0)));
         edtavContagemresultado_requisitante_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_requisitante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_requisitante_Enabled), 5, 0)));
         edtavContagemresultado_datadmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datadmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datadmn_Enabled), 5, 0)));
         edtavContratoservicos_undcntnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_undcntnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_undcntnome_Enabled), 5, 0)));
         edtavContratoservicos_qntuntcns_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_qntuntcns_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_qntuntcns_Enabled), 5, 0)));
         edtavContagemresultado_prazoanalise_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_prazoanalise_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_prazoanalise_Enabled), 5, 0)));
         edtavContagemresultado_sistemagestor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_sistemagestor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_sistemagestor_Enabled), 5, 0)));
      }

      protected void RFT62( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E13T62 */
         E13T62 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E20T62 */
            E20T62 ();
            WBT60( ) ;
         }
      }

      protected void STRUPT60( )
      {
         /* Before Start, stand alone formulas. */
         AV59Pgmname = "WP_testeCombo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         context.Gx_err = 0;
         edtavUsuario_cargouonom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargouonom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargouonom_Enabled), 5, 0)));
         edtavContagemresultado_requisitante_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_requisitante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_requisitante_Enabled), 5, 0)));
         edtavContagemresultado_datadmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datadmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datadmn_Enabled), 5, 0)));
         edtavContratoservicos_undcntnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_undcntnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_undcntnome_Enabled), 5, 0)));
         edtavContratoservicos_qntuntcns_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_qntuntcns_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_qntuntcns_Enabled), 5, 0)));
         edtavContagemresultado_prazoanalise_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_prazoanalise_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_prazoanalise_Enabled), 5, 0)));
         edtavContagemresultado_sistemagestor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_sistemagestor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_sistemagestor_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11T62 */
         E11T62 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTAGEMRESULTADO_OWNER_htmlT62( AV10WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTRATADAORIGEMCOD_htmlT62( AV10WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTADORFSCOD_htmlT62( AV45ContagemResultado_ContratadaOrigemCod) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV11ContagemResultado_DemandaFM = cgiGet( edtavContagemresultado_demandafm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_DemandaFM", AV11ContagemResultado_DemandaFM);
            AV12Usuario_CargoUONom = StringUtil.Upper( cgiGet( edtavUsuario_cargouonom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Usuario_CargoUONom", AV12Usuario_CargoUONom);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CARGOUONOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV12Usuario_CargoUONom, "@!"))));
            AV13ContagemResultado_RequisitadoPorContratante = StringUtil.StrToBool( cgiGet( chkavContagemresultado_requisitadoporcontratante_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_RequisitadoPorContratante", AV13ContagemResultado_RequisitadoPorContratante);
            dynavContagemresultado_owner.CurrentValue = cgiGet( dynavContagemresultado_owner_Internalname);
            AV14ContagemResultado_Owner = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_owner_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ContagemResultado_Owner), 6, 0)));
            AV15ContagemResultado_Requisitante = StringUtil.Upper( cgiGet( edtavContagemresultado_requisitante_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_Requisitante", AV15ContagemResultado_Requisitante);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_REQUISITANTE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15ContagemResultado_Requisitante, "@!"))));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn"}), 1, "vCONTAGEMRESULTADO_DATADMN");
               GX_FocusControl = edtavContagemresultado_datadmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16ContagemResultado_DataDmn = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_DataDmn", context.localUtil.Format(AV16ContagemResultado_DataDmn, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( "", AV16ContagemResultado_DataDmn));
            }
            else
            {
               AV16ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_DataDmn", context.localUtil.Format(AV16ContagemResultado_DataDmn, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( "", AV16ContagemResultado_DataDmn));
            }
            AV17ContagemResultado_Descricao = cgiGet( edtavContagemresultado_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_Descricao", AV17ContagemResultado_Descricao);
            cmbavContagemresultado_cntcod.CurrentValue = cgiGet( cmbavContagemresultado_cntcod_Internalname);
            AV18ContagemResultado_CntCod = (int)(NumberUtil.Val( cgiGet( cmbavContagemresultado_cntcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0)));
            dynavContagemresultado_servicogrupo.CurrentValue = cgiGet( dynavContagemresultado_servicogrupo_Internalname);
            AV8ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_servicogrupo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContagemResultado_ServicoGrupo), 6, 0)));
            dynavContagemresultado_servico.CurrentValue = cgiGet( dynavContagemresultado_servico_Internalname);
            AV9ContagemResultado_Servico = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_servico_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0)));
            AV28ContratoServicos_UndCntNome = StringUtil.Upper( cgiGet( edtavContratoservicos_undcntnome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContratoServicos_UndCntNome", AV28ContratoServicos_UndCntNome);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_qntuntcns_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_qntuntcns_Internalname), ",", ".") > 9999.9999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOS_QNTUNTCNS");
               GX_FocusControl = edtavContratoservicos_qntuntcns_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30ContratoServicos_QntUntCns = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( AV30ContratoServicos_QntUntCns, 9, 4)));
            }
            else
            {
               AV30ContratoServicos_QntUntCns = context.localUtil.CToN( cgiGet( edtavContratoservicos_qntuntcns_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( AV30ContratoServicos_QntUntCns, 9, 4)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_quantidadesolicitada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_quantidadesolicitada_Internalname), ",", ".") > 9999.9999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_QUANTIDADESOLICITADA");
               GX_FocusControl = edtavContagemresultado_quantidadesolicitada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31ContagemResultado_QuantidadeSolicitada = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( AV31ContagemResultado_QuantidadeSolicitada, 9, 4)));
            }
            else
            {
               AV31ContagemResultado_QuantidadeSolicitada = context.localUtil.CToN( cgiGet( edtavContagemresultado_quantidadesolicitada_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( AV31ContagemResultado_QuantidadeSolicitada, 9, 4)));
            }
            cmbavContratoservicosprazo_complexidade.CurrentValue = cgiGet( cmbavContratoservicosprazo_complexidade_Internalname);
            AV34ContratoServicosPrazo_Complexidade = (short)(NumberUtil.Val( cgiGet( cmbavContratoservicosprazo_complexidade_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContratoServicosPrazo_Complexidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34ContratoServicosPrazo_Complexidade), 3, 0)));
            cmbavContratoservicosprioridade_codigo.CurrentValue = cgiGet( cmbavContratoservicosprioridade_codigo_Internalname);
            AV35ContratoServicosPrioridade_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavContratoservicosprioridade_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContratoServicosPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContratoServicosPrioridade_Codigo), 6, 0)));
            dynavContagemresultado_contratadacod.CurrentValue = cgiGet( dynavContagemresultado_contratadacod_Internalname);
            AV36ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contratadacod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36ContagemResultado_ContratadaCod), 6, 0)));
            cmbavContagemresultado_responsavel.CurrentValue = cgiGet( cmbavContagemresultado_responsavel_Internalname);
            AV37ContagemResultado_Responsavel = (int)(NumberUtil.Val( cgiGet( cmbavContagemresultado_responsavel_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContagemResultado_Responsavel), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_osvinculada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_osvinculada_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_OSVINCULADA");
               GX_FocusControl = edtavContagemresultado_osvinculada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40ContagemResultado_OSVinculada = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40ContagemResultado_OSVinculada), 6, 0)));
            }
            else
            {
               AV40ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_osvinculada_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40ContagemResultado_OSVinculada), 6, 0)));
            }
            AV41ContagemResultado_DmnVinculada = StringUtil.Upper( cgiGet( edtavContagemresultado_dmnvinculada_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContagemResultado_DmnVinculada", AV41ContagemResultado_DmnVinculada);
            AV42ContagemResultado_DmnVinculadaRef = StringUtil.Upper( cgiGet( edtavContagemresultado_dmnvinculadaref_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContagemResultado_DmnVinculadaRef", AV42ContagemResultado_DmnVinculadaRef);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfsimp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfsimp_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFBFSIMP");
               GX_FocusControl = edtavContagemresultado_pfbfsimp_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43ContagemResultado_PFBFSImp = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_PFBFSImp", StringUtil.LTrim( StringUtil.Str( AV43ContagemResultado_PFBFSImp, 14, 5)));
            }
            else
            {
               AV43ContagemResultado_PFBFSImp = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfsimp_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_PFBFSImp", StringUtil.LTrim( StringUtil.Str( AV43ContagemResultado_PFBFSImp, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfsimp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfsimp_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFLFSIMP");
               GX_FocusControl = edtavContagemresultado_pflfsimp_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44ContagemResultado_PFLFSImp = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContagemResultado_PFLFSImp", StringUtil.LTrim( StringUtil.Str( AV44ContagemResultado_PFLFSImp, 14, 5)));
            }
            else
            {
               AV44ContagemResultado_PFLFSImp = context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfsimp_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContagemResultado_PFLFSImp", StringUtil.LTrim( StringUtil.Str( AV44ContagemResultado_PFLFSImp, 14, 5)));
            }
            dynavContagemresultado_contratadaorigemcod.CurrentValue = cgiGet( dynavContagemresultado_contratadaorigemcod_Internalname);
            AV45ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contratadaorigemcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45ContagemResultado_ContratadaOrigemCod), 6, 0)));
            dynavContagemresultado_contadorfscod.CurrentValue = cgiGet( dynavContagemresultado_contadorfscod_Internalname);
            AV46ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contadorfscod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46ContagemResultado_ContadorFSCod), 6, 0)));
            cmbavContratoservicos_localexec.CurrentValue = cgiGet( cmbavContratoservicos_localexec_Internalname);
            AV47ContratoServicos_LocalExec = cgiGet( cmbavContratoservicos_localexec_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContratoServicos_LocalExec", AV47ContratoServicos_LocalExec);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_prazoanalise_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Prazo Analise"}), 1, "vCONTAGEMRESULTADO_PRAZOANALISE");
               GX_FocusControl = edtavContagemresultado_prazoanalise_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48ContagemResultado_PrazoAnalise = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_PrazoAnalise", context.localUtil.Format(AV48ContagemResultado_PrazoAnalise, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PRAZOANALISE", GetSecureSignedToken( "", AV48ContagemResultado_PrazoAnalise));
            }
            else
            {
               AV48ContagemResultado_PrazoAnalise = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_prazoanalise_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_PrazoAnalise", context.localUtil.Format(AV48ContagemResultado_PrazoAnalise, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PRAZOANALISE", GetSecureSignedToken( "", AV48ContagemResultado_PrazoAnalise));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentrega_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Entrega"}), 1, "vCONTAGEMRESULTADO_DATAENTREGA");
               GX_FocusControl = edtavContagemresultado_dataentrega_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49ContagemResultado_DataEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_DataEntrega", context.localUtil.Format(AV49ContagemResultado_DataEntrega, "99/99/99"));
            }
            else
            {
               AV49ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentrega_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_DataEntrega", context.localUtil.Format(AV49ContagemResultado_DataEntrega, "99/99/99"));
            }
            dynavContagemresultado_sistemacod.CurrentValue = cgiGet( dynavContagemresultado_sistemacod_Internalname);
            AV50ContagemResultado_SistemaCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_sistemacod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0)));
            dynavModulo_codigo.CurrentValue = cgiGet( dynavModulo_codigo_Internalname);
            AV51Modulo_Codigo = (int)(NumberUtil.Val( cgiGet( dynavModulo_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51Modulo_Codigo), 6, 0)));
            dynavContagemresultado_fncusrcod.CurrentValue = cgiGet( dynavContagemresultado_fncusrcod_Internalname);
            AV52ContagemResultado_FncUsrCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_fncusrcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52ContagemResultado_FncUsrCod), 6, 0)));
            AV53ContagemResultado_SistemaGestor = StringUtil.Upper( cgiGet( edtavContagemresultado_sistemagestor_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContagemResultado_SistemaGestor", AV53ContagemResultado_SistemaGestor);
            AV54ContagemResultado_Link = cgiGet( edtavContagemresultado_link_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_Link", AV54ContagemResultado_Link);
            /* Read saved values. */
            Gxuitabspanel_tabmain_Width = cgiGet( "GXUITABSPANEL_TABMAIN_Width");
            Gxuitabspanel_tabmain_Cls = cgiGet( "GXUITABSPANEL_TABMAIN_Cls");
            Gxuitabspanel_tabmain_Autowidth = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autowidth"));
            Gxuitabspanel_tabmain_Autoheight = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autoheight"));
            Gxuitabspanel_tabmain_Autoscroll = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autoscroll"));
            Gxuitabspanel_tabmain_Designtimetabs = cgiGet( "GXUITABSPANEL_TABMAIN_Designtimetabs");
            Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
            Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
            Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
            Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
            Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
            Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
            Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
            Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
            Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
            Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
            Ucconfirmpanel_Title = cgiGet( "UCCONFIRMPANEL_Title");
            Ucconfirmpanel_Confirmationtext = cgiGet( "UCCONFIRMPANEL_Confirmationtext");
            Ucconfirmpanel_Yesbuttoncaption = cgiGet( "UCCONFIRMPANEL_Yesbuttoncaption");
            Ucconfirmpanel_Confirmtype = cgiGet( "UCCONFIRMPANEL_Confirmtype");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_testeCombo";
            AV16ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_DataDmn", context.localUtil.Format(AV16ContagemResultado_DataDmn, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( "", AV16ContagemResultado_DataDmn));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(AV16ContagemResultado_DataDmn, "99/99/99");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_testecombo:[SecurityCheckFailed value for]"+"ContagemResultado_DataDmn:"+context.localUtil.Format(AV16ContagemResultado_DataDmn, "99/99/99"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            GXVvCONTAGEMRESULTADO_OWNER_htmlT62( AV10WWPContext) ;
            GXVvCONTAGEMRESULTADO_CONTRATADAORIGEMCOD_htmlT62( AV10WWPContext) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11T62 */
         E11T62 ();
         if (returnInSub) return;
      }

      protected void E11T62( )
      {
         /* Start Routine */
         new geralog(context ).execute( ref  AV59Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         GXt_char1 = "Event Start";
         new geralog(context ).execute( ref  GXt_char1) ;
         Form.Meta.addItem("Versao", "1.0 - Data: 01/04/2020", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV10WWPContext) ;
         new geralog(context ).execute( ref  AV59Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         GXt_char2 = "Event Start";
         new geralog(context ).execute( ref  GXt_char2) ;
         GXt_char3 = "&WWPContext.AreaTrabalho_Codigo 	 = " + context.localUtil.Format( (decimal)(AV10WWPContext.gxTpr_Areatrabalho_codigo), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char4 = "&WWPContext.AreaTrabalho_Descricao = " + AV10WWPContext.gxTpr_Areatrabalho_descricao;
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char5 = "&WWPContext.UserEhContratante 	 = " + StringUtil.BoolToStr( AV10WWPContext.gxTpr_Userehcontratante);
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char6 = "&WWPContext.UserEhContratada 		 = " + StringUtil.BoolToStr( AV10WWPContext.gxTpr_Userehcontratada);
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char7 = "&WWPContext.Contratada_Codigo 	 = " + StringUtil.Str( (decimal)(AV10WWPContext.gxTpr_Contratada_codigo), 6, 0);
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char8 = "&WWPContext.Contratada_PessoaNom 	 = " + AV10WWPContext.gxTpr_Contratada_pessoanom;
         new geralog(context ).execute( ref  GXt_char8) ;
         GXt_char9 = "Event Start";
         new geralog(context ).execute( ref  GXt_char9) ;
         GX_msglist.addItem("&WWPContext.AreaTrabalho_Codigo 			= "+context.localUtil.Format( (decimal)(AV10WWPContext.gxTpr_Areatrabalho_codigo), "ZZZZZ9"));
         GX_msglist.addItem("&WWPContext.AreaTrabalho_Descricao 		= "+AV10WWPContext.gxTpr_Areatrabalho_descricao);
         GX_msglist.addItem("&WWPContext.UserEhContratante 				= "+StringUtil.BoolToStr( AV10WWPContext.gxTpr_Userehcontratante));
         GX_msglist.addItem("&WWPContext.UserEhContratada 				= "+StringUtil.BoolToStr( AV10WWPContext.gxTpr_Userehcontratada));
         GX_msglist.addItem("&WWPContext.Contratada_Codigo 				= "+StringUtil.Str( (decimal)(AV10WWPContext.gxTpr_Contratada_codigo), 6, 0));
         GX_msglist.addItem("&WWPContext.Contratada_PessoaNom 			= "+AV10WWPContext.gxTpr_Contratada_pessoanom);
         AV23IsContratanteSemEmailSda = false;
         /* Using cursor H00T612 */
         pr_default.execute(9, new Object[] {AV10WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(9) != 101) )
         {
            A29Contratante_Codigo = H00T612_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00T612_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = H00T612_A5AreaTrabalho_Codigo[0];
            A593Contratante_OSAutomatica = H00T612_A593Contratante_OSAutomatica[0];
            A2085Contratante_RequerOrigem = H00T612_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = H00T612_n2085Contratante_RequerOrigem[0];
            A1822Contratante_UsaOSistema = H00T612_A1822Contratante_UsaOSistema[0];
            n1822Contratante_UsaOSistema = H00T612_n1822Contratante_UsaOSistema[0];
            A6AreaTrabalho_Descricao = H00T612_A6AreaTrabalho_Descricao[0];
            A548Contratante_EmailSdaUser = H00T612_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = H00T612_n548Contratante_EmailSdaUser[0];
            A552Contratante_EmailSdaPort = H00T612_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = H00T612_n552Contratante_EmailSdaPort[0];
            A547Contratante_EmailSdaHost = H00T612_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = H00T612_n547Contratante_EmailSdaHost[0];
            A593Contratante_OSAutomatica = H00T612_A593Contratante_OSAutomatica[0];
            A2085Contratante_RequerOrigem = H00T612_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = H00T612_n2085Contratante_RequerOrigem[0];
            A1822Contratante_UsaOSistema = H00T612_A1822Contratante_UsaOSistema[0];
            n1822Contratante_UsaOSistema = H00T612_n1822Contratante_UsaOSistema[0];
            A548Contratante_EmailSdaUser = H00T612_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = H00T612_n548Contratante_EmailSdaUser[0];
            A552Contratante_EmailSdaPort = H00T612_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = H00T612_n552Contratante_EmailSdaPort[0];
            A547Contratante_EmailSdaHost = H00T612_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = H00T612_n547Contratante_EmailSdaHost[0];
            AV20Contratante_OSAutomatica = A593Contratante_OSAutomatica;
            AV21Contratante_RequerOrigem = A2085Contratante_RequerOrigem;
            AV22Contratante_UsaOSistema = A1822Contratante_UsaOSistema;
            lblContagemresultado_requisitadoporcontratante_righttext_Caption = "�"+A6AreaTrabalho_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_requisitadoporcontratante_righttext_Internalname, "Caption", lblContagemresultado_requisitadoporcontratante_righttext_Caption);
            if ( H00T612_n547Contratante_EmailSdaHost[0] || H00T612_n552Contratante_EmailSdaPort[0] || H00T612_n548Contratante_EmailSdaUser[0] )
            {
               AV23IsContratanteSemEmailSda = true;
               Ucconfirmpanel_Confirmationtext = "Cadastro da Contratante sem dados configurados para envio de e-mails!";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ucconfirmpanel_Internalname, "ConfirmationText", Ucconfirmpanel_Confirmationtext);
               Ucconfirmpanel_Title = "Contratante";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ucconfirmpanel_Internalname, "Title", Ucconfirmpanel_Title);
               this.executeUsercontrolMethod("", false, "UCCONFIRMPANELContainer", "Confirm", "", new Object[] {});
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(9);
         AV16ContagemResultado_DataDmn = DateTimeUtil.ServerDate( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_DataDmn", context.localUtil.Format(AV16ContagemResultado_DataDmn, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( "", AV16ContagemResultado_DataDmn));
         AV11ContagemResultado_DemandaFM = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_DemandaFM", AV11ContagemResultado_DemandaFM);
         if ( AV20Contratante_OSAutomatica )
         {
            AV11ContagemResultado_DemandaFM = "Gera��o Autom�tica";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_DemandaFM", AV11ContagemResultado_DemandaFM);
         }
         edtavContagemresultado_demandafm_Enabled = (!AV20Contratante_OSAutomatica ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm_Enabled), 5, 0)));
         if ( AV10WWPContext.gxTpr_Userehcontratante )
         {
            AV14ContagemResultado_Owner = AV10WWPContext.gxTpr_Userid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ContagemResultado_Owner), 6, 0)));
            dynavContagemresultado_owner.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_owner_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_owner.Enabled), 5, 0)));
            AV13ContagemResultado_RequisitadoPorContratante = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_RequisitadoPorContratante", AV13ContagemResultado_RequisitadoPorContratante);
            chkavContagemresultado_requisitadoporcontratante.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContagemresultado_requisitadoporcontratante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContagemresultado_requisitadoporcontratante.Enabled), 5, 0)));
         }
         else
         {
            AV13ContagemResultado_RequisitadoPorContratante = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_RequisitadoPorContratante", AV13ContagemResultado_RequisitadoPorContratante);
            /* Using cursor H00T613 */
            pr_default.execute(10, new Object[] {AV10WWPContext.gxTpr_Userid});
            while ( (pr_default.getStatus(10) != 101) )
            {
               A57Usuario_PessoaCod = H00T613_A57Usuario_PessoaCod[0];
               A1073Usuario_CargoCod = H00T613_A1073Usuario_CargoCod[0];
               n1073Usuario_CargoCod = H00T613_n1073Usuario_CargoCod[0];
               A1075Usuario_CargoUOCod = H00T613_A1075Usuario_CargoUOCod[0];
               n1075Usuario_CargoUOCod = H00T613_n1075Usuario_CargoUOCod[0];
               A1Usuario_Codigo = H00T613_A1Usuario_Codigo[0];
               A1076Usuario_CargoUONom = H00T613_A1076Usuario_CargoUONom[0];
               n1076Usuario_CargoUONom = H00T613_n1076Usuario_CargoUONom[0];
               A58Usuario_PessoaNom = H00T613_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00T613_n58Usuario_PessoaNom[0];
               A58Usuario_PessoaNom = H00T613_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00T613_n58Usuario_PessoaNom[0];
               A1075Usuario_CargoUOCod = H00T613_A1075Usuario_CargoUOCod[0];
               n1075Usuario_CargoUOCod = H00T613_n1075Usuario_CargoUOCod[0];
               A1076Usuario_CargoUONom = H00T613_A1076Usuario_CargoUONom[0];
               n1076Usuario_CargoUONom = H00T613_n1076Usuario_CargoUONom[0];
               AV12Usuario_CargoUONom = A1076Usuario_CargoUONom;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Usuario_CargoUONom", AV12Usuario_CargoUONom);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CARGOUONOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV12Usuario_CargoUONom, "@!"))));
               AV15ContagemResultado_Requisitante = A58Usuario_PessoaNom;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_Requisitante", AV15ContagemResultado_Requisitante);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_REQUISITANTE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15ContagemResultado_Requisitante, "@!"))));
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(10);
         }
         /* Execute user subroutine: 'BUSCA.CONTRATOS' */
         S112 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S122 ();
         if (returnInSub) return;
      }

      public void GXEnter( )
      {
         /* Execute user event: E12T62 */
         E12T62 ();
         if (returnInSub) return;
      }

      protected void E12T62( )
      {
         /* Enter Routine */
      }

      protected void S162( )
      {
         /* 'CHECKREQUIREDFIELDS' Routine */
         AV19CheckRequiredFieldsResult = true;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11ContagemResultado_DemandaFM)) )
         {
            GX_msglist.addItem("N� OS � obrigat�rio.");
            AV19CheckRequiredFieldsResult = false;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17ContagemResultado_Descricao)) )
         {
            GX_msglist.addItem("T�tulo � obrigat�rio.");
            AV19CheckRequiredFieldsResult = false;
         }
      }

      protected void S122( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
         if ( ! ( ! AV13ContagemResultado_RequisitadoPorContratante ) )
         {
            edtavUsuario_cargouonom_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargouonom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargouonom_Visible), 5, 0)));
            cellUsuario_cargouonom_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_cargouonom_cell_Internalname, "Class", cellUsuario_cargouonom_cell_Class);
            cellTextblockusuario_cargouonom_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_cargouonom_cell_Internalname, "Class", cellTextblockusuario_cargouonom_cell_Class);
         }
         else
         {
            edtavUsuario_cargouonom_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargouonom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargouonom_Visible), 5, 0)));
            cellUsuario_cargouonom_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_cargouonom_cell_Internalname, "Class", cellUsuario_cargouonom_cell_Class);
            cellTextblockusuario_cargouonom_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_cargouonom_cell_Internalname, "Class", cellTextblockusuario_cargouonom_cell_Class);
         }
         if ( ! ( ! AV10WWPContext.gxTpr_Userehcontratante ) )
         {
            chkavContagemresultado_requisitadoporcontratante.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContagemresultado_requisitadoporcontratante_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContagemresultado_requisitadoporcontratante.Visible), 5, 0)));
            cellContagemresultado_requisitadoporcontratante_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_requisitadoporcontratante_cell_Internalname, "Class", cellContagemresultado_requisitadoporcontratante_cell_Class);
            cellTextblockcontagemresultado_requisitadoporcontratante_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_requisitadoporcontratante_cell_Internalname, "Class", cellTextblockcontagemresultado_requisitadoporcontratante_cell_Class);
         }
         else
         {
            chkavContagemresultado_requisitadoporcontratante.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContagemresultado_requisitadoporcontratante_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContagemresultado_requisitadoporcontratante.Visible), 5, 0)));
            cellContagemresultado_requisitadoporcontratante_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_requisitadoporcontratante_cell_Internalname, "Class", cellContagemresultado_requisitadoporcontratante_cell_Class);
            cellTextblockcontagemresultado_requisitadoporcontratante_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_requisitadoporcontratante_cell_Internalname, "Class", cellTextblockcontagemresultado_requisitadoporcontratante_cell_Class);
         }
         if ( ! ( ( AV13ContagemResultado_RequisitadoPorContratante ) ) )
         {
            dynavContagemresultado_owner.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_owner_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_owner.Visible), 5, 0)));
            cellContagemresultado_owner_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_owner_cell_Internalname, "Class", cellContagemresultado_owner_cell_Class);
            cellTextblockcontagemresultado_owner_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_owner_cell_Internalname, "Class", cellTextblockcontagemresultado_owner_cell_Class);
         }
         else
         {
            dynavContagemresultado_owner.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_owner_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_owner.Visible), 5, 0)));
            cellContagemresultado_owner_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_owner_cell_Internalname, "Class", cellContagemresultado_owner_cell_Class);
            cellTextblockcontagemresultado_owner_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_owner_cell_Internalname, "Class", cellTextblockcontagemresultado_owner_cell_Class);
         }
         if ( ! ( ! AV13ContagemResultado_RequisitadoPorContratante ) )
         {
            edtavContagemresultado_requisitante_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_requisitante_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_requisitante_Visible), 5, 0)));
            cellContagemresultado_requisitante_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_requisitante_cell_Internalname, "Class", cellContagemresultado_requisitante_cell_Class);
            cellTextblockcontagemresultado_requisitante_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_requisitante_cell_Internalname, "Class", cellTextblockcontagemresultado_requisitante_cell_Class);
         }
         else
         {
            edtavContagemresultado_requisitante_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_requisitante_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_requisitante_Visible), 5, 0)));
            cellContagemresultado_requisitante_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_requisitante_cell_Internalname, "Class", cellContagemresultado_requisitante_cell_Class);
            cellTextblockcontagemresultado_requisitante_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_requisitante_cell_Internalname, "Class", cellTextblockcontagemresultado_requisitante_cell_Class);
         }
         if ( ! ( ( cmbavContagemresultado_cntcod.ItemCount > 2 ) ) )
         {
            cmbavContagemresultado_cntcod.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_cntcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_cntcod.Visible), 5, 0)));
            cellContagemresultado_cntcod_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_cntcod_cell_Internalname, "Class", cellContagemresultado_cntcod_cell_Class);
            cellTextblockcontagemresultado_cntcod_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_cntcod_cell_Internalname, "Class", cellTextblockcontagemresultado_cntcod_cell_Class);
         }
         else
         {
            cmbavContagemresultado_cntcod.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_cntcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_cntcod.Visible), 5, 0)));
            cellContagemresultado_cntcod_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_cntcod_cell_Internalname, "Class", cellContagemresultado_cntcod_cell_Class);
            cellTextblockcontagemresultado_cntcod_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_cntcod_cell_Internalname, "Class", cellTextblockcontagemresultado_cntcod_cell_Class);
         }
         if ( ! ( ( AV30ContratoServicos_QntUntCns > Convert.ToDecimal( 0 )) ) )
         {
            edtavContagemresultado_quantidadesolicitada_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_quantidadesolicitada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_quantidadesolicitada_Visible), 5, 0)));
            cellContagemresultado_quantidadesolicitada_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_quantidadesolicitada_cell_Internalname, "Class", cellContagemresultado_quantidadesolicitada_cell_Class);
            cellTextblockcontagemresultado_quantidadesolicitada_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_quantidadesolicitada_cell_Internalname, "Class", cellTextblockcontagemresultado_quantidadesolicitada_cell_Class);
         }
         else
         {
            edtavContagemresultado_quantidadesolicitada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_quantidadesolicitada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_quantidadesolicitada_Visible), 5, 0)));
            cellContagemresultado_quantidadesolicitada_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_quantidadesolicitada_cell_Internalname, "Class", cellContagemresultado_quantidadesolicitada_cell_Class);
            cellTextblockcontagemresultado_quantidadesolicitada_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_quantidadesolicitada_cell_Internalname, "Class", cellTextblockcontagemresultado_quantidadesolicitada_cell_Class);
         }
         if ( ! ( ( cmbavContratoservicosprazo_complexidade.ItemCount > 1 ) ) )
         {
            cmbavContratoservicosprazo_complexidade.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_complexidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicosprazo_complexidade.Visible), 5, 0)));
            cellContratoservicosprazo_complexidade_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratoservicosprazo_complexidade_cell_Internalname, "Class", cellContratoservicosprazo_complexidade_cell_Class);
            cellTextblockcontratoservicosprazo_complexidade_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratoservicosprazo_complexidade_cell_Internalname, "Class", cellTextblockcontratoservicosprazo_complexidade_cell_Class);
         }
         else
         {
            cmbavContratoservicosprazo_complexidade.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_complexidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicosprazo_complexidade.Visible), 5, 0)));
            cellContratoservicosprazo_complexidade_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratoservicosprazo_complexidade_cell_Internalname, "Class", cellContratoservicosprazo_complexidade_cell_Class);
            cellTextblockcontratoservicosprazo_complexidade_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratoservicosprazo_complexidade_cell_Internalname, "Class", cellTextblockcontratoservicosprazo_complexidade_cell_Class);
         }
         if ( ! ( ( cmbavContratoservicosprioridade_codigo.ItemCount > 1 ) ) )
         {
            cmbavContratoservicosprioridade_codigo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprioridade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicosprioridade_codigo.Visible), 5, 0)));
            cellContratoservicosprioridade_codigo_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratoservicosprioridade_codigo_cell_Internalname, "Class", cellContratoservicosprioridade_codigo_cell_Class);
            cellTextblockcontratoservicosprioridade_codigo_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratoservicosprioridade_codigo_cell_Internalname, "Class", cellTextblockcontratoservicosprioridade_codigo_cell_Class);
         }
         else
         {
            cmbavContratoservicosprioridade_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprioridade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicosprioridade_codigo.Visible), 5, 0)));
            cellContratoservicosprioridade_codigo_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratoservicosprioridade_codigo_cell_Internalname, "Class", cellContratoservicosprioridade_codigo_cell_Class);
            cellTextblockcontratoservicosprioridade_codigo_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratoservicosprioridade_codigo_cell_Internalname, "Class", cellTextblockcontratoservicosprioridade_codigo_cell_Class);
         }
         if ( ! ( ( AV55Servico_IsOrigemReferencia ) ) )
         {
            dynavContagemresultado_contratadaorigemcod.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadaorigemcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadaorigemcod.Visible), 5, 0)));
            cellContagemresultado_contratadaorigemcod_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_contratadaorigemcod_cell_Internalname, "Class", cellContagemresultado_contratadaorigemcod_cell_Class);
            cellTextblockcontagemresultado_contratadaorigemcod_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_contratadaorigemcod_cell_Internalname, "Class", cellTextblockcontagemresultado_contratadaorigemcod_cell_Class);
         }
         else
         {
            dynavContagemresultado_contratadaorigemcod.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadaorigemcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadaorigemcod.Visible), 5, 0)));
            cellContagemresultado_contratadaorigemcod_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_contratadaorigemcod_cell_Internalname, "Class", cellContagemresultado_contratadaorigemcod_cell_Class);
            cellTextblockcontagemresultado_contratadaorigemcod_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_contratadaorigemcod_cell_Internalname, "Class", cellTextblockcontagemresultado_contratadaorigemcod_cell_Class);
         }
         if ( ! ( ( AV55Servico_IsOrigemReferencia ) ) )
         {
            dynavContagemresultado_contadorfscod.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfscod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfscod.Visible), 5, 0)));
            cellContagemresultado_contadorfscod_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_contadorfscod_cell_Internalname, "Class", cellContagemresultado_contadorfscod_cell_Class);
            cellTextblockcontagemresultado_contadorfscod_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_contadorfscod_cell_Internalname, "Class", cellTextblockcontagemresultado_contadorfscod_cell_Class);
         }
         else
         {
            dynavContagemresultado_contadorfscod.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfscod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfscod.Visible), 5, 0)));
            cellContagemresultado_contadorfscod_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_contadorfscod_cell_Internalname, "Class", cellContagemresultado_contadorfscod_cell_Class);
            cellTextblockcontagemresultado_contadorfscod_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_contadorfscod_cell_Internalname, "Class", cellTextblockcontagemresultado_contadorfscod_cell_Class);
         }
         if ( ! ( ( AV56Servico_IsSolicitaGestorSistema ) ) )
         {
            edtavContagemresultado_sistemagestor_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_sistemagestor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_sistemagestor_Visible), 5, 0)));
            cellContagemresultado_sistemagestor_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_sistemagestor_cell_Internalname, "Class", cellContagemresultado_sistemagestor_cell_Class);
            cellTextblockcontagemresultado_sistemagestor_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_sistemagestor_cell_Internalname, "Class", cellTextblockcontagemresultado_sistemagestor_cell_Class);
         }
         else
         {
            edtavContagemresultado_sistemagestor_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_sistemagestor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_sistemagestor_Visible), 5, 0)));
            cellContagemresultado_sistemagestor_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_sistemagestor_cell_Internalname, "Class", cellContagemresultado_sistemagestor_cell_Class);
            cellTextblockcontagemresultado_sistemagestor_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_sistemagestor_cell_Internalname, "Class", cellTextblockcontagemresultado_sistemagestor_cell_Class);
         }
      }

      protected void S172( )
      {
         /* 'SHOW MESSAGES' Routine */
         AV62GXV1 = 1;
         while ( AV62GXV1 <= AV6Messages.Count )
         {
            AV5Message = ((SdtMessages_Message)AV6Messages.Item(AV62GXV1));
            GX_msglist.addItem(AV5Message.gxTpr_Description);
            AV62GXV1 = (int)(AV62GXV1+1);
         }
      }

      protected void E13T62( )
      {
         /* Refresh Routine */
         new geralog(context ).execute( ref  AV59Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         GXt_char9 = "Event Refresh";
         new geralog(context ).execute( ref  GXt_char9) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7Context) ;
      }

      protected void E14T62( )
      {
         /* Contagemresultado_servicogrupo_Click Routine */
         /* Execute user subroutine: 'BUSCA.SERVICOS' */
         S132 ();
         if (returnInSub) return;
         dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servico_Internalname, "Values", dynavContagemresultado_servico.ToJavascriptSource());
      }

      protected void E15T62( )
      {
         /* Contagemresultado_servico_Click Routine */
         new geralog(context ).execute( ref  AV59Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         GXt_char9 = "Event  &ContagemResultado_Servico.Click";
         new geralog(context ).execute( ref  GXt_char9) ;
         /* Execute user subroutine: 'BUSCA.DADOS.SERVICO' */
         S142 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S122 ();
         if (returnInSub) return;
         cmbavContagemresultado_cntcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_cntcod_Internalname, "Values", cmbavContagemresultado_cntcod.ToJavascriptSource());
         dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servico_Internalname, "Values", dynavContagemresultado_servico.ToJavascriptSource());
         cmbavContratoservicosprazo_complexidade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34ContratoServicosPrazo_Complexidade), 3, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_complexidade_Internalname, "Values", cmbavContratoservicosprazo_complexidade.ToJavascriptSource());
         cmbavContratoservicosprioridade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContratoServicosPrioridade_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprioridade_codigo_Internalname, "Values", cmbavContratoservicosprioridade_codigo.ToJavascriptSource());
      }

      protected void E16T62( )
      {
         /* Contratoservicosprazo_complexidade_Click Routine */
         new geralog(context ).execute( ref  AV59Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         GXt_char9 = "Event &ContratoServicosPrazo_Complexidade.click";
         new geralog(context ).execute( ref  GXt_char9) ;
      }

      protected void E17T62( )
      {
         /* Contratoservicosprioridade_codigo_Click Routine */
         new geralog(context ).execute( ref  AV59Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         GXt_char8 = "Event &ContratoServicosPrioridade_Codigo.click";
         new geralog(context ).execute( ref  GXt_char8) ;
      }

      protected void E18T62( )
      {
         /* Contagemresultado_contratadacod_Click Routine */
         new geralog(context ).execute( ref  AV59Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         GXt_char7 = "Event &ContagemResultado_ContratadaCod.Click";
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = "&ContagemResultado_ContratadaCod = " + context.localUtil.Format( (decimal)(AV36ContagemResultado_ContratadaCod), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char6) ;
         GX_msglist.addItem("&ContagemResultado_ContratadaCod = "+context.localUtil.Format( (decimal)(AV36ContagemResultado_ContratadaCod), "ZZZZZ9"));
         /* Execute user subroutine: 'BUSCA.RESPONSAVEL' */
         S152 ();
         if (returnInSub) return;
         dynavContagemresultado_contratadacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_ContratadaCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Values", dynavContagemresultado_contratadacod.ToJavascriptSource());
         cmbavContagemresultado_responsavel.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_Responsavel), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_responsavel_Internalname, "Values", cmbavContagemresultado_responsavel.ToJavascriptSource());
      }

      protected void E19T62( )
      {
         /* Contagemresultado_sistemacod_Click Routine */
         if ( AV56Servico_IsSolicitaGestorSistema )
         {
            GXt_char9 = AV53ContagemResultado_SistemaGestor;
            new prc_getsistemaresponsavelidentificacao(context ).execute(  AV50ContagemResultado_SistemaCod,  AV10WWPContext.gxTpr_Areatrabalho_codigo, out  GXt_char9) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0)));
            AV53ContagemResultado_SistemaGestor = GXt_char9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContagemResultado_SistemaGestor", AV53ContagemResultado_SistemaGestor);
         }
      }

      protected void S112( )
      {
         /* 'BUSCA.CONTRATOS' Routine */
         new geralog(context ).execute( ref  AV59Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         GXt_char8 = "Busca.Contratos";
         new geralog(context ).execute( ref  GXt_char8) ;
         cmbavContagemresultado_cntcod.removeAllItems();
         cmbavContagemresultado_cntcod.addItem("0", "Selecione", 0);
         AV18ContagemResultado_CntCod = AV27ResetCampoNumerico6;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0)));
         AV25ContagemResultado_CntPrpCod = 0;
         AV26Contrato_IsVencido = false;
         pr_default.dynParam(11, new Object[]{ new Object[]{
                                              AV10WWPContext.gxTpr_Userehcontratante ,
                                              AV10WWPContext.gxTpr_Userehcontratada ,
                                              A75Contrato_AreaTrabalhoCod ,
                                              AV10WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A39Contratada_Codigo ,
                                              AV10WWPContext.gxTpr_Contratada_codigo ,
                                              A92Contrato_Ativo ,
                                              A843Contrato_DataFimTA ,
                                              AV16ContagemResultado_DataDmn ,
                                              A83Contrato_DataVigenciaTermino },
                                              new int[] {
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE
                                              }
         });
         /* Using cursor H00T616 */
         pr_default.execute(11, new Object[] {AV16ContagemResultado_DataDmn, AV16ContagemResultado_DataDmn, AV10WWPContext.gxTpr_Areatrabalho_codigo, AV10WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A40Contratada_PessoaCod = H00T616_A40Contratada_PessoaCod[0];
            A39Contratada_Codigo = H00T616_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = H00T616_A75Contrato_AreaTrabalhoCod[0];
            A1013Contrato_PrepostoCod = H00T616_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00T616_n1013Contrato_PrepostoCod[0];
            A74Contrato_Codigo = H00T616_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00T616_n74Contrato_Codigo[0];
            A843Contrato_DataFimTA = H00T616_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00T616_n843Contrato_DataFimTA[0];
            A83Contrato_DataVigenciaTermino = H00T616_A83Contrato_DataVigenciaTermino[0];
            A41Contratada_PessoaNom = H00T616_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = H00T616_n41Contratada_PessoaNom[0];
            A92Contrato_Ativo = H00T616_A92Contrato_Ativo[0];
            A77Contrato_Numero = H00T616_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = H00T616_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = H00T616_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = H00T616_n41Contratada_PessoaNom[0];
            A843Contrato_DataFimTA = H00T616_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00T616_n843Contrato_DataFimTA[0];
            A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2096Contrato_Identificacao", A2096Contrato_Identificacao);
            A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
            cmbavContagemresultado_cntcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)), A2096Contrato_Identificacao, 0);
            AV18ContagemResultado_CntCod = A74Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0)));
            AV25ContagemResultado_CntPrpCod = A1013Contrato_PrepostoCod;
            AV26Contrato_IsVencido = (bool)((AV16ContagemResultado_DataDmn>A1869Contrato_DataTermino));
            pr_default.readNext(11);
         }
         pr_default.close(11);
         if ( cmbavContagemresultado_cntcod.ItemCount > 2 )
         {
            AV18ContagemResultado_CntCod = AV27ResetCampoNumerico6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0)));
         }
         else
         {
            if ( (0==AV25ContagemResultado_CntPrpCod) )
            {
               Ucconfirmpanel_Confirmationtext = StringUtil.Format( "A %1 n�o tem Preposto estabelecido! A OS n�o poder ser solicitada.", cmbavContagemresultado_cntcod.Description, "", "", "", "", "", "", "", "");
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ucconfirmpanel_Internalname, "ConfirmationText", Ucconfirmpanel_Confirmationtext);
               Ucconfirmpanel_Title = "Prestadora";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ucconfirmpanel_Internalname, "Title", Ucconfirmpanel_Title);
               this.executeUsercontrolMethod("", false, "UCCONFIRMPANELContainer", "Confirm", "", new Object[] {});
            }
            if ( AV26Contrato_IsVencido )
            {
               Ucconfirmpanel_Confirmationtext = StringUtil.Format( "O Contrato %1 do servi�o informado esta vencido!", cmbavContagemresultado_cntcod.Description, "", "", "", "", "", "", "", "");
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ucconfirmpanel_Internalname, "ConfirmationText", Ucconfirmpanel_Confirmationtext);
               Ucconfirmpanel_Title = "Prestadora";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ucconfirmpanel_Internalname, "Title", Ucconfirmpanel_Title);
               this.executeUsercontrolMethod("", false, "UCCONFIRMPANELContainer", "Confirm", "", new Object[] {});
            }
         }
      }

      protected void S132( )
      {
         /* 'BUSCA.SERVICOS' Routine */
         new geralog(context ).execute( ref  AV59Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         GXt_char9 = "Busca.Servicos";
         new geralog(context ).execute( ref  GXt_char9) ;
         dynavContagemresultado_servico.removeAllItems();
         dynavContagemresultado_servico.addItem("0", "Selecione", 0);
         /* Using cursor H00T617 */
         pr_default.execute(12, new Object[] {AV18ContagemResultado_CntCod, AV8ContagemResultado_ServicoGrupo});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A39Contratada_Codigo = H00T617_A39Contratada_Codigo[0];
            A43Contratada_Ativo = H00T617_A43Contratada_Ativo[0];
            A92Contrato_Ativo = H00T617_A92Contrato_Ativo[0];
            A638ContratoServicos_Ativo = H00T617_A638ContratoServicos_Ativo[0];
            A157ServicoGrupo_Codigo = H00T617_A157ServicoGrupo_Codigo[0];
            A74Contrato_Codigo = H00T617_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00T617_n74Contrato_Codigo[0];
            A608Servico_Nome = H00T617_A608Servico_Nome[0];
            A605Servico_Sigla = H00T617_A605Servico_Sigla[0];
            A155Servico_Codigo = H00T617_A155Servico_Codigo[0];
            A39Contratada_Codigo = H00T617_A39Contratada_Codigo[0];
            A92Contrato_Ativo = H00T617_A92Contrato_Ativo[0];
            A43Contratada_Ativo = H00T617_A43Contratada_Ativo[0];
            A157ServicoGrupo_Codigo = H00T617_A157ServicoGrupo_Codigo[0];
            A608Servico_Nome = H00T617_A608Servico_Nome[0];
            A605Servico_Sigla = H00T617_A605Servico_Sigla[0];
            GXt_char9 = " 1 - For - Busca.Servicos";
            new geralog(context ).execute( ref  GXt_char9) ;
            dynavContagemresultado_servico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)), StringUtil.Format( "%1 - %2", StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), "", "", "", "", "", "", ""), 0);
            pr_default.readNext(12);
         }
         pr_default.close(12);
         GXt_char9 = "Busca.Servicos";
         new geralog(context ).execute( ref  GXt_char9) ;
         GXt_char8 = "Busca.Servicos";
         new geralog(context ).execute( ref  GXt_char8) ;
         GXt_char7 = "Busca.Servicos";
         new geralog(context ).execute( ref  GXt_char7) ;
      }

      protected void S142( )
      {
         /* 'BUSCA.DADOS.SERVICO' Routine */
         new geralog(context ).execute( ref  AV59Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Pgmname", AV59Pgmname);
         GXt_char6 = "Busca.Dados.Servico ";
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = "&ContagemResultado_CntCod =  " + context.localUtil.Format( (decimal)(AV18ContagemResultado_CntCod), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char4 = "&ContagemResultado_Servico = " + context.localUtil.Format( (decimal)(AV9ContagemResultado_Servico), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char4) ;
         AV28ContratoServicos_UndCntNome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContratoServicos_UndCntNome", AV28ContratoServicos_UndCntNome);
         AV30ContratoServicos_QntUntCns = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( AV30ContratoServicos_QntUntCns, 9, 4)));
         /* Using cursor H00T618 */
         pr_default.execute(13, new Object[] {AV18ContagemResultado_CntCod, AV9ContagemResultado_Servico});
         while ( (pr_default.getStatus(13) != 101) )
         {
            A39Contratada_Codigo = H00T618_A39Contratada_Codigo[0];
            A1013Contrato_PrepostoCod = H00T618_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00T618_n1013Contrato_PrepostoCod[0];
            A1212ContratoServicos_UnidadeContratada = H00T618_A1212ContratoServicos_UnidadeContratada[0];
            A160ContratoServicos_Codigo = H00T618_A160ContratoServicos_Codigo[0];
            A74Contrato_Codigo = H00T618_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00T618_n74Contrato_Codigo[0];
            A638ContratoServicos_Ativo = H00T618_A638ContratoServicos_Ativo[0];
            A75Contrato_AreaTrabalhoCod = H00T618_A75Contrato_AreaTrabalhoCod[0];
            A92Contrato_Ativo = H00T618_A92Contrato_Ativo[0];
            A52Contratada_AreaTrabalhoCod = H00T618_A52Contratada_AreaTrabalhoCod[0];
            A157ServicoGrupo_Codigo = H00T618_A157ServicoGrupo_Codigo[0];
            A43Contratada_Ativo = H00T618_A43Contratada_Ativo[0];
            A54Usuario_Ativo = H00T618_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00T618_n54Usuario_Ativo[0];
            A155Servico_Codigo = H00T618_A155Servico_Codigo[0];
            A2132ContratoServicos_UndCntNome = H00T618_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = H00T618_n2132ContratoServicos_UndCntNome[0];
            A1340ContratoServicos_QntUntCns = H00T618_A1340ContratoServicos_QntUntCns[0];
            n1340ContratoServicos_QntUntCns = H00T618_n1340ContratoServicos_QntUntCns[0];
            A2092Servico_IsOrigemReferencia = H00T618_A2092Servico_IsOrigemReferencia[0];
            A2094ContratoServicos_SolicitaGestorSistema = H00T618_A2094ContratoServicos_SolicitaGestorSistema[0];
            A2132ContratoServicos_UndCntNome = H00T618_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = H00T618_n2132ContratoServicos_UndCntNome[0];
            A39Contratada_Codigo = H00T618_A39Contratada_Codigo[0];
            A1013Contrato_PrepostoCod = H00T618_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00T618_n1013Contrato_PrepostoCod[0];
            A75Contrato_AreaTrabalhoCod = H00T618_A75Contrato_AreaTrabalhoCod[0];
            A92Contrato_Ativo = H00T618_A92Contrato_Ativo[0];
            A52Contratada_AreaTrabalhoCod = H00T618_A52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00T618_A43Contratada_Ativo[0];
            A54Usuario_Ativo = H00T618_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00T618_n54Usuario_Ativo[0];
            A157ServicoGrupo_Codigo = H00T618_A157ServicoGrupo_Codigo[0];
            A2092Servico_IsOrigemReferencia = H00T618_A2092Servico_IsOrigemReferencia[0];
            AV28ContratoServicos_UndCntNome = A2132ContratoServicos_UndCntNome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContratoServicos_UndCntNome", AV28ContratoServicos_UndCntNome);
            AV30ContratoServicos_QntUntCns = A1340ContratoServicos_QntUntCns;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( AV30ContratoServicos_QntUntCns, 9, 4)));
            AV55Servico_IsOrigemReferencia = A2092Servico_IsOrigemReferencia;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Servico_IsOrigemReferencia", AV55Servico_IsOrigemReferencia);
            AV56Servico_IsSolicitaGestorSistema = A2094ContratoServicos_SolicitaGestorSistema;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Servico_IsSolicitaGestorSistema", AV56Servico_IsSolicitaGestorSistema);
            GXt_char9 = "&&ContratoServicos_UndCntNome = " + StringUtil.RTrim( context.localUtil.Format( AV28ContratoServicos_UndCntNome, "@!"));
            new geralog(context ).execute( ref  GXt_char9) ;
            GXt_char8 = "&&ContratoServicos_QntUntCns = " + context.localUtil.Format( AV30ContratoServicos_QntUntCns, "ZZZ9.9999");
            new geralog(context ).execute( ref  GXt_char8) ;
            cmbavContratoservicosprazo_complexidade.removeAllItems();
            cmbavContratoservicosprazo_complexidade.addItem("0", "Selecione", 0);
            /* Using cursor H00T619 */
            pr_default.execute(14, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(14) != 101) )
            {
               A903ContratoServicosPrazo_CntSrvCod = H00T619_A903ContratoServicosPrazo_CntSrvCod[0];
               A1823ContratoServicosPrazo_Momento = H00T619_A1823ContratoServicosPrazo_Momento[0];
               n1823ContratoServicosPrazo_Momento = H00T619_n1823ContratoServicosPrazo_Momento[0];
               A1265ContratoServicosPrazo_DiasBaixa = H00T619_A1265ContratoServicosPrazo_DiasBaixa[0];
               n1265ContratoServicosPrazo_DiasBaixa = H00T619_n1265ContratoServicosPrazo_DiasBaixa[0];
               A1264ContratoServicosPrazo_DiasMedia = H00T619_A1264ContratoServicosPrazo_DiasMedia[0];
               n1264ContratoServicosPrazo_DiasMedia = H00T619_n1264ContratoServicosPrazo_DiasMedia[0];
               A1263ContratoServicosPrazo_DiasAlta = H00T619_A1263ContratoServicosPrazo_DiasAlta[0];
               n1263ContratoServicosPrazo_DiasAlta = H00T619_n1263ContratoServicosPrazo_DiasAlta[0];
               GXt_char9 = "Busca.Dados.Servico - carrega ContratoServicosPrazo";
               new geralog(context ).execute( ref  GXt_char9) ;
               AV33ContratoServicosPrazo_Momento = A1823ContratoServicosPrazo_Momento;
               if ( A1265ContratoServicosPrazo_DiasBaixa > 0 )
               {
                  cmbavContratoservicosprazo_complexidade.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0)), "Baixa ("+StringUtil.Trim( StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0))+" dias)", 0);
               }
               if ( A1264ContratoServicosPrazo_DiasMedia > 0 )
               {
                  cmbavContratoservicosprazo_complexidade.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0)), "M�dia ("+StringUtil.Trim( StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0))+" dias)", 0);
               }
               if ( A1263ContratoServicosPrazo_DiasAlta > 0 )
               {
                  cmbavContratoservicosprazo_complexidade.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0)), "Alta ("+StringUtil.Trim( StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0))+" dias)", 0);
               }
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(14);
            cmbavContratoservicosprioridade_codigo.removeAllItems();
            cmbavContratoservicosprioridade_codigo.addItem("0", "Normal", 0);
            /* Using cursor H00T620 */
            pr_default.execute(15, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(15) != 101) )
            {
               A1335ContratoServicosPrioridade_CntSrvCod = H00T620_A1335ContratoServicosPrioridade_CntSrvCod[0];
               A1337ContratoServicosPrioridade_Nome = H00T620_A1337ContratoServicosPrioridade_Nome[0];
               A1336ContratoServicosPrioridade_Codigo = H00T620_A1336ContratoServicosPrioridade_Codigo[0];
               GXt_char9 = "Busca.Dados.Servico - carrega ContratoServicosPrioridade";
               new geralog(context ).execute( ref  GXt_char9) ;
               cmbavContratoservicosprioridade_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)), A1337ContratoServicosPrioridade_Nome, 0);
               pr_default.readNext(15);
            }
            pr_default.close(15);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(13);
         }
         pr_default.close(13);
      }

      protected void S152( )
      {
         /* 'BUSCA.RESPONSAVEL' Routine */
         cmbavContagemresultado_responsavel.removeAllItems();
         cmbavContagemresultado_responsavel.addItem("0", "Selecione", 0);
         /* Using cursor H00T621 */
         pr_default.execute(16, new Object[] {AV36ContagemResultado_ContratadaCod});
         while ( (pr_default.getStatus(16) != 101) )
         {
            A70ContratadaUsuario_UsuarioPessoaCod = H00T621_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = H00T621_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A1908Usuario_DeFerias = H00T621_A1908Usuario_DeFerias[0];
            n1908Usuario_DeFerias = H00T621_n1908Usuario_DeFerias[0];
            A1394ContratadaUsuario_UsuarioAtivo = H00T621_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = H00T621_n1394ContratadaUsuario_UsuarioAtivo[0];
            A66ContratadaUsuario_ContratadaCod = H00T621_A66ContratadaUsuario_ContratadaCod[0];
            A71ContratadaUsuario_UsuarioPessoaNom = H00T621_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = H00T621_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A69ContratadaUsuario_UsuarioCod = H00T621_A69ContratadaUsuario_UsuarioCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = H00T621_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = H00T621_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A1908Usuario_DeFerias = H00T621_A1908Usuario_DeFerias[0];
            n1908Usuario_DeFerias = H00T621_n1908Usuario_DeFerias[0];
            A1394ContratadaUsuario_UsuarioAtivo = H00T621_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = H00T621_n1394ContratadaUsuario_UsuarioAtivo[0];
            A71ContratadaUsuario_UsuarioPessoaNom = H00T621_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = H00T621_n71ContratadaUsuario_UsuarioPessoaNom[0];
            cmbavContagemresultado_responsavel.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)), A71ContratadaUsuario_UsuarioPessoaNom, 0);
            GX_msglist.addItem(StringUtil.Format( "%1 - %2", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)), A71ContratadaUsuario_UsuarioPessoaNom, "", "", "", "", "", "", ""));
            pr_default.readNext(16);
         }
         pr_default.close(16);
      }

      protected void nextLoad( )
      {
      }

      protected void E20T62( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_T62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "Nova Ordem de Servi�o", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_T62( true) ;
         }
         else
         {
            wb_table2_8_T62( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_T62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_204_T62( true) ;
         }
         else
         {
            wb_table3_204_T62( false) ;
         }
         return  ;
      }

      protected void wb_table3_204_T62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"UCCONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_T62e( true) ;
         }
         else
         {
            wb_table1_2_T62e( false) ;
         }
      }

      protected void wb_table3_204_T62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 207,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbntartefatos_Internalname, "", "Artefatos", bttBtnbntartefatos_Jsonclick, 7, "Artefatos", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e21t61_client"+"'", TempTags, "", 2, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 209,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 211,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_204_T62e( true) ;
         }
         else
         {
            wb_table3_204_T62e( false) ;
         }
      }

      protected void wb_table2_8_T62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTabSolicitar"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Solicita��o") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TabSolicitar"+"\" style=\"display:none;\">") ;
            wb_table4_17_T62( true) ;
         }
         else
         {
            wb_table4_17_T62( false) ;
         }
         return  ;
      }

      protected void wb_table4_17_T62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTabDescricao"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Descri��o Complementar") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TabDescricao"+"\" style=\"display:none;\">") ;
            wb_table5_187_T62( true) ;
         }
         else
         {
            wb_table5_187_T62( false) ;
         }
         return  ;
      }

      protected void wb_table5_187_T62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTabRequisitos"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Requisitos") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TabRequisitos"+"\" style=\"display:none;\">") ;
            wb_table6_192_T62( true) ;
         }
         else
         {
            wb_table6_192_T62( false) ;
         }
         return  ;
      }

      protected void wb_table6_192_T62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table7_199_T62( true) ;
         }
         else
         {
            wb_table7_199_T62( false) ;
         }
         return  ;
      }

      protected void wb_table7_199_T62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_T62e( true) ;
         }
         else
         {
            wb_table2_8_T62e( false) ;
         }
      }

      protected void wb_table7_199_T62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_199_T62e( true) ;
         }
         else
         {
            wb_table7_199_T62e( false) ;
         }
      }

      protected void wb_table6_192_T62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablerequisitos_Internalname, tblTablerequisitos_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_192_T62e( true) ;
         }
         else
         {
            wb_table6_192_T62e( false) ;
         }
      }

      protected void wb_table5_187_T62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledescricao_Internalname, tblTabledescricao_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_187_T62e( true) ;
         }
         else
         {
            wb_table5_187_T62e( false) ;
         }
      }

      protected void wb_table4_17_T62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesolicita_Internalname, tblTablesolicita_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_demandafm_Internalname, "N� OS", "", "", lblTextblockcontagemresultado_demandafm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm_Internalname, AV11ContagemResultado_DemandaFM, StringUtil.RTrim( context.localUtil.Format( AV11ContagemResultado_DemandaFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContagemresultado_demandafm_Enabled, 1, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockusuario_cargouonom_cell_Internalname+"\"  class='"+cellTextblockusuario_cargouonom_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_cargouonom_Internalname, "Unidade Organizacional", "", "", lblTextblockusuario_cargouonom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellUsuario_cargouonom_cell_Internalname+"\"  class='"+cellUsuario_cargouonom_cell_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_cargouonom_Internalname, StringUtil.RTrim( AV12Usuario_CargoUONom), StringUtil.RTrim( context.localUtil.Format( AV12Usuario_CargoUONom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_cargouonom_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_cargouonom_Visible, edtavUsuario_cargouonom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontagemresultado_requisitadoporcontratante_cell_Internalname+"\"  class='"+cellTextblockcontagemresultado_requisitadoporcontratante_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_requisitadoporcontratante_Internalname, "Requisitado por", "", "", lblTextblockcontagemresultado_requisitadoporcontratante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_requisitadoporcontratante_cell_Internalname+"\" colspan=\"3\"  class='"+cellContagemresultado_requisitadoporcontratante_cell_Class+"'>") ;
            wb_table8_34_T62( true) ;
         }
         else
         {
            wb_table8_34_T62( false) ;
         }
         return  ;
      }

      protected void wb_table8_34_T62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontagemresultado_owner_cell_Internalname+"\"  class='"+cellTextblockcontagemresultado_owner_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_owner_Internalname, "Requisitante", "", "", lblTextblockcontagemresultado_owner_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_owner_cell_Internalname+"\" colspan=\"3\"  class='"+cellContagemresultado_owner_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_owner, dynavContagemresultado_owner_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14ContagemResultado_Owner), 6, 0)), 1, dynavContagemresultado_owner_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_owner.Visible, dynavContagemresultado_owner.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_WP_testeCombo.htm");
            dynavContagemresultado_owner.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14ContagemResultado_Owner), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_owner_Internalname, "Values", (String)(dynavContagemresultado_owner.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontagemresultado_requisitante_cell_Internalname+"\"  class='"+cellTextblockcontagemresultado_requisitante_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_requisitante_Internalname, "Requisitante", "", "", lblTextblockcontagemresultado_requisitante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_requisitante_cell_Internalname+"\"  class='"+cellContagemresultado_requisitante_cell_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_requisitante_Internalname, StringUtil.RTrim( AV15ContagemResultado_Requisitante), StringUtil.RTrim( context.localUtil.Format( AV15ContagemResultado_Requisitante, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_requisitante_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_requisitante_Visible, edtavContagemresultado_requisitante_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_datadmn_Internalname, "Data da OS", "", "", lblTextblockcontagemresultado_datadmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_Internalname, context.localUtil.Format(AV16ContagemResultado_DataDmn, "99/99/99"), context.localUtil.Format( AV16ContagemResultado_DataDmn, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavContagemresultado_datadmn_Enabled, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_testeCombo.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_datadmn_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_testeCombo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_descricao_Internalname, "T�tulo", "", "", lblTextblockcontagemresultado_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_descricao_Internalname, AV17ContagemResultado_Descricao, StringUtil.RTrim( context.localUtil.Format( AV17ContagemResultado_Descricao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontagemresultado_cntcod_cell_Internalname+"\"  class='"+cellTextblockcontagemresultado_cntcod_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_cntcod_Internalname, "Contrato", "", "", lblTextblockcontagemresultado_cntcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_cntcod_cell_Internalname+"\" colspan=\"3\"  class='"+cellContagemresultado_cntcod_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_cntcod, cmbavContagemresultado_cntcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0)), 1, cmbavContagemresultado_cntcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavContagemresultado_cntcod.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "", true, "HLP_WP_testeCombo.htm");
            cmbavContagemresultado_cntcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18ContagemResultado_CntCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_cntcod_Internalname, "Values", (String)(cmbavContagemresultado_cntcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_servicogrupo_Internalname, "Grupo de Servi�os", "", "", lblTextblockcontagemresultado_servicogrupo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_servicogrupo, dynavContagemresultado_servicogrupo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV8ContagemResultado_ServicoGrupo), 6, 0)), 1, dynavContagemresultado_servicogrupo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_SERVICOGRUPO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_WP_testeCombo.htm");
            dynavContagemresultado_servicogrupo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8ContagemResultado_ServicoGrupo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servicogrupo_Internalname, "Values", (String)(dynavContagemresultado_servicogrupo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_servico_Internalname, "Servi�o", "", "", lblTextblockcontagemresultado_servico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_servico, dynavContagemresultado_servico_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0)), 1, dynavContagemresultado_servico_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_SERVICO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "", true, "HLP_WP_testeCombo.htm");
            dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servico_Internalname, "Values", (String)(dynavContagemresultado_servico.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_undcntnome_Internalname, "Unidade de Contrata��o", "", "", lblTextblockcontratoservicos_undcntnome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_undcntnome_Internalname, StringUtil.RTrim( AV28ContratoServicos_UndCntNome), StringUtil.RTrim( context.localUtil.Format( AV28ContratoServicos_UndCntNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_undcntnome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratoservicos_undcntnome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_qntuntcns_Internalname, "Qtde Unit de Consumo", "", "", lblTextblockcontratoservicos_qntuntcns_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_qntuntcns_Internalname, StringUtil.LTrim( StringUtil.NToC( AV30ContratoServicos_QntUntCns, 9, 4, ",", "")), ((edtavContratoservicos_qntuntcns_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV30ContratoServicos_QntUntCns, "ZZZ9.9999")) : context.localUtil.Format( AV30ContratoServicos_QntUntCns, "ZZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_qntuntcns_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratoservicos_qntuntcns_Enabled, 0, "text", "", 80, "px", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontagemresultado_quantidadesolicitada_cell_Internalname+"\"  class='"+cellTextblockcontagemresultado_quantidadesolicitada_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_quantidadesolicitada_Internalname, "Quantidade Solicitada", "", "", lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_quantidadesolicitada_cell_Internalname+"\" colspan=\"3\"  class='"+cellContagemresultado_quantidadesolicitada_cell_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_quantidadesolicitada_Internalname, StringUtil.LTrim( StringUtil.NToC( AV31ContagemResultado_QuantidadeSolicitada, 9, 4, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV31ContagemResultado_QuantidadeSolicitada, "ZZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_quantidadesolicitada_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_quantidadesolicitada_Visible, 1, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontratoservicosprazo_complexidade_cell_Internalname+"\"  class='"+cellTextblockcontratoservicosprazo_complexidade_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprazo_complexidade_Internalname, "Complexidade", "", "", lblTextblockcontratoservicosprazo_complexidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratoservicosprazo_complexidade_cell_Internalname+"\" colspan=\"3\"  class='"+cellContratoservicosprazo_complexidade_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicosprazo_complexidade, cmbavContratoservicosprazo_complexidade_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV34ContratoServicosPrazo_Complexidade), 3, 0)), 1, cmbavContratoservicosprazo_complexidade_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATOSERVICOSPRAZO_COMPLEXIDADE.CLICK."+"'", "int", "", cmbavContratoservicosprazo_complexidade.Visible, 1, 0, 0, 60, "px", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", "", true, "HLP_WP_testeCombo.htm");
            cmbavContratoservicosprazo_complexidade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34ContratoServicosPrazo_Complexidade), 3, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_complexidade_Internalname, "Values", (String)(cmbavContratoservicosprazo_complexidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontratoservicosprioridade_codigo_cell_Internalname+"\"  class='"+cellTextblockcontratoservicosprioridade_codigo_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprioridade_codigo_Internalname, "Prioridade", "", "", lblTextblockcontratoservicosprioridade_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratoservicosprioridade_codigo_cell_Internalname+"\" colspan=\"3\"  class='"+cellContratoservicosprioridade_codigo_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicosprioridade_codigo, cmbavContratoservicosprioridade_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContratoServicosPrioridade_Codigo), 6, 0)), 1, cmbavContratoservicosprioridade_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATOSERVICOSPRIORIDADE_CODIGO.CLICK."+"'", "int", "", cmbavContratoservicosprioridade_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", "", true, "HLP_WP_testeCombo.htm");
            cmbavContratoservicosprioridade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContratoServicosPrioridade_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprioridade_codigo_Internalname, "Values", (String)(cmbavContratoservicosprioridade_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contratadacod_Internalname, "Requisitado", "", "", lblTextblockcontagemresultado_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contratadacod, dynavContagemresultado_contratadacod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_ContratadaCod), 6, 0)), 1, dynavContagemresultado_contratadacod_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_CONTRATADACOD.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", "", true, "HLP_WP_testeCombo.htm");
            dynavContagemresultado_contratadacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_ContratadaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Values", (String)(dynavContagemresultado_contratadacod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_responsavel_Internalname, "Respons�vel", "", "", lblTextblockcontagemresultado_responsavel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_responsavel, cmbavContagemresultado_responsavel_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_Responsavel), 6, 0)), 1, cmbavContagemresultado_responsavel_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", "", true, "HLP_WP_testeCombo.htm");
            cmbavContagemresultado_responsavel.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_Responsavel), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_responsavel_Internalname, "Values", (String)(cmbavContagemresultado_responsavel.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblvincularcom_Internalname, "Vincular com", "", "", lblLblvincularcom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_osvinculada_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40ContagemResultado_OSVinculada), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40ContagemResultado_OSVinculada), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_osvinculada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dmnvinculada_Internalname, AV41ContagemResultado_DmnVinculada, StringUtil.RTrim( context.localUtil.Format( AV41ContagemResultado_DmnVinculada, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "N� OS", edtavContagemresultado_dmnvinculada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dmnvinculadaref_Internalname, AV42ContagemResultado_DmnVinculadaRef, StringUtil.RTrim( context.localUtil.Format( AV42ContagemResultado_DmnVinculadaRef, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "N� OS Refer�ncia", edtavContagemresultado_dmnvinculadaref_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfsimp_Internalname, "Bruto", "", "", lblTextblockcontagemresultado_pfbfsimp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pfbfsimp_Internalname, StringUtil.LTrim( StringUtil.NToC( AV43ContagemResultado_PFBFSImp, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV43ContagemResultado_PFBFSImp, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pfbfsimp_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfsimp_Internalname, "L�quido", "", "", lblTextblockcontagemresultado_pflfsimp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pflfsimp_Internalname, StringUtil.LTrim( StringUtil.NToC( AV44ContagemResultado_PFLFSImp, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV44ContagemResultado_PFLFSImp, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pflfsimp_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontagemresultado_contratadaorigemcod_cell_Internalname+"\"  class='"+cellTextblockcontagemresultado_contratadaorigemcod_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contratadaorigemcod_Internalname, "Origem da Refer�ncia", "", "", lblTextblockcontagemresultado_contratadaorigemcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_contratadaorigemcod_cell_Internalname+"\" colspan=\"3\"  class='"+cellContagemresultado_contratadaorigemcod_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contratadaorigemcod, dynavContagemresultado_contratadaorigemcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV45ContagemResultado_ContratadaOrigemCod), 6, 0)), 1, dynavContagemresultado_contratadaorigemcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_contratadaorigemcod.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", "", true, "HLP_WP_testeCombo.htm");
            dynavContagemresultado_contratadaorigemcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV45ContagemResultado_ContratadaOrigemCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadaorigemcod_Internalname, "Values", (String)(dynavContagemresultado_contratadaorigemcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontagemresultado_contadorfscod_cell_Internalname+"\"  class='"+cellTextblockcontagemresultado_contadorfscod_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contadorfscod_Internalname, "Respons�vel na Origem", "", "", lblTextblockcontagemresultado_contadorfscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_contadorfscod_cell_Internalname+"\" colspan=\"3\"  class='"+cellContagemresultado_contadorfscod_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contadorfscod, dynavContagemresultado_contadorfscod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV46ContagemResultado_ContadorFSCod), 6, 0)), 1, dynavContagemresultado_contadorfscod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_contadorfscod.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,142);\"", "", true, "HLP_WP_testeCombo.htm");
            dynavContagemresultado_contadorfscod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV46ContagemResultado_ContadorFSCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfscod_Internalname, "Values", (String)(dynavContagemresultado_contadorfscod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_localexec_Internalname, "Local de Execu��o", "", "", lblTextblockcontratoservicos_localexec_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicos_localexec, cmbavContratoservicos_localexec_Internalname, StringUtil.RTrim( AV47ContratoServicos_LocalExec), 1, cmbavContratoservicos_localexec_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,147);\"", "", true, "HLP_WP_testeCombo.htm");
            cmbavContratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV47ContratoServicos_LocalExec);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Values", (String)(cmbavContratoservicos_localexec.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_prazoanalise_Internalname, "Prazo de An�lise", "", "", lblTextblockcontagemresultado_prazoanalise_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_prazoanalise_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_prazoanalise_Internalname, context.localUtil.Format(AV48ContagemResultado_PrazoAnalise, "99/99/99"), context.localUtil.Format( AV48ContagemResultado_PrazoAnalise, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,154);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_prazoanalise_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavContagemresultado_prazoanalise_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_testeCombo.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_prazoanalise_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_prazoanalise_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_testeCombo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_dataentrega_Internalname, "Prazo de Entrega", "", "", lblTextblockcontagemresultado_dataentrega_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentrega_Internalname, context.localUtil.Format(AV49ContagemResultado_DataEntrega, "99/99/99"), context.localUtil.Format( AV49ContagemResultado_DataEntrega, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,159);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentrega_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_testeCombo.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_testeCombo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_sistemacod_Internalname, "Sistema", "", "", lblTextblockcontagemresultado_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_sistemacod, dynavContagemresultado_sistemacod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0)), 1, dynavContagemresultado_sistemacod_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_SISTEMACOD.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,164);\"", "", true, "HLP_WP_testeCombo.htm");
            dynavContagemresultado_sistemacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod_Internalname, "Values", (String)(dynavContagemresultado_sistemacod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmodulo_codigo_Internalname, "M�dulo", "", "", lblTextblockmodulo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavModulo_codigo, dynavModulo_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV51Modulo_Codigo), 6, 0)), 1, dynavModulo_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,169);\"", "", true, "HLP_WP_testeCombo.htm");
            dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV51Modulo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Values", (String)(dynavModulo_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_fncusrcod_Internalname, "Fun��o de Usu�rio", "", "", lblTextblockcontagemresultado_fncusrcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 174,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_fncusrcod, dynavContagemresultado_fncusrcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_FncUsrCod), 6, 0)), 1, dynavContagemresultado_fncusrcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,174);\"", "", true, "HLP_WP_testeCombo.htm");
            dynavContagemresultado_fncusrcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_FncUsrCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_fncusrcod_Internalname, "Values", (String)(dynavContagemresultado_fncusrcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontagemresultado_sistemagestor_cell_Internalname+"\"  class='"+cellTextblockcontagemresultado_sistemagestor_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_sistemagestor_Internalname, "Gestor Respons�vel pelo Sistema", "", "", lblTextblockcontagemresultado_sistemagestor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_sistemagestor_cell_Internalname+"\" colspan=\"3\"  class='"+cellContagemresultado_sistemagestor_cell_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 179,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_sistemagestor_Internalname, StringUtil.RTrim( AV53ContagemResultado_SistemaGestor), StringUtil.RTrim( context.localUtil.Format( AV53ContagemResultado_SistemaGestor, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,179);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_sistemagestor_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_sistemagestor_Visible, edtavContagemresultado_sistemagestor_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_link_Internalname, "Link", "", "", lblTextblockcontagemresultado_link_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 184,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_link_Internalname, AV54ContagemResultado_Link, AV54ContagemResultado_Link, TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,184);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_link_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 360, "px", 1, "row", 2097152, 0, 1, 0, 1, 0, -1, true, "", "left", false, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_17_T62e( true) ;
         }
         else
         {
            wb_table4_17_T62e( false) ;
         }
      }

      protected void wb_table8_34_T62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_requisitadoporcontratante_Internalname, tblTablemergedcontagemresultado_requisitadoporcontratante_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavContagemresultado_requisitadoporcontratante_Internalname, StringUtil.BoolToStr( AV13ContagemResultado_RequisitadoPorContratante), "", "", chkavContagemresultado_requisitadoporcontratante.Visible, chkavContagemresultado_requisitadoporcontratante.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(37, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_requisitadoporcontratante_righttext_Internalname, lblContagemresultado_requisitadoporcontratante_righttext_Caption, "", "", lblContagemresultado_requisitadoporcontratante_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_testeCombo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_34_T62e( true) ;
         }
         else
         {
            wb_table8_34_T62e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAT62( ) ;
         WST62( ) ;
         WET62( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204323498");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_testecombo.js", "?20204323498");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         lblTextblockcontagemresultado_demandafm_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DEMANDAFM";
         edtavContagemresultado_demandafm_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM";
         lblTextblockusuario_cargouonom_Internalname = "TEXTBLOCKUSUARIO_CARGOUONOM";
         cellTextblockusuario_cargouonom_cell_Internalname = "TEXTBLOCKUSUARIO_CARGOUONOM_CELL";
         edtavUsuario_cargouonom_Internalname = "vUSUARIO_CARGOUONOM";
         cellUsuario_cargouonom_cell_Internalname = "USUARIO_CARGOUONOM_CELL";
         lblTextblockcontagemresultado_requisitadoporcontratante_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE";
         cellTextblockcontagemresultado_requisitadoporcontratante_cell_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE_CELL";
         chkavContagemresultado_requisitadoporcontratante_Internalname = "vCONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE";
         lblContagemresultado_requisitadoporcontratante_righttext_Internalname = "CONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE_RIGHTTEXT";
         tblTablemergedcontagemresultado_requisitadoporcontratante_Internalname = "TABLEMERGEDCONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE";
         cellContagemresultado_requisitadoporcontratante_cell_Internalname = "CONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE_CELL";
         lblTextblockcontagemresultado_owner_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_OWNER";
         cellTextblockcontagemresultado_owner_cell_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_OWNER_CELL";
         dynavContagemresultado_owner_Internalname = "vCONTAGEMRESULTADO_OWNER";
         cellContagemresultado_owner_cell_Internalname = "CONTAGEMRESULTADO_OWNER_CELL";
         lblTextblockcontagemresultado_requisitante_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_REQUISITANTE";
         cellTextblockcontagemresultado_requisitante_cell_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_REQUISITANTE_CELL";
         edtavContagemresultado_requisitante_Internalname = "vCONTAGEMRESULTADO_REQUISITANTE";
         cellContagemresultado_requisitante_cell_Internalname = "CONTAGEMRESULTADO_REQUISITANTE_CELL";
         lblTextblockcontagemresultado_datadmn_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATADMN";
         edtavContagemresultado_datadmn_Internalname = "vCONTAGEMRESULTADO_DATADMN";
         lblTextblockcontagemresultado_descricao_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DESCRICAO";
         edtavContagemresultado_descricao_Internalname = "vCONTAGEMRESULTADO_DESCRICAO";
         lblTextblockcontagemresultado_cntcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CNTCOD";
         cellTextblockcontagemresultado_cntcod_cell_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CNTCOD_CELL";
         cmbavContagemresultado_cntcod_Internalname = "vCONTAGEMRESULTADO_CNTCOD";
         cellContagemresultado_cntcod_cell_Internalname = "CONTAGEMRESULTADO_CNTCOD_CELL";
         lblTextblockcontagemresultado_servicogrupo_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_SERVICOGRUPO";
         dynavContagemresultado_servicogrupo_Internalname = "vCONTAGEMRESULTADO_SERVICOGRUPO";
         lblTextblockcontagemresultado_servico_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_SERVICO";
         dynavContagemresultado_servico_Internalname = "vCONTAGEMRESULTADO_SERVICO";
         lblTextblockcontratoservicos_undcntnome_Internalname = "TEXTBLOCKCONTRATOSERVICOS_UNDCNTNOME";
         edtavContratoservicos_undcntnome_Internalname = "vCONTRATOSERVICOS_UNDCNTNOME";
         lblTextblockcontratoservicos_qntuntcns_Internalname = "TEXTBLOCKCONTRATOSERVICOS_QNTUNTCNS";
         edtavContratoservicos_qntuntcns_Internalname = "vCONTRATOSERVICOS_QNTUNTCNS";
         lblTextblockcontagemresultado_quantidadesolicitada_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA";
         cellTextblockcontagemresultado_quantidadesolicitada_cell_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL";
         edtavContagemresultado_quantidadesolicitada_Internalname = "vCONTAGEMRESULTADO_QUANTIDADESOLICITADA";
         cellContagemresultado_quantidadesolicitada_cell_Internalname = "CONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL";
         lblTextblockcontratoservicosprazo_complexidade_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRAZO_COMPLEXIDADE";
         cellTextblockcontratoservicosprazo_complexidade_cell_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRAZO_COMPLEXIDADE_CELL";
         cmbavContratoservicosprazo_complexidade_Internalname = "vCONTRATOSERVICOSPRAZO_COMPLEXIDADE";
         cellContratoservicosprazo_complexidade_cell_Internalname = "CONTRATOSERVICOSPRAZO_COMPLEXIDADE_CELL";
         lblTextblockcontratoservicosprioridade_codigo_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_CODIGO";
         cellTextblockcontratoservicosprioridade_codigo_cell_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_CODIGO_CELL";
         cmbavContratoservicosprioridade_codigo_Internalname = "vCONTRATOSERVICOSPRIORIDADE_CODIGO";
         cellContratoservicosprioridade_codigo_cell_Internalname = "CONTRATOSERVICOSPRIORIDADE_CODIGO_CELL";
         lblTextblockcontagemresultado_contratadacod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CONTRATADACOD";
         dynavContagemresultado_contratadacod_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD";
         lblTextblockcontagemresultado_responsavel_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_RESPONSAVEL";
         cmbavContagemresultado_responsavel_Internalname = "vCONTAGEMRESULTADO_RESPONSAVEL";
         lblLblvincularcom_Internalname = "LBLVINCULARCOM";
         edtavContagemresultado_osvinculada_Internalname = "vCONTAGEMRESULTADO_OSVINCULADA";
         edtavContagemresultado_dmnvinculada_Internalname = "vCONTAGEMRESULTADO_DMNVINCULADA";
         edtavContagemresultado_dmnvinculadaref_Internalname = "vCONTAGEMRESULTADO_DMNVINCULADAREF";
         lblTextblockcontagemresultado_pfbfsimp_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFSIMP";
         edtavContagemresultado_pfbfsimp_Internalname = "vCONTAGEMRESULTADO_PFBFSIMP";
         lblTextblockcontagemresultado_pflfsimp_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFSIMP";
         edtavContagemresultado_pflfsimp_Internalname = "vCONTAGEMRESULTADO_PFLFSIMP";
         lblTextblockcontagemresultado_contratadaorigemcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CONTRATADAORIGEMCOD";
         cellTextblockcontagemresultado_contratadaorigemcod_cell_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CONTRATADAORIGEMCOD_CELL";
         dynavContagemresultado_contratadaorigemcod_Internalname = "vCONTAGEMRESULTADO_CONTRATADAORIGEMCOD";
         cellContagemresultado_contratadaorigemcod_cell_Internalname = "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD_CELL";
         lblTextblockcontagemresultado_contadorfscod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CONTADORFSCOD";
         cellTextblockcontagemresultado_contadorfscod_cell_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CONTADORFSCOD_CELL";
         dynavContagemresultado_contadorfscod_Internalname = "vCONTAGEMRESULTADO_CONTADORFSCOD";
         cellContagemresultado_contadorfscod_cell_Internalname = "CONTAGEMRESULTADO_CONTADORFSCOD_CELL";
         lblTextblockcontratoservicos_localexec_Internalname = "TEXTBLOCKCONTRATOSERVICOS_LOCALEXEC";
         cmbavContratoservicos_localexec_Internalname = "vCONTRATOSERVICOS_LOCALEXEC";
         lblTextblockcontagemresultado_prazoanalise_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PRAZOANALISE";
         edtavContagemresultado_prazoanalise_Internalname = "vCONTAGEMRESULTADO_PRAZOANALISE";
         lblTextblockcontagemresultado_dataentrega_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATAENTREGA";
         edtavContagemresultado_dataentrega_Internalname = "vCONTAGEMRESULTADO_DATAENTREGA";
         lblTextblockcontagemresultado_sistemacod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_SISTEMACOD";
         dynavContagemresultado_sistemacod_Internalname = "vCONTAGEMRESULTADO_SISTEMACOD";
         lblTextblockmodulo_codigo_Internalname = "TEXTBLOCKMODULO_CODIGO";
         dynavModulo_codigo_Internalname = "vMODULO_CODIGO";
         lblTextblockcontagemresultado_fncusrcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_FNCUSRCOD";
         dynavContagemresultado_fncusrcod_Internalname = "vCONTAGEMRESULTADO_FNCUSRCOD";
         lblTextblockcontagemresultado_sistemagestor_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_SISTEMAGESTOR";
         cellTextblockcontagemresultado_sistemagestor_cell_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_SISTEMAGESTOR_CELL";
         edtavContagemresultado_sistemagestor_Internalname = "vCONTAGEMRESULTADO_SISTEMAGESTOR";
         cellContagemresultado_sistemagestor_cell_Internalname = "CONTAGEMRESULTADO_SISTEMAGESTOR_CELL";
         lblTextblockcontagemresultado_link_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_LINK";
         edtavContagemresultado_link_Internalname = "vCONTAGEMRESULTADO_LINK";
         tblTablesolicita_Internalname = "TABLESOLICITA";
         tblTabledescricao_Internalname = "TABLEDESCRICAO";
         tblTablerequisitos_Internalname = "TABLEREQUISITOS";
         Gxuitabspanel_tabmain_Internalname = "GXUITABSPANEL_TABMAIN";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnbntartefatos_Internalname = "BTNBNTARTEFATOS";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ucconfirmpanel_Internalname = "UCCONFIRMPANEL";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavContagemresultado_link_Jsonclick = "";
         edtavContagemresultado_sistemagestor_Jsonclick = "";
         edtavContagemresultado_sistemagestor_Enabled = 1;
         cellContagemresultado_sistemagestor_cell_Class = "";
         cellTextblockcontagemresultado_sistemagestor_cell_Class = "";
         dynavContagemresultado_fncusrcod_Jsonclick = "";
         dynavModulo_codigo_Jsonclick = "";
         dynavContagemresultado_sistemacod_Jsonclick = "";
         edtavContagemresultado_dataentrega_Jsonclick = "";
         edtavContagemresultado_prazoanalise_Jsonclick = "";
         edtavContagemresultado_prazoanalise_Enabled = 1;
         cmbavContratoservicos_localexec_Jsonclick = "";
         dynavContagemresultado_contadorfscod_Jsonclick = "";
         cellContagemresultado_contadorfscod_cell_Class = "";
         cellTextblockcontagemresultado_contadorfscod_cell_Class = "";
         dynavContagemresultado_contratadaorigemcod_Jsonclick = "";
         cellContagemresultado_contratadaorigemcod_cell_Class = "";
         cellTextblockcontagemresultado_contratadaorigemcod_cell_Class = "";
         edtavContagemresultado_pflfsimp_Jsonclick = "";
         edtavContagemresultado_pfbfsimp_Jsonclick = "";
         edtavContagemresultado_dmnvinculadaref_Jsonclick = "";
         edtavContagemresultado_dmnvinculada_Jsonclick = "";
         edtavContagemresultado_osvinculada_Jsonclick = "";
         cmbavContagemresultado_responsavel_Jsonclick = "";
         dynavContagemresultado_contratadacod_Jsonclick = "";
         cmbavContratoservicosprioridade_codigo_Jsonclick = "";
         cellContratoservicosprioridade_codigo_cell_Class = "";
         cellTextblockcontratoservicosprioridade_codigo_cell_Class = "";
         cmbavContratoservicosprazo_complexidade_Jsonclick = "";
         cellContratoservicosprazo_complexidade_cell_Class = "";
         cellTextblockcontratoservicosprazo_complexidade_cell_Class = "";
         edtavContagemresultado_quantidadesolicitada_Jsonclick = "";
         cellContagemresultado_quantidadesolicitada_cell_Class = "";
         cellTextblockcontagemresultado_quantidadesolicitada_cell_Class = "";
         edtavContratoservicos_qntuntcns_Jsonclick = "";
         edtavContratoservicos_qntuntcns_Enabled = 1;
         edtavContratoservicos_undcntnome_Jsonclick = "";
         edtavContratoservicos_undcntnome_Enabled = 1;
         dynavContagemresultado_servico_Jsonclick = "";
         dynavContagemresultado_servicogrupo_Jsonclick = "";
         cmbavContagemresultado_cntcod_Jsonclick = "";
         cellContagemresultado_cntcod_cell_Class = "";
         cellTextblockcontagemresultado_cntcod_cell_Class = "";
         edtavContagemresultado_descricao_Jsonclick = "";
         edtavContagemresultado_datadmn_Jsonclick = "";
         edtavContagemresultado_datadmn_Enabled = 1;
         edtavContagemresultado_requisitante_Jsonclick = "";
         edtavContagemresultado_requisitante_Enabled = 1;
         cellContagemresultado_requisitante_cell_Class = "";
         cellTextblockcontagemresultado_requisitante_cell_Class = "";
         dynavContagemresultado_owner_Jsonclick = "";
         cellContagemresultado_owner_cell_Class = "";
         cellTextblockcontagemresultado_owner_cell_Class = "";
         cellContagemresultado_requisitadoporcontratante_cell_Class = "";
         cellTextblockcontagemresultado_requisitadoporcontratante_cell_Class = "";
         edtavUsuario_cargouonom_Jsonclick = "";
         edtavUsuario_cargouonom_Enabled = 1;
         cellUsuario_cargouonom_cell_Class = "";
         cellTextblockusuario_cargouonom_cell_Class = "";
         edtavContagemresultado_demandafm_Jsonclick = "";
         cmbavContagemresultado_cntcod.Description = "";
         edtavContagemresultado_sistemagestor_Visible = 1;
         dynavContagemresultado_contadorfscod.Visible = 1;
         dynavContagemresultado_contratadaorigemcod.Visible = 1;
         cmbavContratoservicosprioridade_codigo.Visible = 1;
         cmbavContratoservicosprazo_complexidade.Visible = 1;
         edtavContagemresultado_quantidadesolicitada_Visible = 1;
         cmbavContagemresultado_cntcod.Visible = 1;
         edtavContagemresultado_requisitante_Visible = 1;
         dynavContagemresultado_owner.Visible = 1;
         chkavContagemresultado_requisitadoporcontratante.Visible = 1;
         edtavUsuario_cargouonom_Visible = 1;
         chkavContagemresultado_requisitadoporcontratante.Enabled = 1;
         dynavContagemresultado_owner.Enabled = 1;
         edtavContagemresultado_demandafm_Enabled = 1;
         lblContagemresultado_requisitadoporcontratante_righttext_Caption = "�(Contratante)";
         chkavContagemresultado_requisitadoporcontratante.Caption = "";
         Ucconfirmpanel_Confirmtype = "";
         Ucconfirmpanel_Yesbuttoncaption = "Fechar";
         Ucconfirmpanel_Confirmationtext = "";
         Ucconfirmpanel_Title = "Confirma��o";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��o Geral";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Gxuitabspanel_tabmain_Designtimetabs = "[{\"id\":\"TabSolicitar\"},{\"id\":\"TabDescricao\"},{\"id\":\"TabRequisitos\"}]";
         Gxuitabspanel_tabmain_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tabmain_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tabmain_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tabmain_Cls = "Tabs";
         Gxuitabspanel_tabmain_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Nova Ordem de Servi�o";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contagemresultado_cntcod( GXCombobox cmbGX_Parm1 ,
                                                   wwpbaseobjects.SdtWWPContext GX_Parm2 ,
                                                   GXCombobox dynGX_Parm3 ,
                                                   GXCombobox dynGX_Parm4 )
      {
         cmbavContagemresultado_cntcod = cmbGX_Parm1;
         AV18ContagemResultado_CntCod = (int)(NumberUtil.Val( cmbavContagemresultado_cntcod.CurrentValue, "."));
         AV10WWPContext = GX_Parm2;
         dynavContagemresultado_servicogrupo = dynGX_Parm3;
         AV8ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.CurrentValue, "."));
         dynavContagemresultado_contratadacod = dynGX_Parm4;
         AV36ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_SERVICOGRUPO_htmlT62( AV18ContagemResultado_CntCod, AV10WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlT62( AV18ContagemResultado_CntCod) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_servicogrupo.ItemCount > 0 )
         {
            AV8ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8ContagemResultado_ServicoGrupo), 6, 0))), "."));
         }
         dynavContagemresultado_servicogrupo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8ContagemResultado_ServicoGrupo), 6, 0));
         isValidOutput.Add(dynavContagemresultado_servicogrupo);
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV36ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_ContratadaCod), 6, 0))), "."));
         }
         dynavContagemresultado_contratadacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36ContagemResultado_ContratadaCod), 6, 0));
         isValidOutput.Add(dynavContagemresultado_contratadacod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contagemresultado_servicogrupo( GXCombobox dynGX_Parm1 ,
                                                         GXCombobox dynGX_Parm2 )
      {
         dynavContagemresultado_servicogrupo = dynGX_Parm1;
         AV8ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.CurrentValue, "."));
         dynavContagemresultado_servico = dynGX_Parm2;
         AV9ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_SERVICO_htmlT62( AV8ContagemResultado_ServicoGrupo) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV9ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0))), "."));
         }
         dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9ContagemResultado_Servico), 6, 0));
         isValidOutput.Add(dynavContagemresultado_servico);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contagemresultado_servico( GXCombobox dynGX_Parm1 ,
                                                    GXCombobox dynGX_Parm2 )
      {
         dynavContagemresultado_servico = dynGX_Parm1;
         AV9ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.CurrentValue, "."));
         dynavContagemresultado_sistemacod = dynGX_Parm2;
         AV50ContagemResultado_SistemaCod = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_SISTEMACOD_htmlT62( AV9ContagemResultado_Servico) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_sistemacod.ItemCount > 0 )
         {
            AV50ContagemResultado_SistemaCod = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0))), "."));
         }
         dynavContagemresultado_sistemacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod), 6, 0));
         isValidOutput.Add(dynavContagemresultado_sistemacod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contagemresultado_contratadaorigemcod( GXCombobox dynGX_Parm1 ,
                                                                GXCombobox dynGX_Parm2 )
      {
         dynavContagemresultado_contratadaorigemcod = dynGX_Parm1;
         AV45ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadaorigemcod.CurrentValue, "."));
         dynavContagemresultado_contadorfscod = dynGX_Parm2;
         AV46ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfscod.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_CONTADORFSCOD_htmlT62( AV45ContagemResultado_ContratadaOrigemCod) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_contadorfscod.ItemCount > 0 )
         {
            AV46ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfscod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV46ContagemResultado_ContadorFSCod), 6, 0))), "."));
         }
         dynavContagemresultado_contadorfscod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV46ContagemResultado_ContadorFSCod), 6, 0));
         isValidOutput.Add(dynavContagemresultado_contadorfscod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contagemresultado_sistemacod( GXCombobox dynGX_Parm1 ,
                                                       GXCombobox dynGX_Parm2 ,
                                                       GXCombobox dynGX_Parm3 )
      {
         dynavContagemresultado_sistemacod = dynGX_Parm1;
         AV50ContagemResultado_SistemaCod = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod.CurrentValue, "."));
         dynavModulo_codigo = dynGX_Parm2;
         AV51Modulo_Codigo = (int)(NumberUtil.Val( dynavModulo_codigo.CurrentValue, "."));
         dynavContagemresultado_fncusrcod = dynGX_Parm3;
         AV52ContagemResultado_FncUsrCod = (int)(NumberUtil.Val( dynavContagemresultado_fncusrcod.CurrentValue, "."));
         GXVvMODULO_CODIGO_htmlT62( AV50ContagemResultado_SistemaCod) ;
         GXVvCONTAGEMRESULTADO_FNCUSRCOD_htmlT62( AV50ContagemResultado_SistemaCod) ;
         dynload_actions( ) ;
         if ( dynavModulo_codigo.ItemCount > 0 )
         {
            AV51Modulo_Codigo = (int)(NumberUtil.Val( dynavModulo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV51Modulo_Codigo), 6, 0))), "."));
         }
         dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV51Modulo_Codigo), 6, 0));
         isValidOutput.Add(dynavModulo_codigo);
         if ( dynavContagemresultado_fncusrcod.ItemCount > 0 )
         {
            AV52ContagemResultado_FncUsrCod = (int)(NumberUtil.Val( dynavContagemresultado_fncusrcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_FncUsrCod), 6, 0))), "."));
         }
         dynavContagemresultado_fncusrcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV52ContagemResultado_FncUsrCod), 6, 0));
         isValidOutput.Add(dynavContagemresultado_fncusrcod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''}]}");
         setEventMetadata("'DOBNTARTEFATOS'","{handler:'E21T61',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12T62',iparms:[],oparms:[]}");
         setEventMetadata("VCONTAGEMRESULTADO_SERVICOGRUPO.CLICK","{handler:'E14T62',iparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV18ContagemResultado_CntCod',fld:'vCONTAGEMRESULTADO_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''}],oparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VCONTAGEMRESULTADO_SERVICO.CLICK","{handler:'E15T62',iparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV18ContagemResultado_CntCod',fld:'vCONTAGEMRESULTADO_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV9ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A2132ContratoServicos_UndCntNome',fld:'CONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'A1340ContratoServicos_QntUntCns',fld:'CONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'A2092Servico_IsOrigemReferencia',fld:'SERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'A2094ContratoServicos_SolicitaGestorSistema',fld:'CONTRATOSERVICOS_SOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A1823ContratoServicosPrazo_Momento',fld:'CONTRATOSERVICOSPRAZO_MOMENTO',pic:'ZZZ9',nv:0},{av:'A1265ContratoServicosPrazo_DiasBaixa',fld:'CONTRATOSERVICOSPRAZO_DIASBAIXA',pic:'ZZ9',nv:0},{av:'A1264ContratoServicosPrazo_DiasMedia',fld:'CONTRATOSERVICOSPRAZO_DIASMEDIA',pic:'ZZ9',nv:0},{av:'A1263ContratoServicosPrazo_DiasAlta',fld:'CONTRATOSERVICOSPRAZO_DIASALTA',pic:'ZZ9',nv:0},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1337ContratoServicosPrioridade_Nome',fld:'CONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV13ContagemResultado_RequisitadoPorContratante',fld:'vCONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE',pic:'',nv:false},{av:'AV10WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV30ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV55Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV56Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false}],oparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV18ContagemResultado_CntCod',fld:'vCONTAGEMRESULTADO_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'AV9ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV28ContratoServicos_UndCntNome',fld:'vCONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'AV30ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV55Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV56Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'AV34ContratoServicosPrazo_Complexidade',fld:'vCONTRATOSERVICOSPRAZO_COMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV35ContratoServicosPrioridade_Codigo',fld:'vCONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtavUsuario_cargouonom_Visible',ctrl:'vUSUARIO_CARGOUONOM',prop:'Visible'},{av:'cellUsuario_cargouonom_cell_Class',ctrl:'USUARIO_CARGOUONOM_CELL',prop:'Class'},{av:'cellTextblockusuario_cargouonom_cell_Class',ctrl:'TEXTBLOCKUSUARIO_CARGOUONOM_CELL',prop:'Class'},{av:'chkavContagemresultado_requisitadoporcontratante.Visible',ctrl:'vCONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE',prop:'Visible'},{av:'cellContagemresultado_requisitadoporcontratante_cell_Class',ctrl:'CONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_requisitadoporcontratante_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE_CELL',prop:'Class'},{av:'dynavContagemresultado_owner'},{av:'cellContagemresultado_owner_cell_Class',ctrl:'CONTAGEMRESULTADO_OWNER_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_owner_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_OWNER_CELL',prop:'Class'},{av:'edtavContagemresultado_requisitante_Visible',ctrl:'vCONTAGEMRESULTADO_REQUISITANTE',prop:'Visible'},{av:'cellContagemresultado_requisitante_cell_Class',ctrl:'CONTAGEMRESULTADO_REQUISITANTE_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_requisitante_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_REQUISITANTE_CELL',prop:'Class'},{av:'cmbavContagemresultado_cntcod'},{av:'cellContagemresultado_cntcod_cell_Class',ctrl:'CONTAGEMRESULTADO_CNTCOD_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_cntcod_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_CNTCOD_CELL',prop:'Class'},{av:'edtavContagemresultado_quantidadesolicitada_Visible',ctrl:'vCONTAGEMRESULTADO_QUANTIDADESOLICITADA',prop:'Visible'},{av:'cellContagemresultado_quantidadesolicitada_cell_Class',ctrl:'CONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_quantidadesolicitada_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'cmbavContratoservicosprazo_complexidade'},{av:'cellContratoservicosprazo_complexidade_cell_Class',ctrl:'CONTRATOSERVICOSPRAZO_COMPLEXIDADE_CELL',prop:'Class'},{av:'cellTextblockcontratoservicosprazo_complexidade_cell_Class',ctrl:'TEXTBLOCKCONTRATOSERVICOSPRAZO_COMPLEXIDADE_CELL',prop:'Class'},{av:'cmbavContratoservicosprioridade_codigo'},{av:'cellContratoservicosprioridade_codigo_cell_Class',ctrl:'CONTRATOSERVICOSPRIORIDADE_CODIGO_CELL',prop:'Class'},{av:'cellTextblockcontratoservicosprioridade_codigo_cell_Class',ctrl:'TEXTBLOCKCONTRATOSERVICOSPRIORIDADE_CODIGO_CELL',prop:'Class'},{av:'dynavContagemresultado_contratadaorigemcod'},{av:'cellContagemresultado_contratadaorigemcod_cell_Class',ctrl:'CONTAGEMRESULTADO_CONTRATADAORIGEMCOD_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_contratadaorigemcod_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_CONTRATADAORIGEMCOD_CELL',prop:'Class'},{av:'dynavContagemresultado_contadorfscod'},{av:'cellContagemresultado_contadorfscod_cell_Class',ctrl:'CONTAGEMRESULTADO_CONTADORFSCOD_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_contadorfscod_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_CONTADORFSCOD_CELL',prop:'Class'},{av:'edtavContagemresultado_sistemagestor_Visible',ctrl:'vCONTAGEMRESULTADO_SISTEMAGESTOR',prop:'Visible'},{av:'cellContagemresultado_sistemagestor_cell_Class',ctrl:'CONTAGEMRESULTADO_SISTEMAGESTOR_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_sistemagestor_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_SISTEMAGESTOR_CELL',prop:'Class'}]}");
         setEventMetadata("VCONTRATOSERVICOSPRAZO_COMPLEXIDADE.CLICK","{handler:'E16T62',iparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''}]}");
         setEventMetadata("VCONTRATOSERVICOSPRIORIDADE_CODIGO.CLICK","{handler:'E17T62',iparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''}]}");
         setEventMetadata("VCONTAGEMRESULTADO_CONTRATADACOD.CLICK","{handler:'E18T62',iparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV36ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV36ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV37ContagemResultado_Responsavel',fld:'vCONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VCONTAGEMRESULTADO_SISTEMACOD.CLICK","{handler:'E19T62',iparms:[{av:'AV56Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'AV50ContagemResultado_SistemaCod',fld:'vCONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV10WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV53ContagemResultado_SistemaGestor',fld:'vCONTAGEMRESULTADO_SISTEMAGESTOR',pic:'@!',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV10WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV59Pgmname = "";
         A605Servico_Sigla = "";
         A608Servico_Nome = "";
         A2132ContratoServicos_UndCntNome = "";
         A1337ContratoServicosPrioridade_Nome = "";
         AV56Servico_IsSolicitaGestorSistema = false;
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         AV12Usuario_CargoUONom = "";
         AV15ContagemResultado_Requisitante = "";
         AV16ContagemResultado_DataDmn = DateTime.MinValue;
         AV48ContagemResultado_PrazoAnalise = DateTime.MinValue;
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV47ContratoServicos_LocalExec = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00T63_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00T63_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00T63_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00T63_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00T63_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00T63_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00T63_A54Usuario_Ativo = new bool[] {false} ;
         H00T63_n54Usuario_Ativo = new bool[] {false} ;
         H00T63_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         H00T63_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00T64_A155Servico_Codigo = new int[1] ;
         H00T64_A160ContratoServicos_Codigo = new int[1] ;
         H00T64_A157ServicoGrupo_Codigo = new int[1] ;
         H00T64_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00T64_A74Contrato_Codigo = new int[1] ;
         H00T64_n74Contrato_Codigo = new bool[] {false} ;
         H00T64_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00T64_A92Contrato_Ativo = new bool[] {false} ;
         H00T64_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00T65_A39Contratada_Codigo = new int[1] ;
         H00T65_A74Contrato_Codigo = new int[1] ;
         H00T65_n74Contrato_Codigo = new bool[] {false} ;
         H00T65_A160ContratoServicos_Codigo = new int[1] ;
         H00T65_A155Servico_Codigo = new int[1] ;
         H00T65_A608Servico_Nome = new String[] {""} ;
         H00T65_A157ServicoGrupo_Codigo = new int[1] ;
         H00T65_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00T65_A92Contrato_Ativo = new bool[] {false} ;
         H00T65_A43Contratada_Ativo = new bool[] {false} ;
         H00T66_A40Contratada_PessoaCod = new int[1] ;
         H00T66_A160ContratoServicos_Codigo = new int[1] ;
         H00T66_A39Contratada_Codigo = new int[1] ;
         H00T66_A41Contratada_PessoaNom = new String[] {""} ;
         H00T66_n41Contratada_PessoaNom = new bool[] {false} ;
         H00T66_A74Contrato_Codigo = new int[1] ;
         H00T66_n74Contrato_Codigo = new bool[] {false} ;
         H00T66_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00T67_A40Contratada_PessoaCod = new int[1] ;
         H00T67_A39Contratada_Codigo = new int[1] ;
         H00T67_A41Contratada_PessoaNom = new String[] {""} ;
         H00T67_n41Contratada_PessoaNom = new bool[] {false} ;
         H00T67_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00T67_A43Contratada_Ativo = new bool[] {false} ;
         H00T68_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00T68_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00T68_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00T68_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00T68_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00T68_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00T69_A160ContratoServicos_Codigo = new int[1] ;
         H00T69_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         H00T69_A1064ContratoServicosSistemas_SistemaSigla = new String[] {""} ;
         H00T69_n1064ContratoServicosSistemas_SistemaSigla = new bool[] {false} ;
         H00T69_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         H00T610_A146Modulo_Codigo = new int[1] ;
         H00T610_A145Modulo_Sigla = new String[] {""} ;
         H00T610_A127Sistema_Codigo = new int[1] ;
         H00T611_A161FuncaoUsuario_Codigo = new int[1] ;
         H00T611_A162FuncaoUsuario_Nome = new String[] {""} ;
         H00T611_A127Sistema_Codigo = new int[1] ;
         H00T611_A164FuncaoUsuario_Ativo = new bool[] {false} ;
         AV11ContagemResultado_DemandaFM = "";
         AV17ContagemResultado_Descricao = "";
         AV28ContratoServicos_UndCntNome = "";
         AV41ContagemResultado_DmnVinculada = "";
         AV42ContagemResultado_DmnVinculadaRef = "";
         AV49ContagemResultado_DataEntrega = DateTime.MinValue;
         AV53ContagemResultado_SistemaGestor = "";
         AV54ContagemResultado_Link = "";
         hsh = "";
         GXt_char1 = "";
         GXt_char2 = "";
         GXt_char3 = "";
         H00T612_A29Contratante_Codigo = new int[1] ;
         H00T612_n29Contratante_Codigo = new bool[] {false} ;
         H00T612_A5AreaTrabalho_Codigo = new int[1] ;
         H00T612_A593Contratante_OSAutomatica = new bool[] {false} ;
         H00T612_A2085Contratante_RequerOrigem = new bool[] {false} ;
         H00T612_n2085Contratante_RequerOrigem = new bool[] {false} ;
         H00T612_A1822Contratante_UsaOSistema = new bool[] {false} ;
         H00T612_n1822Contratante_UsaOSistema = new bool[] {false} ;
         H00T612_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00T612_A548Contratante_EmailSdaUser = new String[] {""} ;
         H00T612_n548Contratante_EmailSdaUser = new bool[] {false} ;
         H00T612_A552Contratante_EmailSdaPort = new short[1] ;
         H00T612_n552Contratante_EmailSdaPort = new bool[] {false} ;
         H00T612_A547Contratante_EmailSdaHost = new String[] {""} ;
         H00T612_n547Contratante_EmailSdaHost = new bool[] {false} ;
         A6AreaTrabalho_Descricao = "";
         A548Contratante_EmailSdaUser = "";
         A547Contratante_EmailSdaHost = "";
         AV21Contratante_RequerOrigem = true;
         AV22Contratante_UsaOSistema = false;
         H00T613_A57Usuario_PessoaCod = new int[1] ;
         H00T613_A1073Usuario_CargoCod = new int[1] ;
         H00T613_n1073Usuario_CargoCod = new bool[] {false} ;
         H00T613_A1075Usuario_CargoUOCod = new int[1] ;
         H00T613_n1075Usuario_CargoUOCod = new bool[] {false} ;
         H00T613_A1Usuario_Codigo = new int[1] ;
         H00T613_A1076Usuario_CargoUONom = new String[] {""} ;
         H00T613_n1076Usuario_CargoUONom = new bool[] {false} ;
         H00T613_A58Usuario_PessoaNom = new String[] {""} ;
         H00T613_n58Usuario_PessoaNom = new bool[] {false} ;
         A1076Usuario_CargoUONom = "";
         A58Usuario_PessoaNom = "";
         AV6Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV5Message = new SdtMessages_Message(context);
         AV7Context = new wwpbaseobjects.SdtWWPContext(context);
         A843Contrato_DataFimTA = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         H00T616_A40Contratada_PessoaCod = new int[1] ;
         H00T616_A39Contratada_Codigo = new int[1] ;
         H00T616_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00T616_A1013Contrato_PrepostoCod = new int[1] ;
         H00T616_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00T616_A74Contrato_Codigo = new int[1] ;
         H00T616_n74Contrato_Codigo = new bool[] {false} ;
         H00T616_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00T616_n843Contrato_DataFimTA = new bool[] {false} ;
         H00T616_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00T616_A41Contratada_PessoaNom = new String[] {""} ;
         H00T616_n41Contratada_PessoaNom = new bool[] {false} ;
         H00T616_A92Contrato_Ativo = new bool[] {false} ;
         H00T616_A77Contrato_Numero = new String[] {""} ;
         A41Contratada_PessoaNom = "";
         A77Contrato_Numero = "";
         A2096Contrato_Identificacao = "";
         A1869Contrato_DataTermino = DateTime.MinValue;
         H00T617_A160ContratoServicos_Codigo = new int[1] ;
         H00T617_A39Contratada_Codigo = new int[1] ;
         H00T617_A43Contratada_Ativo = new bool[] {false} ;
         H00T617_A92Contrato_Ativo = new bool[] {false} ;
         H00T617_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00T617_A157ServicoGrupo_Codigo = new int[1] ;
         H00T617_A74Contrato_Codigo = new int[1] ;
         H00T617_n74Contrato_Codigo = new bool[] {false} ;
         H00T617_A608Servico_Nome = new String[] {""} ;
         H00T617_A605Servico_Sigla = new String[] {""} ;
         H00T617_A155Servico_Codigo = new int[1] ;
         GXt_char7 = "";
         GXt_char6 = "";
         GXt_char5 = "";
         GXt_char4 = "";
         H00T618_A39Contratada_Codigo = new int[1] ;
         H00T618_A1013Contrato_PrepostoCod = new int[1] ;
         H00T618_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00T618_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         H00T618_A160ContratoServicos_Codigo = new int[1] ;
         H00T618_A74Contrato_Codigo = new int[1] ;
         H00T618_n74Contrato_Codigo = new bool[] {false} ;
         H00T618_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00T618_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00T618_A92Contrato_Ativo = new bool[] {false} ;
         H00T618_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00T618_A157ServicoGrupo_Codigo = new int[1] ;
         H00T618_A43Contratada_Ativo = new bool[] {false} ;
         H00T618_A54Usuario_Ativo = new bool[] {false} ;
         H00T618_n54Usuario_Ativo = new bool[] {false} ;
         H00T618_A155Servico_Codigo = new int[1] ;
         H00T618_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         H00T618_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         H00T618_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         H00T618_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         H00T618_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         H00T618_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         GXt_char8 = "";
         H00T619_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00T619_A1823ContratoServicosPrazo_Momento = new short[1] ;
         H00T619_n1823ContratoServicosPrazo_Momento = new bool[] {false} ;
         H00T619_A1265ContratoServicosPrazo_DiasBaixa = new short[1] ;
         H00T619_n1265ContratoServicosPrazo_DiasBaixa = new bool[] {false} ;
         H00T619_A1264ContratoServicosPrazo_DiasMedia = new short[1] ;
         H00T619_n1264ContratoServicosPrazo_DiasMedia = new bool[] {false} ;
         H00T619_A1263ContratoServicosPrazo_DiasAlta = new short[1] ;
         H00T619_n1263ContratoServicosPrazo_DiasAlta = new bool[] {false} ;
         H00T620_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         H00T620_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         H00T620_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         GXt_char9 = "";
         H00T621_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00T621_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00T621_A1908Usuario_DeFerias = new bool[] {false} ;
         H00T621_n1908Usuario_DeFerias = new bool[] {false} ;
         H00T621_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00T621_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00T621_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00T621_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00T621_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00T621_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         sStyleString = "";
         lblTextblocktitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnbntartefatos_Jsonclick = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblTextblockcontagemresultado_demandafm_Jsonclick = "";
         lblTextblockusuario_cargouonom_Jsonclick = "";
         lblTextblockcontagemresultado_requisitadoporcontratante_Jsonclick = "";
         lblTextblockcontagemresultado_owner_Jsonclick = "";
         lblTextblockcontagemresultado_requisitante_Jsonclick = "";
         lblTextblockcontagemresultado_datadmn_Jsonclick = "";
         lblTextblockcontagemresultado_descricao_Jsonclick = "";
         lblTextblockcontagemresultado_cntcod_Jsonclick = "";
         lblTextblockcontagemresultado_servicogrupo_Jsonclick = "";
         lblTextblockcontagemresultado_servico_Jsonclick = "";
         lblTextblockcontratoservicos_undcntnome_Jsonclick = "";
         lblTextblockcontratoservicos_qntuntcns_Jsonclick = "";
         lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick = "";
         lblTextblockcontratoservicosprazo_complexidade_Jsonclick = "";
         lblTextblockcontratoservicosprioridade_codigo_Jsonclick = "";
         lblTextblockcontagemresultado_contratadacod_Jsonclick = "";
         lblTextblockcontagemresultado_responsavel_Jsonclick = "";
         lblLblvincularcom_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfsimp_Jsonclick = "";
         lblTextblockcontagemresultado_pflfsimp_Jsonclick = "";
         lblTextblockcontagemresultado_contratadaorigemcod_Jsonclick = "";
         lblTextblockcontagemresultado_contadorfscod_Jsonclick = "";
         lblTextblockcontratoservicos_localexec_Jsonclick = "";
         lblTextblockcontagemresultado_prazoanalise_Jsonclick = "";
         lblTextblockcontagemresultado_dataentrega_Jsonclick = "";
         lblTextblockcontagemresultado_sistemacod_Jsonclick = "";
         lblTextblockmodulo_codigo_Jsonclick = "";
         lblTextblockcontagemresultado_fncusrcod_Jsonclick = "";
         lblTextblockcontagemresultado_sistemagestor_Jsonclick = "";
         lblTextblockcontagemresultado_link_Jsonclick = "";
         lblContagemresultado_requisitadoporcontratante_righttext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_testecombo__default(),
            new Object[][] {
                new Object[] {
               H00T63_A61ContratanteUsuario_UsuarioPessoaCod, H00T63_n61ContratanteUsuario_UsuarioPessoaCod, H00T63_A63ContratanteUsuario_ContratanteCod, H00T63_A60ContratanteUsuario_UsuarioCod, H00T63_A62ContratanteUsuario_UsuarioPessoaNom, H00T63_n62ContratanteUsuario_UsuarioPessoaNom, H00T63_A54Usuario_Ativo, H00T63_n54Usuario_Ativo, H00T63_A1020ContratanteUsuario_AreaTrabalhoCod, H00T63_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               H00T64_A155Servico_Codigo, H00T64_A160ContratoServicos_Codigo, H00T64_A157ServicoGrupo_Codigo, H00T64_A158ServicoGrupo_Descricao, H00T64_A74Contrato_Codigo, H00T64_A75Contrato_AreaTrabalhoCod, H00T64_A92Contrato_Ativo, H00T64_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00T65_A39Contratada_Codigo, H00T65_A74Contrato_Codigo, H00T65_A160ContratoServicos_Codigo, H00T65_A155Servico_Codigo, H00T65_A608Servico_Nome, H00T65_A157ServicoGrupo_Codigo, H00T65_A638ContratoServicos_Ativo, H00T65_A92Contrato_Ativo, H00T65_A43Contratada_Ativo
               }
               , new Object[] {
               H00T66_A40Contratada_PessoaCod, H00T66_A160ContratoServicos_Codigo, H00T66_A39Contratada_Codigo, H00T66_A41Contratada_PessoaNom, H00T66_n41Contratada_PessoaNom, H00T66_A74Contrato_Codigo, H00T66_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00T67_A40Contratada_PessoaCod, H00T67_A39Contratada_Codigo, H00T67_A41Contratada_PessoaNom, H00T67_n41Contratada_PessoaNom, H00T67_A52Contratada_AreaTrabalhoCod, H00T67_A43Contratada_Ativo
               }
               , new Object[] {
               H00T68_A70ContratadaUsuario_UsuarioPessoaCod, H00T68_n70ContratadaUsuario_UsuarioPessoaCod, H00T68_A69ContratadaUsuario_UsuarioCod, H00T68_A71ContratadaUsuario_UsuarioPessoaNom, H00T68_n71ContratadaUsuario_UsuarioPessoaNom, H00T68_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00T69_A160ContratoServicos_Codigo, H00T69_A1063ContratoServicosSistemas_SistemaCod, H00T69_A1064ContratoServicosSistemas_SistemaSigla, H00T69_n1064ContratoServicosSistemas_SistemaSigla, H00T69_A1067ContratoServicosSistemas_ServicoCod
               }
               , new Object[] {
               H00T610_A146Modulo_Codigo, H00T610_A145Modulo_Sigla, H00T610_A127Sistema_Codigo
               }
               , new Object[] {
               H00T611_A161FuncaoUsuario_Codigo, H00T611_A162FuncaoUsuario_Nome, H00T611_A127Sistema_Codigo, H00T611_A164FuncaoUsuario_Ativo
               }
               , new Object[] {
               H00T612_A29Contratante_Codigo, H00T612_n29Contratante_Codigo, H00T612_A5AreaTrabalho_Codigo, H00T612_A593Contratante_OSAutomatica, H00T612_A2085Contratante_RequerOrigem, H00T612_n2085Contratante_RequerOrigem, H00T612_A1822Contratante_UsaOSistema, H00T612_n1822Contratante_UsaOSistema, H00T612_A6AreaTrabalho_Descricao, H00T612_A548Contratante_EmailSdaUser,
               H00T612_n548Contratante_EmailSdaUser, H00T612_A552Contratante_EmailSdaPort, H00T612_n552Contratante_EmailSdaPort, H00T612_A547Contratante_EmailSdaHost, H00T612_n547Contratante_EmailSdaHost
               }
               , new Object[] {
               H00T613_A57Usuario_PessoaCod, H00T613_A1073Usuario_CargoCod, H00T613_n1073Usuario_CargoCod, H00T613_A1075Usuario_CargoUOCod, H00T613_n1075Usuario_CargoUOCod, H00T613_A1Usuario_Codigo, H00T613_A1076Usuario_CargoUONom, H00T613_n1076Usuario_CargoUONom, H00T613_A58Usuario_PessoaNom, H00T613_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00T616_A40Contratada_PessoaCod, H00T616_A39Contratada_Codigo, H00T616_A75Contrato_AreaTrabalhoCod, H00T616_A1013Contrato_PrepostoCod, H00T616_n1013Contrato_PrepostoCod, H00T616_A74Contrato_Codigo, H00T616_A843Contrato_DataFimTA, H00T616_n843Contrato_DataFimTA, H00T616_A83Contrato_DataVigenciaTermino, H00T616_A41Contratada_PessoaNom,
               H00T616_n41Contratada_PessoaNom, H00T616_A92Contrato_Ativo, H00T616_A77Contrato_Numero
               }
               , new Object[] {
               H00T617_A160ContratoServicos_Codigo, H00T617_A39Contratada_Codigo, H00T617_A43Contratada_Ativo, H00T617_A92Contrato_Ativo, H00T617_A638ContratoServicos_Ativo, H00T617_A157ServicoGrupo_Codigo, H00T617_A74Contrato_Codigo, H00T617_A608Servico_Nome, H00T617_A605Servico_Sigla, H00T617_A155Servico_Codigo
               }
               , new Object[] {
               H00T618_A39Contratada_Codigo, H00T618_A1013Contrato_PrepostoCod, H00T618_n1013Contrato_PrepostoCod, H00T618_A1212ContratoServicos_UnidadeContratada, H00T618_A160ContratoServicos_Codigo, H00T618_A74Contrato_Codigo, H00T618_A638ContratoServicos_Ativo, H00T618_A75Contrato_AreaTrabalhoCod, H00T618_A92Contrato_Ativo, H00T618_A52Contratada_AreaTrabalhoCod,
               H00T618_A157ServicoGrupo_Codigo, H00T618_A43Contratada_Ativo, H00T618_A54Usuario_Ativo, H00T618_n54Usuario_Ativo, H00T618_A155Servico_Codigo, H00T618_A2132ContratoServicos_UndCntNome, H00T618_n2132ContratoServicos_UndCntNome, H00T618_A1340ContratoServicos_QntUntCns, H00T618_n1340ContratoServicos_QntUntCns, H00T618_A2092Servico_IsOrigemReferencia,
               H00T618_A2094ContratoServicos_SolicitaGestorSistema
               }
               , new Object[] {
               H00T619_A903ContratoServicosPrazo_CntSrvCod, H00T619_A1823ContratoServicosPrazo_Momento, H00T619_n1823ContratoServicosPrazo_Momento, H00T619_A1265ContratoServicosPrazo_DiasBaixa, H00T619_n1265ContratoServicosPrazo_DiasBaixa, H00T619_A1264ContratoServicosPrazo_DiasMedia, H00T619_n1264ContratoServicosPrazo_DiasMedia, H00T619_A1263ContratoServicosPrazo_DiasAlta, H00T619_n1263ContratoServicosPrazo_DiasAlta
               }
               , new Object[] {
               H00T620_A1335ContratoServicosPrioridade_CntSrvCod, H00T620_A1337ContratoServicosPrioridade_Nome, H00T620_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               H00T621_A70ContratadaUsuario_UsuarioPessoaCod, H00T621_n70ContratadaUsuario_UsuarioPessoaCod, H00T621_A1908Usuario_DeFerias, H00T621_n1908Usuario_DeFerias, H00T621_A1394ContratadaUsuario_UsuarioAtivo, H00T621_n1394ContratadaUsuario_UsuarioAtivo, H00T621_A66ContratadaUsuario_ContratadaCod, H00T621_A71ContratadaUsuario_UsuarioPessoaNom, H00T621_n71ContratadaUsuario_UsuarioPessoaNom, H00T621_A69ContratadaUsuario_UsuarioCod
               }
            }
         );
         AV59Pgmname = "WP_testeCombo";
         /* GeneXus formulas. */
         AV59Pgmname = "WP_testeCombo";
         context.Gx_err = 0;
         edtavUsuario_cargouonom_Enabled = 0;
         edtavContagemresultado_requisitante_Enabled = 0;
         edtavContagemresultado_datadmn_Enabled = 0;
         edtavContratoservicos_undcntnome_Enabled = 0;
         edtavContratoservicos_qntuntcns_Enabled = 0;
         edtavContagemresultado_prazoanalise_Enabled = 0;
         edtavContagemresultado_sistemagestor_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short A1823ContratoServicosPrazo_Momento ;
      private short A1265ContratoServicosPrazo_DiasBaixa ;
      private short A1264ContratoServicosPrazo_DiasMedia ;
      private short A1263ContratoServicosPrazo_DiasAlta ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV34ContratoServicosPrazo_Complexidade ;
      private short A552Contratante_EmailSdaPort ;
      private short AV27ResetCampoNumerico6 ;
      private short AV33ContratoServicosPrazo_Momento ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int AV18ContagemResultado_CntCod ;
      private int AV8ContagemResultado_ServicoGrupo ;
      private int AV45ContagemResultado_ContratadaOrigemCod ;
      private int AV9ContagemResultado_Servico ;
      private int AV50ContagemResultado_SistemaCod ;
      private int A74Contrato_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int A155Servico_Codigo ;
      private int A1336ContratoServicosPrioridade_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int AV35ContratoServicosPrioridade_Codigo ;
      private int AV37ContagemResultado_Responsavel ;
      private int gxdynajaxindex ;
      private int AV14ContagemResultado_Owner ;
      private int AV36ContagemResultado_ContratadaCod ;
      private int AV46ContagemResultado_ContadorFSCod ;
      private int AV51Modulo_Codigo ;
      private int AV52ContagemResultado_FncUsrCod ;
      private int edtavUsuario_cargouonom_Enabled ;
      private int edtavContagemresultado_requisitante_Enabled ;
      private int edtavContagemresultado_datadmn_Enabled ;
      private int edtavContratoservicos_undcntnome_Enabled ;
      private int edtavContratoservicos_qntuntcns_Enabled ;
      private int edtavContagemresultado_prazoanalise_Enabled ;
      private int edtavContagemresultado_sistemagestor_Enabled ;
      private int AV40ContagemResultado_OSVinculada ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int edtavContagemresultado_demandafm_Enabled ;
      private int A57Usuario_PessoaCod ;
      private int A1073Usuario_CargoCod ;
      private int A1075Usuario_CargoUOCod ;
      private int A1Usuario_Codigo ;
      private int edtavUsuario_cargouonom_Visible ;
      private int edtavContagemresultado_requisitante_Visible ;
      private int edtavContagemresultado_quantidadesolicitada_Visible ;
      private int edtavContagemresultado_sistemagestor_Visible ;
      private int AV62GXV1 ;
      private int AV25ContagemResultado_CntPrpCod ;
      private int AV10WWPContext_gxTpr_Areatrabalho_codigo ;
      private int AV10WWPContext_gxTpr_Contratada_codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A1013Contrato_PrepostoCod ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int A160ContratoServicos_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private int A1335ContratoServicosPrioridade_CntSrvCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int idxLst ;
      private decimal A1340ContratoServicos_QntUntCns ;
      private decimal AV30ContratoServicos_QntUntCns ;
      private decimal AV31ContagemResultado_QuantidadeSolicitada ;
      private decimal AV43ContagemResultado_PFBFSImp ;
      private decimal AV44ContagemResultado_PFLFSImp ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV59Pgmname ;
      private String A605Servico_Sigla ;
      private String A608Servico_Nome ;
      private String A2132ContratoServicos_UndCntNome ;
      private String A1337ContratoServicosPrioridade_Nome ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String AV12Usuario_CargoUONom ;
      private String AV15ContagemResultado_Requisitante ;
      private String Gxuitabspanel_tabmain_Width ;
      private String Gxuitabspanel_tabmain_Cls ;
      private String Gxuitabspanel_tabmain_Designtimetabs ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String Ucconfirmpanel_Title ;
      private String Ucconfirmpanel_Confirmationtext ;
      private String Ucconfirmpanel_Yesbuttoncaption ;
      private String Ucconfirmpanel_Confirmtype ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavContagemresultado_requisitadoporcontratante_Internalname ;
      private String AV47ContratoServicos_LocalExec ;
      private String edtavContagemresultado_demandafm_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavUsuario_cargouonom_Internalname ;
      private String edtavContagemresultado_requisitante_Internalname ;
      private String edtavContagemresultado_datadmn_Internalname ;
      private String edtavContratoservicos_undcntnome_Internalname ;
      private String edtavContratoservicos_qntuntcns_Internalname ;
      private String edtavContagemresultado_prazoanalise_Internalname ;
      private String edtavContagemresultado_sistemagestor_Internalname ;
      private String dynavContagemresultado_owner_Internalname ;
      private String edtavContagemresultado_descricao_Internalname ;
      private String cmbavContagemresultado_cntcod_Internalname ;
      private String dynavContagemresultado_servicogrupo_Internalname ;
      private String dynavContagemresultado_servico_Internalname ;
      private String AV28ContratoServicos_UndCntNome ;
      private String edtavContagemresultado_quantidadesolicitada_Internalname ;
      private String cmbavContratoservicosprazo_complexidade_Internalname ;
      private String cmbavContratoservicosprioridade_codigo_Internalname ;
      private String dynavContagemresultado_contratadacod_Internalname ;
      private String cmbavContagemresultado_responsavel_Internalname ;
      private String edtavContagemresultado_osvinculada_Internalname ;
      private String edtavContagemresultado_dmnvinculada_Internalname ;
      private String edtavContagemresultado_dmnvinculadaref_Internalname ;
      private String edtavContagemresultado_pfbfsimp_Internalname ;
      private String edtavContagemresultado_pflfsimp_Internalname ;
      private String dynavContagemresultado_contratadaorigemcod_Internalname ;
      private String dynavContagemresultado_contadorfscod_Internalname ;
      private String cmbavContratoservicos_localexec_Internalname ;
      private String edtavContagemresultado_dataentrega_Internalname ;
      private String dynavContagemresultado_sistemacod_Internalname ;
      private String dynavModulo_codigo_Internalname ;
      private String dynavContagemresultado_fncusrcod_Internalname ;
      private String AV53ContagemResultado_SistemaGestor ;
      private String edtavContagemresultado_link_Internalname ;
      private String hsh ;
      private String GXt_char1 ;
      private String GXt_char2 ;
      private String GXt_char3 ;
      private String lblContagemresultado_requisitadoporcontratante_righttext_Caption ;
      private String lblContagemresultado_requisitadoporcontratante_righttext_Internalname ;
      private String Ucconfirmpanel_Internalname ;
      private String A1076Usuario_CargoUONom ;
      private String A58Usuario_PessoaNom ;
      private String cellUsuario_cargouonom_cell_Class ;
      private String cellUsuario_cargouonom_cell_Internalname ;
      private String cellTextblockusuario_cargouonom_cell_Class ;
      private String cellTextblockusuario_cargouonom_cell_Internalname ;
      private String cellContagemresultado_requisitadoporcontratante_cell_Class ;
      private String cellContagemresultado_requisitadoporcontratante_cell_Internalname ;
      private String cellTextblockcontagemresultado_requisitadoporcontratante_cell_Class ;
      private String cellTextblockcontagemresultado_requisitadoporcontratante_cell_Internalname ;
      private String cellContagemresultado_owner_cell_Class ;
      private String cellContagemresultado_owner_cell_Internalname ;
      private String cellTextblockcontagemresultado_owner_cell_Class ;
      private String cellTextblockcontagemresultado_owner_cell_Internalname ;
      private String cellContagemresultado_requisitante_cell_Class ;
      private String cellContagemresultado_requisitante_cell_Internalname ;
      private String cellTextblockcontagemresultado_requisitante_cell_Class ;
      private String cellTextblockcontagemresultado_requisitante_cell_Internalname ;
      private String cellContagemresultado_cntcod_cell_Class ;
      private String cellContagemresultado_cntcod_cell_Internalname ;
      private String cellTextblockcontagemresultado_cntcod_cell_Class ;
      private String cellTextblockcontagemresultado_cntcod_cell_Internalname ;
      private String cellContagemresultado_quantidadesolicitada_cell_Class ;
      private String cellContagemresultado_quantidadesolicitada_cell_Internalname ;
      private String cellTextblockcontagemresultado_quantidadesolicitada_cell_Class ;
      private String cellTextblockcontagemresultado_quantidadesolicitada_cell_Internalname ;
      private String cellContratoservicosprazo_complexidade_cell_Class ;
      private String cellContratoservicosprazo_complexidade_cell_Internalname ;
      private String cellTextblockcontratoservicosprazo_complexidade_cell_Class ;
      private String cellTextblockcontratoservicosprazo_complexidade_cell_Internalname ;
      private String cellContratoservicosprioridade_codigo_cell_Class ;
      private String cellContratoservicosprioridade_codigo_cell_Internalname ;
      private String cellTextblockcontratoservicosprioridade_codigo_cell_Class ;
      private String cellTextblockcontratoservicosprioridade_codigo_cell_Internalname ;
      private String cellContagemresultado_contratadaorigemcod_cell_Class ;
      private String cellContagemresultado_contratadaorigemcod_cell_Internalname ;
      private String cellTextblockcontagemresultado_contratadaorigemcod_cell_Class ;
      private String cellTextblockcontagemresultado_contratadaorigemcod_cell_Internalname ;
      private String cellContagemresultado_contadorfscod_cell_Class ;
      private String cellContagemresultado_contadorfscod_cell_Internalname ;
      private String cellTextblockcontagemresultado_contadorfscod_cell_Class ;
      private String cellTextblockcontagemresultado_contadorfscod_cell_Internalname ;
      private String cellContagemresultado_sistemagestor_cell_Class ;
      private String cellContagemresultado_sistemagestor_cell_Internalname ;
      private String cellTextblockcontagemresultado_sistemagestor_cell_Class ;
      private String cellTextblockcontagemresultado_sistemagestor_cell_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String A77Contrato_Numero ;
      private String GXt_char7 ;
      private String GXt_char6 ;
      private String GXt_char5 ;
      private String GXt_char4 ;
      private String GXt_char8 ;
      private String GXt_char9 ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnbntartefatos_Internalname ;
      private String bttBtnbntartefatos_Jsonclick ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String tblTablerequisitos_Internalname ;
      private String tblTabledescricao_Internalname ;
      private String tblTablesolicita_Internalname ;
      private String lblTextblockcontagemresultado_demandafm_Internalname ;
      private String lblTextblockcontagemresultado_demandafm_Jsonclick ;
      private String edtavContagemresultado_demandafm_Jsonclick ;
      private String lblTextblockusuario_cargouonom_Internalname ;
      private String lblTextblockusuario_cargouonom_Jsonclick ;
      private String edtavUsuario_cargouonom_Jsonclick ;
      private String lblTextblockcontagemresultado_requisitadoporcontratante_Internalname ;
      private String lblTextblockcontagemresultado_requisitadoporcontratante_Jsonclick ;
      private String lblTextblockcontagemresultado_owner_Internalname ;
      private String lblTextblockcontagemresultado_owner_Jsonclick ;
      private String dynavContagemresultado_owner_Jsonclick ;
      private String lblTextblockcontagemresultado_requisitante_Internalname ;
      private String lblTextblockcontagemresultado_requisitante_Jsonclick ;
      private String edtavContagemresultado_requisitante_Jsonclick ;
      private String lblTextblockcontagemresultado_datadmn_Internalname ;
      private String lblTextblockcontagemresultado_datadmn_Jsonclick ;
      private String edtavContagemresultado_datadmn_Jsonclick ;
      private String lblTextblockcontagemresultado_descricao_Internalname ;
      private String lblTextblockcontagemresultado_descricao_Jsonclick ;
      private String edtavContagemresultado_descricao_Jsonclick ;
      private String lblTextblockcontagemresultado_cntcod_Internalname ;
      private String lblTextblockcontagemresultado_cntcod_Jsonclick ;
      private String cmbavContagemresultado_cntcod_Jsonclick ;
      private String lblTextblockcontagemresultado_servicogrupo_Internalname ;
      private String lblTextblockcontagemresultado_servicogrupo_Jsonclick ;
      private String dynavContagemresultado_servicogrupo_Jsonclick ;
      private String lblTextblockcontagemresultado_servico_Internalname ;
      private String lblTextblockcontagemresultado_servico_Jsonclick ;
      private String dynavContagemresultado_servico_Jsonclick ;
      private String lblTextblockcontratoservicos_undcntnome_Internalname ;
      private String lblTextblockcontratoservicos_undcntnome_Jsonclick ;
      private String edtavContratoservicos_undcntnome_Jsonclick ;
      private String lblTextblockcontratoservicos_qntuntcns_Internalname ;
      private String lblTextblockcontratoservicos_qntuntcns_Jsonclick ;
      private String edtavContratoservicos_qntuntcns_Jsonclick ;
      private String lblTextblockcontagemresultado_quantidadesolicitada_Internalname ;
      private String lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick ;
      private String edtavContagemresultado_quantidadesolicitada_Jsonclick ;
      private String lblTextblockcontratoservicosprazo_complexidade_Internalname ;
      private String lblTextblockcontratoservicosprazo_complexidade_Jsonclick ;
      private String cmbavContratoservicosprazo_complexidade_Jsonclick ;
      private String lblTextblockcontratoservicosprioridade_codigo_Internalname ;
      private String lblTextblockcontratoservicosprioridade_codigo_Jsonclick ;
      private String cmbavContratoservicosprioridade_codigo_Jsonclick ;
      private String lblTextblockcontagemresultado_contratadacod_Internalname ;
      private String lblTextblockcontagemresultado_contratadacod_Jsonclick ;
      private String dynavContagemresultado_contratadacod_Jsonclick ;
      private String lblTextblockcontagemresultado_responsavel_Internalname ;
      private String lblTextblockcontagemresultado_responsavel_Jsonclick ;
      private String cmbavContagemresultado_responsavel_Jsonclick ;
      private String lblLblvincularcom_Internalname ;
      private String lblLblvincularcom_Jsonclick ;
      private String edtavContagemresultado_osvinculada_Jsonclick ;
      private String edtavContagemresultado_dmnvinculada_Jsonclick ;
      private String edtavContagemresultado_dmnvinculadaref_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfsimp_Internalname ;
      private String lblTextblockcontagemresultado_pfbfsimp_Jsonclick ;
      private String edtavContagemresultado_pfbfsimp_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfsimp_Internalname ;
      private String lblTextblockcontagemresultado_pflfsimp_Jsonclick ;
      private String edtavContagemresultado_pflfsimp_Jsonclick ;
      private String lblTextblockcontagemresultado_contratadaorigemcod_Internalname ;
      private String lblTextblockcontagemresultado_contratadaorigemcod_Jsonclick ;
      private String dynavContagemresultado_contratadaorigemcod_Jsonclick ;
      private String lblTextblockcontagemresultado_contadorfscod_Internalname ;
      private String lblTextblockcontagemresultado_contadorfscod_Jsonclick ;
      private String dynavContagemresultado_contadorfscod_Jsonclick ;
      private String lblTextblockcontratoservicos_localexec_Internalname ;
      private String lblTextblockcontratoservicos_localexec_Jsonclick ;
      private String cmbavContratoservicos_localexec_Jsonclick ;
      private String lblTextblockcontagemresultado_prazoanalise_Internalname ;
      private String lblTextblockcontagemresultado_prazoanalise_Jsonclick ;
      private String edtavContagemresultado_prazoanalise_Jsonclick ;
      private String lblTextblockcontagemresultado_dataentrega_Internalname ;
      private String lblTextblockcontagemresultado_dataentrega_Jsonclick ;
      private String edtavContagemresultado_dataentrega_Jsonclick ;
      private String lblTextblockcontagemresultado_sistemacod_Internalname ;
      private String lblTextblockcontagemresultado_sistemacod_Jsonclick ;
      private String dynavContagemresultado_sistemacod_Jsonclick ;
      private String lblTextblockmodulo_codigo_Internalname ;
      private String lblTextblockmodulo_codigo_Jsonclick ;
      private String dynavModulo_codigo_Jsonclick ;
      private String lblTextblockcontagemresultado_fncusrcod_Internalname ;
      private String lblTextblockcontagemresultado_fncusrcod_Jsonclick ;
      private String dynavContagemresultado_fncusrcod_Jsonclick ;
      private String lblTextblockcontagemresultado_sistemagestor_Internalname ;
      private String lblTextblockcontagemresultado_sistemagestor_Jsonclick ;
      private String edtavContagemresultado_sistemagestor_Jsonclick ;
      private String lblTextblockcontagemresultado_link_Internalname ;
      private String lblTextblockcontagemresultado_link_Jsonclick ;
      private String edtavContagemresultado_link_Jsonclick ;
      private String tblTablemergedcontagemresultado_requisitadoporcontratante_Internalname ;
      private String lblContagemresultado_requisitadoporcontratante_righttext_Jsonclick ;
      private String Gxuitabspanel_tabmain_Internalname ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime AV16ContagemResultado_DataDmn ;
      private DateTime AV48ContagemResultado_PrazoAnalise ;
      private DateTime AV49ContagemResultado_DataEntrega ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A1869Contrato_DataTermino ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A638ContratoServicos_Ativo ;
      private bool A92Contrato_Ativo ;
      private bool A43Contratada_Ativo ;
      private bool A2092Servico_IsOrigemReferencia ;
      private bool A2094ContratoServicos_SolicitaGestorSistema ;
      private bool AV55Servico_IsOrigemReferencia ;
      private bool AV56Servico_IsSolicitaGestorSistema ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool A1908Usuario_DeFerias ;
      private bool Gxuitabspanel_tabmain_Autowidth ;
      private bool Gxuitabspanel_tabmain_Autoheight ;
      private bool Gxuitabspanel_tabmain_Autoscroll ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV13ContagemResultado_RequisitadoPorContratante ;
      private bool returnInSub ;
      private bool AV23IsContratanteSemEmailSda ;
      private bool n29Contratante_Codigo ;
      private bool A593Contratante_OSAutomatica ;
      private bool A2085Contratante_RequerOrigem ;
      private bool n2085Contratante_RequerOrigem ;
      private bool A1822Contratante_UsaOSistema ;
      private bool n1822Contratante_UsaOSistema ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n552Contratante_EmailSdaPort ;
      private bool n547Contratante_EmailSdaHost ;
      private bool AV20Contratante_OSAutomatica ;
      private bool AV21Contratante_RequerOrigem ;
      private bool AV22Contratante_UsaOSistema ;
      private bool n1073Usuario_CargoCod ;
      private bool n1075Usuario_CargoUOCod ;
      private bool n1076Usuario_CargoUONom ;
      private bool n58Usuario_PessoaNom ;
      private bool AV19CheckRequiredFieldsResult ;
      private bool AV26Contrato_IsVencido ;
      private bool AV10WWPContext_gxTpr_Userehcontratante ;
      private bool AV10WWPContext_gxTpr_Userehcontratada ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n74Contrato_Codigo ;
      private bool n843Contrato_DataFimTA ;
      private bool n41Contratada_PessoaNom ;
      private bool A54Usuario_Ativo ;
      private bool n54Usuario_Ativo ;
      private bool n2132ContratoServicos_UndCntNome ;
      private bool n1340ContratoServicos_QntUntCns ;
      private bool n1823ContratoServicosPrazo_Momento ;
      private bool n1265ContratoServicosPrazo_DiasBaixa ;
      private bool n1264ContratoServicosPrazo_DiasMedia ;
      private bool n1263ContratoServicosPrazo_DiasAlta ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n1908Usuario_DeFerias ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private String AV54ContagemResultado_Link ;
      private String AV11ContagemResultado_DemandaFM ;
      private String AV17ContagemResultado_Descricao ;
      private String AV41ContagemResultado_DmnVinculada ;
      private String AV42ContagemResultado_DmnVinculadaRef ;
      private String A6AreaTrabalho_Descricao ;
      private String A548Contratante_EmailSdaUser ;
      private String A547Contratante_EmailSdaHost ;
      private String A2096Contrato_Identificacao ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavContagemresultado_requisitadoporcontratante ;
      private GXCombobox dynavContagemresultado_owner ;
      private GXCombobox cmbavContagemresultado_cntcod ;
      private GXCombobox dynavContagemresultado_servicogrupo ;
      private GXCombobox dynavContagemresultado_servico ;
      private GXCombobox cmbavContratoservicosprazo_complexidade ;
      private GXCombobox cmbavContratoservicosprioridade_codigo ;
      private GXCombobox dynavContagemresultado_contratadacod ;
      private GXCombobox cmbavContagemresultado_responsavel ;
      private GXCombobox dynavContagemresultado_contratadaorigemcod ;
      private GXCombobox dynavContagemresultado_contadorfscod ;
      private GXCombobox cmbavContratoservicos_localexec ;
      private GXCombobox dynavContagemresultado_sistemacod ;
      private GXCombobox dynavModulo_codigo ;
      private GXCombobox dynavContagemresultado_fncusrcod ;
      private IDataStoreProvider pr_default ;
      private int[] H00T63_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00T63_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00T63_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00T63_A60ContratanteUsuario_UsuarioCod ;
      private String[] H00T63_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00T63_n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00T63_A54Usuario_Ativo ;
      private bool[] H00T63_n54Usuario_Ativo ;
      private int[] H00T63_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] H00T63_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private int[] H00T64_A155Servico_Codigo ;
      private int[] H00T64_A160ContratoServicos_Codigo ;
      private int[] H00T64_A157ServicoGrupo_Codigo ;
      private String[] H00T64_A158ServicoGrupo_Descricao ;
      private int[] H00T64_A74Contrato_Codigo ;
      private bool[] H00T64_n74Contrato_Codigo ;
      private int[] H00T64_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00T64_A92Contrato_Ativo ;
      private bool[] H00T64_A638ContratoServicos_Ativo ;
      private int[] H00T65_A39Contratada_Codigo ;
      private int[] H00T65_A74Contrato_Codigo ;
      private bool[] H00T65_n74Contrato_Codigo ;
      private int[] H00T65_A160ContratoServicos_Codigo ;
      private int[] H00T65_A155Servico_Codigo ;
      private String[] H00T65_A608Servico_Nome ;
      private int[] H00T65_A157ServicoGrupo_Codigo ;
      private bool[] H00T65_A638ContratoServicos_Ativo ;
      private bool[] H00T65_A92Contrato_Ativo ;
      private bool[] H00T65_A43Contratada_Ativo ;
      private int[] H00T66_A40Contratada_PessoaCod ;
      private int[] H00T66_A160ContratoServicos_Codigo ;
      private int[] H00T66_A39Contratada_Codigo ;
      private String[] H00T66_A41Contratada_PessoaNom ;
      private bool[] H00T66_n41Contratada_PessoaNom ;
      private int[] H00T66_A74Contrato_Codigo ;
      private bool[] H00T66_n74Contrato_Codigo ;
      private bool[] H00T66_A638ContratoServicos_Ativo ;
      private int[] H00T67_A40Contratada_PessoaCod ;
      private int[] H00T67_A39Contratada_Codigo ;
      private String[] H00T67_A41Contratada_PessoaNom ;
      private bool[] H00T67_n41Contratada_PessoaNom ;
      private int[] H00T67_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00T67_A43Contratada_Ativo ;
      private int[] H00T68_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00T68_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00T68_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00T68_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00T68_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00T68_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00T69_A160ContratoServicos_Codigo ;
      private int[] H00T69_A1063ContratoServicosSistemas_SistemaCod ;
      private String[] H00T69_A1064ContratoServicosSistemas_SistemaSigla ;
      private bool[] H00T69_n1064ContratoServicosSistemas_SistemaSigla ;
      private int[] H00T69_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] H00T610_A146Modulo_Codigo ;
      private String[] H00T610_A145Modulo_Sigla ;
      private int[] H00T610_A127Sistema_Codigo ;
      private int[] H00T611_A161FuncaoUsuario_Codigo ;
      private String[] H00T611_A162FuncaoUsuario_Nome ;
      private int[] H00T611_A127Sistema_Codigo ;
      private bool[] H00T611_A164FuncaoUsuario_Ativo ;
      private int[] H00T612_A29Contratante_Codigo ;
      private bool[] H00T612_n29Contratante_Codigo ;
      private int[] H00T612_A5AreaTrabalho_Codigo ;
      private bool[] H00T612_A593Contratante_OSAutomatica ;
      private bool[] H00T612_A2085Contratante_RequerOrigem ;
      private bool[] H00T612_n2085Contratante_RequerOrigem ;
      private bool[] H00T612_A1822Contratante_UsaOSistema ;
      private bool[] H00T612_n1822Contratante_UsaOSistema ;
      private String[] H00T612_A6AreaTrabalho_Descricao ;
      private String[] H00T612_A548Contratante_EmailSdaUser ;
      private bool[] H00T612_n548Contratante_EmailSdaUser ;
      private short[] H00T612_A552Contratante_EmailSdaPort ;
      private bool[] H00T612_n552Contratante_EmailSdaPort ;
      private String[] H00T612_A547Contratante_EmailSdaHost ;
      private bool[] H00T612_n547Contratante_EmailSdaHost ;
      private int[] H00T613_A57Usuario_PessoaCod ;
      private int[] H00T613_A1073Usuario_CargoCod ;
      private bool[] H00T613_n1073Usuario_CargoCod ;
      private int[] H00T613_A1075Usuario_CargoUOCod ;
      private bool[] H00T613_n1075Usuario_CargoUOCod ;
      private int[] H00T613_A1Usuario_Codigo ;
      private String[] H00T613_A1076Usuario_CargoUONom ;
      private bool[] H00T613_n1076Usuario_CargoUONom ;
      private String[] H00T613_A58Usuario_PessoaNom ;
      private bool[] H00T613_n58Usuario_PessoaNom ;
      private int[] H00T616_A40Contratada_PessoaCod ;
      private int[] H00T616_A39Contratada_Codigo ;
      private int[] H00T616_A75Contrato_AreaTrabalhoCod ;
      private int[] H00T616_A1013Contrato_PrepostoCod ;
      private bool[] H00T616_n1013Contrato_PrepostoCod ;
      private int[] H00T616_A74Contrato_Codigo ;
      private bool[] H00T616_n74Contrato_Codigo ;
      private DateTime[] H00T616_A843Contrato_DataFimTA ;
      private bool[] H00T616_n843Contrato_DataFimTA ;
      private DateTime[] H00T616_A83Contrato_DataVigenciaTermino ;
      private String[] H00T616_A41Contratada_PessoaNom ;
      private bool[] H00T616_n41Contratada_PessoaNom ;
      private bool[] H00T616_A92Contrato_Ativo ;
      private String[] H00T616_A77Contrato_Numero ;
      private int[] H00T617_A160ContratoServicos_Codigo ;
      private int[] H00T617_A39Contratada_Codigo ;
      private bool[] H00T617_A43Contratada_Ativo ;
      private bool[] H00T617_A92Contrato_Ativo ;
      private bool[] H00T617_A638ContratoServicos_Ativo ;
      private int[] H00T617_A157ServicoGrupo_Codigo ;
      private int[] H00T617_A74Contrato_Codigo ;
      private bool[] H00T617_n74Contrato_Codigo ;
      private String[] H00T617_A608Servico_Nome ;
      private String[] H00T617_A605Servico_Sigla ;
      private int[] H00T617_A155Servico_Codigo ;
      private int[] H00T618_A39Contratada_Codigo ;
      private int[] H00T618_A1013Contrato_PrepostoCod ;
      private bool[] H00T618_n1013Contrato_PrepostoCod ;
      private int[] H00T618_A1212ContratoServicos_UnidadeContratada ;
      private int[] H00T618_A160ContratoServicos_Codigo ;
      private int[] H00T618_A74Contrato_Codigo ;
      private bool[] H00T618_n74Contrato_Codigo ;
      private bool[] H00T618_A638ContratoServicos_Ativo ;
      private int[] H00T618_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00T618_A92Contrato_Ativo ;
      private int[] H00T618_A52Contratada_AreaTrabalhoCod ;
      private int[] H00T618_A157ServicoGrupo_Codigo ;
      private bool[] H00T618_A43Contratada_Ativo ;
      private bool[] H00T618_A54Usuario_Ativo ;
      private bool[] H00T618_n54Usuario_Ativo ;
      private int[] H00T618_A155Servico_Codigo ;
      private String[] H00T618_A2132ContratoServicos_UndCntNome ;
      private bool[] H00T618_n2132ContratoServicos_UndCntNome ;
      private decimal[] H00T618_A1340ContratoServicos_QntUntCns ;
      private bool[] H00T618_n1340ContratoServicos_QntUntCns ;
      private bool[] H00T618_A2092Servico_IsOrigemReferencia ;
      private bool[] H00T618_A2094ContratoServicos_SolicitaGestorSistema ;
      private int[] H00T619_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] H00T619_A1823ContratoServicosPrazo_Momento ;
      private bool[] H00T619_n1823ContratoServicosPrazo_Momento ;
      private short[] H00T619_A1265ContratoServicosPrazo_DiasBaixa ;
      private bool[] H00T619_n1265ContratoServicosPrazo_DiasBaixa ;
      private short[] H00T619_A1264ContratoServicosPrazo_DiasMedia ;
      private bool[] H00T619_n1264ContratoServicosPrazo_DiasMedia ;
      private short[] H00T619_A1263ContratoServicosPrazo_DiasAlta ;
      private bool[] H00T619_n1263ContratoServicosPrazo_DiasAlta ;
      private int[] H00T620_A1335ContratoServicosPrioridade_CntSrvCod ;
      private String[] H00T620_A1337ContratoServicosPrioridade_Nome ;
      private int[] H00T620_A1336ContratoServicosPrioridade_Codigo ;
      private int[] H00T621_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00T621_n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00T621_A1908Usuario_DeFerias ;
      private bool[] H00T621_n1908Usuario_DeFerias ;
      private bool[] H00T621_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00T621_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H00T621_A66ContratadaUsuario_ContratadaCod ;
      private String[] H00T621_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00T621_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00T621_A69ContratadaUsuario_UsuarioCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV6Messages ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV10WWPContext ;
      private wwpbaseobjects.SdtWWPContext AV7Context ;
      private SdtMessages_Message AV5Message ;
   }

   public class wp_testecombo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00T616( IGxContext context ,
                                              bool AV10WWPContext_gxTpr_Userehcontratante ,
                                              bool AV10WWPContext_gxTpr_Userehcontratada ,
                                              int A75Contrato_AreaTrabalhoCod ,
                                              int AV10WWPContext_gxTpr_Areatrabalho_codigo ,
                                              int A39Contratada_Codigo ,
                                              int AV10WWPContext_gxTpr_Contratada_codigo ,
                                              bool A92Contrato_Ativo ,
                                              DateTime A843Contrato_DataFimTA ,
                                              DateTime AV16ContagemResultado_DataDmn ,
                                              DateTime A83Contrato_DataVigenciaTermino )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int10 ;
         GXv_int10 = new short [4] ;
         Object[] GXv_Object11 ;
         GXv_Object11 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T1.[Contrato_AreaTrabalhoCod], T1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T1.[Contrato_Codigo], COALESCE( T4.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T1.[Contrato_DataVigenciaTermino], T3.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_Ativo], T1.[Contrato_Numero] FROM ((([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN (SELECT T5.[ContratoTermoAditivo_DataFim], T5.[Contrato_Codigo], T5.[ContratoTermoAditivo_Codigo], T6.[GXC3] AS GXC3 FROM ([ContratoTermoAditivo] T5 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC3, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T6 ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) WHERE T5.[ContratoTermoAditivo_Codigo] = T6.[GXC3] ) T4 ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (COALESCE( T4.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV16ContagemResultado_DataDmn or T1.[Contrato_DataVigenciaTermino] >= @AV16ContagemResultado_DataDmn)";
         if ( AV10WWPContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_AreaTrabalhoCod] = @AV10WWPC_1Areatrabalho_codigo)";
         }
         else
         {
            GXv_int10[2] = 1;
         }
         if ( AV10WWPContext_gxTpr_Userehcontratada )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV10WWPC_2Contratada_codigo)";
         }
         else
         {
            GXv_int10[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Codigo]";
         GXv_Object11[0] = scmdbuf;
         GXv_Object11[1] = GXv_int10;
         return GXv_Object11 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 11 :
                     return conditional_H00T616(context, (bool)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (bool)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00T63 ;
          prmH00T63 = new Object[] {
          new Object[] {"@AV10WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T64 ;
          prmH00T64 = new Object[] {
          new Object[] {"@AV18ContagemResultado_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T65 ;
          prmH00T65 = new Object[] {
          new Object[] {"@AV8ContagemResultado_ServicoGrupo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T66 ;
          prmH00T66 = new Object[] {
          new Object[] {"@AV18ContagemResultado_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T67 ;
          prmH00T67 = new Object[] {
          new Object[] {"@AV10WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T68 ;
          prmH00T68 = new Object[] {
          new Object[] {"@AV45ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T69 ;
          prmH00T69 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T610 ;
          prmH00T610 = new Object[] {
          new Object[] {"@AV50ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T611 ;
          prmH00T611 = new Object[] {
          new Object[] {"@AV50ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T612 ;
          prmH00T612 = new Object[] {
          new Object[] {"@AV10WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T613 ;
          prmH00T613 = new Object[] {
          new Object[] {"@AV10WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00T617 ;
          prmH00T617 = new Object[] {
          new Object[] {"@AV18ContagemResultado_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContagemResultado_ServicoGrupo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T618 ;
          prmH00T618 = new Object[] {
          new Object[] {"@AV18ContagemResultado_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T619 ;
          prmH00T619 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T620 ;
          prmH00T620 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T621 ;
          prmH00T621 = new Object[] {
          new Object[] {"@AV36ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T616 ;
          prmH00T616 = new Object[] {
          new Object[] {"@AV16ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV16ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10WWPC_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00T63", "SELECT T3.[Pessoa_Codigo] AS ContratanteUsuario_UsuarioPess, T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T2.[Usuario_Ativo], COALESCE( T4.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T5.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [AreaTrabalho] T5 WITH (NOLOCK),  [ContratanteUsuario] T6 WITH (NOLOCK) WHERE T5.[Contratante_Codigo] = T6.[ContratanteUsuario_ContratanteCod] GROUP BY T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] ) T4 ON T4.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T4.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T2.[Usuario_Ativo] = 1) AND (COALESCE( T4.[ContratanteUsuario_AreaTrabalhoCod], 0) = @AV10WWPC_1Areatrabalho_codigo) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T63,0,0,true,false )
             ,new CursorDef("H00T64", "SELECT T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T2.[ServicoGrupo_Codigo], T3.[ServicoGrupo_Descricao], T1.[Contrato_Codigo], T4.[Contrato_AreaTrabalhoCod], T4.[Contrato_Ativo], T1.[ContratoServicos_Ativo] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T3 WITH (NOLOCK) ON T3.[ServicoGrupo_Codigo] = T2.[ServicoGrupo_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T4.[Contrato_Ativo] = 1) AND (T1.[ContratoServicos_Ativo] = 1) AND (T1.[Contrato_Codigo] = @AV18ContagemResultado_CntCod) AND (T4.[Contrato_AreaTrabalhoCod] = @AV10WWPC_1Areatrabalho_codigo) ORDER BY T3.[ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T64,0,0,true,false )
             ,new CursorDef("H00T65", "SELECT T4.[Contratada_Codigo], T3.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_Codigo], T2.[Servico_Nome], T2.[ServicoGrupo_Codigo], T1.[ContratoServicos_Ativo], T3.[Contrato_Ativo], T4.[Contratada_Ativo] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T3.[Contrato_Ativo] = 1) AND (T4.[Contratada_Ativo] = 1) AND (T2.[ServicoGrupo_Codigo] = @AV8ContagemResultado_ServicoGrupo) ORDER BY T2.[Servico_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T65,0,0,true,false )
             ,new CursorDef("H00T66", "SELECT T4.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[ContratoServicos_Codigo], T2.[Contratada_Codigo], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_Codigo], T1.[ContratoServicos_Ativo] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T1.[Contrato_Codigo] = @AV18ContagemResultado_CntCod) ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T66,0,0,true,false )
             ,new CursorDef("H00T67", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod], T1.[Contratada_Ativo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE (T1.[Contratada_Ativo] = 1) AND (T1.[Contratada_AreaTrabalhoCod] = @AV10WWPC_1Areatrabalho_codigo) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T67,0,0,true,false )
             ,new CursorDef("H00T68", "SELECT T3.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContratadaUsuario_ContratadaCod] = @AV45ContagemResultado_ContratadaOrigemCod ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T68,0,0,true,false )
             ,new CursorDef("H00T69", "SELECT T1.[ContratoServicos_Codigo], T1.[ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod, T2.[Sistema_Sigla] AS ContratoServicosSistemas_SistemaSigla, T1.[ContratoServicosSistemas_ServicoCod] FROM ([ContratoServicosSistemas] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContratoServicosSistemas_SistemaCod]) WHERE T1.[ContratoServicosSistemas_ServicoCod] = @AV9ContagemResultado_Servico ORDER BY T2.[Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T69,0,0,true,false )
             ,new CursorDef("H00T610", "SELECT [Modulo_Codigo], [Modulo_Sigla], [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV50ContagemResultado_SistemaCod ORDER BY [Modulo_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T610,0,0,true,false )
             ,new CursorDef("H00T611", "SELECT [FuncaoUsuario_Codigo], [FuncaoUsuario_Nome], [Sistema_Codigo], [FuncaoUsuario_Ativo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE ([FuncaoUsuario_Ativo] = 1) AND ([Sistema_Codigo] = @AV50ContagemResultado_SistemaCod) ORDER BY [FuncaoUsuario_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T611,0,0,true,false )
             ,new CursorDef("H00T612", "SELECT T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_OSAutomatica], T2.[Contratante_RequerOrigem], T2.[Contratante_UsaOSistema], T1.[AreaTrabalho_Descricao], T2.[Contratante_EmailSdaUser], T2.[Contratante_EmailSdaPort], T2.[Contratante_EmailSdaHost] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AV10WWPC_1Areatrabalho_codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T612,1,0,false,true )
             ,new CursorDef("H00T613", "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_CargoCod] AS Usuario_CargoCod, T3.[Cargo_UOCod] AS Usuario_CargoUOCod, T1.[Usuario_Codigo], T4.[UnidadeOrganizacional_Nome] AS Usuario_CargoUONom, T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ((([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) LEFT JOIN [Geral_Cargo] T3 WITH (NOLOCK) ON T3.[Cargo_Codigo] = T1.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T4 WITH (NOLOCK) ON T4.[UnidadeOrganizacional_Codigo] = T3.[Cargo_UOCod]) WHERE T1.[Usuario_Codigo] = @AV10WWPContext__Userid ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T613,1,0,false,true )
             ,new CursorDef("H00T616", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T616,100,0,false,false )
             ,new CursorDef("H00T617", "SELECT T1.[ContratoServicos_Codigo], T2.[Contratada_Codigo], T3.[Contratada_Ativo], T2.[Contrato_Ativo], T1.[ContratoServicos_Ativo], T4.[ServicoGrupo_Codigo], T1.[Contrato_Codigo], T4.[Servico_Nome], T4.[Servico_Sigla], T1.[Servico_Codigo] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE (T1.[Contrato_Codigo] = @AV18ContagemResultado_CntCod) AND (T1.[ContratoServicos_Ativo] = 1) AND (T2.[Contrato_Ativo] = 1) AND (T3.[Contratada_Ativo] = 1) AND (T4.[ServicoGrupo_Codigo] = @AV8ContagemResultado_ServicoGrupo) ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T617,100,0,true,false )
             ,new CursorDef("H00T618", "SELECT TOP 1 T3.[Contratada_Codigo], T3.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T1.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, T1.[ContratoServicos_Codigo], T1.[Contrato_Codigo], T1.[ContratoServicos_Ativo], T3.[Contrato_AreaTrabalhoCod], T3.[Contrato_Ativo], T4.[Contratada_AreaTrabalhoCod], T6.[ServicoGrupo_Codigo], T4.[Contratada_Ativo], T5.[Usuario_Ativo], T1.[Servico_Codigo], T2.[UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, T1.[ContratoServicos_QntUntCns], T6.[Servico_IsOrigemReferencia], T1.[ContratoServicos_SolicitaGestorSistema] FROM ((((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoServicos_UnidadeContratada]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T3.[Contrato_PrepostoCod]) INNER JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE (T1.[Contrato_Codigo] = @AV18ContagemResultado_CntCod) AND (T1.[ContratoServicos_Ativo] = 1) AND (T1.[Servico_Codigo] = @AV9ContagemResultado_Servico) ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T618,1,0,true,true )
             ,new CursorDef("H00T619", "SELECT [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazo_Momento], [ContratoServicosPrazo_DiasBaixa], [ContratoServicosPrazo_DiasMedia], [ContratoServicosPrazo_DiasAlta] FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T619,1,0,true,true )
             ,new CursorDef("H00T620", "SELECT [ContratoServicosPrioridade_CntSrvCod], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Codigo] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosPrioridade_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T620,100,0,true,false )
             ,new CursorDef("H00T621", "SELECT T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso, T2.[Usuario_DeFerias], T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_ContratadaCod], T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV36ContagemResultado_ContratadaCod) AND (T2.[Usuario_Ativo] = 1) AND (Not T2.[Usuario_DeFerias] = 1) ORDER BY T1.[ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T621,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                ((bool[]) buf[7])[0] = rslt.getBool(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                ((bool[]) buf[7])[0] = rslt.getBool(8) ;
                ((bool[]) buf[8])[0] = rslt.getBool(9) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((bool[]) buf[11])[0] = rslt.getBool(9) ;
                ((String[]) buf[12])[0] = rslt.getString(10, 20) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((String[]) buf[7])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[8])[0] = rslt.getString(9, 15) ;
                ((int[]) buf[9])[0] = rslt.getInt(10) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((bool[]) buf[8])[0] = rslt.getBool(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((bool[]) buf[11])[0] = rslt.getBool(11) ;
                ((bool[]) buf[12])[0] = rslt.getBool(12) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(12);
                ((int[]) buf[14])[0] = rslt.getInt(13) ;
                ((String[]) buf[15])[0] = rslt.getString(14, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(14);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(15);
                ((bool[]) buf[19])[0] = rslt.getBool(16) ;
                ((bool[]) buf[20])[0] = rslt.getBool(17) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 11 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
