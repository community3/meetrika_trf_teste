/*
               File: ValidatePurchase
        Description: Validate Purchase
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:57.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class validatepurchase : GXProcedure
   {
      public validatepurchase( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public validatepurchase( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_TransactionData ,
                           out SdtPurchaseReceiptInformation aP1_PurchaseRecipeInformation )
      {
         this.AV8TransactionData = aP0_TransactionData;
         this.AV9PurchaseRecipeInformation = new SdtPurchaseReceiptInformation(context) ;
         initialize();
         executePrivate();
         aP1_PurchaseRecipeInformation=this.AV9PurchaseRecipeInformation;
      }

      public SdtPurchaseReceiptInformation executeUdp( String aP0_TransactionData )
      {
         this.AV8TransactionData = aP0_TransactionData;
         this.AV9PurchaseRecipeInformation = new SdtPurchaseReceiptInformation(context) ;
         initialize();
         executePrivate();
         aP1_PurchaseRecipeInformation=this.AV9PurchaseRecipeInformation;
         return AV9PurchaseRecipeInformation ;
      }

      public void executeSubmit( String aP0_TransactionData ,
                                 out SdtPurchaseReceiptInformation aP1_PurchaseRecipeInformation )
      {
         validatepurchase objvalidatepurchase;
         objvalidatepurchase = new validatepurchase();
         objvalidatepurchase.AV8TransactionData = aP0_TransactionData;
         objvalidatepurchase.AV9PurchaseRecipeInformation = new SdtPurchaseReceiptInformation(context) ;
         objvalidatepurchase.context.SetSubmitInitialConfig(context);
         objvalidatepurchase.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objvalidatepurchase);
         aP1_PurchaseRecipeInformation=this.AV9PurchaseRecipeInformation;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((validatepurchase)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV8TransactionData ;
      private SdtPurchaseReceiptInformation aP1_PurchaseRecipeInformation ;
      private SdtPurchaseReceiptInformation AV9PurchaseRecipeInformation ;
   }

}
