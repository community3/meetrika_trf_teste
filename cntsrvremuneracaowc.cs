/*
               File: CntSrvRemuneracaoWC
        Description: Cnt Srv Remuneracao WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:49:14.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class cntsrvremuneracaowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public cntsrvremuneracaowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public cntsrvremuneracaowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo )
      {
         this.AV7ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContratoServicos_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV17TFContratoServicosRmn_Inicio = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContratoServicosRmn_Inicio", StringUtil.LTrim( StringUtil.Str( AV17TFContratoServicosRmn_Inicio, 14, 5)));
                  AV18TFContratoServicosRmn_Inicio_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosRmn_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV18TFContratoServicosRmn_Inicio_To, 14, 5)));
                  AV21TFContratoServicosRmn_Fim = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosRmn_Fim", StringUtil.LTrim( StringUtil.Str( AV21TFContratoServicosRmn_Fim, 14, 5)));
                  AV22TFContratoServicosRmn_Fim_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosRmn_Fim_To", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosRmn_Fim_To, 14, 5)));
                  AV25TFContratoServicosRmn_Valor = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosRmn_Valor", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosRmn_Valor, 14, 5)));
                  AV26TFContratoServicosRmn_Valor_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosRmn_Valor_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosRmn_Valor_To, 14, 5)));
                  AV7ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
                  AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace", AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace);
                  AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace", AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace);
                  AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace", AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace);
                  AV34Pgmname = GetNextPar( );
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosRmn_Inicio, AV18TFContratoServicosRmn_Inicio_To, AV21TFContratoServicosRmn_Fim, AV22TFContratoServicosRmn_Fim_To, AV25TFContratoServicosRmn_Valor, AV26TFContratoServicosRmn_Valor_To, AV7ContratoServicos_Codigo, AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace, AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace, AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace, AV34Pgmname, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PARY2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV34Pgmname = "CntSrvRemuneracaoWC";
               context.Gx_err = 0;
               WSRY2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Cnt Srv Remuneracao WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822491457");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("cntsrvremuneracaowc.aspx") + "?" + UrlEncode("" +AV7ContratoServicos_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_INICIO", StringUtil.LTrim( StringUtil.NToC( AV17TFContratoServicosRmn_Inicio, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_INICIO_TO", StringUtil.LTrim( StringUtil.NToC( AV18TFContratoServicosRmn_Inicio_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_FIM", StringUtil.LTrim( StringUtil.NToC( AV21TFContratoServicosRmn_Fim, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_FIM_TO", StringUtil.LTrim( StringUtil.NToC( AV22TFContratoServicosRmn_Fim_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_VALOR", StringUtil.LTrim( StringUtil.NToC( AV25TFContratoServicosRmn_Valor, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_VALOR_TO", StringUtil.LTrim( StringUtil.NToC( AV26TFContratoServicosRmn_Valor_To, 14, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV28DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV28DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSRMN_INICIOTITLEFILTERDATA", AV16ContratoServicosRmn_InicioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSRMN_INICIOTITLEFILTERDATA", AV16ContratoServicosRmn_InicioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSRMN_FIMTITLEFILTERDATA", AV20ContratoServicosRmn_FimTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSRMN_FIMTITLEFILTERDATA", AV20ContratoServicosRmn_FimTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSRMN_VALORTITLEFILTERDATA", AV24ContratoServicosRmn_ValorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSRMN_VALORTITLEFILTERDATA", AV24ContratoServicosRmn_ValorTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV34Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Caption", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Cls", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosrmn_inicio_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosrmn_inicio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosrmn_inicio_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosrmn_inicio_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosrmn_inicio_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosrmn_inicio_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Caption", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Tooltip", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Cls", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosrmn_fim_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosrmn_fim_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosrmn_fim_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filtertype", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosrmn_fim_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosrmn_fim_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosrmn_fim_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Sortasc", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Caption", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Tooltip", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Cls", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosrmn_valor_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosrmn_valor_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosrmn_valor_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filtertype", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosrmn_valor_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosrmn_valor_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosrmn_valor_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Sortasc", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosrmn_inicio_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosrmn_fim_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosrmn_valor_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormRY2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("cntsrvremuneracaowc.js", "?202042822491578");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "CntSrvRemuneracaoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Cnt Srv Remuneracao WC" ;
      }

      protected void WBRY0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "cntsrvremuneracaowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_RY2( true) ;
         }
         else
         {
            wb_table1_2_RY2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_RY2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicos_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_CntSrvRemuneracaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,18);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CntSrvRemuneracaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_CntSrvRemuneracaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosrmn_inicio_Internalname, StringUtil.LTrim( StringUtil.NToC( AV17TFContratoServicosRmn_Inicio, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV17TFContratoServicosRmn_Inicio, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,20);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosrmn_inicio_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosrmn_inicio_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CntSrvRemuneracaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosrmn_inicio_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV18TFContratoServicosRmn_Inicio_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV18TFContratoServicosRmn_Inicio_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,21);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosrmn_inicio_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosrmn_inicio_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CntSrvRemuneracaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosrmn_fim_Internalname, StringUtil.LTrim( StringUtil.NToC( AV21TFContratoServicosRmn_Fim, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV21TFContratoServicosRmn_Fim, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,22);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosrmn_fim_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosrmn_fim_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CntSrvRemuneracaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosrmn_fim_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV22TFContratoServicosRmn_Fim_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV22TFContratoServicosRmn_Fim_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosrmn_fim_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosrmn_fim_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CntSrvRemuneracaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosrmn_valor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV25TFContratoServicosRmn_Valor, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV25TFContratoServicosRmn_Valor, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,24);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosrmn_valor_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosrmn_valor_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CntSrvRemuneracaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosrmn_valor_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV26TFContratoServicosRmn_Valor_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV26TFContratoServicosRmn_Valor_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,25);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosrmn_valor_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosrmn_valor_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CntSrvRemuneracaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosrmn_iniciotitlecontrolidtoreplace_Internalname, AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", 0, edtavDdo_contratoservicosrmn_iniciotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_CntSrvRemuneracaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSRMN_FIMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosrmn_fimtitlecontrolidtoreplace_Internalname, AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", 0, edtavDdo_contratoservicosrmn_fimtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_CntSrvRemuneracaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSRMN_VALORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosrmn_valortitlecontrolidtoreplace_Internalname, AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", 0, edtavDdo_contratoservicosrmn_valortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_CntSrvRemuneracaoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTRY2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Cnt Srv Remuneracao WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPRY0( ) ;
            }
         }
      }

      protected void WSRY2( )
      {
         STARTRY2( ) ;
         EVTRY2( ) ;
      }

      protected void EVTRY2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11RY2 */
                                    E11RY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSRMN_INICIO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12RY2 */
                                    E12RY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSRMN_FIM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13RY2 */
                                    E13RY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSRMN_VALOR.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14RY2 */
                                    E14RY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRY0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              A2069ContratoServicosRmn_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Sequencial_Internalname), ",", "."));
                              A2070ContratoServicosRmn_Inicio = context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Inicio_Internalname), ",", ".");
                              A2071ContratoServicosRmn_Fim = context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Fim_Internalname), ",", ".");
                              A2072ContratoServicosRmn_Valor = context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Valor_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15RY2 */
                                          E15RY2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16RY2 */
                                          E16RY2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17RY2 */
                                          E17RY2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosrmn_inicio Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_INICIO"), ",", ".") != AV17TFContratoServicosRmn_Inicio )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosrmn_inicio_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_INICIO_TO"), ",", ".") != AV18TFContratoServicosRmn_Inicio_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosrmn_fim Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_FIM"), ",", ".") != AV21TFContratoServicosRmn_Fim )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosrmn_fim_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_FIM_TO"), ",", ".") != AV22TFContratoServicosRmn_Fim_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosrmn_valor Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_VALOR"), ",", ".") != AV25TFContratoServicosRmn_Valor )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosrmn_valor_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_VALOR_TO"), ",", ".") != AV26TFContratoServicosRmn_Valor_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPRY0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WERY2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormRY2( ) ;
            }
         }
      }

      protected void PARY2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       decimal AV17TFContratoServicosRmn_Inicio ,
                                       decimal AV18TFContratoServicosRmn_Inicio_To ,
                                       decimal AV21TFContratoServicosRmn_Fim ,
                                       decimal AV22TFContratoServicosRmn_Fim_To ,
                                       decimal AV25TFContratoServicosRmn_Valor ,
                                       decimal AV26TFContratoServicosRmn_Valor_To ,
                                       int AV7ContratoServicos_Codigo ,
                                       String AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace ,
                                       String AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace ,
                                       String AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace ,
                                       String AV34Pgmname ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFRY2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSRMN_SEQUENCIAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2069ContratoServicosRmn_Sequencial), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSRMN_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSRMN_INICIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2070ContratoServicosRmn_Inicio, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSRMN_INICIO", StringUtil.LTrim( StringUtil.NToC( A2070ContratoServicosRmn_Inicio, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSRMN_FIM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2071ContratoServicosRmn_Fim, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSRMN_FIM", StringUtil.LTrim( StringUtil.NToC( A2071ContratoServicosRmn_Fim, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSRMN_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2072ContratoServicosRmn_Valor, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSRMN_VALOR", StringUtil.LTrim( StringUtil.NToC( A2072ContratoServicosRmn_Valor, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFRY2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV34Pgmname = "CntSrvRemuneracaoWC";
         context.Gx_err = 0;
      }

      protected void RFRY2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E16RY2 */
         E16RY2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV17TFContratoServicosRmn_Inicio ,
                                                 AV18TFContratoServicosRmn_Inicio_To ,
                                                 AV21TFContratoServicosRmn_Fim ,
                                                 AV22TFContratoServicosRmn_Fim_To ,
                                                 AV25TFContratoServicosRmn_Valor ,
                                                 AV26TFContratoServicosRmn_Valor_To ,
                                                 A2070ContratoServicosRmn_Inicio ,
                                                 A2071ContratoServicosRmn_Fim ,
                                                 A2072ContratoServicosRmn_Valor ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A160ContratoServicos_Codigo ,
                                                 AV7ContratoServicos_Codigo },
                                                 new int[] {
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00RY2 */
            pr_default.execute(0, new Object[] {AV7ContratoServicos_Codigo, AV17TFContratoServicosRmn_Inicio, AV18TFContratoServicosRmn_Inicio_To, AV21TFContratoServicosRmn_Fim, AV22TFContratoServicosRmn_Fim_To, AV25TFContratoServicosRmn_Valor, AV26TFContratoServicosRmn_Valor_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A160ContratoServicos_Codigo = H00RY2_A160ContratoServicos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               A2072ContratoServicosRmn_Valor = H00RY2_A2072ContratoServicosRmn_Valor[0];
               A2071ContratoServicosRmn_Fim = H00RY2_A2071ContratoServicosRmn_Fim[0];
               A2070ContratoServicosRmn_Inicio = H00RY2_A2070ContratoServicosRmn_Inicio[0];
               A2069ContratoServicosRmn_Sequencial = H00RY2_A2069ContratoServicosRmn_Sequencial[0];
               /* Execute user event: E17RY2 */
               E17RY2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 8;
            WBRY0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV17TFContratoServicosRmn_Inicio ,
                                              AV18TFContratoServicosRmn_Inicio_To ,
                                              AV21TFContratoServicosRmn_Fim ,
                                              AV22TFContratoServicosRmn_Fim_To ,
                                              AV25TFContratoServicosRmn_Valor ,
                                              AV26TFContratoServicosRmn_Valor_To ,
                                              A2070ContratoServicosRmn_Inicio ,
                                              A2071ContratoServicosRmn_Fim ,
                                              A2072ContratoServicosRmn_Valor ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A160ContratoServicos_Codigo ,
                                              AV7ContratoServicos_Codigo },
                                              new int[] {
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00RY3 */
         pr_default.execute(1, new Object[] {AV7ContratoServicos_Codigo, AV17TFContratoServicosRmn_Inicio, AV18TFContratoServicosRmn_Inicio_To, AV21TFContratoServicosRmn_Fim, AV22TFContratoServicosRmn_Fim_To, AV25TFContratoServicosRmn_Valor, AV26TFContratoServicosRmn_Valor_To});
         GRID_nRecordCount = H00RY3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosRmn_Inicio, AV18TFContratoServicosRmn_Inicio_To, AV21TFContratoServicosRmn_Fim, AV22TFContratoServicosRmn_Fim_To, AV25TFContratoServicosRmn_Valor, AV26TFContratoServicosRmn_Valor_To, AV7ContratoServicos_Codigo, AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace, AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace, AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace, AV34Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosRmn_Inicio, AV18TFContratoServicosRmn_Inicio_To, AV21TFContratoServicosRmn_Fim, AV22TFContratoServicosRmn_Fim_To, AV25TFContratoServicosRmn_Valor, AV26TFContratoServicosRmn_Valor_To, AV7ContratoServicos_Codigo, AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace, AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace, AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace, AV34Pgmname, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosRmn_Inicio, AV18TFContratoServicosRmn_Inicio_To, AV21TFContratoServicosRmn_Fim, AV22TFContratoServicosRmn_Fim_To, AV25TFContratoServicosRmn_Valor, AV26TFContratoServicosRmn_Valor_To, AV7ContratoServicos_Codigo, AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace, AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace, AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace, AV34Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosRmn_Inicio, AV18TFContratoServicosRmn_Inicio_To, AV21TFContratoServicosRmn_Fim, AV22TFContratoServicosRmn_Fim_To, AV25TFContratoServicosRmn_Valor, AV26TFContratoServicosRmn_Valor_To, AV7ContratoServicos_Codigo, AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace, AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace, AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace, AV34Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContratoServicosRmn_Inicio, AV18TFContratoServicosRmn_Inicio_To, AV21TFContratoServicosRmn_Fim, AV22TFContratoServicosRmn_Fim_To, AV25TFContratoServicosRmn_Valor, AV26TFContratoServicosRmn_Valor_To, AV7ContratoServicos_Codigo, AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace, AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace, AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace, AV34Pgmname, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPRY0( )
      {
         /* Before Start, stand alone formulas. */
         AV34Pgmname = "CntSrvRemuneracaoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E15RY2 */
         E15RY2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV28DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSRMN_INICIOTITLEFILTERDATA"), AV16ContratoServicosRmn_InicioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSRMN_FIMTITLEFILTERDATA"), AV20ContratoServicosRmn_FimTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSRMN_VALORTITLEFILTERDATA"), AV24ContratoServicosRmn_ValorTitleFilterData);
            /* Read variables values. */
            A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_inicio_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_inicio_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSRMN_INICIO");
               GX_FocusControl = edtavTfcontratoservicosrmn_inicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17TFContratoServicosRmn_Inicio = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContratoServicosRmn_Inicio", StringUtil.LTrim( StringUtil.Str( AV17TFContratoServicosRmn_Inicio, 14, 5)));
            }
            else
            {
               AV17TFContratoServicosRmn_Inicio = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_inicio_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContratoServicosRmn_Inicio", StringUtil.LTrim( StringUtil.Str( AV17TFContratoServicosRmn_Inicio, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_inicio_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_inicio_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSRMN_INICIO_TO");
               GX_FocusControl = edtavTfcontratoservicosrmn_inicio_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18TFContratoServicosRmn_Inicio_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosRmn_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV18TFContratoServicosRmn_Inicio_To, 14, 5)));
            }
            else
            {
               AV18TFContratoServicosRmn_Inicio_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_inicio_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosRmn_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV18TFContratoServicosRmn_Inicio_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_fim_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_fim_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSRMN_FIM");
               GX_FocusControl = edtavTfcontratoservicosrmn_fim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21TFContratoServicosRmn_Fim = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosRmn_Fim", StringUtil.LTrim( StringUtil.Str( AV21TFContratoServicosRmn_Fim, 14, 5)));
            }
            else
            {
               AV21TFContratoServicosRmn_Fim = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_fim_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosRmn_Fim", StringUtil.LTrim( StringUtil.Str( AV21TFContratoServicosRmn_Fim, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_fim_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_fim_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSRMN_FIM_TO");
               GX_FocusControl = edtavTfcontratoservicosrmn_fim_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22TFContratoServicosRmn_Fim_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosRmn_Fim_To", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosRmn_Fim_To, 14, 5)));
            }
            else
            {
               AV22TFContratoServicosRmn_Fim_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_fim_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosRmn_Fim_To", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosRmn_Fim_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_valor_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_valor_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSRMN_VALOR");
               GX_FocusControl = edtavTfcontratoservicosrmn_valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25TFContratoServicosRmn_Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosRmn_Valor", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosRmn_Valor, 14, 5)));
            }
            else
            {
               AV25TFContratoServicosRmn_Valor = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosRmn_Valor", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosRmn_Valor, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_valor_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_valor_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSRMN_VALOR_TO");
               GX_FocusControl = edtavTfcontratoservicosrmn_valor_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26TFContratoServicosRmn_Valor_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosRmn_Valor_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosRmn_Valor_To, 14, 5)));
            }
            else
            {
               AV26TFContratoServicosRmn_Valor_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosrmn_valor_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosRmn_Valor_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosRmn_Valor_To, 14, 5)));
            }
            AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosrmn_iniciotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace", AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace);
            AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosrmn_fimtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace", AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace);
            AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosrmn_valortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace", AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV30GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV31GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicos_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoservicosrmn_inicio_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Caption");
            Ddo_contratoservicosrmn_inicio_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Tooltip");
            Ddo_contratoservicosrmn_inicio_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Cls");
            Ddo_contratoservicosrmn_inicio_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filteredtext_set");
            Ddo_contratoservicosrmn_inicio_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filteredtextto_set");
            Ddo_contratoservicosrmn_inicio_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Dropdownoptionstype");
            Ddo_contratoservicosrmn_inicio_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Titlecontrolidtoreplace");
            Ddo_contratoservicosrmn_inicio_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Includesortasc"));
            Ddo_contratoservicosrmn_inicio_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Includesortdsc"));
            Ddo_contratoservicosrmn_inicio_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Sortedstatus");
            Ddo_contratoservicosrmn_inicio_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Includefilter"));
            Ddo_contratoservicosrmn_inicio_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filtertype");
            Ddo_contratoservicosrmn_inicio_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filterisrange"));
            Ddo_contratoservicosrmn_inicio_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Includedatalist"));
            Ddo_contratoservicosrmn_inicio_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Datalistfixedvalues");
            Ddo_contratoservicosrmn_inicio_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosrmn_inicio_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Sortasc");
            Ddo_contratoservicosrmn_inicio_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Sortdsc");
            Ddo_contratoservicosrmn_inicio_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Loadingdata");
            Ddo_contratoservicosrmn_inicio_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Cleanfilter");
            Ddo_contratoservicosrmn_inicio_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Rangefilterfrom");
            Ddo_contratoservicosrmn_inicio_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Rangefilterto");
            Ddo_contratoservicosrmn_inicio_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Noresultsfound");
            Ddo_contratoservicosrmn_inicio_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Searchbuttontext");
            Ddo_contratoservicosrmn_fim_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Caption");
            Ddo_contratoservicosrmn_fim_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Tooltip");
            Ddo_contratoservicosrmn_fim_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Cls");
            Ddo_contratoservicosrmn_fim_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filteredtext_set");
            Ddo_contratoservicosrmn_fim_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filteredtextto_set");
            Ddo_contratoservicosrmn_fim_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Dropdownoptionstype");
            Ddo_contratoservicosrmn_fim_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Titlecontrolidtoreplace");
            Ddo_contratoservicosrmn_fim_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Includesortasc"));
            Ddo_contratoservicosrmn_fim_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Includesortdsc"));
            Ddo_contratoservicosrmn_fim_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Sortedstatus");
            Ddo_contratoservicosrmn_fim_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Includefilter"));
            Ddo_contratoservicosrmn_fim_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filtertype");
            Ddo_contratoservicosrmn_fim_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filterisrange"));
            Ddo_contratoservicosrmn_fim_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Includedatalist"));
            Ddo_contratoservicosrmn_fim_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Datalistfixedvalues");
            Ddo_contratoservicosrmn_fim_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosrmn_fim_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Sortasc");
            Ddo_contratoservicosrmn_fim_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Sortdsc");
            Ddo_contratoservicosrmn_fim_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Loadingdata");
            Ddo_contratoservicosrmn_fim_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Cleanfilter");
            Ddo_contratoservicosrmn_fim_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Rangefilterfrom");
            Ddo_contratoservicosrmn_fim_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Rangefilterto");
            Ddo_contratoservicosrmn_fim_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Noresultsfound");
            Ddo_contratoservicosrmn_fim_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Searchbuttontext");
            Ddo_contratoservicosrmn_valor_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Caption");
            Ddo_contratoservicosrmn_valor_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Tooltip");
            Ddo_contratoservicosrmn_valor_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Cls");
            Ddo_contratoservicosrmn_valor_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filteredtext_set");
            Ddo_contratoservicosrmn_valor_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filteredtextto_set");
            Ddo_contratoservicosrmn_valor_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Dropdownoptionstype");
            Ddo_contratoservicosrmn_valor_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Titlecontrolidtoreplace");
            Ddo_contratoservicosrmn_valor_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Includesortasc"));
            Ddo_contratoservicosrmn_valor_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Includesortdsc"));
            Ddo_contratoservicosrmn_valor_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Sortedstatus");
            Ddo_contratoservicosrmn_valor_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Includefilter"));
            Ddo_contratoservicosrmn_valor_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filtertype");
            Ddo_contratoservicosrmn_valor_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filterisrange"));
            Ddo_contratoservicosrmn_valor_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Includedatalist"));
            Ddo_contratoservicosrmn_valor_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Datalistfixedvalues");
            Ddo_contratoservicosrmn_valor_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosrmn_valor_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Sortasc");
            Ddo_contratoservicosrmn_valor_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Sortdsc");
            Ddo_contratoservicosrmn_valor_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Loadingdata");
            Ddo_contratoservicosrmn_valor_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Cleanfilter");
            Ddo_contratoservicosrmn_valor_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Rangefilterfrom");
            Ddo_contratoservicosrmn_valor_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Rangefilterto");
            Ddo_contratoservicosrmn_valor_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Noresultsfound");
            Ddo_contratoservicosrmn_valor_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicosrmn_inicio_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Activeeventkey");
            Ddo_contratoservicosrmn_inicio_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filteredtext_get");
            Ddo_contratoservicosrmn_inicio_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO_Filteredtextto_get");
            Ddo_contratoservicosrmn_fim_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Activeeventkey");
            Ddo_contratoservicosrmn_fim_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filteredtext_get");
            Ddo_contratoservicosrmn_fim_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM_Filteredtextto_get");
            Ddo_contratoservicosrmn_valor_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Activeeventkey");
            Ddo_contratoservicosrmn_valor_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filteredtext_get");
            Ddo_contratoservicosrmn_valor_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_INICIO"), ",", ".") != AV17TFContratoServicosRmn_Inicio )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_INICIO_TO"), ",", ".") != AV18TFContratoServicosRmn_Inicio_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_FIM"), ",", ".") != AV21TFContratoServicosRmn_Fim )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_FIM_TO"), ",", ".") != AV22TFContratoServicosRmn_Fim_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_VALOR"), ",", ".") != AV25TFContratoServicosRmn_Valor )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSRMN_VALOR_TO"), ",", ".") != AV26TFContratoServicosRmn_Valor_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E15RY2 */
         E15RY2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15RY2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratoservicosrmn_inicio_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosrmn_inicio_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosrmn_inicio_Visible), 5, 0)));
         edtavTfcontratoservicosrmn_inicio_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosrmn_inicio_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosrmn_inicio_to_Visible), 5, 0)));
         edtavTfcontratoservicosrmn_fim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosrmn_fim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosrmn_fim_Visible), 5, 0)));
         edtavTfcontratoservicosrmn_fim_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosrmn_fim_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosrmn_fim_to_Visible), 5, 0)));
         edtavTfcontratoservicosrmn_valor_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosrmn_valor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosrmn_valor_Visible), 5, 0)));
         edtavTfcontratoservicosrmn_valor_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosrmn_valor_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosrmn_valor_to_Visible), 5, 0)));
         Ddo_contratoservicosrmn_inicio_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosRmn_Inicio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_inicio_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosrmn_inicio_Titlecontrolidtoreplace);
         AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace = Ddo_contratoservicosrmn_inicio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace", AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace);
         edtavDdo_contratoservicosrmn_iniciotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosrmn_iniciotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosrmn_iniciotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosrmn_fim_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosRmn_Fim";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_fim_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosrmn_fim_Titlecontrolidtoreplace);
         AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace = Ddo_contratoservicosrmn_fim_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace", AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace);
         edtavDdo_contratoservicosrmn_fimtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosrmn_fimtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosrmn_fimtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosrmn_valor_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosRmn_Valor";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_valor_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosrmn_valor_Titlecontrolidtoreplace);
         AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace = Ddo_contratoservicosrmn_valor_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace", AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace);
         edtavDdo_contratoservicosrmn_valortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosrmn_valortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosrmn_valortitlecontrolidtoreplace_Visible), 5, 0)));
         edtContratoServicos_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicos_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV28DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV28DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E16RY2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV16ContratoServicosRmn_InicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV20ContratoServicosRmn_FimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ContratoServicosRmn_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicosRmn_Inicio_Titleformat = 2;
         edtContratoServicosRmn_Inicio_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Desde", AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosRmn_Inicio_Internalname, "Title", edtContratoServicosRmn_Inicio_Title);
         edtContratoServicosRmn_Fim_Titleformat = 2;
         edtContratoServicosRmn_Fim_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "At�", AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosRmn_Fim_Internalname, "Title", edtContratoServicosRmn_Fim_Title);
         edtContratoServicosRmn_Valor_Titleformat = 2;
         edtContratoServicosRmn_Valor_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor", AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosRmn_Valor_Internalname, "Title", edtContratoServicosRmn_Valor_Title);
         AV30GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30GridCurrentPage), 10, 0)));
         AV31GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV16ContratoServicosRmn_InicioTitleFilterData", AV16ContratoServicosRmn_InicioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV20ContratoServicosRmn_FimTitleFilterData", AV20ContratoServicosRmn_FimTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV24ContratoServicosRmn_ValorTitleFilterData", AV24ContratoServicosRmn_ValorTitleFilterData);
      }

      protected void E11RY2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV29PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV29PageToGo) ;
         }
      }

      protected void E12RY2( )
      {
         /* Ddo_contratoservicosrmn_inicio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosrmn_inicio_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosrmn_inicio_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_inicio_Internalname, "SortedStatus", Ddo_contratoservicosrmn_inicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosrmn_inicio_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosrmn_inicio_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_inicio_Internalname, "SortedStatus", Ddo_contratoservicosrmn_inicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosrmn_inicio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV17TFContratoServicosRmn_Inicio = NumberUtil.Val( Ddo_contratoservicosrmn_inicio_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContratoServicosRmn_Inicio", StringUtil.LTrim( StringUtil.Str( AV17TFContratoServicosRmn_Inicio, 14, 5)));
            AV18TFContratoServicosRmn_Inicio_To = NumberUtil.Val( Ddo_contratoservicosrmn_inicio_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosRmn_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV18TFContratoServicosRmn_Inicio_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13RY2( )
      {
         /* Ddo_contratoservicosrmn_fim_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosrmn_fim_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosrmn_fim_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_fim_Internalname, "SortedStatus", Ddo_contratoservicosrmn_fim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosrmn_fim_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosrmn_fim_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_fim_Internalname, "SortedStatus", Ddo_contratoservicosrmn_fim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosrmn_fim_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV21TFContratoServicosRmn_Fim = NumberUtil.Val( Ddo_contratoservicosrmn_fim_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosRmn_Fim", StringUtil.LTrim( StringUtil.Str( AV21TFContratoServicosRmn_Fim, 14, 5)));
            AV22TFContratoServicosRmn_Fim_To = NumberUtil.Val( Ddo_contratoservicosrmn_fim_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosRmn_Fim_To", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosRmn_Fim_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E14RY2( )
      {
         /* Ddo_contratoservicosrmn_valor_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosrmn_valor_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosrmn_valor_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_valor_Internalname, "SortedStatus", Ddo_contratoservicosrmn_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosrmn_valor_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosrmn_valor_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_valor_Internalname, "SortedStatus", Ddo_contratoservicosrmn_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosrmn_valor_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV25TFContratoServicosRmn_Valor = NumberUtil.Val( Ddo_contratoservicosrmn_valor_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosRmn_Valor", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosRmn_Valor, 14, 5)));
            AV26TFContratoServicosRmn_Valor_To = NumberUtil.Val( Ddo_contratoservicosrmn_valor_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosRmn_Valor_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosRmn_Valor_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      private void E17RY2( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicosrmn_inicio_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_inicio_Internalname, "SortedStatus", Ddo_contratoservicosrmn_inicio_Sortedstatus);
         Ddo_contratoservicosrmn_fim_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_fim_Internalname, "SortedStatus", Ddo_contratoservicosrmn_fim_Sortedstatus);
         Ddo_contratoservicosrmn_valor_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_valor_Internalname, "SortedStatus", Ddo_contratoservicosrmn_valor_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoservicosrmn_inicio_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_inicio_Internalname, "SortedStatus", Ddo_contratoservicosrmn_inicio_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratoservicosrmn_fim_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_fim_Internalname, "SortedStatus", Ddo_contratoservicosrmn_fim_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoservicosrmn_valor_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_valor_Internalname, "SortedStatus", Ddo_contratoservicosrmn_valor_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV34Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV34Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV34Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV35GXV1 = 1;
         while ( AV35GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV35GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSRMN_INICIO") == 0 )
            {
               AV17TFContratoServicosRmn_Inicio = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContratoServicosRmn_Inicio", StringUtil.LTrim( StringUtil.Str( AV17TFContratoServicosRmn_Inicio, 14, 5)));
               AV18TFContratoServicosRmn_Inicio_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosRmn_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV18TFContratoServicosRmn_Inicio_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV17TFContratoServicosRmn_Inicio) )
               {
                  Ddo_contratoservicosrmn_inicio_Filteredtext_set = StringUtil.Str( AV17TFContratoServicosRmn_Inicio, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_inicio_Internalname, "FilteredText_set", Ddo_contratoservicosrmn_inicio_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV18TFContratoServicosRmn_Inicio_To) )
               {
                  Ddo_contratoservicosrmn_inicio_Filteredtextto_set = StringUtil.Str( AV18TFContratoServicosRmn_Inicio_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_inicio_Internalname, "FilteredTextTo_set", Ddo_contratoservicosrmn_inicio_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSRMN_FIM") == 0 )
            {
               AV21TFContratoServicosRmn_Fim = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosRmn_Fim", StringUtil.LTrim( StringUtil.Str( AV21TFContratoServicosRmn_Fim, 14, 5)));
               AV22TFContratoServicosRmn_Fim_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosRmn_Fim_To", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosRmn_Fim_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV21TFContratoServicosRmn_Fim) )
               {
                  Ddo_contratoservicosrmn_fim_Filteredtext_set = StringUtil.Str( AV21TFContratoServicosRmn_Fim, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_fim_Internalname, "FilteredText_set", Ddo_contratoservicosrmn_fim_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV22TFContratoServicosRmn_Fim_To) )
               {
                  Ddo_contratoservicosrmn_fim_Filteredtextto_set = StringUtil.Str( AV22TFContratoServicosRmn_Fim_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_fim_Internalname, "FilteredTextTo_set", Ddo_contratoservicosrmn_fim_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSRMN_VALOR") == 0 )
            {
               AV25TFContratoServicosRmn_Valor = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosRmn_Valor", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosRmn_Valor, 14, 5)));
               AV26TFContratoServicosRmn_Valor_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosRmn_Valor_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosRmn_Valor_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV25TFContratoServicosRmn_Valor) )
               {
                  Ddo_contratoservicosrmn_valor_Filteredtext_set = StringUtil.Str( AV25TFContratoServicosRmn_Valor, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_valor_Internalname, "FilteredText_set", Ddo_contratoservicosrmn_valor_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV26TFContratoServicosRmn_Valor_To) )
               {
                  Ddo_contratoservicosrmn_valor_Filteredtextto_set = StringUtil.Str( AV26TFContratoServicosRmn_Valor_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosrmn_valor_Internalname, "FilteredTextTo_set", Ddo_contratoservicosrmn_valor_Filteredtextto_set);
               }
            }
            AV35GXV1 = (int)(AV35GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV34Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (Convert.ToDecimal(0)==AV17TFContratoServicosRmn_Inicio) && (Convert.ToDecimal(0)==AV18TFContratoServicosRmn_Inicio_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSRMN_INICIO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV17TFContratoServicosRmn_Inicio, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV18TFContratoServicosRmn_Inicio_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV21TFContratoServicosRmn_Fim) && (Convert.ToDecimal(0)==AV22TFContratoServicosRmn_Fim_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSRMN_FIM";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV21TFContratoServicosRmn_Fim, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV22TFContratoServicosRmn_Fim_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV25TFContratoServicosRmn_Valor) && (Convert.ToDecimal(0)==AV26TFContratoServicosRmn_Valor_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSRMN_VALOR";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV25TFContratoServicosRmn_Valor, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV26TFContratoServicosRmn_Valor_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV34Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV34Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoServicos";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratoServicos_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_RY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_RY2( true) ;
         }
         else
         {
            wb_table2_5_RY2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_RY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_RY2e( true) ;
         }
         else
         {
            wb_table1_2_RY2e( false) ;
         }
      }

      protected void wb_table2_5_RY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Sequencial") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosRmn_Inicio_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosRmn_Inicio_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosRmn_Inicio_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosRmn_Fim_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosRmn_Fim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosRmn_Fim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosRmn_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosRmn_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosRmn_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2070ContratoServicosRmn_Inicio, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosRmn_Inicio_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Inicio_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2071ContratoServicosRmn_Fim, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosRmn_Fim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Fim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2072ContratoServicosRmn_Valor, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosRmn_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_RY2e( true) ;
         }
         else
         {
            wb_table2_5_RY2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PARY2( ) ;
         WSRY2( ) ;
         WERY2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContratoServicos_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PARY2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "cntsrvremuneracaowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PARY2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
         }
         wcpOAV7ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicos_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContratoServicos_Codigo != wcpOAV7ContratoServicos_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContratoServicos_Codigo = AV7ContratoServicos_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContratoServicos_Codigo = cgiGet( sPrefix+"AV7ContratoServicos_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContratoServicos_Codigo) > 0 )
         {
            AV7ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContratoServicos_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
         }
         else
         {
            AV7ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContratoServicos_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PARY2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSRY2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSRY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicos_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicos_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContratoServicos_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicos_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ContratoServicos_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WERY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822491829");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("cntsrvremuneracaowc.js", "?202042822491829");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtContratoServicosRmn_Sequencial_Internalname = sPrefix+"CONTRATOSERVICOSRMN_SEQUENCIAL_"+sGXsfl_8_idx;
         edtContratoServicosRmn_Inicio_Internalname = sPrefix+"CONTRATOSERVICOSRMN_INICIO_"+sGXsfl_8_idx;
         edtContratoServicosRmn_Fim_Internalname = sPrefix+"CONTRATOSERVICOSRMN_FIM_"+sGXsfl_8_idx;
         edtContratoServicosRmn_Valor_Internalname = sPrefix+"CONTRATOSERVICOSRMN_VALOR_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtContratoServicosRmn_Sequencial_Internalname = sPrefix+"CONTRATOSERVICOSRMN_SEQUENCIAL_"+sGXsfl_8_fel_idx;
         edtContratoServicosRmn_Inicio_Internalname = sPrefix+"CONTRATOSERVICOSRMN_INICIO_"+sGXsfl_8_fel_idx;
         edtContratoServicosRmn_Fim_Internalname = sPrefix+"CONTRATOSERVICOSRMN_FIM_"+sGXsfl_8_fel_idx;
         edtContratoServicosRmn_Valor_Internalname = sPrefix+"CONTRATOSERVICOSRMN_VALOR_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBRY0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosRmn_Sequencial_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A2069ContratoServicosRmn_Sequencial), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosRmn_Sequencial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosRmn_Inicio_Internalname,StringUtil.LTrim( StringUtil.NToC( A2070ContratoServicosRmn_Inicio, 14, 5, ",", "")),context.localUtil.Format( A2070ContratoServicosRmn_Inicio, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosRmn_Inicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosRmn_Fim_Internalname,StringUtil.LTrim( StringUtil.NToC( A2071ContratoServicosRmn_Fim, 14, 5, ",", "")),context.localUtil.Format( A2071ContratoServicosRmn_Fim, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosRmn_Fim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosRmn_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A2072ContratoServicosRmn_Valor, 14, 5, ",", "")),context.localUtil.Format( A2072ContratoServicosRmn_Valor, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosRmn_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSRMN_SEQUENCIAL"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A2069ContratoServicosRmn_Sequencial), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSRMN_INICIO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A2070ContratoServicosRmn_Inicio, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSRMN_FIM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A2071ContratoServicosRmn_Fim, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSRMN_VALOR"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A2072ContratoServicosRmn_Valor, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtContratoServicosRmn_Sequencial_Internalname = sPrefix+"CONTRATOSERVICOSRMN_SEQUENCIAL";
         edtContratoServicosRmn_Inicio_Internalname = sPrefix+"CONTRATOSERVICOSRMN_INICIO";
         edtContratoServicosRmn_Fim_Internalname = sPrefix+"CONTRATOSERVICOSRMN_FIM";
         edtContratoServicosRmn_Valor_Internalname = sPrefix+"CONTRATOSERVICOSRMN_VALOR";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtContratoServicos_Codigo_Internalname = sPrefix+"CONTRATOSERVICOS_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontratoservicosrmn_inicio_Internalname = sPrefix+"vTFCONTRATOSERVICOSRMN_INICIO";
         edtavTfcontratoservicosrmn_inicio_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSRMN_INICIO_TO";
         edtavTfcontratoservicosrmn_fim_Internalname = sPrefix+"vTFCONTRATOSERVICOSRMN_FIM";
         edtavTfcontratoservicosrmn_fim_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSRMN_FIM_TO";
         edtavTfcontratoservicosrmn_valor_Internalname = sPrefix+"vTFCONTRATOSERVICOSRMN_VALOR";
         edtavTfcontratoservicosrmn_valor_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSRMN_VALOR_TO";
         Ddo_contratoservicosrmn_inicio_Internalname = sPrefix+"DDO_CONTRATOSERVICOSRMN_INICIO";
         edtavDdo_contratoservicosrmn_iniciotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSRMN_INICIOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosrmn_fim_Internalname = sPrefix+"DDO_CONTRATOSERVICOSRMN_FIM";
         edtavDdo_contratoservicosrmn_fimtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSRMN_FIMTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosrmn_valor_Internalname = sPrefix+"DDO_CONTRATOSERVICOSRMN_VALOR";
         edtavDdo_contratoservicosrmn_valortitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSRMN_VALORTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoServicosRmn_Valor_Jsonclick = "";
         edtContratoServicosRmn_Fim_Jsonclick = "";
         edtContratoServicosRmn_Inicio_Jsonclick = "";
         edtContratoServicosRmn_Sequencial_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoServicosRmn_Valor_Titleformat = 0;
         edtContratoServicosRmn_Fim_Titleformat = 0;
         edtContratoServicosRmn_Inicio_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtContratoServicosRmn_Valor_Title = "Valor";
         edtContratoServicosRmn_Fim_Title = "At�";
         edtContratoServicosRmn_Inicio_Title = "Desde";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contratoservicosrmn_valortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosrmn_fimtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosrmn_iniciotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicosrmn_valor_to_Jsonclick = "";
         edtavTfcontratoservicosrmn_valor_to_Visible = 1;
         edtavTfcontratoservicosrmn_valor_Jsonclick = "";
         edtavTfcontratoservicosrmn_valor_Visible = 1;
         edtavTfcontratoservicosrmn_fim_to_Jsonclick = "";
         edtavTfcontratoservicosrmn_fim_to_Visible = 1;
         edtavTfcontratoservicosrmn_fim_Jsonclick = "";
         edtavTfcontratoservicosrmn_fim_Visible = 1;
         edtavTfcontratoservicosrmn_inicio_to_Jsonclick = "";
         edtavTfcontratoservicosrmn_inicio_to_Visible = 1;
         edtavTfcontratoservicosrmn_inicio_Jsonclick = "";
         edtavTfcontratoservicosrmn_inicio_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtContratoServicos_Codigo_Jsonclick = "";
         edtContratoServicos_Codigo_Visible = 1;
         Ddo_contratoservicosrmn_valor_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosrmn_valor_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosrmn_valor_Rangefilterto = "At�";
         Ddo_contratoservicosrmn_valor_Rangefilterfrom = "Desde";
         Ddo_contratoservicosrmn_valor_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosrmn_valor_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosrmn_valor_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosrmn_valor_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosrmn_valor_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosrmn_valor_Datalistfixedvalues = "";
         Ddo_contratoservicosrmn_valor_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosrmn_valor_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_valor_Filtertype = "Numeric";
         Ddo_contratoservicosrmn_valor_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_valor_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_valor_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_valor_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosrmn_valor_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosrmn_valor_Cls = "ColumnSettings";
         Ddo_contratoservicosrmn_valor_Tooltip = "Op��es";
         Ddo_contratoservicosrmn_valor_Caption = "";
         Ddo_contratoservicosrmn_fim_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosrmn_fim_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosrmn_fim_Rangefilterto = "At�";
         Ddo_contratoservicosrmn_fim_Rangefilterfrom = "Desde";
         Ddo_contratoservicosrmn_fim_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosrmn_fim_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosrmn_fim_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosrmn_fim_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosrmn_fim_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosrmn_fim_Datalistfixedvalues = "";
         Ddo_contratoservicosrmn_fim_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosrmn_fim_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_fim_Filtertype = "Numeric";
         Ddo_contratoservicosrmn_fim_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_fim_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_fim_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_fim_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosrmn_fim_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosrmn_fim_Cls = "ColumnSettings";
         Ddo_contratoservicosrmn_fim_Tooltip = "Op��es";
         Ddo_contratoservicosrmn_fim_Caption = "";
         Ddo_contratoservicosrmn_inicio_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosrmn_inicio_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosrmn_inicio_Rangefilterto = "At�";
         Ddo_contratoservicosrmn_inicio_Rangefilterfrom = "Desde";
         Ddo_contratoservicosrmn_inicio_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosrmn_inicio_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosrmn_inicio_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosrmn_inicio_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosrmn_inicio_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosrmn_inicio_Datalistfixedvalues = "";
         Ddo_contratoservicosrmn_inicio_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosrmn_inicio_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_inicio_Filtertype = "Numeric";
         Ddo_contratoservicosrmn_inicio_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_inicio_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_inicio_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosrmn_inicio_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosrmn_inicio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosrmn_inicio_Cls = "ColumnSettings";
         Ddo_contratoservicosrmn_inicio_Tooltip = "Op��es";
         Ddo_contratoservicosrmn_inicio_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContratoServicosRmn_Inicio',fld:'vTFCONTRATOSERVICOSRMN_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18TFContratoServicosRmn_Inicio_To',fld:'vTFCONTRATOSERVICOSRMN_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV21TFContratoServicosRmn_Fim',fld:'vTFCONTRATOSERVICOSRMN_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosRmn_Fim_To',fld:'vTFCONTRATOSERVICOSRMN_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TFContratoServicosRmn_Valor',fld:'vTFCONTRATOSERVICOSRMN_VALOR',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosRmn_Valor_To',fld:'vTFCONTRATOSERVICOSRMN_VALOR_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV16ContratoServicosRmn_InicioTitleFilterData',fld:'vCONTRATOSERVICOSRMN_INICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV20ContratoServicosRmn_FimTitleFilterData',fld:'vCONTRATOSERVICOSRMN_FIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ContratoServicosRmn_ValorTitleFilterData',fld:'vCONTRATOSERVICOSRMN_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosRmn_Inicio_Titleformat',ctrl:'CONTRATOSERVICOSRMN_INICIO',prop:'Titleformat'},{av:'edtContratoServicosRmn_Inicio_Title',ctrl:'CONTRATOSERVICOSRMN_INICIO',prop:'Title'},{av:'edtContratoServicosRmn_Fim_Titleformat',ctrl:'CONTRATOSERVICOSRMN_FIM',prop:'Titleformat'},{av:'edtContratoServicosRmn_Fim_Title',ctrl:'CONTRATOSERVICOSRMN_FIM',prop:'Title'},{av:'edtContratoServicosRmn_Valor_Titleformat',ctrl:'CONTRATOSERVICOSRMN_VALOR',prop:'Titleformat'},{av:'edtContratoServicosRmn_Valor_Title',ctrl:'CONTRATOSERVICOSRMN_VALOR',prop:'Title'},{av:'AV30GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV31GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11RY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContratoServicosRmn_Inicio',fld:'vTFCONTRATOSERVICOSRMN_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18TFContratoServicosRmn_Inicio_To',fld:'vTFCONTRATOSERVICOSRMN_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV21TFContratoServicosRmn_Fim',fld:'vTFCONTRATOSERVICOSRMN_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosRmn_Fim_To',fld:'vTFCONTRATOSERVICOSRMN_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TFContratoServicosRmn_Valor',fld:'vTFCONTRATOSERVICOSRMN_VALOR',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosRmn_Valor_To',fld:'vTFCONTRATOSERVICOSRMN_VALOR_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOSRMN_INICIO.ONOPTIONCLICKED","{handler:'E12RY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContratoServicosRmn_Inicio',fld:'vTFCONTRATOSERVICOSRMN_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18TFContratoServicosRmn_Inicio_To',fld:'vTFCONTRATOSERVICOSRMN_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV21TFContratoServicosRmn_Fim',fld:'vTFCONTRATOSERVICOSRMN_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosRmn_Fim_To',fld:'vTFCONTRATOSERVICOSRMN_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TFContratoServicosRmn_Valor',fld:'vTFCONTRATOSERVICOSRMN_VALOR',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosRmn_Valor_To',fld:'vTFCONTRATOSERVICOSRMN_VALOR_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosrmn_inicio_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSRMN_INICIO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosrmn_inicio_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSRMN_INICIO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosrmn_inicio_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSRMN_INICIO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosrmn_inicio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSRMN_INICIO',prop:'SortedStatus'},{av:'AV17TFContratoServicosRmn_Inicio',fld:'vTFCONTRATOSERVICOSRMN_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18TFContratoServicosRmn_Inicio_To',fld:'vTFCONTRATOSERVICOSRMN_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratoservicosrmn_fim_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSRMN_FIM',prop:'SortedStatus'},{av:'Ddo_contratoservicosrmn_valor_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSRMN_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSRMN_FIM.ONOPTIONCLICKED","{handler:'E13RY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContratoServicosRmn_Inicio',fld:'vTFCONTRATOSERVICOSRMN_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18TFContratoServicosRmn_Inicio_To',fld:'vTFCONTRATOSERVICOSRMN_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV21TFContratoServicosRmn_Fim',fld:'vTFCONTRATOSERVICOSRMN_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosRmn_Fim_To',fld:'vTFCONTRATOSERVICOSRMN_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TFContratoServicosRmn_Valor',fld:'vTFCONTRATOSERVICOSRMN_VALOR',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosRmn_Valor_To',fld:'vTFCONTRATOSERVICOSRMN_VALOR_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosrmn_fim_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSRMN_FIM',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosrmn_fim_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSRMN_FIM',prop:'FilteredText_get'},{av:'Ddo_contratoservicosrmn_fim_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSRMN_FIM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosrmn_fim_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSRMN_FIM',prop:'SortedStatus'},{av:'AV21TFContratoServicosRmn_Fim',fld:'vTFCONTRATOSERVICOSRMN_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosRmn_Fim_To',fld:'vTFCONTRATOSERVICOSRMN_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratoservicosrmn_inicio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSRMN_INICIO',prop:'SortedStatus'},{av:'Ddo_contratoservicosrmn_valor_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSRMN_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSRMN_VALOR.ONOPTIONCLICKED","{handler:'E14RY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContratoServicosRmn_Inicio',fld:'vTFCONTRATOSERVICOSRMN_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV18TFContratoServicosRmn_Inicio_To',fld:'vTFCONTRATOSERVICOSRMN_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV21TFContratoServicosRmn_Fim',fld:'vTFCONTRATOSERVICOSRMN_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22TFContratoServicosRmn_Fim_To',fld:'vTFCONTRATOSERVICOSRMN_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25TFContratoServicosRmn_Valor',fld:'vTFCONTRATOSERVICOSRMN_VALOR',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosRmn_Valor_To',fld:'vTFCONTRATOSERVICOSRMN_VALOR_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSRMN_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosrmn_valor_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSRMN_VALOR',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosrmn_valor_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSRMN_VALOR',prop:'FilteredText_get'},{av:'Ddo_contratoservicosrmn_valor_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSRMN_VALOR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosrmn_valor_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSRMN_VALOR',prop:'SortedStatus'},{av:'AV25TFContratoServicosRmn_Valor',fld:'vTFCONTRATOSERVICOSRMN_VALOR',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoServicosRmn_Valor_To',fld:'vTFCONTRATOSERVICOSRMN_VALOR_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratoservicosrmn_inicio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSRMN_INICIO',prop:'SortedStatus'},{av:'Ddo_contratoservicosrmn_fim_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSRMN_FIM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E17RY2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicosrmn_inicio_Activeeventkey = "";
         Ddo_contratoservicosrmn_inicio_Filteredtext_get = "";
         Ddo_contratoservicosrmn_inicio_Filteredtextto_get = "";
         Ddo_contratoservicosrmn_fim_Activeeventkey = "";
         Ddo_contratoservicosrmn_fim_Filteredtext_get = "";
         Ddo_contratoservicosrmn_fim_Filteredtextto_get = "";
         Ddo_contratoservicosrmn_valor_Activeeventkey = "";
         Ddo_contratoservicosrmn_valor_Filteredtext_get = "";
         Ddo_contratoservicosrmn_valor_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace = "";
         AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace = "";
         AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace = "";
         AV34Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV28DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV16ContratoServicosRmn_InicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV20ContratoServicosRmn_FimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ContratoServicosRmn_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoservicosrmn_inicio_Filteredtext_set = "";
         Ddo_contratoservicosrmn_inicio_Filteredtextto_set = "";
         Ddo_contratoservicosrmn_inicio_Sortedstatus = "";
         Ddo_contratoservicosrmn_fim_Filteredtext_set = "";
         Ddo_contratoservicosrmn_fim_Filteredtextto_set = "";
         Ddo_contratoservicosrmn_fim_Sortedstatus = "";
         Ddo_contratoservicosrmn_valor_Filteredtext_set = "";
         Ddo_contratoservicosrmn_valor_Filteredtextto_set = "";
         Ddo_contratoservicosrmn_valor_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00RY2_A160ContratoServicos_Codigo = new int[1] ;
         H00RY2_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         H00RY2_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         H00RY2_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         H00RY2_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         H00RY3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContratoServicos_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.cntsrvremuneracaowc__default(),
            new Object[][] {
                new Object[] {
               H00RY2_A160ContratoServicos_Codigo, H00RY2_A2072ContratoServicosRmn_Valor, H00RY2_A2071ContratoServicosRmn_Fim, H00RY2_A2070ContratoServicosRmn_Inicio, H00RY2_A2069ContratoServicosRmn_Sequencial
               }
               , new Object[] {
               H00RY3_AGRID_nRecordCount
               }
            }
         );
         AV34Pgmname = "CntSrvRemuneracaoWC";
         /* GeneXus formulas. */
         AV34Pgmname = "CntSrvRemuneracaoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A2069ContratoServicosRmn_Sequencial ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicosRmn_Inicio_Titleformat ;
      private short edtContratoServicosRmn_Fim_Titleformat ;
      private short edtContratoServicosRmn_Valor_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContratoServicos_Codigo ;
      private int wcpOAV7ContratoServicos_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratoservicosrmn_inicio_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicosrmn_fim_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicosrmn_valor_Datalistupdateminimumcharacters ;
      private int A160ContratoServicos_Codigo ;
      private int edtContratoServicos_Codigo_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontratoservicosrmn_inicio_Visible ;
      private int edtavTfcontratoservicosrmn_inicio_to_Visible ;
      private int edtavTfcontratoservicosrmn_fim_Visible ;
      private int edtavTfcontratoservicosrmn_fim_to_Visible ;
      private int edtavTfcontratoservicosrmn_valor_Visible ;
      private int edtavTfcontratoservicosrmn_valor_to_Visible ;
      private int edtavDdo_contratoservicosrmn_iniciotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosrmn_fimtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosrmn_valortitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV29PageToGo ;
      private int AV35GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV30GridCurrentPage ;
      private long AV31GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV17TFContratoServicosRmn_Inicio ;
      private decimal AV18TFContratoServicosRmn_Inicio_To ;
      private decimal AV21TFContratoServicosRmn_Fim ;
      private decimal AV22TFContratoServicosRmn_Fim_To ;
      private decimal AV25TFContratoServicosRmn_Valor ;
      private decimal AV26TFContratoServicosRmn_Valor_To ;
      private decimal A2070ContratoServicosRmn_Inicio ;
      private decimal A2071ContratoServicosRmn_Fim ;
      private decimal A2072ContratoServicosRmn_Valor ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicosrmn_inicio_Activeeventkey ;
      private String Ddo_contratoservicosrmn_inicio_Filteredtext_get ;
      private String Ddo_contratoservicosrmn_inicio_Filteredtextto_get ;
      private String Ddo_contratoservicosrmn_fim_Activeeventkey ;
      private String Ddo_contratoservicosrmn_fim_Filteredtext_get ;
      private String Ddo_contratoservicosrmn_fim_Filteredtextto_get ;
      private String Ddo_contratoservicosrmn_valor_Activeeventkey ;
      private String Ddo_contratoservicosrmn_valor_Filteredtext_get ;
      private String Ddo_contratoservicosrmn_valor_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV34Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoservicosrmn_inicio_Caption ;
      private String Ddo_contratoservicosrmn_inicio_Tooltip ;
      private String Ddo_contratoservicosrmn_inicio_Cls ;
      private String Ddo_contratoservicosrmn_inicio_Filteredtext_set ;
      private String Ddo_contratoservicosrmn_inicio_Filteredtextto_set ;
      private String Ddo_contratoservicosrmn_inicio_Dropdownoptionstype ;
      private String Ddo_contratoservicosrmn_inicio_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosrmn_inicio_Sortedstatus ;
      private String Ddo_contratoservicosrmn_inicio_Filtertype ;
      private String Ddo_contratoservicosrmn_inicio_Datalistfixedvalues ;
      private String Ddo_contratoservicosrmn_inicio_Sortasc ;
      private String Ddo_contratoservicosrmn_inicio_Sortdsc ;
      private String Ddo_contratoservicosrmn_inicio_Loadingdata ;
      private String Ddo_contratoservicosrmn_inicio_Cleanfilter ;
      private String Ddo_contratoservicosrmn_inicio_Rangefilterfrom ;
      private String Ddo_contratoservicosrmn_inicio_Rangefilterto ;
      private String Ddo_contratoservicosrmn_inicio_Noresultsfound ;
      private String Ddo_contratoservicosrmn_inicio_Searchbuttontext ;
      private String Ddo_contratoservicosrmn_fim_Caption ;
      private String Ddo_contratoservicosrmn_fim_Tooltip ;
      private String Ddo_contratoservicosrmn_fim_Cls ;
      private String Ddo_contratoservicosrmn_fim_Filteredtext_set ;
      private String Ddo_contratoservicosrmn_fim_Filteredtextto_set ;
      private String Ddo_contratoservicosrmn_fim_Dropdownoptionstype ;
      private String Ddo_contratoservicosrmn_fim_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosrmn_fim_Sortedstatus ;
      private String Ddo_contratoservicosrmn_fim_Filtertype ;
      private String Ddo_contratoservicosrmn_fim_Datalistfixedvalues ;
      private String Ddo_contratoservicosrmn_fim_Sortasc ;
      private String Ddo_contratoservicosrmn_fim_Sortdsc ;
      private String Ddo_contratoservicosrmn_fim_Loadingdata ;
      private String Ddo_contratoservicosrmn_fim_Cleanfilter ;
      private String Ddo_contratoservicosrmn_fim_Rangefilterfrom ;
      private String Ddo_contratoservicosrmn_fim_Rangefilterto ;
      private String Ddo_contratoservicosrmn_fim_Noresultsfound ;
      private String Ddo_contratoservicosrmn_fim_Searchbuttontext ;
      private String Ddo_contratoservicosrmn_valor_Caption ;
      private String Ddo_contratoservicosrmn_valor_Tooltip ;
      private String Ddo_contratoservicosrmn_valor_Cls ;
      private String Ddo_contratoservicosrmn_valor_Filteredtext_set ;
      private String Ddo_contratoservicosrmn_valor_Filteredtextto_set ;
      private String Ddo_contratoservicosrmn_valor_Dropdownoptionstype ;
      private String Ddo_contratoservicosrmn_valor_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosrmn_valor_Sortedstatus ;
      private String Ddo_contratoservicosrmn_valor_Filtertype ;
      private String Ddo_contratoservicosrmn_valor_Datalistfixedvalues ;
      private String Ddo_contratoservicosrmn_valor_Sortasc ;
      private String Ddo_contratoservicosrmn_valor_Sortdsc ;
      private String Ddo_contratoservicosrmn_valor_Loadingdata ;
      private String Ddo_contratoservicosrmn_valor_Cleanfilter ;
      private String Ddo_contratoservicosrmn_valor_Rangefilterfrom ;
      private String Ddo_contratoservicosrmn_valor_Rangefilterto ;
      private String Ddo_contratoservicosrmn_valor_Noresultsfound ;
      private String Ddo_contratoservicosrmn_valor_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContratoServicos_Codigo_Internalname ;
      private String edtContratoServicos_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontratoservicosrmn_inicio_Internalname ;
      private String edtavTfcontratoservicosrmn_inicio_Jsonclick ;
      private String edtavTfcontratoservicosrmn_inicio_to_Internalname ;
      private String edtavTfcontratoservicosrmn_inicio_to_Jsonclick ;
      private String edtavTfcontratoservicosrmn_fim_Internalname ;
      private String edtavTfcontratoservicosrmn_fim_Jsonclick ;
      private String edtavTfcontratoservicosrmn_fim_to_Internalname ;
      private String edtavTfcontratoservicosrmn_fim_to_Jsonclick ;
      private String edtavTfcontratoservicosrmn_valor_Internalname ;
      private String edtavTfcontratoservicosrmn_valor_Jsonclick ;
      private String edtavTfcontratoservicosrmn_valor_to_Internalname ;
      private String edtavTfcontratoservicosrmn_valor_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratoservicosrmn_iniciotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosrmn_fimtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosrmn_valortitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContratoServicosRmn_Sequencial_Internalname ;
      private String edtContratoServicosRmn_Inicio_Internalname ;
      private String edtContratoServicosRmn_Fim_Internalname ;
      private String edtContratoServicosRmn_Valor_Internalname ;
      private String scmdbuf ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicosrmn_inicio_Internalname ;
      private String Ddo_contratoservicosrmn_fim_Internalname ;
      private String Ddo_contratoservicosrmn_valor_Internalname ;
      private String edtContratoServicosRmn_Inicio_Title ;
      private String edtContratoServicosRmn_Fim_Title ;
      private String edtContratoServicosRmn_Valor_Title ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7ContratoServicos_Codigo ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoServicosRmn_Sequencial_Jsonclick ;
      private String edtContratoServicosRmn_Inicio_Jsonclick ;
      private String edtContratoServicosRmn_Fim_Jsonclick ;
      private String edtContratoServicosRmn_Valor_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicosrmn_inicio_Includesortasc ;
      private bool Ddo_contratoservicosrmn_inicio_Includesortdsc ;
      private bool Ddo_contratoservicosrmn_inicio_Includefilter ;
      private bool Ddo_contratoservicosrmn_inicio_Filterisrange ;
      private bool Ddo_contratoservicosrmn_inicio_Includedatalist ;
      private bool Ddo_contratoservicosrmn_fim_Includesortasc ;
      private bool Ddo_contratoservicosrmn_fim_Includesortdsc ;
      private bool Ddo_contratoservicosrmn_fim_Includefilter ;
      private bool Ddo_contratoservicosrmn_fim_Filterisrange ;
      private bool Ddo_contratoservicosrmn_fim_Includedatalist ;
      private bool Ddo_contratoservicosrmn_valor_Includesortasc ;
      private bool Ddo_contratoservicosrmn_valor_Includesortdsc ;
      private bool Ddo_contratoservicosrmn_valor_Includefilter ;
      private bool Ddo_contratoservicosrmn_valor_Filterisrange ;
      private bool Ddo_contratoservicosrmn_valor_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV19ddo_ContratoServicosRmn_InicioTitleControlIdToReplace ;
      private String AV23ddo_ContratoServicosRmn_FimTitleControlIdToReplace ;
      private String AV27ddo_ContratoServicosRmn_ValorTitleControlIdToReplace ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00RY2_A160ContratoServicos_Codigo ;
      private decimal[] H00RY2_A2072ContratoServicosRmn_Valor ;
      private decimal[] H00RY2_A2071ContratoServicosRmn_Fim ;
      private decimal[] H00RY2_A2070ContratoServicosRmn_Inicio ;
      private short[] H00RY2_A2069ContratoServicosRmn_Sequencial ;
      private long[] H00RY3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV16ContratoServicosRmn_InicioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV20ContratoServicosRmn_FimTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV24ContratoServicosRmn_ValorTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV28DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class cntsrvremuneracaowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00RY2( IGxContext context ,
                                             decimal AV17TFContratoServicosRmn_Inicio ,
                                             decimal AV18TFContratoServicosRmn_Inicio_To ,
                                             decimal AV21TFContratoServicosRmn_Fim ,
                                             decimal AV22TFContratoServicosRmn_Fim_To ,
                                             decimal AV25TFContratoServicosRmn_Valor ,
                                             decimal AV26TFContratoServicosRmn_Valor_To ,
                                             decimal A2070ContratoServicosRmn_Inicio ,
                                             decimal A2071ContratoServicosRmn_Fim ,
                                             decimal A2072ContratoServicosRmn_Valor ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A160ContratoServicos_Codigo ,
                                             int AV7ContratoServicos_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ContratoServicos_Codigo], [ContratoServicosRmn_Valor], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Sequencial]";
         sFromString = " FROM [ContratoServicosRmn] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([ContratoServicos_Codigo] = @AV7ContratoServicos_Codigo)";
         if ( ! (Convert.ToDecimal(0)==AV17TFContratoServicosRmn_Inicio) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Inicio] >= @AV17TFContratoServicosRmn_Inicio)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFContratoServicosRmn_Inicio_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Inicio] <= @AV18TFContratoServicosRmn_Inicio_To)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFContratoServicosRmn_Fim) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Fim] >= @AV21TFContratoServicosRmn_Fim)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFContratoServicosRmn_Fim_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Fim] <= @AV22TFContratoServicosRmn_Fim_To)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFContratoServicosRmn_Valor) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Valor] >= @AV25TFContratoServicosRmn_Valor)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFContratoServicosRmn_Valor_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Valor] <= @AV26TFContratoServicosRmn_Valor_To)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicos_Codigo], [ContratoServicosRmn_Inicio]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicos_Codigo] DESC, [ContratoServicosRmn_Inicio] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicos_Codigo], [ContratoServicosRmn_Fim]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicos_Codigo] DESC, [ContratoServicosRmn_Fim] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicos_Codigo], [ContratoServicosRmn_Valor]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicos_Codigo] DESC, [ContratoServicosRmn_Valor] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00RY3( IGxContext context ,
                                             decimal AV17TFContratoServicosRmn_Inicio ,
                                             decimal AV18TFContratoServicosRmn_Inicio_To ,
                                             decimal AV21TFContratoServicosRmn_Fim ,
                                             decimal AV22TFContratoServicosRmn_Fim_To ,
                                             decimal AV25TFContratoServicosRmn_Valor ,
                                             decimal AV26TFContratoServicosRmn_Valor_To ,
                                             decimal A2070ContratoServicosRmn_Inicio ,
                                             decimal A2071ContratoServicosRmn_Fim ,
                                             decimal A2072ContratoServicosRmn_Valor ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A160ContratoServicos_Codigo ,
                                             int AV7ContratoServicos_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContratoServicosRmn] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContratoServicos_Codigo] = @AV7ContratoServicos_Codigo)";
         if ( ! (Convert.ToDecimal(0)==AV17TFContratoServicosRmn_Inicio) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Inicio] >= @AV17TFContratoServicosRmn_Inicio)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFContratoServicosRmn_Inicio_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Inicio] <= @AV18TFContratoServicosRmn_Inicio_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFContratoServicosRmn_Fim) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Fim] >= @AV21TFContratoServicosRmn_Fim)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFContratoServicosRmn_Fim_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Fim] <= @AV22TFContratoServicosRmn_Fim_To)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFContratoServicosRmn_Valor) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Valor] >= @AV25TFContratoServicosRmn_Valor)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFContratoServicosRmn_Valor_To) )
         {
            sWhereString = sWhereString + " and ([ContratoServicosRmn_Valor] <= @AV26TFContratoServicosRmn_Valor_To)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00RY2(context, (decimal)dynConstraints[0] , (decimal)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
               case 1 :
                     return conditional_H00RY3(context, (decimal)dynConstraints[0] , (decimal)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00RY2 ;
          prmH00RY2 = new Object[] {
          new Object[] {"@AV7ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratoServicosRmn_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV18TFContratoServicosRmn_Inicio_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV21TFContratoServicosRmn_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22TFContratoServicosRmn_Fim_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV25TFContratoServicosRmn_Valor",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26TFContratoServicosRmn_Valor_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00RY3 ;
          prmH00RY3 = new Object[] {
          new Object[] {"@AV7ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratoServicosRmn_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV18TFContratoServicosRmn_Inicio_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV21TFContratoServicosRmn_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22TFContratoServicosRmn_Fim_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV25TFContratoServicosRmn_Valor",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26TFContratoServicosRmn_Valor_To",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00RY2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RY2,11,0,true,false )
             ,new CursorDef("H00RY3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RY3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[13]);
                }
                return;
       }
    }

 }

}
