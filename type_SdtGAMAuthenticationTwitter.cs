/*
               File: type_SdtGAMAuthenticationTwitter
        Description: GAMAuthenticationTwitter
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:42.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMAuthenticationTwitter : GxUserType, IGxExternalObject
   {
      public SdtGAMAuthenticationTwitter( )
      {
         initialize();
      }

      public SdtGAMAuthenticationTwitter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMAuthenticationTwitter_externalReference == null )
         {
            GAMAuthenticationTwitter_externalReference = new Artech.Security.GAMAuthenticationTwitter(context);
         }
         returntostring = "";
         returntostring = (String)(GAMAuthenticationTwitter_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Consumerkey
      {
         get {
            if ( GAMAuthenticationTwitter_externalReference == null )
            {
               GAMAuthenticationTwitter_externalReference = new Artech.Security.GAMAuthenticationTwitter(context);
            }
            return GAMAuthenticationTwitter_externalReference.ConsumerKey ;
         }

         set {
            if ( GAMAuthenticationTwitter_externalReference == null )
            {
               GAMAuthenticationTwitter_externalReference = new Artech.Security.GAMAuthenticationTwitter(context);
            }
            GAMAuthenticationTwitter_externalReference.ConsumerKey = value;
         }

      }

      public String gxTpr_Consumersecret
      {
         get {
            if ( GAMAuthenticationTwitter_externalReference == null )
            {
               GAMAuthenticationTwitter_externalReference = new Artech.Security.GAMAuthenticationTwitter(context);
            }
            return GAMAuthenticationTwitter_externalReference.ConsumerSecret ;
         }

         set {
            if ( GAMAuthenticationTwitter_externalReference == null )
            {
               GAMAuthenticationTwitter_externalReference = new Artech.Security.GAMAuthenticationTwitter(context);
            }
            GAMAuthenticationTwitter_externalReference.ConsumerSecret = value;
         }

      }

      public String gxTpr_Callbackurl
      {
         get {
            if ( GAMAuthenticationTwitter_externalReference == null )
            {
               GAMAuthenticationTwitter_externalReference = new Artech.Security.GAMAuthenticationTwitter(context);
            }
            return GAMAuthenticationTwitter_externalReference.CallbackURL ;
         }

         set {
            if ( GAMAuthenticationTwitter_externalReference == null )
            {
               GAMAuthenticationTwitter_externalReference = new Artech.Security.GAMAuthenticationTwitter(context);
            }
            GAMAuthenticationTwitter_externalReference.CallbackURL = value;
         }

      }

      public String gxTpr_Additionalscope
      {
         get {
            if ( GAMAuthenticationTwitter_externalReference == null )
            {
               GAMAuthenticationTwitter_externalReference = new Artech.Security.GAMAuthenticationTwitter(context);
            }
            return GAMAuthenticationTwitter_externalReference.AdditionalScope ;
         }

         set {
            if ( GAMAuthenticationTwitter_externalReference == null )
            {
               GAMAuthenticationTwitter_externalReference = new Artech.Security.GAMAuthenticationTwitter(context);
            }
            GAMAuthenticationTwitter_externalReference.AdditionalScope = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMAuthenticationTwitter_externalReference == null )
            {
               GAMAuthenticationTwitter_externalReference = new Artech.Security.GAMAuthenticationTwitter(context);
            }
            return GAMAuthenticationTwitter_externalReference ;
         }

         set {
            GAMAuthenticationTwitter_externalReference = (Artech.Security.GAMAuthenticationTwitter)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMAuthenticationTwitter GAMAuthenticationTwitter_externalReference=null ;
   }

}
