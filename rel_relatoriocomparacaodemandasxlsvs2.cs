/*
               File: REL_RelatorioComparacaoDemandasXLSVs2
        Description: Stub for REL_RelatorioComparacaoDemandasXLSVs2
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/4/2020 8:24:5.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_relatoriocomparacaodemandasxlsvs2 : GXProcedure
   {
      public rel_relatoriocomparacaodemandasxlsvs2( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_relatoriocomparacaodemandasxlsvs2( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_ContagemResultado_ContratadaCod ,
                           int aP2_ContagemResultado_ServicoGrupo ,
                           int aP3_ContagemResultado_Servico ,
                           int aP4_ContagemResultado_CntadaOsVinc ,
                           int aP5_ContagemResultado_SerGrupoVinc ,
                           int aP6_ContagemResultado_CodSrvVnc ,
                           String aP7_GridStateXML ,
                           out String aP8_Filename ,
                           out String aP9_ErrorMessage )
      {
         this.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV3ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         this.AV4ContagemResultado_ServicoGrupo = aP2_ContagemResultado_ServicoGrupo;
         this.AV5ContagemResultado_Servico = aP3_ContagemResultado_Servico;
         this.AV6ContagemResultado_CntadaOsVinc = aP4_ContagemResultado_CntadaOsVinc;
         this.AV7ContagemResultado_SerGrupoVinc = aP5_ContagemResultado_SerGrupoVinc;
         this.AV8ContagemResultado_CodSrvVnc = aP6_ContagemResultado_CodSrvVnc;
         this.AV9GridStateXML = aP7_GridStateXML;
         this.AV10Filename = "" ;
         this.AV11ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP8_Filename=this.AV10Filename;
         aP9_ErrorMessage=this.AV11ErrorMessage;
      }

      public String executeUdp( int aP0_Contratada_AreaTrabalhoCod ,
                                int aP1_ContagemResultado_ContratadaCod ,
                                int aP2_ContagemResultado_ServicoGrupo ,
                                int aP3_ContagemResultado_Servico ,
                                int aP4_ContagemResultado_CntadaOsVinc ,
                                int aP5_ContagemResultado_SerGrupoVinc ,
                                int aP6_ContagemResultado_CodSrvVnc ,
                                String aP7_GridStateXML ,
                                out String aP8_Filename )
      {
         this.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV3ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         this.AV4ContagemResultado_ServicoGrupo = aP2_ContagemResultado_ServicoGrupo;
         this.AV5ContagemResultado_Servico = aP3_ContagemResultado_Servico;
         this.AV6ContagemResultado_CntadaOsVinc = aP4_ContagemResultado_CntadaOsVinc;
         this.AV7ContagemResultado_SerGrupoVinc = aP5_ContagemResultado_SerGrupoVinc;
         this.AV8ContagemResultado_CodSrvVnc = aP6_ContagemResultado_CodSrvVnc;
         this.AV9GridStateXML = aP7_GridStateXML;
         this.AV10Filename = "" ;
         this.AV11ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP8_Filename=this.AV10Filename;
         aP9_ErrorMessage=this.AV11ErrorMessage;
         return AV11ErrorMessage ;
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_ContagemResultado_ContratadaCod ,
                                 int aP2_ContagemResultado_ServicoGrupo ,
                                 int aP3_ContagemResultado_Servico ,
                                 int aP4_ContagemResultado_CntadaOsVinc ,
                                 int aP5_ContagemResultado_SerGrupoVinc ,
                                 int aP6_ContagemResultado_CodSrvVnc ,
                                 String aP7_GridStateXML ,
                                 out String aP8_Filename ,
                                 out String aP9_ErrorMessage )
      {
         rel_relatoriocomparacaodemandasxlsvs2 objrel_relatoriocomparacaodemandasxlsvs2;
         objrel_relatoriocomparacaodemandasxlsvs2 = new rel_relatoriocomparacaodemandasxlsvs2();
         objrel_relatoriocomparacaodemandasxlsvs2.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objrel_relatoriocomparacaodemandasxlsvs2.AV3ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         objrel_relatoriocomparacaodemandasxlsvs2.AV4ContagemResultado_ServicoGrupo = aP2_ContagemResultado_ServicoGrupo;
         objrel_relatoriocomparacaodemandasxlsvs2.AV5ContagemResultado_Servico = aP3_ContagemResultado_Servico;
         objrel_relatoriocomparacaodemandasxlsvs2.AV6ContagemResultado_CntadaOsVinc = aP4_ContagemResultado_CntadaOsVinc;
         objrel_relatoriocomparacaodemandasxlsvs2.AV7ContagemResultado_SerGrupoVinc = aP5_ContagemResultado_SerGrupoVinc;
         objrel_relatoriocomparacaodemandasxlsvs2.AV8ContagemResultado_CodSrvVnc = aP6_ContagemResultado_CodSrvVnc;
         objrel_relatoriocomparacaodemandasxlsvs2.AV9GridStateXML = aP7_GridStateXML;
         objrel_relatoriocomparacaodemandasxlsvs2.AV10Filename = "" ;
         objrel_relatoriocomparacaodemandasxlsvs2.AV11ErrorMessage = "" ;
         objrel_relatoriocomparacaodemandasxlsvs2.context.SetSubmitInitialConfig(context);
         objrel_relatoriocomparacaodemandasxlsvs2.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_relatoriocomparacaodemandasxlsvs2);
         aP8_Filename=this.AV10Filename;
         aP9_ErrorMessage=this.AV11ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_relatoriocomparacaodemandasxlsvs2)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Contratada_AreaTrabalhoCod,(int)AV3ContagemResultado_ContratadaCod,(int)AV4ContagemResultado_ServicoGrupo,(int)AV5ContagemResultado_Servico,(int)AV6ContagemResultado_CntadaOsVinc,(int)AV7ContagemResultado_SerGrupoVinc,(int)AV8ContagemResultado_CodSrvVnc,(String)AV9GridStateXML,(String)AV10Filename,(String)AV11ErrorMessage} ;
         ClassLoader.Execute("arel_relatoriocomparacaodemandasxlsvs2","GeneXus.Programs.arel_relatoriocomparacaodemandasxlsvs2", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 10 ) )
         {
            AV10Filename = (String)(args[8]) ;
            AV11ErrorMessage = (String)(args[9]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV2Contratada_AreaTrabalhoCod ;
      private int AV3ContagemResultado_ContratadaCod ;
      private int AV4ContagemResultado_ServicoGrupo ;
      private int AV5ContagemResultado_Servico ;
      private int AV6ContagemResultado_CntadaOsVinc ;
      private int AV7ContagemResultado_SerGrupoVinc ;
      private int AV8ContagemResultado_CodSrvVnc ;
      private String AV9GridStateXML ;
      private String AV11ErrorMessage ;
      private String AV10Filename ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
      private String aP8_Filename ;
      private String aP9_ErrorMessage ;
   }

}
