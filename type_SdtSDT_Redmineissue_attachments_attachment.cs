/*
               File: type_SdtSDT_Redmineissue_attachments_attachment
        Description: SDT_Redmineissue
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:4.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Redmineissue.attachments.attachment" )]
   [XmlType(TypeName =  "SDT_Redmineissue.attachments.attachment" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_Redmineissue_attachments_attachment_author ))]
   [Serializable]
   public class SdtSDT_Redmineissue_attachments_attachment : GxUserType
   {
      public SdtSDT_Redmineissue_attachments_attachment( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Filename = "";
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Filesize = "";
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_type = "";
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Description = "";
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_url = "";
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Created_on = "";
      }

      public SdtSDT_Redmineissue_attachments_attachment( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineissue_attachments_attachment deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineissue_attachments_attachment)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineissue_attachments_attachment obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Filename = deserialized.gxTpr_Filename;
         obj.gxTpr_Filesize = deserialized.gxTpr_Filesize;
         obj.gxTpr_Content_type = deserialized.gxTpr_Content_type;
         obj.gxTpr_Description = deserialized.gxTpr_Description;
         obj.gxTpr_Content_url = deserialized.gxTpr_Content_url;
         obj.gxTpr_Author = deserialized.gxTpr_Author;
         obj.gxTpr_Created_on = deserialized.gxTpr_Created_on;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "id") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_attachments_attachment_Id = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "filename") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_attachments_attachment_Filename = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "filesize") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_attachments_attachment_Filesize = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "content_type") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_type = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "description") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_attachments_attachment_Description = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "content_url") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_url = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "author") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_Redmineissue_attachments_attachment_Author == null )
                  {
                     gxTv_SdtSDT_Redmineissue_attachments_attachment_Author = new SdtSDT_Redmineissue_attachments_attachment_author(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_Redmineissue_attachments_attachment_Author.readxml(oReader, "author");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "created_on") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_attachments_attachment_Created_on = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Redmineissue.attachments.attachment";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Redmineissue_attachments_attachment_Id), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("filename", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_attachments_attachment_Filename));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("filesize", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_attachments_attachment_Filesize));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("content_type", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_type));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("description", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_attachments_attachment_Description));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("content_url", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_url));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         if ( gxTv_SdtSDT_Redmineissue_attachments_attachment_Author != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_Redmineissue_attachments_attachment_Author.writexml(oWriter, "author", sNameSpace1);
         }
         oWriter.WriteElement("created_on", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_attachments_attachment_Created_on));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("id", gxTv_SdtSDT_Redmineissue_attachments_attachment_Id, false);
         AddObjectProperty("filename", gxTv_SdtSDT_Redmineissue_attachments_attachment_Filename, false);
         AddObjectProperty("filesize", gxTv_SdtSDT_Redmineissue_attachments_attachment_Filesize, false);
         AddObjectProperty("content_type", gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_type, false);
         AddObjectProperty("description", gxTv_SdtSDT_Redmineissue_attachments_attachment_Description, false);
         AddObjectProperty("content_url", gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_url, false);
         if ( gxTv_SdtSDT_Redmineissue_attachments_attachment_Author != null )
         {
            AddObjectProperty("author", gxTv_SdtSDT_Redmineissue_attachments_attachment_Author, false);
         }
         AddObjectProperty("created_on", gxTv_SdtSDT_Redmineissue_attachments_attachment_Created_on, false);
         return  ;
      }

      [  SoapElement( ElementName = "id" )]
      [  XmlElement( ElementName = "id" , Namespace = ""  )]
      public short gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_Redmineissue_attachments_attachment_Id ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_attachments_attachment_Id = (short)(value);
         }

      }

      [  SoapElement( ElementName = "filename" )]
      [  XmlElement( ElementName = "filename" , Namespace = ""  )]
      public String gxTpr_Filename
      {
         get {
            return gxTv_SdtSDT_Redmineissue_attachments_attachment_Filename ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_attachments_attachment_Filename = (String)(value);
         }

      }

      [  SoapElement( ElementName = "filesize" )]
      [  XmlElement( ElementName = "filesize" , Namespace = ""  )]
      public String gxTpr_Filesize
      {
         get {
            return gxTv_SdtSDT_Redmineissue_attachments_attachment_Filesize ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_attachments_attachment_Filesize = (String)(value);
         }

      }

      [  SoapElement( ElementName = "content_type" )]
      [  XmlElement( ElementName = "content_type" , Namespace = ""  )]
      public String gxTpr_Content_type
      {
         get {
            return gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_type ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_type = (String)(value);
         }

      }

      [  SoapElement( ElementName = "description" )]
      [  XmlElement( ElementName = "description" , Namespace = ""  )]
      public String gxTpr_Description
      {
         get {
            return gxTv_SdtSDT_Redmineissue_attachments_attachment_Description ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_attachments_attachment_Description = (String)(value);
         }

      }

      [  SoapElement( ElementName = "content_url" )]
      [  XmlElement( ElementName = "content_url" , Namespace = ""  )]
      public String gxTpr_Content_url
      {
         get {
            return gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_url ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_url = (String)(value);
         }

      }

      [  SoapElement( ElementName = "author" )]
      [  XmlElement( ElementName = "author"   )]
      public SdtSDT_Redmineissue_attachments_attachment_author gxTpr_Author
      {
         get {
            if ( gxTv_SdtSDT_Redmineissue_attachments_attachment_Author == null )
            {
               gxTv_SdtSDT_Redmineissue_attachments_attachment_Author = new SdtSDT_Redmineissue_attachments_attachment_author(context);
            }
            return gxTv_SdtSDT_Redmineissue_attachments_attachment_Author ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_attachments_attachment_Author = value;
         }

      }

      public void gxTv_SdtSDT_Redmineissue_attachments_attachment_Author_SetNull( )
      {
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Author = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Redmineissue_attachments_attachment_Author_IsNull( )
      {
         if ( gxTv_SdtSDT_Redmineissue_attachments_attachment_Author == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "created_on" )]
      [  XmlElement( ElementName = "created_on" , Namespace = ""  )]
      public String gxTpr_Created_on
      {
         get {
            return gxTv_SdtSDT_Redmineissue_attachments_attachment_Created_on ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_attachments_attachment_Created_on = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Filename = "";
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Filesize = "";
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_type = "";
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Description = "";
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_url = "";
         gxTv_SdtSDT_Redmineissue_attachments_attachment_Created_on = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_Redmineissue_attachments_attachment_Id ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_Redmineissue_attachments_attachment_Filename ;
      protected String gxTv_SdtSDT_Redmineissue_attachments_attachment_Filesize ;
      protected String gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_type ;
      protected String gxTv_SdtSDT_Redmineissue_attachments_attachment_Description ;
      protected String gxTv_SdtSDT_Redmineissue_attachments_attachment_Content_url ;
      protected String gxTv_SdtSDT_Redmineissue_attachments_attachment_Created_on ;
      protected String sTagName ;
      protected SdtSDT_Redmineissue_attachments_attachment_author gxTv_SdtSDT_Redmineissue_attachments_attachment_Author=null ;
   }

   [DataContract(Name = @"SDT_Redmineissue.attachments.attachment", Namespace = "")]
   public class SdtSDT_Redmineissue_attachments_attachment_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineissue_attachments_attachment>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineissue_attachments_attachment_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineissue_attachments_attachment_RESTInterface( SdtSDT_Redmineissue_attachments_attachment psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "id" , Order = 0 )]
      public Nullable<short> gxTpr_Id
      {
         get {
            return sdt.gxTpr_Id ;
         }

         set {
            sdt.gxTpr_Id = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "filename" , Order = 1 )]
      public String gxTpr_Filename
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Filename) ;
         }

         set {
            sdt.gxTpr_Filename = (String)(value);
         }

      }

      [DataMember( Name = "filesize" , Order = 2 )]
      public String gxTpr_Filesize
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Filesize) ;
         }

         set {
            sdt.gxTpr_Filesize = (String)(value);
         }

      }

      [DataMember( Name = "content_type" , Order = 3 )]
      public String gxTpr_Content_type
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Content_type) ;
         }

         set {
            sdt.gxTpr_Content_type = (String)(value);
         }

      }

      [DataMember( Name = "description" , Order = 4 )]
      public String gxTpr_Description
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Description) ;
         }

         set {
            sdt.gxTpr_Description = (String)(value);
         }

      }

      [DataMember( Name = "content_url" , Order = 5 )]
      public String gxTpr_Content_url
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Content_url) ;
         }

         set {
            sdt.gxTpr_Content_url = (String)(value);
         }

      }

      [DataMember( Name = "author" , Order = 6 )]
      public SdtSDT_Redmineissue_attachments_attachment_author_RESTInterface gxTpr_Author
      {
         get {
            return new SdtSDT_Redmineissue_attachments_attachment_author_RESTInterface(sdt.gxTpr_Author) ;
         }

         set {
            sdt.gxTpr_Author = value.sdt;
         }

      }

      [DataMember( Name = "created_on" , Order = 7 )]
      public String gxTpr_Created_on
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Created_on) ;
         }

         set {
            sdt.gxTpr_Created_on = (String)(value);
         }

      }

      public SdtSDT_Redmineissue_attachments_attachment sdt
      {
         get {
            return (SdtSDT_Redmineissue_attachments_attachment)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineissue_attachments_attachment() ;
         }
      }

   }

}
