/*
               File: SistemaDePara_BC
        Description: De Para
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:7:39.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemadepara_bc : GXHttpHandler, IGxSilentTrn
   {
      public sistemadepara_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public sistemadepara_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow3R172( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey3R172( ) ;
         standaloneModal( ) ;
         AddRow3R172( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z127Sistema_Codigo = A127Sistema_Codigo;
               Z1460SistemaDePara_Codigo = A1460SistemaDePara_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_3R0( )
      {
         BeforeValidate3R172( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3R172( ) ;
            }
            else
            {
               CheckExtendedTable3R172( ) ;
               if ( AnyError == 0 )
               {
                  ZM3R172( 3) ;
               }
               CloseExtendedTableCursors3R172( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM3R172( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z1461SistemaDePara_Origem = A1461SistemaDePara_Origem;
            Z1462SistemaDePara_OrigemId = A1462SistemaDePara_OrigemId;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -2 )
         {
            Z1460SistemaDePara_Codigo = A1460SistemaDePara_Codigo;
            Z1461SistemaDePara_Origem = A1461SistemaDePara_Origem;
            Z1462SistemaDePara_OrigemId = A1462SistemaDePara_OrigemId;
            Z1463SistemaDePara_OrigemDsc = A1463SistemaDePara_OrigemDsc;
            Z127Sistema_Codigo = A127Sistema_Codigo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load3R172( )
      {
         /* Using cursor BC003R5 */
         pr_default.execute(3, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound172 = 1;
            A1461SistemaDePara_Origem = BC003R5_A1461SistemaDePara_Origem[0];
            A1462SistemaDePara_OrigemId = BC003R5_A1462SistemaDePara_OrigemId[0];
            A1463SistemaDePara_OrigemDsc = BC003R5_A1463SistemaDePara_OrigemDsc[0];
            n1463SistemaDePara_OrigemDsc = BC003R5_n1463SistemaDePara_OrigemDsc[0];
            ZM3R172( -2) ;
         }
         pr_default.close(3);
         OnLoadActions3R172( ) ;
      }

      protected void OnLoadActions3R172( )
      {
      }

      protected void CheckExtendedTable3R172( )
      {
         standaloneModal( ) ;
         /* Using cursor BC003R4 */
         pr_default.execute(2, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
         }
         pr_default.close(2);
         if ( ! ( ( StringUtil.StrCmp(A1461SistemaDePara_Origem, "R") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Origem fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors3R172( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey3R172( )
      {
         /* Using cursor BC003R6 */
         pr_default.execute(4, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound172 = 1;
         }
         else
         {
            RcdFound172 = 0;
         }
         pr_default.close(4);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC003R3 */
         pr_default.execute(1, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3R172( 2) ;
            RcdFound172 = 1;
            A1460SistemaDePara_Codigo = BC003R3_A1460SistemaDePara_Codigo[0];
            A1461SistemaDePara_Origem = BC003R3_A1461SistemaDePara_Origem[0];
            A1462SistemaDePara_OrigemId = BC003R3_A1462SistemaDePara_OrigemId[0];
            A1463SistemaDePara_OrigemDsc = BC003R3_A1463SistemaDePara_OrigemDsc[0];
            n1463SistemaDePara_OrigemDsc = BC003R3_n1463SistemaDePara_OrigemDsc[0];
            A127Sistema_Codigo = BC003R3_A127Sistema_Codigo[0];
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z1460SistemaDePara_Codigo = A1460SistemaDePara_Codigo;
            sMode172 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load3R172( ) ;
            if ( AnyError == 1 )
            {
               RcdFound172 = 0;
               InitializeNonKey3R172( ) ;
            }
            Gx_mode = sMode172;
         }
         else
         {
            RcdFound172 = 0;
            InitializeNonKey3R172( ) ;
            sMode172 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode172;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3R172( ) ;
         if ( RcdFound172 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_3R0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency3R172( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC003R2 */
            pr_default.execute(0, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SistemaDePara"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1461SistemaDePara_Origem, BC003R2_A1461SistemaDePara_Origem[0]) != 0 ) || ( Z1462SistemaDePara_OrigemId != BC003R2_A1462SistemaDePara_OrigemId[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SistemaDePara"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3R172( )
      {
         BeforeValidate3R172( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3R172( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3R172( 0) ;
            CheckOptimisticConcurrency3R172( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3R172( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3R172( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003R7 */
                     pr_default.execute(5, new Object[] {A1460SistemaDePara_Codigo, A1461SistemaDePara_Origem, A1462SistemaDePara_OrigemId, n1463SistemaDePara_OrigemDsc, A1463SistemaDePara_OrigemDsc, A127Sistema_Codigo});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("SistemaDePara") ;
                     if ( (pr_default.getStatus(5) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3R172( ) ;
            }
            EndLevel3R172( ) ;
         }
         CloseExtendedTableCursors3R172( ) ;
      }

      protected void Update3R172( )
      {
         BeforeValidate3R172( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3R172( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3R172( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3R172( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3R172( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003R8 */
                     pr_default.execute(6, new Object[] {A1461SistemaDePara_Origem, A1462SistemaDePara_OrigemId, n1463SistemaDePara_OrigemDsc, A1463SistemaDePara_OrigemDsc, A127Sistema_Codigo, A1460SistemaDePara_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("SistemaDePara") ;
                     if ( (pr_default.getStatus(6) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SistemaDePara"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3R172( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3R172( ) ;
         }
         CloseExtendedTableCursors3R172( ) ;
      }

      protected void DeferredUpdate3R172( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate3R172( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3R172( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3R172( ) ;
            AfterConfirm3R172( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3R172( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003R9 */
                  pr_default.execute(7, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("SistemaDePara") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode172 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel3R172( ) ;
         Gx_mode = sMode172;
      }

      protected void OnDeleteControls3R172( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel3R172( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3R172( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart3R172( )
      {
         /* Using cursor BC003R10 */
         pr_default.execute(8, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
         RcdFound172 = 0;
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound172 = 1;
            A1460SistemaDePara_Codigo = BC003R10_A1460SistemaDePara_Codigo[0];
            A1461SistemaDePara_Origem = BC003R10_A1461SistemaDePara_Origem[0];
            A1462SistemaDePara_OrigemId = BC003R10_A1462SistemaDePara_OrigemId[0];
            A1463SistemaDePara_OrigemDsc = BC003R10_A1463SistemaDePara_OrigemDsc[0];
            n1463SistemaDePara_OrigemDsc = BC003R10_n1463SistemaDePara_OrigemDsc[0];
            A127Sistema_Codigo = BC003R10_A127Sistema_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext3R172( )
      {
         /* Scan next routine */
         pr_default.readNext(8);
         RcdFound172 = 0;
         ScanKeyLoad3R172( ) ;
      }

      protected void ScanKeyLoad3R172( )
      {
         sMode172 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound172 = 1;
            A1460SistemaDePara_Codigo = BC003R10_A1460SistemaDePara_Codigo[0];
            A1461SistemaDePara_Origem = BC003R10_A1461SistemaDePara_Origem[0];
            A1462SistemaDePara_OrigemId = BC003R10_A1462SistemaDePara_OrigemId[0];
            A1463SistemaDePara_OrigemDsc = BC003R10_A1463SistemaDePara_OrigemDsc[0];
            n1463SistemaDePara_OrigemDsc = BC003R10_n1463SistemaDePara_OrigemDsc[0];
            A127Sistema_Codigo = BC003R10_A127Sistema_Codigo[0];
         }
         Gx_mode = sMode172;
      }

      protected void ScanKeyEnd3R172( )
      {
         pr_default.close(8);
      }

      protected void AfterConfirm3R172( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3R172( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3R172( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3R172( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3R172( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3R172( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3R172( )
      {
      }

      protected void AddRow3R172( )
      {
         VarsToRow172( bcSistemaDePara) ;
      }

      protected void ReadRow3R172( )
      {
         RowToVars172( bcSistemaDePara, 1) ;
      }

      protected void InitializeNonKey3R172( )
      {
         A1461SistemaDePara_Origem = "";
         A1462SistemaDePara_OrigemId = 0;
         A1463SistemaDePara_OrigemDsc = "";
         n1463SistemaDePara_OrigemDsc = false;
         Z1461SistemaDePara_Origem = "";
         Z1462SistemaDePara_OrigemId = 0;
      }

      protected void InitAll3R172( )
      {
         A127Sistema_Codigo = 0;
         A1460SistemaDePara_Codigo = 0;
         InitializeNonKey3R172( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow172( SdtSistemaDePara obj172 )
      {
         obj172.gxTpr_Mode = Gx_mode;
         obj172.gxTpr_Sistemadepara_origem = A1461SistemaDePara_Origem;
         obj172.gxTpr_Sistemadepara_origemid = A1462SistemaDePara_OrigemId;
         obj172.gxTpr_Sistemadepara_origemdsc = A1463SistemaDePara_OrigemDsc;
         obj172.gxTpr_Sistema_codigo = A127Sistema_Codigo;
         obj172.gxTpr_Sistemadepara_codigo = A1460SistemaDePara_Codigo;
         obj172.gxTpr_Sistema_codigo_Z = Z127Sistema_Codigo;
         obj172.gxTpr_Sistemadepara_codigo_Z = Z1460SistemaDePara_Codigo;
         obj172.gxTpr_Sistemadepara_origem_Z = Z1461SistemaDePara_Origem;
         obj172.gxTpr_Sistemadepara_origemid_Z = Z1462SistemaDePara_OrigemId;
         obj172.gxTpr_Sistemadepara_origemdsc_N = (short)(Convert.ToInt16(n1463SistemaDePara_OrigemDsc));
         obj172.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow172( SdtSistemaDePara obj172 )
      {
         obj172.gxTpr_Sistema_codigo = A127Sistema_Codigo;
         obj172.gxTpr_Sistemadepara_codigo = A1460SistemaDePara_Codigo;
         return  ;
      }

      public void RowToVars172( SdtSistemaDePara obj172 ,
                                int forceLoad )
      {
         Gx_mode = obj172.gxTpr_Mode;
         A1461SistemaDePara_Origem = obj172.gxTpr_Sistemadepara_origem;
         A1462SistemaDePara_OrigemId = obj172.gxTpr_Sistemadepara_origemid;
         A1463SistemaDePara_OrigemDsc = obj172.gxTpr_Sistemadepara_origemdsc;
         n1463SistemaDePara_OrigemDsc = false;
         A127Sistema_Codigo = obj172.gxTpr_Sistema_codigo;
         A1460SistemaDePara_Codigo = obj172.gxTpr_Sistemadepara_codigo;
         Z127Sistema_Codigo = obj172.gxTpr_Sistema_codigo_Z;
         Z1460SistemaDePara_Codigo = obj172.gxTpr_Sistemadepara_codigo_Z;
         Z1461SistemaDePara_Origem = obj172.gxTpr_Sistemadepara_origem_Z;
         Z1462SistemaDePara_OrigemId = obj172.gxTpr_Sistemadepara_origemid_Z;
         n1463SistemaDePara_OrigemDsc = (bool)(Convert.ToBoolean(obj172.gxTpr_Sistemadepara_origemdsc_N));
         Gx_mode = obj172.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A127Sistema_Codigo = (int)getParm(obj,0);
         A1460SistemaDePara_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey3R172( ) ;
         ScanKeyStart3R172( ) ;
         if ( RcdFound172 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC003R11 */
            pr_default.execute(9, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
               AnyError = 1;
            }
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z1460SistemaDePara_Codigo = A1460SistemaDePara_Codigo;
         }
         ZM3R172( -2) ;
         OnLoadActions3R172( ) ;
         AddRow3R172( ) ;
         ScanKeyEnd3R172( ) ;
         if ( RcdFound172 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars172( bcSistemaDePara, 0) ;
         ScanKeyStart3R172( ) ;
         if ( RcdFound172 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC003R11 */
            pr_default.execute(9, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
               AnyError = 1;
            }
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z1460SistemaDePara_Codigo = A1460SistemaDePara_Codigo;
         }
         ZM3R172( -2) ;
         OnLoadActions3R172( ) ;
         AddRow3R172( ) ;
         ScanKeyEnd3R172( ) ;
         if ( RcdFound172 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars172( bcSistemaDePara, 0) ;
         nKeyPressed = 1;
         GetKey3R172( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert3R172( ) ;
         }
         else
         {
            if ( RcdFound172 == 1 )
            {
               if ( ( A127Sistema_Codigo != Z127Sistema_Codigo ) || ( A1460SistemaDePara_Codigo != Z1460SistemaDePara_Codigo ) )
               {
                  A127Sistema_Codigo = Z127Sistema_Codigo;
                  A1460SistemaDePara_Codigo = Z1460SistemaDePara_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update3R172( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A127Sistema_Codigo != Z127Sistema_Codigo ) || ( A1460SistemaDePara_Codigo != Z1460SistemaDePara_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3R172( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3R172( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow172( bcSistemaDePara) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars172( bcSistemaDePara, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey3R172( ) ;
         if ( RcdFound172 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A127Sistema_Codigo != Z127Sistema_Codigo ) || ( A1460SistemaDePara_Codigo != Z1460SistemaDePara_Codigo ) )
            {
               A127Sistema_Codigo = Z127Sistema_Codigo;
               A1460SistemaDePara_Codigo = Z1460SistemaDePara_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A127Sistema_Codigo != Z127Sistema_Codigo ) || ( A1460SistemaDePara_Codigo != Z1460SistemaDePara_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         context.RollbackDataStores( "SistemaDePara_BC");
         VarsToRow172( bcSistemaDePara) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcSistemaDePara.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcSistemaDePara.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcSistemaDePara )
         {
            bcSistemaDePara = (SdtSistemaDePara)(sdt);
            if ( StringUtil.StrCmp(bcSistemaDePara.gxTpr_Mode, "") == 0 )
            {
               bcSistemaDePara.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow172( bcSistemaDePara) ;
            }
            else
            {
               RowToVars172( bcSistemaDePara, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcSistemaDePara.gxTpr_Mode, "") == 0 )
            {
               bcSistemaDePara.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars172( bcSistemaDePara, 1) ;
         return  ;
      }

      public SdtSistemaDePara SistemaDePara_BC
      {
         get {
            return bcSistemaDePara ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1461SistemaDePara_Origem = "";
         A1461SistemaDePara_Origem = "";
         Z1463SistemaDePara_OrigemDsc = "";
         A1463SistemaDePara_OrigemDsc = "";
         BC003R5_A1460SistemaDePara_Codigo = new int[1] ;
         BC003R5_A1461SistemaDePara_Origem = new String[] {""} ;
         BC003R5_A1462SistemaDePara_OrigemId = new long[1] ;
         BC003R5_A1463SistemaDePara_OrigemDsc = new String[] {""} ;
         BC003R5_n1463SistemaDePara_OrigemDsc = new bool[] {false} ;
         BC003R5_A127Sistema_Codigo = new int[1] ;
         BC003R4_A127Sistema_Codigo = new int[1] ;
         BC003R6_A127Sistema_Codigo = new int[1] ;
         BC003R6_A1460SistemaDePara_Codigo = new int[1] ;
         BC003R3_A1460SistemaDePara_Codigo = new int[1] ;
         BC003R3_A1461SistemaDePara_Origem = new String[] {""} ;
         BC003R3_A1462SistemaDePara_OrigemId = new long[1] ;
         BC003R3_A1463SistemaDePara_OrigemDsc = new String[] {""} ;
         BC003R3_n1463SistemaDePara_OrigemDsc = new bool[] {false} ;
         BC003R3_A127Sistema_Codigo = new int[1] ;
         sMode172 = "";
         BC003R2_A1460SistemaDePara_Codigo = new int[1] ;
         BC003R2_A1461SistemaDePara_Origem = new String[] {""} ;
         BC003R2_A1462SistemaDePara_OrigemId = new long[1] ;
         BC003R2_A1463SistemaDePara_OrigemDsc = new String[] {""} ;
         BC003R2_n1463SistemaDePara_OrigemDsc = new bool[] {false} ;
         BC003R2_A127Sistema_Codigo = new int[1] ;
         BC003R10_A1460SistemaDePara_Codigo = new int[1] ;
         BC003R10_A1461SistemaDePara_Origem = new String[] {""} ;
         BC003R10_A1462SistemaDePara_OrigemId = new long[1] ;
         BC003R10_A1463SistemaDePara_OrigemDsc = new String[] {""} ;
         BC003R10_n1463SistemaDePara_OrigemDsc = new bool[] {false} ;
         BC003R10_A127Sistema_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC003R11_A127Sistema_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemadepara_bc__default(),
            new Object[][] {
                new Object[] {
               BC003R2_A1460SistemaDePara_Codigo, BC003R2_A1461SistemaDePara_Origem, BC003R2_A1462SistemaDePara_OrigemId, BC003R2_A1463SistemaDePara_OrigemDsc, BC003R2_n1463SistemaDePara_OrigemDsc, BC003R2_A127Sistema_Codigo
               }
               , new Object[] {
               BC003R3_A1460SistemaDePara_Codigo, BC003R3_A1461SistemaDePara_Origem, BC003R3_A1462SistemaDePara_OrigemId, BC003R3_A1463SistemaDePara_OrigemDsc, BC003R3_n1463SistemaDePara_OrigemDsc, BC003R3_A127Sistema_Codigo
               }
               , new Object[] {
               BC003R4_A127Sistema_Codigo
               }
               , new Object[] {
               BC003R5_A1460SistemaDePara_Codigo, BC003R5_A1461SistemaDePara_Origem, BC003R5_A1462SistemaDePara_OrigemId, BC003R5_A1463SistemaDePara_OrigemDsc, BC003R5_n1463SistemaDePara_OrigemDsc, BC003R5_A127Sistema_Codigo
               }
               , new Object[] {
               BC003R6_A127Sistema_Codigo, BC003R6_A1460SistemaDePara_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003R10_A1460SistemaDePara_Codigo, BC003R10_A1461SistemaDePara_Origem, BC003R10_A1462SistemaDePara_OrigemId, BC003R10_A1463SistemaDePara_OrigemDsc, BC003R10_n1463SistemaDePara_OrigemDsc, BC003R10_A127Sistema_Codigo
               }
               , new Object[] {
               BC003R11_A127Sistema_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound172 ;
      private int trnEnded ;
      private int Z127Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private int Z1460SistemaDePara_Codigo ;
      private int A1460SistemaDePara_Codigo ;
      private long Z1462SistemaDePara_OrigemId ;
      private long A1462SistemaDePara_OrigemId ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z1461SistemaDePara_Origem ;
      private String A1461SistemaDePara_Origem ;
      private String sMode172 ;
      private bool n1463SistemaDePara_OrigemDsc ;
      private String Z1463SistemaDePara_OrigemDsc ;
      private String A1463SistemaDePara_OrigemDsc ;
      private SdtSistemaDePara bcSistemaDePara ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC003R5_A1460SistemaDePara_Codigo ;
      private String[] BC003R5_A1461SistemaDePara_Origem ;
      private long[] BC003R5_A1462SistemaDePara_OrigemId ;
      private String[] BC003R5_A1463SistemaDePara_OrigemDsc ;
      private bool[] BC003R5_n1463SistemaDePara_OrigemDsc ;
      private int[] BC003R5_A127Sistema_Codigo ;
      private int[] BC003R4_A127Sistema_Codigo ;
      private int[] BC003R6_A127Sistema_Codigo ;
      private int[] BC003R6_A1460SistemaDePara_Codigo ;
      private int[] BC003R3_A1460SistemaDePara_Codigo ;
      private String[] BC003R3_A1461SistemaDePara_Origem ;
      private long[] BC003R3_A1462SistemaDePara_OrigemId ;
      private String[] BC003R3_A1463SistemaDePara_OrigemDsc ;
      private bool[] BC003R3_n1463SistemaDePara_OrigemDsc ;
      private int[] BC003R3_A127Sistema_Codigo ;
      private int[] BC003R2_A1460SistemaDePara_Codigo ;
      private String[] BC003R2_A1461SistemaDePara_Origem ;
      private long[] BC003R2_A1462SistemaDePara_OrigemId ;
      private String[] BC003R2_A1463SistemaDePara_OrigemDsc ;
      private bool[] BC003R2_n1463SistemaDePara_OrigemDsc ;
      private int[] BC003R2_A127Sistema_Codigo ;
      private int[] BC003R10_A1460SistemaDePara_Codigo ;
      private String[] BC003R10_A1461SistemaDePara_Origem ;
      private long[] BC003R10_A1462SistemaDePara_OrigemId ;
      private String[] BC003R10_A1463SistemaDePara_OrigemDsc ;
      private bool[] BC003R10_n1463SistemaDePara_OrigemDsc ;
      private int[] BC003R10_A127Sistema_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC003R11_A127Sistema_Codigo ;
   }

   public class sistemadepara_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC003R5 ;
          prmBC003R5 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003R4 ;
          prmBC003R4 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003R6 ;
          prmBC003R6 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003R3 ;
          prmBC003R3 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003R2 ;
          prmBC003R2 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003R7 ;
          prmBC003R7 = new Object[] {
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Origem",SqlDbType.Char,2,0} ,
          new Object[] {"@SistemaDePara_OrigemId",SqlDbType.Decimal,18,0} ,
          new Object[] {"@SistemaDePara_OrigemDsc",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003R8 ;
          prmBC003R8 = new Object[] {
          new Object[] {"@SistemaDePara_Origem",SqlDbType.Char,2,0} ,
          new Object[] {"@SistemaDePara_OrigemId",SqlDbType.Decimal,18,0} ,
          new Object[] {"@SistemaDePara_OrigemDsc",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003R9 ;
          prmBC003R9 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003R10 ;
          prmBC003R10 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003R11 ;
          prmBC003R11 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC003R2", "SELECT [SistemaDePara_Codigo], [SistemaDePara_Origem], [SistemaDePara_OrigemId], [SistemaDePara_OrigemDsc], [Sistema_Codigo] FROM [SistemaDePara] WITH (UPDLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaDePara_Codigo] = @SistemaDePara_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003R2,1,0,true,false )
             ,new CursorDef("BC003R3", "SELECT [SistemaDePara_Codigo], [SistemaDePara_Origem], [SistemaDePara_OrigemId], [SistemaDePara_OrigemDsc], [Sistema_Codigo] FROM [SistemaDePara] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaDePara_Codigo] = @SistemaDePara_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003R3,1,0,true,false )
             ,new CursorDef("BC003R4", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003R4,1,0,true,false )
             ,new CursorDef("BC003R5", "SELECT TM1.[SistemaDePara_Codigo], TM1.[SistemaDePara_Origem], TM1.[SistemaDePara_OrigemId], TM1.[SistemaDePara_OrigemDsc], TM1.[Sistema_Codigo] FROM [SistemaDePara] TM1 WITH (NOLOCK) WHERE TM1.[Sistema_Codigo] = @Sistema_Codigo and TM1.[SistemaDePara_Codigo] = @SistemaDePara_Codigo ORDER BY TM1.[Sistema_Codigo], TM1.[SistemaDePara_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003R5,100,0,true,false )
             ,new CursorDef("BC003R6", "SELECT [Sistema_Codigo], [SistemaDePara_Codigo] FROM [SistemaDePara] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaDePara_Codigo] = @SistemaDePara_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003R6,1,0,true,false )
             ,new CursorDef("BC003R7", "INSERT INTO [SistemaDePara]([SistemaDePara_Codigo], [SistemaDePara_Origem], [SistemaDePara_OrigemId], [SistemaDePara_OrigemDsc], [Sistema_Codigo]) VALUES(@SistemaDePara_Codigo, @SistemaDePara_Origem, @SistemaDePara_OrigemId, @SistemaDePara_OrigemDsc, @Sistema_Codigo)", GxErrorMask.GX_NOMASK,prmBC003R7)
             ,new CursorDef("BC003R8", "UPDATE [SistemaDePara] SET [SistemaDePara_Origem]=@SistemaDePara_Origem, [SistemaDePara_OrigemId]=@SistemaDePara_OrigemId, [SistemaDePara_OrigemDsc]=@SistemaDePara_OrigemDsc  WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaDePara_Codigo] = @SistemaDePara_Codigo", GxErrorMask.GX_NOMASK,prmBC003R8)
             ,new CursorDef("BC003R9", "DELETE FROM [SistemaDePara]  WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaDePara_Codigo] = @SistemaDePara_Codigo", GxErrorMask.GX_NOMASK,prmBC003R9)
             ,new CursorDef("BC003R10", "SELECT TM1.[SistemaDePara_Codigo], TM1.[SistemaDePara_Origem], TM1.[SistemaDePara_OrigemId], TM1.[SistemaDePara_OrigemDsc], TM1.[Sistema_Codigo] FROM [SistemaDePara] TM1 WITH (NOLOCK) WHERE TM1.[Sistema_Codigo] = @Sistema_Codigo and TM1.[SistemaDePara_Codigo] = @SistemaDePara_Codigo ORDER BY TM1.[Sistema_Codigo], TM1.[SistemaDePara_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003R10,100,0,true,false )
             ,new CursorDef("BC003R11", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003R11,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (long)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (long)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
