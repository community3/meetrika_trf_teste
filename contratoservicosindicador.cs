/*
               File: ContratoServicosIndicador
        Description: Indicador
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:20.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosindicador : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_28") == 0 )
         {
            A1269ContratoServicosIndicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_28( A1269ContratoServicosIndicador_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_26") == 0 )
         {
            A1270ContratoServicosIndicador_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_26( A1270ContratoServicosIndicador_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_27") == 0 )
         {
            A1296ContratoServicosIndicador_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1296ContratoServicosIndicador_ContratoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1296ContratoServicosIndicador_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_27( A1296ContratoServicosIndicador_ContratoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridlevel_faixas") == 0 )
         {
            nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_80_idx = GetNextPar( );
            AV15Label = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLabel_Internalname, AV15Label);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLABEL"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( AV15Label, ""))));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridlevel_faixas_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoServicosIndicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOSINDICADOR_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosIndicador_Codigo), "ZZZZZ9")));
               AV14ContratoServicosIndicador_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ContratoServicosIndicador_CntSrvCod), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContratoServicosIndicador_Tipo.Name = "CONTRATOSERVICOSINDICADOR_TIPO";
         cmbContratoServicosIndicador_Tipo.WebTags = "";
         cmbContratoServicosIndicador_Tipo.addItem("", "(Nenhum)", 0);
         cmbContratoServicosIndicador_Tipo.addItem("P", "Pontualidade (demanda)", 0);
         cmbContratoServicosIndicador_Tipo.addItem("PL", "Frequ�ncia de atrasos (lote)", 0);
         cmbContratoServicosIndicador_Tipo.addItem("D", "Diverg�ncias (demanda)", 0);
         cmbContratoServicosIndicador_Tipo.addItem("FD", "Frequ�ncia de diverg�ncias (lote)", 0);
         cmbContratoServicosIndicador_Tipo.addItem("FP", "Frequ�ncia de pend�ncias (�ndice de rejei��o de demandas) (lote)", 0);
         cmbContratoServicosIndicador_Tipo.addItem("AC", "Ajustes de contagens (qtd) (lote)", 0);
         cmbContratoServicosIndicador_Tipo.addItem("AV", "Ajustes de contagens (soma do valor bruto) (lote)", 0);
         cmbContratoServicosIndicador_Tipo.addItem("T", "Tempestividade", 0);
         if ( cmbContratoServicosIndicador_Tipo.ItemCount > 0 )
         {
            A1308ContratoServicosIndicador_Tipo = cmbContratoServicosIndicador_Tipo.getValidValue(A1308ContratoServicosIndicador_Tipo);
            n1308ContratoServicosIndicador_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1308ContratoServicosIndicador_Tipo", A1308ContratoServicosIndicador_Tipo);
         }
         cmbContratoServicosIndicador_Periodicidade.Name = "CONTRATOSERVICOSINDICADOR_PERIODICIDADE";
         cmbContratoServicosIndicador_Periodicidade.WebTags = "";
         cmbContratoServicosIndicador_Periodicidade.addItem("M", "Mensal", 0);
         if ( cmbContratoServicosIndicador_Periodicidade.ItemCount > 0 )
         {
            A1309ContratoServicosIndicador_Periodicidade = cmbContratoServicosIndicador_Periodicidade.getValidValue(A1309ContratoServicosIndicador_Periodicidade);
            n1309ContratoServicosIndicador_Periodicidade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1309ContratoServicosIndicador_Periodicidade", A1309ContratoServicosIndicador_Periodicidade);
         }
         cmbContratoServicosIndicador_CalculoSob.Name = "CONTRATOSERVICOSINDICADOR_CALCULOSOB";
         cmbContratoServicosIndicador_CalculoSob.WebTags = "";
         cmbContratoServicosIndicador_CalculoSob.addItem("", "(Nenhum)", 0);
         cmbContratoServicosIndicador_CalculoSob.addItem("D", "Dias", 0);
         cmbContratoServicosIndicador_CalculoSob.addItem("P", "Percentuais", 0);
         if ( cmbContratoServicosIndicador_CalculoSob.ItemCount > 0 )
         {
            A1345ContratoServicosIndicador_CalculoSob = cmbContratoServicosIndicador_CalculoSob.getValidValue(A1345ContratoServicosIndicador_CalculoSob);
            n1345ContratoServicosIndicador_CalculoSob = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
         }
         cmbContratoServicosIndicador_Formato.Name = "CONTRATOSERVICOSINDICADOR_FORMATO";
         cmbContratoServicosIndicador_Formato.WebTags = "";
         cmbContratoServicosIndicador_Formato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Porcentagem", 0);
         cmbContratoServicosIndicador_Formato.addItem("1", "Numeral", 0);
         if ( cmbContratoServicosIndicador_Formato.ItemCount > 0 )
         {
            A2052ContratoServicosIndicador_Formato = (short)(NumberUtil.Val( cmbContratoServicosIndicador_Formato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Indicador", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoServicosIndicador_Numero_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoservicosindicador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicosindicador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoServicosIndicador_Codigo ,
                           ref int aP2_ContratoServicosIndicador_CntSrvCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoServicosIndicador_Codigo = aP1_ContratoServicosIndicador_Codigo;
         this.AV14ContratoServicosIndicador_CntSrvCod = aP2_ContratoServicosIndicador_CntSrvCod;
         executePrivate();
         aP2_ContratoServicosIndicador_CntSrvCod=this.AV14ContratoServicosIndicador_CntSrvCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContratoServicosIndicador_Tipo = new GXCombobox();
         cmbContratoServicosIndicador_Periodicidade = new GXCombobox();
         cmbContratoServicosIndicador_CalculoSob = new GXCombobox();
         cmbContratoServicosIndicador_Formato = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoServicosIndicador_Tipo.ItemCount > 0 )
         {
            A1308ContratoServicosIndicador_Tipo = cmbContratoServicosIndicador_Tipo.getValidValue(A1308ContratoServicosIndicador_Tipo);
            n1308ContratoServicosIndicador_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1308ContratoServicosIndicador_Tipo", A1308ContratoServicosIndicador_Tipo);
         }
         if ( cmbContratoServicosIndicador_Periodicidade.ItemCount > 0 )
         {
            A1309ContratoServicosIndicador_Periodicidade = cmbContratoServicosIndicador_Periodicidade.getValidValue(A1309ContratoServicosIndicador_Periodicidade);
            n1309ContratoServicosIndicador_Periodicidade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1309ContratoServicosIndicador_Periodicidade", A1309ContratoServicosIndicador_Periodicidade);
         }
         if ( cmbContratoServicosIndicador_CalculoSob.ItemCount > 0 )
         {
            A1345ContratoServicosIndicador_CalculoSob = cmbContratoServicosIndicador_CalculoSob.getValidValue(A1345ContratoServicosIndicador_CalculoSob);
            n1345ContratoServicosIndicador_CalculoSob = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
         }
         if ( cmbContratoServicosIndicador_Formato.ItemCount > 0 )
         {
            A2052ContratoServicosIndicador_Formato = (short)(NumberUtil.Val( cmbContratoServicosIndicador_Formato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3H154( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3H154e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ",", "")), ((edtContratoServicosIndicador_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosIndicador_Codigo_Visible, edtContratoServicosIndicador_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosIndicador.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_CntSrvCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosIndicador_CntSrvCod_Visible, edtContratoServicosIndicador_CntSrvCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosIndicador.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3H154( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3H154( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3H154e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_92_3H154( true) ;
         }
         return  ;
      }

      protected void wb_table3_92_3H154e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3H154e( true) ;
         }
         else
         {
            wb_table1_2_3H154e( false) ;
         }
      }

      protected void wb_table3_92_3H154( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_92_3H154e( true) ;
         }
         else
         {
            wb_table3_92_3H154e( false) ;
         }
      }

      protected void wb_table2_5_3H154( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXUITABSPANEL_TABSContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TitleTabDados"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Indicador") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TabDados"+"\" style=\"display:none;\">") ;
            wb_table4_14_3H154( true) ;
         }
         return  ;
      }

      protected void wb_table4_14_3H154e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TitleTabFaixas"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Faixas") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TabFaixas"+"\" style=\"display:none;\">") ;
            wb_table5_72_3H154( true) ;
         }
         return  ;
      }

      protected void wb_table5_72_3H154e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3H154e( true) ;
         }
         else
         {
            wb_table2_5_3H154e( false) ;
         }
      }

      protected void wb_table5_72_3H154( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_formato_Internalname, "Formato", "", "", lblTextblockcontratoservicosindicador_formato_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"9\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosIndicador_Formato, cmbContratoServicosIndicador_Formato_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)), 1, cmbContratoServicosIndicador_Formato_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContratoServicosIndicador_Formato.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", "", true, "HLP_ContratoServicosIndicador.htm");
            cmbContratoServicosIndicador_Formato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_Formato_Internalname, "Values", (String)(cmbContratoServicosIndicador_Formato.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"10\" >") ;
            /*  Grid Control  */
            Gridlevel_faixasContainer.AddObjectProperty("GridName", "Gridlevel_faixas");
            Gridlevel_faixasContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
            Gridlevel_faixasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Gridlevel_faixasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Gridlevel_faixasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_faixas_Backcolorstyle), 1, 0, ".", "")));
            Gridlevel_faixasContainer.AddObjectProperty("CmpContext", "");
            Gridlevel_faixasContainer.AddObjectProperty("InMasterPage", "false");
            Gridlevel_faixasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_faixasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ".", "")));
            Gridlevel_faixasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Codigo_Enabled), 5, 0, ".", "")));
            Gridlevel_faixasContainer.AddColumnProperties(Gridlevel_faixasColumn);
            Gridlevel_faixasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_faixasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ".", "")));
            Gridlevel_faixasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Numero_Enabled), 5, 0, ".", "")));
            Gridlevel_faixasContainer.AddColumnProperties(Gridlevel_faixasColumn);
            Gridlevel_faixasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_faixasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ".", "")));
            Gridlevel_faixasColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Desde_Title));
            Gridlevel_faixasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Desde_Enabled), 5, 0, ".", "")));
            Gridlevel_faixasContainer.AddColumnProperties(Gridlevel_faixasColumn);
            Gridlevel_faixasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_faixasColumn.AddObjectProperty("Value", StringUtil.RTrim( AV15Label));
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            Gridlevel_faixasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLabel_Enabled), 5, 0, ".", "")));
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            Gridlevel_faixasContainer.AddColumnProperties(Gridlevel_faixasColumn);
            Gridlevel_faixasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_faixasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ".", "")));
            Gridlevel_faixasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Ate_Enabled), 5, 0, ".", "")));
            Gridlevel_faixasContainer.AddColumnProperties(Gridlevel_faixasColumn);
            Gridlevel_faixasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridlevel_faixasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1303ContratoServicosIndicadorFaixa_Reduz, 8, 2, ".", "")));
            Gridlevel_faixasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Reduz_Enabled), 5, 0, ".", "")));
            Gridlevel_faixasContainer.AddColumnProperties(Gridlevel_faixasColumn);
            Gridlevel_faixasContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_faixas_Allowselection), 1, 0, ".", "")));
            Gridlevel_faixasContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_faixas_Selectioncolor), 9, 0, ".", "")));
            Gridlevel_faixasContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_faixas_Allowhovering), 1, 0, ".", "")));
            Gridlevel_faixasContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_faixas_Hoveringcolor), 9, 0, ".", "")));
            Gridlevel_faixasContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_faixas_Allowcollapsing), 1, 0, ".", "")));
            Gridlevel_faixasContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridlevel_faixas_Collapsed), 1, 0, ".", "")));
            nGXsfl_80_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount159 = 10;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_159 = 1;
                  ScanStart3H159( ) ;
                  while ( RcdFound159 != 0 )
                  {
                     init_level_properties159( ) ;
                     getByPrimaryKey3H159( ) ;
                     AddRow3H159( ) ;
                     ScanNext3H159( ) ;
                  }
                  ScanEnd3H159( ) ;
                  nBlankRcdCount159 = 10;
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               B1298ContratoServicosIndicador_QtdeFaixas = A1298ContratoServicosIndicador_QtdeFaixas;
               n1298ContratoServicosIndicador_QtdeFaixas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
               standaloneNotModal3H159( ) ;
               standaloneModal3H159( ) ;
               sMode159 = Gx_mode;
               while ( nGXsfl_80_idx < nRC_GXsfl_80 )
               {
                  ReadRow3H159( ) ;
                  edtContratoServicosIndicadorFaixa_Codigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_CODIGO_"+sGXsfl_80_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Codigo_Enabled), 5, 0)));
                  edtContratoServicosIndicadorFaixa_Numero_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_NUMERO_"+sGXsfl_80_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Numero_Enabled), 5, 0)));
                  edtContratoServicosIndicadorFaixa_Desde_Title = cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_idx+"Title");
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Desde_Internalname, "Title", edtContratoServicosIndicadorFaixa_Desde_Title);
                  edtContratoServicosIndicadorFaixa_Desde_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Desde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Desde_Enabled), 5, 0)));
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  edtavLabel_Enabled = (int)(context.localUtil.CToN( cgiGet( "vLABEL_"+sGXsfl_80_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLabel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLabel_Enabled), 5, 0)));
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  /* * Property isDeleted not supported in */
                  edtContratoServicosIndicadorFaixa_Ate_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_ATE_"+sGXsfl_80_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Ate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Ate_Enabled), 5, 0)));
                  edtContratoServicosIndicadorFaixa_Reduz_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_REDUZ_"+sGXsfl_80_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Reduz_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Reduz_Enabled), 5, 0)));
                  if ( ( nRcdExists_159 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     standaloneModal3H159( ) ;
                  }
                  SendRow3H159( ) ;
               }
               Gx_mode = sMode159;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A1298ContratoServicosIndicador_QtdeFaixas = B1298ContratoServicosIndicador_QtdeFaixas;
               n1298ContratoServicosIndicador_QtdeFaixas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount159 = 10;
               nRcdExists_159 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart3H159( ) ;
                  while ( RcdFound159 != 0 )
                  {
                     sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_80159( ) ;
                     init_level_properties159( ) ;
                     standaloneNotModal3H159( ) ;
                     getByPrimaryKey3H159( ) ;
                     standaloneModal3H159( ) ;
                     AddRow3H159( ) ;
                     ScanNext3H159( ) ;
                  }
                  ScanEnd3H159( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
            {
               sMode159 = Gx_mode;
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx+1), 4, 0)), 4, "0");
               SubsflControlProps_80159( ) ;
               InitAll3H159( ) ;
               init_level_properties159( ) ;
               B1298ContratoServicosIndicador_QtdeFaixas = A1298ContratoServicosIndicador_QtdeFaixas;
               n1298ContratoServicosIndicador_QtdeFaixas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
               standaloneNotModal3H159( ) ;
               standaloneModal3H159( ) ;
               nRcdExists_159 = 0;
               nIsMod_159 = 0;
               nRcdDeleted_159 = 0;
               nBlankRcdCount159 = (short)(nBlankRcdUsr159+nBlankRcdCount159);
               fRowAdded = 0;
               while ( nBlankRcdCount159 > 0 )
               {
                  AddRow3H159( ) ;
                  if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
                  {
                     fRowAdded = 1;
                     GX_FocusControl = edtContratoServicosIndicadorFaixa_Numero_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  nBlankRcdCount159 = (short)(nBlankRcdCount159-1);
               }
               Gx_mode = sMode159;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A1298ContratoServicosIndicador_QtdeFaixas = B1298ContratoServicosIndicador_QtdeFaixas;
               n1298ContratoServicosIndicador_QtdeFaixas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            sStyleString = "";
            context.WriteHtmlText( "<div id=\""+"Gridlevel_faixasContainer"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridlevel_faixas", Gridlevel_faixasContainer);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridlevel_faixasContainerData", Gridlevel_faixasContainer.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridlevel_faixasContainerData"+"V", Gridlevel_faixasContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridlevel_faixasContainerData"+"V"+"\" value='"+Gridlevel_faixasContainer.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_72_3H154e( true) ;
         }
         else
         {
            wb_table5_72_3H154e( false) ;
         }
      }

      protected void wb_table4_14_3H154( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_17_3H154( true) ;
         }
         return  ;
      }

      protected void wb_table6_17_3H154e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_14_3H154e( true) ;
         }
         else
         {
            wb_table4_14_3H154e( false) ;
         }
      }

      protected void wb_table6_17_3H154( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_numero_Internalname, "N�mero", "", "", lblTextblockcontratoservicosindicador_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_Numero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0, ",", "")), ((edtContratoServicosIndicador_Numero_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9")) : context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosIndicador_Numero_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_tipo_Internalname, "Tipo de Indicador", "", "", lblTextblockcontratoservicosindicador_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosIndicador_Tipo, cmbContratoServicosIndicador_Tipo_Internalname, StringUtil.RTrim( A1308ContratoServicosIndicador_Tipo), 1, cmbContratoServicosIndicador_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoServicosIndicador_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "", true, "HLP_ContratoServicosIndicador.htm");
            cmbContratoServicosIndicador_Tipo.CurrentValue = StringUtil.RTrim( A1308ContratoServicosIndicador_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_Tipo_Internalname, "Values", (String)(cmbContratoServicosIndicador_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_indicador_Internalname, "Indicador", "", "", lblTextblockcontratoservicosindicador_indicador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosIndicador_Indicador_Internalname, A1274ContratoServicosIndicador_Indicador, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", 0, 1, edtContratoServicosIndicador_Indicador_Enabled, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_sigla_Internalname, "Sigla", "", "", lblTextblockcontratoservicosindicador_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_Sigla_Internalname, StringUtil.RTrim( A2051ContratoServicosIndicador_Sigla), StringUtil.RTrim( context.localUtil.Format( A2051ContratoServicosIndicador_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosIndicador_Sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_finalidade_Internalname, "Finalidade", "", "", lblTextblockcontratoservicosindicador_finalidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosIndicador_Finalidade_Internalname, A1305ContratoServicosIndicador_Finalidade, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", 0, 1, edtContratoServicosIndicador_Finalidade_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_meta_Internalname, "Meta a cumprir", "", "", lblTextblockcontratoservicosindicador_meta_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosIndicador_Meta_Internalname, A1306ContratoServicosIndicador_Meta, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", 0, 1, edtContratoServicosIndicador_Meta_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_instrumentomedicao_Internalname, "Instrumento de Medi��o", "", "", lblTextblockcontratoservicosindicador_instrumentomedicao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosIndicador_InstrumentoMedicao_Internalname, A1307ContratoServicosIndicador_InstrumentoMedicao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, 1, edtContratoServicosIndicador_InstrumentoMedicao_Enabled, 0, 100, "%", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_periodicidade_Internalname, "Periodicidade", "", "", lblTextblockcontratoservicosindicador_periodicidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosIndicador_Periodicidade, cmbContratoServicosIndicador_Periodicidade_Internalname, StringUtil.RTrim( A1309ContratoServicosIndicador_Periodicidade), 1, cmbContratoServicosIndicador_Periodicidade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoServicosIndicador_Periodicidade.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_ContratoServicosIndicador.htm");
            cmbContratoServicosIndicador_Periodicidade.CurrentValue = StringUtil.RTrim( A1309ContratoServicosIndicador_Periodicidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_Periodicidade_Internalname, "Values", (String)(cmbContratoServicosIndicador_Periodicidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_calculosob_Internalname, "C�lculo Sob", "", "", lblTextblockcontratoservicosindicador_calculosob_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosIndicador_CalculoSob, cmbContratoServicosIndicador_CalculoSob_Internalname, StringUtil.RTrim( A1345ContratoServicosIndicador_CalculoSob), 1, cmbContratoServicosIndicador_CalculoSob_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e113h154_client"+"'", "char", "", 1, cmbContratoServicosIndicador_CalculoSob.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_ContratoServicosIndicador.htm");
            cmbContratoServicosIndicador_CalculoSob.CurrentValue = StringUtil.RTrim( A1345ContratoServicosIndicador_CalculoSob);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_CalculoSob_Internalname, "Values", (String)(cmbContratoServicosIndicador_CalculoSob.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_vigencia_Internalname, "Vig�ncia", "", "", lblTextblockcontratoservicosindicador_vigencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_Vigencia_Internalname, A1310ContratoServicosIndicador_Vigencia, StringUtil.RTrim( context.localUtil.Format( A1310ContratoServicosIndicador_Vigencia, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_Vigencia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosIndicador_Vigencia_Enabled, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContratoServicosIndicador.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_17_3H154e( true) ;
         }
         else
         {
            wb_table6_17_3H154e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E123H2 */
         E123H2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Numero_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Numero_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSINDICADOR_NUMERO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosIndicador_Numero_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1271ContratoServicosIndicador_Numero = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1271ContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0)));
               }
               else
               {
                  A1271ContratoServicosIndicador_Numero = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Numero_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1271ContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0)));
               }
               cmbContratoServicosIndicador_Tipo.Name = cmbContratoServicosIndicador_Tipo_Internalname;
               cmbContratoServicosIndicador_Tipo.CurrentValue = cgiGet( cmbContratoServicosIndicador_Tipo_Internalname);
               A1308ContratoServicosIndicador_Tipo = cgiGet( cmbContratoServicosIndicador_Tipo_Internalname);
               n1308ContratoServicosIndicador_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1308ContratoServicosIndicador_Tipo", A1308ContratoServicosIndicador_Tipo);
               n1308ContratoServicosIndicador_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A1308ContratoServicosIndicador_Tipo)) ? true : false);
               A1274ContratoServicosIndicador_Indicador = cgiGet( edtContratoServicosIndicador_Indicador_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1274ContratoServicosIndicador_Indicador", A1274ContratoServicosIndicador_Indicador);
               A2051ContratoServicosIndicador_Sigla = StringUtil.Upper( cgiGet( edtContratoServicosIndicador_Sigla_Internalname));
               n2051ContratoServicosIndicador_Sigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2051ContratoServicosIndicador_Sigla", A2051ContratoServicosIndicador_Sigla);
               n2051ContratoServicosIndicador_Sigla = (String.IsNullOrEmpty(StringUtil.RTrim( A2051ContratoServicosIndicador_Sigla)) ? true : false);
               A1305ContratoServicosIndicador_Finalidade = cgiGet( edtContratoServicosIndicador_Finalidade_Internalname);
               n1305ContratoServicosIndicador_Finalidade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1305ContratoServicosIndicador_Finalidade", A1305ContratoServicosIndicador_Finalidade);
               n1305ContratoServicosIndicador_Finalidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1305ContratoServicosIndicador_Finalidade)) ? true : false);
               A1306ContratoServicosIndicador_Meta = cgiGet( edtContratoServicosIndicador_Meta_Internalname);
               n1306ContratoServicosIndicador_Meta = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1306ContratoServicosIndicador_Meta", A1306ContratoServicosIndicador_Meta);
               n1306ContratoServicosIndicador_Meta = (String.IsNullOrEmpty(StringUtil.RTrim( A1306ContratoServicosIndicador_Meta)) ? true : false);
               A1307ContratoServicosIndicador_InstrumentoMedicao = cgiGet( edtContratoServicosIndicador_InstrumentoMedicao_Internalname);
               n1307ContratoServicosIndicador_InstrumentoMedicao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1307ContratoServicosIndicador_InstrumentoMedicao", A1307ContratoServicosIndicador_InstrumentoMedicao);
               n1307ContratoServicosIndicador_InstrumentoMedicao = (String.IsNullOrEmpty(StringUtil.RTrim( A1307ContratoServicosIndicador_InstrumentoMedicao)) ? true : false);
               cmbContratoServicosIndicador_Periodicidade.Name = cmbContratoServicosIndicador_Periodicidade_Internalname;
               cmbContratoServicosIndicador_Periodicidade.CurrentValue = cgiGet( cmbContratoServicosIndicador_Periodicidade_Internalname);
               A1309ContratoServicosIndicador_Periodicidade = cgiGet( cmbContratoServicosIndicador_Periodicidade_Internalname);
               n1309ContratoServicosIndicador_Periodicidade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1309ContratoServicosIndicador_Periodicidade", A1309ContratoServicosIndicador_Periodicidade);
               n1309ContratoServicosIndicador_Periodicidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1309ContratoServicosIndicador_Periodicidade)) ? true : false);
               cmbContratoServicosIndicador_CalculoSob.Name = cmbContratoServicosIndicador_CalculoSob_Internalname;
               cmbContratoServicosIndicador_CalculoSob.CurrentValue = cgiGet( cmbContratoServicosIndicador_CalculoSob_Internalname);
               A1345ContratoServicosIndicador_CalculoSob = cgiGet( cmbContratoServicosIndicador_CalculoSob_Internalname);
               n1345ContratoServicosIndicador_CalculoSob = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
               n1345ContratoServicosIndicador_CalculoSob = (String.IsNullOrEmpty(StringUtil.RTrim( A1345ContratoServicosIndicador_CalculoSob)) ? true : false);
               A1310ContratoServicosIndicador_Vigencia = StringUtil.Upper( cgiGet( edtContratoServicosIndicador_Vigencia_Internalname));
               n1310ContratoServicosIndicador_Vigencia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1310ContratoServicosIndicador_Vigencia", A1310ContratoServicosIndicador_Vigencia);
               n1310ContratoServicosIndicador_Vigencia = (String.IsNullOrEmpty(StringUtil.RTrim( A1310ContratoServicosIndicador_Vigencia)) ? true : false);
               cmbContratoServicosIndicador_Formato.Name = cmbContratoServicosIndicador_Formato_Internalname;
               cmbContratoServicosIndicador_Formato.CurrentValue = cgiGet( cmbContratoServicosIndicador_Formato_Internalname);
               A2052ContratoServicosIndicador_Formato = (short)(NumberUtil.Val( cgiGet( cmbContratoServicosIndicador_Formato_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
               A1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_CntSrvCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_CntSrvCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSINDICADOR_CNTSRVCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosIndicador_CntSrvCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1270ContratoServicosIndicador_CntSrvCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
               }
               else
               {
                  A1270ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_CntSrvCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
               }
               /* Read saved values. */
               Z1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1269ContratoServicosIndicador_Codigo"), ",", "."));
               Z1271ContratoServicosIndicador_Numero = (short)(context.localUtil.CToN( cgiGet( "Z1271ContratoServicosIndicador_Numero"), ",", "."));
               Z2051ContratoServicosIndicador_Sigla = cgiGet( "Z2051ContratoServicosIndicador_Sigla");
               n2051ContratoServicosIndicador_Sigla = (String.IsNullOrEmpty(StringUtil.RTrim( A2051ContratoServicosIndicador_Sigla)) ? true : false);
               Z1308ContratoServicosIndicador_Tipo = cgiGet( "Z1308ContratoServicosIndicador_Tipo");
               n1308ContratoServicosIndicador_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A1308ContratoServicosIndicador_Tipo)) ? true : false);
               Z1309ContratoServicosIndicador_Periodicidade = cgiGet( "Z1309ContratoServicosIndicador_Periodicidade");
               n1309ContratoServicosIndicador_Periodicidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1309ContratoServicosIndicador_Periodicidade)) ? true : false);
               Z1310ContratoServicosIndicador_Vigencia = cgiGet( "Z1310ContratoServicosIndicador_Vigencia");
               n1310ContratoServicosIndicador_Vigencia = (String.IsNullOrEmpty(StringUtil.RTrim( A1310ContratoServicosIndicador_Vigencia)) ? true : false);
               Z1345ContratoServicosIndicador_CalculoSob = cgiGet( "Z1345ContratoServicosIndicador_CalculoSob");
               n1345ContratoServicosIndicador_CalculoSob = (String.IsNullOrEmpty(StringUtil.RTrim( A1345ContratoServicosIndicador_CalculoSob)) ? true : false);
               Z2052ContratoServicosIndicador_Formato = (short)(context.localUtil.CToN( cgiGet( "Z2052ContratoServicosIndicador_Formato"), ",", "."));
               Z1270ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "Z1270ContratoServicosIndicador_CntSrvCod"), ",", "."));
               O1298ContratoServicosIndicador_QtdeFaixas = (short)(context.localUtil.CToN( cgiGet( "O1298ContratoServicosIndicador_QtdeFaixas"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_80"), ",", "."));
               N1270ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "N1270ContratoServicosIndicador_CntSrvCod"), ",", "."));
               A1298ContratoServicosIndicador_QtdeFaixas = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADOR_QTDEFAIXAS"), ",", "."));
               A1395ContratoServicosIndicador_ComQtdeFaixas = cgiGet( "CONTRATOSERVICOSINDICADOR_COMQTDEFAIXAS");
               AV7ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOSINDICADOR_CODIGO"), ",", "."));
               AV11Insert_ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATOSERVICOSINDICADOR_CNTSRVCOD"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV16AuditingObject);
               A1296ContratoServicosIndicador_ContratoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADOR_CONTRATOCOD"), ",", "."));
               A1295ContratoServicosIndicador_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD"), ",", "."));
               n1295ContratoServicosIndicador_AreaTrabalhoCod = false;
               AV18Pgmname = cgiGet( "vPGMNAME");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               A1304ContratoServicosIndicadorFaixa_Und = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_UND"), ",", "."));
               Gxuitabspanel_tabs_Width = cgiGet( "GXUITABSPANEL_TABS_Width");
               Gxuitabspanel_tabs_Height = cgiGet( "GXUITABSPANEL_TABS_Height");
               Gxuitabspanel_tabs_Cls = cgiGet( "GXUITABSPANEL_TABS_Cls");
               Gxuitabspanel_tabs_Enabled = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Enabled"));
               Gxuitabspanel_tabs_Class = cgiGet( "GXUITABSPANEL_TABS_Class");
               Gxuitabspanel_tabs_Autowidth = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Autowidth"));
               Gxuitabspanel_tabs_Autoheight = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Autoheight"));
               Gxuitabspanel_tabs_Autoscroll = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Autoscroll"));
               Gxuitabspanel_tabs_Activetabid = cgiGet( "GXUITABSPANEL_TABS_Activetabid");
               Gxuitabspanel_tabs_Designtimetabs = cgiGet( "GXUITABSPANEL_TABS_Designtimetabs");
               Gxuitabspanel_tabs_Selectedtabindex = (int)(context.localUtil.CToN( cgiGet( "GXUITABSPANEL_TABS_Selectedtabindex"), ",", "."));
               Gxuitabspanel_tabs_Visible = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoServicosIndicador";
               A1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV18Pgmname, ""));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1269ContratoServicosIndicador_Codigo != Z1269ContratoServicosIndicador_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[SecurityCheckFailed value for]"+"ContratoServicosIndicador_Codigo:"+context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contratoservicosindicador:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratoservicosindicador:[SecurityCheckFailed value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV18Pgmname, "")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1269ContratoServicosIndicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode154 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode154;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound154 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3H0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATOSERVICOSINDICADOR_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContratoServicosIndicador_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123H2 */
                           E123H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E133H2 */
                           E133H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E143H2 */
                           E143H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E133H2 */
            E133H2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3H154( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3H154( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3H0( )
      {
         BeforeValidate3H154( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3H154( ) ;
            }
            else
            {
               CheckExtendedTable3H154( ) ;
               CloseExtendedTableCursors3H154( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode154 = Gx_mode;
            CONFIRM_3H159( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode154;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               IsConfirmed = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
            }
            /* Restore parent mode. */
            Gx_mode = sMode154;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void CONFIRM_3H159( )
      {
         s1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
         n1298ContratoServicosIndicador_QtdeFaixas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         nGXsfl_80_idx = 0;
         while ( nGXsfl_80_idx < nRC_GXsfl_80 )
         {
            ReadRow3H159( ) ;
            if ( ( nRcdExists_159 != 0 ) || ( nIsMod_159 != 0 ) )
            {
               GetKey3H159( ) ;
               if ( ( nRcdExists_159 == 0 ) && ( nRcdDeleted_159 == 0 ) )
               {
                  if ( RcdFound159 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     BeforeValidate3H159( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable3H159( ) ;
                        CloseExtendedTableCursors3H159( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                        O1298ContratoServicosIndicador_QtdeFaixas = A1298ContratoServicosIndicador_QtdeFaixas;
                        n1298ContratoServicosIndicador_QtdeFaixas = false;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound159 != 0 )
                  {
                     if ( nRcdDeleted_159 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        getByPrimaryKey3H159( ) ;
                        Load3H159( ) ;
                        BeforeValidate3H159( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls3H159( ) ;
                           O1298ContratoServicosIndicador_QtdeFaixas = A1298ContratoServicosIndicador_QtdeFaixas;
                           n1298ContratoServicosIndicador_QtdeFaixas = false;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
                        }
                     }
                     else
                     {
                        if ( nIsMod_159 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           BeforeValidate3H159( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable3H159( ) ;
                              CloseExtendedTableCursors3H159( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                              O1298ContratoServicosIndicador_QtdeFaixas = A1298ContratoServicosIndicador_QtdeFaixas;
                              n1298ContratoServicosIndicador_QtdeFaixas = false;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_159 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            ChangePostValue( edtContratoServicosIndicadorFaixa_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( edtContratoServicosIndicadorFaixa_Numero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ",", ""))) ;
            ChangePostValue( edtContratoServicosIndicadorFaixa_Desde_Internalname, StringUtil.LTrim( StringUtil.NToC( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ",", ""))) ;
            ChangePostValue( edtavLabel_Internalname, StringUtil.RTrim( AV15Label)) ;
            ChangePostValue( edtContratoServicosIndicadorFaixa_Ate_Internalname, StringUtil.LTrim( StringUtil.NToC( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ",", ""))) ;
            ChangePostValue( edtContratoServicosIndicadorFaixa_Reduz_Internalname, StringUtil.LTrim( StringUtil.NToC( A1303ContratoServicosIndicadorFaixa_Reduz, 8, 2, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1299ContratoServicosIndicadorFaixa_Codigo_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1304ContratoServicosIndicadorFaixa_Und_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1304ContratoServicosIndicadorFaixa_Und), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1303ContratoServicosIndicadorFaixa_Reduz_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( Z1303ContratoServicosIndicadorFaixa_Reduz, 6, 2, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1302ContratoServicosIndicadorFaixa_Ate_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( Z1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1301ContratoServicosIndicadorFaixa_Desde_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( Z1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1300ContratoServicosIndicadorFaixa_Numero_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_159_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_159), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_159_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_159), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_159_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_159), 4, 0, ",", ""))) ;
            if ( nIsMod_159 != 0 )
            {
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_CODIGO_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Codigo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_NUMERO_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Numero_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_idx+"Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Desde_Title)) ;
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Desde_Enabled), 5, 0, ".", ""))) ;
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               ChangePostValue( "vLABEL_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLabel_Enabled), 5, 0, ".", ""))) ;
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_ATE_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Ate_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_REDUZ_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Reduz_Enabled), 5, 0, ".", ""))) ;
            }
         }
         O1298ContratoServicosIndicador_QtdeFaixas = s1298ContratoServicosIndicador_QtdeFaixas;
         n1298ContratoServicosIndicador_QtdeFaixas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption3H0( )
      {
      }

      protected void E123H2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV18Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV19GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GXV1), 8, 0)));
            while ( AV19GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV19GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ContratoServicosIndicador_CntSrvCod") == 0 )
               {
                  AV11Insert_ContratoServicosIndicador_CntSrvCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContratoServicosIndicador_CntSrvCod), 6, 0)));
               }
               AV19GXV1 = (int)(AV19GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GXV1), 8, 0)));
            }
         }
         edtContratoServicosIndicador_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Codigo_Visible), 5, 0)));
         edtContratoServicosIndicador_CntSrvCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_CntSrvCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_CntSrvCod_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\"> document.getElementById(\"GXUITABSPANEL_TABSContainer\").setAttribute(\"class\",\"gx_usercontrol tab-container\");</script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         AV10WebSession.Remove("Aba");
         this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABSContainer", "SelectTab", "", new Object[] {AV10WebSession.Get("Aba")});
         AV15Label = " at� ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLabel_Internalname, AV15Label);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLABEL"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( AV15Label, ""))));
      }

      protected void E133H2( )
      {
         /* After Trn Routine */
         AV10WebSession.Remove("Aba");
         new wwpbaseobjects.audittransaction(context ).execute(  AV16AuditingObject,  AV18Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Pgmname", AV18Pgmname);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratoservicosindicador.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV14ContratoServicosIndicador_CntSrvCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E143H2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV14ContratoServicosIndicador_CntSrvCod) + "," + UrlEncode(StringUtil.RTrim("Indicadores"));
         context.wjLocDisableFrm = 1;
      }

      protected void ZM3H154( short GX_JID )
      {
         if ( ( GX_JID == 25 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1271ContratoServicosIndicador_Numero = T003H5_A1271ContratoServicosIndicador_Numero[0];
               Z2051ContratoServicosIndicador_Sigla = T003H5_A2051ContratoServicosIndicador_Sigla[0];
               Z1308ContratoServicosIndicador_Tipo = T003H5_A1308ContratoServicosIndicador_Tipo[0];
               Z1309ContratoServicosIndicador_Periodicidade = T003H5_A1309ContratoServicosIndicador_Periodicidade[0];
               Z1310ContratoServicosIndicador_Vigencia = T003H5_A1310ContratoServicosIndicador_Vigencia[0];
               Z1345ContratoServicosIndicador_CalculoSob = T003H5_A1345ContratoServicosIndicador_CalculoSob[0];
               Z2052ContratoServicosIndicador_Formato = T003H5_A2052ContratoServicosIndicador_Formato[0];
               Z1270ContratoServicosIndicador_CntSrvCod = T003H5_A1270ContratoServicosIndicador_CntSrvCod[0];
            }
            else
            {
               Z1271ContratoServicosIndicador_Numero = A1271ContratoServicosIndicador_Numero;
               Z2051ContratoServicosIndicador_Sigla = A2051ContratoServicosIndicador_Sigla;
               Z1308ContratoServicosIndicador_Tipo = A1308ContratoServicosIndicador_Tipo;
               Z1309ContratoServicosIndicador_Periodicidade = A1309ContratoServicosIndicador_Periodicidade;
               Z1310ContratoServicosIndicador_Vigencia = A1310ContratoServicosIndicador_Vigencia;
               Z1345ContratoServicosIndicador_CalculoSob = A1345ContratoServicosIndicador_CalculoSob;
               Z2052ContratoServicosIndicador_Formato = A2052ContratoServicosIndicador_Formato;
               Z1270ContratoServicosIndicador_CntSrvCod = A1270ContratoServicosIndicador_CntSrvCod;
            }
         }
         if ( GX_JID == -25 )
         {
            Z1269ContratoServicosIndicador_Codigo = A1269ContratoServicosIndicador_Codigo;
            Z1271ContratoServicosIndicador_Numero = A1271ContratoServicosIndicador_Numero;
            Z1274ContratoServicosIndicador_Indicador = A1274ContratoServicosIndicador_Indicador;
            Z2051ContratoServicosIndicador_Sigla = A2051ContratoServicosIndicador_Sigla;
            Z1305ContratoServicosIndicador_Finalidade = A1305ContratoServicosIndicador_Finalidade;
            Z1306ContratoServicosIndicador_Meta = A1306ContratoServicosIndicador_Meta;
            Z1307ContratoServicosIndicador_InstrumentoMedicao = A1307ContratoServicosIndicador_InstrumentoMedicao;
            Z1308ContratoServicosIndicador_Tipo = A1308ContratoServicosIndicador_Tipo;
            Z1309ContratoServicosIndicador_Periodicidade = A1309ContratoServicosIndicador_Periodicidade;
            Z1310ContratoServicosIndicador_Vigencia = A1310ContratoServicosIndicador_Vigencia;
            Z1345ContratoServicosIndicador_CalculoSob = A1345ContratoServicosIndicador_CalculoSob;
            Z2052ContratoServicosIndicador_Formato = A2052ContratoServicosIndicador_Formato;
            Z1270ContratoServicosIndicador_CntSrvCod = A1270ContratoServicosIndicador_CntSrvCod;
            Z1298ContratoServicosIndicador_QtdeFaixas = A1298ContratoServicosIndicador_QtdeFaixas;
            Z1296ContratoServicosIndicador_ContratoCod = A1296ContratoServicosIndicador_ContratoCod;
            Z1295ContratoServicosIndicador_AreaTrabalhoCod = A1295ContratoServicosIndicador_AreaTrabalhoCod;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContratoServicosIndicador_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Codigo_Enabled), 5, 0)));
         edtavLabel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLabel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLabel_Enabled), 5, 0)));
         AV18Pgmname = "ContratoServicosIndicador";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Pgmname", AV18Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtContratoServicosIndicador_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoServicosIndicador_Codigo) )
         {
            A1269ContratoServicosIndicador_Codigo = AV7ContratoServicosIndicador_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContratoServicosIndicador_CntSrvCod) )
         {
            edtContratoServicosIndicador_CntSrvCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_CntSrvCod_Enabled), 5, 0)));
         }
         else
         {
            edtContratoServicosIndicador_CntSrvCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_CntSrvCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContratoServicosIndicador_CntSrvCod) )
         {
            A1270ContratoServicosIndicador_CntSrvCod = AV11Insert_ContratoServicosIndicador_CntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003H9 */
            pr_default.execute(6, new Object[] {A1269ContratoServicosIndicador_Codigo});
            if ( (pr_default.getStatus(6) != 101) )
            {
               A1298ContratoServicosIndicador_QtdeFaixas = T003H9_A1298ContratoServicosIndicador_QtdeFaixas[0];
               n1298ContratoServicosIndicador_QtdeFaixas = T003H9_n1298ContratoServicosIndicador_QtdeFaixas[0];
            }
            else
            {
               A1298ContratoServicosIndicador_QtdeFaixas = 0;
               n1298ContratoServicosIndicador_QtdeFaixas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            O1298ContratoServicosIndicador_QtdeFaixas = A1298ContratoServicosIndicador_QtdeFaixas;
            n1298ContratoServicosIndicador_QtdeFaixas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
            pr_default.close(6);
            /* Using cursor T003H6 */
            pr_default.execute(4, new Object[] {A1270ContratoServicosIndicador_CntSrvCod});
            A1296ContratoServicosIndicador_ContratoCod = T003H6_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = T003H6_n1296ContratoServicosIndicador_ContratoCod[0];
            pr_default.close(4);
            /* Using cursor T003H7 */
            pr_default.execute(5, new Object[] {n1296ContratoServicosIndicador_ContratoCod, A1296ContratoServicosIndicador_ContratoCod});
            A1295ContratoServicosIndicador_AreaTrabalhoCod = T003H7_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = T003H7_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            pr_default.close(5);
         }
      }

      protected void Load3H154( )
      {
         /* Using cursor T003H11 */
         pr_default.execute(7, new Object[] {A1269ContratoServicosIndicador_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound154 = 1;
            A1271ContratoServicosIndicador_Numero = T003H11_A1271ContratoServicosIndicador_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1271ContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0)));
            A1274ContratoServicosIndicador_Indicador = T003H11_A1274ContratoServicosIndicador_Indicador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1274ContratoServicosIndicador_Indicador", A1274ContratoServicosIndicador_Indicador);
            A2051ContratoServicosIndicador_Sigla = T003H11_A2051ContratoServicosIndicador_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2051ContratoServicosIndicador_Sigla", A2051ContratoServicosIndicador_Sigla);
            n2051ContratoServicosIndicador_Sigla = T003H11_n2051ContratoServicosIndicador_Sigla[0];
            A1305ContratoServicosIndicador_Finalidade = T003H11_A1305ContratoServicosIndicador_Finalidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1305ContratoServicosIndicador_Finalidade", A1305ContratoServicosIndicador_Finalidade);
            n1305ContratoServicosIndicador_Finalidade = T003H11_n1305ContratoServicosIndicador_Finalidade[0];
            A1306ContratoServicosIndicador_Meta = T003H11_A1306ContratoServicosIndicador_Meta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1306ContratoServicosIndicador_Meta", A1306ContratoServicosIndicador_Meta);
            n1306ContratoServicosIndicador_Meta = T003H11_n1306ContratoServicosIndicador_Meta[0];
            A1307ContratoServicosIndicador_InstrumentoMedicao = T003H11_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1307ContratoServicosIndicador_InstrumentoMedicao", A1307ContratoServicosIndicador_InstrumentoMedicao);
            n1307ContratoServicosIndicador_InstrumentoMedicao = T003H11_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
            A1308ContratoServicosIndicador_Tipo = T003H11_A1308ContratoServicosIndicador_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1308ContratoServicosIndicador_Tipo", A1308ContratoServicosIndicador_Tipo);
            n1308ContratoServicosIndicador_Tipo = T003H11_n1308ContratoServicosIndicador_Tipo[0];
            A1309ContratoServicosIndicador_Periodicidade = T003H11_A1309ContratoServicosIndicador_Periodicidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1309ContratoServicosIndicador_Periodicidade", A1309ContratoServicosIndicador_Periodicidade);
            n1309ContratoServicosIndicador_Periodicidade = T003H11_n1309ContratoServicosIndicador_Periodicidade[0];
            A1310ContratoServicosIndicador_Vigencia = T003H11_A1310ContratoServicosIndicador_Vigencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1310ContratoServicosIndicador_Vigencia", A1310ContratoServicosIndicador_Vigencia);
            n1310ContratoServicosIndicador_Vigencia = T003H11_n1310ContratoServicosIndicador_Vigencia[0];
            A1345ContratoServicosIndicador_CalculoSob = T003H11_A1345ContratoServicosIndicador_CalculoSob[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
            n1345ContratoServicosIndicador_CalculoSob = T003H11_n1345ContratoServicosIndicador_CalculoSob[0];
            A2052ContratoServicosIndicador_Formato = T003H11_A2052ContratoServicosIndicador_Formato[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
            A1270ContratoServicosIndicador_CntSrvCod = T003H11_A1270ContratoServicosIndicador_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
            A1296ContratoServicosIndicador_ContratoCod = T003H11_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = T003H11_n1296ContratoServicosIndicador_ContratoCod[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = T003H11_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = T003H11_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1298ContratoServicosIndicador_QtdeFaixas = T003H11_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = T003H11_n1298ContratoServicosIndicador_QtdeFaixas[0];
            ZM3H154( -25) ;
         }
         pr_default.close(7);
         OnLoadActions3H154( ) ;
      }

      protected void OnLoadActions3H154( )
      {
         O1298ContratoServicosIndicador_QtdeFaixas = A1298ContratoServicosIndicador_QtdeFaixas;
         n1298ContratoServicosIndicador_QtdeFaixas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         A1395ContratoServicosIndicador_ComQtdeFaixas = StringUtil.Substring( A1274ContratoServicosIndicador_Indicador, 1, 30) + "(" + StringUtil.Trim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)) + ")";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1395ContratoServicosIndicador_ComQtdeFaixas", A1395ContratoServicosIndicador_ComQtdeFaixas);
         edtContratoServicosIndicadorFaixa_Desde_Title = ((StringUtil.StrCmp(A1345ContratoServicosIndicador_CalculoSob, "D")==0) ? "Dias" : "Percentuais");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Desde_Internalname, "Title", edtContratoServicosIndicadorFaixa_Desde_Title);
      }

      protected void CheckExtendedTable3H154( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T003H9 */
         pr_default.execute(6, new Object[] {A1269ContratoServicosIndicador_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            A1298ContratoServicosIndicador_QtdeFaixas = T003H9_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = T003H9_n1298ContratoServicosIndicador_QtdeFaixas[0];
         }
         else
         {
            A1298ContratoServicosIndicador_QtdeFaixas = 0;
            n1298ContratoServicosIndicador_QtdeFaixas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         }
         pr_default.close(6);
         A1395ContratoServicosIndicador_ComQtdeFaixas = StringUtil.Substring( A1274ContratoServicosIndicador_Indicador, 1, 30) + "(" + StringUtil.Trim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)) + ")";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1395ContratoServicosIndicador_ComQtdeFaixas", A1395ContratoServicosIndicador_ComQtdeFaixas);
         /* Using cursor T003H6 */
         pr_default.execute(4, new Object[] {A1270ContratoServicosIndicador_CntSrvCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Indicador_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSINDICADOR_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosIndicador_CntSrvCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1296ContratoServicosIndicador_ContratoCod = T003H6_A1296ContratoServicosIndicador_ContratoCod[0];
         n1296ContratoServicosIndicador_ContratoCod = T003H6_n1296ContratoServicosIndicador_ContratoCod[0];
         pr_default.close(4);
         /* Using cursor T003H7 */
         pr_default.execute(5, new Object[] {n1296ContratoServicosIndicador_ContratoCod, A1296ContratoServicosIndicador_ContratoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Indicador_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1295ContratoServicosIndicador_AreaTrabalhoCod = T003H7_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
         n1295ContratoServicosIndicador_AreaTrabalhoCod = T003H7_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
         pr_default.close(5);
         edtContratoServicosIndicadorFaixa_Desde_Title = ((StringUtil.StrCmp(A1345ContratoServicosIndicador_CalculoSob, "D")==0) ? "Dias" : "Percentuais");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Desde_Internalname, "Title", edtContratoServicosIndicadorFaixa_Desde_Title);
      }

      protected void CloseExtendedTableCursors3H154( )
      {
         pr_default.close(6);
         pr_default.close(4);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_28( int A1269ContratoServicosIndicador_Codigo )
      {
         /* Using cursor T003H13 */
         pr_default.execute(8, new Object[] {A1269ContratoServicosIndicador_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            A1298ContratoServicosIndicador_QtdeFaixas = T003H13_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = T003H13_n1298ContratoServicosIndicador_QtdeFaixas[0];
         }
         else
         {
            A1298ContratoServicosIndicador_QtdeFaixas = 0;
            n1298ContratoServicosIndicador_QtdeFaixas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_26( int A1270ContratoServicosIndicador_CntSrvCod )
      {
         /* Using cursor T003H14 */
         pr_default.execute(9, new Object[] {A1270ContratoServicosIndicador_CntSrvCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Indicador_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSINDICADOR_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosIndicador_CntSrvCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1296ContratoServicosIndicador_ContratoCod = T003H14_A1296ContratoServicosIndicador_ContratoCod[0];
         n1296ContratoServicosIndicador_ContratoCod = T003H14_n1296ContratoServicosIndicador_ContratoCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_27( int A1296ContratoServicosIndicador_ContratoCod )
      {
         /* Using cursor T003H15 */
         pr_default.execute(10, new Object[] {n1296ContratoServicosIndicador_ContratoCod, A1296ContratoServicosIndicador_ContratoCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Indicador_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1295ContratoServicosIndicador_AreaTrabalhoCod = T003H15_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
         n1295ContratoServicosIndicador_AreaTrabalhoCod = T003H15_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void GetKey3H154( )
      {
         /* Using cursor T003H16 */
         pr_default.execute(11, new Object[] {A1269ContratoServicosIndicador_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound154 = 1;
         }
         else
         {
            RcdFound154 = 0;
         }
         pr_default.close(11);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003H5 */
         pr_default.execute(3, new Object[] {A1269ContratoServicosIndicador_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            ZM3H154( 25) ;
            RcdFound154 = 1;
            A1269ContratoServicosIndicador_Codigo = T003H5_A1269ContratoServicosIndicador_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
            A1271ContratoServicosIndicador_Numero = T003H5_A1271ContratoServicosIndicador_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1271ContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0)));
            A1274ContratoServicosIndicador_Indicador = T003H5_A1274ContratoServicosIndicador_Indicador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1274ContratoServicosIndicador_Indicador", A1274ContratoServicosIndicador_Indicador);
            A2051ContratoServicosIndicador_Sigla = T003H5_A2051ContratoServicosIndicador_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2051ContratoServicosIndicador_Sigla", A2051ContratoServicosIndicador_Sigla);
            n2051ContratoServicosIndicador_Sigla = T003H5_n2051ContratoServicosIndicador_Sigla[0];
            A1305ContratoServicosIndicador_Finalidade = T003H5_A1305ContratoServicosIndicador_Finalidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1305ContratoServicosIndicador_Finalidade", A1305ContratoServicosIndicador_Finalidade);
            n1305ContratoServicosIndicador_Finalidade = T003H5_n1305ContratoServicosIndicador_Finalidade[0];
            A1306ContratoServicosIndicador_Meta = T003H5_A1306ContratoServicosIndicador_Meta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1306ContratoServicosIndicador_Meta", A1306ContratoServicosIndicador_Meta);
            n1306ContratoServicosIndicador_Meta = T003H5_n1306ContratoServicosIndicador_Meta[0];
            A1307ContratoServicosIndicador_InstrumentoMedicao = T003H5_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1307ContratoServicosIndicador_InstrumentoMedicao", A1307ContratoServicosIndicador_InstrumentoMedicao);
            n1307ContratoServicosIndicador_InstrumentoMedicao = T003H5_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
            A1308ContratoServicosIndicador_Tipo = T003H5_A1308ContratoServicosIndicador_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1308ContratoServicosIndicador_Tipo", A1308ContratoServicosIndicador_Tipo);
            n1308ContratoServicosIndicador_Tipo = T003H5_n1308ContratoServicosIndicador_Tipo[0];
            A1309ContratoServicosIndicador_Periodicidade = T003H5_A1309ContratoServicosIndicador_Periodicidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1309ContratoServicosIndicador_Periodicidade", A1309ContratoServicosIndicador_Periodicidade);
            n1309ContratoServicosIndicador_Periodicidade = T003H5_n1309ContratoServicosIndicador_Periodicidade[0];
            A1310ContratoServicosIndicador_Vigencia = T003H5_A1310ContratoServicosIndicador_Vigencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1310ContratoServicosIndicador_Vigencia", A1310ContratoServicosIndicador_Vigencia);
            n1310ContratoServicosIndicador_Vigencia = T003H5_n1310ContratoServicosIndicador_Vigencia[0];
            A1345ContratoServicosIndicador_CalculoSob = T003H5_A1345ContratoServicosIndicador_CalculoSob[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
            n1345ContratoServicosIndicador_CalculoSob = T003H5_n1345ContratoServicosIndicador_CalculoSob[0];
            A2052ContratoServicosIndicador_Formato = T003H5_A2052ContratoServicosIndicador_Formato[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
            A1270ContratoServicosIndicador_CntSrvCod = T003H5_A1270ContratoServicosIndicador_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
            Z1269ContratoServicosIndicador_Codigo = A1269ContratoServicosIndicador_Codigo;
            sMode154 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3H154( ) ;
            if ( AnyError == 1 )
            {
               RcdFound154 = 0;
               InitializeNonKey3H154( ) ;
            }
            Gx_mode = sMode154;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound154 = 0;
            InitializeNonKey3H154( ) ;
            sMode154 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode154;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(3);
      }

      protected void getEqualNoModal( )
      {
         GetKey3H154( ) ;
         if ( RcdFound154 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound154 = 0;
         /* Using cursor T003H17 */
         pr_default.execute(12, new Object[] {A1269ContratoServicosIndicador_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T003H17_A1269ContratoServicosIndicador_Codigo[0] < A1269ContratoServicosIndicador_Codigo ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T003H17_A1269ContratoServicosIndicador_Codigo[0] > A1269ContratoServicosIndicador_Codigo ) ) )
            {
               A1269ContratoServicosIndicador_Codigo = T003H17_A1269ContratoServicosIndicador_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
               RcdFound154 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void move_previous( )
      {
         RcdFound154 = 0;
         /* Using cursor T003H18 */
         pr_default.execute(13, new Object[] {A1269ContratoServicosIndicador_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T003H18_A1269ContratoServicosIndicador_Codigo[0] > A1269ContratoServicosIndicador_Codigo ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T003H18_A1269ContratoServicosIndicador_Codigo[0] < A1269ContratoServicosIndicador_Codigo ) ) )
            {
               A1269ContratoServicosIndicador_Codigo = T003H18_A1269ContratoServicosIndicador_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
               RcdFound154 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3H154( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
            n1298ContratoServicosIndicador_QtdeFaixas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
            GX_FocusControl = edtContratoServicosIndicador_Numero_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3H154( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound154 == 1 )
            {
               if ( A1269ContratoServicosIndicador_Codigo != Z1269ContratoServicosIndicador_Codigo )
               {
                  A1269ContratoServicosIndicador_Codigo = Z1269ContratoServicosIndicador_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOSERVICOSINDICADOR_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosIndicador_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
                  n1298ContratoServicosIndicador_QtdeFaixas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoServicosIndicador_Numero_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  A1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
                  n1298ContratoServicosIndicador_QtdeFaixas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
                  Update3H154( ) ;
                  GX_FocusControl = edtContratoServicosIndicador_Numero_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1269ContratoServicosIndicador_Codigo != Z1269ContratoServicosIndicador_Codigo )
               {
                  /* Insert record */
                  A1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
                  n1298ContratoServicosIndicador_QtdeFaixas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
                  GX_FocusControl = edtContratoServicosIndicador_Numero_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3H154( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOSINDICADOR_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContratoServicosIndicador_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     A1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
                     n1298ContratoServicosIndicador_QtdeFaixas = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
                     GX_FocusControl = edtContratoServicosIndicador_Numero_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3H154( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1269ContratoServicosIndicador_Codigo != Z1269ContratoServicosIndicador_Codigo )
         {
            A1269ContratoServicosIndicador_Codigo = Z1269ContratoServicosIndicador_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOSERVICOSINDICADOR_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosIndicador_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            A1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
            n1298ContratoServicosIndicador_QtdeFaixas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoServicosIndicador_Numero_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3H154( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003H4 */
            pr_default.execute(2, new Object[] {A1269ContratoServicosIndicador_Codigo});
            if ( (pr_default.getStatus(2) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosIndicador"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(2) == 101) || ( Z1271ContratoServicosIndicador_Numero != T003H4_A1271ContratoServicosIndicador_Numero[0] ) || ( StringUtil.StrCmp(Z2051ContratoServicosIndicador_Sigla, T003H4_A2051ContratoServicosIndicador_Sigla[0]) != 0 ) || ( StringUtil.StrCmp(Z1308ContratoServicosIndicador_Tipo, T003H4_A1308ContratoServicosIndicador_Tipo[0]) != 0 ) || ( StringUtil.StrCmp(Z1309ContratoServicosIndicador_Periodicidade, T003H4_A1309ContratoServicosIndicador_Periodicidade[0]) != 0 ) || ( StringUtil.StrCmp(Z1310ContratoServicosIndicador_Vigencia, T003H4_A1310ContratoServicosIndicador_Vigencia[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1345ContratoServicosIndicador_CalculoSob, T003H4_A1345ContratoServicosIndicador_CalculoSob[0]) != 0 ) || ( Z2052ContratoServicosIndicador_Formato != T003H4_A2052ContratoServicosIndicador_Formato[0] ) || ( Z1270ContratoServicosIndicador_CntSrvCod != T003H4_A1270ContratoServicosIndicador_CntSrvCod[0] ) )
            {
               if ( Z1271ContratoServicosIndicador_Numero != T003H4_A1271ContratoServicosIndicador_Numero[0] )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicador_Numero");
                  GXUtil.WriteLogRaw("Old: ",Z1271ContratoServicosIndicador_Numero);
                  GXUtil.WriteLogRaw("Current: ",T003H4_A1271ContratoServicosIndicador_Numero[0]);
               }
               if ( StringUtil.StrCmp(Z2051ContratoServicosIndicador_Sigla, T003H4_A2051ContratoServicosIndicador_Sigla[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicador_Sigla");
                  GXUtil.WriteLogRaw("Old: ",Z2051ContratoServicosIndicador_Sigla);
                  GXUtil.WriteLogRaw("Current: ",T003H4_A2051ContratoServicosIndicador_Sigla[0]);
               }
               if ( StringUtil.StrCmp(Z1308ContratoServicosIndicador_Tipo, T003H4_A1308ContratoServicosIndicador_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicador_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z1308ContratoServicosIndicador_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T003H4_A1308ContratoServicosIndicador_Tipo[0]);
               }
               if ( StringUtil.StrCmp(Z1309ContratoServicosIndicador_Periodicidade, T003H4_A1309ContratoServicosIndicador_Periodicidade[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicador_Periodicidade");
                  GXUtil.WriteLogRaw("Old: ",Z1309ContratoServicosIndicador_Periodicidade);
                  GXUtil.WriteLogRaw("Current: ",T003H4_A1309ContratoServicosIndicador_Periodicidade[0]);
               }
               if ( StringUtil.StrCmp(Z1310ContratoServicosIndicador_Vigencia, T003H4_A1310ContratoServicosIndicador_Vigencia[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicador_Vigencia");
                  GXUtil.WriteLogRaw("Old: ",Z1310ContratoServicosIndicador_Vigencia);
                  GXUtil.WriteLogRaw("Current: ",T003H4_A1310ContratoServicosIndicador_Vigencia[0]);
               }
               if ( StringUtil.StrCmp(Z1345ContratoServicosIndicador_CalculoSob, T003H4_A1345ContratoServicosIndicador_CalculoSob[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicador_CalculoSob");
                  GXUtil.WriteLogRaw("Old: ",Z1345ContratoServicosIndicador_CalculoSob);
                  GXUtil.WriteLogRaw("Current: ",T003H4_A1345ContratoServicosIndicador_CalculoSob[0]);
               }
               if ( Z2052ContratoServicosIndicador_Formato != T003H4_A2052ContratoServicosIndicador_Formato[0] )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicador_Formato");
                  GXUtil.WriteLogRaw("Old: ",Z2052ContratoServicosIndicador_Formato);
                  GXUtil.WriteLogRaw("Current: ",T003H4_A2052ContratoServicosIndicador_Formato[0]);
               }
               if ( Z1270ContratoServicosIndicador_CntSrvCod != T003H4_A1270ContratoServicosIndicador_CntSrvCod[0] )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicador_CntSrvCod");
                  GXUtil.WriteLogRaw("Old: ",Z1270ContratoServicosIndicador_CntSrvCod);
                  GXUtil.WriteLogRaw("Current: ",T003H4_A1270ContratoServicosIndicador_CntSrvCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosIndicador"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3H154( )
      {
         BeforeValidate3H154( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3H154( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3H154( 0) ;
            CheckOptimisticConcurrency3H154( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3H154( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3H154( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003H19 */
                     pr_default.execute(14, new Object[] {A1271ContratoServicosIndicador_Numero, A1274ContratoServicosIndicador_Indicador, n2051ContratoServicosIndicador_Sigla, A2051ContratoServicosIndicador_Sigla, n1305ContratoServicosIndicador_Finalidade, A1305ContratoServicosIndicador_Finalidade, n1306ContratoServicosIndicador_Meta, A1306ContratoServicosIndicador_Meta, n1307ContratoServicosIndicador_InstrumentoMedicao, A1307ContratoServicosIndicador_InstrumentoMedicao, n1308ContratoServicosIndicador_Tipo, A1308ContratoServicosIndicador_Tipo, n1309ContratoServicosIndicador_Periodicidade, A1309ContratoServicosIndicador_Periodicidade, n1310ContratoServicosIndicador_Vigencia, A1310ContratoServicosIndicador_Vigencia, n1345ContratoServicosIndicador_CalculoSob, A1345ContratoServicosIndicador_CalculoSob, A2052ContratoServicosIndicador_Formato, A1270ContratoServicosIndicador_CntSrvCod});
                     A1269ContratoServicosIndicador_Codigo = T003H19_A1269ContratoServicosIndicador_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosIndicador") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel3H154( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                              ResetCaption3H0( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3H154( ) ;
            }
            EndLevel3H154( ) ;
         }
         CloseExtendedTableCursors3H154( ) ;
      }

      protected void Update3H154( )
      {
         BeforeValidate3H154( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3H154( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3H154( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3H154( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3H154( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003H20 */
                     pr_default.execute(15, new Object[] {A1271ContratoServicosIndicador_Numero, A1274ContratoServicosIndicador_Indicador, n2051ContratoServicosIndicador_Sigla, A2051ContratoServicosIndicador_Sigla, n1305ContratoServicosIndicador_Finalidade, A1305ContratoServicosIndicador_Finalidade, n1306ContratoServicosIndicador_Meta, A1306ContratoServicosIndicador_Meta, n1307ContratoServicosIndicador_InstrumentoMedicao, A1307ContratoServicosIndicador_InstrumentoMedicao, n1308ContratoServicosIndicador_Tipo, A1308ContratoServicosIndicador_Tipo, n1309ContratoServicosIndicador_Periodicidade, A1309ContratoServicosIndicador_Periodicidade, n1310ContratoServicosIndicador_Vigencia, A1310ContratoServicosIndicador_Vigencia, n1345ContratoServicosIndicador_CalculoSob, A1345ContratoServicosIndicador_CalculoSob, A2052ContratoServicosIndicador_Formato, A1270ContratoServicosIndicador_CntSrvCod, A1269ContratoServicosIndicador_Codigo});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosIndicador") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosIndicador"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3H154( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel3H154( ) ;
                           if ( AnyError == 0 )
                           {
                              if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                              {
                                 if ( AnyError == 0 )
                                 {
                                    context.nUserReturn = 1;
                                 }
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3H154( ) ;
         }
         CloseExtendedTableCursors3H154( ) ;
      }

      protected void DeferredUpdate3H154( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3H154( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3H154( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3H154( ) ;
            AfterConfirm3H154( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3H154( ) ;
               if ( AnyError == 0 )
               {
                  A1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
                  n1298ContratoServicosIndicador_QtdeFaixas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
                  ScanStart3H159( ) ;
                  while ( RcdFound159 != 0 )
                  {
                     getByPrimaryKey3H159( ) ;
                     Delete3H159( ) ;
                     ScanNext3H159( ) ;
                     O1298ContratoServicosIndicador_QtdeFaixas = A1298ContratoServicosIndicador_QtdeFaixas;
                     n1298ContratoServicosIndicador_QtdeFaixas = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
                  }
                  ScanEnd3H159( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003H21 */
                     pr_default.execute(16, new Object[] {A1269ContratoServicosIndicador_Codigo});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosIndicador") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode154 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3H154( ) ;
         Gx_mode = sMode154;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3H154( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003H23 */
            pr_default.execute(17, new Object[] {A1269ContratoServicosIndicador_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               A1298ContratoServicosIndicador_QtdeFaixas = T003H23_A1298ContratoServicosIndicador_QtdeFaixas[0];
               n1298ContratoServicosIndicador_QtdeFaixas = T003H23_n1298ContratoServicosIndicador_QtdeFaixas[0];
            }
            else
            {
               A1298ContratoServicosIndicador_QtdeFaixas = 0;
               n1298ContratoServicosIndicador_QtdeFaixas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            pr_default.close(17);
            /* Using cursor T003H24 */
            pr_default.execute(18, new Object[] {A1270ContratoServicosIndicador_CntSrvCod});
            A1296ContratoServicosIndicador_ContratoCod = T003H24_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = T003H24_n1296ContratoServicosIndicador_ContratoCod[0];
            pr_default.close(18);
            /* Using cursor T003H25 */
            pr_default.execute(19, new Object[] {n1296ContratoServicosIndicador_ContratoCod, A1296ContratoServicosIndicador_ContratoCod});
            A1295ContratoServicosIndicador_AreaTrabalhoCod = T003H25_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = T003H25_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            pr_default.close(19);
            A1395ContratoServicosIndicador_ComQtdeFaixas = StringUtil.Substring( A1274ContratoServicosIndicador_Indicador, 1, 30) + "(" + StringUtil.Trim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)) + ")";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1395ContratoServicosIndicador_ComQtdeFaixas", A1395ContratoServicosIndicador_ComQtdeFaixas);
            edtContratoServicosIndicadorFaixa_Desde_Title = ((StringUtil.StrCmp(A1345ContratoServicosIndicador_CalculoSob, "D")==0) ? "Dias" : "Percentuais");
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Desde_Internalname, "Title", edtContratoServicosIndicadorFaixa_Desde_Title);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T003H26 */
            pr_default.execute(20, new Object[] {A1269ContratoServicosIndicador_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Indicadores"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
         }
      }

      protected void ProcessNestedLevel3H159( )
      {
         s1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
         n1298ContratoServicosIndicador_QtdeFaixas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         nGXsfl_80_idx = 0;
         while ( nGXsfl_80_idx < nRC_GXsfl_80 )
         {
            ReadRow3H159( ) ;
            if ( ( nRcdExists_159 != 0 ) || ( nIsMod_159 != 0 ) )
            {
               standaloneNotModal3H159( ) ;
               GetKey3H159( ) ;
               if ( ( nRcdExists_159 == 0 ) && ( nRcdDeleted_159 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  Insert3H159( ) ;
               }
               else
               {
                  if ( RcdFound159 != 0 )
                  {
                     if ( ( nRcdDeleted_159 != 0 ) && ( nRcdExists_159 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        Delete3H159( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_159 != 0 ) && ( nRcdExists_159 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           Update3H159( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_159 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               O1298ContratoServicosIndicador_QtdeFaixas = A1298ContratoServicosIndicador_QtdeFaixas;
               n1298ContratoServicosIndicador_QtdeFaixas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            ChangePostValue( edtContratoServicosIndicadorFaixa_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( edtContratoServicosIndicadorFaixa_Numero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ",", ""))) ;
            ChangePostValue( edtContratoServicosIndicadorFaixa_Desde_Internalname, StringUtil.LTrim( StringUtil.NToC( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ",", ""))) ;
            ChangePostValue( edtavLabel_Internalname, StringUtil.RTrim( AV15Label)) ;
            ChangePostValue( edtContratoServicosIndicadorFaixa_Ate_Internalname, StringUtil.LTrim( StringUtil.NToC( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ",", ""))) ;
            ChangePostValue( edtContratoServicosIndicadorFaixa_Reduz_Internalname, StringUtil.LTrim( StringUtil.NToC( A1303ContratoServicosIndicadorFaixa_Reduz, 8, 2, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1299ContratoServicosIndicadorFaixa_Codigo_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1304ContratoServicosIndicadorFaixa_Und_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1304ContratoServicosIndicadorFaixa_Und), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1303ContratoServicosIndicadorFaixa_Reduz_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( Z1303ContratoServicosIndicadorFaixa_Reduz, 6, 2, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1302ContratoServicosIndicadorFaixa_Ate_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( Z1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1301ContratoServicosIndicadorFaixa_Desde_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( Z1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1300ContratoServicosIndicadorFaixa_Numero_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_159_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_159), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_159_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_159), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_159_"+sGXsfl_80_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_159), 4, 0, ",", ""))) ;
            if ( nIsMod_159 != 0 )
            {
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_CODIGO_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Codigo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_NUMERO_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Numero_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_idx+"Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Desde_Title)) ;
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Desde_Enabled), 5, 0, ".", ""))) ;
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               ChangePostValue( "vLABEL_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLabel_Enabled), 5, 0, ".", ""))) ;
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               /* * Property isDeleted not supported in */
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_ATE_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Ate_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSINDICADORFAIXA_REDUZ_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Reduz_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll3H159( ) ;
         if ( AnyError != 0 )
         {
            O1298ContratoServicosIndicador_QtdeFaixas = s1298ContratoServicosIndicador_QtdeFaixas;
            n1298ContratoServicosIndicador_QtdeFaixas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         }
         nRcdExists_159 = 0;
         nIsMod_159 = 0;
         nRcdDeleted_159 = 0;
      }

      protected void ProcessLevel3H154( )
      {
         /* Save parent mode. */
         sMode154 = Gx_mode;
         ProcessNestedLevel3H159( ) ;
         if ( AnyError != 0 )
         {
            O1298ContratoServicosIndicador_QtdeFaixas = s1298ContratoServicosIndicador_QtdeFaixas;
            n1298ContratoServicosIndicador_QtdeFaixas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         }
         /* Restore parent mode. */
         Gx_mode = sMode154;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         /* ' Update level parameters */
      }

      protected void EndLevel3H154( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(2);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3H154( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(3);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(18);
            pr_default.close(19);
            pr_default.close(17);
            context.CommitDataStores( "ContratoServicosIndicador");
            if ( AnyError == 0 )
            {
               ConfirmValues3H0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(3);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(18);
            pr_default.close(19);
            pr_default.close(17);
            context.RollbackDataStores( "ContratoServicosIndicador");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3H154( )
      {
         /* Scan By routine */
         /* Using cursor T003H27 */
         pr_default.execute(21);
         RcdFound154 = 0;
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound154 = 1;
            A1269ContratoServicosIndicador_Codigo = T003H27_A1269ContratoServicosIndicador_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3H154( )
      {
         /* Scan next routine */
         pr_default.readNext(21);
         RcdFound154 = 0;
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound154 = 1;
            A1269ContratoServicosIndicador_Codigo = T003H27_A1269ContratoServicosIndicador_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3H154( )
      {
         pr_default.close(21);
      }

      protected void AfterConfirm3H154( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3H154( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3H154( )
      {
         /* Before Update Rules */
         new loadauditcontratoservicosindicador(context ).execute(  "Y", ref  AV16AuditingObject,  A1269ContratoServicosIndicador_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeDelete3H154( )
      {
         /* Before Delete Rules */
         new loadauditcontratoservicosindicador(context ).execute(  "Y", ref  AV16AuditingObject,  A1269ContratoServicosIndicador_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete3H154( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratoservicosindicador(context ).execute(  "N", ref  AV16AuditingObject,  A1269ContratoServicosIndicador_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratoservicosindicador(context ).execute(  "N", ref  AV16AuditingObject,  A1269ContratoServicosIndicador_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate3H154( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3H154( )
      {
         edtContratoServicosIndicador_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Numero_Enabled), 5, 0)));
         cmbContratoServicosIndicador_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicosIndicador_Tipo.Enabled), 5, 0)));
         edtContratoServicosIndicador_Indicador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Indicador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Indicador_Enabled), 5, 0)));
         edtContratoServicosIndicador_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Sigla_Enabled), 5, 0)));
         edtContratoServicosIndicador_Finalidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Finalidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Finalidade_Enabled), 5, 0)));
         edtContratoServicosIndicador_Meta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Meta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Meta_Enabled), 5, 0)));
         edtContratoServicosIndicador_InstrumentoMedicao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_InstrumentoMedicao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_InstrumentoMedicao_Enabled), 5, 0)));
         cmbContratoServicosIndicador_Periodicidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_Periodicidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicosIndicador_Periodicidade.Enabled), 5, 0)));
         cmbContratoServicosIndicador_CalculoSob.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_CalculoSob_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicosIndicador_CalculoSob.Enabled), 5, 0)));
         edtContratoServicosIndicador_Vigencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Vigencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Vigencia_Enabled), 5, 0)));
         cmbContratoServicosIndicador_Formato.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosIndicador_Formato_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicosIndicador_Formato.Enabled), 5, 0)));
         edtavLabel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLabel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLabel_Enabled), 5, 0)));
         edtContratoServicosIndicador_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Codigo_Enabled), 5, 0)));
         edtContratoServicosIndicador_CntSrvCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicador_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_CntSrvCod_Enabled), 5, 0)));
      }

      protected void ZM3H159( short GX_JID )
      {
         if ( ( GX_JID == 29 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1304ContratoServicosIndicadorFaixa_Und = T003H3_A1304ContratoServicosIndicadorFaixa_Und[0];
               Z1303ContratoServicosIndicadorFaixa_Reduz = T003H3_A1303ContratoServicosIndicadorFaixa_Reduz[0];
               Z1302ContratoServicosIndicadorFaixa_Ate = T003H3_A1302ContratoServicosIndicadorFaixa_Ate[0];
               Z1301ContratoServicosIndicadorFaixa_Desde = T003H3_A1301ContratoServicosIndicadorFaixa_Desde[0];
               Z1300ContratoServicosIndicadorFaixa_Numero = T003H3_A1300ContratoServicosIndicadorFaixa_Numero[0];
            }
            else
            {
               Z1304ContratoServicosIndicadorFaixa_Und = A1304ContratoServicosIndicadorFaixa_Und;
               Z1303ContratoServicosIndicadorFaixa_Reduz = A1303ContratoServicosIndicadorFaixa_Reduz;
               Z1302ContratoServicosIndicadorFaixa_Ate = A1302ContratoServicosIndicadorFaixa_Ate;
               Z1301ContratoServicosIndicadorFaixa_Desde = A1301ContratoServicosIndicadorFaixa_Desde;
               Z1300ContratoServicosIndicadorFaixa_Numero = A1300ContratoServicosIndicadorFaixa_Numero;
            }
         }
         if ( GX_JID == -29 )
         {
            Z1269ContratoServicosIndicador_Codigo = A1269ContratoServicosIndicador_Codigo;
            Z1299ContratoServicosIndicadorFaixa_Codigo = A1299ContratoServicosIndicadorFaixa_Codigo;
            Z1304ContratoServicosIndicadorFaixa_Und = A1304ContratoServicosIndicadorFaixa_Und;
            Z1303ContratoServicosIndicadorFaixa_Reduz = A1303ContratoServicosIndicadorFaixa_Reduz;
            Z1302ContratoServicosIndicadorFaixa_Ate = A1302ContratoServicosIndicadorFaixa_Ate;
            Z1301ContratoServicosIndicadorFaixa_Desde = A1301ContratoServicosIndicadorFaixa_Desde;
            Z1300ContratoServicosIndicadorFaixa_Numero = A1300ContratoServicosIndicadorFaixa_Numero;
         }
      }

      protected void standaloneNotModal3H159( )
      {
         edtContratoServicosIndicadorFaixa_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Codigo_Enabled), 5, 0)));
      }

      protected void standaloneModal3H159( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1298ContratoServicosIndicador_QtdeFaixas = (short)(O1298ContratoServicosIndicador_QtdeFaixas+1);
            n1298ContratoServicosIndicador_QtdeFaixas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1304ContratoServicosIndicadorFaixa_Und) && ( Gx_BScreen == 0 ) )
         {
            A1304ContratoServicosIndicadorFaixa_Und = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1304ContratoServicosIndicadorFaixa_Und", StringUtil.LTrim( StringUtil.Str( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A1303ContratoServicosIndicadorFaixa_Reduz) && ( Gx_BScreen == 0 ) )
         {
            A1303ContratoServicosIndicadorFaixa_Reduz = 0;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A1302ContratoServicosIndicadorFaixa_Ate) && ( Gx_BScreen == 0 ) )
         {
            A1302ContratoServicosIndicadorFaixa_Ate = 0;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A1301ContratoServicosIndicadorFaixa_Desde) && ( Gx_BScreen == 0 ) )
         {
            A1301ContratoServicosIndicadorFaixa_Desde = 0;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1300ContratoServicosIndicadorFaixa_Numero) && ( Gx_BScreen == 0 ) )
         {
            A1300ContratoServicosIndicadorFaixa_Numero = 0;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ( Gx_BScreen == 1 ) )
         {
            A1299ContratoServicosIndicadorFaixa_Codigo = A1298ContratoServicosIndicador_QtdeFaixas;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load3H159( )
      {
         /* Using cursor T003H28 */
         pr_default.execute(22, new Object[] {A1269ContratoServicosIndicador_Codigo, A1299ContratoServicosIndicadorFaixa_Codigo});
         if ( (pr_default.getStatus(22) != 101) )
         {
            RcdFound159 = 1;
            A1304ContratoServicosIndicadorFaixa_Und = T003H28_A1304ContratoServicosIndicadorFaixa_Und[0];
            A1303ContratoServicosIndicadorFaixa_Reduz = T003H28_A1303ContratoServicosIndicadorFaixa_Reduz[0];
            A1302ContratoServicosIndicadorFaixa_Ate = T003H28_A1302ContratoServicosIndicadorFaixa_Ate[0];
            A1301ContratoServicosIndicadorFaixa_Desde = T003H28_A1301ContratoServicosIndicadorFaixa_Desde[0];
            A1300ContratoServicosIndicadorFaixa_Numero = T003H28_A1300ContratoServicosIndicadorFaixa_Numero[0];
            ZM3H159( -29) ;
         }
         pr_default.close(22);
         OnLoadActions3H159( ) ;
      }

      protected void OnLoadActions3H159( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1298ContratoServicosIndicador_QtdeFaixas = (short)(O1298ContratoServicosIndicador_QtdeFaixas+1);
            n1298ContratoServicosIndicador_QtdeFaixas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
               n1298ContratoServicosIndicador_QtdeFaixas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A1298ContratoServicosIndicador_QtdeFaixas = (short)(O1298ContratoServicosIndicador_QtdeFaixas-1);
                  n1298ContratoServicosIndicador_QtdeFaixas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
               }
            }
         }
      }

      protected void CheckExtendedTable3H159( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal3H159( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1298ContratoServicosIndicador_QtdeFaixas = (short)(O1298ContratoServicosIndicador_QtdeFaixas+1);
            n1298ContratoServicosIndicador_QtdeFaixas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
               n1298ContratoServicosIndicador_QtdeFaixas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A1298ContratoServicosIndicador_QtdeFaixas = (short)(O1298ContratoServicosIndicador_QtdeFaixas-1);
                  n1298ContratoServicosIndicador_QtdeFaixas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
               }
            }
         }
         if ( ! ( ( ( A1303ContratoServicosIndicadorFaixa_Reduz >= Convert.ToDecimal( 0 )) && ( A1303ContratoServicosIndicadorFaixa_Reduz <= Convert.ToDecimal( 100 )) ) ) )
         {
            GXCCtl = "CONTRATOSERVICOSINDICADORFAIXA_REDUZ_" + sGXsfl_80_idx;
            GX_msglist.addItem("Campo Reduz fora do intervalo", "OutOfRange", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosIndicadorFaixa_Reduz_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors3H159( )
      {
      }

      protected void enableDisable3H159( )
      {
      }

      protected void GetKey3H159( )
      {
         /* Using cursor T003H29 */
         pr_default.execute(23, new Object[] {A1269ContratoServicosIndicador_Codigo, A1299ContratoServicosIndicadorFaixa_Codigo});
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound159 = 1;
         }
         else
         {
            RcdFound159 = 0;
         }
         pr_default.close(23);
      }

      protected void getByPrimaryKey3H159( )
      {
         /* Using cursor T003H3 */
         pr_default.execute(1, new Object[] {A1269ContratoServicosIndicador_Codigo, A1299ContratoServicosIndicadorFaixa_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3H159( 29) ;
            RcdFound159 = 1;
            InitializeNonKey3H159( ) ;
            A1299ContratoServicosIndicadorFaixa_Codigo = T003H3_A1299ContratoServicosIndicadorFaixa_Codigo[0];
            A1304ContratoServicosIndicadorFaixa_Und = T003H3_A1304ContratoServicosIndicadorFaixa_Und[0];
            A1303ContratoServicosIndicadorFaixa_Reduz = T003H3_A1303ContratoServicosIndicadorFaixa_Reduz[0];
            A1302ContratoServicosIndicadorFaixa_Ate = T003H3_A1302ContratoServicosIndicadorFaixa_Ate[0];
            A1301ContratoServicosIndicadorFaixa_Desde = T003H3_A1301ContratoServicosIndicadorFaixa_Desde[0];
            A1300ContratoServicosIndicadorFaixa_Numero = T003H3_A1300ContratoServicosIndicadorFaixa_Numero[0];
            Z1269ContratoServicosIndicador_Codigo = A1269ContratoServicosIndicador_Codigo;
            Z1299ContratoServicosIndicadorFaixa_Codigo = A1299ContratoServicosIndicadorFaixa_Codigo;
            sMode159 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3H159( ) ;
            Gx_mode = sMode159;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound159 = 0;
            InitializeNonKey3H159( ) ;
            sMode159 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal3H159( ) ;
            Gx_mode = sMode159;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes3H159( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency3H159( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003H2 */
            pr_default.execute(0, new Object[] {A1269ContratoServicosIndicador_Codigo, A1299ContratoServicosIndicadorFaixa_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosIndicadorFaixas"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1304ContratoServicosIndicadorFaixa_Und != T003H2_A1304ContratoServicosIndicadorFaixa_Und[0] ) || ( Z1303ContratoServicosIndicadorFaixa_Reduz != T003H2_A1303ContratoServicosIndicadorFaixa_Reduz[0] ) || ( Z1302ContratoServicosIndicadorFaixa_Ate != T003H2_A1302ContratoServicosIndicadorFaixa_Ate[0] ) || ( Z1301ContratoServicosIndicadorFaixa_Desde != T003H2_A1301ContratoServicosIndicadorFaixa_Desde[0] ) || ( Z1300ContratoServicosIndicadorFaixa_Numero != T003H2_A1300ContratoServicosIndicadorFaixa_Numero[0] ) )
            {
               if ( Z1304ContratoServicosIndicadorFaixa_Und != T003H2_A1304ContratoServicosIndicadorFaixa_Und[0] )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicadorFaixa_Und");
                  GXUtil.WriteLogRaw("Old: ",Z1304ContratoServicosIndicadorFaixa_Und);
                  GXUtil.WriteLogRaw("Current: ",T003H2_A1304ContratoServicosIndicadorFaixa_Und[0]);
               }
               if ( Z1303ContratoServicosIndicadorFaixa_Reduz != T003H2_A1303ContratoServicosIndicadorFaixa_Reduz[0] )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicadorFaixa_Reduz");
                  GXUtil.WriteLogRaw("Old: ",Z1303ContratoServicosIndicadorFaixa_Reduz);
                  GXUtil.WriteLogRaw("Current: ",T003H2_A1303ContratoServicosIndicadorFaixa_Reduz[0]);
               }
               if ( Z1302ContratoServicosIndicadorFaixa_Ate != T003H2_A1302ContratoServicosIndicadorFaixa_Ate[0] )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicadorFaixa_Ate");
                  GXUtil.WriteLogRaw("Old: ",Z1302ContratoServicosIndicadorFaixa_Ate);
                  GXUtil.WriteLogRaw("Current: ",T003H2_A1302ContratoServicosIndicadorFaixa_Ate[0]);
               }
               if ( Z1301ContratoServicosIndicadorFaixa_Desde != T003H2_A1301ContratoServicosIndicadorFaixa_Desde[0] )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicadorFaixa_Desde");
                  GXUtil.WriteLogRaw("Old: ",Z1301ContratoServicosIndicadorFaixa_Desde);
                  GXUtil.WriteLogRaw("Current: ",T003H2_A1301ContratoServicosIndicadorFaixa_Desde[0]);
               }
               if ( Z1300ContratoServicosIndicadorFaixa_Numero != T003H2_A1300ContratoServicosIndicadorFaixa_Numero[0] )
               {
                  GXUtil.WriteLog("contratoservicosindicador:[seudo value changed for attri]"+"ContratoServicosIndicadorFaixa_Numero");
                  GXUtil.WriteLogRaw("Old: ",Z1300ContratoServicosIndicadorFaixa_Numero);
                  GXUtil.WriteLogRaw("Current: ",T003H2_A1300ContratoServicosIndicadorFaixa_Numero[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosIndicadorFaixas"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3H159( )
      {
         BeforeValidate3H159( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3H159( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3H159( 0) ;
            CheckOptimisticConcurrency3H159( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3H159( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3H159( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003H30 */
                     pr_default.execute(24, new Object[] {A1269ContratoServicosIndicador_Codigo, A1299ContratoServicosIndicadorFaixa_Codigo, A1304ContratoServicosIndicadorFaixa_Und, A1303ContratoServicosIndicadorFaixa_Reduz, A1302ContratoServicosIndicadorFaixa_Ate, A1301ContratoServicosIndicadorFaixa_Desde, A1300ContratoServicosIndicadorFaixa_Numero});
                     pr_default.close(24);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosIndicadorFaixas") ;
                     if ( (pr_default.getStatus(24) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3H159( ) ;
            }
            EndLevel3H159( ) ;
         }
         CloseExtendedTableCursors3H159( ) ;
      }

      protected void Update3H159( )
      {
         BeforeValidate3H159( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3H159( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3H159( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3H159( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3H159( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003H31 */
                     pr_default.execute(25, new Object[] {A1304ContratoServicosIndicadorFaixa_Und, A1303ContratoServicosIndicadorFaixa_Reduz, A1302ContratoServicosIndicadorFaixa_Ate, A1301ContratoServicosIndicadorFaixa_Desde, A1300ContratoServicosIndicadorFaixa_Numero, A1269ContratoServicosIndicador_Codigo, A1299ContratoServicosIndicadorFaixa_Codigo});
                     pr_default.close(25);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosIndicadorFaixas") ;
                     if ( (pr_default.getStatus(25) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosIndicadorFaixas"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3H159( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey3H159( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3H159( ) ;
         }
         CloseExtendedTableCursors3H159( ) ;
      }

      protected void DeferredUpdate3H159( )
      {
      }

      protected void Delete3H159( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         BeforeValidate3H159( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3H159( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3H159( ) ;
            AfterConfirm3H159( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3H159( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003H32 */
                  pr_default.execute(26, new Object[] {A1269ContratoServicosIndicador_Codigo, A1299ContratoServicosIndicadorFaixa_Codigo});
                  pr_default.close(26);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosIndicadorFaixas") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode159 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3H159( ) ;
         Gx_mode = sMode159;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3H159( )
      {
         standaloneModal3H159( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A1298ContratoServicosIndicador_QtdeFaixas = (short)(O1298ContratoServicosIndicador_QtdeFaixas+1);
               n1298ContratoServicosIndicador_QtdeFaixas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
               {
                  A1298ContratoServicosIndicador_QtdeFaixas = O1298ContratoServicosIndicador_QtdeFaixas;
                  n1298ContratoServicosIndicador_QtdeFaixas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
               }
               else
               {
                  if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
                  {
                     A1298ContratoServicosIndicador_QtdeFaixas = (short)(O1298ContratoServicosIndicador_QtdeFaixas-1);
                     n1298ContratoServicosIndicador_QtdeFaixas = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
                  }
               }
            }
         }
      }

      protected void EndLevel3H159( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3H159( )
      {
         /* Scan By routine */
         /* Using cursor T003H33 */
         pr_default.execute(27, new Object[] {A1269ContratoServicosIndicador_Codigo});
         RcdFound159 = 0;
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound159 = 1;
            A1299ContratoServicosIndicadorFaixa_Codigo = T003H33_A1299ContratoServicosIndicadorFaixa_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3H159( )
      {
         /* Scan next routine */
         pr_default.readNext(27);
         RcdFound159 = 0;
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound159 = 1;
            A1299ContratoServicosIndicadorFaixa_Codigo = T003H33_A1299ContratoServicosIndicadorFaixa_Codigo[0];
         }
      }

      protected void ScanEnd3H159( )
      {
         pr_default.close(27);
      }

      protected void AfterConfirm3H159( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3H159( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3H159( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3H159( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3H159( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3H159( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3H159( )
      {
         edtContratoServicosIndicadorFaixa_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Codigo_Enabled), 5, 0)));
         edtContratoServicosIndicadorFaixa_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Numero_Enabled), 5, 0)));
         edtContratoServicosIndicadorFaixa_Desde_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Desde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Desde_Enabled), 5, 0)));
         edtContratoServicosIndicadorFaixa_Ate_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Ate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Ate_Enabled), 5, 0)));
         edtContratoServicosIndicadorFaixa_Reduz_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Reduz_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Reduz_Enabled), 5, 0)));
      }

      protected void SubsflControlProps_80159( )
      {
         edtContratoServicosIndicadorFaixa_Codigo_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_CODIGO_"+sGXsfl_80_idx;
         edtContratoServicosIndicadorFaixa_Numero_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_NUMERO_"+sGXsfl_80_idx;
         edtContratoServicosIndicadorFaixa_Desde_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_idx;
         edtavLabel_Internalname = "vLABEL_"+sGXsfl_80_idx;
         edtContratoServicosIndicadorFaixa_Ate_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_ATE_"+sGXsfl_80_idx;
         edtContratoServicosIndicadorFaixa_Reduz_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_REDUZ_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_80159( )
      {
         edtContratoServicosIndicadorFaixa_Codigo_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_CODIGO_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicadorFaixa_Numero_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_NUMERO_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicadorFaixa_Desde_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_fel_idx;
         edtavLabel_Internalname = "vLABEL_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicadorFaixa_Ate_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_ATE_"+sGXsfl_80_fel_idx;
         edtContratoServicosIndicadorFaixa_Reduz_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_REDUZ_"+sGXsfl_80_fel_idx;
      }

      protected void AddRow3H159( )
      {
         nGXsfl_80_idx = (short)(nGXsfl_80_idx+1);
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_80159( ) ;
         SendRow3H159( ) ;
      }

      protected void SendRow3H159( )
      {
         Gridlevel_faixasRow = GXWebRow.GetNew(context);
         if ( subGridlevel_faixas_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridlevel_faixas_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridlevel_faixas_Class, "") != 0 )
            {
               subGridlevel_faixas_Linesclass = subGridlevel_faixas_Class+"Odd";
            }
         }
         else if ( subGridlevel_faixas_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridlevel_faixas_Backstyle = 0;
            subGridlevel_faixas_Backcolor = subGridlevel_faixas_Allbackcolor;
            if ( StringUtil.StrCmp(subGridlevel_faixas_Class, "") != 0 )
            {
               subGridlevel_faixas_Linesclass = subGridlevel_faixas_Class+"Uniform";
            }
         }
         else if ( subGridlevel_faixas_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridlevel_faixas_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridlevel_faixas_Class, "") != 0 )
            {
               subGridlevel_faixas_Linesclass = subGridlevel_faixas_Class+"Odd";
            }
            subGridlevel_faixas_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGridlevel_faixas_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridlevel_faixas_Backstyle = 1;
            if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
            {
               subGridlevel_faixas_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridlevel_faixas_Class, "") != 0 )
               {
                  subGridlevel_faixas_Linesclass = subGridlevel_faixas_Class+"Even";
               }
            }
            else
            {
               subGridlevel_faixas_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridlevel_faixas_Class, "") != 0 )
               {
                  subGridlevel_faixas_Linesclass = subGridlevel_faixas_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Gridlevel_faixasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ",", "")),((edtContratoServicosIndicadorFaixa_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtContratoServicosIndicadorFaixa_Codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_159_" + sGXsfl_80_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_80_idx + "',80)\"";
         ROClassString = "BootstrapAttribute";
         Gridlevel_faixasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Numero_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ",", "")),((edtContratoServicosIndicadorFaixa_Numero_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")) : context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,82);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosIndicadorFaixa_Numero_Enabled,(short)0,(String)"text",(String)"",(short)51,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_159_" + sGXsfl_80_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_80_idx + "',80)\"";
         ROClassString = "BootstrapAttribute";
         Gridlevel_faixasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Desde_Internalname,StringUtil.LTrim( StringUtil.NToC( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ",", "")),((edtContratoServicosIndicadorFaixa_Desde_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")) : context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,83);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Desde_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosIndicadorFaixa_Desde_Enabled,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
         /* Subfile cell */
         /* * Property isDeleted not supported in */
         /* Single line edit */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         ROClassString = "BootstrapAttribute";
         /* * Property isDeleted not supported in */
         Gridlevel_faixasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavLabel_Internalname,StringUtil.RTrim( AV15Label),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavLabel_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavLabel_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_159_" + sGXsfl_80_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_80_idx + "',80)\"";
         ROClassString = "BootstrapAttribute";
         Gridlevel_faixasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Ate_Internalname,StringUtil.LTrim( StringUtil.NToC( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ",", "")),((edtContratoServicosIndicadorFaixa_Ate_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")) : context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,85);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Ate_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosIndicadorFaixa_Ate_Enabled,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_159_" + sGXsfl_80_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_80_idx + "',80)\"";
         ROClassString = "BootstrapAttribute";
         Gridlevel_faixasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Reduz_Internalname,StringUtil.LTrim( StringUtil.NToC( A1303ContratoServicosIndicadorFaixa_Reduz, 8, 2, ",", "")),((edtContratoServicosIndicadorFaixa_Reduz_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")) : context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,86);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Reduz_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosIndicadorFaixa_Reduz_Enabled,(short)0,(String)"text",(String)"",(short)58,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
         context.httpAjaxContext.ajax_sending_grid_row(Gridlevel_faixasRow);
         GXCCtl = "Z1299ContratoServicosIndicadorFaixa_Codigo_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ",", "")));
         GXCCtl = "Z1304ContratoServicosIndicadorFaixa_Und_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1304ContratoServicosIndicadorFaixa_Und), 4, 0, ",", "")));
         GXCCtl = "Z1303ContratoServicosIndicadorFaixa_Reduz_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( Z1303ContratoServicosIndicadorFaixa_Reduz, 6, 2, ",", "")));
         GXCCtl = "Z1302ContratoServicosIndicadorFaixa_Ate_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( Z1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ",", "")));
         GXCCtl = "Z1301ContratoServicosIndicadorFaixa_Desde_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( Z1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ",", "")));
         GXCCtl = "Z1300ContratoServicosIndicadorFaixa_Numero_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ",", "")));
         GXCCtl = "nRcdDeleted_159_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_159), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_159_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_159), 4, 0, ",", "")));
         GXCCtl = "nIsMod_159_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_159), 4, 0, ",", "")));
         GXCCtl = "vAUDITINGOBJECT_" + sGXsfl_80_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV16AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV16AuditingObject);
         }
         GXCCtl = "vPGMNAME_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( AV18Pgmname));
         GXCCtl = "vMODE_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Gx_mode));
         GXCCtl = "vTRNCONTEXT_" + sGXsfl_80_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV9TrnContext);
         }
         GXCCtl = "vCONTRATOSERVICOSINDICADOR_CNTSRVCOD_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")));
         GXCCtl = "vCONTRATOSERVICOSINDICADOR_CODIGO_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0, ",", "")));
         GXCCtl = "CONTRATOSERVICOSINDICADORFAIXA_UND_" + sGXsfl_80_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vLABEL"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( AV15Label, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_CODIGO_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Codigo_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Numero_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_idx+"Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Desde_Title));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Desde_Enabled), 5, 0, ".", "")));
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         GxWebStd.gx_hidden_field( context, "vLABEL_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLabel_Enabled), 5, 0, ".", "")));
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_ATE_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Ate_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_REDUZ_"+sGXsfl_80_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Reduz_Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridlevel_faixasContainer.AddRow(Gridlevel_faixasRow);
      }

      protected void ReadRow3H159( )
      {
         nGXsfl_80_idx = (short)(nGXsfl_80_idx+1);
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_80159( ) ;
         edtContratoServicosIndicadorFaixa_Codigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_CODIGO_"+sGXsfl_80_idx+"Enabled"), ",", "."));
         edtContratoServicosIndicadorFaixa_Numero_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_NUMERO_"+sGXsfl_80_idx+"Enabled"), ",", "."));
         edtContratoServicosIndicadorFaixa_Desde_Title = cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_idx+"Title");
         edtContratoServicosIndicadorFaixa_Desde_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_80_idx+"Enabled"), ",", "."));
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         edtavLabel_Enabled = (int)(context.localUtil.CToN( cgiGet( "vLABEL_"+sGXsfl_80_idx+"Enabled"), ",", "."));
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         /* * Property isDeleted not supported in */
         edtContratoServicosIndicadorFaixa_Ate_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_ATE_"+sGXsfl_80_idx+"Enabled"), ",", "."));
         edtContratoServicosIndicadorFaixa_Reduz_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSINDICADORFAIXA_REDUZ_"+sGXsfl_80_idx+"Enabled"), ",", "."));
         A1299ContratoServicosIndicadorFaixa_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Codigo_Internalname), ",", "."));
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Numero_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Numero_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
         {
            GXCCtl = "CONTRATOSERVICOSINDICADORFAIXA_NUMERO_" + sGXsfl_80_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosIndicadorFaixa_Numero_Internalname;
            wbErr = true;
            A1300ContratoServicosIndicadorFaixa_Numero = 0;
         }
         else
         {
            A1300ContratoServicosIndicadorFaixa_Numero = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Numero_Internalname), ",", "."));
         }
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Desde_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Desde_Internalname), ",", ".") > 999.99m ) ) )
         {
            GXCCtl = "CONTRATOSERVICOSINDICADORFAIXA_DESDE_" + sGXsfl_80_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosIndicadorFaixa_Desde_Internalname;
            wbErr = true;
            A1301ContratoServicosIndicadorFaixa_Desde = 0;
         }
         else
         {
            A1301ContratoServicosIndicadorFaixa_Desde = context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Desde_Internalname), ",", ".");
         }
         AV15Label = cgiGet( edtavLabel_Internalname);
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Ate_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Ate_Internalname), ",", ".") > 999.99m ) ) )
         {
            GXCCtl = "CONTRATOSERVICOSINDICADORFAIXA_ATE_" + sGXsfl_80_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosIndicadorFaixa_Ate_Internalname;
            wbErr = true;
            A1302ContratoServicosIndicadorFaixa_Ate = 0;
         }
         else
         {
            A1302ContratoServicosIndicadorFaixa_Ate = context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Ate_Internalname), ",", ".");
         }
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Reduz_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Reduz_Internalname), ",", ".") > 999.99m ) ) )
         {
            GXCCtl = "CONTRATOSERVICOSINDICADORFAIXA_REDUZ_" + sGXsfl_80_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosIndicadorFaixa_Reduz_Internalname;
            wbErr = true;
            A1303ContratoServicosIndicadorFaixa_Reduz = 0;
         }
         else
         {
            A1303ContratoServicosIndicadorFaixa_Reduz = context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Reduz_Internalname), ",", ".");
         }
         GXCCtl = "Z1299ContratoServicosIndicadorFaixa_Codigo_" + sGXsfl_80_idx;
         Z1299ContratoServicosIndicadorFaixa_Codigo = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z1304ContratoServicosIndicadorFaixa_Und_" + sGXsfl_80_idx;
         Z1304ContratoServicosIndicadorFaixa_Und = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z1303ContratoServicosIndicadorFaixa_Reduz_" + sGXsfl_80_idx;
         Z1303ContratoServicosIndicadorFaixa_Reduz = context.localUtil.CToN( cgiGet( GXCCtl), ",", ".");
         GXCCtl = "Z1302ContratoServicosIndicadorFaixa_Ate_" + sGXsfl_80_idx;
         Z1302ContratoServicosIndicadorFaixa_Ate = context.localUtil.CToN( cgiGet( GXCCtl), ",", ".");
         GXCCtl = "Z1301ContratoServicosIndicadorFaixa_Desde_" + sGXsfl_80_idx;
         Z1301ContratoServicosIndicadorFaixa_Desde = context.localUtil.CToN( cgiGet( GXCCtl), ",", ".");
         GXCCtl = "Z1300ContratoServicosIndicadorFaixa_Numero_" + sGXsfl_80_idx;
         Z1300ContratoServicosIndicadorFaixa_Numero = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z1304ContratoServicosIndicadorFaixa_Und_" + sGXsfl_80_idx;
         A1304ContratoServicosIndicadorFaixa_Und = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdDeleted_159_" + sGXsfl_80_idx;
         nRcdDeleted_159 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_159_" + sGXsfl_80_idx;
         nRcdExists_159 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_159_" + sGXsfl_80_idx;
         nIsMod_159 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "CONTRATOSERVICOSINDICADORFAIXA_UND_" + sGXsfl_80_idx;
         A1304ContratoServicosIndicadorFaixa_Und = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void assign_properties_default( )
      {
         defedtContratoServicosIndicadorFaixa_Codigo_Enabled = edtContratoServicosIndicadorFaixa_Codigo_Enabled;
      }

      protected void ConfirmValues3H0( )
      {
         nGXsfl_80_idx = 0;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_80159( ) ;
         while ( nGXsfl_80_idx < nRC_GXsfl_80 )
         {
            nGXsfl_80_idx = (short)(nGXsfl_80_idx+1);
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_80159( ) ;
            ChangePostValue( "Z1299ContratoServicosIndicadorFaixa_Codigo_"+sGXsfl_80_idx, cgiGet( "ZT_"+"Z1299ContratoServicosIndicadorFaixa_Codigo_"+sGXsfl_80_idx)) ;
            DeletePostValue( "ZT_"+"Z1299ContratoServicosIndicadorFaixa_Codigo_"+sGXsfl_80_idx) ;
            ChangePostValue( "Z1304ContratoServicosIndicadorFaixa_Und_"+sGXsfl_80_idx, cgiGet( "ZT_"+"Z1304ContratoServicosIndicadorFaixa_Und_"+sGXsfl_80_idx)) ;
            DeletePostValue( "ZT_"+"Z1304ContratoServicosIndicadorFaixa_Und_"+sGXsfl_80_idx) ;
            ChangePostValue( "Z1303ContratoServicosIndicadorFaixa_Reduz_"+sGXsfl_80_idx, cgiGet( "ZT_"+"Z1303ContratoServicosIndicadorFaixa_Reduz_"+sGXsfl_80_idx)) ;
            DeletePostValue( "ZT_"+"Z1303ContratoServicosIndicadorFaixa_Reduz_"+sGXsfl_80_idx) ;
            ChangePostValue( "Z1302ContratoServicosIndicadorFaixa_Ate_"+sGXsfl_80_idx, cgiGet( "ZT_"+"Z1302ContratoServicosIndicadorFaixa_Ate_"+sGXsfl_80_idx)) ;
            DeletePostValue( "ZT_"+"Z1302ContratoServicosIndicadorFaixa_Ate_"+sGXsfl_80_idx) ;
            ChangePostValue( "Z1301ContratoServicosIndicadorFaixa_Desde_"+sGXsfl_80_idx, cgiGet( "ZT_"+"Z1301ContratoServicosIndicadorFaixa_Desde_"+sGXsfl_80_idx)) ;
            DeletePostValue( "ZT_"+"Z1301ContratoServicosIndicadorFaixa_Desde_"+sGXsfl_80_idx) ;
            ChangePostValue( "Z1300ContratoServicosIndicadorFaixa_Numero_"+sGXsfl_80_idx, cgiGet( "ZT_"+"Z1300ContratoServicosIndicadorFaixa_Numero_"+sGXsfl_80_idx)) ;
            DeletePostValue( "ZT_"+"Z1300ContratoServicosIndicadorFaixa_Numero_"+sGXsfl_80_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299312397");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosindicador.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicosIndicador_Codigo) + "," + UrlEncode("" +AV14ContratoServicosIndicador_CntSrvCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1269ContratoServicosIndicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1271ContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1271ContratoServicosIndicador_Numero), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2051ContratoServicosIndicador_Sigla", StringUtil.RTrim( Z2051ContratoServicosIndicador_Sigla));
         GxWebStd.gx_hidden_field( context, "Z1308ContratoServicosIndicador_Tipo", StringUtil.RTrim( Z1308ContratoServicosIndicador_Tipo));
         GxWebStd.gx_hidden_field( context, "Z1309ContratoServicosIndicador_Periodicidade", StringUtil.RTrim( Z1309ContratoServicosIndicador_Periodicidade));
         GxWebStd.gx_hidden_field( context, "Z1310ContratoServicosIndicador_Vigencia", Z1310ContratoServicosIndicador_Vigencia);
         GxWebStd.gx_hidden_field( context, "Z1345ContratoServicosIndicador_CalculoSob", StringUtil.RTrim( Z1345ContratoServicosIndicador_CalculoSob));
         GxWebStd.gx_hidden_field( context, "Z2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2052ContratoServicosIndicador_Formato), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1270ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.NToC( (decimal)(O1298ContratoServicosIndicador_QtdeFaixas), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_80_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOSINDICADOR_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_QTDEFAIXAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_COMQTDEFAIXAS", A1395ContratoServicosIndicador_ComQtdeFaixas);
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOSINDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATOSERVICOSINDICADOR_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV16AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV16AuditingObject);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV18Pgmname));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_UND", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOSINDICADOR_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosIndicador_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Width", StringUtil.RTrim( Gxuitabspanel_tabs_Width));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Cls", StringUtil.RTrim( Gxuitabspanel_tabs_Cls));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Enabled", StringUtil.BoolToStr( Gxuitabspanel_tabs_Enabled));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autowidth));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autoheight));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tabs_Designtimetabs));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoServicosIndicador";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV18Pgmname, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicosindicador:[SendSecurityCheck value for]"+"ContratoServicosIndicador_Codigo:"+context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contratoservicosindicador:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratoservicosindicador:[SendSecurityCheck value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV18Pgmname, "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoservicosindicador.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicosIndicador_Codigo) + "," + UrlEncode("" +AV14ContratoServicosIndicador_CntSrvCod) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosIndicador" ;
      }

      public override String GetPgmdesc( )
      {
         return "Indicador" ;
      }

      protected void InitializeNonKey3H154( )
      {
         A1270ContratoServicosIndicador_CntSrvCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
         AV16AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A1395ContratoServicosIndicador_ComQtdeFaixas = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1395ContratoServicosIndicador_ComQtdeFaixas", A1395ContratoServicosIndicador_ComQtdeFaixas);
         A1296ContratoServicosIndicador_ContratoCod = 0;
         n1296ContratoServicosIndicador_ContratoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1296ContratoServicosIndicador_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0)));
         A1295ContratoServicosIndicador_AreaTrabalhoCod = 0;
         n1295ContratoServicosIndicador_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1295ContratoServicosIndicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0)));
         A1271ContratoServicosIndicador_Numero = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1271ContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0)));
         A1274ContratoServicosIndicador_Indicador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1274ContratoServicosIndicador_Indicador", A1274ContratoServicosIndicador_Indicador);
         A2051ContratoServicosIndicador_Sigla = "";
         n2051ContratoServicosIndicador_Sigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2051ContratoServicosIndicador_Sigla", A2051ContratoServicosIndicador_Sigla);
         n2051ContratoServicosIndicador_Sigla = (String.IsNullOrEmpty(StringUtil.RTrim( A2051ContratoServicosIndicador_Sigla)) ? true : false);
         A1305ContratoServicosIndicador_Finalidade = "";
         n1305ContratoServicosIndicador_Finalidade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1305ContratoServicosIndicador_Finalidade", A1305ContratoServicosIndicador_Finalidade);
         n1305ContratoServicosIndicador_Finalidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1305ContratoServicosIndicador_Finalidade)) ? true : false);
         A1306ContratoServicosIndicador_Meta = "";
         n1306ContratoServicosIndicador_Meta = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1306ContratoServicosIndicador_Meta", A1306ContratoServicosIndicador_Meta);
         n1306ContratoServicosIndicador_Meta = (String.IsNullOrEmpty(StringUtil.RTrim( A1306ContratoServicosIndicador_Meta)) ? true : false);
         A1307ContratoServicosIndicador_InstrumentoMedicao = "";
         n1307ContratoServicosIndicador_InstrumentoMedicao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1307ContratoServicosIndicador_InstrumentoMedicao", A1307ContratoServicosIndicador_InstrumentoMedicao);
         n1307ContratoServicosIndicador_InstrumentoMedicao = (String.IsNullOrEmpty(StringUtil.RTrim( A1307ContratoServicosIndicador_InstrumentoMedicao)) ? true : false);
         A1308ContratoServicosIndicador_Tipo = "";
         n1308ContratoServicosIndicador_Tipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1308ContratoServicosIndicador_Tipo", A1308ContratoServicosIndicador_Tipo);
         n1308ContratoServicosIndicador_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A1308ContratoServicosIndicador_Tipo)) ? true : false);
         A1309ContratoServicosIndicador_Periodicidade = "";
         n1309ContratoServicosIndicador_Periodicidade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1309ContratoServicosIndicador_Periodicidade", A1309ContratoServicosIndicador_Periodicidade);
         n1309ContratoServicosIndicador_Periodicidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1309ContratoServicosIndicador_Periodicidade)) ? true : false);
         A1310ContratoServicosIndicador_Vigencia = "";
         n1310ContratoServicosIndicador_Vigencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1310ContratoServicosIndicador_Vigencia", A1310ContratoServicosIndicador_Vigencia);
         n1310ContratoServicosIndicador_Vigencia = (String.IsNullOrEmpty(StringUtil.RTrim( A1310ContratoServicosIndicador_Vigencia)) ? true : false);
         A1345ContratoServicosIndicador_CalculoSob = "";
         n1345ContratoServicosIndicador_CalculoSob = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
         n1345ContratoServicosIndicador_CalculoSob = (String.IsNullOrEmpty(StringUtil.RTrim( A1345ContratoServicosIndicador_CalculoSob)) ? true : false);
         A2052ContratoServicosIndicador_Formato = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
         A1298ContratoServicosIndicador_QtdeFaixas = 0;
         n1298ContratoServicosIndicador_QtdeFaixas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         O1298ContratoServicosIndicador_QtdeFaixas = A1298ContratoServicosIndicador_QtdeFaixas;
         n1298ContratoServicosIndicador_QtdeFaixas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         Z1271ContratoServicosIndicador_Numero = 0;
         Z2051ContratoServicosIndicador_Sigla = "";
         Z1308ContratoServicosIndicador_Tipo = "";
         Z1309ContratoServicosIndicador_Periodicidade = "";
         Z1310ContratoServicosIndicador_Vigencia = "";
         Z1345ContratoServicosIndicador_CalculoSob = "";
         Z2052ContratoServicosIndicador_Formato = 0;
         Z1270ContratoServicosIndicador_CntSrvCod = 0;
      }

      protected void InitAll3H154( )
      {
         A1269ContratoServicosIndicador_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
         InitializeNonKey3H154( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey3H159( )
      {
         A1304ContratoServicosIndicadorFaixa_Und = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1304ContratoServicosIndicadorFaixa_Und", StringUtil.LTrim( StringUtil.Str( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0)));
         A1303ContratoServicosIndicadorFaixa_Reduz = 0;
         A1302ContratoServicosIndicadorFaixa_Ate = 0;
         A1301ContratoServicosIndicadorFaixa_Desde = 0;
         A1300ContratoServicosIndicadorFaixa_Numero = 0;
         Z1304ContratoServicosIndicadorFaixa_Und = 0;
         Z1303ContratoServicosIndicadorFaixa_Reduz = 0;
         Z1302ContratoServicosIndicadorFaixa_Ate = 0;
         Z1301ContratoServicosIndicadorFaixa_Desde = 0;
         Z1300ContratoServicosIndicadorFaixa_Numero = 0;
      }

      protected void InitAll3H159( )
      {
         A1299ContratoServicosIndicadorFaixa_Codigo = 0;
         InitializeNonKey3H159( ) ;
      }

      protected void StandaloneModalInsert3H159( )
      {
         A1298ContratoServicosIndicador_QtdeFaixas = i1298ContratoServicosIndicador_QtdeFaixas;
         n1298ContratoServicosIndicador_QtdeFaixas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1298ContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)));
         A1304ContratoServicosIndicadorFaixa_Und = i1304ContratoServicosIndicadorFaixa_Und;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1304ContratoServicosIndicadorFaixa_Und", StringUtil.LTrim( StringUtil.Str( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0)));
         A1303ContratoServicosIndicadorFaixa_Reduz = i1303ContratoServicosIndicadorFaixa_Reduz;
         A1302ContratoServicosIndicadorFaixa_Ate = i1302ContratoServicosIndicadorFaixa_Ate;
         A1301ContratoServicosIndicadorFaixa_Desde = i1301ContratoServicosIndicadorFaixa_Desde;
         A1300ContratoServicosIndicadorFaixa_Numero = i1300ContratoServicosIndicadorFaixa_Numero;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299312426");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoservicosindicador.js", "?20205299312426");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_level_properties159( )
      {
         edtContratoServicosIndicadorFaixa_Codigo_Enabled = defedtContratoServicosIndicadorFaixa_Codigo_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicadorFaixa_Codigo_Enabled), 5, 0)));
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratoservicosindicador_numero_Internalname = "TEXTBLOCKCONTRATOSERVICOSINDICADOR_NUMERO";
         edtContratoServicosIndicador_Numero_Internalname = "CONTRATOSERVICOSINDICADOR_NUMERO";
         lblTextblockcontratoservicosindicador_tipo_Internalname = "TEXTBLOCKCONTRATOSERVICOSINDICADOR_TIPO";
         cmbContratoServicosIndicador_Tipo_Internalname = "CONTRATOSERVICOSINDICADOR_TIPO";
         lblTextblockcontratoservicosindicador_indicador_Internalname = "TEXTBLOCKCONTRATOSERVICOSINDICADOR_INDICADOR";
         edtContratoServicosIndicador_Indicador_Internalname = "CONTRATOSERVICOSINDICADOR_INDICADOR";
         lblTextblockcontratoservicosindicador_sigla_Internalname = "TEXTBLOCKCONTRATOSERVICOSINDICADOR_SIGLA";
         edtContratoServicosIndicador_Sigla_Internalname = "CONTRATOSERVICOSINDICADOR_SIGLA";
         lblTextblockcontratoservicosindicador_finalidade_Internalname = "TEXTBLOCKCONTRATOSERVICOSINDICADOR_FINALIDADE";
         edtContratoServicosIndicador_Finalidade_Internalname = "CONTRATOSERVICOSINDICADOR_FINALIDADE";
         lblTextblockcontratoservicosindicador_meta_Internalname = "TEXTBLOCKCONTRATOSERVICOSINDICADOR_META";
         edtContratoServicosIndicador_Meta_Internalname = "CONTRATOSERVICOSINDICADOR_META";
         lblTextblockcontratoservicosindicador_instrumentomedicao_Internalname = "TEXTBLOCKCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO";
         edtContratoServicosIndicador_InstrumentoMedicao_Internalname = "CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO";
         lblTextblockcontratoservicosindicador_periodicidade_Internalname = "TEXTBLOCKCONTRATOSERVICOSINDICADOR_PERIODICIDADE";
         cmbContratoServicosIndicador_Periodicidade_Internalname = "CONTRATOSERVICOSINDICADOR_PERIODICIDADE";
         lblTextblockcontratoservicosindicador_calculosob_Internalname = "TEXTBLOCKCONTRATOSERVICOSINDICADOR_CALCULOSOB";
         cmbContratoServicosIndicador_CalculoSob_Internalname = "CONTRATOSERVICOSINDICADOR_CALCULOSOB";
         lblTextblockcontratoservicosindicador_vigencia_Internalname = "TEXTBLOCKCONTRATOSERVICOSINDICADOR_VIGENCIA";
         edtContratoServicosIndicador_Vigencia_Internalname = "CONTRATOSERVICOSINDICADOR_VIGENCIA";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         lblTextblockcontratoservicosindicador_formato_Internalname = "TEXTBLOCKCONTRATOSERVICOSINDICADOR_FORMATO";
         cmbContratoServicosIndicador_Formato_Internalname = "CONTRATOSERVICOSINDICADOR_FORMATO";
         edtContratoServicosIndicadorFaixa_Codigo_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_CODIGO";
         edtContratoServicosIndicadorFaixa_Numero_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_NUMERO";
         edtContratoServicosIndicadorFaixa_Desde_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_DESDE";
         edtavLabel_Internalname = "vLABEL";
         edtContratoServicosIndicadorFaixa_Ate_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_ATE";
         edtContratoServicosIndicadorFaixa_Reduz_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_REDUZ";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         Gxuitabspanel_tabs_Internalname = "GXUITABSPANEL_TABS";
         lblTbjava_Internalname = "TBJAVA";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratoServicosIndicador_Codigo_Internalname = "CONTRATOSERVICOSINDICADOR_CODIGO";
         edtContratoServicosIndicador_CntSrvCod_Internalname = "CONTRATOSERVICOSINDICADOR_CNTSRVCOD";
         Form.Internalname = "FORM";
         subGridlevel_faixas_Internalname = "GRIDLEVEL_FAIXAS";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Gxuitabspanel_tabs_Designtimetabs = "[{\"id\":\"TabDados\"},{\"id\":\"TabFaixas\"}]";
         Gxuitabspanel_tabs_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tabs_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tabs_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tabs_Cls = "gx_usercontrol tab-container";
         Gxuitabspanel_tabs_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Indicador";
         edtContratoServicosIndicadorFaixa_Reduz_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Ate_Jsonclick = "";
         edtavLabel_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Desde_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Numero_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Codigo_Jsonclick = "";
         subGridlevel_faixas_Class = "WorkWithBorder WorkWith";
         edtContratoServicosIndicador_Vigencia_Jsonclick = "";
         edtContratoServicosIndicador_Vigencia_Enabled = 1;
         cmbContratoServicosIndicador_CalculoSob_Jsonclick = "";
         cmbContratoServicosIndicador_CalculoSob.Enabled = 1;
         cmbContratoServicosIndicador_Periodicidade_Jsonclick = "";
         cmbContratoServicosIndicador_Periodicidade.Enabled = 1;
         edtContratoServicosIndicador_InstrumentoMedicao_Enabled = 1;
         edtContratoServicosIndicador_Meta_Enabled = 1;
         edtContratoServicosIndicador_Finalidade_Enabled = 1;
         edtContratoServicosIndicador_Sigla_Jsonclick = "";
         edtContratoServicosIndicador_Sigla_Enabled = 1;
         edtContratoServicosIndicador_Indicador_Enabled = 1;
         cmbContratoServicosIndicador_Tipo_Jsonclick = "";
         cmbContratoServicosIndicador_Tipo.Enabled = 1;
         edtContratoServicosIndicador_Numero_Jsonclick = "";
         edtContratoServicosIndicador_Numero_Enabled = 1;
         subGridlevel_faixas_Allowcollapsing = 0;
         subGridlevel_faixas_Allowselection = 0;
         edtContratoServicosIndicadorFaixa_Reduz_Enabled = 1;
         edtContratoServicosIndicadorFaixa_Ate_Enabled = 1;
         edtavLabel_Enabled = 0;
         edtContratoServicosIndicadorFaixa_Desde_Enabled = 1;
         edtContratoServicosIndicadorFaixa_Desde_Title = "Dias";
         edtContratoServicosIndicadorFaixa_Numero_Enabled = 1;
         edtContratoServicosIndicadorFaixa_Codigo_Enabled = 0;
         subGridlevel_faixas_Backcolorstyle = 3;
         cmbContratoServicosIndicador_Formato_Jsonclick = "";
         cmbContratoServicosIndicador_Formato.Enabled = 1;
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContratoServicosIndicador_CntSrvCod_Jsonclick = "";
         edtContratoServicosIndicador_CntSrvCod_Enabled = 1;
         edtContratoServicosIndicador_CntSrvCod_Visible = 1;
         edtContratoServicosIndicador_Codigo_Jsonclick = "";
         edtContratoServicosIndicador_Codigo_Enabled = 0;
         edtContratoServicosIndicador_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void XC_11_3H154( wwpbaseobjects.SdtAuditingObject AV16AuditingObject ,
                                  int A1269ContratoServicosIndicador_Codigo ,
                                  String Gx_mode )
      {
         new loadauditcontratoservicosindicador(context ).execute(  "Y", ref  AV16AuditingObject,  A1269ContratoServicosIndicador_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV16AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_12_3H154( wwpbaseobjects.SdtAuditingObject AV16AuditingObject ,
                                  int A1269ContratoServicosIndicador_Codigo ,
                                  String Gx_mode )
      {
         new loadauditcontratoservicosindicador(context ).execute(  "Y", ref  AV16AuditingObject,  A1269ContratoServicosIndicador_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV16AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_13_3H154( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV16AuditingObject ,
                                  int A1269ContratoServicosIndicador_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratoservicosindicador(context ).execute(  "N", ref  AV16AuditingObject,  A1269ContratoServicosIndicador_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV16AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_14_3H154( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV16AuditingObject ,
                                  int A1269ContratoServicosIndicador_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratoservicosindicador(context ).execute(  "N", ref  AV16AuditingObject,  A1269ContratoServicosIndicador_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV16AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void gxnrGridlevel_faixas_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         SubsflControlProps_80159( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal3H159( ) ;
            standaloneModal3H159( ) ;
            cmbContratoServicosIndicador_Tipo.Name = "CONTRATOSERVICOSINDICADOR_TIPO";
            cmbContratoServicosIndicador_Tipo.WebTags = "";
            cmbContratoServicosIndicador_Tipo.addItem("", "(Nenhum)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("P", "Pontualidade (demanda)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("PL", "Frequ�ncia de atrasos (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("D", "Diverg�ncias (demanda)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("FD", "Frequ�ncia de diverg�ncias (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("FP", "Frequ�ncia de pend�ncias (�ndice de rejei��o de demandas) (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("AC", "Ajustes de contagens (qtd) (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("AV", "Ajustes de contagens (soma do valor bruto) (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("T", "Tempestividade", 0);
            if ( cmbContratoServicosIndicador_Tipo.ItemCount > 0 )
            {
               A1308ContratoServicosIndicador_Tipo = cmbContratoServicosIndicador_Tipo.getValidValue(A1308ContratoServicosIndicador_Tipo);
               n1308ContratoServicosIndicador_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1308ContratoServicosIndicador_Tipo", A1308ContratoServicosIndicador_Tipo);
            }
            cmbContratoServicosIndicador_Periodicidade.Name = "CONTRATOSERVICOSINDICADOR_PERIODICIDADE";
            cmbContratoServicosIndicador_Periodicidade.WebTags = "";
            cmbContratoServicosIndicador_Periodicidade.addItem("M", "Mensal", 0);
            if ( cmbContratoServicosIndicador_Periodicidade.ItemCount > 0 )
            {
               A1309ContratoServicosIndicador_Periodicidade = cmbContratoServicosIndicador_Periodicidade.getValidValue(A1309ContratoServicosIndicador_Periodicidade);
               n1309ContratoServicosIndicador_Periodicidade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1309ContratoServicosIndicador_Periodicidade", A1309ContratoServicosIndicador_Periodicidade);
            }
            cmbContratoServicosIndicador_CalculoSob.Name = "CONTRATOSERVICOSINDICADOR_CALCULOSOB";
            cmbContratoServicosIndicador_CalculoSob.WebTags = "";
            cmbContratoServicosIndicador_CalculoSob.addItem("", "(Nenhum)", 0);
            cmbContratoServicosIndicador_CalculoSob.addItem("D", "Dias", 0);
            cmbContratoServicosIndicador_CalculoSob.addItem("P", "Percentuais", 0);
            if ( cmbContratoServicosIndicador_CalculoSob.ItemCount > 0 )
            {
               A1345ContratoServicosIndicador_CalculoSob = cmbContratoServicosIndicador_CalculoSob.getValidValue(A1345ContratoServicosIndicador_CalculoSob);
               n1345ContratoServicosIndicador_CalculoSob = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
            }
            cmbContratoServicosIndicador_Formato.Name = "CONTRATOSERVICOSINDICADOR_FORMATO";
            cmbContratoServicosIndicador_Formato.WebTags = "";
            cmbContratoServicosIndicador_Formato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Porcentagem", 0);
            cmbContratoServicosIndicador_Formato.addItem("1", "Numeral", 0);
            if ( cmbContratoServicosIndicador_Formato.ItemCount > 0 )
            {
               A2052ContratoServicosIndicador_Formato = (short)(NumberUtil.Val( cmbContratoServicosIndicador_Formato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
            }
            dynload_actions( ) ;
            SendRow3H159( ) ;
            nGXsfl_80_idx = (short)(nGXsfl_80_idx+1);
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_80159( ) ;
         }
         context.GX_webresponse.AddString(Gridlevel_faixasContainer.ToJavascriptSource());
         /* End function gxnrGridlevel_faixas_newrow */
      }

      public void Valid_Contratoservicosindicador_codigo( int GX_Parm1 ,
                                                          short GX_Parm2 )
      {
         A1269ContratoServicosIndicador_Codigo = GX_Parm1;
         A1298ContratoServicosIndicador_QtdeFaixas = GX_Parm2;
         n1298ContratoServicosIndicador_QtdeFaixas = false;
         /* Using cursor T003H23 */
         pr_default.execute(17, new Object[] {A1269ContratoServicosIndicador_Codigo});
         if ( (pr_default.getStatus(17) != 101) )
         {
            A1298ContratoServicosIndicador_QtdeFaixas = T003H23_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = T003H23_n1298ContratoServicosIndicador_QtdeFaixas[0];
         }
         else
         {
            A1298ContratoServicosIndicador_QtdeFaixas = 0;
            n1298ContratoServicosIndicador_QtdeFaixas = false;
         }
         pr_default.close(17);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1298ContratoServicosIndicador_QtdeFaixas = 0;
            n1298ContratoServicosIndicador_QtdeFaixas = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratoservicosindicador_calculosob( GXCombobox cmbGX_Parm1 )
      {
         cmbContratoServicosIndicador_CalculoSob = cmbGX_Parm1;
         A1345ContratoServicosIndicador_CalculoSob = cmbContratoServicosIndicador_CalculoSob.CurrentValue;
         n1345ContratoServicosIndicador_CalculoSob = false;
         edtContratoServicosIndicadorFaixa_Desde_Title = ((StringUtil.StrCmp(A1345ContratoServicosIndicador_CalculoSob, "D")==0) ? "Dias" : "Percentuais");
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(edtContratoServicosIndicadorFaixa_Desde_Title);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratoservicosindicador_cntsrvcod( int GX_Parm1 ,
                                                             int GX_Parm2 ,
                                                             int GX_Parm3 )
      {
         A1270ContratoServicosIndicador_CntSrvCod = GX_Parm1;
         A1296ContratoServicosIndicador_ContratoCod = GX_Parm2;
         n1296ContratoServicosIndicador_ContratoCod = false;
         A1295ContratoServicosIndicador_AreaTrabalhoCod = GX_Parm3;
         n1295ContratoServicosIndicador_AreaTrabalhoCod = false;
         /* Using cursor T003H24 */
         pr_default.execute(18, new Object[] {A1270ContratoServicosIndicador_CntSrvCod});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Indicador_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSINDICADOR_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosIndicador_CntSrvCod_Internalname;
         }
         A1296ContratoServicosIndicador_ContratoCod = T003H24_A1296ContratoServicosIndicador_ContratoCod[0];
         n1296ContratoServicosIndicador_ContratoCod = T003H24_n1296ContratoServicosIndicador_ContratoCod[0];
         pr_default.close(18);
         /* Using cursor T003H25 */
         pr_default.execute(19, new Object[] {n1296ContratoServicosIndicador_ContratoCod, A1296ContratoServicosIndicador_ContratoCod});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Indicador_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1295ContratoServicosIndicador_AreaTrabalhoCod = T003H25_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
         n1295ContratoServicosIndicador_AreaTrabalhoCod = T003H25_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
         pr_default.close(19);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1296ContratoServicosIndicador_ContratoCod = 0;
            n1296ContratoServicosIndicador_ContratoCod = false;
            A1295ContratoServicosIndicador_AreaTrabalhoCod = 0;
            n1295ContratoServicosIndicador_AreaTrabalhoCod = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoServicosIndicador_Codigo',fld:'vCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV14ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E133H2',iparms:[{av:'AV16AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV18Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E143H2',iparms:[{av:'AV14ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("CONTRATOSERVICOSINDICADOR_CALCULOSOB.CLICK","{handler:'E113H154',iparms:[{av:'A1345ContratoServicosIndicador_CalculoSob',fld:'CONTRATOSERVICOSINDICADOR_CALCULOSOB',pic:'',nv:''}],oparms:[{av:'edtContratoServicosIndicadorFaixa_Desde_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Title'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(3);
         pr_default.close(18);
         pr_default.close(19);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z2051ContratoServicosIndicador_Sigla = "";
         Z1308ContratoServicosIndicador_Tipo = "";
         Z1309ContratoServicosIndicador_Periodicidade = "";
         Z1310ContratoServicosIndicador_Vigencia = "";
         Z1345ContratoServicosIndicador_CalculoSob = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15Label = "";
         GXKey = "";
         A1308ContratoServicosIndicador_Tipo = "";
         A1309ContratoServicosIndicador_Periodicidade = "";
         A1345ContratoServicosIndicador_CalculoSob = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTbjava_Jsonclick = "";
         lblTextblockcontratoservicosindicador_formato_Jsonclick = "";
         Gridlevel_faixasContainer = new GXWebGrid( context);
         Gridlevel_faixasColumn = new GXWebColumn();
         sMode159 = "";
         lblTextblockcontratoservicosindicador_numero_Jsonclick = "";
         lblTextblockcontratoservicosindicador_tipo_Jsonclick = "";
         lblTextblockcontratoservicosindicador_indicador_Jsonclick = "";
         A1274ContratoServicosIndicador_Indicador = "";
         lblTextblockcontratoservicosindicador_sigla_Jsonclick = "";
         A2051ContratoServicosIndicador_Sigla = "";
         lblTextblockcontratoservicosindicador_finalidade_Jsonclick = "";
         A1305ContratoServicosIndicador_Finalidade = "";
         lblTextblockcontratoservicosindicador_meta_Jsonclick = "";
         A1306ContratoServicosIndicador_Meta = "";
         lblTextblockcontratoservicosindicador_instrumentomedicao_Jsonclick = "";
         A1307ContratoServicosIndicador_InstrumentoMedicao = "";
         lblTextblockcontratoservicosindicador_periodicidade_Jsonclick = "";
         lblTextblockcontratoservicosindicador_calculosob_Jsonclick = "";
         lblTextblockcontratoservicosindicador_vigencia_Jsonclick = "";
         A1310ContratoServicosIndicador_Vigencia = "";
         A1395ContratoServicosIndicador_ComQtdeFaixas = "";
         AV16AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV18Pgmname = "";
         Gxuitabspanel_tabs_Height = "";
         Gxuitabspanel_tabs_Class = "";
         Gxuitabspanel_tabs_Activetabid = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode154 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1274ContratoServicosIndicador_Indicador = "";
         Z1305ContratoServicosIndicador_Finalidade = "";
         Z1306ContratoServicosIndicador_Meta = "";
         Z1307ContratoServicosIndicador_InstrumentoMedicao = "";
         T003H9_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         T003H9_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         T003H6_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         T003H6_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         T003H7_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         T003H7_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         T003H11_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H11_A1271ContratoServicosIndicador_Numero = new short[1] ;
         T003H11_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         T003H11_A2051ContratoServicosIndicador_Sigla = new String[] {""} ;
         T003H11_n2051ContratoServicosIndicador_Sigla = new bool[] {false} ;
         T003H11_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         T003H11_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         T003H11_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         T003H11_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         T003H11_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         T003H11_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         T003H11_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         T003H11_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         T003H11_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         T003H11_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         T003H11_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         T003H11_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         T003H11_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         T003H11_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         T003H11_A2052ContratoServicosIndicador_Formato = new short[1] ;
         T003H11_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         T003H11_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         T003H11_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         T003H11_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         T003H11_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         T003H11_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         T003H11_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         T003H13_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         T003H13_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         T003H14_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         T003H14_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         T003H15_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         T003H15_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         T003H16_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H5_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H5_A1271ContratoServicosIndicador_Numero = new short[1] ;
         T003H5_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         T003H5_A2051ContratoServicosIndicador_Sigla = new String[] {""} ;
         T003H5_n2051ContratoServicosIndicador_Sigla = new bool[] {false} ;
         T003H5_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         T003H5_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         T003H5_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         T003H5_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         T003H5_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         T003H5_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         T003H5_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         T003H5_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         T003H5_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         T003H5_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         T003H5_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         T003H5_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         T003H5_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         T003H5_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         T003H5_A2052ContratoServicosIndicador_Formato = new short[1] ;
         T003H5_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         T003H17_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H18_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H4_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H4_A1271ContratoServicosIndicador_Numero = new short[1] ;
         T003H4_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         T003H4_A2051ContratoServicosIndicador_Sigla = new String[] {""} ;
         T003H4_n2051ContratoServicosIndicador_Sigla = new bool[] {false} ;
         T003H4_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         T003H4_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         T003H4_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         T003H4_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         T003H4_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         T003H4_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         T003H4_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         T003H4_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         T003H4_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         T003H4_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         T003H4_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         T003H4_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         T003H4_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         T003H4_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         T003H4_A2052ContratoServicosIndicador_Formato = new short[1] ;
         T003H4_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         T003H19_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H23_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         T003H23_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         T003H24_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         T003H24_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         T003H25_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         T003H25_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         T003H26_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T003H26_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         T003H27_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H28_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H28_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         T003H28_A1304ContratoServicosIndicadorFaixa_Und = new short[1] ;
         T003H28_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         T003H28_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         T003H28_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         T003H28_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         GXCCtl = "";
         T003H29_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H29_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         T003H3_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H3_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         T003H3_A1304ContratoServicosIndicadorFaixa_Und = new short[1] ;
         T003H3_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         T003H3_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         T003H3_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         T003H3_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         T003H2_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H2_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         T003H2_A1304ContratoServicosIndicadorFaixa_Und = new short[1] ;
         T003H2_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         T003H2_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         T003H2_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         T003H2_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         T003H33_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T003H33_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         Gridlevel_faixasRow = new GXWebRow();
         subGridlevel_faixas_Linesclass = "";
         ROClassString = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosindicador__default(),
            new Object[][] {
                new Object[] {
               T003H2_A1269ContratoServicosIndicador_Codigo, T003H2_A1299ContratoServicosIndicadorFaixa_Codigo, T003H2_A1304ContratoServicosIndicadorFaixa_Und, T003H2_A1303ContratoServicosIndicadorFaixa_Reduz, T003H2_A1302ContratoServicosIndicadorFaixa_Ate, T003H2_A1301ContratoServicosIndicadorFaixa_Desde, T003H2_A1300ContratoServicosIndicadorFaixa_Numero
               }
               , new Object[] {
               T003H3_A1269ContratoServicosIndicador_Codigo, T003H3_A1299ContratoServicosIndicadorFaixa_Codigo, T003H3_A1304ContratoServicosIndicadorFaixa_Und, T003H3_A1303ContratoServicosIndicadorFaixa_Reduz, T003H3_A1302ContratoServicosIndicadorFaixa_Ate, T003H3_A1301ContratoServicosIndicadorFaixa_Desde, T003H3_A1300ContratoServicosIndicadorFaixa_Numero
               }
               , new Object[] {
               T003H4_A1269ContratoServicosIndicador_Codigo, T003H4_A1271ContratoServicosIndicador_Numero, T003H4_A1274ContratoServicosIndicador_Indicador, T003H4_A2051ContratoServicosIndicador_Sigla, T003H4_n2051ContratoServicosIndicador_Sigla, T003H4_A1305ContratoServicosIndicador_Finalidade, T003H4_n1305ContratoServicosIndicador_Finalidade, T003H4_A1306ContratoServicosIndicador_Meta, T003H4_n1306ContratoServicosIndicador_Meta, T003H4_A1307ContratoServicosIndicador_InstrumentoMedicao,
               T003H4_n1307ContratoServicosIndicador_InstrumentoMedicao, T003H4_A1308ContratoServicosIndicador_Tipo, T003H4_n1308ContratoServicosIndicador_Tipo, T003H4_A1309ContratoServicosIndicador_Periodicidade, T003H4_n1309ContratoServicosIndicador_Periodicidade, T003H4_A1310ContratoServicosIndicador_Vigencia, T003H4_n1310ContratoServicosIndicador_Vigencia, T003H4_A1345ContratoServicosIndicador_CalculoSob, T003H4_n1345ContratoServicosIndicador_CalculoSob, T003H4_A2052ContratoServicosIndicador_Formato,
               T003H4_A1270ContratoServicosIndicador_CntSrvCod
               }
               , new Object[] {
               T003H5_A1269ContratoServicosIndicador_Codigo, T003H5_A1271ContratoServicosIndicador_Numero, T003H5_A1274ContratoServicosIndicador_Indicador, T003H5_A2051ContratoServicosIndicador_Sigla, T003H5_n2051ContratoServicosIndicador_Sigla, T003H5_A1305ContratoServicosIndicador_Finalidade, T003H5_n1305ContratoServicosIndicador_Finalidade, T003H5_A1306ContratoServicosIndicador_Meta, T003H5_n1306ContratoServicosIndicador_Meta, T003H5_A1307ContratoServicosIndicador_InstrumentoMedicao,
               T003H5_n1307ContratoServicosIndicador_InstrumentoMedicao, T003H5_A1308ContratoServicosIndicador_Tipo, T003H5_n1308ContratoServicosIndicador_Tipo, T003H5_A1309ContratoServicosIndicador_Periodicidade, T003H5_n1309ContratoServicosIndicador_Periodicidade, T003H5_A1310ContratoServicosIndicador_Vigencia, T003H5_n1310ContratoServicosIndicador_Vigencia, T003H5_A1345ContratoServicosIndicador_CalculoSob, T003H5_n1345ContratoServicosIndicador_CalculoSob, T003H5_A2052ContratoServicosIndicador_Formato,
               T003H5_A1270ContratoServicosIndicador_CntSrvCod
               }
               , new Object[] {
               T003H6_A1296ContratoServicosIndicador_ContratoCod, T003H6_n1296ContratoServicosIndicador_ContratoCod
               }
               , new Object[] {
               T003H7_A1295ContratoServicosIndicador_AreaTrabalhoCod, T003H7_n1295ContratoServicosIndicador_AreaTrabalhoCod
               }
               , new Object[] {
               T003H9_A1298ContratoServicosIndicador_QtdeFaixas, T003H9_n1298ContratoServicosIndicador_QtdeFaixas
               }
               , new Object[] {
               T003H11_A1269ContratoServicosIndicador_Codigo, T003H11_A1271ContratoServicosIndicador_Numero, T003H11_A1274ContratoServicosIndicador_Indicador, T003H11_A2051ContratoServicosIndicador_Sigla, T003H11_n2051ContratoServicosIndicador_Sigla, T003H11_A1305ContratoServicosIndicador_Finalidade, T003H11_n1305ContratoServicosIndicador_Finalidade, T003H11_A1306ContratoServicosIndicador_Meta, T003H11_n1306ContratoServicosIndicador_Meta, T003H11_A1307ContratoServicosIndicador_InstrumentoMedicao,
               T003H11_n1307ContratoServicosIndicador_InstrumentoMedicao, T003H11_A1308ContratoServicosIndicador_Tipo, T003H11_n1308ContratoServicosIndicador_Tipo, T003H11_A1309ContratoServicosIndicador_Periodicidade, T003H11_n1309ContratoServicosIndicador_Periodicidade, T003H11_A1310ContratoServicosIndicador_Vigencia, T003H11_n1310ContratoServicosIndicador_Vigencia, T003H11_A1345ContratoServicosIndicador_CalculoSob, T003H11_n1345ContratoServicosIndicador_CalculoSob, T003H11_A2052ContratoServicosIndicador_Formato,
               T003H11_A1270ContratoServicosIndicador_CntSrvCod, T003H11_A1296ContratoServicosIndicador_ContratoCod, T003H11_n1296ContratoServicosIndicador_ContratoCod, T003H11_A1295ContratoServicosIndicador_AreaTrabalhoCod, T003H11_n1295ContratoServicosIndicador_AreaTrabalhoCod, T003H11_A1298ContratoServicosIndicador_QtdeFaixas, T003H11_n1298ContratoServicosIndicador_QtdeFaixas
               }
               , new Object[] {
               T003H13_A1298ContratoServicosIndicador_QtdeFaixas, T003H13_n1298ContratoServicosIndicador_QtdeFaixas
               }
               , new Object[] {
               T003H14_A1296ContratoServicosIndicador_ContratoCod, T003H14_n1296ContratoServicosIndicador_ContratoCod
               }
               , new Object[] {
               T003H15_A1295ContratoServicosIndicador_AreaTrabalhoCod, T003H15_n1295ContratoServicosIndicador_AreaTrabalhoCod
               }
               , new Object[] {
               T003H16_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               T003H17_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               T003H18_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               T003H19_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003H23_A1298ContratoServicosIndicador_QtdeFaixas, T003H23_n1298ContratoServicosIndicador_QtdeFaixas
               }
               , new Object[] {
               T003H24_A1296ContratoServicosIndicador_ContratoCod, T003H24_n1296ContratoServicosIndicador_ContratoCod
               }
               , new Object[] {
               T003H25_A1295ContratoServicosIndicador_AreaTrabalhoCod, T003H25_n1295ContratoServicosIndicador_AreaTrabalhoCod
               }
               , new Object[] {
               T003H26_A1314ContagemResultadoIndicadores_DemandaCod, T003H26_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               T003H27_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               T003H28_A1269ContratoServicosIndicador_Codigo, T003H28_A1299ContratoServicosIndicadorFaixa_Codigo, T003H28_A1304ContratoServicosIndicadorFaixa_Und, T003H28_A1303ContratoServicosIndicadorFaixa_Reduz, T003H28_A1302ContratoServicosIndicadorFaixa_Ate, T003H28_A1301ContratoServicosIndicadorFaixa_Desde, T003H28_A1300ContratoServicosIndicadorFaixa_Numero
               }
               , new Object[] {
               T003H29_A1269ContratoServicosIndicador_Codigo, T003H29_A1299ContratoServicosIndicadorFaixa_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003H33_A1269ContratoServicosIndicador_Codigo, T003H33_A1299ContratoServicosIndicadorFaixa_Codigo
               }
            }
         );
         Z1304ContratoServicosIndicadorFaixa_Und = 0;
         A1304ContratoServicosIndicadorFaixa_Und = 0;
         i1304ContratoServicosIndicadorFaixa_Und = 0;
         Z1303ContratoServicosIndicadorFaixa_Reduz = 0;
         A1303ContratoServicosIndicadorFaixa_Reduz = 0;
         i1303ContratoServicosIndicadorFaixa_Reduz = 0;
         Z1302ContratoServicosIndicadorFaixa_Ate = 0;
         A1302ContratoServicosIndicadorFaixa_Ate = 0;
         i1302ContratoServicosIndicadorFaixa_Ate = 0;
         Z1301ContratoServicosIndicadorFaixa_Desde = 0;
         A1301ContratoServicosIndicadorFaixa_Desde = 0;
         i1301ContratoServicosIndicadorFaixa_Desde = 0;
         Z1300ContratoServicosIndicadorFaixa_Numero = 0;
         A1300ContratoServicosIndicadorFaixa_Numero = 0;
         i1300ContratoServicosIndicadorFaixa_Numero = 0;
         AV18Pgmname = "ContratoServicosIndicador";
      }

      private short Z1271ContratoServicosIndicador_Numero ;
      private short Z2052ContratoServicosIndicador_Formato ;
      private short O1298ContratoServicosIndicador_QtdeFaixas ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short Z1304ContratoServicosIndicadorFaixa_Und ;
      private short Z1300ContratoServicosIndicadorFaixa_Numero ;
      private short nRcdDeleted_159 ;
      private short nRcdExists_159 ;
      private short nIsMod_159 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A2052ContratoServicosIndicador_Formato ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGridlevel_faixas_Backcolorstyle ;
      private short A1300ContratoServicosIndicadorFaixa_Numero ;
      private short subGridlevel_faixas_Allowselection ;
      private short subGridlevel_faixas_Allowhovering ;
      private short subGridlevel_faixas_Allowcollapsing ;
      private short subGridlevel_faixas_Collapsed ;
      private short nBlankRcdCount159 ;
      private short RcdFound159 ;
      private short B1298ContratoServicosIndicador_QtdeFaixas ;
      private short A1298ContratoServicosIndicador_QtdeFaixas ;
      private short nBlankRcdUsr159 ;
      private short A1271ContratoServicosIndicador_Numero ;
      private short Gx_BScreen ;
      private short A1304ContratoServicosIndicadorFaixa_Und ;
      private short RcdFound154 ;
      private short s1298ContratoServicosIndicador_QtdeFaixas ;
      private short GX_JID ;
      private short Z1298ContratoServicosIndicador_QtdeFaixas ;
      private short subGridlevel_faixas_Backstyle ;
      private short gxajaxcallmode ;
      private short i1298ContratoServicosIndicador_QtdeFaixas ;
      private short i1304ContratoServicosIndicadorFaixa_Und ;
      private short i1300ContratoServicosIndicadorFaixa_Numero ;
      private short wbTemp ;
      private int wcpOAV7ContratoServicosIndicador_Codigo ;
      private int wcpOAV14ContratoServicosIndicador_CntSrvCod ;
      private int Z1269ContratoServicosIndicador_Codigo ;
      private int Z1270ContratoServicosIndicador_CntSrvCod ;
      private int N1270ContratoServicosIndicador_CntSrvCod ;
      private int Z1299ContratoServicosIndicadorFaixa_Codigo ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1296ContratoServicosIndicador_ContratoCod ;
      private int AV7ContratoServicosIndicador_Codigo ;
      private int AV14ContratoServicosIndicador_CntSrvCod ;
      private int trnEnded ;
      private int edtContratoServicosIndicador_Codigo_Enabled ;
      private int edtContratoServicosIndicador_Codigo_Visible ;
      private int edtContratoServicosIndicador_CntSrvCod_Visible ;
      private int edtContratoServicosIndicador_CntSrvCod_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int lblTbjava_Visible ;
      private int A1299ContratoServicosIndicadorFaixa_Codigo ;
      private int edtContratoServicosIndicadorFaixa_Codigo_Enabled ;
      private int edtContratoServicosIndicadorFaixa_Numero_Enabled ;
      private int edtContratoServicosIndicadorFaixa_Desde_Enabled ;
      private int edtavLabel_Enabled ;
      private int edtContratoServicosIndicadorFaixa_Ate_Enabled ;
      private int edtContratoServicosIndicadorFaixa_Reduz_Enabled ;
      private int subGridlevel_faixas_Selectioncolor ;
      private int subGridlevel_faixas_Hoveringcolor ;
      private int fRowAdded ;
      private int edtContratoServicosIndicador_Numero_Enabled ;
      private int edtContratoServicosIndicador_Indicador_Enabled ;
      private int edtContratoServicosIndicador_Sigla_Enabled ;
      private int edtContratoServicosIndicador_Finalidade_Enabled ;
      private int edtContratoServicosIndicador_Meta_Enabled ;
      private int edtContratoServicosIndicador_InstrumentoMedicao_Enabled ;
      private int edtContratoServicosIndicador_Vigencia_Enabled ;
      private int AV11Insert_ContratoServicosIndicador_CntSrvCod ;
      private int A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int Gxuitabspanel_tabs_Selectedtabindex ;
      private int AV19GXV1 ;
      private int Z1296ContratoServicosIndicador_ContratoCod ;
      private int Z1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int subGridlevel_faixas_Backcolor ;
      private int subGridlevel_faixas_Allbackcolor ;
      private int defedtContratoServicosIndicadorFaixa_Codigo_Enabled ;
      private int idxLst ;
      private long GRIDLEVEL_FAIXAS_nFirstRecordOnPage ;
      private decimal Z1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal Z1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal Z1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal A1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal i1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal i1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal i1301ContratoServicosIndicadorFaixa_Desde ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z2051ContratoServicosIndicador_Sigla ;
      private String Z1308ContratoServicosIndicador_Tipo ;
      private String Z1309ContratoServicosIndicador_Periodicidade ;
      private String Z1345ContratoServicosIndicador_CalculoSob ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_80_idx="0001" ;
      private String AV15Label ;
      private String edtavLabel_Internalname ;
      private String Gx_mode ;
      private String GXKey ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private String A1309ContratoServicosIndicador_Periodicidade ;
      private String A1345ContratoServicosIndicador_CalculoSob ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoServicosIndicador_Numero_Internalname ;
      private String edtContratoServicosIndicador_Codigo_Internalname ;
      private String edtContratoServicosIndicador_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtContratoServicosIndicador_CntSrvCod_Internalname ;
      private String edtContratoServicosIndicador_CntSrvCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockcontratoservicosindicador_formato_Internalname ;
      private String lblTextblockcontratoservicosindicador_formato_Jsonclick ;
      private String cmbContratoServicosIndicador_Formato_Internalname ;
      private String cmbContratoServicosIndicador_Formato_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Desde_Title ;
      private String sMode159 ;
      private String edtContratoServicosIndicadorFaixa_Codigo_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Numero_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Desde_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Ate_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Reduz_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblockcontratoservicosindicador_numero_Internalname ;
      private String lblTextblockcontratoservicosindicador_numero_Jsonclick ;
      private String edtContratoServicosIndicador_Numero_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_tipo_Internalname ;
      private String lblTextblockcontratoservicosindicador_tipo_Jsonclick ;
      private String cmbContratoServicosIndicador_Tipo_Internalname ;
      private String cmbContratoServicosIndicador_Tipo_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_indicador_Internalname ;
      private String lblTextblockcontratoservicosindicador_indicador_Jsonclick ;
      private String edtContratoServicosIndicador_Indicador_Internalname ;
      private String lblTextblockcontratoservicosindicador_sigla_Internalname ;
      private String lblTextblockcontratoservicosindicador_sigla_Jsonclick ;
      private String edtContratoServicosIndicador_Sigla_Internalname ;
      private String A2051ContratoServicosIndicador_Sigla ;
      private String edtContratoServicosIndicador_Sigla_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_finalidade_Internalname ;
      private String lblTextblockcontratoservicosindicador_finalidade_Jsonclick ;
      private String edtContratoServicosIndicador_Finalidade_Internalname ;
      private String lblTextblockcontratoservicosindicador_meta_Internalname ;
      private String lblTextblockcontratoservicosindicador_meta_Jsonclick ;
      private String edtContratoServicosIndicador_Meta_Internalname ;
      private String lblTextblockcontratoservicosindicador_instrumentomedicao_Internalname ;
      private String lblTextblockcontratoservicosindicador_instrumentomedicao_Jsonclick ;
      private String edtContratoServicosIndicador_InstrumentoMedicao_Internalname ;
      private String lblTextblockcontratoservicosindicador_periodicidade_Internalname ;
      private String lblTextblockcontratoservicosindicador_periodicidade_Jsonclick ;
      private String cmbContratoServicosIndicador_Periodicidade_Internalname ;
      private String cmbContratoServicosIndicador_Periodicidade_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_calculosob_Internalname ;
      private String lblTextblockcontratoservicosindicador_calculosob_Jsonclick ;
      private String cmbContratoServicosIndicador_CalculoSob_Internalname ;
      private String cmbContratoServicosIndicador_CalculoSob_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_vigencia_Internalname ;
      private String lblTextblockcontratoservicosindicador_vigencia_Jsonclick ;
      private String edtContratoServicosIndicador_Vigencia_Internalname ;
      private String edtContratoServicosIndicador_Vigencia_Jsonclick ;
      private String AV18Pgmname ;
      private String Gxuitabspanel_tabs_Width ;
      private String Gxuitabspanel_tabs_Height ;
      private String Gxuitabspanel_tabs_Cls ;
      private String Gxuitabspanel_tabs_Class ;
      private String Gxuitabspanel_tabs_Activetabid ;
      private String Gxuitabspanel_tabs_Designtimetabs ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode154 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String subGridlevel_faixas_Class ;
      private String subGridlevel_faixas_Linesclass ;
      private String ROClassString ;
      private String edtContratoServicosIndicadorFaixa_Codigo_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Numero_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Desde_Jsonclick ;
      private String edtavLabel_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Ate_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Reduz_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gxuitabspanel_tabs_Internalname ;
      private String subGridlevel_faixas_Internalname ;
      private bool entryPointCalled ;
      private bool n1296ContratoServicosIndicador_ContratoCod ;
      private bool toggleJsOutput ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private bool n1309ContratoServicosIndicador_Periodicidade ;
      private bool n1345ContratoServicosIndicador_CalculoSob ;
      private bool wbErr ;
      private bool n1298ContratoServicosIndicador_QtdeFaixas ;
      private bool n2051ContratoServicosIndicador_Sigla ;
      private bool n1305ContratoServicosIndicador_Finalidade ;
      private bool n1306ContratoServicosIndicador_Meta ;
      private bool n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool n1310ContratoServicosIndicador_Vigencia ;
      private bool n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool Gxuitabspanel_tabs_Enabled ;
      private bool Gxuitabspanel_tabs_Autowidth ;
      private bool Gxuitabspanel_tabs_Autoheight ;
      private bool Gxuitabspanel_tabs_Autoscroll ;
      private bool Gxuitabspanel_tabs_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A1274ContratoServicosIndicador_Indicador ;
      private String A1305ContratoServicosIndicador_Finalidade ;
      private String A1306ContratoServicosIndicador_Meta ;
      private String A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String Z1274ContratoServicosIndicador_Indicador ;
      private String Z1305ContratoServicosIndicador_Finalidade ;
      private String Z1306ContratoServicosIndicador_Meta ;
      private String Z1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String Z1310ContratoServicosIndicador_Vigencia ;
      private String A1310ContratoServicosIndicador_Vigencia ;
      private String A1395ContratoServicosIndicador_ComQtdeFaixas ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid Gridlevel_faixasContainer ;
      private GXWebRow Gridlevel_faixasRow ;
      private GXWebColumn Gridlevel_faixasColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_ContratoServicosIndicador_CntSrvCod ;
      private GXCombobox cmbContratoServicosIndicador_Tipo ;
      private GXCombobox cmbContratoServicosIndicador_Periodicidade ;
      private GXCombobox cmbContratoServicosIndicador_CalculoSob ;
      private GXCombobox cmbContratoServicosIndicador_Formato ;
      private IDataStoreProvider pr_default ;
      private short[] T003H9_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] T003H9_n1298ContratoServicosIndicador_QtdeFaixas ;
      private int[] T003H6_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] T003H6_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] T003H7_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] T003H7_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int[] T003H11_A1269ContratoServicosIndicador_Codigo ;
      private short[] T003H11_A1271ContratoServicosIndicador_Numero ;
      private String[] T003H11_A1274ContratoServicosIndicador_Indicador ;
      private String[] T003H11_A2051ContratoServicosIndicador_Sigla ;
      private bool[] T003H11_n2051ContratoServicosIndicador_Sigla ;
      private String[] T003H11_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] T003H11_n1305ContratoServicosIndicador_Finalidade ;
      private String[] T003H11_A1306ContratoServicosIndicador_Meta ;
      private bool[] T003H11_n1306ContratoServicosIndicador_Meta ;
      private String[] T003H11_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] T003H11_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] T003H11_A1308ContratoServicosIndicador_Tipo ;
      private bool[] T003H11_n1308ContratoServicosIndicador_Tipo ;
      private String[] T003H11_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] T003H11_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] T003H11_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] T003H11_n1310ContratoServicosIndicador_Vigencia ;
      private String[] T003H11_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] T003H11_n1345ContratoServicosIndicador_CalculoSob ;
      private short[] T003H11_A2052ContratoServicosIndicador_Formato ;
      private int[] T003H11_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] T003H11_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] T003H11_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] T003H11_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] T003H11_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private short[] T003H11_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] T003H11_n1298ContratoServicosIndicador_QtdeFaixas ;
      private short[] T003H13_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] T003H13_n1298ContratoServicosIndicador_QtdeFaixas ;
      private int[] T003H14_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] T003H14_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] T003H15_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] T003H15_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int[] T003H16_A1269ContratoServicosIndicador_Codigo ;
      private int[] T003H5_A1269ContratoServicosIndicador_Codigo ;
      private short[] T003H5_A1271ContratoServicosIndicador_Numero ;
      private String[] T003H5_A1274ContratoServicosIndicador_Indicador ;
      private String[] T003H5_A2051ContratoServicosIndicador_Sigla ;
      private bool[] T003H5_n2051ContratoServicosIndicador_Sigla ;
      private String[] T003H5_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] T003H5_n1305ContratoServicosIndicador_Finalidade ;
      private String[] T003H5_A1306ContratoServicosIndicador_Meta ;
      private bool[] T003H5_n1306ContratoServicosIndicador_Meta ;
      private String[] T003H5_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] T003H5_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] T003H5_A1308ContratoServicosIndicador_Tipo ;
      private bool[] T003H5_n1308ContratoServicosIndicador_Tipo ;
      private String[] T003H5_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] T003H5_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] T003H5_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] T003H5_n1310ContratoServicosIndicador_Vigencia ;
      private String[] T003H5_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] T003H5_n1345ContratoServicosIndicador_CalculoSob ;
      private short[] T003H5_A2052ContratoServicosIndicador_Formato ;
      private int[] T003H5_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] T003H17_A1269ContratoServicosIndicador_Codigo ;
      private int[] T003H18_A1269ContratoServicosIndicador_Codigo ;
      private int[] T003H4_A1269ContratoServicosIndicador_Codigo ;
      private short[] T003H4_A1271ContratoServicosIndicador_Numero ;
      private String[] T003H4_A1274ContratoServicosIndicador_Indicador ;
      private String[] T003H4_A2051ContratoServicosIndicador_Sigla ;
      private bool[] T003H4_n2051ContratoServicosIndicador_Sigla ;
      private String[] T003H4_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] T003H4_n1305ContratoServicosIndicador_Finalidade ;
      private String[] T003H4_A1306ContratoServicosIndicador_Meta ;
      private bool[] T003H4_n1306ContratoServicosIndicador_Meta ;
      private String[] T003H4_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] T003H4_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] T003H4_A1308ContratoServicosIndicador_Tipo ;
      private bool[] T003H4_n1308ContratoServicosIndicador_Tipo ;
      private String[] T003H4_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] T003H4_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] T003H4_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] T003H4_n1310ContratoServicosIndicador_Vigencia ;
      private String[] T003H4_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] T003H4_n1345ContratoServicosIndicador_CalculoSob ;
      private short[] T003H4_A2052ContratoServicosIndicador_Formato ;
      private int[] T003H4_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] T003H19_A1269ContratoServicosIndicador_Codigo ;
      private short[] T003H23_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] T003H23_n1298ContratoServicosIndicador_QtdeFaixas ;
      private int[] T003H24_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] T003H24_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] T003H25_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] T003H25_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int[] T003H26_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T003H26_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] T003H27_A1269ContratoServicosIndicador_Codigo ;
      private int[] T003H28_A1269ContratoServicosIndicador_Codigo ;
      private int[] T003H28_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private short[] T003H28_A1304ContratoServicosIndicadorFaixa_Und ;
      private decimal[] T003H28_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal[] T003H28_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] T003H28_A1301ContratoServicosIndicadorFaixa_Desde ;
      private short[] T003H28_A1300ContratoServicosIndicadorFaixa_Numero ;
      private int[] T003H29_A1269ContratoServicosIndicador_Codigo ;
      private int[] T003H29_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private int[] T003H3_A1269ContratoServicosIndicador_Codigo ;
      private int[] T003H3_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private short[] T003H3_A1304ContratoServicosIndicadorFaixa_Und ;
      private decimal[] T003H3_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal[] T003H3_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] T003H3_A1301ContratoServicosIndicadorFaixa_Desde ;
      private short[] T003H3_A1300ContratoServicosIndicadorFaixa_Numero ;
      private int[] T003H2_A1269ContratoServicosIndicador_Codigo ;
      private int[] T003H2_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private short[] T003H2_A1304ContratoServicosIndicadorFaixa_Und ;
      private decimal[] T003H2_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal[] T003H2_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] T003H2_A1301ContratoServicosIndicadorFaixa_Desde ;
      private short[] T003H2_A1300ContratoServicosIndicadorFaixa_Numero ;
      private int[] T003H33_A1269ContratoServicosIndicador_Codigo ;
      private int[] T003H33_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtAuditingObject AV16AuditingObject ;
   }

   public class contratoservicosindicador__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new UpdateCursor(def[25])
         ,new UpdateCursor(def[26])
         ,new ForEachCursor(def[27])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003H11 ;
          prmT003H11 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H9 ;
          prmT003H9 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H6 ;
          prmT003H6 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H7 ;
          prmT003H7 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H13 ;
          prmT003H13 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H14 ;
          prmT003H14 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H15 ;
          prmT003H15 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H16 ;
          prmT003H16 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H5 ;
          prmT003H5 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H17 ;
          prmT003H17 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H18 ;
          prmT003H18 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H4 ;
          prmT003H4 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H19 ;
          prmT003H19 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosIndicador_Indicador",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@ContratoServicosIndicador_Finalidade",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Meta",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_InstrumentoMedicao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Tipo",SqlDbType.Char,2,0} ,
          new Object[] {"@ContratoServicosIndicador_Periodicidade",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosIndicador_Vigencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContratoServicosIndicador_CalculoSob",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosIndicador_Formato",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H20 ;
          prmT003H20 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosIndicador_Indicador",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@ContratoServicosIndicador_Finalidade",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Meta",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_InstrumentoMedicao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Tipo",SqlDbType.Char,2,0} ,
          new Object[] {"@ContratoServicosIndicador_Periodicidade",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosIndicador_Vigencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContratoServicosIndicador_CalculoSob",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosIndicador_Formato",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H21 ;
          prmT003H21 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H26 ;
          prmT003H26 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H27 ;
          prmT003H27 = new Object[] {
          } ;
          Object[] prmT003H28 ;
          prmT003H28 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H29 ;
          prmT003H29 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H3 ;
          prmT003H3 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H2 ;
          prmT003H2 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H30 ;
          prmT003H30 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Und",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Ate",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Desde",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Numero",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT003H31 ;
          prmT003H31 = new Object[] {
          new Object[] {"@ContratoServicosIndicadorFaixa_Und",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Ate",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Desde",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H32 ;
          prmT003H32 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H33 ;
          prmT003H33 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H23 ;
          prmT003H23 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H24 ;
          prmT003H24 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003H25 ;
          prmT003H25 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_ContratoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003H2", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicadorFaixa_Und], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Numero] FROM [ContratoServicosIndicadorFaixas] WITH (UPDLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo AND [ContratoServicosIndicadorFaixa_Codigo] = @ContratoServicosIndicadorFaixa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H2,1,0,true,false )
             ,new CursorDef("T003H3", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicadorFaixa_Und], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Numero] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo AND [ContratoServicosIndicadorFaixa_Codigo] = @ContratoServicosIndicadorFaixa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H3,1,0,true,false )
             ,new CursorDef("T003H4", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicador_Numero], [ContratoServicosIndicador_Indicador], [ContratoServicosIndicador_Sigla], [ContratoServicosIndicador_Finalidade], [ContratoServicosIndicador_Meta], [ContratoServicosIndicador_InstrumentoMedicao], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_Periodicidade], [ContratoServicosIndicador_Vigencia], [ContratoServicosIndicador_CalculoSob], [ContratoServicosIndicador_Formato], [ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod FROM [ContratoServicosIndicador] WITH (UPDLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H4,1,0,true,false )
             ,new CursorDef("T003H5", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicador_Numero], [ContratoServicosIndicador_Indicador], [ContratoServicosIndicador_Sigla], [ContratoServicosIndicador_Finalidade], [ContratoServicosIndicador_Meta], [ContratoServicosIndicador_InstrumentoMedicao], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_Periodicidade], [ContratoServicosIndicador_Vigencia], [ContratoServicosIndicador_CalculoSob], [ContratoServicosIndicador_Formato], [ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H5,1,0,true,false )
             ,new CursorDef("T003H6", "SELECT [Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosIndicador_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H6,1,0,true,false )
             ,new CursorDef("T003H7", "SELECT [Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoServicosIndicador_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H7,1,0,true,false )
             ,new CursorDef("T003H9", "SELECT COALESCE( T1.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas FROM (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (UPDLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T1 WHERE T1.[ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H9,1,0,true,false )
             ,new CursorDef("T003H11", "SELECT TM1.[ContratoServicosIndicador_Codigo], TM1.[ContratoServicosIndicador_Numero], TM1.[ContratoServicosIndicador_Indicador], TM1.[ContratoServicosIndicador_Sigla], TM1.[ContratoServicosIndicador_Finalidade], TM1.[ContratoServicosIndicador_Meta], TM1.[ContratoServicosIndicador_InstrumentoMedicao], TM1.[ContratoServicosIndicador_Tipo], TM1.[ContratoServicosIndicador_Periodicidade], TM1.[ContratoServicosIndicador_Vigencia], TM1.[ContratoServicosIndicador_CalculoSob], TM1.[ContratoServicosIndicador_Formato], TM1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T3.[Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod, T4.[Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod, COALESCE( T2.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas FROM ((([ContratoServicosIndicador] TM1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T2 ON T2.[ContratoServicosIndicador_Codigo] = TM1.[ContratoServicosIndicador_Codigo]) INNER JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = TM1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) WHERE TM1.[ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ORDER BY TM1.[ContratoServicosIndicador_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003H11,100,0,true,false )
             ,new CursorDef("T003H13", "SELECT COALESCE( T1.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas FROM (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (UPDLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T1 WHERE T1.[ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H13,1,0,true,false )
             ,new CursorDef("T003H14", "SELECT [Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosIndicador_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H14,1,0,true,false )
             ,new CursorDef("T003H15", "SELECT [Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoServicosIndicador_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H15,1,0,true,false )
             ,new CursorDef("T003H16", "SELECT [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003H16,1,0,true,false )
             ,new CursorDef("T003H17", "SELECT TOP 1 [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE ( [ContratoServicosIndicador_Codigo] > @ContratoServicosIndicador_Codigo) ORDER BY [ContratoServicosIndicador_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003H17,1,0,true,true )
             ,new CursorDef("T003H18", "SELECT TOP 1 [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE ( [ContratoServicosIndicador_Codigo] < @ContratoServicosIndicador_Codigo) ORDER BY [ContratoServicosIndicador_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003H18,1,0,true,true )
             ,new CursorDef("T003H19", "INSERT INTO [ContratoServicosIndicador]([ContratoServicosIndicador_Numero], [ContratoServicosIndicador_Indicador], [ContratoServicosIndicador_Sigla], [ContratoServicosIndicador_Finalidade], [ContratoServicosIndicador_Meta], [ContratoServicosIndicador_InstrumentoMedicao], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_Periodicidade], [ContratoServicosIndicador_Vigencia], [ContratoServicosIndicador_CalculoSob], [ContratoServicosIndicador_Formato], [ContratoServicosIndicador_CntSrvCod]) VALUES(@ContratoServicosIndicador_Numero, @ContratoServicosIndicador_Indicador, @ContratoServicosIndicador_Sigla, @ContratoServicosIndicador_Finalidade, @ContratoServicosIndicador_Meta, @ContratoServicosIndicador_InstrumentoMedicao, @ContratoServicosIndicador_Tipo, @ContratoServicosIndicador_Periodicidade, @ContratoServicosIndicador_Vigencia, @ContratoServicosIndicador_CalculoSob, @ContratoServicosIndicador_Formato, @ContratoServicosIndicador_CntSrvCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003H19)
             ,new CursorDef("T003H20", "UPDATE [ContratoServicosIndicador] SET [ContratoServicosIndicador_Numero]=@ContratoServicosIndicador_Numero, [ContratoServicosIndicador_Indicador]=@ContratoServicosIndicador_Indicador, [ContratoServicosIndicador_Sigla]=@ContratoServicosIndicador_Sigla, [ContratoServicosIndicador_Finalidade]=@ContratoServicosIndicador_Finalidade, [ContratoServicosIndicador_Meta]=@ContratoServicosIndicador_Meta, [ContratoServicosIndicador_InstrumentoMedicao]=@ContratoServicosIndicador_InstrumentoMedicao, [ContratoServicosIndicador_Tipo]=@ContratoServicosIndicador_Tipo, [ContratoServicosIndicador_Periodicidade]=@ContratoServicosIndicador_Periodicidade, [ContratoServicosIndicador_Vigencia]=@ContratoServicosIndicador_Vigencia, [ContratoServicosIndicador_CalculoSob]=@ContratoServicosIndicador_CalculoSob, [ContratoServicosIndicador_Formato]=@ContratoServicosIndicador_Formato, [ContratoServicosIndicador_CntSrvCod]=@ContratoServicosIndicador_CntSrvCod  WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo", GxErrorMask.GX_NOMASK,prmT003H20)
             ,new CursorDef("T003H21", "DELETE FROM [ContratoServicosIndicador]  WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo", GxErrorMask.GX_NOMASK,prmT003H21)
             ,new CursorDef("T003H23", "SELECT COALESCE( T1.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas FROM (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (UPDLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T1 WHERE T1.[ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H23,1,0,true,false )
             ,new CursorDef("T003H24", "SELECT [Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosIndicador_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H24,1,0,true,false )
             ,new CursorDef("T003H25", "SELECT [Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoServicosIndicador_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H25,1,0,true,false )
             ,new CursorDef("T003H26", "SELECT TOP 1 [ContagemResultadoIndicadores_DemandaCod], [ContagemResultadoIndicadores_IndicadorCod] FROM [ContagemResultadoIndicadores] WITH (NOLOCK) WHERE [ContagemResultadoIndicadores_IndicadorCod] = @ContratoServicosIndicador_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H26,1,0,true,true )
             ,new CursorDef("T003H27", "SELECT [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) ORDER BY [ContratoServicosIndicador_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003H27,100,0,true,false )
             ,new CursorDef("T003H28", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicadorFaixa_Und], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Numero] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo and [ContratoServicosIndicadorFaixa_Codigo] = @ContratoServicosIndicadorFaixa_Codigo ORDER BY [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H28,11,0,true,false )
             ,new CursorDef("T003H29", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo AND [ContratoServicosIndicadorFaixa_Codigo] = @ContratoServicosIndicadorFaixa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H29,1,0,true,false )
             ,new CursorDef("T003H30", "INSERT INTO [ContratoServicosIndicadorFaixas]([ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicadorFaixa_Und], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Numero]) VALUES(@ContratoServicosIndicador_Codigo, @ContratoServicosIndicadorFaixa_Codigo, @ContratoServicosIndicadorFaixa_Und, @ContratoServicosIndicadorFaixa_Reduz, @ContratoServicosIndicadorFaixa_Ate, @ContratoServicosIndicadorFaixa_Desde, @ContratoServicosIndicadorFaixa_Numero)", GxErrorMask.GX_NOMASK,prmT003H30)
             ,new CursorDef("T003H31", "UPDATE [ContratoServicosIndicadorFaixas] SET [ContratoServicosIndicadorFaixa_Und]=@ContratoServicosIndicadorFaixa_Und, [ContratoServicosIndicadorFaixa_Reduz]=@ContratoServicosIndicadorFaixa_Reduz, [ContratoServicosIndicadorFaixa_Ate]=@ContratoServicosIndicadorFaixa_Ate, [ContratoServicosIndicadorFaixa_Desde]=@ContratoServicosIndicadorFaixa_Desde, [ContratoServicosIndicadorFaixa_Numero]=@ContratoServicosIndicadorFaixa_Numero  WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo AND [ContratoServicosIndicadorFaixa_Codigo] = @ContratoServicosIndicadorFaixa_Codigo", GxErrorMask.GX_NOMASK,prmT003H31)
             ,new CursorDef("T003H32", "DELETE FROM [ContratoServicosIndicadorFaixas]  WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo AND [ContratoServicosIndicadorFaixa_Codigo] = @ContratoServicosIndicadorFaixa_Codigo", GxErrorMask.GX_NOMASK,prmT003H32)
             ,new CursorDef("T003H33", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ORDER BY [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003H33,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 2) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((short[]) buf[19])[0] = rslt.getShort(12) ;
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 2) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((short[]) buf[19])[0] = rslt.getShort(12) ;
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 2) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((short[]) buf[19])[0] = rslt.getShort(12) ;
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                ((int[]) buf[21])[0] = rslt.getInt(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((int[]) buf[23])[0] = rslt.getInt(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((short[]) buf[25])[0] = rslt.getShort(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[17]);
                }
                stmt.SetParameter(11, (short)parms[18]);
                stmt.SetParameter(12, (int)parms[19]);
                return;
             case 15 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[17]);
                }
                stmt.SetParameter(11, (short)parms[18]);
                stmt.SetParameter(12, (int)parms[19]);
                stmt.SetParameter(13, (int)parms[20]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                return;
             case 25 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (int)parms[5]);
                stmt.SetParameter(7, (int)parms[6]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
