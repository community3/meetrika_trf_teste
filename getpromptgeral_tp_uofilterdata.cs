/*
               File: GetPromptGeral_Tp_UOFilterData
        Description: Get Prompt Geral_Tp_UOFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:4.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptgeral_tp_uofilterdata : GXProcedure
   {
      public getpromptgeral_tp_uofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptgeral_tp_uofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptgeral_tp_uofilterdata objgetpromptgeral_tp_uofilterdata;
         objgetpromptgeral_tp_uofilterdata = new getpromptgeral_tp_uofilterdata();
         objgetpromptgeral_tp_uofilterdata.AV14DDOName = aP0_DDOName;
         objgetpromptgeral_tp_uofilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetpromptgeral_tp_uofilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptgeral_tp_uofilterdata.AV18OptionsJson = "" ;
         objgetpromptgeral_tp_uofilterdata.AV21OptionsDescJson = "" ;
         objgetpromptgeral_tp_uofilterdata.AV23OptionIndexesJson = "" ;
         objgetpromptgeral_tp_uofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptgeral_tp_uofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptgeral_tp_uofilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptgeral_tp_uofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_TPUO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTPUO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("PromptGeral_Tp_UOGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptGeral_Tp_UOGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("PromptGeral_Tp_UOGridState"), "");
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TPUO_ATIVO") == 0 )
            {
               AV30TpUo_Ativo = BooleanUtil.Val( AV28GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFTPUO_NOME") == 0 )
            {
               AV10TFTpUo_Nome = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFTPUO_NOME_SEL") == 0 )
            {
               AV11TFTpUo_Nome_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
         if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(1));
            AV31DynamicFiltersSelector1 = AV29GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV31DynamicFiltersSelector1, "TPUO_NOME") == 0 )
            {
               AV32TpUo_Nome1 = AV29GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV33DynamicFiltersEnabled2 = true;
               AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(2));
               AV34DynamicFiltersSelector2 = AV29GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "TPUO_NOME") == 0 )
               {
                  AV35TpUo_Nome2 = AV29GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV36DynamicFiltersEnabled3 = true;
                  AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(3));
                  AV37DynamicFiltersSelector3 = AV29GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV37DynamicFiltersSelector3, "TPUO_NOME") == 0 )
                  {
                     AV38TpUo_Nome3 = AV29GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADTPUO_NOMEOPTIONS' Routine */
         AV10TFTpUo_Nome = AV12SearchTxt;
         AV11TFTpUo_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV31DynamicFiltersSelector1 ,
                                              AV32TpUo_Nome1 ,
                                              AV33DynamicFiltersEnabled2 ,
                                              AV34DynamicFiltersSelector2 ,
                                              AV35TpUo_Nome2 ,
                                              AV36DynamicFiltersEnabled3 ,
                                              AV37DynamicFiltersSelector3 ,
                                              AV38TpUo_Nome3 ,
                                              AV11TFTpUo_Nome_Sel ,
                                              AV10TFTpUo_Nome ,
                                              A610TpUo_Nome ,
                                              A627TpUo_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV32TpUo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV32TpUo_Nome1), 50, "%");
         lV35TpUo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV35TpUo_Nome2), 50, "%");
         lV38TpUo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV38TpUo_Nome3), 50, "%");
         lV10TFTpUo_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFTpUo_Nome), 50, "%");
         /* Using cursor P00NW2 */
         pr_default.execute(0, new Object[] {lV32TpUo_Nome1, lV35TpUo_Nome2, lV38TpUo_Nome3, lV10TFTpUo_Nome, AV11TFTpUo_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKNW2 = false;
            A627TpUo_Ativo = P00NW2_A627TpUo_Ativo[0];
            A610TpUo_Nome = P00NW2_A610TpUo_Nome[0];
            A609TpUo_Codigo = P00NW2_A609TpUo_Codigo[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00NW2_A610TpUo_Nome[0], A610TpUo_Nome) == 0 ) )
            {
               BRKNW2 = false;
               A609TpUo_Codigo = P00NW2_A609TpUo_Codigo[0];
               AV24count = (long)(AV24count+1);
               BRKNW2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A610TpUo_Nome)) )
            {
               AV16Option = A610TpUo_Nome;
               AV17Options.Add(AV16Option, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNW2 )
            {
               BRKNW2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV30TpUo_Ativo = true;
         AV10TFTpUo_Nome = "";
         AV11TFTpUo_Nome_Sel = "";
         AV29GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV31DynamicFiltersSelector1 = "";
         AV32TpUo_Nome1 = "";
         AV34DynamicFiltersSelector2 = "";
         AV35TpUo_Nome2 = "";
         AV37DynamicFiltersSelector3 = "";
         AV38TpUo_Nome3 = "";
         scmdbuf = "";
         lV32TpUo_Nome1 = "";
         lV35TpUo_Nome2 = "";
         lV38TpUo_Nome3 = "";
         lV10TFTpUo_Nome = "";
         A610TpUo_Nome = "";
         P00NW2_A627TpUo_Ativo = new bool[] {false} ;
         P00NW2_A610TpUo_Nome = new String[] {""} ;
         P00NW2_A609TpUo_Codigo = new int[1] ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptgeral_tp_uofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00NW2_A627TpUo_Ativo, P00NW2_A610TpUo_Nome, P00NW2_A609TpUo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV41GXV1 ;
      private int A609TpUo_Codigo ;
      private long AV24count ;
      private String AV10TFTpUo_Nome ;
      private String AV11TFTpUo_Nome_Sel ;
      private String AV32TpUo_Nome1 ;
      private String AV35TpUo_Nome2 ;
      private String AV38TpUo_Nome3 ;
      private String scmdbuf ;
      private String lV32TpUo_Nome1 ;
      private String lV35TpUo_Nome2 ;
      private String lV38TpUo_Nome3 ;
      private String lV10TFTpUo_Nome ;
      private String A610TpUo_Nome ;
      private bool returnInSub ;
      private bool AV30TpUo_Ativo ;
      private bool AV33DynamicFiltersEnabled2 ;
      private bool AV36DynamicFiltersEnabled3 ;
      private bool A627TpUo_Ativo ;
      private bool BRKNW2 ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV31DynamicFiltersSelector1 ;
      private String AV34DynamicFiltersSelector2 ;
      private String AV37DynamicFiltersSelector3 ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P00NW2_A627TpUo_Ativo ;
      private String[] P00NW2_A610TpUo_Nome ;
      private int[] P00NW2_A609TpUo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV29GridStateDynamicFilter ;
   }

   public class getpromptgeral_tp_uofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00NW2( IGxContext context ,
                                             String AV31DynamicFiltersSelector1 ,
                                             String AV32TpUo_Nome1 ,
                                             bool AV33DynamicFiltersEnabled2 ,
                                             String AV34DynamicFiltersSelector2 ,
                                             String AV35TpUo_Nome2 ,
                                             bool AV36DynamicFiltersEnabled3 ,
                                             String AV37DynamicFiltersSelector3 ,
                                             String AV38TpUo_Nome3 ,
                                             String AV11TFTpUo_Nome_Sel ,
                                             String AV10TFTpUo_Nome ,
                                             String A610TpUo_Nome ,
                                             bool A627TpUo_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [TpUo_Ativo], [TpUo_Nome], [TpUo_Codigo] FROM [Geral_tp_uo] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([TpUo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector1, "TPUO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TpUo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([TpUo_Nome] like '%' + @lV32TpUo_Nome1 + '%')";
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV33DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "TPUO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TpUo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([TpUo_Nome] like '%' + @lV35TpUo_Nome2 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV36DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector3, "TPUO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TpUo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([TpUo_Nome] like '%' + @lV38TpUo_Nome3 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTpUo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFTpUo_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([TpUo_Nome] like @lV10TFTpUo_Nome)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTpUo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([TpUo_Nome] = @AV11TFTpUo_Nome_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [TpUo_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00NW2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00NW2 ;
          prmP00NW2 = new Object[] {
          new Object[] {"@lV32TpUo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV35TpUo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38TpUo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFTpUo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFTpUo_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00NW2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NW2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptgeral_tp_uofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptgeral_tp_uofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptgeral_tp_uofilterdata") )
          {
             return  ;
          }
          getpromptgeral_tp_uofilterdata worker = new getpromptgeral_tp_uofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
