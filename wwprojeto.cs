/*
               File: WWProjeto
        Description:  Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:49:4.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwprojeto : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwprojeto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwprojeto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavProjeto_areatrabalhocodigo = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavProjeto_tecnicacontagem1 = new GXCombobox();
         cmbavProjeto_status1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavProjeto_tecnicacontagem2 = new GXCombobox();
         cmbavProjeto_status2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavProjeto_tecnicacontagem3 = new GXCombobox();
         cmbavProjeto_status3 = new GXCombobox();
         chkProjeto_Incremental = new GXCheckbox();
         cmbProjeto_Status = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_81 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_81_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_81_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV31Projeto_Sigla1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
               AV17Projeto_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Projeto_Nome1", AV17Projeto_Nome1);
               AV33Projeto_TecnicaContagem1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
               AV34Projeto_Status1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV35Projeto_Sigla2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
               AV21Projeto_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Projeto_Nome2", AV21Projeto_Nome2);
               AV37Projeto_TecnicaContagem2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
               AV38Projeto_Status2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV39Projeto_Sigla3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
               AV25Projeto_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Projeto_Nome3", AV25Projeto_Nome3);
               AV41Projeto_TecnicaContagem3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
               AV42Projeto_Status3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV77Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV101Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV77Projeto_AreaTrabalhoCodigo, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PADF2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTDF2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202054849528");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwprojeto.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_SIGLA1", StringUtil.RTrim( AV31Projeto_Sigla1));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_NOME1", StringUtil.RTrim( AV17Projeto_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TECNICACONTAGEM1", StringUtil.RTrim( AV33Projeto_TecnicaContagem1));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_STATUS1", StringUtil.RTrim( AV34Projeto_Status1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_SIGLA2", StringUtil.RTrim( AV35Projeto_Sigla2));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_NOME2", StringUtil.RTrim( AV21Projeto_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TECNICACONTAGEM2", StringUtil.RTrim( AV37Projeto_TecnicaContagem2));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_STATUS2", StringUtil.RTrim( AV38Projeto_Status2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_SIGLA3", StringUtil.RTrim( AV39Projeto_Sigla3));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_NOME3", StringUtil.RTrim( AV25Projeto_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TECNICACONTAGEM3", StringUtil.RTrim( AV41Projeto_TecnicaContagem3));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_STATUS3", StringUtil.RTrim( AV42Projeto_Status3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_81", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_81), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV101Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEDF2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTDF2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwprojeto.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWProjeto" ;
      }

      public override String GetPgmdesc( )
      {
         return " Projeto" ;
      }

      protected void WBDF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_DF2( true) ;
         }
         else
         {
            wb_table1_2_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_81_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(97, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_81_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(98, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"");
         }
         wbLoad = true;
      }

      protected void STARTDF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Projeto", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDF0( ) ;
      }

      protected void WSDF2( )
      {
         STARTDF2( ) ;
         EVTDF2( ) ;
      }

      protected void EVTDF2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11DF2 */
                              E11DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12DF2 */
                              E12DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13DF2 */
                              E13DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14DF2 */
                              E14DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15DF2 */
                              E15DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16DF2 */
                              E16DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17DF2 */
                              E17DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18DF2 */
                              E18DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19DF2 */
                              E19DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20DF2 */
                              E20DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21DF2 */
                              E21DF2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_81_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_81_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_81_idx), 4, 0)), 4, "0");
                              SubsflControlProps_812( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV98Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV99Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              AV43Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV43Display)) ? AV100Display_GXI : context.convertURL( context.PathToRelativeUrl( AV43Display))));
                              A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProjeto_Codigo_Internalname), ",", "."));
                              A649Projeto_Nome = StringUtil.Upper( cgiGet( edtProjeto_Nome_Internalname));
                              A650Projeto_Sigla = StringUtil.Upper( cgiGet( edtProjeto_Sigla_Internalname));
                              A656Projeto_Prazo = (short)(context.localUtil.CToN( cgiGet( edtProjeto_Prazo_Internalname), ",", "."));
                              n656Projeto_Prazo = false;
                              A655Projeto_Custo = context.localUtil.CToN( cgiGet( edtProjeto_Custo_Internalname), ",", ".");
                              A1232Projeto_Incremental = StringUtil.StrToBool( cgiGet( chkProjeto_Incremental_Internalname));
                              n1232Projeto_Incremental = false;
                              A657Projeto_Esforco = (short)(context.localUtil.CToN( cgiGet( edtProjeto_Esforco_Internalname), ",", "."));
                              cmbProjeto_Status.Name = cmbProjeto_Status_Internalname;
                              cmbProjeto_Status.CurrentValue = cgiGet( cmbProjeto_Status_Internalname);
                              A658Projeto_Status = cgiGet( cmbProjeto_Status_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E22DF2 */
                                    E22DF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E23DF2 */
                                    E23DF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24DF2 */
                                    E24DF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_sigla1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA1"), AV31Projeto_Sigla1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME1"), AV17Projeto_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_tecnicacontagem1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM1"), AV33Projeto_TecnicaContagem1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_status1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS1"), AV34Projeto_Status1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_sigla2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA2"), AV35Projeto_Sigla2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME2"), AV21Projeto_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_tecnicacontagem2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM2"), AV37Projeto_TecnicaContagem2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_status2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS2"), AV38Projeto_Status2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_sigla3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA3"), AV39Projeto_Sigla3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME3"), AV25Projeto_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_tecnicacontagem3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM3"), AV41Projeto_TecnicaContagem3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Projeto_status3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS3"), AV42Projeto_Status3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PADF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavProjeto_areatrabalhocodigo.Name = "vPROJETO_AREATRABALHOCODIGO";
            dynavProjeto_areatrabalhocodigo.WebTags = "";
            dynavProjeto_areatrabalhocodigo.removeAllItems();
            /* Using cursor H00DF2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavProjeto_areatrabalhocodigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00DF2_A5AreaTrabalho_Codigo[0]), 6, 0)), H00DF2_A6AreaTrabalho_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavProjeto_areatrabalhocodigo.ItemCount > 0 )
            {
               AV77Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( dynavProjeto_areatrabalhocodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PROJETO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector1.addItem("PROJETO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("PROJETO_TECNICACONTAGEM", "T�cnica de Contagem", 0);
            cmbavDynamicfiltersselector1.addItem("PROJETO_STATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavProjeto_tecnicacontagem1.Name = "vPROJETO_TECNICACONTAGEM1";
            cmbavProjeto_tecnicacontagem1.WebTags = "";
            cmbavProjeto_tecnicacontagem1.addItem("", "Todos", 0);
            cmbavProjeto_tecnicacontagem1.addItem("1", "Indicativa", 0);
            cmbavProjeto_tecnicacontagem1.addItem("2", "Estimada", 0);
            cmbavProjeto_tecnicacontagem1.addItem("3", "Detalhada", 0);
            if ( cmbavProjeto_tecnicacontagem1.ItemCount > 0 )
            {
               AV33Projeto_TecnicaContagem1 = cmbavProjeto_tecnicacontagem1.getValidValue(AV33Projeto_TecnicaContagem1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
            }
            cmbavProjeto_status1.Name = "vPROJETO_STATUS1";
            cmbavProjeto_status1.WebTags = "";
            cmbavProjeto_status1.addItem("", "Todos", 0);
            cmbavProjeto_status1.addItem("A", "Aberto", 0);
            cmbavProjeto_status1.addItem("E", "Em Contagem", 0);
            cmbavProjeto_status1.addItem("C", "Contado", 0);
            if ( cmbavProjeto_status1.ItemCount > 0 )
            {
               AV34Projeto_Status1 = cmbavProjeto_status1.getValidValue(AV34Projeto_Status1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("PROJETO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector2.addItem("PROJETO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("PROJETO_TECNICACONTAGEM", "T�cnica de Contagem", 0);
            cmbavDynamicfiltersselector2.addItem("PROJETO_STATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavProjeto_tecnicacontagem2.Name = "vPROJETO_TECNICACONTAGEM2";
            cmbavProjeto_tecnicacontagem2.WebTags = "";
            cmbavProjeto_tecnicacontagem2.addItem("", "Todos", 0);
            cmbavProjeto_tecnicacontagem2.addItem("1", "Indicativa", 0);
            cmbavProjeto_tecnicacontagem2.addItem("2", "Estimada", 0);
            cmbavProjeto_tecnicacontagem2.addItem("3", "Detalhada", 0);
            if ( cmbavProjeto_tecnicacontagem2.ItemCount > 0 )
            {
               AV37Projeto_TecnicaContagem2 = cmbavProjeto_tecnicacontagem2.getValidValue(AV37Projeto_TecnicaContagem2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
            }
            cmbavProjeto_status2.Name = "vPROJETO_STATUS2";
            cmbavProjeto_status2.WebTags = "";
            cmbavProjeto_status2.addItem("", "Todos", 0);
            cmbavProjeto_status2.addItem("A", "Aberto", 0);
            cmbavProjeto_status2.addItem("E", "Em Contagem", 0);
            cmbavProjeto_status2.addItem("C", "Contado", 0);
            if ( cmbavProjeto_status2.ItemCount > 0 )
            {
               AV38Projeto_Status2 = cmbavProjeto_status2.getValidValue(AV38Projeto_Status2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("PROJETO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector3.addItem("PROJETO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("PROJETO_TECNICACONTAGEM", "T�cnica de Contagem", 0);
            cmbavDynamicfiltersselector3.addItem("PROJETO_STATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavProjeto_tecnicacontagem3.Name = "vPROJETO_TECNICACONTAGEM3";
            cmbavProjeto_tecnicacontagem3.WebTags = "";
            cmbavProjeto_tecnicacontagem3.addItem("", "Todos", 0);
            cmbavProjeto_tecnicacontagem3.addItem("1", "Indicativa", 0);
            cmbavProjeto_tecnicacontagem3.addItem("2", "Estimada", 0);
            cmbavProjeto_tecnicacontagem3.addItem("3", "Detalhada", 0);
            if ( cmbavProjeto_tecnicacontagem3.ItemCount > 0 )
            {
               AV41Projeto_TecnicaContagem3 = cmbavProjeto_tecnicacontagem3.getValidValue(AV41Projeto_TecnicaContagem3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
            }
            cmbavProjeto_status3.Name = "vPROJETO_STATUS3";
            cmbavProjeto_status3.WebTags = "";
            cmbavProjeto_status3.addItem("", "Todos", 0);
            cmbavProjeto_status3.addItem("A", "Aberto", 0);
            cmbavProjeto_status3.addItem("E", "Em Contagem", 0);
            cmbavProjeto_status3.addItem("C", "Contado", 0);
            if ( cmbavProjeto_status3.ItemCount > 0 )
            {
               AV42Projeto_Status3 = cmbavProjeto_status3.getValidValue(AV42Projeto_Status3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
            }
            GXCCtl = "PROJETO_INCREMENTAL_" + sGXsfl_81_idx;
            chkProjeto_Incremental.Name = GXCCtl;
            chkProjeto_Incremental.WebTags = "";
            chkProjeto_Incremental.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkProjeto_Incremental_Internalname, "TitleCaption", chkProjeto_Incremental.Caption);
            chkProjeto_Incremental.CheckedValue = "false";
            GXCCtl = "PROJETO_STATUS_" + sGXsfl_81_idx;
            cmbProjeto_Status.Name = GXCCtl;
            cmbProjeto_Status.WebTags = "";
            cmbProjeto_Status.addItem("A", "Aberto", 0);
            cmbProjeto_Status.addItem("E", "Em Contagem", 0);
            cmbProjeto_Status.addItem("C", "Contado", 0);
            if ( cmbProjeto_Status.ItemCount > 0 )
            {
               A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavProjeto_areatrabalhocodigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvPROJETO_AREATRABALHOCODIGODF1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvPROJETO_AREATRABALHOCODIGO_dataDF1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvPROJETO_AREATRABALHOCODIGO_htmlDF1( )
      {
         int gxdynajaxvalue ;
         GXDLVvPROJETO_AREATRABALHOCODIGO_dataDF1( ) ;
         gxdynajaxindex = 1;
         dynavProjeto_areatrabalhocodigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavProjeto_areatrabalhocodigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavProjeto_areatrabalhocodigo.ItemCount > 0 )
         {
            AV77Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( dynavProjeto_areatrabalhocodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0)));
         }
      }

      protected void GXDLVvPROJETO_AREATRABALHOCODIGO_dataDF1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00DF3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00DF3_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00DF3_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_812( ) ;
         while ( nGXsfl_81_idx <= nRC_GXsfl_81 )
         {
            sendrow_812( ) ;
            nGXsfl_81_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_81_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_81_idx+1));
            sGXsfl_81_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_81_idx), 4, 0)), 4, "0");
            SubsflControlProps_812( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV31Projeto_Sigla1 ,
                                       String AV17Projeto_Nome1 ,
                                       String AV33Projeto_TecnicaContagem1 ,
                                       String AV34Projeto_Status1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV35Projeto_Sigla2 ,
                                       String AV21Projeto_Nome2 ,
                                       String AV37Projeto_TecnicaContagem2 ,
                                       String AV38Projeto_Status2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV39Projeto_Sigla3 ,
                                       String AV25Projeto_Nome3 ,
                                       String AV41Projeto_TecnicaContagem3 ,
                                       String AV42Projeto_Status3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV77Projeto_AreaTrabalhoCodigo ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV101Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A648Projeto_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFDF2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "PROJETO_NOME", StringUtil.RTrim( A649Projeto_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "PROJETO_SIGLA", StringUtil.RTrim( A650Projeto_Sigla));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_INCREMENTAL", GetSecureSignedToken( "", A1232Projeto_Incremental));
         GxWebStd.gx_hidden_field( context, "PROJETO_INCREMENTAL", StringUtil.BoolToStr( A1232Projeto_Incremental));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_STATUS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
         GxWebStd.gx_hidden_field( context, "PROJETO_STATUS", StringUtil.RTrim( A658Projeto_Status));
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavProjeto_areatrabalhocodigo.ItemCount > 0 )
         {
            AV77Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( dynavProjeto_areatrabalhocodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavProjeto_tecnicacontagem1.ItemCount > 0 )
         {
            AV33Projeto_TecnicaContagem1 = cmbavProjeto_tecnicacontagem1.getValidValue(AV33Projeto_TecnicaContagem1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
         }
         if ( cmbavProjeto_status1.ItemCount > 0 )
         {
            AV34Projeto_Status1 = cmbavProjeto_status1.getValidValue(AV34Projeto_Status1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavProjeto_tecnicacontagem2.ItemCount > 0 )
         {
            AV37Projeto_TecnicaContagem2 = cmbavProjeto_tecnicacontagem2.getValidValue(AV37Projeto_TecnicaContagem2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
         }
         if ( cmbavProjeto_status2.ItemCount > 0 )
         {
            AV38Projeto_Status2 = cmbavProjeto_status2.getValidValue(AV38Projeto_Status2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavProjeto_tecnicacontagem3.ItemCount > 0 )
         {
            AV41Projeto_TecnicaContagem3 = cmbavProjeto_tecnicacontagem3.getValidValue(AV41Projeto_TecnicaContagem3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
         }
         if ( cmbavProjeto_status3.ItemCount > 0 )
         {
            AV42Projeto_Status3 = cmbavProjeto_status3.getValidValue(AV42Projeto_Status3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV101Pgmname = "WWProjeto";
         context.Gx_err = 0;
      }

      protected void RFDF2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 81;
         /* Execute user event: E23DF2 */
         E23DF2 ();
         nGXsfl_81_idx = 1;
         sGXsfl_81_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_81_idx), 4, 0)), 4, "0");
         SubsflControlProps_812( ) ;
         nGXsfl_81_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_812( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV81WWProjetoDS_2_Dynamicfiltersselector1 ,
                                                 AV82WWProjetoDS_3_Projeto_sigla1 ,
                                                 AV83WWProjetoDS_4_Projeto_nome1 ,
                                                 AV84WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                                 AV85WWProjetoDS_6_Projeto_status1 ,
                                                 AV86WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                                 AV87WWProjetoDS_8_Dynamicfiltersselector2 ,
                                                 AV88WWProjetoDS_9_Projeto_sigla2 ,
                                                 AV89WWProjetoDS_10_Projeto_nome2 ,
                                                 AV90WWProjetoDS_11_Projeto_tecnicacontagem2 ,
                                                 AV91WWProjetoDS_12_Projeto_status2 ,
                                                 AV92WWProjetoDS_13_Dynamicfiltersenabled3 ,
                                                 AV93WWProjetoDS_14_Dynamicfiltersselector3 ,
                                                 AV94WWProjetoDS_15_Projeto_sigla3 ,
                                                 AV95WWProjetoDS_16_Projeto_nome3 ,
                                                 AV96WWProjetoDS_17_Projeto_tecnicacontagem3 ,
                                                 AV97WWProjetoDS_18_Projeto_status3 ,
                                                 A650Projeto_Sigla ,
                                                 A649Projeto_Nome ,
                                                 A652Projeto_TecnicaContagem ,
                                                 A658Projeto_Status ,
                                                 A2151Projeto_AreaTrabalhoCodigo ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV82WWProjetoDS_3_Projeto_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV82WWProjetoDS_3_Projeto_sigla1), 15, "%");
            lV83WWProjetoDS_4_Projeto_nome1 = StringUtil.PadR( StringUtil.RTrim( AV83WWProjetoDS_4_Projeto_nome1), 50, "%");
            lV88WWProjetoDS_9_Projeto_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV88WWProjetoDS_9_Projeto_sigla2), 15, "%");
            lV89WWProjetoDS_10_Projeto_nome2 = StringUtil.PadR( StringUtil.RTrim( AV89WWProjetoDS_10_Projeto_nome2), 50, "%");
            lV94WWProjetoDS_15_Projeto_sigla3 = StringUtil.PadR( StringUtil.RTrim( AV94WWProjetoDS_15_Projeto_sigla3), 15, "%");
            lV95WWProjetoDS_16_Projeto_nome3 = StringUtil.PadR( StringUtil.RTrim( AV95WWProjetoDS_16_Projeto_nome3), 50, "%");
            /* Using cursor H00DF5 */
            pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV82WWProjetoDS_3_Projeto_sigla1, lV83WWProjetoDS_4_Projeto_nome1, AV84WWProjetoDS_5_Projeto_tecnicacontagem1, AV85WWProjetoDS_6_Projeto_status1, lV88WWProjetoDS_9_Projeto_sigla2, lV89WWProjetoDS_10_Projeto_nome2, AV90WWProjetoDS_11_Projeto_tecnicacontagem2, AV91WWProjetoDS_12_Projeto_status2, lV94WWProjetoDS_15_Projeto_sigla3, lV95WWProjetoDS_16_Projeto_nome3, AV96WWProjetoDS_17_Projeto_tecnicacontagem3, AV97WWProjetoDS_18_Projeto_status3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_81_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2155Projeto_SistemaCodigo = H00DF5_A2155Projeto_SistemaCodigo[0];
               n2155Projeto_SistemaCodigo = H00DF5_n2155Projeto_SistemaCodigo[0];
               A2151Projeto_AreaTrabalhoCodigo = H00DF5_A2151Projeto_AreaTrabalhoCodigo[0];
               n2151Projeto_AreaTrabalhoCodigo = H00DF5_n2151Projeto_AreaTrabalhoCodigo[0];
               A652Projeto_TecnicaContagem = H00DF5_A652Projeto_TecnicaContagem[0];
               A658Projeto_Status = H00DF5_A658Projeto_Status[0];
               A1232Projeto_Incremental = H00DF5_A1232Projeto_Incremental[0];
               n1232Projeto_Incremental = H00DF5_n1232Projeto_Incremental[0];
               A650Projeto_Sigla = H00DF5_A650Projeto_Sigla[0];
               A649Projeto_Nome = H00DF5_A649Projeto_Nome[0];
               A648Projeto_Codigo = H00DF5_A648Projeto_Codigo[0];
               A657Projeto_Esforco = H00DF5_A657Projeto_Esforco[0];
               A655Projeto_Custo = H00DF5_A655Projeto_Custo[0];
               A656Projeto_Prazo = H00DF5_A656Projeto_Prazo[0];
               n656Projeto_Prazo = H00DF5_n656Projeto_Prazo[0];
               A657Projeto_Esforco = H00DF5_A657Projeto_Esforco[0];
               A655Projeto_Custo = H00DF5_A655Projeto_Custo[0];
               A656Projeto_Prazo = H00DF5_A656Projeto_Prazo[0];
               n656Projeto_Prazo = H00DF5_n656Projeto_Prazo[0];
               /* Execute user event: E24DF2 */
               E24DF2 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 81;
            WBDF0( ) ;
         }
         nGXsfl_81_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV80WWProjetoDS_1_Projeto_areatrabalhocodigo = AV77Projeto_AreaTrabalhoCodigo;
         AV81WWProjetoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWProjetoDS_3_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV83WWProjetoDS_4_Projeto_nome1 = AV17Projeto_Nome1;
         AV84WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV85WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV86WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV89WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV90WWProjetoDS_11_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_12_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_15_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_16_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_17_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV97WWProjetoDS_18_Projeto_status3 = AV42Projeto_Status3;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV81WWProjetoDS_2_Dynamicfiltersselector1 ,
                                              AV82WWProjetoDS_3_Projeto_sigla1 ,
                                              AV83WWProjetoDS_4_Projeto_nome1 ,
                                              AV84WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                              AV85WWProjetoDS_6_Projeto_status1 ,
                                              AV86WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                              AV87WWProjetoDS_8_Dynamicfiltersselector2 ,
                                              AV88WWProjetoDS_9_Projeto_sigla2 ,
                                              AV89WWProjetoDS_10_Projeto_nome2 ,
                                              AV90WWProjetoDS_11_Projeto_tecnicacontagem2 ,
                                              AV91WWProjetoDS_12_Projeto_status2 ,
                                              AV92WWProjetoDS_13_Dynamicfiltersenabled3 ,
                                              AV93WWProjetoDS_14_Dynamicfiltersselector3 ,
                                              AV94WWProjetoDS_15_Projeto_sigla3 ,
                                              AV95WWProjetoDS_16_Projeto_nome3 ,
                                              AV96WWProjetoDS_17_Projeto_tecnicacontagem3 ,
                                              AV97WWProjetoDS_18_Projeto_status3 ,
                                              A650Projeto_Sigla ,
                                              A649Projeto_Nome ,
                                              A652Projeto_TecnicaContagem ,
                                              A658Projeto_Status ,
                                              A2151Projeto_AreaTrabalhoCodigo ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV82WWProjetoDS_3_Projeto_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV82WWProjetoDS_3_Projeto_sigla1), 15, "%");
         lV83WWProjetoDS_4_Projeto_nome1 = StringUtil.PadR( StringUtil.RTrim( AV83WWProjetoDS_4_Projeto_nome1), 50, "%");
         lV88WWProjetoDS_9_Projeto_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV88WWProjetoDS_9_Projeto_sigla2), 15, "%");
         lV89WWProjetoDS_10_Projeto_nome2 = StringUtil.PadR( StringUtil.RTrim( AV89WWProjetoDS_10_Projeto_nome2), 50, "%");
         lV94WWProjetoDS_15_Projeto_sigla3 = StringUtil.PadR( StringUtil.RTrim( AV94WWProjetoDS_15_Projeto_sigla3), 15, "%");
         lV95WWProjetoDS_16_Projeto_nome3 = StringUtil.PadR( StringUtil.RTrim( AV95WWProjetoDS_16_Projeto_nome3), 50, "%");
         /* Using cursor H00DF7 */
         pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV82WWProjetoDS_3_Projeto_sigla1, lV83WWProjetoDS_4_Projeto_nome1, AV84WWProjetoDS_5_Projeto_tecnicacontagem1, AV85WWProjetoDS_6_Projeto_status1, lV88WWProjetoDS_9_Projeto_sigla2, lV89WWProjetoDS_10_Projeto_nome2, AV90WWProjetoDS_11_Projeto_tecnicacontagem2, AV91WWProjetoDS_12_Projeto_status2, lV94WWProjetoDS_15_Projeto_sigla3, lV95WWProjetoDS_16_Projeto_nome3, AV96WWProjetoDS_17_Projeto_tecnicacontagem3, AV97WWProjetoDS_18_Projeto_status3});
         GRID_nRecordCount = H00DF7_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV80WWProjetoDS_1_Projeto_areatrabalhocodigo = AV77Projeto_AreaTrabalhoCodigo;
         AV81WWProjetoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWProjetoDS_3_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV83WWProjetoDS_4_Projeto_nome1 = AV17Projeto_Nome1;
         AV84WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV85WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV86WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV89WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV90WWProjetoDS_11_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_12_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_15_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_16_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_17_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV97WWProjetoDS_18_Projeto_status3 = AV42Projeto_Status3;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV77Projeto_AreaTrabalhoCodigo, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV80WWProjetoDS_1_Projeto_areatrabalhocodigo = AV77Projeto_AreaTrabalhoCodigo;
         AV81WWProjetoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWProjetoDS_3_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV83WWProjetoDS_4_Projeto_nome1 = AV17Projeto_Nome1;
         AV84WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV85WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV86WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV89WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV90WWProjetoDS_11_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_12_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_15_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_16_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_17_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV97WWProjetoDS_18_Projeto_status3 = AV42Projeto_Status3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV77Projeto_AreaTrabalhoCodigo, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV80WWProjetoDS_1_Projeto_areatrabalhocodigo = AV77Projeto_AreaTrabalhoCodigo;
         AV81WWProjetoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWProjetoDS_3_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV83WWProjetoDS_4_Projeto_nome1 = AV17Projeto_Nome1;
         AV84WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV85WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV86WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV89WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV90WWProjetoDS_11_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_12_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_15_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_16_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_17_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV97WWProjetoDS_18_Projeto_status3 = AV42Projeto_Status3;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV77Projeto_AreaTrabalhoCodigo, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV80WWProjetoDS_1_Projeto_areatrabalhocodigo = AV77Projeto_AreaTrabalhoCodigo;
         AV81WWProjetoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWProjetoDS_3_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV83WWProjetoDS_4_Projeto_nome1 = AV17Projeto_Nome1;
         AV84WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV85WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV86WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV89WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV90WWProjetoDS_11_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_12_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_15_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_16_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_17_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV97WWProjetoDS_18_Projeto_status3 = AV42Projeto_Status3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV77Projeto_AreaTrabalhoCodigo, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV80WWProjetoDS_1_Projeto_areatrabalhocodigo = AV77Projeto_AreaTrabalhoCodigo;
         AV81WWProjetoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWProjetoDS_3_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV83WWProjetoDS_4_Projeto_nome1 = AV17Projeto_Nome1;
         AV84WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV85WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV86WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV89WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV90WWProjetoDS_11_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_12_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_15_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_16_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_17_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV97WWProjetoDS_18_Projeto_status3 = AV42Projeto_Status3;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV77Projeto_AreaTrabalhoCodigo, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPDF0( )
      {
         /* Before Start, stand alone formulas. */
         AV101Pgmname = "WWProjeto";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22DF2 */
         E22DF2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavProjeto_areatrabalhocodigo.Name = dynavProjeto_areatrabalhocodigo_Internalname;
            dynavProjeto_areatrabalhocodigo.CurrentValue = cgiGet( dynavProjeto_areatrabalhocodigo_Internalname);
            AV77Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( cgiGet( dynavProjeto_areatrabalhocodigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV31Projeto_Sigla1 = StringUtil.Upper( cgiGet( edtavProjeto_sigla1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
            AV17Projeto_Nome1 = StringUtil.Upper( cgiGet( edtavProjeto_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Projeto_Nome1", AV17Projeto_Nome1);
            cmbavProjeto_tecnicacontagem1.Name = cmbavProjeto_tecnicacontagem1_Internalname;
            cmbavProjeto_tecnicacontagem1.CurrentValue = cgiGet( cmbavProjeto_tecnicacontagem1_Internalname);
            AV33Projeto_TecnicaContagem1 = cgiGet( cmbavProjeto_tecnicacontagem1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
            cmbavProjeto_status1.Name = cmbavProjeto_status1_Internalname;
            cmbavProjeto_status1.CurrentValue = cgiGet( cmbavProjeto_status1_Internalname);
            AV34Projeto_Status1 = cgiGet( cmbavProjeto_status1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV35Projeto_Sigla2 = StringUtil.Upper( cgiGet( edtavProjeto_sigla2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
            AV21Projeto_Nome2 = StringUtil.Upper( cgiGet( edtavProjeto_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Projeto_Nome2", AV21Projeto_Nome2);
            cmbavProjeto_tecnicacontagem2.Name = cmbavProjeto_tecnicacontagem2_Internalname;
            cmbavProjeto_tecnicacontagem2.CurrentValue = cgiGet( cmbavProjeto_tecnicacontagem2_Internalname);
            AV37Projeto_TecnicaContagem2 = cgiGet( cmbavProjeto_tecnicacontagem2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
            cmbavProjeto_status2.Name = cmbavProjeto_status2_Internalname;
            cmbavProjeto_status2.CurrentValue = cgiGet( cmbavProjeto_status2_Internalname);
            AV38Projeto_Status2 = cgiGet( cmbavProjeto_status2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV39Projeto_Sigla3 = StringUtil.Upper( cgiGet( edtavProjeto_sigla3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
            AV25Projeto_Nome3 = StringUtil.Upper( cgiGet( edtavProjeto_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Projeto_Nome3", AV25Projeto_Nome3);
            cmbavProjeto_tecnicacontagem3.Name = cmbavProjeto_tecnicacontagem3_Internalname;
            cmbavProjeto_tecnicacontagem3.CurrentValue = cgiGet( cmbavProjeto_tecnicacontagem3_Internalname);
            AV41Projeto_TecnicaContagem3 = cgiGet( cmbavProjeto_tecnicacontagem3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
            cmbavProjeto_status3.Name = cmbavProjeto_status3_Internalname;
            cmbavProjeto_status3.CurrentValue = cgiGet( cmbavProjeto_status3_Internalname);
            AV42Projeto_Status3 = cgiGet( cmbavProjeto_status3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_81 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_81"), ",", "."));
            AV75GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV76GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA1"), AV31Projeto_Sigla1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME1"), AV17Projeto_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM1"), AV33Projeto_TecnicaContagem1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS1"), AV34Projeto_Status1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA2"), AV35Projeto_Sigla2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME2"), AV21Projeto_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM2"), AV37Projeto_TecnicaContagem2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS2"), AV38Projeto_Status2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA3"), AV39Projeto_Sigla3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_NOME3"), AV25Projeto_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM3"), AV41Projeto_TecnicaContagem3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS3"), AV42Projeto_Status3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22DF2 */
         E22DF2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22DF2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV33Projeto_TecnicaContagem1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
         AV34Projeto_Status1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
         AV15DynamicFiltersSelector1 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV37Projeto_TecnicaContagem2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
         AV38Projeto_Status2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
         AV19DynamicFiltersSelector2 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV41Projeto_TecnicaContagem3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
         AV42Projeto_Status3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
         AV23DynamicFiltersSelector3 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = " Projeto";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E23DF2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV75GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75GridCurrentPage), 10, 0)));
         AV76GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76GridPageCount), 10, 0)));
         AV80WWProjetoDS_1_Projeto_areatrabalhocodigo = AV77Projeto_AreaTrabalhoCodigo;
         AV81WWProjetoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWProjetoDS_3_Projeto_sigla1 = AV31Projeto_Sigla1;
         AV83WWProjetoDS_4_Projeto_nome1 = AV17Projeto_Nome1;
         AV84WWProjetoDS_5_Projeto_tecnicacontagem1 = AV33Projeto_TecnicaContagem1;
         AV85WWProjetoDS_6_Projeto_status1 = AV34Projeto_Status1;
         AV86WWProjetoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWProjetoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWProjetoDS_9_Projeto_sigla2 = AV35Projeto_Sigla2;
         AV89WWProjetoDS_10_Projeto_nome2 = AV21Projeto_Nome2;
         AV90WWProjetoDS_11_Projeto_tecnicacontagem2 = AV37Projeto_TecnicaContagem2;
         AV91WWProjetoDS_12_Projeto_status2 = AV38Projeto_Status2;
         AV92WWProjetoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWProjetoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWProjetoDS_15_Projeto_sigla3 = AV39Projeto_Sigla3;
         AV95WWProjetoDS_16_Projeto_nome3 = AV25Projeto_Nome3;
         AV96WWProjetoDS_17_Projeto_tecnicacontagem3 = AV41Projeto_TecnicaContagem3;
         AV97WWProjetoDS_18_Projeto_status3 = AV42Projeto_Status3;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11DF2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV74PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV74PageToGo) ;
         }
      }

      private void E24DF2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A648Projeto_Codigo);
            AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV98Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV28Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV98Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A648Projeto_Codigo);
            AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV99Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV29Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV99Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         AV43Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV43Display);
         AV100Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Sub projetos, Estimativas";
         edtavDisplay_Link = formatLink("viewprojeto.aspx") + "?" + UrlEncode("" +A648Projeto_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtProjeto_Nome_Link = formatLink("viewprojeto.aspx") + "?" + UrlEncode("" +A648Projeto_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 81;
         }
         sendrow_812( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_81_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(81, GridRow);
         }
      }

      protected void E17DF2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E12DF2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV77Projeto_AreaTrabalhoCodigo, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void E18DF2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19DF2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E13DF2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV77Projeto_AreaTrabalhoCodigo, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void E20DF2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14DF2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV17Projeto_Nome1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV21Projeto_Nome2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV25Projeto_Nome3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV77Projeto_AreaTrabalhoCodigo, AV6WWPContext, AV101Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A648Projeto_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void E21DF2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15DF2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavProjeto_areatrabalhocodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavProjeto_areatrabalhocodigo_Internalname, "Values", dynavProjeto_areatrabalhocodigo.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void E16DF2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S162( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavProjeto_sigla1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla1_Visible), 5, 0)));
         edtavProjeto_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome1_Visible), 5, 0)));
         cmbavProjeto_tecnicacontagem1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem1.Visible), 5, 0)));
         cmbavProjeto_status1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 )
         {
            edtavProjeto_sigla1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_NOME") == 0 )
         {
            edtavProjeto_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 )
         {
            cmbavProjeto_tecnicacontagem1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_STATUS") == 0 )
         {
            cmbavProjeto_status1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavProjeto_sigla2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla2_Visible), 5, 0)));
         edtavProjeto_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome2_Visible), 5, 0)));
         cmbavProjeto_tecnicacontagem2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem2.Visible), 5, 0)));
         cmbavProjeto_status2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 )
         {
            edtavProjeto_sigla2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_NOME") == 0 )
         {
            edtavProjeto_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 )
         {
            cmbavProjeto_tecnicacontagem2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_STATUS") == 0 )
         {
            cmbavProjeto_status2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavProjeto_sigla3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla3_Visible), 5, 0)));
         edtavProjeto_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome3_Visible), 5, 0)));
         cmbavProjeto_tecnicacontagem3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem3.Visible), 5, 0)));
         cmbavProjeto_status3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 )
         {
            edtavProjeto_sigla3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_NOME") == 0 )
         {
            edtavProjeto_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 )
         {
            cmbavProjeto_tecnicacontagem3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_STATUS") == 0 )
         {
            cmbavProjeto_status3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV35Projeto_Sigla2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV39Projeto_Sigla3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV77Projeto_AreaTrabalhoCodigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0)));
         AV15DynamicFiltersSelector1 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV31Projeto_Sigla1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV101Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV101Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV101Pgmname+"GridState"), "");
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV102GXV1 = 1;
         while ( AV102GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV102GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "PROJETO_AREATRABALHOCODIGO") == 0 )
            {
               AV77Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0)));
            }
            AV102GXV1 = (int)(AV102GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 )
            {
               AV31Projeto_Sigla1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_NOME") == 0 )
            {
               AV17Projeto_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Projeto_Nome1", AV17Projeto_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 )
            {
               AV33Projeto_TecnicaContagem1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_STATUS") == 0 )
            {
               AV34Projeto_Status1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 )
               {
                  AV35Projeto_Sigla2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_NOME") == 0 )
               {
                  AV21Projeto_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Projeto_Nome2", AV21Projeto_Nome2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 )
               {
                  AV37Projeto_TecnicaContagem2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_STATUS") == 0 )
               {
                  AV38Projeto_Status2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 )
                  {
                     AV39Projeto_Sigla3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_NOME") == 0 )
                  {
                     AV25Projeto_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Projeto_Nome3", AV25Projeto_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 )
                  {
                     AV41Projeto_TecnicaContagem3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_STATUS") == 0 )
                  {
                     AV42Projeto_Status3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV101Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV77Projeto_AreaTrabalhoCodigo) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "PROJETO_AREATRABALHOCODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV101Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Projeto_Sigla1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31Projeto_Sigla1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Projeto_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Projeto_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Projeto_TecnicaContagem1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV33Projeto_TecnicaContagem1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_STATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Projeto_Status1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV34Projeto_Status1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Projeto_Sigla2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV35Projeto_Sigla2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Projeto_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21Projeto_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Projeto_TecnicaContagem2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV37Projeto_TecnicaContagem2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_STATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Projeto_Status2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV38Projeto_Status2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Projeto_Sigla3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV39Projeto_Sigla3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Projeto_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Projeto_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Projeto_TecnicaContagem3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV41Projeto_TecnicaContagem3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_STATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Projeto_Status3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV42Projeto_Status3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV101Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Projeto";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_DF2( true) ;
         }
         else
         {
            wb_table2_8_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_75_DF2( true) ;
         }
         else
         {
            wb_table3_75_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table3_75_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DF2e( true) ;
         }
         else
         {
            wb_table1_2_DF2e( false) ;
         }
      }

      protected void wb_table3_75_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_78_DF2( true) ;
         }
         else
         {
            wb_table4_78_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table4_78_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_75_DF2e( true) ;
         }
         else
         {
            wb_table3_75_DF2e( false) ;
         }
      }

      protected void wb_table4_78_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"81\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Projeto") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(260), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Sigla") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(34), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Prazo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Custo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Incremental") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(45), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Esfor�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Status") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV43Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A649Projeto_Nome));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtProjeto_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A650Projeto_Sigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 18, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1232Projeto_Incremental));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A658Projeto_Status));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 81 )
         {
            wbEnd = 0;
            nRC_GXsfl_81 = (short)(nGXsfl_81_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_78_DF2e( true) ;
         }
         else
         {
            wb_table4_78_DF2e( false) ;
         }
      }

      protected void wb_table2_8_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblProjetotitle_Internalname, "Projetos", "", "", lblProjetotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_DF2( true) ;
         }
         else
         {
            wb_table5_13_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_18_DF2( true) ;
         }
         else
         {
            wb_table6_18_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table6_18_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_DF2e( true) ;
         }
         else
         {
            wb_table2_8_DF2e( false) ;
         }
      }

      protected void wb_table6_18_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextprojeto_areatrabalhocodigo_Internalname, "�rea de Trabalhlho", "", "", lblFiltertextprojeto_areatrabalhocodigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavProjeto_areatrabalhocodigo, dynavProjeto_areatrabalhocodigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0)), 1, dynavProjeto_areatrabalhocodigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_WWProjeto.htm");
            dynavProjeto_areatrabalhocodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV77Projeto_AreaTrabalhoCodigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavProjeto_areatrabalhocodigo_Internalname, "Values", (String)(dynavProjeto_areatrabalhocodigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_27_DF2( true) ;
         }
         else
         {
            wb_table7_27_DF2( false) ;
         }
         return  ;
      }

      protected void wb_table7_27_DF2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_18_DF2e( true) ;
         }
         else
         {
            wb_table6_18_DF2e( false) ;
         }
      }

      protected void wb_table7_27_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", true, "HLP_WWProjeto.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla1_Internalname, StringUtil.RTrim( AV31Projeto_Sigla1), StringUtil.RTrim( context.localUtil.Format( AV31Projeto_Sigla1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_sigla1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_nome1_Internalname, StringUtil.RTrim( AV17Projeto_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Projeto_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_nome1_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWProjeto.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tecnicacontagem1, cmbavProjeto_tecnicacontagem1_Internalname, StringUtil.RTrim( AV33Projeto_TecnicaContagem1), 1, cmbavProjeto_tecnicacontagem1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tecnicacontagem1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", (String)(cmbavProjeto_tecnicacontagem1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_status1, cmbavProjeto_status1_Internalname, StringUtil.RTrim( AV34Projeto_Status1), 1, cmbavProjeto_status1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_status1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", (String)(cmbavProjeto_status1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_WWProjeto.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla2_Internalname, StringUtil.RTrim( AV35Projeto_Sigla2), StringUtil.RTrim( context.localUtil.Format( AV35Projeto_Sigla2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_sigla2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_nome2_Internalname, StringUtil.RTrim( AV21Projeto_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21Projeto_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_nome2_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWProjeto.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tecnicacontagem2, cmbavProjeto_tecnicacontagem2_Internalname, StringUtil.RTrim( AV37Projeto_TecnicaContagem2), 1, cmbavProjeto_tecnicacontagem2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tecnicacontagem2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", (String)(cmbavProjeto_tecnicacontagem2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_status2, cmbavProjeto_status2_Internalname, StringUtil.RTrim( AV38Projeto_Status2), 1, cmbavProjeto_status2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_status2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", (String)(cmbavProjeto_status2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_WWProjeto.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla3_Internalname, StringUtil.RTrim( AV39Projeto_Sigla3), StringUtil.RTrim( context.localUtil.Format( AV39Projeto_Sigla3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_sigla3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_81_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_nome3_Internalname, StringUtil.RTrim( AV25Projeto_Nome3), StringUtil.RTrim( context.localUtil.Format( AV25Projeto_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_nome3_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWProjeto.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tecnicacontagem3, cmbavProjeto_tecnicacontagem3_Internalname, StringUtil.RTrim( AV41Projeto_TecnicaContagem3), 1, cmbavProjeto_tecnicacontagem3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tecnicacontagem3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", (String)(cmbavProjeto_tecnicacontagem3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_81_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_status3, cmbavProjeto_status3_Internalname, StringUtil.RTrim( AV42Projeto_Status3), 1, cmbavProjeto_status3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_status3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_WWProjeto.htm");
            cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", (String)(cmbavProjeto_status3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_27_DF2e( true) ;
         }
         else
         {
            wb_table7_27_DF2e( false) ;
         }
      }

      protected void wb_table5_13_DF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_DF2e( true) ;
         }
         else
         {
            wb_table5_13_DF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADF2( ) ;
         WSDF2( ) ;
         WEDF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202054849834");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwprojeto.js", "?202054849834");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_812( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_81_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_81_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_81_idx;
         edtProjeto_Codigo_Internalname = "PROJETO_CODIGO_"+sGXsfl_81_idx;
         edtProjeto_Nome_Internalname = "PROJETO_NOME_"+sGXsfl_81_idx;
         edtProjeto_Sigla_Internalname = "PROJETO_SIGLA_"+sGXsfl_81_idx;
         edtProjeto_Prazo_Internalname = "PROJETO_PRAZO_"+sGXsfl_81_idx;
         edtProjeto_Custo_Internalname = "PROJETO_CUSTO_"+sGXsfl_81_idx;
         chkProjeto_Incremental_Internalname = "PROJETO_INCREMENTAL_"+sGXsfl_81_idx;
         edtProjeto_Esforco_Internalname = "PROJETO_ESFORCO_"+sGXsfl_81_idx;
         cmbProjeto_Status_Internalname = "PROJETO_STATUS_"+sGXsfl_81_idx;
      }

      protected void SubsflControlProps_fel_812( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_81_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_81_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_81_fel_idx;
         edtProjeto_Codigo_Internalname = "PROJETO_CODIGO_"+sGXsfl_81_fel_idx;
         edtProjeto_Nome_Internalname = "PROJETO_NOME_"+sGXsfl_81_fel_idx;
         edtProjeto_Sigla_Internalname = "PROJETO_SIGLA_"+sGXsfl_81_fel_idx;
         edtProjeto_Prazo_Internalname = "PROJETO_PRAZO_"+sGXsfl_81_fel_idx;
         edtProjeto_Custo_Internalname = "PROJETO_CUSTO_"+sGXsfl_81_fel_idx;
         chkProjeto_Incremental_Internalname = "PROJETO_INCREMENTAL_"+sGXsfl_81_fel_idx;
         edtProjeto_Esforco_Internalname = "PROJETO_ESFORCO_"+sGXsfl_81_fel_idx;
         cmbProjeto_Status_Internalname = "PROJETO_STATUS_"+sGXsfl_81_fel_idx;
      }

      protected void sendrow_812( )
      {
         SubsflControlProps_812( ) ;
         WBDF0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_81_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_81_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_81_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV98Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV98Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV99Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV99Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV43Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV43Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV100Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV43Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV43Display)) ? AV100Display_GXI : context.PathToRelativeUrl( AV43Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV43Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)81,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Nome_Internalname,StringUtil.RTrim( A649Projeto_Nome),StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtProjeto_Nome_Link,(String)"",(String)"",(String)"",(String)edtProjeto_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)260,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)81,(short)1,(short)-1,(short)0,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Sigla_Internalname,StringUtil.RTrim( A650Projeto_Sigla),StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)81,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Prazo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A656Projeto_Prazo), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Prazo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)34,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)81,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Custo_Internalname,StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 18, 2, ",", "")),context.localUtil.Format( A655Projeto_Custo, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Custo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)81,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkProjeto_Incremental_Internalname,StringUtil.BoolToStr( A1232Projeto_Incremental),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Esforco_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A657Projeto_Esforco), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Esforco_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)45,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)81,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_81_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "PROJETO_STATUS_" + sGXsfl_81_idx;
               cmbProjeto_Status.Name = GXCCtl;
               cmbProjeto_Status.WebTags = "";
               cmbProjeto_Status.addItem("A", "Aberto", 0);
               cmbProjeto_Status.addItem("E", "Em Contagem", 0);
               cmbProjeto_Status.addItem("C", "Contado", 0);
               if ( cmbProjeto_Status.ItemCount > 0 )
               {
                  A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbProjeto_Status,(String)cmbProjeto_Status_Internalname,StringUtil.RTrim( A658Projeto_Status),(short)1,(String)cmbProjeto_Status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbProjeto_Status.CurrentValue = StringUtil.RTrim( A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Values", (String)(cmbProjeto_Status.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_CODIGO"+"_"+sGXsfl_81_idx, GetSecureSignedToken( sGXsfl_81_idx, context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_NOME"+"_"+sGXsfl_81_idx, GetSecureSignedToken( sGXsfl_81_idx, StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_SIGLA"+"_"+sGXsfl_81_idx, GetSecureSignedToken( sGXsfl_81_idx, StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_INCREMENTAL"+"_"+sGXsfl_81_idx, GetSecureSignedToken( sGXsfl_81_idx, A1232Projeto_Incremental));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_STATUS"+"_"+sGXsfl_81_idx, GetSecureSignedToken( sGXsfl_81_idx, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_81_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_81_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_81_idx+1));
            sGXsfl_81_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_81_idx), 4, 0)), 4, "0");
            SubsflControlProps_812( ) ;
         }
         /* End function sendrow_812 */
      }

      protected void init_default_properties( )
      {
         lblProjetotitle_Internalname = "PROJETOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextprojeto_areatrabalhocodigo_Internalname = "FILTERTEXTPROJETO_AREATRABALHOCODIGO";
         dynavProjeto_areatrabalhocodigo_Internalname = "vPROJETO_AREATRABALHOCODIGO";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavProjeto_sigla1_Internalname = "vPROJETO_SIGLA1";
         edtavProjeto_nome1_Internalname = "vPROJETO_NOME1";
         cmbavProjeto_tecnicacontagem1_Internalname = "vPROJETO_TECNICACONTAGEM1";
         cmbavProjeto_status1_Internalname = "vPROJETO_STATUS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavProjeto_sigla2_Internalname = "vPROJETO_SIGLA2";
         edtavProjeto_nome2_Internalname = "vPROJETO_NOME2";
         cmbavProjeto_tecnicacontagem2_Internalname = "vPROJETO_TECNICACONTAGEM2";
         cmbavProjeto_status2_Internalname = "vPROJETO_STATUS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavProjeto_sigla3_Internalname = "vPROJETO_SIGLA3";
         edtavProjeto_nome3_Internalname = "vPROJETO_NOME3";
         cmbavProjeto_tecnicacontagem3_Internalname = "vPROJETO_TECNICACONTAGEM3";
         cmbavProjeto_status3_Internalname = "vPROJETO_STATUS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtProjeto_Codigo_Internalname = "PROJETO_CODIGO";
         edtProjeto_Nome_Internalname = "PROJETO_NOME";
         edtProjeto_Sigla_Internalname = "PROJETO_SIGLA";
         edtProjeto_Prazo_Internalname = "PROJETO_PRAZO";
         edtProjeto_Custo_Internalname = "PROJETO_CUSTO";
         chkProjeto_Incremental_Internalname = "PROJETO_INCREMENTAL";
         edtProjeto_Esforco_Internalname = "PROJETO_ESFORCO";
         cmbProjeto_Status_Internalname = "PROJETO_STATUS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbProjeto_Status_Jsonclick = "";
         edtProjeto_Esforco_Jsonclick = "";
         edtProjeto_Custo_Jsonclick = "";
         edtProjeto_Prazo_Jsonclick = "";
         edtProjeto_Sigla_Jsonclick = "";
         edtProjeto_Nome_Jsonclick = "";
         edtProjeto_Codigo_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         cmbavProjeto_status3_Jsonclick = "";
         cmbavProjeto_tecnicacontagem3_Jsonclick = "";
         edtavProjeto_nome3_Jsonclick = "";
         edtavProjeto_sigla3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavProjeto_status2_Jsonclick = "";
         cmbavProjeto_tecnicacontagem2_Jsonclick = "";
         edtavProjeto_nome2_Jsonclick = "";
         edtavProjeto_sigla2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavProjeto_status1_Jsonclick = "";
         cmbavProjeto_tecnicacontagem1_Jsonclick = "";
         edtavProjeto_nome1_Jsonclick = "";
         edtavProjeto_sigla1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavProjeto_areatrabalhocodigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtProjeto_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Sub projetos, Estimativas";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavProjeto_status3.Visible = 1;
         cmbavProjeto_tecnicacontagem3.Visible = 1;
         edtavProjeto_nome3_Visible = 1;
         edtavProjeto_sigla3_Visible = 1;
         cmbavProjeto_status2.Visible = 1;
         cmbavProjeto_tecnicacontagem2.Visible = 1;
         edtavProjeto_nome2_Visible = 1;
         edtavProjeto_sigla2_Visible = 1;
         cmbavProjeto_status1.Visible = 1;
         cmbavProjeto_tecnicacontagem1.Visible = 1;
         edtavProjeto_nome1_Visible = 1;
         edtavProjeto_sigla1_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkProjeto_Incremental.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Projeto";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV77Projeto_AreaTrabalhoCodigo',fld:'vPROJETO_AREATRABALHOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV75GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV76GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV77Projeto_AreaTrabalhoCodigo',fld:'vPROJETO_AREATRABALHOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E24DF2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'AV43Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtProjeto_Nome_Link',ctrl:'PROJETO_NOME',prop:'Link'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17DF2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E12DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV77Projeto_AreaTrabalhoCodigo',fld:'vPROJETO_AREATRABALHOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'edtavProjeto_nome2_Visible',ctrl:'vPROJETO_NOME2',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'edtavProjeto_nome3_Visible',ctrl:'vPROJETO_NOME3',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'edtavProjeto_nome1_Visible',ctrl:'vPROJETO_NOME1',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18DF2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'edtavProjeto_nome1_Visible',ctrl:'vPROJETO_NOME1',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19DF2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E13DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV77Projeto_AreaTrabalhoCodigo',fld:'vPROJETO_AREATRABALHOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'edtavProjeto_nome2_Visible',ctrl:'vPROJETO_NOME2',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'edtavProjeto_nome3_Visible',ctrl:'vPROJETO_NOME3',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'edtavProjeto_nome1_Visible',ctrl:'vPROJETO_NOME1',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20DF2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'edtavProjeto_nome2_Visible',ctrl:'vPROJETO_NOME2',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E14DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV77Projeto_AreaTrabalhoCodigo',fld:'vPROJETO_AREATRABALHOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'edtavProjeto_nome2_Visible',ctrl:'vPROJETO_NOME2',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'edtavProjeto_nome3_Visible',ctrl:'vPROJETO_NOME3',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'edtavProjeto_nome1_Visible',ctrl:'vPROJETO_NOME1',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21DF2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'edtavProjeto_nome3_Visible',ctrl:'vPROJETO_NOME3',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E15DF2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV77Projeto_AreaTrabalhoCodigo',fld:'vPROJETO_AREATRABALHOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV101Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV77Projeto_AreaTrabalhoCodigo',fld:'vPROJETO_AREATRABALHOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'edtavProjeto_nome1_Visible',ctrl:'vPROJETO_NOME1',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17Projeto_Nome1',fld:'vPROJETO_NOME1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21Projeto_Nome2',fld:'vPROJETO_NOME2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV25Projeto_Nome3',fld:'vPROJETO_NOME3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'edtavProjeto_nome2_Visible',ctrl:'vPROJETO_NOME2',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'edtavProjeto_nome3_Visible',ctrl:'vPROJETO_NOME3',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16DF2',iparms:[{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV31Projeto_Sigla1 = "";
         AV17Projeto_Nome1 = "";
         AV33Projeto_TecnicaContagem1 = "";
         AV34Projeto_Status1 = "A";
         AV19DynamicFiltersSelector2 = "";
         AV35Projeto_Sigla2 = "";
         AV21Projeto_Nome2 = "";
         AV37Projeto_TecnicaContagem2 = "";
         AV38Projeto_Status2 = "A";
         AV23DynamicFiltersSelector3 = "";
         AV39Projeto_Sigla3 = "";
         AV25Projeto_Nome3 = "";
         AV41Projeto_TecnicaContagem3 = "";
         AV42Projeto_Status3 = "A";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV101Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV98Update_GXI = "";
         AV29Delete = "";
         AV99Delete_GXI = "";
         AV43Display = "";
         AV100Display_GXI = "";
         A649Projeto_Nome = "";
         A650Projeto_Sigla = "";
         A658Projeto_Status = "";
         scmdbuf = "";
         H00DF2_A5AreaTrabalho_Codigo = new int[1] ;
         H00DF2_A6AreaTrabalho_Descricao = new String[] {""} ;
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00DF3_A5AreaTrabalho_Codigo = new int[1] ;
         H00DF3_A6AreaTrabalho_Descricao = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV82WWProjetoDS_3_Projeto_sigla1 = "";
         lV83WWProjetoDS_4_Projeto_nome1 = "";
         lV88WWProjetoDS_9_Projeto_sigla2 = "";
         lV89WWProjetoDS_10_Projeto_nome2 = "";
         lV94WWProjetoDS_15_Projeto_sigla3 = "";
         lV95WWProjetoDS_16_Projeto_nome3 = "";
         AV81WWProjetoDS_2_Dynamicfiltersselector1 = "";
         AV82WWProjetoDS_3_Projeto_sigla1 = "";
         AV83WWProjetoDS_4_Projeto_nome1 = "";
         AV84WWProjetoDS_5_Projeto_tecnicacontagem1 = "";
         AV85WWProjetoDS_6_Projeto_status1 = "";
         AV87WWProjetoDS_8_Dynamicfiltersselector2 = "";
         AV88WWProjetoDS_9_Projeto_sigla2 = "";
         AV89WWProjetoDS_10_Projeto_nome2 = "";
         AV90WWProjetoDS_11_Projeto_tecnicacontagem2 = "";
         AV91WWProjetoDS_12_Projeto_status2 = "";
         AV93WWProjetoDS_14_Dynamicfiltersselector3 = "";
         AV94WWProjetoDS_15_Projeto_sigla3 = "";
         AV95WWProjetoDS_16_Projeto_nome3 = "";
         AV96WWProjetoDS_17_Projeto_tecnicacontagem3 = "";
         AV97WWProjetoDS_18_Projeto_status3 = "";
         A652Projeto_TecnicaContagem = "";
         H00DF5_A2155Projeto_SistemaCodigo = new int[1] ;
         H00DF5_n2155Projeto_SistemaCodigo = new bool[] {false} ;
         H00DF5_A2151Projeto_AreaTrabalhoCodigo = new int[1] ;
         H00DF5_n2151Projeto_AreaTrabalhoCodigo = new bool[] {false} ;
         H00DF5_A652Projeto_TecnicaContagem = new String[] {""} ;
         H00DF5_A658Projeto_Status = new String[] {""} ;
         H00DF5_A1232Projeto_Incremental = new bool[] {false} ;
         H00DF5_n1232Projeto_Incremental = new bool[] {false} ;
         H00DF5_A650Projeto_Sigla = new String[] {""} ;
         H00DF5_A649Projeto_Nome = new String[] {""} ;
         H00DF5_A648Projeto_Codigo = new int[1] ;
         H00DF5_A657Projeto_Esforco = new short[1] ;
         H00DF5_A655Projeto_Custo = new decimal[1] ;
         H00DF5_A656Projeto_Prazo = new short[1] ;
         H00DF5_n656Projeto_Prazo = new bool[] {false} ;
         H00DF7_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblProjetotitle_Jsonclick = "";
         lblFiltertextprojeto_areatrabalhocodigo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwprojeto__default(),
            new Object[][] {
                new Object[] {
               H00DF2_A5AreaTrabalho_Codigo, H00DF2_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00DF3_A5AreaTrabalho_Codigo, H00DF3_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00DF5_A2155Projeto_SistemaCodigo, H00DF5_n2155Projeto_SistemaCodigo, H00DF5_A2151Projeto_AreaTrabalhoCodigo, H00DF5_n2151Projeto_AreaTrabalhoCodigo, H00DF5_A652Projeto_TecnicaContagem, H00DF5_A658Projeto_Status, H00DF5_A1232Projeto_Incremental, H00DF5_n1232Projeto_Incremental, H00DF5_A650Projeto_Sigla, H00DF5_A649Projeto_Nome,
               H00DF5_A648Projeto_Codigo, H00DF5_A657Projeto_Esforco, H00DF5_A655Projeto_Custo, H00DF5_A656Projeto_Prazo, H00DF5_n656Projeto_Prazo
               }
               , new Object[] {
               H00DF7_AGRID_nRecordCount
               }
            }
         );
         AV101Pgmname = "WWProjeto";
         /* GeneXus formulas. */
         AV101Pgmname = "WWProjeto";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_81 ;
      private short nGXsfl_81_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A656Projeto_Prazo ;
      private short A657Projeto_Esforco ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_81_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV77Projeto_AreaTrabalhoCodigo ;
      private int A648Projeto_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A2151Projeto_AreaTrabalhoCodigo ;
      private int A2155Projeto_SistemaCodigo ;
      private int AV80WWProjetoDS_1_Projeto_areatrabalhocodigo ;
      private int AV74PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavProjeto_sigla1_Visible ;
      private int edtavProjeto_nome1_Visible ;
      private int edtavProjeto_sigla2_Visible ;
      private int edtavProjeto_nome2_Visible ;
      private int edtavProjeto_sigla3_Visible ;
      private int edtavProjeto_nome3_Visible ;
      private int AV102GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV75GridCurrentPage ;
      private long AV76GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A655Projeto_Custo ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_81_idx="0001" ;
      private String AV31Projeto_Sigla1 ;
      private String AV17Projeto_Nome1 ;
      private String AV33Projeto_TecnicaContagem1 ;
      private String AV34Projeto_Status1 ;
      private String AV35Projeto_Sigla2 ;
      private String AV21Projeto_Nome2 ;
      private String AV37Projeto_TecnicaContagem2 ;
      private String AV38Projeto_Status2 ;
      private String AV39Projeto_Sigla3 ;
      private String AV25Projeto_Nome3 ;
      private String AV41Projeto_TecnicaContagem3 ;
      private String AV42Projeto_Status3 ;
      private String AV101Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtProjeto_Codigo_Internalname ;
      private String A649Projeto_Nome ;
      private String edtProjeto_Nome_Internalname ;
      private String A650Projeto_Sigla ;
      private String edtProjeto_Sigla_Internalname ;
      private String edtProjeto_Prazo_Internalname ;
      private String edtProjeto_Custo_Internalname ;
      private String chkProjeto_Incremental_Internalname ;
      private String edtProjeto_Esforco_Internalname ;
      private String cmbProjeto_Status_Internalname ;
      private String A658Projeto_Status ;
      private String scmdbuf ;
      private String GXCCtl ;
      private String dynavProjeto_areatrabalhocodigo_Internalname ;
      private String gxwrpcisep ;
      private String lV82WWProjetoDS_3_Projeto_sigla1 ;
      private String lV83WWProjetoDS_4_Projeto_nome1 ;
      private String lV88WWProjetoDS_9_Projeto_sigla2 ;
      private String lV89WWProjetoDS_10_Projeto_nome2 ;
      private String lV94WWProjetoDS_15_Projeto_sigla3 ;
      private String lV95WWProjetoDS_16_Projeto_nome3 ;
      private String AV82WWProjetoDS_3_Projeto_sigla1 ;
      private String AV83WWProjetoDS_4_Projeto_nome1 ;
      private String AV84WWProjetoDS_5_Projeto_tecnicacontagem1 ;
      private String AV85WWProjetoDS_6_Projeto_status1 ;
      private String AV88WWProjetoDS_9_Projeto_sigla2 ;
      private String AV89WWProjetoDS_10_Projeto_nome2 ;
      private String AV90WWProjetoDS_11_Projeto_tecnicacontagem2 ;
      private String AV91WWProjetoDS_12_Projeto_status2 ;
      private String AV94WWProjetoDS_15_Projeto_sigla3 ;
      private String AV95WWProjetoDS_16_Projeto_nome3 ;
      private String AV96WWProjetoDS_17_Projeto_tecnicacontagem3 ;
      private String AV97WWProjetoDS_18_Projeto_status3 ;
      private String A652Projeto_TecnicaContagem ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavProjeto_sigla1_Internalname ;
      private String edtavProjeto_nome1_Internalname ;
      private String cmbavProjeto_tecnicacontagem1_Internalname ;
      private String cmbavProjeto_status1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavProjeto_sigla2_Internalname ;
      private String edtavProjeto_nome2_Internalname ;
      private String cmbavProjeto_tecnicacontagem2_Internalname ;
      private String cmbavProjeto_status2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavProjeto_sigla3_Internalname ;
      private String edtavProjeto_nome3_Internalname ;
      private String cmbavProjeto_tecnicacontagem3_Internalname ;
      private String cmbavProjeto_status3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtProjeto_Nome_Link ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblProjetotitle_Internalname ;
      private String lblProjetotitle_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextprojeto_areatrabalhocodigo_Internalname ;
      private String lblFiltertextprojeto_areatrabalhocodigo_Jsonclick ;
      private String dynavProjeto_areatrabalhocodigo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavProjeto_sigla1_Jsonclick ;
      private String edtavProjeto_nome1_Jsonclick ;
      private String cmbavProjeto_tecnicacontagem1_Jsonclick ;
      private String cmbavProjeto_status1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavProjeto_sigla2_Jsonclick ;
      private String edtavProjeto_nome2_Jsonclick ;
      private String cmbavProjeto_tecnicacontagem2_Jsonclick ;
      private String cmbavProjeto_status2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavProjeto_sigla3_Jsonclick ;
      private String edtavProjeto_nome3_Jsonclick ;
      private String cmbavProjeto_tecnicacontagem3_Jsonclick ;
      private String cmbavProjeto_status3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_81_fel_idx="0001" ;
      private String ROClassString ;
      private String edtProjeto_Codigo_Jsonclick ;
      private String edtProjeto_Nome_Jsonclick ;
      private String edtProjeto_Sigla_Jsonclick ;
      private String edtProjeto_Prazo_Jsonclick ;
      private String edtProjeto_Custo_Jsonclick ;
      private String edtProjeto_Esforco_Jsonclick ;
      private String cmbProjeto_Status_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n656Projeto_Prazo ;
      private bool A1232Projeto_Incremental ;
      private bool n1232Projeto_Incremental ;
      private bool AV86WWProjetoDS_7_Dynamicfiltersenabled2 ;
      private bool AV92WWProjetoDS_13_Dynamicfiltersenabled3 ;
      private bool n2155Projeto_SistemaCodigo ;
      private bool n2151Projeto_AreaTrabalhoCodigo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV43Display_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV98Update_GXI ;
      private String AV99Delete_GXI ;
      private String AV100Display_GXI ;
      private String AV81WWProjetoDS_2_Dynamicfiltersselector1 ;
      private String AV87WWProjetoDS_8_Dynamicfiltersselector2 ;
      private String AV93WWProjetoDS_14_Dynamicfiltersselector3 ;
      private String AV28Update ;
      private String AV29Delete ;
      private String AV43Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV30Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavProjeto_areatrabalhocodigo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavProjeto_tecnicacontagem1 ;
      private GXCombobox cmbavProjeto_status1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavProjeto_tecnicacontagem2 ;
      private GXCombobox cmbavProjeto_status2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavProjeto_tecnicacontagem3 ;
      private GXCombobox cmbavProjeto_status3 ;
      private GXCheckbox chkProjeto_Incremental ;
      private GXCombobox cmbProjeto_Status ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00DF2_A5AreaTrabalho_Codigo ;
      private String[] H00DF2_A6AreaTrabalho_Descricao ;
      private int[] H00DF3_A5AreaTrabalho_Codigo ;
      private String[] H00DF3_A6AreaTrabalho_Descricao ;
      private int[] H00DF5_A2155Projeto_SistemaCodigo ;
      private bool[] H00DF5_n2155Projeto_SistemaCodigo ;
      private int[] H00DF5_A2151Projeto_AreaTrabalhoCodigo ;
      private bool[] H00DF5_n2151Projeto_AreaTrabalhoCodigo ;
      private String[] H00DF5_A652Projeto_TecnicaContagem ;
      private String[] H00DF5_A658Projeto_Status ;
      private bool[] H00DF5_A1232Projeto_Incremental ;
      private bool[] H00DF5_n1232Projeto_Incremental ;
      private String[] H00DF5_A650Projeto_Sigla ;
      private String[] H00DF5_A649Projeto_Nome ;
      private int[] H00DF5_A648Projeto_Codigo ;
      private short[] H00DF5_A657Projeto_Esforco ;
      private decimal[] H00DF5_A655Projeto_Custo ;
      private short[] H00DF5_A656Projeto_Prazo ;
      private bool[] H00DF5_n656Projeto_Prazo ;
      private long[] H00DF7_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wwprojeto__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00DF5( IGxContext context ,
                                             String AV81WWProjetoDS_2_Dynamicfiltersselector1 ,
                                             String AV82WWProjetoDS_3_Projeto_sigla1 ,
                                             String AV83WWProjetoDS_4_Projeto_nome1 ,
                                             String AV84WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                             String AV85WWProjetoDS_6_Projeto_status1 ,
                                             bool AV86WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                             String AV87WWProjetoDS_8_Dynamicfiltersselector2 ,
                                             String AV88WWProjetoDS_9_Projeto_sigla2 ,
                                             String AV89WWProjetoDS_10_Projeto_nome2 ,
                                             String AV90WWProjetoDS_11_Projeto_tecnicacontagem2 ,
                                             String AV91WWProjetoDS_12_Projeto_status2 ,
                                             bool AV92WWProjetoDS_13_Dynamicfiltersenabled3 ,
                                             String AV93WWProjetoDS_14_Dynamicfiltersselector3 ,
                                             String AV94WWProjetoDS_15_Projeto_sigla3 ,
                                             String AV95WWProjetoDS_16_Projeto_nome3 ,
                                             String AV96WWProjetoDS_17_Projeto_tecnicacontagem3 ,
                                             String AV97WWProjetoDS_18_Projeto_status3 ,
                                             String A650Projeto_Sigla ,
                                             String A649Projeto_Nome ,
                                             String A652Projeto_TecnicaContagem ,
                                             String A658Projeto_Status ,
                                             int A2151Projeto_AreaTrabalhoCodigo ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [18] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Projeto_SistemaCodigo], T1.[Projeto_AreaTrabalhoCodigo], T1.[Projeto_TecnicaContagem], T1.[Projeto_Status], T1.[Projeto_Incremental], T1.[Projeto_Sigla], T1.[Projeto_Nome], T1.[Projeto_Codigo], COALESCE( T2.[Projeto_Esforco], 0) AS Projeto_Esforco, COALESCE( T2.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T2.[Projeto_Prazo], 0) AS Projeto_Prazo";
         sFromString = " FROM ([Projeto] T1 WITH (NOLOCK) LEFT JOIN (SELECT [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Codigo], [Sistema_Custo] AS Projeto_Custo, [Sistema_Esforco] AS Projeto_Esforco FROM [Sistema] WITH (NOLOCK) ) T2 ON T2.[Sistema_Codigo] = T1.[Projeto_SistemaCodigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Projeto_AreaTrabalhoCodigo] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV81WWProjetoDS_2_Dynamicfiltersselector1, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWProjetoDS_3_Projeto_sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV82WWProjetoDS_3_Projeto_sigla1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWProjetoDS_2_Dynamicfiltersselector1, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWProjetoDS_4_Projeto_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV83WWProjetoDS_4_Projeto_nome1 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWProjetoDS_2_Dynamicfiltersselector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWProjetoDS_5_Projeto_tecnicacontagem1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV84WWProjetoDS_5_Projeto_tecnicacontagem1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWProjetoDS_2_Dynamicfiltersselector1, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWProjetoDS_6_Projeto_status1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV85WWProjetoDS_6_Projeto_status1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV86WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWProjetoDS_9_Projeto_sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV88WWProjetoDS_9_Projeto_sigla2 + '%')";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV86WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWProjetoDS_10_Projeto_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV89WWProjetoDS_10_Projeto_nome2 + '%')";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV86WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWProjetoDS_11_Projeto_tecnicacontagem2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV90WWProjetoDS_11_Projeto_tecnicacontagem2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV86WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWProjetoDS_12_Projeto_status2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV91WWProjetoDS_12_Projeto_status2)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV92WWProjetoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_14_Dynamicfiltersselector3, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWProjetoDS_15_Projeto_sigla3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV94WWProjetoDS_15_Projeto_sigla3 + '%')";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV92WWProjetoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_14_Dynamicfiltersselector3, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWProjetoDS_16_Projeto_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV95WWProjetoDS_16_Projeto_nome3 + '%')";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV92WWProjetoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_14_Dynamicfiltersselector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWProjetoDS_17_Projeto_tecnicacontagem3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV96WWProjetoDS_17_Projeto_tecnicacontagem3)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV92WWProjetoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_14_Dynamicfiltersselector3, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWProjetoDS_18_Projeto_status3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV97WWProjetoDS_18_Projeto_status3)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         sOrderString = sOrderString + " ORDER BY T1.[Projeto_Sigla]";
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00DF7( IGxContext context ,
                                             String AV81WWProjetoDS_2_Dynamicfiltersselector1 ,
                                             String AV82WWProjetoDS_3_Projeto_sigla1 ,
                                             String AV83WWProjetoDS_4_Projeto_nome1 ,
                                             String AV84WWProjetoDS_5_Projeto_tecnicacontagem1 ,
                                             String AV85WWProjetoDS_6_Projeto_status1 ,
                                             bool AV86WWProjetoDS_7_Dynamicfiltersenabled2 ,
                                             String AV87WWProjetoDS_8_Dynamicfiltersselector2 ,
                                             String AV88WWProjetoDS_9_Projeto_sigla2 ,
                                             String AV89WWProjetoDS_10_Projeto_nome2 ,
                                             String AV90WWProjetoDS_11_Projeto_tecnicacontagem2 ,
                                             String AV91WWProjetoDS_12_Projeto_status2 ,
                                             bool AV92WWProjetoDS_13_Dynamicfiltersenabled3 ,
                                             String AV93WWProjetoDS_14_Dynamicfiltersselector3 ,
                                             String AV94WWProjetoDS_15_Projeto_sigla3 ,
                                             String AV95WWProjetoDS_16_Projeto_nome3 ,
                                             String AV96WWProjetoDS_17_Projeto_tecnicacontagem3 ,
                                             String AV97WWProjetoDS_18_Projeto_status3 ,
                                             String A650Projeto_Sigla ,
                                             String A649Projeto_Nome ,
                                             String A652Projeto_TecnicaContagem ,
                                             String A658Projeto_Status ,
                                             int A2151Projeto_AreaTrabalhoCodigo ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Projeto] T1 WITH (NOLOCK) LEFT JOIN (SELECT [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Codigo], [Sistema_Custo] AS Projeto_Custo, [Sistema_Esforco] AS Projeto_Esforco FROM [Sistema] WITH (NOLOCK) ) T2 ON T2.[Sistema_Codigo] = T1.[Projeto_SistemaCodigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Projeto_AreaTrabalhoCodigo] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV81WWProjetoDS_2_Dynamicfiltersselector1, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWProjetoDS_3_Projeto_sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV82WWProjetoDS_3_Projeto_sigla1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWProjetoDS_2_Dynamicfiltersselector1, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWProjetoDS_4_Projeto_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV83WWProjetoDS_4_Projeto_nome1 + '%')";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWProjetoDS_2_Dynamicfiltersselector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWProjetoDS_5_Projeto_tecnicacontagem1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV84WWProjetoDS_5_Projeto_tecnicacontagem1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWProjetoDS_2_Dynamicfiltersselector1, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWProjetoDS_6_Projeto_status1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV85WWProjetoDS_6_Projeto_status1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV86WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWProjetoDS_9_Projeto_sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV88WWProjetoDS_9_Projeto_sigla2 + '%')";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV86WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWProjetoDS_10_Projeto_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV89WWProjetoDS_10_Projeto_nome2 + '%')";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV86WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWProjetoDS_11_Projeto_tecnicacontagem2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV90WWProjetoDS_11_Projeto_tecnicacontagem2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV86WWProjetoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWProjetoDS_8_Dynamicfiltersselector2, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWProjetoDS_12_Projeto_status2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV91WWProjetoDS_12_Projeto_status2)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV92WWProjetoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_14_Dynamicfiltersselector3, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWProjetoDS_15_Projeto_sigla3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV94WWProjetoDS_15_Projeto_sigla3 + '%')";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV92WWProjetoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_14_Dynamicfiltersselector3, "PROJETO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWProjetoDS_16_Projeto_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Nome] like '%' + @lV95WWProjetoDS_16_Projeto_nome3 + '%')";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV92WWProjetoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_14_Dynamicfiltersselector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWProjetoDS_17_Projeto_tecnicacontagem3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV96WWProjetoDS_17_Projeto_tecnicacontagem3)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV92WWProjetoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWProjetoDS_14_Dynamicfiltersselector3, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWProjetoDS_18_Projeto_status3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV97WWProjetoDS_18_Projeto_status3)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00DF5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] );
               case 3 :
                     return conditional_H00DF7(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DF2 ;
          prmH00DF2 = new Object[] {
          } ;
          Object[] prmH00DF3 ;
          prmH00DF3 = new Object[] {
          } ;
          Object[] prmH00DF5 ;
          prmH00DF5 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV82WWProjetoDS_3_Projeto_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV83WWProjetoDS_4_Projeto_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV84WWProjetoDS_5_Projeto_tecnicacontagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV85WWProjetoDS_6_Projeto_status1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV88WWProjetoDS_9_Projeto_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV89WWProjetoDS_10_Projeto_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV90WWProjetoDS_11_Projeto_tecnicacontagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV91WWProjetoDS_12_Projeto_status2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV94WWProjetoDS_15_Projeto_sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV95WWProjetoDS_16_Projeto_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV96WWProjetoDS_17_Projeto_tecnicacontagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV97WWProjetoDS_18_Projeto_status3",SqlDbType.Char,1,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00DF7 ;
          prmH00DF7 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV82WWProjetoDS_3_Projeto_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV83WWProjetoDS_4_Projeto_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV84WWProjetoDS_5_Projeto_tecnicacontagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV85WWProjetoDS_6_Projeto_status1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV88WWProjetoDS_9_Projeto_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV89WWProjetoDS_10_Projeto_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV90WWProjetoDS_11_Projeto_tecnicacontagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV91WWProjetoDS_12_Projeto_status2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV94WWProjetoDS_15_Projeto_sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV95WWProjetoDS_16_Projeto_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV96WWProjetoDS_17_Projeto_tecnicacontagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV97WWProjetoDS_18_Projeto_status3",SqlDbType.Char,1,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DF2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DF2,0,0,true,false )
             ,new CursorDef("H00DF3", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DF3,0,0,true,false )
             ,new CursorDef("H00DF5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DF5,11,0,true,false )
             ,new CursorDef("H00DF7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DF7,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 15) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((short[]) buf[11])[0] = rslt.getShort(9) ;
                ((decimal[]) buf[12])[0] = rslt.getDecimal(10) ;
                ((short[]) buf[13])[0] = rslt.getShort(11) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

}
