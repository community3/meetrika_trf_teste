/*
               File: ReferenciaTecnicaGeneral
        Description: Referencia Tecnica General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:58:29.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class referenciatecnicageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public referenciatecnicageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public referenciatecnicageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ReferenciaTecnica_Codigo )
      {
         this.A97ReferenciaTecnica_Codigo = aP0_ReferenciaTecnica_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbReferenciaTecnica_Unidade = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A97ReferenciaTecnica_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A97ReferenciaTecnica_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA5C2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ReferenciaTecnicaGeneral";
               context.Gx_err = 0;
               WS5C2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Referencia Tecnica General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822582936");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("referenciatecnicageneral.aspx") + "?" + UrlEncode("" +A97ReferenciaTecnica_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA97ReferenciaTecnica_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REFERENCIATECNICA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A98ReferenciaTecnica_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REFERENCIATECNICA_DESCRICAO", GetSecureSignedToken( sPrefix, A99ReferenciaTecnica_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REFERENCIATECNICA_UNIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A114ReferenciaTecnica_Unidade), "Z9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REFERENCIATECNICA_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A100ReferenciaTecnica_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_GUIA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ReferenciaTecnicaGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("referenciatecnicageneral:[SendSecurityCheck value for]"+"Guia_Codigo:"+context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm5C2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("referenciatecnicageneral.js", "?202042822582938");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ReferenciaTecnicaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Referencia Tecnica General" ;
      }

      protected void WB5C0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "referenciatecnicageneral.aspx");
            }
            wb_table1_2_5C2( true) ;
         }
         else
         {
            wb_table1_2_5C2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_5C2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START5C2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Referencia Tecnica General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP5C0( ) ;
            }
         }
      }

      protected void WS5C2( )
      {
         START5C2( ) ;
         EVT5C2( ) ;
      }

      protected void EVT5C2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5C0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5C0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E115C2 */
                                    E115C2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5C0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E125C2 */
                                    E125C2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5C0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E135C2 */
                                    E135C2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5C0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E145C2 */
                                    E145C2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5C0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5C0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE5C2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm5C2( ) ;
            }
         }
      }

      protected void PA5C2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbReferenciaTecnica_Unidade.Name = "REFERENCIATECNICA_UNIDADE";
            cmbReferenciaTecnica_Unidade.WebTags = "";
            cmbReferenciaTecnica_Unidade.addItem("1", "Percentual", 0);
            cmbReferenciaTecnica_Unidade.addItem("2", "Ponto Fun��o", 0);
            if ( cmbReferenciaTecnica_Unidade.ItemCount > 0 )
            {
               A114ReferenciaTecnica_Unidade = (short)(NumberUtil.Val( cmbReferenciaTecnica_Unidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A114ReferenciaTecnica_Unidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIATECNICA_UNIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A114ReferenciaTecnica_Unidade), "Z9")));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbReferenciaTecnica_Unidade.ItemCount > 0 )
         {
            A114ReferenciaTecnica_Unidade = (short)(NumberUtil.Val( cmbReferenciaTecnica_Unidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A114ReferenciaTecnica_Unidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIATECNICA_UNIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A114ReferenciaTecnica_Unidade), "Z9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF5C2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ReferenciaTecnicaGeneral";
         context.Gx_err = 0;
      }

      protected void RF5C2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H005C2 */
            pr_default.execute(0, new Object[] {A97ReferenciaTecnica_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A94Guia_Nome = H005C2_A94Guia_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A94Guia_Nome", A94Guia_Nome);
               A93Guia_Codigo = H005C2_A93Guia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_GUIA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")));
               A100ReferenciaTecnica_Valor = H005C2_A100ReferenciaTecnica_Valor[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A100ReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( A100ReferenciaTecnica_Valor, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIATECNICA_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A100ReferenciaTecnica_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A114ReferenciaTecnica_Unidade = H005C2_A114ReferenciaTecnica_Unidade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A114ReferenciaTecnica_Unidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIATECNICA_UNIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A114ReferenciaTecnica_Unidade), "Z9")));
               A99ReferenciaTecnica_Descricao = H005C2_A99ReferenciaTecnica_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A99ReferenciaTecnica_Descricao", A99ReferenciaTecnica_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIATECNICA_DESCRICAO", GetSecureSignedToken( sPrefix, A99ReferenciaTecnica_Descricao));
               A98ReferenciaTecnica_Nome = H005C2_A98ReferenciaTecnica_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A98ReferenciaTecnica_Nome", A98ReferenciaTecnica_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIATECNICA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A98ReferenciaTecnica_Nome, "@!"))));
               A94Guia_Nome = H005C2_A94Guia_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A94Guia_Nome", A94Guia_Nome);
               /* Execute user event: E125C2 */
               E125C2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB5C0( ) ;
         }
      }

      protected void STRUP5C0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ReferenciaTecnicaGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E115C2 */
         E115C2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A98ReferenciaTecnica_Nome = StringUtil.Upper( cgiGet( edtReferenciaTecnica_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A98ReferenciaTecnica_Nome", A98ReferenciaTecnica_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIATECNICA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A98ReferenciaTecnica_Nome, "@!"))));
            A99ReferenciaTecnica_Descricao = cgiGet( edtReferenciaTecnica_Descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A99ReferenciaTecnica_Descricao", A99ReferenciaTecnica_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIATECNICA_DESCRICAO", GetSecureSignedToken( sPrefix, A99ReferenciaTecnica_Descricao));
            cmbReferenciaTecnica_Unidade.CurrentValue = cgiGet( cmbReferenciaTecnica_Unidade_Internalname);
            A114ReferenciaTecnica_Unidade = (short)(NumberUtil.Val( cgiGet( cmbReferenciaTecnica_Unidade_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A114ReferenciaTecnica_Unidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIATECNICA_UNIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A114ReferenciaTecnica_Unidade), "Z9")));
            A100ReferenciaTecnica_Valor = context.localUtil.CToN( cgiGet( edtReferenciaTecnica_Valor_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A100ReferenciaTecnica_Valor", StringUtil.LTrim( StringUtil.Str( A100ReferenciaTecnica_Valor, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIATECNICA_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A100ReferenciaTecnica_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_GUIA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")));
            A94Guia_Nome = StringUtil.Upper( cgiGet( edtGuia_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A94Guia_Nome", A94Guia_Nome);
            /* Read saved values. */
            wcpOA97ReferenciaTecnica_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA97ReferenciaTecnica_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ReferenciaTecnicaGeneral";
            A93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_GUIA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("referenciatecnicageneral:[SecurityCheckFailed value for]"+"Guia_Codigo:"+context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E115C2 */
         E115C2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E115C2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E125C2( )
      {
         /* Load Routine */
         edtGuia_Nome_Link = formatLink("viewguia.aspx") + "?" + UrlEncode("" +A93Guia_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGuia_Nome_Internalname, "Link", edtGuia_Nome_Link);
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E135C2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("referenciatecnica.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A97ReferenciaTecnica_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E145C2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("referenciatecnica.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A97ReferenciaTecnica_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ReferenciaTecnica";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ReferenciaTecnica_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ReferenciaTecnica_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_5C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_5C2( true) ;
         }
         else
         {
            wb_table2_8_5C2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_5C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_46_5C2( true) ;
         }
         else
         {
            wb_table3_46_5C2( false) ;
         }
         return  ;
      }

      protected void wb_table3_46_5C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_5C2e( true) ;
         }
         else
         {
            wb_table1_2_5C2e( false) ;
         }
      }

      protected void wb_table3_46_5C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_46_5C2e( true) ;
         }
         else
         {
            wb_table3_46_5C2e( false) ;
         }
      }

      protected void wb_table2_8_5C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciatecnica_codigo_Internalname, "C�digo", "", "", lblTextblockreferenciatecnica_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtReferenciaTecnica_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A97ReferenciaTecnica_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReferenciaTecnica_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciatecnica_nome_Internalname, "da Refer�ncia", "", "", lblTextblockreferenciatecnica_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtReferenciaTecnica_Nome_Internalname, StringUtil.RTrim( A98ReferenciaTecnica_Nome), StringUtil.RTrim( context.localUtil.Format( A98ReferenciaTecnica_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReferenciaTecnica_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciatecnica_descricao_Internalname, "Descri��o", "", "", lblTextblockreferenciatecnica_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtReferenciaTecnica_Descricao_Internalname, A99ReferenciaTecnica_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciatecnica_unidade_Internalname, "Unidade", "", "", lblTextblockreferenciatecnica_unidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbReferenciaTecnica_Unidade, cmbReferenciaTecnica_Unidade_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0)), 1, cmbReferenciaTecnica_Unidade_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ReferenciaTecnicaGeneral.htm");
            cmbReferenciaTecnica_Unidade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A114ReferenciaTecnica_Unidade), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbReferenciaTecnica_Unidade_Internalname, "Values", (String)(cmbReferenciaTecnica_Unidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciatecnica_valor_Internalname, "Valor", "", "", lblTextblockreferenciatecnica_valor_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtReferenciaTecnica_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A100ReferenciaTecnica_Valor, 18, 5, ",", "")), context.localUtil.Format( A100ReferenciaTecnica_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReferenciaTecnica_Valor_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockguia_codigo_Internalname, "C�digo", "", "", lblTextblockguia_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGuia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A93Guia_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockguia_nome_Internalname, "Nome", "", "", lblTextblockguia_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGuia_Nome_Internalname, StringUtil.RTrim( A94Guia_Nome), StringUtil.RTrim( context.localUtil.Format( A94Guia_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtGuia_Nome_Link, "", "", "", edtGuia_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ReferenciaTecnicaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_5C2e( true) ;
         }
         else
         {
            wb_table2_8_5C2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A97ReferenciaTecnica_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA5C2( ) ;
         WS5C2( ) ;
         WE5C2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA97ReferenciaTecnica_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA5C2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "referenciatecnicageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA5C2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A97ReferenciaTecnica_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
         }
         wcpOA97ReferenciaTecnica_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA97ReferenciaTecnica_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A97ReferenciaTecnica_Codigo != wcpOA97ReferenciaTecnica_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA97ReferenciaTecnica_Codigo = A97ReferenciaTecnica_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA97ReferenciaTecnica_Codigo = cgiGet( sPrefix+"A97ReferenciaTecnica_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA97ReferenciaTecnica_Codigo) > 0 )
         {
            A97ReferenciaTecnica_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA97ReferenciaTecnica_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A97ReferenciaTecnica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0)));
         }
         else
         {
            A97ReferenciaTecnica_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A97ReferenciaTecnica_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA5C2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS5C2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS5C2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A97ReferenciaTecnica_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A97ReferenciaTecnica_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA97ReferenciaTecnica_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A97ReferenciaTecnica_Codigo_CTRL", StringUtil.RTrim( sCtrlA97ReferenciaTecnica_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE5C2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822582966");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("referenciatecnicageneral.js", "?202042822582966");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockreferenciatecnica_codigo_Internalname = sPrefix+"TEXTBLOCKREFERENCIATECNICA_CODIGO";
         edtReferenciaTecnica_Codigo_Internalname = sPrefix+"REFERENCIATECNICA_CODIGO";
         lblTextblockreferenciatecnica_nome_Internalname = sPrefix+"TEXTBLOCKREFERENCIATECNICA_NOME";
         edtReferenciaTecnica_Nome_Internalname = sPrefix+"REFERENCIATECNICA_NOME";
         lblTextblockreferenciatecnica_descricao_Internalname = sPrefix+"TEXTBLOCKREFERENCIATECNICA_DESCRICAO";
         edtReferenciaTecnica_Descricao_Internalname = sPrefix+"REFERENCIATECNICA_DESCRICAO";
         lblTextblockreferenciatecnica_unidade_Internalname = sPrefix+"TEXTBLOCKREFERENCIATECNICA_UNIDADE";
         cmbReferenciaTecnica_Unidade_Internalname = sPrefix+"REFERENCIATECNICA_UNIDADE";
         lblTextblockreferenciatecnica_valor_Internalname = sPrefix+"TEXTBLOCKREFERENCIATECNICA_VALOR";
         edtReferenciaTecnica_Valor_Internalname = sPrefix+"REFERENCIATECNICA_VALOR";
         lblTextblockguia_codigo_Internalname = sPrefix+"TEXTBLOCKGUIA_CODIGO";
         edtGuia_Codigo_Internalname = sPrefix+"GUIA_CODIGO";
         lblTextblockguia_nome_Internalname = sPrefix+"TEXTBLOCKGUIA_NOME";
         edtGuia_Nome_Internalname = sPrefix+"GUIA_NOME";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtGuia_Nome_Jsonclick = "";
         edtGuia_Codigo_Jsonclick = "";
         edtReferenciaTecnica_Valor_Jsonclick = "";
         cmbReferenciaTecnica_Unidade_Jsonclick = "";
         edtReferenciaTecnica_Nome_Jsonclick = "";
         edtReferenciaTecnica_Codigo_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         edtGuia_Nome_Link = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E135C2',iparms:[{av:'A97ReferenciaTecnica_Codigo',fld:'REFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E145C2',iparms:[{av:'A97ReferenciaTecnica_Codigo',fld:'REFERENCIATECNICA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A98ReferenciaTecnica_Nome = "";
         A99ReferenciaTecnica_Descricao = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H005C2_A97ReferenciaTecnica_Codigo = new int[1] ;
         H005C2_A94Guia_Nome = new String[] {""} ;
         H005C2_A93Guia_Codigo = new int[1] ;
         H005C2_A100ReferenciaTecnica_Valor = new decimal[1] ;
         H005C2_A114ReferenciaTecnica_Unidade = new short[1] ;
         H005C2_A99ReferenciaTecnica_Descricao = new String[] {""} ;
         H005C2_A98ReferenciaTecnica_Nome = new String[] {""} ;
         A94Guia_Nome = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockreferenciatecnica_codigo_Jsonclick = "";
         lblTextblockreferenciatecnica_nome_Jsonclick = "";
         lblTextblockreferenciatecnica_descricao_Jsonclick = "";
         lblTextblockreferenciatecnica_unidade_Jsonclick = "";
         lblTextblockreferenciatecnica_valor_Jsonclick = "";
         lblTextblockguia_codigo_Jsonclick = "";
         lblTextblockguia_nome_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA97ReferenciaTecnica_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.referenciatecnicageneral__default(),
            new Object[][] {
                new Object[] {
               H005C2_A97ReferenciaTecnica_Codigo, H005C2_A94Guia_Nome, H005C2_A93Guia_Codigo, H005C2_A100ReferenciaTecnica_Valor, H005C2_A114ReferenciaTecnica_Unidade, H005C2_A99ReferenciaTecnica_Descricao, H005C2_A98ReferenciaTecnica_Nome
               }
            }
         );
         AV14Pgmname = "ReferenciaTecnicaGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ReferenciaTecnicaGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A114ReferenciaTecnica_Unidade ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A97ReferenciaTecnica_Codigo ;
      private int wcpOA97ReferenciaTecnica_Codigo ;
      private int A93Guia_Codigo ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7ReferenciaTecnica_Codigo ;
      private int idxLst ;
      private decimal A100ReferenciaTecnica_Valor ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A98ReferenciaTecnica_Nome ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A94Guia_Nome ;
      private String edtReferenciaTecnica_Nome_Internalname ;
      private String edtReferenciaTecnica_Descricao_Internalname ;
      private String cmbReferenciaTecnica_Unidade_Internalname ;
      private String edtReferenciaTecnica_Valor_Internalname ;
      private String edtGuia_Codigo_Internalname ;
      private String edtGuia_Nome_Internalname ;
      private String hsh ;
      private String edtGuia_Nome_Link ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockreferenciatecnica_codigo_Internalname ;
      private String lblTextblockreferenciatecnica_codigo_Jsonclick ;
      private String edtReferenciaTecnica_Codigo_Internalname ;
      private String edtReferenciaTecnica_Codigo_Jsonclick ;
      private String lblTextblockreferenciatecnica_nome_Internalname ;
      private String lblTextblockreferenciatecnica_nome_Jsonclick ;
      private String edtReferenciaTecnica_Nome_Jsonclick ;
      private String lblTextblockreferenciatecnica_descricao_Internalname ;
      private String lblTextblockreferenciatecnica_descricao_Jsonclick ;
      private String lblTextblockreferenciatecnica_unidade_Internalname ;
      private String lblTextblockreferenciatecnica_unidade_Jsonclick ;
      private String cmbReferenciaTecnica_Unidade_Jsonclick ;
      private String lblTextblockreferenciatecnica_valor_Internalname ;
      private String lblTextblockreferenciatecnica_valor_Jsonclick ;
      private String edtReferenciaTecnica_Valor_Jsonclick ;
      private String lblTextblockguia_codigo_Internalname ;
      private String lblTextblockguia_codigo_Jsonclick ;
      private String edtGuia_Codigo_Jsonclick ;
      private String lblTextblockguia_nome_Internalname ;
      private String lblTextblockguia_nome_Jsonclick ;
      private String edtGuia_Nome_Jsonclick ;
      private String sCtrlA97ReferenciaTecnica_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String A99ReferenciaTecnica_Descricao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbReferenciaTecnica_Unidade ;
      private IDataStoreProvider pr_default ;
      private int[] H005C2_A97ReferenciaTecnica_Codigo ;
      private String[] H005C2_A94Guia_Nome ;
      private int[] H005C2_A93Guia_Codigo ;
      private decimal[] H005C2_A100ReferenciaTecnica_Valor ;
      private short[] H005C2_A114ReferenciaTecnica_Unidade ;
      private String[] H005C2_A99ReferenciaTecnica_Descricao ;
      private String[] H005C2_A98ReferenciaTecnica_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class referenciatecnicageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH005C2 ;
          prmH005C2 = new Object[] {
          new Object[] {"@ReferenciaTecnica_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H005C2", "SELECT T1.[ReferenciaTecnica_Codigo], T2.[Guia_Nome], T1.[Guia_Codigo], T1.[ReferenciaTecnica_Valor], T1.[ReferenciaTecnica_Unidade], T1.[ReferenciaTecnica_Descricao], T1.[ReferenciaTecnica_Nome] FROM ([ReferenciaTecnica] T1 WITH (NOLOCK) INNER JOIN [Guia] T2 WITH (NOLOCK) ON T2.[Guia_Codigo] = T1.[Guia_Codigo]) WHERE T1.[ReferenciaTecnica_Codigo] = @ReferenciaTecnica_Codigo ORDER BY T1.[ReferenciaTecnica_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005C2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
